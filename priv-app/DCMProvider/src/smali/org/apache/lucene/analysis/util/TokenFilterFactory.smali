.class public abstract Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.super Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
.source "TokenFilterFactory.java"


# static fields
.field private static final loader:Lorg/apache/lucene/analysis/util/AnalysisSPILoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/AnalysisSPILoader",
            "<",
            "Lorg/apache/lucene/analysis/util/TokenFilterFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 32
    new-instance v0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;

    const-class v1, Lorg/apache/lucene/analysis/util/TokenFilterFactory;

    .line 33
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TokenFilterFactory"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "FilterFactory"

    aput-object v4, v2, v3

    .line 32
    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;-><init>(Ljava/lang/Class;[Ljava/lang/String;)V

    .line 31
    sput-object v0, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->loader:Lorg/apache/lucene/analysis/util/AnalysisSPILoader;

    .line 33
    return-void
.end method

.method protected constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;-><init>(Ljava/util/Map;)V

    .line 70
    return-void
.end method

.method public static availableTokenFilters()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    sget-object v0, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->loader:Lorg/apache/lucene/analysis/util/AnalysisSPILoader;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->availableServices()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static forName(Ljava/lang/String;Ljava/util/Map;)Lorg/apache/lucene/analysis/util/TokenFilterFactory;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/analysis/util/TokenFilterFactory;"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v0, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->loader:Lorg/apache/lucene/analysis/util/AnalysisSPILoader;

    invoke-virtual {v0, p0, p1}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->newInstance(Ljava/lang/String;Ljava/util/Map;)Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/util/TokenFilterFactory;

    return-object v0
.end method

.method public static lookupClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/analysis/util/TokenFilterFactory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    sget-object v0, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->loader:Lorg/apache/lucene/analysis/util/AnalysisSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->lookupClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public static reloadTokenFilters(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p0, "classloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 62
    sget-object v0, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->loader:Lorg/apache/lucene/analysis/util/AnalysisSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 63
    return-void
.end method


# virtual methods
.method public abstract create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
.end method
