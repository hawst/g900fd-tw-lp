.class public Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilterFactory;
.super Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;
.source "CommonGramsQueryFilterFactory.java"


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;-><init>(Ljava/util/Map;)V

    .line 43
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenFilter;
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenFilter;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;

    .line 51
    .local v0, "commonGrams":Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;
    new-instance v1, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;-><init>(Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;)V

    return-object v1
.end method
