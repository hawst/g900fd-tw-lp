.class public Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;
.super Ljava/lang/Object;
.source "Hyphen.java"


# instance fields
.field public noBreak:Ljava/lang/String;

.field public postBreak:Ljava/lang/String;

.field public preBreak:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "pre"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->preBreak:Ljava/lang/String;

    .line 48
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->noBreak:Ljava/lang/String;

    .line 49
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->postBreak:Ljava/lang/String;

    .line 50
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pre"    # Ljava/lang/String;
    .param p2, "no"    # Ljava/lang/String;
    .param p3, "post"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->preBreak:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->noBreak:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->postBreak:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->noBreak:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->postBreak:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->preBreak:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->preBreak:Ljava/lang/String;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    const-string v1, "-"

    .line 65
    :goto_0
    return-object v1

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "res":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->preBreak:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    const-string/jumbo v1, "}{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->postBreak:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string/jumbo v1, "}{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->noBreak:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
