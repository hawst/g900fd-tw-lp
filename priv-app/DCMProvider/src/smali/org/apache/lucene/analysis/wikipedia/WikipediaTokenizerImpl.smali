.class Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;
.super Ljava/lang/Object;
.source "WikipediaTokenizerImpl.java"


# static fields
.field public static final ACRONYM:I = 0x2

.field public static final ALPHANUM:I = 0x0

.field public static final APOSTROPHE:I = 0x1

.field public static final BOLD:I = 0xc

.field public static final BOLD_ITALICS:I = 0xe

.field public static final CATEGORY:I = 0xb

.field public static final CATEGORY_STATE:I = 0x2

.field public static final CITATION:I = 0xa

.field public static final CJ:I = 0x7

.field public static final COMPANY:I = 0x3

.field public static final DOUBLE_BRACE_STATE:I = 0x10

.field public static final DOUBLE_EQUALS_STATE:I = 0xe

.field public static final EMAIL:I = 0x4

.field public static final EXTERNAL_LINK:I = 0x9

.field public static final EXTERNAL_LINK_STATE:I = 0x6

.field public static final EXTERNAL_LINK_URL:I = 0x11

.field public static final FIVE_SINGLE_QUOTES_STATE:I = 0xc

.field public static final HEADING:I = 0xf

.field public static final HOST:I = 0x5

.field public static final INTERNAL_LINK:I = 0x8

.field public static final INTERNAL_LINK_STATE:I = 0x4

.field public static final ITALICS:I = 0xd

.field public static final NUM:I = 0x6

.field public static final STRING:I = 0x12

.field public static final SUB_HEADING:I = 0x10

.field public static final THREE_SINGLE_QUOTES_STATE:I = 0xa

.field public static final TOKEN_TYPES:[Ljava/lang/String;

.field public static final TWO_SINGLE_QUOTES_STATE:I = 0x8

.field public static final YYEOF:I = -0x1

.field public static final YYINITIAL:I = 0x0

.field private static final ZZ_ACTION:[I

.field private static final ZZ_ACTION_PACKED_0:Ljava/lang/String; = "\n\u0000\u0004\u0001\u0004\u0002\u0001\u0003\u0001\u0001\u0001\u0004\u0001\u0001\u0002\u0005\u0001\u0006\u0002\u0005\u0001\u0007\u0001\u0005\u0002\u0008\u0001\t\u0001\n\u0001\t\u0001\u000b\u0001\u000c\u0001\u0008\u0001\r\u0001\u000e\u0001\r\u0001\u000f\u0001\u0010\u0001\u0008\u0001\u0011\u0001\u0008\u0004\u0012\u0001\u0013\u0001\u0012\u0001\u0014\u0001\u0015\u0001\u0016\u0003\u0000\u0001\u0017\u000c\u0000\u0001\u0018\u0001\u0019\u0001\u001a\u0001\u001b\u0001\t\u0001\u0000\u0001\u001c\u0001\u001d\u0001\u001e\u0001\u0000\u0001\u001f\u0001\u0000\u0001 \u0003\u0000\u0001!\u0001\"\u0002#\u0001\"\u0002$\u0002\u0000\u0001#\u0001\u0000\u000c#\u0001\"\u0003\u0000\u0001\t\u0001%\u0003\u0000\u0001&\u0001\'\u0005\u0000\u0001(\u0004\u0000\u0001(\u0002\u0000\u0002(\u0002\u0000\u0001\t\u0005\u0000\u0001\u0019\u0001\"\u0001#\u0001)\u0003\u0000\u0001\t\u0002\u0000\u0001*\u0018\u0000\u0001+\u0002\u0000\u0001,\u0001-\u0001."

.field private static final ZZ_ATTRIBUTE:[I

.field private static final ZZ_ATTRIBUTE_PACKED_0:Ljava/lang/String; = "\n\u0000\u0001\t\u0007\u0001\u0001\t\u0003\u0001\u0001\t\u0006\u0001\u0001\t\u0002\u0001\u0001\t\u000c\u0001\u0001\t\u0006\u0001\u0002\t\u0003\u0000\u0001\t\u000c\u0000\u0002\u0001\u0002\t\u0001\u0001\u0001\u0000\u0002\u0001\u0001\t\u0001\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0003\u0000\u0007\u0001\u0002\u0000\u0001\u0001\u0001\u0000\r\u0001\u0003\u0000\u0001\u0001\u0001\t\u0003\u0000\u0001\u0001\u0001\t\u0005\u0000\u0001\u0001\u0004\u0000\u0001\u0001\u0002\u0000\u0002\u0001\u0002\u0000\u0001\u0001\u0005\u0000\u0001\t\u0003\u0001\u0003\u0000\u0001\u0001\u0002\u0000\u0001\t\u0018\u0000\u0001\u0001\u0002\u0000\u0003\t"

.field private static final ZZ_BUFFERSIZE:I = 0x1000

.field private static final ZZ_CMAP:[C

.field private static final ZZ_CMAP_PACKED:Ljava/lang/String; = "\t\u0000\u0001\u0014\u0001\u0013\u0001\u0000\u0001\u0014\u0001\u0012\u0012\u0000\u0001\u0014\u0001\u0000\u0001\n\u0001+\u0002\u0000\u0001\u0003\u0001\u0001\u0004\u0000\u0001\u000c\u0001\u0005\u0001\u0002\u0001\u0008\n\u000e\u0001\u0017\u0001\u0000\u0001\u0007\u0001\t\u0001\u000b\u0001+\u0001\u0004\u0002\r\u0001\u0018\u0005\r\u0001!\u0011\r\u0001\u0015\u0001\u0000\u0001\u0016\u0001\u0000\u0001\u0006\u0001\u0000\u0001\u0019\u0001#\u0002\r\u0001\u001b\u0001 \u0001\u001c\u0001(\u0001!\u0004\r\u0001\"\u0001\u001d\u0001)\u0001\r\u0001\u001e\u0001*\u0001\u001a\u0003\r\u0001$\u0001\u001f\u0001\r\u0001%\u0001\'\u0001&B\u0000\u0017\r\u0001\u0000\u001f\r\u0001\u0000\u0568\r\n\u000f\u0086\r\n\u000f\u026c\r\n\u000fv\r\n\u000fv\r\n\u000fv\r\n\u000fv\r\n\u000fw\r\t\u000fv\r\n\u000fv\r\n\u000fv\r\n\u000f\u00e0\r\n\u000fv\r\n\u000f\u0166\r\n\u000f\u00b6\r\u0100\r\u0e00\r\u1040\u0000\u0150\u0011`\u0000\u0010\u0011\u0100\u0000\u0080\u0011\u0080\u0000\u19c0\u0011@\u0000\u5200\u0011\u0c00\u0000\u2bb0\u0010\u2150\u0000\u0200\u0011\u0465\u0000;\u0011=\r#\u0000"

.field private static final ZZ_ERROR_MSG:[Ljava/lang/String;

.field private static final ZZ_LEXSTATE:[I

.field private static final ZZ_NO_MATCH:I = 0x1

.field private static final ZZ_PUSHBACK_2BIG:I = 0x2

.field private static final ZZ_ROWMAP:[I

.field private static final ZZ_ROWMAP_PACKED_0:Ljava/lang/String; = "\u0000\u0000\u0000,\u0000X\u0000\u0084\u0000\u00b0\u0000\u00dc\u0000\u0108\u0000\u0134\u0000\u0160\u0000\u018c\u0000\u01b8\u0000\u01e4\u0000\u0210\u0000\u023c\u0000\u0268\u0000\u0294\u0000\u02c0\u0000\u02ec\u0000\u01b8\u0000\u0318\u0000\u0344\u0000\u0370\u0000\u01b8\u0000\u039c\u0000\u03c8\u0000\u03f4\u0000\u0420\u0000\u044c\u0000\u0478\u0000\u01b8\u0000\u039c\u0000\u04a4\u0000\u01b8\u0000\u04d0\u0000\u04fc\u0000\u0528\u0000\u0554\u0000\u0580\u0000\u05ac\u0000\u05d8\u0000\u0604\u0000\u0630\u0000\u065c\u0000\u0688\u0000\u06b4\u0000\u01b8\u0000\u06e0\u0000\u039c\u0000\u070c\u0000\u0738\u0000\u0764\u0000\u0790\u0000\u01b8\u0000\u01b8\u0000\u07bc\u0000\u07e8\u0000\u0814\u0000\u01b8\u0000\u0840\u0000\u086c\u0000\u0898\u0000\u08c4\u0000\u08f0\u0000\u091c\u0000\u0948\u0000\u0974\u0000\u09a0\u0000\u09cc\u0000\u09f8\u0000\u0a24\u0000\u0a50\u0000\u0a7c\u0000\u01b8\u0000\u01b8\u0000\u0aa8\u0000\u0ad4\u0000\u0b00\u0000\u0b00\u0000\u01b8\u0000\u0b2c\u0000\u0b58\u0000\u0b84\u0000\u0bb0\u0000\u0bdc\u0000\u0c08\u0000\u0c34\u0000\u0c60\u0000\u0c8c\u0000\u0cb8\u0000\u0ce4\u0000\u0d10\u0000\u0898\u0000\u0d3c\u0000\u0d68\u0000\u0d94\u0000\u0dc0\u0000\u0dec\u0000\u0e18\u0000\u0e44\u0000\u0e70\u0000\u0e9c\u0000\u0ec8\u0000\u0ef4\u0000\u0f20\u0000\u0f4c\u0000\u0f78\u0000\u0fa4\u0000\u0fd0\u0000\u0ffc\u0000\u1028\u0000\u1054\u0000\u1080\u0000\u10ac\u0000\u10d8\u0000\u01b8\u0000\u1104\u0000\u1130\u0000\u115c\u0000\u1188\u0000\u01b8\u0000\u11b4\u0000\u11e0\u0000\u120c\u0000\u1238\u0000\u1264\u0000\u1290\u0000\u12bc\u0000\u12e8\u0000\u1314\u0000\u1340\u0000\u136c\u0000\u1398\u0000\u13c4\u0000\u086c\u0000\u09f8\u0000\u13f0\u0000\u141c\u0000\u1448\u0000\u1474\u0000\u14a0\u0000\u14cc\u0000\u14f8\u0000\u1524\u0000\u01b8\u0000\u1550\u0000\u157c\u0000\u15a8\u0000\u15d4\u0000\u1600\u0000\u162c\u0000\u1658\u0000\u1684\u0000\u16b0\u0000\u01b8\u0000\u16dc\u0000\u1708\u0000\u1734\u0000\u1760\u0000\u178c\u0000\u17b8\u0000\u17e4\u0000\u1810\u0000\u183c\u0000\u1868\u0000\u1894\u0000\u18c0\u0000\u18ec\u0000\u1918\u0000\u1944\u0000\u1970\u0000\u199c\u0000\u19c8\u0000\u19f4\u0000\u1a20\u0000\u1a4c\u0000\u1a78\u0000\u1aa4\u0000\u1ad0\u0000\u1afc\u0000\u1b28\u0000\u1b54\u0000\u01b8\u0000\u01b8\u0000\u01b8"

.field private static final ZZ_TRANS:[I

.field private static final ZZ_TRANS_PACKED_0:Ljava/lang/String; = "\u0001\u000b\u0001\u000c\u0005\u000b\u0001\r\u0001\u000b\u0001\u000e\u0003\u000b\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0001\u0013\u0001\u0014\u0002\u000b\u0001\u0015\u0002\u000b\r\u000f\u0001\u0016\u0002\u000b\u0003\u000f\u0001\u000b\u0007\u0017\u0001\u0018\u0005\u0017\u0004\u0019\u0001\u0017\u0001\u001a\u0003\u0017\u0001\u001b\u0001\u0017\r\u0019\u0003\u0017\u0003\u0019\u0008\u0017\u0001\u0018\u0005\u0017\u0004\u001c\u0001\u0017\u0001\u001a\u0003\u0017\u0001\u001d\u0001\u0017\r\u001c\u0003\u0017\u0003\u001c\u0001\u0017\u0007\u001e\u0001\u001f\u0005\u001e\u0004 \u0001\u001e\u0001\u001a\u0002\u0017\u0001\u001e\u0001!\u0001\u001e\r \u0003\u001e\u0001\"\u0002 \u0002\u001e\u0001#\u0005\u001e\u0001\u001f\u0005\u001e\u0004$\u0001\u001e\u0001%\u0002\u001e\u0001&\u0002\u001e\r$\u0003\u001e\u0003$\u0008\u001e\u0001\u001f\u0005\u001e\u0004\'\u0001\u001e\u0001%\u0002\u001e\u0001&\u0002\u001e\r\'\u0003\u001e\u0003\'\u0008\u001e\u0001\u001f\u0005\u001e\u0004\'\u0001\u001e\u0001%\u0002\u001e\u0001(\u0002\u001e\r\'\u0003\u001e\u0003\'\u0008\u001e\u0001\u001f\u0001\u001e\u0001)\u0003\u001e\u0004*\u0001\u001e\u0001%\u0005\u001e\r*\u0003\u001e\u0003*\u0008\u001e\u0001+\u0005\u001e\u0004,\u0001\u001e\u0001%\u0005\u001e\r,\u0001\u001e\u0001-\u0001\u001e\u0003,\u0001\u001e\u0001.\u0001/\u0005.\u00010\u0001.\u00011\u0003.\u00042\u0001.\u00013\u0002.\u00014\u0002.\r2\u0002.\u00015\u00032\u0001.-\u0000\u000162\u0000\u00017\u0004\u0000\u00048\u0007\u0000\u00068\u00019\u00068\u0003\u0000\u00038\n\u0000\u0001:#\u0000\u0001;\u0001<\u0001=\u0001>\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0007\u0000\r\u000f\u0003\u0000\u0003\u000f\u0003\u0000\u0001A\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003\u0010\u0001\u0012\u0007\u0000\r\u0010\u0003\u0000\u0003\u0010\u0002\u0000\u0001;\u0001E\u0001=\u0001>\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0001\u0011\u0001\u0010\u0001\u0011\u0001\u0012\u0007\u0000\r\u0011\u0003\u0000\u0003\u0011\u0003\u0000\u0001F\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004\u0012\u0007\u0000\r\u0012\u0003\u0000\u0003\u0012\u0014\u0000\u0001\u000b-\u0000\u0001G;\u0000\u0001H\u000e\u0000\u00017\u0004\u0000\u00048\u0007\u0000\r8\u0003\u0000\u00038\u000e\u0000\u0004\u0019\u0007\u0000\r\u0019\u0003\u0000\u0003\u0019\u0014\u0000\u0001\u0017.\u0000\u0001I\"\u0000\u0004\u001c\u0007\u0000\r\u001c\u0003\u0000\u0003\u001c\u0017\u0000\u0001J\"\u0000\u0004 \u0007\u0000\r \u0003\u0000\u0003 \u000e\u0000\u0004 \u0007\u0000\u0002 \u0001K\n \u0003\u0000\u0003 \u0002\u0000\u0001L7\u0000\u0004$\u0007\u0000\r$\u0003\u0000\u0003$\u0014\u0000\u0001\u001e-\u0000\u0001M#\u0000\u0004\'\u0007\u0000\r\'\u0003\u0000\u0003\'\u0016\u0000\u0001N\u001f\u0000\u0001O/\u0000\u0004*\u0007\u0000\r*\u0003\u0000\u0003*\t\u0000\u0001P\u0004\u0000\u00048\u0007\u0000\r8\u0003\u0000\u00038\u000e\u0000\u0004,\u0007\u0000\r,\u0003\u0000\u0003,\'\u0000\u0001O\u0006\u0000\u0001Q3\u0000\u0001R/\u0000\u00042\u0007\u0000\r2\u0003\u0000\u00032\u0014\u0000\u0001.-\u0000\u0001S#\u0000\u00048\u0007\u0000\r8\u0003\u0000\u00038\u000c\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u000c\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0003T\u0001V\tT\u0003\u0000\u0003T\u000e\u0000\u0001W\u0001\u0000\u0001W\u0008\u0000\rW\u0003\u0000\u0003W\u000e\u0000\u0001X\u0001Y\u0001Z\u0001[\u0007\u0000\rX\u0003\u0000\u0003X\u000e\u0000\u0001\\\u0001\u0000\u0001\\\u0008\u0000\r\\\u0003\u0000\u0003\\\u000e\u0000\u0001]\u0001^\u0001]\u0001^\u0007\u0000\r]\u0003\u0000\u0003]\u000e\u0000\u0001_\u0002`\u0001a\u0007\u0000\r_\u0003\u0000\u0003_\u000e\u0000\u0001@\u0002b\u0008\u0000\r@\u0003\u0000\u0003@\u000e\u0000\u0001c\u0002d\u0001e\u0007\u0000\rc\u0003\u0000\u0003c\u000e\u0000\u0004^\u0007\u0000\r^\u0003\u0000\u0003^\u000e\u0000\u0001f\u0002g\u0001h\u0007\u0000\rf\u0003\u0000\u0003f\u000e\u0000\u0001i\u0002j\u0001k\u0007\u0000\ri\u0003\u0000\u0003i\u000e\u0000\u0001l\u0001d\u0001m\u0001e\u0007\u0000\rl\u0003\u0000\u0003l\u000e\u0000\u0001n\u0002Y\u0001[\u0007\u0000\rn\u0003\u0000\u0003n\u0018\u0000\u0001o\u0001p4\u0000\u0001q\u0017\u0000\u0004 \u0007\u0000\u0002 \u0001r\n \u0003\u0000\u0003 \u0002\u0000\u0001sA\u0000\u0001t\u0001u \u0000\u00048\u0007\u0000\u00068\u0001v\u00068\u0003\u0000\u00038\u0002\u0000\u0001w3\u0000\u0001x9\u0000\u0001y\u0001z\u001c\u0000\u0001{\u0001\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u000e\u0000\u0004|\u0001\u0000\u0003U\u0003\u0000\r|\u0003\u0000\u0003|\n\u0000\u0001{\u0001\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0008T\u0001}\u0004T\u0003\u0000\u0003T\u0002\u0000\u0001;\u000b\u0000\u0001W\u0001\u0000\u0001W\u0008\u0000\rW\u0003\u0000\u0003W\u0003\u0000\u0001~\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001X\u0001Y\u0001Z\u0001[\u0007\u0000\rX\u0003\u0000\u0003X\u0003\u0000\u0001\u0080\u0001\u0000\u0001B\u0002\u0081\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0003Y\u0001[\u0007\u0000\rY\u0003\u0000\u0003Y\u0003\u0000\u0001\u0083\u0001\u0000\u0001B\u0002\u0081\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0001Z\u0001Y\u0001Z\u0001[\u0007\u0000\rZ\u0003\u0000\u0003Z\u0003\u0000\u0001\u0084\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0004[\u0007\u0000\r[\u0003\u0000\u0003[\u0003\u0000\u0001\u0085\u0002\u0000\u0001\u0085\u0007\u0000\u0001]\u0001^\u0001]\u0001^\u0007\u0000\r]\u0003\u0000\u0003]\u0003\u0000\u0001\u0085\u0002\u0000\u0001\u0085\u0007\u0000\u0004^\u0007\u0000\r^\u0003\u0000\u0003^\u0003\u0000\u0001\u007f\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001_\u0002`\u0001a\u0007\u0000\r_\u0003\u0000\u0003_\u0003\u0000\u0001\u0081\u0001\u0000\u0001B\u0002\u0081\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0003`\u0001a\u0007\u0000\r`\u0003\u0000\u0003`\u0003\u0000\u0001\u007f\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0004a\u0007\u0000\ra\u0003\u0000\u0003a\u0003\u0000\u0001\u0082\u0002\u0000\u0002\u0082\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0003b\u0008\u0000\rb\u0003\u0000\u0003b\u0003\u0000\u0001F\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001c\u0002d\u0001e\u0007\u0000\rc\u0003\u0000\u0003c\u0003\u0000\u0001A\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003d\u0001e\u0007\u0000\rd\u0003\u0000\u0003d\u0003\u0000\u0001F\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004e\u0007\u0000\re\u0003\u0000\u0003e\u0003\u0000\u0001?\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001f\u0002g\u0001h\u0007\u0000\rf\u0003\u0000\u0003f\u0003\u0000\u0001C\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003g\u0001h\u0007\u0000\rg\u0003\u0000\u0003g\u0003\u0000\u0001?\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004h\u0007\u0000\rh\u0003\u0000\u0003h\u0003\u0000\u0001@\u0002\u0000\u0002@\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001i\u0002j\u0001k\u0007\u0000\ri\u0003\u0000\u0003i\u0003\u0000\u0001D\u0002\u0000\u0002D\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003j\u0001k\u0007\u0000\rj\u0003\u0000\u0003j\u0003\u0000\u0001@\u0002\u0000\u0002@\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004k\u0007\u0000\rk\u0003\u0000\u0003k\u0003\u0000\u0001\u0086\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001l\u0001d\u0001m\u0001e\u0007\u0000\rl\u0003\u0000\u0003l\u0003\u0000\u0001\u0087\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0001m\u0001d\u0001m\u0001e\u0007\u0000\rm\u0003\u0000\u0003m\u0003\u0000\u0001\u0084\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001n\u0002Y\u0001[\u0007\u0000\rn\u0003\u0000\u0003n\u0019\u0000\u0001p,\u0000\u0001\u00884\u0000\u0001\u0089\u0016\u0000\u0004 \u0007\u0000\r \u0003\u0000\u0001 \u0001\u008a\u0001 \u0019\u0000\u0001u,\u0000\u0001\u008b\u001d\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0003T\u0001\u008c\tT\u0003\u0000\u0003T\u0002\u0000\u0001\u008dB\u0000\u0001z,\u0000\u0001\u008e\u001c\u0000\u0001\u008f*\u0000\u0001{\u0003\u0000\u0004|\u0007\u0000\r|\u0003\u0000\u0003|\n\u0000\u0001{\u0001\u0000\u0001\u0090\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u000e\u0000\u0001\u0091\u0001[\u0001\u0091\u0001[\u0007\u0000\r\u0091\u0003\u0000\u0003\u0091\u000e\u0000\u0004a\u0007\u0000\ra\u0003\u0000\u0003a\u000e\u0000\u0004e\u0007\u0000\re\u0003\u0000\u0003e\u000e\u0000\u0004h\u0007\u0000\rh\u0003\u0000\u0003h\u000e\u0000\u0004k\u0007\u0000\rk\u0003\u0000\u0003k\u000e\u0000\u0001\u0092\u0001e\u0001\u0092\u0001e\u0007\u0000\r\u0092\u0003\u0000\u0003\u0092\u000e\u0000\u0004[\u0007\u0000\r[\u0003\u0000\u0003[\u000e\u0000\u0004\u0093\u0007\u0000\r\u0093\u0003\u0000\u0003\u0093\u001b\u0000\u0001\u00941\u0000\u0001\u0095\u0018\u0000\u0004 \u0006\u0000\u0001\u0096\r \u0003\u0000\u0002 \u0001\u0097\u001b\u0000\u0001\u0098\u001a\u0000\u0001{\u0001\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0008T\u0001\u0099\u0004T\u0003\u0000\u0003T\u0002\u0000\u0001\u009aD\u0000\u0001\u009b\u001e\u0000\u0004\u009c\u0007\u0000\r\u009c\u0003\u0000\u0003\u009c\u0003\u0000\u0001~\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001\u0091\u0001[\u0001\u0091\u0001[\u0007\u0000\r\u0091\u0003\u0000\u0003\u0091\u0003\u0000\u0001\u0086\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001\u0092\u0001e\u0001\u0092\u0001e\u0007\u0000\r\u0092\u0003\u0000\u0003\u0092\u0003\u0000\u0001\u0085\u0002\u0000\u0001\u0085\u0007\u0000\u0004\u0093\u0007\u0000\r\u0093\u0003\u0000\u0003\u0093\u001c\u0000\u0001\u009d-\u0000\u0001\u009e\u0016\u0000\u0001\u009f0\u0000\u0004 \u0006\u0000\u0001\u0096\r \u0003\u0000\u0003 \u001c\u0000\u0001\u00a0\u0019\u0000\u0001{\u0001\u0000\u0001O\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u001c\u0000\u0001\u00a1\u001a\u0000\u0001\u00a2\u0002\u0000\u0004\u009c\u0007\u0000\r\u009c\u0003\u0000\u0003\u009c\u001d\u0000\u0001\u00a32\u0000\u0001\u00a4\u0010\u0000\u0001\u00a5?\u0000\u0001\u00a6+\u0000\u0001\u00a7\u001a\u0000\u0001\u001e\u0001\u0000\u0004|\u0001\u0000\u0003U\u0003\u0000\r|\u0003\u0000\u0003|\u001e\u0000\u0001\u00a8+\u0000\u0001\u00a9\u001b\u0000\u0004\u00aa\u0007\u0000\r\u00aa\u0003\u0000\u0003\u00aa\u001e\u0000\u0001\u00ab+\u0000\u0001\u00ac,\u0000\u0001\u00ad1\u0000\u0001\u00ae\t\u0000\u0001\u00af\n\u0000\u0004\u00aa\u0007\u0000\r\u00aa\u0003\u0000\u0003\u00aa\u001f\u0000\u0001\u00b0+\u0000\u0001\u00b1,\u0000\u0001\u00b2\u0012\u0000\u0001\u000b2\u0000\u0004\u00b3\u0007\u0000\r\u00b3\u0003\u0000\u0003\u00b3 \u0000\u0001\u00b4+\u0000\u0001\u00b5#\u0000\u0001\u00b6\u0016\u0000\u0002\u00b3\u0001\u0000\u0002\u00b3\u0001\u0000\u0002\u00b3\u0002\u0000\u0005\u00b3\u0007\u0000\r\u00b3\u0003\u0000\u0004\u00b3\u0017\u0000\u0001\u00b7+\u0000\u0001\u00b8\u0014\u0000"

.field private static final ZZ_UNKNOWN_ERROR:I


# instance fields
.field private currentTokType:I

.field private numBalanced:I

.field private numLinkToks:I

.field private numWikiTokensSeen:I

.field private positionInc:I

.field private yychar:I

.field private yycolumn:I

.field private yyline:I

.field private zzAtBOL:Z

.field private zzAtEOF:Z

.field private zzBuffer:[C

.field private zzCurrentPos:I

.field private zzEOFDone:Z

.field private zzEndRead:I

.field private zzLexicalState:I

.field private zzMarkedPos:I

.field private zzReader:Ljava/io/Reader;

.field private zzStartRead:I

.field private zzState:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v2, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 57
    const/16 v0, 0x14

    new-array v0, v0, [I

    .line 58
    aput v5, v0, v3

    aput v5, v0, v4

    aput v3, v0, v2

    aput v3, v0, v6

    const/4 v1, 0x6

    aput v4, v0, v1

    const/4 v1, 0x7

    aput v4, v0, v1

    const/16 v1, 0x8

    aput v2, v0, v1

    const/16 v1, 0x9

    aput v2, v0, v1

    const/16 v1, 0xa

    aput v6, v0, v1

    const/16 v1, 0xb

    aput v6, v0, v1

    const/16 v1, 0xc

    const/4 v2, 0x6

    aput v2, v0, v1

    const/16 v1, 0xd

    const/4 v2, 0x6

    aput v2, v0, v1

    const/16 v1, 0xe

    const/4 v2, 0x7

    aput v2, v0, v1

    const/16 v1, 0xf

    const/4 v2, 0x7

    aput v2, v0, v1

    const/16 v1, 0x10

    .line 59
    const/16 v2, 0x8

    aput v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x8

    aput v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x9

    aput v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x9

    aput v2, v0, v1

    .line 57
    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_LEXSTATE:[I

    .line 82
    const-string v0, "\t\u0000\u0001\u0014\u0001\u0013\u0001\u0000\u0001\u0014\u0001\u0012\u0012\u0000\u0001\u0014\u0001\u0000\u0001\n\u0001+\u0002\u0000\u0001\u0003\u0001\u0001\u0004\u0000\u0001\u000c\u0001\u0005\u0001\u0002\u0001\u0008\n\u000e\u0001\u0017\u0001\u0000\u0001\u0007\u0001\t\u0001\u000b\u0001+\u0001\u0004\u0002\r\u0001\u0018\u0005\r\u0001!\u0011\r\u0001\u0015\u0001\u0000\u0001\u0016\u0001\u0000\u0001\u0006\u0001\u0000\u0001\u0019\u0001#\u0002\r\u0001\u001b\u0001 \u0001\u001c\u0001(\u0001!\u0004\r\u0001\"\u0001\u001d\u0001)\u0001\r\u0001\u001e\u0001*\u0001\u001a\u0003\r\u0001$\u0001\u001f\u0001\r\u0001%\u0001\'\u0001&B\u0000\u0017\r\u0001\u0000\u001f\r\u0001\u0000\u0568\r\n\u000f\u0086\r\n\u000f\u026c\r\n\u000fv\r\n\u000fv\r\n\u000fv\r\n\u000fv\r\n\u000fw\r\t\u000fv\r\n\u000fv\r\n\u000fv\r\n\u000f\u00e0\r\n\u000fv\r\n\u000f\u0166\r\n\u000f\u00b6\r\u0100\r\u0e00\r\u1040\u0000\u0150\u0011`\u0000\u0010\u0011\u0100\u0000\u0080\u0011\u0080\u0000\u19c0\u0011@\u0000\u5200\u0011\u0c00\u0000\u2bb0\u0010\u2150\u0000\u0200\u0011\u0465\u0000;\u0011=\r#\u0000"

    invoke-static {v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackCMap(Ljava/lang/String;)[C

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_CMAP:[C

    .line 87
    invoke-static {}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackAction()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ACTION:[I

    .line 126
    invoke-static {}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackRowMap()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ROWMAP:[I

    .line 174
    invoke-static {}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackTrans()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_TRANS:[I

    .line 349
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 350
    const-string v2, "Unkown internal scanner error"

    aput-object v2, v0, v1

    .line 351
    const-string v1, "Error: could not match input"

    aput-object v1, v0, v5

    .line 352
    const-string v1, "Error: pushback value was too large"

    aput-object v1, v0, v3

    .line 349
    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ERROR_MSG:[Ljava/lang/String;

    .line 358
    invoke-static {}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackAttribute()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ATTRIBUTE:[I

    .line 469
    sget-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->TOKEN_TYPES:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/io/Reader;)V
    .locals 3
    .param p1, "in"    # Ljava/io/Reader;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzLexicalState:I

    .line 400
    const/16 v0, 0x1000

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    .line 430
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzAtBOL:Z

    .line 461
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    .line 462
    iput v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    .line 463
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    .line 467
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    .line 519
    iput-object p1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzReader:Ljava/io/Reader;

    .line 520
    return-void
.end method

.method private zzRefill()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 553
    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    if-lez v5, :cond_0

    .line 554
    iget-object v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    .line 555
    iget-object v7, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    .line 556
    iget v8, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    iget v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v8, v9

    .line 554
    invoke-static {v5, v6, v7, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 559
    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 560
    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    .line 561
    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    .line 562
    iput v3, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    .line 566
    :cond_0
    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    array-length v6, v6

    if-lt v5, v6, :cond_1

    .line 568
    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    mul-int/lit8 v5, v5, 0x2

    new-array v1, v5, [C

    .line 569
    .local v1, "newBuffer":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    array-length v6, v6

    invoke-static {v5, v3, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 570
    iput-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    .line 574
    .end local v1    # "newBuffer":[C
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzReader:Ljava/io/Reader;

    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v7, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 575
    iget-object v8, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    array-length v8, v8

    iget v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    sub-int/2addr v8, v9

    .line 574
    invoke-virtual {v5, v6, v7, v8}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 577
    .local v2, "numRead":I
    if-lez v2, :cond_2

    .line 578
    iget v4, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 593
    :goto_0
    return v3

    .line 582
    :cond_2
    if-nez v2, :cond_4

    .line 583
    iget-object v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzReader:Ljava/io/Reader;

    invoke-virtual {v5}, Ljava/io/Reader;->read()I

    move-result v0

    .line 584
    .local v0, "c":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_3

    move v3, v4

    .line 585
    goto :goto_0

    .line 587
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    int-to-char v6, v0

    aput-char v6, v4, v5

    goto :goto_0

    .end local v0    # "c":I
    :cond_4
    move v3, v4

    .line 593
    goto :goto_0
.end method

.method private zzScanError(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 702
    :try_start_0
    sget-object v2, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ERROR_MSG:[Ljava/lang/String;

    aget-object v1, v2, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 708
    .local v1, "message":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/Error;

    invoke-direct {v2, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v2

    .line 704
    .end local v1    # "message":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 705
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v2, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ERROR_MSG:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v1, v2, v3

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0
.end method

.method private static zzUnpackAction(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 111
    const/4 v1, 0x0

    .line 112
    .local v1, "i":I
    move v3, p1

    .line 113
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v2, v1

    .line 114
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 119
    return v3

    .line 115
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 116
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 117
    .local v6, "value":I
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackAction()[I
    .locals 3

    .prologue
    .line 104
    const/16 v2, 0xb8

    new-array v1, v2, [I

    .line 105
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 106
    .local v0, "offset":I
    const-string v2, "\n\u0000\u0004\u0001\u0004\u0002\u0001\u0003\u0001\u0001\u0001\u0004\u0001\u0001\u0002\u0005\u0001\u0006\u0002\u0005\u0001\u0007\u0001\u0005\u0002\u0008\u0001\t\u0001\n\u0001\t\u0001\u000b\u0001\u000c\u0001\u0008\u0001\r\u0001\u000e\u0001\r\u0001\u000f\u0001\u0010\u0001\u0008\u0001\u0011\u0001\u0008\u0004\u0012\u0001\u0013\u0001\u0012\u0001\u0014\u0001\u0015\u0001\u0016\u0003\u0000\u0001\u0017\u000c\u0000\u0001\u0018\u0001\u0019\u0001\u001a\u0001\u001b\u0001\t\u0001\u0000\u0001\u001c\u0001\u001d\u0001\u001e\u0001\u0000\u0001\u001f\u0001\u0000\u0001 \u0003\u0000\u0001!\u0001\"\u0002#\u0001\"\u0002$\u0002\u0000\u0001#\u0001\u0000\u000c#\u0001\"\u0003\u0000\u0001\t\u0001%\u0003\u0000\u0001&\u0001\'\u0005\u0000\u0001(\u0004\u0000\u0001(\u0002\u0000\u0002(\u0002\u0000\u0001\t\u0005\u0000\u0001\u0019\u0001\"\u0001#\u0001)\u0003\u0000\u0001\t\u0002\u0000\u0001*\u0018\u0000\u0001+\u0002\u0000\u0001,\u0001-\u0001."

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackAction(Ljava/lang/String;I[I)I

    move-result v0

    .line 107
    return-object v1
.end method

.method private static zzUnpackAttribute(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 378
    const/4 v1, 0x0

    .line 379
    .local v1, "i":I
    move v3, p1

    .line 380
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v2, v1

    .line 381
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 386
    return v3

    .line 382
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 383
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 384
    .local v6, "value":I
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackAttribute()[I
    .locals 3

    .prologue
    .line 371
    const/16 v2, 0xb8

    new-array v1, v2, [I

    .line 372
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 373
    .local v0, "offset":I
    const-string v2, "\n\u0000\u0001\t\u0007\u0001\u0001\t\u0003\u0001\u0001\t\u0006\u0001\u0001\t\u0002\u0001\u0001\t\u000c\u0001\u0001\t\u0006\u0001\u0002\t\u0003\u0000\u0001\t\u000c\u0000\u0002\u0001\u0002\t\u0001\u0001\u0001\u0000\u0002\u0001\u0001\t\u0001\u0000\u0001\u0001\u0001\u0000\u0001\u0001\u0003\u0000\u0007\u0001\u0002\u0000\u0001\u0001\u0001\u0000\r\u0001\u0003\u0000\u0001\u0001\u0001\t\u0003\u0000\u0001\u0001\u0001\t\u0005\u0000\u0001\u0001\u0004\u0000\u0001\u0001\u0002\u0000\u0002\u0001\u0002\u0000\u0001\u0001\u0005\u0000\u0001\t\u0003\u0001\u0003\u0000\u0001\u0001\u0002\u0000\u0001\t\u0018\u0000\u0001\u0001\u0002\u0000\u0003\t"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackAttribute(Ljava/lang/String;I[I)I

    move-result v0

    .line 374
    return-object v1
.end method

.method private static zzUnpackCMap(Ljava/lang/String;)[C
    .locals 8
    .param p0, "packed"    # Ljava/lang/String;

    .prologue
    .line 531
    const/high16 v7, 0x10000

    new-array v5, v7, [C

    .line 532
    .local v5, "map":[C
    const/4 v1, 0x0

    .line 533
    .local v1, "i":I
    const/4 v3, 0x0

    .local v3, "j":I
    move v2, v1

    .line 534
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    const/16 v7, 0xe6

    if-lt v2, v7, :cond_0

    .line 539
    return-object v5

    .line 535
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 536
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 537
    .local v6, "value":C
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput-char v6, v5, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackRowMap(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 161
    const/4 v1, 0x0

    .line 162
    .local v1, "i":I
    move v3, p1

    .line 163
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v4, v3

    .end local v3    # "j":I
    .local v4, "j":I
    move v2, v1

    .line 164
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 168
    return v4

    .line 165
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    shl-int/lit8 v0, v6, 0x10

    .line 166
    .local v0, "high":I
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "j":I
    .restart local v3    # "j":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    or-int/2addr v6, v0

    aput v6, p2, v4

    move v4, v3

    .end local v3    # "j":I
    .restart local v4    # "j":I
    goto :goto_0
.end method

.method private static zzUnpackRowMap()[I
    .locals 3

    .prologue
    .line 154
    const/16 v2, 0xb8

    new-array v1, v2, [I

    .line 155
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 156
    .local v0, "offset":I
    const-string v2, "\u0000\u0000\u0000,\u0000X\u0000\u0084\u0000\u00b0\u0000\u00dc\u0000\u0108\u0000\u0134\u0000\u0160\u0000\u018c\u0000\u01b8\u0000\u01e4\u0000\u0210\u0000\u023c\u0000\u0268\u0000\u0294\u0000\u02c0\u0000\u02ec\u0000\u01b8\u0000\u0318\u0000\u0344\u0000\u0370\u0000\u01b8\u0000\u039c\u0000\u03c8\u0000\u03f4\u0000\u0420\u0000\u044c\u0000\u0478\u0000\u01b8\u0000\u039c\u0000\u04a4\u0000\u01b8\u0000\u04d0\u0000\u04fc\u0000\u0528\u0000\u0554\u0000\u0580\u0000\u05ac\u0000\u05d8\u0000\u0604\u0000\u0630\u0000\u065c\u0000\u0688\u0000\u06b4\u0000\u01b8\u0000\u06e0\u0000\u039c\u0000\u070c\u0000\u0738\u0000\u0764\u0000\u0790\u0000\u01b8\u0000\u01b8\u0000\u07bc\u0000\u07e8\u0000\u0814\u0000\u01b8\u0000\u0840\u0000\u086c\u0000\u0898\u0000\u08c4\u0000\u08f0\u0000\u091c\u0000\u0948\u0000\u0974\u0000\u09a0\u0000\u09cc\u0000\u09f8\u0000\u0a24\u0000\u0a50\u0000\u0a7c\u0000\u01b8\u0000\u01b8\u0000\u0aa8\u0000\u0ad4\u0000\u0b00\u0000\u0b00\u0000\u01b8\u0000\u0b2c\u0000\u0b58\u0000\u0b84\u0000\u0bb0\u0000\u0bdc\u0000\u0c08\u0000\u0c34\u0000\u0c60\u0000\u0c8c\u0000\u0cb8\u0000\u0ce4\u0000\u0d10\u0000\u0898\u0000\u0d3c\u0000\u0d68\u0000\u0d94\u0000\u0dc0\u0000\u0dec\u0000\u0e18\u0000\u0e44\u0000\u0e70\u0000\u0e9c\u0000\u0ec8\u0000\u0ef4\u0000\u0f20\u0000\u0f4c\u0000\u0f78\u0000\u0fa4\u0000\u0fd0\u0000\u0ffc\u0000\u1028\u0000\u1054\u0000\u1080\u0000\u10ac\u0000\u10d8\u0000\u01b8\u0000\u1104\u0000\u1130\u0000\u115c\u0000\u1188\u0000\u01b8\u0000\u11b4\u0000\u11e0\u0000\u120c\u0000\u1238\u0000\u1264\u0000\u1290\u0000\u12bc\u0000\u12e8\u0000\u1314\u0000\u1340\u0000\u136c\u0000\u1398\u0000\u13c4\u0000\u086c\u0000\u09f8\u0000\u13f0\u0000\u141c\u0000\u1448\u0000\u1474\u0000\u14a0\u0000\u14cc\u0000\u14f8\u0000\u1524\u0000\u01b8\u0000\u1550\u0000\u157c\u0000\u15a8\u0000\u15d4\u0000\u1600\u0000\u162c\u0000\u1658\u0000\u1684\u0000\u16b0\u0000\u01b8\u0000\u16dc\u0000\u1708\u0000\u1734\u0000\u1760\u0000\u178c\u0000\u17b8\u0000\u17e4\u0000\u1810\u0000\u183c\u0000\u1868\u0000\u1894\u0000\u18c0\u0000\u18ec\u0000\u1918\u0000\u1944\u0000\u1970\u0000\u199c\u0000\u19c8\u0000\u19f4\u0000\u1a20\u0000\u1a4c\u0000\u1a78\u0000\u1aa4\u0000\u1ad0\u0000\u1afc\u0000\u1b28\u0000\u1b54\u0000\u01b8\u0000\u01b8\u0000\u01b8"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackRowMap(Ljava/lang/String;I[I)I

    move-result v0

    .line 157
    return-object v1
.end method

.method private static zzUnpackTrans(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 330
    const/4 v1, 0x0

    .line 331
    .local v1, "i":I
    move v3, p1

    .line 332
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v2, v1

    .line 333
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 339
    return v3

    .line 334
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 335
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 336
    .local v6, "value":I
    add-int/lit8 v6, v6, -0x1

    .line 337
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackTrans()[I
    .locals 3

    .prologue
    .line 323
    const/16 v2, 0x1b80

    new-array v1, v2, [I

    .line 324
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 325
    .local v0, "offset":I
    const-string v2, "\u0001\u000b\u0001\u000c\u0005\u000b\u0001\r\u0001\u000b\u0001\u000e\u0003\u000b\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0001\u0013\u0001\u0014\u0002\u000b\u0001\u0015\u0002\u000b\r\u000f\u0001\u0016\u0002\u000b\u0003\u000f\u0001\u000b\u0007\u0017\u0001\u0018\u0005\u0017\u0004\u0019\u0001\u0017\u0001\u001a\u0003\u0017\u0001\u001b\u0001\u0017\r\u0019\u0003\u0017\u0003\u0019\u0008\u0017\u0001\u0018\u0005\u0017\u0004\u001c\u0001\u0017\u0001\u001a\u0003\u0017\u0001\u001d\u0001\u0017\r\u001c\u0003\u0017\u0003\u001c\u0001\u0017\u0007\u001e\u0001\u001f\u0005\u001e\u0004 \u0001\u001e\u0001\u001a\u0002\u0017\u0001\u001e\u0001!\u0001\u001e\r \u0003\u001e\u0001\"\u0002 \u0002\u001e\u0001#\u0005\u001e\u0001\u001f\u0005\u001e\u0004$\u0001\u001e\u0001%\u0002\u001e\u0001&\u0002\u001e\r$\u0003\u001e\u0003$\u0008\u001e\u0001\u001f\u0005\u001e\u0004\'\u0001\u001e\u0001%\u0002\u001e\u0001&\u0002\u001e\r\'\u0003\u001e\u0003\'\u0008\u001e\u0001\u001f\u0005\u001e\u0004\'\u0001\u001e\u0001%\u0002\u001e\u0001(\u0002\u001e\r\'\u0003\u001e\u0003\'\u0008\u001e\u0001\u001f\u0001\u001e\u0001)\u0003\u001e\u0004*\u0001\u001e\u0001%\u0005\u001e\r*\u0003\u001e\u0003*\u0008\u001e\u0001+\u0005\u001e\u0004,\u0001\u001e\u0001%\u0005\u001e\r,\u0001\u001e\u0001-\u0001\u001e\u0003,\u0001\u001e\u0001.\u0001/\u0005.\u00010\u0001.\u00011\u0003.\u00042\u0001.\u00013\u0002.\u00014\u0002.\r2\u0002.\u00015\u00032\u0001.-\u0000\u000162\u0000\u00017\u0004\u0000\u00048\u0007\u0000\u00068\u00019\u00068\u0003\u0000\u00038\n\u0000\u0001:#\u0000\u0001;\u0001<\u0001=\u0001>\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0007\u0000\r\u000f\u0003\u0000\u0003\u000f\u0003\u0000\u0001A\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003\u0010\u0001\u0012\u0007\u0000\r\u0010\u0003\u0000\u0003\u0010\u0002\u0000\u0001;\u0001E\u0001=\u0001>\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0001\u0011\u0001\u0010\u0001\u0011\u0001\u0012\u0007\u0000\r\u0011\u0003\u0000\u0003\u0011\u0003\u0000\u0001F\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004\u0012\u0007\u0000\r\u0012\u0003\u0000\u0003\u0012\u0014\u0000\u0001\u000b-\u0000\u0001G;\u0000\u0001H\u000e\u0000\u00017\u0004\u0000\u00048\u0007\u0000\r8\u0003\u0000\u00038\u000e\u0000\u0004\u0019\u0007\u0000\r\u0019\u0003\u0000\u0003\u0019\u0014\u0000\u0001\u0017.\u0000\u0001I\"\u0000\u0004\u001c\u0007\u0000\r\u001c\u0003\u0000\u0003\u001c\u0017\u0000\u0001J\"\u0000\u0004 \u0007\u0000\r \u0003\u0000\u0003 \u000e\u0000\u0004 \u0007\u0000\u0002 \u0001K\n \u0003\u0000\u0003 \u0002\u0000\u0001L7\u0000\u0004$\u0007\u0000\r$\u0003\u0000\u0003$\u0014\u0000\u0001\u001e-\u0000\u0001M#\u0000\u0004\'\u0007\u0000\r\'\u0003\u0000\u0003\'\u0016\u0000\u0001N\u001f\u0000\u0001O/\u0000\u0004*\u0007\u0000\r*\u0003\u0000\u0003*\t\u0000\u0001P\u0004\u0000\u00048\u0007\u0000\r8\u0003\u0000\u00038\u000e\u0000\u0004,\u0007\u0000\r,\u0003\u0000\u0003,\'\u0000\u0001O\u0006\u0000\u0001Q3\u0000\u0001R/\u0000\u00042\u0007\u0000\r2\u0003\u0000\u00032\u0014\u0000\u0001.-\u0000\u0001S#\u0000\u00048\u0007\u0000\r8\u0003\u0000\u00038\u000c\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u000c\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0003T\u0001V\tT\u0003\u0000\u0003T\u000e\u0000\u0001W\u0001\u0000\u0001W\u0008\u0000\rW\u0003\u0000\u0003W\u000e\u0000\u0001X\u0001Y\u0001Z\u0001[\u0007\u0000\rX\u0003\u0000\u0003X\u000e\u0000\u0001\\\u0001\u0000\u0001\\\u0008\u0000\r\\\u0003\u0000\u0003\\\u000e\u0000\u0001]\u0001^\u0001]\u0001^\u0007\u0000\r]\u0003\u0000\u0003]\u000e\u0000\u0001_\u0002`\u0001a\u0007\u0000\r_\u0003\u0000\u0003_\u000e\u0000\u0001@\u0002b\u0008\u0000\r@\u0003\u0000\u0003@\u000e\u0000\u0001c\u0002d\u0001e\u0007\u0000\rc\u0003\u0000\u0003c\u000e\u0000\u0004^\u0007\u0000\r^\u0003\u0000\u0003^\u000e\u0000\u0001f\u0002g\u0001h\u0007\u0000\rf\u0003\u0000\u0003f\u000e\u0000\u0001i\u0002j\u0001k\u0007\u0000\ri\u0003\u0000\u0003i\u000e\u0000\u0001l\u0001d\u0001m\u0001e\u0007\u0000\rl\u0003\u0000\u0003l\u000e\u0000\u0001n\u0002Y\u0001[\u0007\u0000\rn\u0003\u0000\u0003n\u0018\u0000\u0001o\u0001p4\u0000\u0001q\u0017\u0000\u0004 \u0007\u0000\u0002 \u0001r\n \u0003\u0000\u0003 \u0002\u0000\u0001sA\u0000\u0001t\u0001u \u0000\u00048\u0007\u0000\u00068\u0001v\u00068\u0003\u0000\u00038\u0002\u0000\u0001w3\u0000\u0001x9\u0000\u0001y\u0001z\u001c\u0000\u0001{\u0001\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u000e\u0000\u0004|\u0001\u0000\u0003U\u0003\u0000\r|\u0003\u0000\u0003|\n\u0000\u0001{\u0001\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0008T\u0001}\u0004T\u0003\u0000\u0003T\u0002\u0000\u0001;\u000b\u0000\u0001W\u0001\u0000\u0001W\u0008\u0000\rW\u0003\u0000\u0003W\u0003\u0000\u0001~\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001X\u0001Y\u0001Z\u0001[\u0007\u0000\rX\u0003\u0000\u0003X\u0003\u0000\u0001\u0080\u0001\u0000\u0001B\u0002\u0081\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0003Y\u0001[\u0007\u0000\rY\u0003\u0000\u0003Y\u0003\u0000\u0001\u0083\u0001\u0000\u0001B\u0002\u0081\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0001Z\u0001Y\u0001Z\u0001[\u0007\u0000\rZ\u0003\u0000\u0003Z\u0003\u0000\u0001\u0084\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0004[\u0007\u0000\r[\u0003\u0000\u0003[\u0003\u0000\u0001\u0085\u0002\u0000\u0001\u0085\u0007\u0000\u0001]\u0001^\u0001]\u0001^\u0007\u0000\r]\u0003\u0000\u0003]\u0003\u0000\u0001\u0085\u0002\u0000\u0001\u0085\u0007\u0000\u0004^\u0007\u0000\r^\u0003\u0000\u0003^\u0003\u0000\u0001\u007f\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001_\u0002`\u0001a\u0007\u0000\r_\u0003\u0000\u0003_\u0003\u0000\u0001\u0081\u0001\u0000\u0001B\u0002\u0081\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0003`\u0001a\u0007\u0000\r`\u0003\u0000\u0003`\u0003\u0000\u0001\u007f\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0004a\u0007\u0000\ra\u0003\u0000\u0003a\u0003\u0000\u0001\u0082\u0002\u0000\u0002\u0082\u0001\u0000\u0001\u0082\u0003\u0000\u0001\u0082\u0003b\u0008\u0000\rb\u0003\u0000\u0003b\u0003\u0000\u0001F\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001c\u0002d\u0001e\u0007\u0000\rc\u0003\u0000\u0003c\u0003\u0000\u0001A\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003d\u0001e\u0007\u0000\rd\u0003\u0000\u0003d\u0003\u0000\u0001F\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004e\u0007\u0000\re\u0003\u0000\u0003e\u0003\u0000\u0001?\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001f\u0002g\u0001h\u0007\u0000\rf\u0003\u0000\u0003f\u0003\u0000\u0001C\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003g\u0001h\u0007\u0000\rg\u0003\u0000\u0003g\u0003\u0000\u0001?\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004h\u0007\u0000\rh\u0003\u0000\u0003h\u0003\u0000\u0001@\u0002\u0000\u0002@\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001i\u0002j\u0001k\u0007\u0000\ri\u0003\u0000\u0003i\u0003\u0000\u0001D\u0002\u0000\u0002D\u0001\u0000\u0001D\u0003\u0000\u0001D\u0003j\u0001k\u0007\u0000\rj\u0003\u0000\u0003j\u0003\u0000\u0001@\u0002\u0000\u0002@\u0001\u0000\u0001@\u0003\u0000\u0001@\u0004k\u0007\u0000\rk\u0003\u0000\u0003k\u0003\u0000\u0001\u0086\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001l\u0001d\u0001m\u0001e\u0007\u0000\rl\u0003\u0000\u0003l\u0003\u0000\u0001\u0087\u0001\u0000\u0001B\u0002C\u0001\u0000\u0001D\u0003\u0000\u0001D\u0001m\u0001d\u0001m\u0001e\u0007\u0000\rm\u0003\u0000\u0003m\u0003\u0000\u0001\u0084\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001n\u0002Y\u0001[\u0007\u0000\rn\u0003\u0000\u0003n\u0019\u0000\u0001p,\u0000\u0001\u00884\u0000\u0001\u0089\u0016\u0000\u0004 \u0007\u0000\r \u0003\u0000\u0001 \u0001\u008a\u0001 \u0019\u0000\u0001u,\u0000\u0001\u008b\u001d\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0003T\u0001\u008c\tT\u0003\u0000\u0003T\u0002\u0000\u0001\u008dB\u0000\u0001z,\u0000\u0001\u008e\u001c\u0000\u0001\u008f*\u0000\u0001{\u0003\u0000\u0004|\u0007\u0000\r|\u0003\u0000\u0003|\n\u0000\u0001{\u0001\u0000\u0001\u0090\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u000e\u0000\u0001\u0091\u0001[\u0001\u0091\u0001[\u0007\u0000\r\u0091\u0003\u0000\u0003\u0091\u000e\u0000\u0004a\u0007\u0000\ra\u0003\u0000\u0003a\u000e\u0000\u0004e\u0007\u0000\re\u0003\u0000\u0003e\u000e\u0000\u0004h\u0007\u0000\rh\u0003\u0000\u0003h\u000e\u0000\u0004k\u0007\u0000\rk\u0003\u0000\u0003k\u000e\u0000\u0001\u0092\u0001e\u0001\u0092\u0001e\u0007\u0000\r\u0092\u0003\u0000\u0003\u0092\u000e\u0000\u0004[\u0007\u0000\r[\u0003\u0000\u0003[\u000e\u0000\u0004\u0093\u0007\u0000\r\u0093\u0003\u0000\u0003\u0093\u001b\u0000\u0001\u00941\u0000\u0001\u0095\u0018\u0000\u0004 \u0006\u0000\u0001\u0096\r \u0003\u0000\u0002 \u0001\u0097\u001b\u0000\u0001\u0098\u001a\u0000\u0001{\u0001\u0000\u0001\u001e\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\u0008T\u0001\u0099\u0004T\u0003\u0000\u0003T\u0002\u0000\u0001\u009aD\u0000\u0001\u009b\u001e\u0000\u0004\u009c\u0007\u0000\r\u009c\u0003\u0000\u0003\u009c\u0003\u0000\u0001~\u0001\u0000\u0001B\u0002\u007f\u0006\u0000\u0001\u0091\u0001[\u0001\u0091\u0001[\u0007\u0000\r\u0091\u0003\u0000\u0003\u0091\u0003\u0000\u0001\u0086\u0001\u0000\u0001B\u0002?\u0001\u0000\u0001@\u0003\u0000\u0001@\u0001\u0092\u0001e\u0001\u0092\u0001e\u0007\u0000\r\u0092\u0003\u0000\u0003\u0092\u0003\u0000\u0001\u0085\u0002\u0000\u0001\u0085\u0007\u0000\u0004\u0093\u0007\u0000\r\u0093\u0003\u0000\u0003\u0093\u001c\u0000\u0001\u009d-\u0000\u0001\u009e\u0016\u0000\u0001\u009f0\u0000\u0004 \u0006\u0000\u0001\u0096\r \u0003\u0000\u0003 \u001c\u0000\u0001\u00a0\u0019\u0000\u0001{\u0001\u0000\u0001O\u0001\u0000\u0004T\u0001\u0000\u0003U\u0003\u0000\rT\u0003\u0000\u0003T\u001c\u0000\u0001\u00a1\u001a\u0000\u0001\u00a2\u0002\u0000\u0004\u009c\u0007\u0000\r\u009c\u0003\u0000\u0003\u009c\u001d\u0000\u0001\u00a32\u0000\u0001\u00a4\u0010\u0000\u0001\u00a5?\u0000\u0001\u00a6+\u0000\u0001\u00a7\u001a\u0000\u0001\u001e\u0001\u0000\u0004|\u0001\u0000\u0003U\u0003\u0000\r|\u0003\u0000\u0003|\u001e\u0000\u0001\u00a8+\u0000\u0001\u00a9\u001b\u0000\u0004\u00aa\u0007\u0000\r\u00aa\u0003\u0000\u0003\u00aa\u001e\u0000\u0001\u00ab+\u0000\u0001\u00ac,\u0000\u0001\u00ad1\u0000\u0001\u00ae\t\u0000\u0001\u00af\n\u0000\u0004\u00aa\u0007\u0000\r\u00aa\u0003\u0000\u0003\u00aa\u001f\u0000\u0001\u00b0+\u0000\u0001\u00b1,\u0000\u0001\u00b2\u0012\u0000\u0001\u000b2\u0000\u0004\u00b3\u0007\u0000\r\u00b3\u0003\u0000\u0003\u00b3 \u0000\u0001\u00b4+\u0000\u0001\u00b5#\u0000\u0001\u00b6\u0016\u0000\u0002\u00b3\u0001\u0000\u0002\u00b3\u0001\u0000\u0002\u00b3\u0002\u0000\u0005\u00b3\u0007\u0000\r\u00b3\u0003\u0000\u0004\u00b3\u0017\u0000\u0001\u00b7+\u0000\u0001\u00b8\u0014\u0000"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzUnpackTrans(Ljava/lang/String;I[I)I

    move-result v0

    .line 326
    return-object v1
.end method


# virtual methods
.method public getNextToken()I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 742
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 743
    .local v9, "zzEndReadL":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    .line 744
    .local v5, "zzBufferL":[C
    sget-object v6, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_CMAP:[C

    .line 746
    .local v6, "zzCMapL":[C
    sget-object v14, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_TRANS:[I

    .line 747
    .local v14, "zzTransL":[I
    sget-object v13, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ROWMAP:[I

    .line 748
    .local v13, "zzRowMapL":[I
    sget-object v3, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ATTRIBUTE:[I

    .line 751
    .local v3, "zzAttrL":[I
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    .line 753
    .local v11, "zzMarkedPosL":I
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    move/from16 v16, v0

    sub-int v16, v11, v16

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar:I

    .line 755
    const/4 v2, -0x1

    .line 757
    .local v2, "zzAction":I
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    move v7, v11

    .line 759
    .local v7, "zzCurrentPosL":I
    sget-object v15, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_LEXSTATE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzLexicalState:I

    move/from16 v16, v0

    aget v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    .line 762
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    aget v4, v3, v15

    .line 763
    .local v4, "zzAttributes":I
    and-int/lit8 v15, v4, 0x1

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 764
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    move v8, v7

    .line 771
    .end local v7    # "zzCurrentPosL":I
    .local v8, "zzCurrentPosL":I
    :goto_1
    if-ge v8, v9, :cond_1

    .line 772
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    aget-char v10, v5, v8

    .line 795
    .local v10, "zzInput":I
    :goto_2
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    aget v15, v13, v15

    aget-char v16, v6, v10

    add-int v15, v15, v16

    aget v12, v14, v15

    .line 796
    .local v12, "zzNext":I
    const/4 v15, -0x1

    if-ne v12, v15, :cond_4

    .line 810
    .end local v12    # "zzNext":I
    :cond_0
    :goto_3
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    .line 812
    if-gez v2, :cond_6

    .end local v2    # "zzAction":I
    :goto_4
    packed-switch v2, :pswitch_data_0

    .line 998
    const/4 v15, -0x1

    if-ne v10, v15, :cond_9

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 999
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzAtEOF:Z

    .line 1000
    const/4 v15, -0x1

    :goto_5
    return v15

    .line 773
    .end local v7    # "zzCurrentPosL":I
    .end local v10    # "zzInput":I
    .restart local v2    # "zzAction":I
    .restart local v8    # "zzCurrentPosL":I
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzAtEOF:Z

    if-eqz v15, :cond_2

    .line 774
    const/4 v10, -0x1

    .restart local v10    # "zzInput":I
    move v7, v8

    .line 775
    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    goto :goto_3

    .line 779
    .end local v7    # "zzCurrentPosL":I
    .end local v10    # "zzInput":I
    .restart local v8    # "zzCurrentPosL":I
    :cond_2
    move-object/from16 v0, p0

    iput v8, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    .line 780
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    .line 781
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzRefill()Z

    move-result v1

    .line 783
    .local v1, "eof":Z
    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    .line 784
    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    .line 785
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    .line 786
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 787
    if-eqz v1, :cond_3

    .line 788
    const/4 v10, -0x1

    .line 789
    .restart local v10    # "zzInput":I
    goto :goto_3

    .line 792
    .end local v10    # "zzInput":I
    :cond_3
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "zzCurrentPosL":I
    .restart local v8    # "zzCurrentPosL":I
    aget-char v10, v5, v7

    .restart local v10    # "zzInput":I
    move v7, v8

    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    goto :goto_2

    .line 797
    .end local v1    # "eof":Z
    .restart local v12    # "zzNext":I
    :cond_4
    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    .line 799
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    aget v4, v3, v15

    .line 800
    and-int/lit8 v15, v4, 0x1

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 801
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzState:I

    .line 802
    move v11, v7

    .line 803
    and-int/lit8 v15, v4, 0x8

    const/16 v16, 0x8

    move/from16 v0, v16

    if-eq v15, v0, :cond_0

    .end local v10    # "zzInput":I
    .end local v12    # "zzNext":I
    :cond_5
    move v8, v7

    .end local v7    # "zzCurrentPosL":I
    .restart local v8    # "zzCurrentPosL":I
    goto/16 :goto_1

    .line 812
    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    .restart local v10    # "zzInput":I
    :cond_6
    sget-object v15, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->ZZ_ACTION:[I

    aget v2, v15, v2

    goto :goto_4

    .line 814
    .end local v2    # "zzAction":I
    :pswitch_1
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    goto/16 :goto_0

    .line 818
    :pswitch_2
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x0

    goto :goto_5

    .line 822
    :pswitch_3
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x7

    goto :goto_5

    .line 826
    :pswitch_4
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/16 v15, 0x11

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 830
    :pswitch_5
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    goto/16 :goto_0

    .line 834
    :pswitch_6
    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 838
    :pswitch_7
    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 846
    :pswitch_8
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    if-nez v15, :cond_7

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    :goto_6
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/16 v15, 0x9

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    :cond_7
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    goto :goto_6

    .line 850
    :pswitch_9
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 854
    :pswitch_a
    const/16 v15, 0xc

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/16 v15, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 858
    :pswitch_b
    const/16 v15, 0xd

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/16 v15, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 862
    :pswitch_c
    const/16 v15, 0x9

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 866
    :pswitch_d
    const/16 v15, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 870
    :pswitch_e
    const/16 v15, 0x10

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/16 v15, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 874
    :pswitch_f
    const/16 v15, 0xf

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/16 v15, 0xe

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 878
    :pswitch_10
    const/16 v15, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 886
    :pswitch_11
    const/16 v15, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 890
    :pswitch_12
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/16 v15, 0x9

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 894
    :pswitch_13
    const/16 v15, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 898
    :pswitch_14
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    if-nez v15, :cond_8

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/16 v15, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    :cond_8
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    goto/16 :goto_0

    .line 902
    :pswitch_15
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/16 v15, 0xe

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 906
    :pswitch_16
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/16 v15, 0x8

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 910
    :pswitch_17
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/16 v15, 0xa

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/16 v15, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 914
    :pswitch_18
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 918
    :pswitch_19
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 922
    :pswitch_1a
    const/16 v15, 0x8

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 926
    :pswitch_1b
    const/16 v15, 0x8

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 930
    :pswitch_1c
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 934
    :pswitch_1d
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 938
    :pswitch_1e
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/16 v15, 0x8

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 942
    :pswitch_1f
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x1

    goto/16 :goto_5

    .line 946
    :pswitch_20
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x5

    goto/16 :goto_5

    .line 950
    :pswitch_21
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x6

    goto/16 :goto_5

    .line 954
    :pswitch_22
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x3

    goto/16 :goto_5

    .line 958
    :pswitch_23
    const/16 v15, 0xe

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/16 v15, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 962
    :pswitch_24
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 966
    :pswitch_25
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 970
    :pswitch_26
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x2

    goto/16 :goto_5

    .line 974
    :pswitch_27
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/4 v15, 0x4

    goto/16 :goto_5

    .line 978
    :pswitch_28
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 982
    :pswitch_29
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    goto/16 :goto_5

    .line 986
    :pswitch_2a
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    const/16 v15, 0xb

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 990
    :pswitch_2b
    const/16 v15, 0xb

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 994
    :pswitch_2c
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    const/16 v15, 0xb

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    const/4 v15, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yybegin(I)V

    goto/16 :goto_0

    .line 1003
    :cond_9
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzScanError(I)V

    goto/16 :goto_0

    .line 812
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final getNumWikiTokensSeen()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    return v0
.end method

.method public final getPositionIncrement()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    return v0
.end method

.method final getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V
    .locals 4
    .param p1, "t"    # Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .prologue
    .line 492
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    iget v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    iget v3, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v2, v3

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 493
    return-void
.end method

.method final reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 502
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->currentTokType:I

    .line 503
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numBalanced:I

    .line 504
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->positionInc:I

    .line 505
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numLinkToks:I

    .line 506
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->numWikiTokensSeen:I

    .line 507
    return-void
.end method

.method final setText(Ljava/lang/StringBuilder;)I
    .locals 3
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 496
    iget v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    iget v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int v0, v1, v2

    .line 497
    .local v0, "length":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    invoke-virtual {p1, v1, v2, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 498
    return v0
.end method

.method public final yybegin(I)V
    .locals 0
    .param p1, "newState"    # I

    .prologue
    .line 649
    iput p1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzLexicalState:I

    .line 650
    return-void
.end method

.method public final yychar()I
    .locals 1

    .prologue
    .line 481
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar:I

    return v0
.end method

.method public final yycharat(I)C
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 673
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public final yyclose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzAtEOF:Z

    .line 602
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    iput v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 604
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzReader:Ljava/io/Reader;

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzReader:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 606
    :cond_0
    return-void
.end method

.method public final yylength()I
    .locals 2

    .prologue
    .line 681
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    iget v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public yypushback(I)V
    .locals 1
    .param p1, "number"    # I

    .prologue
    .line 721
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yylength()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 722
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzScanError(I)V

    .line 724
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    .line 725
    return-void
.end method

.method public final yyreset(Ljava/io/Reader;)V
    .locals 3
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    const/16 v2, 0x1000

    const/4 v1, 0x0

    .line 622
    iput-object p1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzReader:Ljava/io/Reader;

    .line 623
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzAtBOL:Z

    .line 624
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzAtEOF:Z

    .line 625
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEOFDone:Z

    .line 626
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzEndRead:I

    .line 627
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzCurrentPos:I

    .line 628
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yycolumn:I

    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar:I

    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yyline:I

    .line 629
    iput v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzLexicalState:I

    .line 630
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    array-length v0, v0

    if-le v0, v2, :cond_0

    .line 631
    new-array v0, v2, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    .line 632
    :cond_0
    return-void
.end method

.method public final yystate()I
    .locals 1

    .prologue
    .line 639
    iget v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzLexicalState:I

    return v0
.end method

.method public final yytext()Ljava/lang/String;
    .locals 5

    .prologue
    .line 657
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzBuffer:[C

    iget v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    iget v3, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzMarkedPos:I

    iget v4, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->zzStartRead:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
