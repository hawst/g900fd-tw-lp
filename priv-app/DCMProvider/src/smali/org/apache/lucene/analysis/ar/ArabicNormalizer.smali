.class public Lorg/apache/lucene/analysis/ar/ArabicNormalizer;
.super Ljava/lang/Object;
.source "ArabicNormalizer.java"


# static fields
.field public static final ALEF:C = '\u0627'

.field public static final ALEF_HAMZA_ABOVE:C = '\u0623'

.field public static final ALEF_HAMZA_BELOW:C = '\u0625'

.field public static final ALEF_MADDA:C = '\u0622'

.field public static final DAMMA:C = '\u064f'

.field public static final DAMMATAN:C = '\u064c'

.field public static final DOTLESS_YEH:C = '\u0649'

.field public static final FATHA:C = '\u064e'

.field public static final FATHATAN:C = '\u064b'

.field public static final HEH:C = '\u0647'

.field public static final KASRA:C = '\u0650'

.field public static final KASRATAN:C = '\u064d'

.field public static final SHADDA:C = '\u0651'

.field public static final SUKUN:C = '\u0652'

.field public static final TATWEEL:C = '\u0640'

.field public static final TEH_MARBUTA:C = '\u0629'

.field public static final YEH:C = '\u064a'


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public normalize([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 69
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 99
    return p2

    .line 70
    :cond_0
    aget-char v1, p1, v0

    sparse-switch v1, :sswitch_data_0

    .line 69
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :sswitch_0
    const/16 v1, 0x627

    aput-char v1, p1, v0

    goto :goto_1

    .line 77
    :sswitch_1
    const/16 v1, 0x64a

    aput-char v1, p1, v0

    goto :goto_1

    .line 80
    :sswitch_2
    const/16 v1, 0x647

    aput-char v1, p1, v0

    goto :goto_1

    .line 91
    :sswitch_3
    invoke-static {p1, v0, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 92
    add-int/lit8 v0, v0, -0x1

    .line 93
    goto :goto_1

    .line 70
    :sswitch_data_0
    .sparse-switch
        0x622 -> :sswitch_0
        0x623 -> :sswitch_0
        0x625 -> :sswitch_0
        0x629 -> :sswitch_2
        0x640 -> :sswitch_3
        0x649 -> :sswitch_1
        0x64b -> :sswitch_3
        0x64c -> :sswitch_3
        0x64d -> :sswitch_3
        0x64e -> :sswitch_3
        0x64f -> :sswitch_3
        0x650 -> :sswitch_3
        0x651 -> :sswitch_3
        0x652 -> :sswitch_3
    .end sparse-switch
.end method
