.class public final Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;
.super Ljava/lang/Object;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/util/CharacterUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CharacterBuffer"
.end annotation


# instance fields
.field private final buffer:[C

.field lastTrailingHighSurrogate:C

.field private length:I

.field private offset:I


# direct methods
.method constructor <init>([CII)V
    .locals 0
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->buffer:[C

    .line 296
    iput p2, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->offset:I

    .line 297
    iput p3, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I

    .line 298
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)[C
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->buffer:[C

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V
    .locals 0

    .prologue
    .line 288
    iput p1, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->offset:I

    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V
    .locals 0

    .prologue
    .line 289
    iput p1, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I

    return-void
.end method

.method static synthetic access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I
    .locals 1

    .prologue
    .line 289
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I

    return v0
.end method


# virtual methods
.method public getBuffer()[C
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->buffer:[C

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->offset:I

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 333
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->offset:I

    .line 334
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I

    .line 335
    iput-char v0, p0, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    .line 336
    return-void
.end method
