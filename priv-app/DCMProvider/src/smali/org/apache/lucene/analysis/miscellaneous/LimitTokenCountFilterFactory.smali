.class public Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "LimitTokenCountFilterFactory.java"


# static fields
.field public static final CONSUME_ALL_TOKENS_KEY:Ljava/lang/String; = "consumeAllTokens"

.field public static final MAX_TOKEN_COUNT_KEY:Ljava/lang/String; = "maxTokenCount"


# instance fields
.field final consumeAllTokens:Z

.field final maxTokenCount:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 48
    const-string v0, "maxTokenCount"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;->requireInt(Ljava/util/Map;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;->maxTokenCount:I

    .line 49
    const-string v0, "consumeAllTokens"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;->consumeAllTokens:Z

    .line 50
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 57
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;->maxTokenCount:I

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilterFactory;->consumeAllTokens:Z

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenCountFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V

    return-object v0
.end method
