.class public Lorg/apache/lucene/analysis/hi/HindiNormalizer;
.super Ljava/lang/Object;
.source "HindiNormalizer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public normalize([CI)I
    .locals 8
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v7, 0x947

    const/16 v6, 0x913

    const/16 v5, 0x90f

    const/16 v4, 0x905

    const/16 v3, 0x902

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 179
    return p2

    .line 52
    :cond_0
    aget-char v1, p1, v0

    sparse-switch v1, :sswitch_data_0

    .line 51
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 55
    :sswitch_0
    add-int/lit8 v1, v0, 0x1

    if-ge v1, p2, :cond_1

    add-int/lit8 v1, v0, 0x1

    aget-char v1, p1, v1

    const/16 v2, 0x94d

    if-ne v1, v2, :cond_1

    .line 56
    aput-char v3, p1, v0

    .line 57
    add-int/lit8 v1, v0, 0x1

    invoke-static {p1, v1, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 59
    goto :goto_1

    .line 62
    :sswitch_1
    aput-char v3, p1, v0

    goto :goto_1

    .line 66
    :sswitch_2
    invoke-static {p1, v0, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 67
    add-int/lit8 v0, v0, -0x1

    .line 68
    goto :goto_1

    .line 70
    :sswitch_3
    const/16 v1, 0x928

    aput-char v1, p1, v0

    goto :goto_1

    .line 73
    :sswitch_4
    const/16 v1, 0x930

    aput-char v1, p1, v0

    goto :goto_1

    .line 76
    :sswitch_5
    const/16 v1, 0x933

    aput-char v1, p1, v0

    goto :goto_1

    .line 79
    :sswitch_6
    const/16 v1, 0x915

    aput-char v1, p1, v0

    goto :goto_1

    .line 82
    :sswitch_7
    const/16 v1, 0x916

    aput-char v1, p1, v0

    goto :goto_1

    .line 85
    :sswitch_8
    const/16 v1, 0x917

    aput-char v1, p1, v0

    goto :goto_1

    .line 88
    :sswitch_9
    const/16 v1, 0x91c

    aput-char v1, p1, v0

    goto :goto_1

    .line 91
    :sswitch_a
    const/16 v1, 0x921

    aput-char v1, p1, v0

    goto :goto_1

    .line 94
    :sswitch_b
    const/16 v1, 0x922

    aput-char v1, p1, v0

    goto :goto_1

    .line 97
    :sswitch_c
    const/16 v1, 0x92b

    aput-char v1, p1, v0

    goto :goto_1

    .line 100
    :sswitch_d
    const/16 v1, 0x92f

    aput-char v1, p1, v0

    goto :goto_1

    .line 105
    :sswitch_e
    invoke-static {p1, v0, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 106
    add-int/lit8 v0, v0, -0x1

    .line 107
    goto :goto_1

    .line 110
    :sswitch_f
    invoke-static {p1, v0, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 111
    add-int/lit8 v0, v0, -0x1

    .line 112
    goto :goto_1

    .line 116
    :sswitch_10
    aput-char v7, p1, v0

    goto :goto_1

    .line 120
    :sswitch_11
    const/16 v1, 0x94b

    aput-char v1, p1, v0

    goto :goto_1

    .line 124
    :sswitch_12
    aput-char v5, p1, v0

    goto :goto_1

    .line 128
    :sswitch_13
    aput-char v6, p1, v0

    goto :goto_1

    .line 131
    :sswitch_14
    aput-char v4, p1, v0

    goto :goto_1

    .line 135
    :sswitch_15
    aput-char v4, p1, v0

    goto :goto_1

    .line 138
    :sswitch_16
    const/16 v1, 0x907

    aput-char v1, p1, v0

    goto :goto_1

    .line 141
    :sswitch_17
    const/16 v1, 0x909

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 144
    :sswitch_18
    const/16 v1, 0x90b

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 147
    :sswitch_19
    const/16 v1, 0x90c

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 150
    :sswitch_1a
    aput-char v5, p1, v0

    goto/16 :goto_1

    .line 153
    :sswitch_1b
    aput-char v6, p1, v0

    goto/16 :goto_1

    .line 157
    :sswitch_1c
    const/16 v1, 0x93f

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 160
    :sswitch_1d
    const/16 v1, 0x941

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 163
    :sswitch_1e
    const/16 v1, 0x943

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 166
    :sswitch_1f
    const/16 v1, 0x962

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 169
    :sswitch_20
    aput-char v7, p1, v0

    goto/16 :goto_1

    .line 172
    :sswitch_21
    const/16 v1, 0x94b

    aput-char v1, p1, v0

    goto/16 :goto_1

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x901 -> :sswitch_1
        0x906 -> :sswitch_15
        0x908 -> :sswitch_16
        0x90a -> :sswitch_17
        0x90d -> :sswitch_12
        0x90e -> :sswitch_12
        0x910 -> :sswitch_1a
        0x911 -> :sswitch_13
        0x912 -> :sswitch_13
        0x914 -> :sswitch_1b
        0x928 -> :sswitch_0
        0x929 -> :sswitch_3
        0x931 -> :sswitch_4
        0x934 -> :sswitch_5
        0x93c -> :sswitch_2
        0x940 -> :sswitch_1c
        0x942 -> :sswitch_1d
        0x944 -> :sswitch_1e
        0x945 -> :sswitch_10
        0x946 -> :sswitch_10
        0x948 -> :sswitch_20
        0x949 -> :sswitch_11
        0x94a -> :sswitch_11
        0x94c -> :sswitch_21
        0x94d -> :sswitch_f
        0x958 -> :sswitch_6
        0x959 -> :sswitch_7
        0x95a -> :sswitch_8
        0x95b -> :sswitch_9
        0x95c -> :sswitch_a
        0x95d -> :sswitch_b
        0x95e -> :sswitch_c
        0x95f -> :sswitch_d
        0x960 -> :sswitch_18
        0x961 -> :sswitch_19
        0x963 -> :sswitch_1f
        0x972 -> :sswitch_14
        0x200c -> :sswitch_e
        0x200d -> :sswitch_e
    .end sparse-switch
.end method
