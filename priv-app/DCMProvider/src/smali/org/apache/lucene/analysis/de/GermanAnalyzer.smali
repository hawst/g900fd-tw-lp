.class public final Lorg/apache/lucene/analysis/de/GermanAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "GermanAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "german_stop.txt"

.field private static final GERMAN_STOP_WORDS:[Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final exclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    const/16 v0, 0x30

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 71
    const-string v2, "einer"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "eine"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "eines"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "einem"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "einen"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 72
    const-string v2, "der"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "die"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "das"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dass"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "da\u00df"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 73
    const-string v2, "du"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "er"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sie"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "es"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 74
    const-string/jumbo v2, "was"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "wer"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "wie"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "wir"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 75
    const-string v2, "und"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "oder"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ohne"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "mit"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 76
    const-string v2, "am"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "im"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "aus"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "auf"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 77
    const-string v2, "ist"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "sein"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "war"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "wird"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 78
    const-string v2, "ihr"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "ihre"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "ihres"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 79
    const-string v2, "als"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "f\u00fcr"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "von"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "mit"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 80
    const-string v2, "dich"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "dir"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "mich"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "mir"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 81
    const-string v2, "mein"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "sein"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "kein"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 82
    const-string v2, "durch"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "wegen"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "wird"

    aput-object v2, v0, v1

    .line 70
    sput-object v0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->GERMAN_STOP_WORDS:[Ljava/lang/String;

    .line 86
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 128
    .line 129
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {}, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->access$0()Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    .line 130
    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/de/GermanAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 131
    return-void

    .line 130
    :cond_0
    # getter for: Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET_30:Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {}, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->access$1()Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 142
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/de/GermanAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 157
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->exclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 158
    return-void
.end method

.method static synthetic access$0()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->GERMAN_STOP_WORDS:[Ljava/lang/String;

    return-object v0
.end method

.method public static final getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 93
    # getter for: Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {}, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->access$0()Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 174
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 175
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 176
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 177
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 178
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->exclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 179
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 180
    new-instance v0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 181
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/de/GermanLightStemFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/de/GermanLightStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 187
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v3

    .line 182
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 183
    new-instance v0, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v3, Lorg/tartarus/snowball/ext/German2Stemmer;

    invoke-direct {v3}, Lorg/tartarus/snowball/ext/German2Stemmer;-><init>()V

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .line 184
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0

    .line 185
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    new-instance v0, Lorg/apache/lucene/analysis/de/GermanStemFilter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/de/GermanStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0
.end method
