.class public final Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CommonGramsQueryFilter.java"


# instance fields
.field private exhausted:Z

.field private final posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private previous:Lorg/apache/lucene/util/AttributeSource$State;

.field private previousType:Ljava/lang/String;

.field private final typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 47
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 48
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 61
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 84
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->exhausted:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_3

    .line 101
    :cond_0
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->exhausted:Z

    .line 103
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v2, :cond_1

    const-string v2, "gram"

    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previousType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 104
    :cond_1
    const/4 v1, 0x0

    .line 113
    :cond_2
    :goto_1
    return v1

    .line 85
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 87
    .local v0, "current":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->isGramType()Z

    move-result v2

    if-nez v2, :cond_4

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 89
    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    .line 90
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previousType:Ljava/lang/String;

    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->isGramType()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 93
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_1

    .line 98
    :cond_4
    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    .line 107
    .end local v0    # "current":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 108
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    .line 110
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->isGramType()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 111
    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_1
.end method

.method public isGramType()Z
    .locals 2

    .prologue
    .line 124
    const-string v0, "gram"

    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 69
    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previous:Lorg/apache/lucene/util/AttributeSource$State;

    .line 70
    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->previousType:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsQueryFilter;->exhausted:Z

    .line 72
    return-void
.end method
