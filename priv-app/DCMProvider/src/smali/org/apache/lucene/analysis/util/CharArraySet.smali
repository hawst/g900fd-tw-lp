.class public Lorg/apache/lucene/analysis/util/CharArraySet;
.super Ljava/util/AbstractSet;
.source "CharArraySet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final PLACEHOLDER:Ljava/lang/Object;


# instance fields
.field private final map:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    sput-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "map":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Ljava/lang/Object;>;"
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 99
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 100
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;IZ)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "startSize"    # I
    .param p3, "ignoreCase"    # Z

    .prologue
    .line 77
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p3, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Collection",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 94
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 95
    return-void
.end method

.method public static copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 4
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Set",
            "<*>;)",
            "Lorg/apache/lucene/analysis/util/CharArraySet;"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<*>;"
    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-ne p1, v1, :cond_0

    .line 195
    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 200
    :goto_0
    return-object v1

    .line 196
    :cond_0
    instance-of v1, p1, Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 197
    check-cast v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 198
    .local v0, "source":Lorg/apache/lucene/analysis/util/CharArraySet;
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v2, v0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    iget-object v2, v2, Lorg/apache/lucene/analysis/util/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v3, v0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-static {v2, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Map;)Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    goto :goto_0

    .line 200
    .end local v0    # "source":Lorg/apache/lucene/analysis/util/CharArraySet;
    :cond_1
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    goto :goto_0
.end method

.method public static unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 2
    .param p0, "set"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 163
    if-nez p0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Given set is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-ne p0, v0, :cond_2

    .line 166
    sget-object p0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 169
    .end local p0    # "set":Lorg/apache/lucene/analysis/util/CharArraySet;
    :cond_1
    :goto_0
    return-object p0

    .line 167
    .restart local p0    # "set":Lorg/apache/lucene/analysis/util/CharArraySet;
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    instance-of v0, v0, Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;

    if-nez v0, :cond_1

    .line 169
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-static {v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->unmodifiableMap(Lorg/apache/lucene/analysis/util/CharArrayMap;)Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public add(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 131
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add(Ljava/lang/String;)Z
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public add([C)Z
    .locals 2
    .param p1, "text"    # [C

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->PLACEHOLDER:Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put([CLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->clear()V

    .line 106
    return-void
.end method

.method public contains(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public contains([CII)Z
    .locals 1
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey([CII)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->originalKeySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArraySet;->map:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 215
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 223
    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 215
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 216
    .local v0, "item":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_1
    instance-of v3, v0, [C

    if-eqz v3, :cond_2

    .line 218
    check-cast v0, [C

    .end local v0    # "item":Ljava/lang/Object;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 220
    .restart local v0    # "item":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
