.class public final Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "WikipediaTokenizer.java"


# static fields
.field public static final ACRONYM_ID:I = 0x2

.field public static final ALPHANUM_ID:I = 0x0

.field public static final APOSTROPHE_ID:I = 0x1

.field public static final BOLD:Ljava/lang/String; = "b"

.field public static final BOLD_ID:I = 0xc

.field public static final BOLD_ITALICS:Ljava/lang/String; = "bi"

.field public static final BOLD_ITALICS_ID:I = 0xe

.field public static final BOTH:I = 0x2

.field public static final CATEGORY:Ljava/lang/String; = "c"

.field public static final CATEGORY_ID:I = 0xb

.field public static final CITATION:Ljava/lang/String; = "ci"

.field public static final CITATION_ID:I = 0xa

.field public static final CJ_ID:I = 0x7

.field public static final COMPANY_ID:I = 0x3

.field public static final EMAIL_ID:I = 0x4

.field public static final EXTERNAL_LINK:Ljava/lang/String; = "el"

.field public static final EXTERNAL_LINK_ID:I = 0x9

.field public static final EXTERNAL_LINK_URL:Ljava/lang/String; = "elu"

.field public static final EXTERNAL_LINK_URL_ID:I = 0x11

.field public static final HEADING:Ljava/lang/String; = "h"

.field public static final HEADING_ID:I = 0xf

.field public static final HOST_ID:I = 0x5

.field public static final INTERNAL_LINK:Ljava/lang/String; = "il"

.field public static final INTERNAL_LINK_ID:I = 0x8

.field public static final ITALICS:Ljava/lang/String; = "i"

.field public static final ITALICS_ID:I = 0xd

.field public static final NUM_ID:I = 0x6

.field public static final SUB_HEADING:Ljava/lang/String; = "sh"

.field public static final SUB_HEADING_ID:I = 0x10

.field public static final TOKENS_ONLY:I = 0x0

.field public static final TOKEN_TYPES:[Ljava/lang/String;

.field public static final UNTOKENIZED_ONLY:I = 0x1

.field public static final UNTOKENIZED_TOKEN_FLAG:I = 0x1


# instance fields
.field private first:Z

.field private final flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private tokenOutput:I

.field private tokens:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private untokenizedTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 74
    const-string v2, "<ALPHANUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 75
    const-string v2, "<APOSTROPHE>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 76
    const-string v2, "<ACRONYM>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 77
    const-string v2, "<COMPANY>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 78
    const-string v2, "<EMAIL>"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 79
    const-string v2, "<HOST>"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 80
    const-string v2, "<NUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 81
    const-string v2, "<CJ>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 82
    const-string v2, "il"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 83
    const-string v2, "el"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 84
    const-string v2, "ci"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 85
    const-string v2, "c"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 86
    const-string v2, "b"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 87
    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 88
    const-string v2, "bi"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 89
    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 90
    const-string v2, "sh"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 91
    const-string v2, "elu"

    aput-object v2, v0, v1

    .line 73
    sput-object v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    .line 109
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 134
    const/4 v0, 0x0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;-><init>(Ljava/io/Reader;ILjava/util/Set;)V

    .line 135
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ILjava/util/Set;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "tokenOutput"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "untokenizedTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokenOutput:I

    .line 116
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->untokenizedTypes:Ljava/util/Set;

    .line 117
    iput-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    .line 119
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 120
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 121
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 122
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 123
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    .line 146
    new-instance v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    .line 147
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->init(ILjava/util/Set;)V

    .line 148
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ILjava/util/Set;)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "tokenOutput"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/AttributeSource$AttributeFactory;",
            "Ljava/io/Reader;",
            "I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "untokenizedTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 158
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokenOutput:I

    .line 116
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->untokenizedTypes:Ljava/util/Set;

    .line 117
    iput-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    .line 119
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 120
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 121
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 122
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 123
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    .line 159
    new-instance v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    .line 160
    invoke-direct {p0, p3, p4}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->init(ILjava/util/Set;)V

    .line 161
    return-void
.end method

.method private collapseAndSaveTokens(ILjava/lang/String;)V
    .locals 13
    .param p1, "tokenType"    # I
    .param p2, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v10, 0x20

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 217
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->setText(Ljava/lang/StringBuilder;)I

    move-result v4

    .line 219
    .local v4, "numAdded":I
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar()I

    move-result v7

    .line 220
    .local v7, "theStart":I
    add-int v3, v7, v4

    .line 222
    .local v3, "lastPos":I
    const/4 v5, 0x0

    .line 223
    .local v5, "numSeen":I
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v8, "tmp":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/AttributeSource$State;>;"
    const/4 v10, 0x0

    invoke-direct {p0, v10, p2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->setupSavedToken(ILjava/lang/String;)V

    .line 225
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    :goto_0
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getNextToken()I

    move-result v9

    .local v9, "tmpTokType":I
    const/4 v10, -0x1

    if-eq v9, v10, :cond_0

    if-ne v9, p1, :cond_0

    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getNumWikiTokensSeen()I

    move-result v10

    if-gt v10, v5, :cond_2

    .line 241
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 242
    .local v6, "s":Ljava/lang/String;
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v10

    invoke-interface {v10, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 243
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v11

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v12, v7

    invoke-virtual {p0, v12}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v12

    invoke-interface {v10, v11, v12}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 244
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->setFlags(I)V

    .line 246
    const/4 v10, -0x1

    if-eq v9, v10, :cond_1

    .line 247
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    iget-object v11, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v11}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yylength()I

    move-result v11

    invoke-virtual {v10, v11}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yypushback(I)V

    .line 249
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    .line 250
    return-void

    .line 228
    .end local v6    # "s":Ljava/lang/String;
    :cond_2
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar()I

    move-result v1

    .line 230
    .local v1, "currPos":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    sub-int v10, v1, v3

    if-lt v2, v10, :cond_3

    .line 233
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->setText(Ljava/lang/StringBuilder;)I

    move-result v4

    .line 234
    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getPositionIncrement()I

    move-result v10

    invoke-direct {p0, v10, p2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->setupSavedToken(ILjava/lang/String;)V

    .line 235
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v5, v5, 0x1

    .line 237
    add-int v3, v1, v4

    goto :goto_0

    .line 231
    :cond_3
    const/16 v10, 0x20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private collapseTokens(I)V
    .locals 13
    .param p1, "tokenType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x20

    const/4 v12, -0x1

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 261
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->setText(Ljava/lang/StringBuilder;)I

    move-result v4

    .line 263
    .local v4, "numAdded":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar()I

    move-result v7

    .line 264
    .local v7, "theStart":I
    add-int v3, v7, v4

    .line 266
    .local v3, "lastPos":I
    const/4 v5, 0x0

    .line 268
    .local v5, "numSeen":I
    :goto_0
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getNextToken()I

    move-result v8

    .local v8, "tmpTokType":I
    if-eq v8, v12, :cond_0

    if-ne v8, p1, :cond_0

    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getNumWikiTokensSeen()I

    move-result v9

    if-gt v9, v5, :cond_1

    .line 280
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 281
    .local v6, "s":Ljava/lang/String;
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v9

    invoke-interface {v9, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 282
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v10

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v7

    invoke-virtual {p0, v11}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v11

    invoke-interface {v9, v10, v11}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 283
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    const/4 v10, 0x1

    invoke-interface {v9, v10}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->setFlags(I)V

    .line 285
    if-eq v8, v12, :cond_3

    .line 286
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    iget-object v10, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yylength()I

    move-result v10

    invoke-virtual {v9, v10}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yypushback(I)V

    .line 290
    :goto_1
    return-void

    .line 269
    .end local v6    # "s":Ljava/lang/String;
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar()I

    move-result v1

    .line 271
    .local v1, "currPos":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    sub-int v9, v1, v3

    if-lt v2, v9, :cond_2

    .line 274
    iget-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v9, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->setText(Ljava/lang/StringBuilder;)I

    move-result v4

    .line 275
    add-int/lit8 v5, v5, 0x1

    .line 276
    add-int v3, v1, v4

    goto :goto_0

    .line 272
    :cond_2
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 271
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 288
    .end local v1    # "currPos":I
    .end local v2    # "i":I
    .restart local v6    # "s":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x0

    iput-object v9, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    goto :goto_1
.end method

.method private init(ILjava/util/Set;)V
    .locals 2
    .param p1, "tokenOutput"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p2, "untokenizedTypes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 166
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 167
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "tokenOutput must be TOKENS_ONLY, UNTOKENIZED_ONLY or BOTH"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokenOutput:I

    .line 171
    iput-object p2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->untokenizedTypes:Ljava/util/Set;

    .line 172
    return-void
.end method

.method private setupSavedToken(ILjava/lang/String;)V
    .locals 1
    .param p1, "positionInc"    # I
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 253
    invoke-direct {p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->setupToken()V

    .line 254
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 255
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v0, p2}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 256
    return-void
.end method

.method private setupToken()V
    .locals 4

    .prologue
    .line 293
    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    iget-object v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V

    .line 294
    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar()I

    move-result v0

    .line 295
    .local v0, "start":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 296
    return-void
.end method


# virtual methods
.method public end()V
    .locals 3

    .prologue
    .line 314
    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yychar()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yylength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->correctOffset(I)I

    move-result v0

    .line 315
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 316
    return-void
.end method

.method public final incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 181
    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 182
    iget-object v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeSource$State;

    .line 183
    .local v1, "state":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 211
    .end local v1    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_0
    return v4

    .line 186
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->clearAttributes()V

    .line 187
    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getNextToken()I

    move-result v2

    .line 189
    .local v2, "tokenType":I
    const/4 v6, -0x1

    if-ne v2, v6, :cond_1

    move v4, v5

    .line 190
    goto :goto_0

    .line 192
    :cond_1
    sget-object v6, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v3, v6, v2

    .line 193
    .local v3, "type":Ljava/lang/String;
    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokenOutput:I

    if-eqz v6, :cond_2

    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->untokenizedTypes:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 194
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->setupToken()V

    .line 204
    :cond_3
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->getPositionIncrement()I

    move-result v0

    .line 205
    .local v0, "posinc":I
    iget-boolean v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->first:Z

    if-eqz v6, :cond_4

    if-nez v0, :cond_4

    .line 206
    const/4 v0, 0x1

    .line 208
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v6, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 209
    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v6, v3}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 210
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->first:Z

    goto :goto_0

    .line 195
    .end local v0    # "posinc":I
    :cond_5
    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokenOutput:I

    if-ne v6, v4, :cond_6

    iget-object v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->untokenizedTypes:Ljava/util/Set;

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 196
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->collapseTokens(I)V

    goto :goto_1

    .line 199
    :cond_6
    iget v6, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokenOutput:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 202
    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->collapseAndSaveTokens(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    iget-object v1, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->yyreset(Ljava/io/Reader;)V

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->tokens:Ljava/util/Iterator;

    .line 307
    iget-object v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->scanner:Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerImpl;->reset()V

    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;->first:Z

    .line 309
    return-void
.end method
