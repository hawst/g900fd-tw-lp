.class public final Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "UAX29URLEmailTokenizer.java"


# static fields
.field public static final ALPHANUM:I = 0x0

.field public static final EMAIL:I = 0x8

.field public static final HANGUL:I = 0x6

.field public static final HIRAGANA:I = 0x4

.field public static final IDEOGRAPHIC:I = 0x3

.field public static final KATAKANA:I = 0x5

.field public static final NUM:I = 0x1

.field public static final SOUTHEAST_ASIAN:I = 0x2

.field public static final TOKEN_TYPES:[Ljava/lang/String;

.field public static final URL:I = 0x7


# instance fields
.field private maxTokenLength:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v3, 0x9

    const/4 v4, 0x6

    const/4 v2, 0x0

    .line 75
    new-array v0, v3, [Ljava/lang/String;

    .line 76
    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v1, v1, v2

    aput-object v1, v0, v2

    const/4 v1, 0x1

    .line 77
    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v2, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 78
    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 79
    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 80
    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 81
    sget-object v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    .line 82
    sget-object v1, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    aput-object v1, v0, v4

    const/4 v1, 0x7

    .line 83
    const-string v2, "<URL>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 84
    const-string v2, "<EMAIL>"

    aput-object v2, v0, v1

    .line 75
    sput-object v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    .line 85
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 107
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 87
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 134
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 135
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 136
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 137
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 108
    invoke-static {p1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->getScannerFor(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 109
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 115
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 87
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 134
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 135
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 136
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 137
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 116
    invoke-static {p1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->getScannerFor(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 117
    return-void
.end method

.method private static getScannerFor(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;
    .locals 2
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    const/4 v1, 0x0

    .line 121
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizerImpl;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizerImpl;-><init>(Ljava/io/Reader;)V

    .line 128
    :goto_0
    return-object v0

    .line 123
    :cond_0
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    new-instance v0, Lorg/apache/lucene/analysis/standard/std36/UAX29URLEmailTokenizerImpl36;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/std36/UAX29URLEmailTokenizerImpl36;-><init>(Ljava/io/Reader;)V

    goto :goto_0

    .line 125
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    new-instance v0, Lorg/apache/lucene/analysis/standard/std34/UAX29URLEmailTokenizerImpl34;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/std34/UAX29URLEmailTokenizerImpl34;-><init>(Ljava/io/Reader;)V

    goto :goto_0

    .line 128
    :cond_2
    new-instance v0, Lorg/apache/lucene/analysis/standard/std31/UAX29URLEmailTokenizerImpl31;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/std31/UAX29URLEmailTokenizerImpl31;-><init>(Ljava/io/Reader;)V

    goto :goto_0
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 168
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->correctOffset(I)I

    move-result v0

    .line 169
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 170
    return-void
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    return v0
.end method

.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->clearAttributes()V

    .line 142
    const/4 v0, 0x1

    .line 145
    .local v0, "posIncr":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getNextToken()I

    move-result v2

    .line 147
    .local v2, "tokenType":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 148
    const/4 v3, 0x0

    .line 157
    :goto_1
    return v3

    .line 151
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    if-gt v3, v4, :cond_1

    .line 152
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 153
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V

    .line 154
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    .line 155
    .local v1, "start":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->correctOffset(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->correctOffset(I)I

    move-result v5

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 156
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 157
    const/4 v3, 0x1

    goto :goto_1

    .line 161
    .end local v1    # "start":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 144
    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->input:Ljava/io/Reader;

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yyreset(Ljava/io/Reader;)V

    .line 175
    return-void
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 92
    iput p1, p0, Lorg/apache/lucene/analysis/standard/UAX29URLEmailTokenizer;->maxTokenLength:I

    .line 93
    return-void
.end method
