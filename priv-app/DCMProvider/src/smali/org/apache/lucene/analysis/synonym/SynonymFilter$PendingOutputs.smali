.class Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
.super Ljava/lang/Object;
.source "SynonymFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/synonym/SynonymFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingOutputs"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field count:I

.field endOffsets:[I

.field lastEndOffset:I

.field lastPosLength:I

.field outputs:[Lorg/apache/lucene/util/CharsRef;

.field posIncr:I

.field posLengths:[I

.field upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 159
    const-class v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    .line 170
    new-array v0, v1, [Lorg/apache/lucene/util/CharsRef;

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    .line 171
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->endOffsets:[I

    .line 172
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posLengths:[I

    .line 173
    return-void
.end method


# virtual methods
.method public add([CIIII)V
    .locals 5
    .param p1, "output"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "endOffset"    # I
    .param p5, "posLength"    # I

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 201
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 202
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    add-int/lit8 v1, v1, 0x1

    sget v2, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [Lorg/apache/lucene/util/CharsRef;

    .line 203
    .local v0, "next":[Lorg/apache/lucene/util/CharsRef;
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 204
    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    .line 206
    .end local v0    # "next":[Lorg/apache/lucene/util/CharsRef;
    :cond_0
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->endOffsets:[I

    array-length v2, v2

    if-ne v1, v2, :cond_1

    .line 207
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [I

    .line 208
    .local v0, "next":[I
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->endOffsets:[I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 209
    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->endOffsets:[I

    .line 211
    .end local v0    # "next":[I
    :cond_1
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posLengths:[I

    array-length v2, v2

    if-ne v1, v2, :cond_2

    .line 212
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [I

    .line 213
    .restart local v0    # "next":[I
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posLengths:[I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 214
    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posLengths:[I

    .line 216
    .end local v0    # "next":[I
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    aget-object v1, v1, v2

    if-nez v1, :cond_3

    .line 217
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    new-instance v3, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v3}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    aput-object v3, v1, v2

    .line 219
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    aget-object v1, v1, v2

    invoke-virtual {v1, p1, p2, p3}, Lorg/apache/lucene/util/CharsRef;->copyChars([CII)V

    .line 223
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->endOffsets:[I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    aput p4, v1, v2

    .line 224
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posLengths:[I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    aput p5, v1, v2

    .line 225
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    .line 226
    return-void
.end method

.method public getLastEndOffset()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->lastEndOffset:I

    return v0
.end method

.method public getLastPosLength()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->lastPosLength:I

    return v0
.end method

.method public pullNext()Lorg/apache/lucene/util/CharsRef;
    .locals 4

    .prologue
    .line 181
    sget-boolean v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-lt v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 182
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->endOffsets:[I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->lastEndOffset:I

    .line 183
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posLengths:[I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    aget v1, v1, v2

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->lastPosLength:I

    .line 184
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->outputs:[Lorg/apache/lucene/util/CharsRef;

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    aget-object v0, v1, v2

    .line 185
    .local v0, "result":Lorg/apache/lucene/util/CharsRef;
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    .line 186
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-ne v1, v2, :cond_1

    .line 187
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->reset()V

    .line 189
    :cond_1
    return-object v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    iput v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    .line 177
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    .line 178
    return-void
.end method
