.class public Lorg/apache/lucene/analysis/util/CharArrayMap;
.super Ljava/util/AbstractMap;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;,
        Lorg/apache/lucene/analysis/util/CharArrayMap$EntryIterator;,
        Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;,
        Lorg/apache/lucene/analysis/util/CharArrayMap$MapEntry;,
        Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<",
        "Ljava/lang/Object;",
        "TV;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final EMPTY_MAP:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INIT_SIZE:I = 0x8


# instance fields
.field private final charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

.field private count:I

.field private entrySet:Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation
.end field

.field private ignoreCase:Z

.field private keySet:Lorg/apache/lucene/analysis/util/CharArraySet;

.field keys:[[C

.field final matchVersion:Lorg/apache/lucene/util/Version;

.field values:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TV;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/util/CharArrayMap;->$assertionsDisabled:Z

    .line 55
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/util/CharArrayMap;->EMPTY_MAP:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 57
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local p1, "toCopy":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    const/4 v0, 0x0

    .line 107
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 356
    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    .line 357
    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 108
    iget-object v0, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    .line 109
    iget-object v0, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    .line 110
    iget-boolean v0, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    .line 111
    iget v0, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    .line 112
    iget-object v0, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 113
    iget-object v0, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 114
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/util/CharArrayMap;Lorg/apache/lucene/analysis/util/CharArrayMap;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;IZ)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "startSize"    # I
    .param p3, "ignoreCase"    # Z

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 356
    iput-object v1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    .line 357
    iput-object v1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 79
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    .line 80
    const/16 v0, 0x8

    .line 81
    .local v0, "size":I
    :goto_0
    shr-int/lit8 v1, p2, 0x2

    add-int/2addr v1, p2

    if-gt v1, v0, :cond_0

    .line 83
    new-array v1, v0, [[C

    iput-object v1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    .line 84
    new-array v1, v0, [Ljava/lang/Object;

    iput-object v1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    .line 85
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 86
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 87
    return-void

    .line 82
    :cond_0
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/Map;Z)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p3, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Map",
            "<*+TV;>;Z)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local p2, "c":Ljava/util/Map;, "Ljava/util/Map<*+TV;>;"
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 103
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->putAll(Ljava/util/Map;)V

    .line 104
    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/analysis/util/CharArrayMap;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    return v0
.end method

.method public static copy(Lorg/apache/lucene/util/Version;Ljava/util/Map;)Lorg/apache/lucene/analysis/util/CharArrayMap;
    .locals 7
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/Map",
            "<*+TV;>;)",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<*+TV;>;"
    const/4 v6, 0x0

    .line 588
    sget-object v4, Lorg/apache/lucene/analysis/util/CharArrayMap;->EMPTY_MAP:Lorg/apache/lucene/analysis/util/CharArrayMap;

    if-ne p1, v4, :cond_0

    .line 589
    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v2

    .line 603
    :goto_0
    return-object v2

    .line 590
    :cond_0
    instance-of v4, p1, Lorg/apache/lucene/analysis/util/CharArrayMap;

    if-eqz v4, :cond_1

    move-object v1, p1

    .line 591
    check-cast v1, Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 594
    .local v1, "m":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v4, v1, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v4, v4

    new-array v0, v4, [[C

    .line 595
    .local v0, "keys":[[C
    iget-object v4, v1, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v5, v0

    invoke-static {v4, v6, v0, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 596
    iget-object v4, v1, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/Object;

    .line 597
    .local v3, "values":[Ljava/lang/Object;
    iget-object v4, v1, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    array-length v5, v3

    invoke-static {v4, v6, v3, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 598
    new-instance v2, Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-direct {v2, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 599
    .end local v1    # "m":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local v2, "m":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iput-object v0, v2, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    .line 600
    iput-object v3, v2, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    goto :goto_0

    .line 603
    .end local v0    # "keys":[[C
    .end local v2    # "m":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .end local v3    # "values":[Ljava/lang/Object;
    :cond_1
    new-instance v2, Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-direct {v2, p0, p1, v6}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Map;Z)V

    goto :goto_0
.end method

.method public static emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 609
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArrayMap;->EMPTY_MAP:Lorg/apache/lucene/analysis/util/CharArrayMap;

    return-object v0
.end method

.method private equals(Ljava/lang/CharSequence;[C)Z
    .locals 6
    .param p1, "text1"    # Ljava/lang/CharSequence;
    .param p2, "text2"    # [C

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    const/4 v3, 0x0

    .line 279
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 280
    .local v2, "len":I
    array-length v4, p2

    if-eq v2, v4, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v3

    .line 282
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_4

    .line 283
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_3

    .line 295
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 284
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v4, p1, v1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 285
    .local v0, "codePointAt":I
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v5, p2, v1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CI)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 287
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_1

    .line 290
    .end local v0    # "codePointAt":I
    .end local v1    # "i":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v2, :cond_2

    .line 291
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    aget-char v5, p2, v1

    if-ne v4, v5, :cond_0

    .line 290
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private equals([CII[C)Z
    .locals 6
    .param p1, "text1"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .param p4, "text2"    # [C

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    const/4 v3, 0x0

    .line 259
    array-length v4, p4

    if-eq p3, v4, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v3

    .line 261
    :cond_1
    add-int v2, p2, p3

    .line 262
    .local v2, "limit":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_4

    .line 263
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, p3, :cond_3

    .line 275
    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    .line 264
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    add-int v5, p2, v1

    invoke-virtual {v4, p1, v5, v2}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CII)I

    move-result v0

    .line 265
    .local v0, "codePointAt":I
    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v5, p4, v1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CI)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 267
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_1

    .line 270
    .end local v0    # "codePointAt":I
    .end local v1    # "i":I
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p3, :cond_2

    .line 271
    add-int v4, p2, v1

    aget-char v4, p1, v4

    aget-char v5, p4, v1

    if-ne v4, v5, :cond_0

    .line 270
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private getHashCode(Ljava/lang/CharSequence;)I
    .locals 6
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 318
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 319
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    .line 320
    :cond_0
    const/4 v0, 0x0

    .line 321
    .local v0, "code":I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 322
    .local v3, "len":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_3

    .line 323
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_2

    .line 333
    :cond_1
    return v0

    .line 324
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v4, p1, v2}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v1

    .line 325
    .local v1, "codePointAt":I
    mul-int/lit8 v4, v0, 0x1f

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v5

    add-int v0, v4, v5

    .line 326
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_0

    .line 329
    .end local v1    # "codePointAt":I
    .end local v2    # "i":I
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 330
    mul-int/lit8 v4, v0, 0x1f

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    add-int v0, v4, v5

    .line 329
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getHashCode([CII)I
    .locals 6
    .param p1, "text"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 299
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 300
    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    .line 301
    :cond_0
    const/4 v0, 0x0

    .line 302
    .local v0, "code":I
    add-int v3, p2, p3

    .line 303
    .local v3, "stop":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    if-eqz v4, :cond_3

    .line 304
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_2

    .line 314
    :cond_1
    return v0

    .line 305
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v4, p1, v2, v3}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CII)I

    move-result v1

    .line 306
    .local v1, "codePointAt":I
    mul-int/lit8 v4, v0, 0x1f

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v5

    add-int v0, v4, v5

    .line 307
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_0

    .line 310
    .end local v1    # "codePointAt":I
    .end local v2    # "i":I
    :cond_3
    move v2, p2

    .restart local v2    # "i":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 311
    mul-int/lit8 v4, v0, 0x1f

    aget-char v5, p1, v2

    add-int v0, v4, v5

    .line 310
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getSlot(Ljava/lang/CharSequence;)I
    .locals 5
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 181
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getHashCode(Ljava/lang/CharSequence;)I

    move-result v0

    .line 182
    .local v0, "code":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 183
    .local v2, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 184
    .local v3, "text2":[C
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->equals(Ljava/lang/CharSequence;[C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 185
    shr-int/lit8 v4, v0, 0x8

    add-int/2addr v4, v0

    or-int/lit8 v1, v4, 0x1

    .line 187
    .local v1, "inc":I
    :cond_0
    add-int/2addr v0, v1

    .line 188
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 189
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 190
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->equals(Ljava/lang/CharSequence;[C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 192
    .end local v1    # "inc":I
    :cond_1
    return v2
.end method

.method private getSlot([CII)I
    .locals 5
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 165
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getHashCode([CII)I

    move-result v0

    .line 166
    .local v0, "code":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 167
    .local v2, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 168
    .local v3, "text2":[C
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, p2, p3, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->equals([CII[C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 169
    shr-int/lit8 v4, v0, 0x8

    add-int/2addr v4, v0

    or-int/lit8 v1, v4, 0x1

    .line 171
    .local v1, "inc":I
    :cond_0
    add-int/2addr v0, v1

    .line 172
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    and-int v2, v0, v4

    .line 173
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aget-object v3, v4, v2

    .line 174
    if-eqz v3, :cond_1

    invoke-direct {p0, p1, p2, p3, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->equals([CII[C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 176
    .end local v1    # "inc":I
    :cond_1
    return v2
.end method

.method private rehash()V
    .locals 8

    .prologue
    .line 240
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    sget-boolean v6, Lorg/apache/lucene/analysis/util/CharArrayMap;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v6, v6

    iget-object v7, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    array-length v7, v7

    if-eq v6, v7, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 241
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x2

    .line 242
    .local v1, "newSize":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    .line 243
    .local v2, "oldkeys":[[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    .line 244
    .local v3, "oldvalues":[Ljava/lang/Object;
    new-array v6, v1, [[C

    iput-object v6, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    .line 245
    new-array v6, v1, [Ljava/lang/Object;

    iput-object v6, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    .line 247
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v2

    if-lt v0, v6, :cond_1

    .line 256
    return-void

    .line 248
    :cond_1
    aget-object v5, v2, v0

    .line 249
    .local v5, "text":[C
    if-eqz v5, :cond_2

    .line 251
    const/4 v6, 0x0

    array-length v7, v5

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getSlot([CII)I

    move-result v4

    .line 252
    .local v4, "slot":I
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aput-object v5, v6, v4

    .line 253
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    aget-object v7, v3, v0

    aput-object v7, v6, v4

    .line 247
    .end local v4    # "slot":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static unmodifiableMap(Lorg/apache/lucene/analysis/util/CharArrayMap;)Lorg/apache/lucene/analysis/util/CharArrayMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>;)",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 556
    .local p0, "map":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    if-nez p0, :cond_0

    .line 557
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Given map is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 558
    :cond_0
    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v0

    if-eq p0, v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 559
    :cond_1
    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object p0

    .line 562
    .end local p0    # "map":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    :cond_2
    :goto_0
    return-object p0

    .line 560
    .restart local p0    # "map":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    :cond_3
    instance-of v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;

    if-nez v0, :cond_2

    .line 562
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    const/4 v1, 0x0

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public containsKey(Ljava/lang/CharSequence;)Z
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 132
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getSlot(Ljava/lang/CharSequence;)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 137
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    instance-of v1, p1, [C

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 138
    check-cast v0, [C

    .line 139
    .local v0, "text":[C
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey([CII)Z

    move-result v1

    .line 141
    .end local v0    # "text":[C
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_0
.end method

.method public containsKey([CII)Z
    .locals 2
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 127
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getSlot([CII)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method createEntrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation

    .prologue
    .line 360
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;ZLorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;)V

    return-object v0
.end method

.method public bridge synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    move-result-object v0

    return-object v0
.end method

.method public final entrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation

    .prologue
    .line 365
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    if-nez v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->createEntrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    .line 368
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet:Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    return-object v0
.end method

.method public get(Ljava/lang/CharSequence;)Ljava/lang/Object;
    .locals 2
    .param p1, "cs"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getSlot(Ljava/lang/CharSequence;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 157
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    instance-of v1, p1, [C

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 158
    check-cast v0, [C

    .line 159
    .local v0, "text":[C
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v1

    .line 161
    .end local v0    # "text":[C
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get(Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public get([CII)Ljava/lang/Object;
    .locals 2
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CII)TV;"
        }
    .end annotation

    .prologue
    .line 147
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getSlot([CII)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet()Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public final keySet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 380
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-nez v0, :cond_0

    .line 382
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap$1;

    invoke-direct {v0, p0, p0}, Lorg/apache/lucene/analysis/util/CharArrayMap$1;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 401
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keySet:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method

.method final originalKeySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 373
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    invoke-super {p0}, Ljava/util/AbstractMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 197
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 202
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    instance-of v0, p1, [C

    if-eqz v0, :cond_0

    .line 203
    check-cast p1, [C

    .end local p1    # "o":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put([CLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 205
    :goto_0
    return-object v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 210
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put([CLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public put([CLjava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1, "text"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CTV;)TV;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    const/4 v4, 0x0

    .line 218
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->ignoreCase:Z

    if-eqz v2, :cond_0

    .line 219
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    array-length v3, p1

    invoke-virtual {v2, p1, v4, v3}, Lorg/apache/lucene/analysis/util/CharacterUtils;->toLowerCase([CII)V

    .line 221
    :cond_0
    array-length v2, p1

    invoke-direct {p0, p1, v4, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->getSlot([CII)I

    move-result v1

    .line 222
    .local v1, "slot":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aget-object v2, v2, v1

    if-eqz v2, :cond_1

    .line 223
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    aget-object v0, v2, v1

    .line 224
    .local v0, "oldValue":Ljava/lang/Object;, "TV;"
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 235
    .end local v0    # "oldValue":Ljava/lang/Object;, "TV;"
    :goto_0
    return-object v0

    .line 227
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    aput-object p1, v2, v1

    .line 228
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->values:[Ljava/lang/Object;

    aput-object p2, v2, v1

    .line 229
    iget v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    .line 231
    iget v2, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    iget v3, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    shr-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->keys:[[C

    array-length v3, v3

    if-le v2, v3, :cond_2

    .line 232
    invoke-direct {p0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->rehash()V

    .line 235
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 338
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 343
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayMap;->count:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 348
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 349
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 353
    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 349
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 350
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Object;TV;>;"
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
