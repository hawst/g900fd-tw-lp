.class public abstract Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;
.super Ljava/lang/Object;
.source "Analyzer.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/Analyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReuseStrategy"
.end annotation


# instance fields
.field private storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    new-instance v0, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 278
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->close()V

    .line 342
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 344
    :cond_0
    return-void
.end method

.method public abstract getReusableComponents(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.end method

.method protected final getStoredValue()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 307
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "npe":Ljava/lang/NullPointerException;
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    if-nez v1, :cond_0

    .line 310
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v2, "this Analyzer is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 312
    :cond_0
    throw v0
.end method

.method public abstract setReusableComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)V
.end method

.method protected final setStoredValue(Ljava/lang/Object;)V
    .locals 3
    .param p1, "storedValue"    # Ljava/lang/Object;

    .prologue
    .line 325
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->storedValue:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "npe":Ljava/lang/NullPointerException;
    if-nez p1, :cond_0

    .line 328
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v2, "this Analyzer is closed"

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 330
    :cond_0
    throw v0
.end method
