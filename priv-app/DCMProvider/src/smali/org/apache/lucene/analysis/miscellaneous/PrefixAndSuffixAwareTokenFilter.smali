.class public Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "PrefixAndSuffixAwareTokenFilter.java"


# instance fields
.field private suffix:Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 2
    .param p1, "prefix"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "suffix"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 36
    invoke-direct {p0, p3}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 37
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter$1;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter$1;-><init>(Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 43
    .end local p1    # "prefix":Lorg/apache/lucene/analysis/TokenStream;
    .local v0, "prefix":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter$2;

    invoke-direct {v1, p0, v0, p3}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter$2;-><init>(Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/TokenStream;)V

    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;

    .line 49
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->close()V

    .line 78
    return-void
.end method

.method public end()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->end()V

    .line 83
    return-void
.end method

.method public final incrementToken()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->incrementToken()Z

    move-result v0

    return v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAndSuffixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->reset()V

    .line 72
    return-void
.end method

.method public updateInputToken(Lorg/apache/lucene/analysis/Token;Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;
    .locals 3
    .param p1, "inputToken"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "lastPrefixToken"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 52
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->startOffset()I

    move-result v1

    add-int/2addr v0, v1

    .line 53
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v2

    add-int/2addr v1, v2

    .line 52
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/analysis/Token;->setOffset(II)V

    .line 54
    return-object p1
.end method

.method public updateSuffixToken(Lorg/apache/lucene/analysis/Token;Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;
    .locals 3
    .param p1, "suffixToken"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "lastInputToken"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 58
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->startOffset()I

    move-result v1

    add-int/2addr v0, v1

    .line 59
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v2

    add-int/2addr v1, v2

    .line 58
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/analysis/Token;->setOffset(II)V

    .line 60
    return-object p1
.end method
