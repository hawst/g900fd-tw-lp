.class public final Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CapitalizationFilter.java"


# static fields
.field public static final DEFAULT_MAX_TOKEN_LENGTH:I = 0x7fffffff

.field public static final DEFAULT_MAX_WORD_COUNT:I = 0x7fffffff


# instance fields
.field private final forceFirstLetter:Z

.field private final keep:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final maxTokenLength:I

.field private final maxWordCount:I

.field private final minWordLength:I

.field private final okPrefix:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<[C>;"
        }
    .end annotation
.end field

.field private final onlyFirstWord:Z

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 9
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    const/4 v3, 0x0

    const v7, 0x7fffffff

    const/4 v2, 0x1

    .line 57
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move-object v5, v3

    move v8, v7

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;ZLorg/apache/lucene/analysis/util/CharArraySet;ZLjava/util/Collection;III)V

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;ZLorg/apache/lucene/analysis/util/CharArraySet;ZLjava/util/Collection;III)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "onlyFirstWord"    # Z
    .param p3, "keep"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p4, "forceFirstLetter"    # Z
    .param p6, "minWordLength"    # I
    .param p7, "maxWordCount"    # I
    .param p8, "maxTokenLength"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Z",
            "Lorg/apache/lucene/analysis/util/CharArraySet;",
            "Z",
            "Ljava/util/Collection",
            "<[C>;III)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p5, "okPrefix":Ljava/util/Collection;, "Ljava/util/Collection<[C>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 48
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 77
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->onlyFirstWord:Z

    .line 78
    iput-object p3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->keep:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 79
    iput-boolean p4, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->forceFirstLetter:Z

    .line 80
    iput-object p5, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->okPrefix:Ljava/util/Collection;

    .line 81
    iput p6, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->minWordLength:I

    .line 82
    iput p7, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->maxWordCount:I

    .line 83
    iput p8, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->maxTokenLength:I

    .line 84
    return-void
.end method

.method private processWord([CIII)V
    .locals 6
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "wordCount"    # I

    .prologue
    .line 130
    const/4 v3, 0x1

    if-ge p3, v3, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->onlyFirstWord:Z

    if-eqz v3, :cond_2

    if-lez p4, :cond_2

    .line 135
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p3, :cond_0

    .line 136
    add-int v3, p2, v0

    add-int v4, p2, v0

    aget-char v4, p1, v4

    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    aput-char v4, p1, v3

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 142
    .end local v0    # "i":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->keep:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->keep:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 143
    if-nez p4, :cond_0

    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->forceFirstLetter:Z

    if-eqz v3, :cond_0

    .line 144
    aget-char v3, p1, p2

    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v3

    aput-char v3, p1, p2

    goto :goto_0

    .line 149
    :cond_3
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->minWordLength:I

    if-lt p3, v3, :cond_0

    .line 153
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->okPrefix:Ljava/util/Collection;

    if-eqz v3, :cond_5

    .line 154
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->okPrefix:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_6

    .line 174
    :cond_5
    aget-char v3, p1, p2

    invoke-static {v3}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v3

    aput-char v3, p1, p2

    .line 176
    const/4 v0, 0x1

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, p3, :cond_0

    .line 177
    add-int v3, p2, v0

    add-int v4, p2, v0

    aget-char v4, p1, v4

    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v4

    aput-char v4, p1, v3

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 154
    .end local v0    # "i":I
    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [C

    .line 155
    .local v2, "prefix":[C
    array-length v4, v2

    if-lt p3, v4, :cond_4

    .line 156
    const/4 v1, 0x1

    .line 157
    .local v1, "match":Z
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    array-length v4, v2

    if-lt v0, v4, :cond_7

    .line 163
    :goto_4
    if-eqz v1, :cond_4

    goto :goto_0

    .line 158
    :cond_7
    aget-char v4, v2, v0

    add-int v5, p2, v0

    aget-char v5, p1, v5

    if-eq v4, v5, :cond_8

    .line 159
    const/4 v1, 0x0

    .line 160
    goto :goto_4

    .line 157
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method


# virtual methods
.method public incrementToken()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 88
    iget-object v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v10

    if-nez v10, :cond_0

    .line 126
    :goto_0
    return v9

    .line 90
    :cond_0
    iget-object v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v5

    .line 91
    .local v5, "termBuffer":[C
    iget-object v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v6

    .line 92
    .local v6, "termBufferLength":I
    const/4 v0, 0x0

    .line 94
    .local v0, "backup":[C
    iget v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->maxWordCount:I

    const v11, 0x7fffffff

    if-ge v10, v11, :cond_1

    .line 96
    new-array v0, v6, [C

    .line 97
    invoke-static {v5, v9, v0, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    :cond_1
    iget v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->maxTokenLength:I

    if-ge v6, v10, :cond_2

    .line 101
    const/4 v7, 0x0

    .line 103
    .local v7, "wordCount":I
    const/4 v3, 0x0

    .line 104
    .local v3, "lastWordStart":I
    const/4 v2, 0x0

    .local v2, "i":I
    move v8, v7

    .end local v7    # "wordCount":I
    .local v8, "wordCount":I
    :goto_1
    if-lt v2, v6, :cond_3

    .line 117
    if-ge v3, v6, :cond_5

    .line 118
    sub-int v10, v6, v3

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "wordCount":I
    .restart local v7    # "wordCount":I
    invoke-direct {p0, v5, v3, v10, v8}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->processWord([CIII)V

    .line 121
    :goto_2
    iget v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->maxWordCount:I

    if-le v7, v10, :cond_2

    .line 122
    iget-object v10, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v10, v0, v9, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 126
    .end local v2    # "i":I
    .end local v3    # "lastWordStart":I
    .end local v7    # "wordCount":I
    :cond_2
    const/4 v9, 0x1

    goto :goto_0

    .line 105
    .restart local v2    # "i":I
    .restart local v3    # "lastWordStart":I
    .restart local v8    # "wordCount":I
    :cond_3
    aget-char v1, v5, v2

    .line 106
    .local v1, "c":C
    const/16 v10, 0x20

    if-le v1, v10, :cond_4

    const/16 v10, 0x2e

    if-ne v1, v10, :cond_6

    .line 107
    :cond_4
    sub-int v4, v2, v3

    .line 108
    .local v4, "len":I
    if-lez v4, :cond_6

    .line 109
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "wordCount":I
    .restart local v7    # "wordCount":I
    invoke-direct {p0, v5, v3, v4, v8}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;->processWord([CIII)V

    .line 110
    add-int/lit8 v3, v2, 0x1

    .line 111
    add-int/lit8 v2, v2, 0x1

    .line 104
    .end local v4    # "len":I
    :goto_3
    add-int/lit8 v2, v2, 0x1

    move v8, v7

    .end local v7    # "wordCount":I
    .restart local v8    # "wordCount":I
    goto :goto_1

    .end local v1    # "c":C
    :cond_5
    move v7, v8

    .end local v8    # "wordCount":I
    .restart local v7    # "wordCount":I
    goto :goto_2

    .end local v7    # "wordCount":I
    .restart local v1    # "c":C
    .restart local v8    # "wordCount":I
    :cond_6
    move v7, v8

    .end local v8    # "wordCount":I
    .restart local v7    # "wordCount":I
    goto :goto_3
.end method
