.class public Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "StemmerOverrideFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private dictionary:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

.field private final dictionaryFiles:Ljava/lang/String;

.field private final ignoreCase:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 48
    const-string v0, "dictionary"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->dictionaryFiles:Ljava/lang/String;

    .line 49
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->ignoreCase:Z

    .line 50
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 80
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->dictionary:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    if-nez v0, :cond_0

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    return-object p1

    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->dictionary:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 10
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->dictionaryFiles:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 58
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->assureMatchVersion()V

    .line 59
    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->dictionaryFiles:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 60
    .local v2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 61
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;

    iget-boolean v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->ignoreCase:Z

    invoke-direct {v0, v6}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;-><init>(Z)V

    .line 62
    .local v0, "builder":Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_2

    .line 69
    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->build()Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->dictionary:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    .line 72
    .end local v0    # "builder":Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;
    .end local v2    # "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void

    .line 62
    .restart local v0    # "builder":Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;
    .restart local v2    # "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 63
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, p1, v7}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 64
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 65
    .local v3, "line":Ljava/lang/String;
    const-string v8, "\t"

    const/4 v9, 0x2

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "mapping":[Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v8, v5, v8

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-virtual {v0, v8, v9}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->add(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    goto :goto_0
.end method

.method public isIgnoreCase()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilterFactory;->ignoreCase:Z

    return v0
.end method
