.class public final Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "NumericTokenStream.java"

# interfaces
.implements Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/NumericTokenStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NumericTermAttributeImpl"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bytes:Lorg/apache/lucene/util/BytesRef;

.field private precisionStep:I

.field private shift:I

.field private value:J

.field private valueSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    const-class v0, Lorg/apache/lucene/analysis/NumericTokenStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 146
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->value:J

    .line 147
    iput v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    iput v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    iput v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->precisionStep:I

    .line 148
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 154
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 7
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 214
    move-object v1, p1

    check-cast v1, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    .line 215
    .local v1, "a":Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;
    iget-wide v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->value:J

    iget v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->precisionStep:I

    iget v6, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    invoke-interface/range {v1 .. v6}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->init(JIII)V

    .line 216
    return-void
.end method

.method public fillBytesRef()I
    .locals 6

    .prologue
    const/16 v4, 0x40

    const/4 v1, 0x0

    .line 164
    :try_start_0
    sget-boolean v2, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    const/16 v3, 0x20

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :catch_0
    move-exception v0

    .line 170
    .local v0, "iae":Ljava/lang/IllegalArgumentException;
    iget-object v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    iput v1, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 171
    .end local v0    # "iae":Ljava/lang/IllegalArgumentException;
    :goto_0
    return v1

    .line 165
    :cond_0
    :try_start_1
    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    if-ne v2, v4, :cond_1

    .line 166
    iget-wide v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->value:J

    iget v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    iget-object v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/util/NumericUtils;->longToPrefixCoded(JILorg/apache/lucene/util/BytesRef;)I

    move-result v1

    goto :goto_0

    .line 167
    :cond_1
    iget-wide v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->value:J

    long-to-int v2, v2

    iget v3, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    iget-object v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/NumericUtils;->intToPrefixCoded(IILorg/apache/lucene/util/BytesRef;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto :goto_0
.end method

.method public getBytesRef()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public getRawValue()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 185
    iget-wide v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->value:J

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    shl-long v2, v4, v2

    sub-long/2addr v2, v4

    const-wide/16 v4, -0x1

    xor-long/2addr v2, v4

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public getShift()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    return v0
.end method

.method public getValueSize()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    return v0
.end method

.method public incShift()I
    .locals 2

    .prologue
    .line 181
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->precisionStep:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    return v0
.end method

.method public init(JIII)V
    .locals 1
    .param p1, "value"    # J
    .param p3, "valueSize"    # I
    .param p4, "precisionStep"    # I
    .param p5, "shift"    # I

    .prologue
    .line 191
    iput-wide p1, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->value:J

    .line 192
    iput p3, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    .line 193
    iput p4, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->precisionStep:I

    .line 194
    iput p5, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    .line 195
    return-void
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 4
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 205
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->fillBytesRef()I

    .line 206
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    const-string v1, "bytes"

    iget-object v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    const-class v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    const-string v1, "shift"

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    const-class v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    const-string v1, "rawValue"

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->getRawValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 209
    const-class v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    const-string v1, "valueSize"

    iget v2, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->valueSize:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 210
    return-void
.end method

.method public setShift(I)V
    .locals 0
    .param p1, "shift"    # I

    .prologue
    .line 178
    iput p1, p0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;->shift:I

    return-void
.end method
