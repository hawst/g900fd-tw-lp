.class public Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "CharTermAttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
.implements Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;


# static fields
.field private static MIN_BUFFER_SIZE:I


# instance fields
.field private bytes:Lorg/apache/lucene/util/BytesRef;

.field private termBuffer:[C

.field private termLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0xa

    sput v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->MIN_BUFFER_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 33
    sget v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->MIN_BUFFER_SIZE:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 86
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    sget v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->MIN_BUFFER_SIZE:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 37
    return-void
.end method

.method private appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 4

    .prologue
    const/16 v3, 0x6c

    .line 205
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    .line 206
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    const/16 v2, 0x6e

    aput-char v2, v0, v1

    .line 207
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    const/16 v2, 0x75

    aput-char v2, v0, v1

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    aput-char v3, v0, v1

    .line 209
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    aput-char v3, v0, v1

    .line 210
    return-object p0
.end method

.method private growTermBuffer(I)V
    .locals 1
    .param p1, "newSize"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v0, v0

    if-ge v0, p1, :cond_0

    .line 67
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 69
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic append(C)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    return-object v0
.end method

.method public final append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 3
    .param p1, "c"    # C

    .prologue
    .line 168
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    aput-char p1, v0, v1

    .line 169
    return-object p0
.end method

.method public final append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 2
    .param p1, "csq"    # Ljava/lang/CharSequence;

    .prologue
    .line 124
    if-nez p1, :cond_0

    .line 125
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v0

    goto :goto_0
.end method

.method public final append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 8
    .param p1, "csq"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 131
    if-nez p1, :cond_0

    .line 132
    const-string p1, "null"

    .line 133
    :cond_0
    sub-int v2, p3, p2

    .local v2, "len":I
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 134
    .local v1, "csqlen":I
    if-ltz v2, :cond_1

    if-gt p2, v1, :cond_1

    if-le p3, v1, :cond_2

    .line 135
    :cond_1
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v4}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v4

    .line 136
    :cond_2
    if-nez v2, :cond_3

    .line 162
    .end local p1    # "csq":Ljava/lang/CharSequence;
    :goto_0
    return-object p0

    .line 138
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_3
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v4, v2

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    .line 139
    const/4 v4, 0x4

    if-le v2, v4, :cond_b

    .line 140
    instance-of v4, p1, Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 141
    check-cast p1, Ljava/lang/String;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, p2, p3, v4, v5}, Ljava/lang/String;->getChars(II[CI)V

    .line 157
    :goto_1
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0

    .line 142
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_4
    instance-of v4, p1, Ljava/lang/StringBuilder;

    if-eqz v4, :cond_5

    .line 143
    check-cast p1, Ljava/lang/StringBuilder;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, p2, p3, v4, v5}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    goto :goto_1

    .line 144
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_5
    instance-of v4, p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    if-eqz v4, :cond_6

    .line 145
    check-cast p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    invoke-interface {p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v4, p2, v5, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 146
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_6
    instance-of v4, p1, Ljava/nio/CharBuffer;

    if-eqz v4, :cond_7

    move-object v4, p1

    check-cast v4, Ljava/nio/CharBuffer;

    invoke-virtual {v4}, Ljava/nio/CharBuffer;->hasArray()Z

    move-result v4

    if-eqz v4, :cond_7

    move-object v0, p1

    .line 147
    check-cast v0, Ljava/nio/CharBuffer;

    .line 148
    .local v0, "cb":Ljava/nio/CharBuffer;
    invoke-virtual {v0}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->arrayOffset()I

    move-result v5

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->position()I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v5, p2

    iget-object v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v7, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v4, v5, v6, v7, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1

    .line 149
    .end local v0    # "cb":Ljava/nio/CharBuffer;
    :cond_7
    instance-of v4, p1, Ljava/lang/StringBuffer;

    if-eqz v4, :cond_9

    .line 150
    check-cast p1, Ljava/lang/StringBuffer;

    .end local p1    # "csq":Ljava/lang/CharSequence;
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, p2, p3, v4, v5}, Ljava/lang/StringBuffer;->getChars(II[CI)V

    goto :goto_1

    .line 153
    .end local p2    # "start":I
    .local v3, "start":I
    .restart local p1    # "csq":Ljava/lang/CharSequence;
    :cond_8
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 p2, v3, 0x1

    .end local v3    # "start":I
    .restart local p2    # "start":I
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    aput-char v6, v4, v5

    :cond_9
    move v3, p2

    .line 152
    .end local p2    # "start":I
    .restart local v3    # "start":I
    if-lt v3, p3, :cond_8

    move p2, v3

    .line 155
    .end local v3    # "start":I
    .restart local p2    # "start":I
    goto/16 :goto_0

    .line 161
    .end local p2    # "start":I
    .restart local v3    # "start":I
    :cond_a
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/lit8 p2, v3, 0x1

    .end local v3    # "start":I
    .restart local p2    # "start":I
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    aput-char v6, v4, v5

    :cond_b
    move v3, p2

    .line 160
    .end local p2    # "start":I
    .restart local v3    # "start":I
    if-lt v3, p3, :cond_a

    move p2, v3

    .line 162
    .end local v3    # "start":I
    .restart local p2    # "start":I
    goto/16 :goto_0
.end method

.method public final append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 177
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object p0

    .line 181
    .end local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :goto_0
    return-object p0

    .line 178
    .restart local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 179
    .local v0, "len":I
    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, v1, v0, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 180
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0
.end method

.method public final append(Ljava/lang/StringBuilder;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 4
    .param p1, "s"    # Ljava/lang/StringBuilder;

    .prologue
    .line 186
    if-nez p1, :cond_0

    .line 187
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object p0

    .line 191
    .end local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :goto_0
    return-object p0

    .line 188
    .restart local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 189
    .local v0, "len":I
    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-virtual {p1, v1, v0, v2, v3}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 190
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0
.end method

.method public final append(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 5
    .param p1, "ta"    # Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .prologue
    .line 196
    if-nez p1, :cond_0

    .line 197
    invoke-direct {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->appendNull()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object p0

    .line 201
    .end local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :goto_0
    return-object p0

    .line 198
    .restart local p0    # "this":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_0
    invoke-interface {p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 199
    .local v0, "len":I
    invoke-interface {p1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->resizeBuffer(I)[C

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 200
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    goto :goto_0
.end method

.method public final buffer()[C
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    return-object v0
.end method

.method public final charAt(I)C
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 108
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-lt p1, v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 225
    return-void
.end method

.method public clone()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 229
    invoke-super {p0}, Lorg/apache/lucene/util/AttributeImpl;->clone()Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    .line 231
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    new-array v1, v1, [C

    iput-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 232
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget-object v2, v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 233
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 234
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/util/AttributeImpl;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->clone()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    move-result-object v0

    return-object v0
.end method

.method public final copyBuffer([CII)V
    .locals 2
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 41
    invoke-direct {p0, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->growTermBuffer(I)V

    .line 42
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 43
    iput p3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 44
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 4
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 281
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 282
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-interface {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 283
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 239
    if-ne p1, p0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return v2

    .line 243
    :cond_1
    instance-of v4, p1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    if-eqz v4, :cond_4

    move-object v1, p1

    .line 244
    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;

    .line 245
    .local v1, "o":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    iget v5, v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-eq v4, v5, :cond_2

    move v2, v3

    .line 246
    goto :goto_0

    .line 247
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-ge v0, v4, :cond_0

    .line 248
    iget-object v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    aget-char v4, v4, v0

    iget-object v5, v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    aget-char v5, v5, v0

    if-eq v4, v5, :cond_3

    move v2, v3

    .line 249
    goto :goto_0

    .line 247
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "o":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;
    :cond_4
    move v2, v3

    .line 255
    goto :goto_0
.end method

.method public fillBytesRef()I
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    iget-object v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8WithHash([CIILorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method public getBytesRef()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 217
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 218
    .local v0, "code":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->hashCode([CII)I

    move-result v2

    add-int v0, v1, v2

    .line 219
    return v0
.end method

.method public final length()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    return v0
.end method

.method public reflectWith(Lorg/apache/lucene/util/AttributeReflector;)V
    .locals 3
    .param p1, "reflector"    # Lorg/apache/lucene/util/AttributeReflector;

    .prologue
    .line 274
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const-string v1, "term"

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 275
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->fillBytesRef()I

    .line 276
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    const-string v1, "bytes"

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/util/AttributeReflector;->reflect(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 277
    return-void
.end method

.method public final resizeBuffer(I)[C
    .locals 4
    .param p1, "newSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 53
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v1, v1

    if-ge v1, p1, :cond_0

    .line 56
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v0, v1, [C

    .line 57
    .local v0, "newCharBuffer":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    .line 60
    .end local v0    # "newCharBuffer":[C
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    return-object v1
.end method

.method public final setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 82
    return-object p0
.end method

.method public final setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .locals 3
    .param p1, "length"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "length "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " exceeds the size of the termBuffer ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    .line 76
    return-object p0
.end method

.method public final subSequence(II)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-gt p1, v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    if-le p2, v0, :cond_1

    .line 116
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 117
    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    sub-int v2, p2, p1

    invoke-direct {v0, v1, p1, v2}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 269
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termBuffer:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttributeImpl;->termLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
