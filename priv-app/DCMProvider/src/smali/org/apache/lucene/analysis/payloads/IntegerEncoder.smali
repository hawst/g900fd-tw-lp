.class public Lorg/apache/lucene/analysis/payloads/IntegerEncoder;
.super Lorg/apache/lucene/analysis/payloads/AbstractEncoder;
.source "IntegerEncoder.java"

# interfaces
.implements Lorg/apache/lucene/analysis/payloads/PayloadEncoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/analysis/payloads/AbstractEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public encode([CII)Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 33
    invoke-static {p1, p2, p3}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v1

    .line 34
    .local v1, "payload":I
    invoke-static {v1}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeInt(I)[B

    move-result-object v0

    .line 35
    .local v0, "bytes":[B
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, v0}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 36
    .local v2, "result":Lorg/apache/lucene/util/BytesRef;
    return-object v2
.end method
