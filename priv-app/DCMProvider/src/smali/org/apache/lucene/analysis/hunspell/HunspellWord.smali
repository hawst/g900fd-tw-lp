.class public Lorg/apache/lucene/analysis/hunspell/HunspellWord;
.super Ljava/lang/Object;
.source "HunspellWord.java"


# instance fields
.field private final flags:[C


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->flags:[C

    .line 34
    return-void
.end method

.method public constructor <init>([C)V
    .locals 0
    .param p1, "flags"    # [C

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->flags:[C

    .line 43
    return-void
.end method


# virtual methods
.method public getFlags()[C
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->flags:[C

    return-object v0
.end method

.method public hasFlag(C)Z
    .locals 1
    .param p1, "flag"    # C

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->flags:[C

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->flags:[C

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([CC)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
