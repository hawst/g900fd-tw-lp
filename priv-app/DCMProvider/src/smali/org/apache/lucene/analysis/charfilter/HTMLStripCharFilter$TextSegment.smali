.class Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;
.super Lorg/apache/lucene/analysis/util/OpenStringBuilder;
.source "HTMLStripCharFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextSegment"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field pos:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30866
    const-class v0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;I)V
    .locals 1
    .param p2, "size"    # I

    .prologue
    .line 30876
    iput-object p1, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->this$0:Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;

    .line 30877
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;-><init>(I)V

    .line 30868
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->pos:I

    .line 30878
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;[CI)V
    .locals 1
    .param p2, "buffer"    # [C
    .param p3, "length"    # I

    .prologue
    .line 30871
    iput-object p1, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->this$0:Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;

    .line 30872
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;-><init>([CI)V

    .line 30868
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->pos:I

    .line 30873
    return-void
.end method


# virtual methods
.method clear()V
    .locals 0

    .prologue
    .line 30882
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->reset()V

    .line 30883
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->restart()V

    .line 30884
    return-void
.end method

.method isRead()Z
    .locals 2

    .prologue
    .line 30899
    iget v0, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->pos:I

    iget v1, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->len:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method nextChar()I
    .locals 3

    .prologue
    .line 30893
    sget-boolean v0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->isRead()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Attempting to read past the end of a segment."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 30894
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->buf:[C

    iget v1, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->pos:I

    aget-char v0, v0, v1

    return v0
.end method

.method restart()V
    .locals 1

    .prologue
    .line 30888
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter$TextSegment;->pos:I

    .line 30889
    return-void
.end method
