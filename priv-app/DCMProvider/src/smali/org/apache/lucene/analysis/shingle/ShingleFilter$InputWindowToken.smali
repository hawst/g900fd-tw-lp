.class Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
.super Ljava/lang/Object;
.source "ShingleFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/shingle/ShingleFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InputWindowToken"
.end annotation


# instance fields
.field final attSource:Lorg/apache/lucene/util/AttributeSource;

.field isFiller:Z

.field final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field final synthetic this$0:Lorg/apache/lucene/analysis/shingle/ShingleFilter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;Lorg/apache/lucene/util/AttributeSource;)V
    .locals 1
    .param p2, "attSource"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 540
    iput-object p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->this$0:Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->isFiller:Z

    .line 541
    iput-object p2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->attSource:Lorg/apache/lucene/util/AttributeSource;

    .line 542
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p2, v0}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 543
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p2, v0}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 544
    return-void
.end method
