.class public Lorg/apache/lucene/analysis/payloads/IdentityEncoder;
.super Lorg/apache/lucene/analysis/payloads/AbstractEncoder;
.source "IdentityEncoder.java"

# interfaces
.implements Lorg/apache/lucene/analysis/payloads/PayloadEncoder;


# instance fields
.field protected charset:Ljava/nio/charset/Charset;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/analysis/payloads/AbstractEncoder;-><init>()V

    .line 31
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/IdentityEncoder;->charset:Ljava/nio/charset/Charset;

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/nio/charset/Charset;)V
    .locals 1
    .param p1, "charset"    # Ljava/nio/charset/Charset;

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/analysis/payloads/AbstractEncoder;-><init>()V

    .line 31
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/IdentityEncoder;->charset:Ljava/nio/charset/Charset;

    .line 37
    iput-object p1, p0, Lorg/apache/lucene/analysis/payloads/IdentityEncoder;->charset:Ljava/nio/charset/Charset;

    .line 38
    return-void
.end method


# virtual methods
.method public encode([CII)Lorg/apache/lucene/util/BytesRef;
    .locals 6
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 42
    iget-object v2, p0, Lorg/apache/lucene/analysis/payloads/IdentityEncoder;->charset:Ljava/nio/charset/Charset;

    invoke-static {p1, p2, p3}, Ljava/nio/CharBuffer;->wrap([CII)Ljava/nio/CharBuffer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/charset/Charset;->encode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 43
    .local v1, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    .line 49
    :goto_0
    return-object v2

    .line 47
    :cond_0
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    new-array v0, v2, [B

    .line 48
    .local v0, "b":[B
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 49
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, v0}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    goto :goto_0
.end method
