.class public final Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;
.super Lorg/apache/lucene/analysis/AnalyzerWrapper;
.source "PerFieldAnalyzerWrapper.java"


# instance fields
.field private final defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private final fieldAnalyzers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;-><init>(Lorg/apache/lucene/analysis/Analyzer;Ljava/util/Map;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;Ljava/util/Map;)V
    .locals 0
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "fieldAnalyzers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;>;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/AnalyzerWrapper;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 77
    if-eqz p2, :cond_0

    .end local p2    # "fieldAnalyzers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;>;"
    :goto_0
    iput-object p2, p0, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;->fieldAnalyzers:Ljava/util/Map;

    .line 78
    return-void

    .line 77
    .restart local p2    # "fieldAnalyzers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer;>;"
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method protected getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 82
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;->fieldAnalyzers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer;

    .line 83
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    if-eqz v0, :cond_0

    .end local v0    # "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    :goto_0
    return-object v0

    .restart local v0    # "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PerFieldAnalyzerWrapper("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;->fieldAnalyzers:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PerFieldAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected wrapComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "components"    # Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    .prologue
    .line 88
    return-object p2
.end method
