.class public Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;
.super Ljava/lang/Object;
.source "StemmerOverrideFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final charsSpare:Lorg/apache/lucene/util/CharsRef;

.field private final hash:Lorg/apache/lucene/util/BytesRefHash;

.field private final ignoreCase:Z

.field private final outputValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final spare:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;-><init>(Z)V

    .line 157
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "ignoreCase"    # Z

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->hash:Lorg/apache/lucene/util/BytesRefHash;

    .line 147
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->spare:Lorg/apache/lucene/util/BytesRef;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->outputValues:Ljava/util/ArrayList;

    .line 150
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->charsSpare:Lorg/apache/lucene/util/CharsRef;

    .line 164
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->ignoreCase:Z

    .line 165
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 6
    .param p1, "input"    # Ljava/lang/CharSequence;
    .param p2, "output"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 175
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 176
    .local v2, "length":I
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->ignoreCase:Z

    if-eqz v4, :cond_2

    .line 178
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->charsSpare:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/util/CharsRef;->grow(I)V

    .line 179
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->charsSpare:Lorg/apache/lucene/util/CharsRef;

    iget-object v0, v4, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 180
    .local v0, "buffer":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_1

    .line 185
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->spare:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v3, v2, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8([CIILorg/apache/lucene/util/BytesRef;)V

    .line 189
    .end local v0    # "buffer":[C
    .end local v1    # "i":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->hash:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->spare:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;)I

    move-result v4

    if-ltz v4, :cond_0

    .line 190
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->outputValues:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    const/4 v3, 0x1

    .line 193
    :cond_0
    return v3

    .line 183
    .restart local v0    # "buffer":[C
    .restart local v1    # "i":I
    :cond_1
    invoke-static {p1, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v4

    .line 182
    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    .line 183
    invoke-static {v4, v0, v1}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v4

    add-int/2addr v1, v4

    goto :goto_0

    .line 187
    .end local v0    # "buffer":[C
    .end local v1    # "i":I
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->spare:Lorg/apache/lucene/util/BytesRef;

    invoke-static {p1, v3, v2, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V

    goto :goto_1
.end method

.method public build()Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    invoke-static {}, Lorg/apache/lucene/util/fst/ByteSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/ByteSequenceOutputs;

    move-result-object v5

    .line 203
    .local v5, "outputs":Lorg/apache/lucene/util/fst/ByteSequenceOutputs;
    new-instance v0, Lorg/apache/lucene/util/fst/Builder;

    .line 204
    sget-object v8, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 203
    invoke-direct {v0, v8, v5}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 205
    .local v0, "builder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v8, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/BytesRefHash;->sort(Ljava/util/Comparator;)[I

    move-result-object v7

    .line 206
    .local v7, "sort":[I
    new-instance v4, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 207
    .local v4, "intsSpare":Lorg/apache/lucene/util/IntsRef;
    iget-object v8, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->hash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v8}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v6

    .line 208
    .local v6, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v6, :cond_0

    .line 214
    new-instance v8, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    move-result-object v9

    iget-boolean v10, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->ignoreCase:Z

    invoke-direct {v8, v9, v10}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;-><init>(Lorg/apache/lucene/util/fst/FST;Z)V

    return-object v8

    .line 209
    :cond_0
    aget v3, v7, v2

    .line 210
    .local v3, "id":I
    iget-object v8, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->hash:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->spare:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v8, v3, v9}, Lorg/apache/lucene/util/BytesRefHash;->get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 211
    .local v1, "bytesRef":Lorg/apache/lucene/util/BytesRef;
    invoke-static {v1, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF32(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)V

    .line 212
    new-instance v9, Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->outputValues:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-direct {v9, v8}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4, v9}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 208
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
