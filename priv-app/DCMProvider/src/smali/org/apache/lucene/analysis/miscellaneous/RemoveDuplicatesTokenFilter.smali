.class public final Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "RemoveDuplicatesTokenFilter.java"


# instance fields
.field private final posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final previous:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 34
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 35
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 38
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->previous:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 47
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 54
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v7

    if-nez v7, :cond_1

    move v5, v6

    .line 74
    :goto_0
    return v5

    .line 55
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    .line 56
    .local v4, "term":[C
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 57
    .local v1, "length":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v2

    .line 59
    .local v2, "posIncrement":I
    if-lez v2, :cond_2

    .line 60
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->previous:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/util/CharArraySet;->clear()V

    .line 63
    :cond_2
    if-nez v2, :cond_3

    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->previous:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v7, v4, v6, v1}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v7

    if-eqz v7, :cond_3

    move v0, v5

    .line 66
    .local v0, "duplicate":Z
    :goto_1
    new-array v3, v1, [C

    .line 67
    .local v3, "saved":[C
    invoke-static {v4, v6, v3, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->previous:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v7, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->add([C)Z

    .line 70
    if-nez v0, :cond_0

    goto :goto_0

    .end local v0    # "duplicate":Z
    .end local v3    # "saved":[C
    :cond_3
    move v0, v6

    .line 63
    goto :goto_1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 83
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/RemoveDuplicatesTokenFilter;->previous:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->clear()V

    .line 84
    return-void
.end method
