.class public Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;
.super Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;
.source "Analyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/Analyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PerFieldReuseStrategy"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 374
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public getReusableComponents(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 379
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;->getStoredValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 380
    .local v0, "componentsPerField":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setReusableComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)V
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "components"    # Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    .prologue
    .line 386
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;->getStoredValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 387
    .local v0, "componentsPerField":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;>;"
    if-nez v0, :cond_0

    .line 388
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "componentsPerField":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 389
    .restart local v0    # "componentsPerField":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;>;"
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;->setStoredValue(Ljava/lang/Object;)V

    .line 391
    :cond_0
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    return-void
.end method
