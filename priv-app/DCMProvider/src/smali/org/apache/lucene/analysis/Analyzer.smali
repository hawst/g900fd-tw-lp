.class public abstract Lorg/apache/lucene/analysis/Analyzer;
.super Ljava/lang/Object;
.source "Analyzer.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/Analyzer$GlobalReuseStrategy;,
        Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;,
        Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;,
        Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    }
.end annotation


# instance fields
.field private final reuseStrategy:Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lorg/apache/lucene/analysis/Analyzer$GlobalReuseStrategy;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/Analyzer$GlobalReuseStrategy;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/Analyzer;-><init>(Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;)V
    .locals 0
    .param p1, "reuseStrategy"    # Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lorg/apache/lucene/analysis/Analyzer;->reuseStrategy:Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;

    .line 93
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer;->reuseStrategy:Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->close()V

    .line 192
    return-void
.end method

.method protected abstract createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.end method

.method public getOffsetGap(Ljava/lang/String;)I
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 185
    const/4 v0, 0x1

    return v0
.end method

.method public getPositionIncrementGap(Ljava/lang/String;)I
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method protected initReader(Ljava/lang/String;Ljava/io/Reader;)Ljava/io/Reader;
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 153
    return-object p2
.end method

.method public final tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v2, p0, Lorg/apache/lucene/analysis/Analyzer;->reuseStrategy:Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->getReusableComponents(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    move-result-object v0

    .line 132
    .local v0, "components":Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->initReader(Ljava/lang/String;Ljava/io/Reader;)Ljava/io/Reader;

    move-result-object v1

    .line 133
    .local v1, "r":Ljava/io/Reader;
    if-nez v0, :cond_0

    .line 134
    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/analysis/Analyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    move-result-object v0

    .line 135
    iget-object v2, p0, Lorg/apache/lucene/analysis/Analyzer;->reuseStrategy:Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;

    invoke-virtual {v2, p1, v0}, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;->setReusableComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)V

    .line 139
    :goto_0
    invoke-virtual {v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenStream()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v2

    return-object v2

    .line 137
    :cond_0
    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->setReader(Ljava/io/Reader;)V

    goto :goto_0
.end method
