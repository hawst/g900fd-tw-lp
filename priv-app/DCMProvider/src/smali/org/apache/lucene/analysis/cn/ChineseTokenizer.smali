.class public final Lorg/apache/lucene/analysis/cn/ChineseTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "ChineseTokenizer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final IO_BUFFER_SIZE:I = 0x400

.field private static final MAX_WORD_LEN:I = 0xff


# instance fields
.field private final buffer:[C

.field private bufferIndex:I

.field private dataLen:I

.field private final ioBuffer:[C

.field private length:I

.field private offset:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private start:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "in"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 69
    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->dataLen:I

    .line 72
    const/16 v0, 0xff

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->buffer:[C

    .line 73
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->ioBuffer:[C

    .line 79
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 80
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 69
    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->dataLen:I

    .line 72
    const/16 v0, 0xff

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->buffer:[C

    .line 73
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->ioBuffer:[C

    .line 79
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 80
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 67
    return-void
.end method

.method private final flush()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 91
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    if-lez v1, :cond_0

    .line 94
    iget-object v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v2, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->buffer:[C

    iget v3, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    invoke-interface {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->start:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->correctOffset(I)I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->start:I

    iget v3, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    add-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->correctOffset(I)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 96
    const/4 v0, 0x1

    .line 99
    :cond_0
    return v0
.end method

.method private final push(C)V
    .locals 3
    .param p1, "c"    # C

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->start:I

    .line 85
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->buffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    aput-char v2, v0, v1

    .line 87
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 2

    .prologue
    .line 155
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->correctOffset(I)I

    move-result v0

    .line 156
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 157
    return-void
.end method

.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 104
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->clearAttributes()V

    .line 106
    iput v4, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    .line 107
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    iput v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->start:I

    .line 113
    :cond_0
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    .line 115
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    iget v2, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->dataLen:I

    if-lt v1, v2, :cond_1

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->input:Ljava/io/Reader;

    iget-object v2, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->ioBuffer:[C

    invoke-virtual {v1, v2}, Ljava/io/Reader;->read([C)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->dataLen:I

    .line 117
    iput v4, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    .line 120
    :cond_1
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->dataLen:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 121
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    .line 122
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->flush()Z

    move-result v1

    .line 146
    :goto_0
    return v1

    .line 124
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->ioBuffer:[C

    iget v2, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    aget-char v0, v1, v2

    .line 127
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->getType(C)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 146
    :pswitch_0
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    if-lez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->flush()Z

    move-result v1

    goto :goto_0

    .line 132
    :pswitch_1
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->push(C)V

    .line 133
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    const/16 v2, 0xff

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->flush()Z

    move-result v1

    goto :goto_0

    .line 137
    :pswitch_2
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->length:I

    if-lez v1, :cond_3

    .line 138
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    .line 139
    iget v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    .line 140
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->flush()Z

    move-result v1

    goto :goto_0

    .line 142
    :cond_3
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->push(C)V

    .line 143
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->flush()Z

    move-result v1

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 162
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->dataLen:I

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->bufferIndex:I

    iput v0, p0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;->offset:I

    .line 163
    return-void
.end method
