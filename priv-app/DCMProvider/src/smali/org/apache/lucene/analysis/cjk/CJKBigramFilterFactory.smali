.class public Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "CJKBigramFilterFactory.java"


# instance fields
.field final flags:I

.field final outputUnigrams:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 47
    const/4 v0, 0x0

    .line 48
    .local v0, "flags":I
    const-string v1, "han"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    or-int/lit8 v0, v0, 0x1

    .line 51
    :cond_0
    const-string v1, "hiragana"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52
    or-int/lit8 v0, v0, 0x2

    .line 54
    :cond_1
    const-string v1, "katakana"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 55
    or-int/lit8 v0, v0, 0x4

    .line 57
    :cond_2
    const-string v1, "hangul"

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 58
    or-int/lit8 v0, v0, 0x8

    .line 60
    :cond_3
    iput v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->flags:I

    .line 61
    const-string v1, "outputUnigrams"

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->outputUnigrams:Z

    .line 62
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 63
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown parameters: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 65
    :cond_4
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 69
    new-instance v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;

    iget v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->flags:I

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilterFactory;->outputUnigrams:Z

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V

    return-object v0
.end method
