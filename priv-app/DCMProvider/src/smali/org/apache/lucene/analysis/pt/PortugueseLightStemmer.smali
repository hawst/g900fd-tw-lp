.class public Lorg/apache/lucene/analysis/pt/PortugueseLightStemmer;
.super Ljava/lang/Object;
.source "PortugueseLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private normFeminine([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v2, 0x6f

    .line 166
    const/4 v0, 0x7

    if-le p2, v0, :cond_2

    .line 167
    const-string v0, "inha"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    const-string v0, "iaca"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    const-string v0, "eira"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    :cond_0
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    .line 204
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 174
    .restart local p2    # "len":I
    :cond_2
    const/4 v0, 0x6

    if-le p2, v0, :cond_1

    .line 175
    const-string v0, "osa"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 176
    const-string v0, "ica"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 177
    const-string v0, "ida"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 178
    const-string v0, "ada"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 179
    const-string v0, "iva"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 180
    const-string v0, "ama"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 181
    :cond_3
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    goto :goto_0

    .line 185
    :cond_4
    const-string v0, "ona"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 186
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0xe3

    aput-char v1, p1, v0

    .line 187
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 188
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 191
    :cond_5
    const-string v0, "ora"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 192
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 194
    :cond_6
    const-string v0, "esa"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 195
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0xea

    aput-char v1, p1, v0

    .line 196
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 199
    :cond_7
    const-string v0, "na"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    goto :goto_0
.end method

.method private removeSuffix([CI)I
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v4, 0x6f

    const/4 v3, 0x3

    const/16 v2, 0x6c

    const/4 v1, 0x4

    .line 113
    if-le p2, v1, :cond_0

    const-string v0, "es"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    .line 121
    :cond_0
    if-le p2, v3, :cond_1

    const-string v0, "ns"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x6d

    aput-char v1, p1, v0

    .line 123
    add-int/lit8 v0, p2, -0x1

    .line 162
    :goto_0
    return v0

    .line 118
    :sswitch_0
    add-int/lit8 v0, p2, -0x2

    goto :goto_0

    .line 126
    :cond_1
    if-le p2, v1, :cond_3

    const-string v0, "eis"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "\u00e9is"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    :cond_2
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x65

    aput-char v1, p1, v0

    .line 128
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 129
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 132
    :cond_3
    if-le p2, v1, :cond_4

    const-string v0, "ais"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 133
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 134
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 137
    :cond_4
    if-le p2, v1, :cond_5

    const-string/jumbo v0, "\u00f3is"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 138
    add-int/lit8 v0, p2, -0x3

    aput-char v4, p1, v0

    .line 139
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 140
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 143
    :cond_5
    if-le p2, v1, :cond_6

    const-string v0, "is"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 144
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    move v0, p2

    .line 145
    goto :goto_0

    .line 148
    :cond_6
    if-le p2, v3, :cond_8

    .line 149
    const-string/jumbo v0, "\u00f5es"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 150
    const-string/jumbo v0, "\u00e3es"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 151
    :cond_7
    add-int/lit8 p2, p2, -0x1

    .line 152
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0xe3

    aput-char v1, p1, v0

    .line 153
    add-int/lit8 v0, p2, -0x1

    aput-char v4, p1, v0

    move v0, p2

    .line 154
    goto :goto_0

    .line 157
    :cond_8
    const/4 v0, 0x6

    if-le p2, v0, :cond_9

    const-string v0, "mente"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 158
    add-int/lit8 v0, p2, -0x5

    goto/16 :goto_0

    .line 160
    :cond_9
    if-le p2, v3, :cond_a

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_a

    .line 161
    add-int/lit8 v0, p2, -0x1

    goto/16 :goto_0

    :cond_a
    move v0, p2

    .line 162
    goto/16 :goto_0

    .line 114
    :sswitch_data_0
    .sparse-switch
        0x6c -> :sswitch_0
        0x72 -> :sswitch_0
        0x73 -> :sswitch_0
        0x7a -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public stem([CI)I
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v4, 0x61

    const/4 v3, 0x4

    .line 67
    if-ge p2, v3, :cond_0

    move v1, p2

    .line 109
    .end local p2    # "len":I
    .local v1, "len":I
    :goto_0
    return v1

    .line 70
    .end local v1    # "len":I
    .restart local p2    # "len":I
    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/pt/PortugueseLightStemmer;->removeSuffix([CI)I

    move-result p2

    .line 72
    const/4 v2, 0x3

    if-le p2, v2, :cond_1

    add-int/lit8 v2, p2, -0x1

    aget-char v2, p1, v2

    if-ne v2, v4, :cond_1

    .line 73
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/pt/PortugueseLightStemmer;->normFeminine([CI)I

    move-result p2

    .line 75
    :cond_1
    if-le p2, v3, :cond_2

    .line 76
    add-int/lit8 v2, p2, -0x1

    aget-char v2, p1, v2

    sparse-switch v2, :sswitch_data_0

    .line 82
    :cond_2
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-lt v0, p2, :cond_3

    move v1, p2

    .line 109
    .end local p2    # "len":I
    .restart local v1    # "len":I
    goto :goto_0

    .line 79
    .end local v0    # "i":I
    .end local v1    # "len":I
    .restart local p2    # "len":I
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    .line 83
    .restart local v0    # "i":I
    :cond_3
    aget-char v2, p1, v0

    packed-switch v2, :pswitch_data_0

    .line 82
    :goto_3
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 88
    :pswitch_1
    aput-char v4, p1, v0

    goto :goto_3

    .line 93
    :pswitch_2
    const/16 v2, 0x6f

    aput-char v2, p1, v0

    goto :goto_3

    .line 97
    :pswitch_3
    const/16 v2, 0x65

    aput-char v2, p1, v0

    goto :goto_3

    .line 101
    :pswitch_4
    const/16 v2, 0x75

    aput-char v2, p1, v0

    goto :goto_3

    .line 105
    :pswitch_5
    const/16 v2, 0x69

    aput-char v2, p1, v0

    goto :goto_3

    .line 106
    :pswitch_6
    const/16 v2, 0x63

    aput-char v2, p1, v0

    goto :goto_3

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x6f -> :sswitch_0
    .end sparse-switch

    .line 83
    :pswitch_data_0
    .packed-switch 0xe0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
