.class public final Lorg/apache/lucene/analysis/ru/RussianAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "RussianAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/ru/RussianAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "russian_stop.txt"

.field private static final RUSSIAN_STOP_WORDS_30:[Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    const/16 v0, 0x65

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 62
    const-string/jumbo v2, "\u0430"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "\u0431\u0435\u0437"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "\u0431\u043e\u043b\u0435\u0435"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "\u0431\u044b"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "\u0431\u044b\u043b"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "\u0431\u044b\u043b\u0430"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "\u0431\u044b\u043b\u0438"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "\u0431\u044b\u043b\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "\u0431\u044b\u0442\u044c"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "\u0432"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 63
    const-string/jumbo v2, "\u0432\u0430\u043c"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "\u0432\u0430\u0441"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "\u0432\u0435\u0441\u044c"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "\u0432\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "\u0432\u043e\u0442"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "\u0432\u0441\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "\u0432\u0441\u0435\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "\u0432\u0441\u0435\u0445"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "\u0432\u044b"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "\u0433\u0434\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 64
    const-string/jumbo v2, "\u0434\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "\u0434\u0430\u0436\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "\u0434\u043b\u044f"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "\u0434\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "\u0435\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "\u0435\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "\u0435\u0439"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "\u0435\u044e"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "\u0435\u0441\u043b\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "\u0435\u0441\u0442\u044c"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 65
    const-string/jumbo v2, "\u0435\u0449\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "\u0436\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "\u0437\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "\u0437\u0434\u0435\u0441\u044c"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "\u0438\u0437"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "\u0438\u043b\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "\u0438\u043c"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string/jumbo v2, "\u0438\u0445"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string/jumbo v2, "\u043a"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string/jumbo v2, "\u043a\u0430\u043a"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 66
    const-string/jumbo v2, "\u043a\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string/jumbo v2, "\u043a\u043e\u0433\u0434\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string/jumbo v2, "\u043a\u0442\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string/jumbo v2, "\u043b\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string/jumbo v2, "\u043b\u0438\u0431\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string/jumbo v2, "\u043c\u043d\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string/jumbo v2, "\u043c\u043e\u0436\u0435\u0442"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string/jumbo v2, "\u043c\u044b"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string/jumbo v2, "\u043d\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string/jumbo v2, "\u043d\u0430\u0434\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 67
    const-string/jumbo v2, "\u043d\u0430\u0448"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string/jumbo v2, "\u043d\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string/jumbo v2, "\u043d\u0435\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string/jumbo v2, "\u043d\u0435\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string/jumbo v2, "\u043d\u0435\u0442"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string/jumbo v2, "\u043d\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string/jumbo v2, "\u043d\u0438\u0445"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string/jumbo v2, "\u043d\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string/jumbo v2, "\u043d\u0443"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string/jumbo v2, "\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string/jumbo v2, "\u043e\u0431"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    .line 68
    const-string/jumbo v2, "\u043e\u0434\u043d\u0430\u043a\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string/jumbo v2, "\u043e\u043d"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string/jumbo v2, "\u043e\u043d\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string/jumbo v2, "\u043e\u043d\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string/jumbo v2, "\u043e\u043d\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string/jumbo v2, "\u043e\u0442"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string/jumbo v2, "\u043e\u0447\u0435\u043d\u044c"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string/jumbo v2, "\u043f\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string/jumbo v2, "\u043f\u043e\u0434"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string/jumbo v2, "\u043f\u0440\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 69
    const-string/jumbo v2, "\u0441"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string/jumbo v2, "\u0441\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string/jumbo v2, "\u0442\u0430\u043a"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string/jumbo v2, "\u0442\u0430\u043a\u0436\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string/jumbo v2, "\u0442\u0430\u043a\u043e\u0439"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string/jumbo v2, "\u0442\u0430\u043c"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string/jumbo v2, "\u0442\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string/jumbo v2, "\u0442\u0435\u043c"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string/jumbo v2, "\u0442\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string/jumbo v2, "\u0442\u043e\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    .line 70
    const-string/jumbo v2, "\u0442\u043e\u0436\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string/jumbo v2, "\u0442\u043e\u0439"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string/jumbo v2, "\u0442\u043e\u043b\u044c\u043a\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string/jumbo v2, "\u0442\u043e\u043c"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string/jumbo v2, "\u0442\u044b"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string/jumbo v2, "\u0443"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string/jumbo v2, "\u0443\u0436\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string/jumbo v2, "\u0445\u043e\u0442\u044f"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string/jumbo v2, "\u0447\u0435\u0433\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string/jumbo v2, "\u0447\u0435\u0439"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    .line 71
    const-string/jumbo v2, "\u0447\u0435\u043c"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string/jumbo v2, "\u0447\u0442\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string/jumbo v2, "\u0447\u0442\u043e\u0431\u044b"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string/jumbo v2, "\u0447\u044c\u0435"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string/jumbo v2, "\u0447\u044c\u044f"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string/jumbo v2, "\u044d\u0442\u0430"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string/jumbo v2, "\u044d\u0442\u0438"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string/jumbo v2, "\u044d\u0442\u043e"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string/jumbo v2, "\u044f"

    aput-object v2, v0, v1

    .line 61
    sput-object v0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->RUSSIAN_STOP_WORDS_30:[Ljava/lang/String;

    .line 75
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 109
    .line 110
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 111
    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 112
    return-void

    .line 111
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET_30:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 123
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 124
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 137
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 138
    return-void
.end method

.method static synthetic access$0()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->RUSSIAN_STOP_WORDS_30:[Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 154
    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 155
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 156
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 157
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 158
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 159
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    .line 160
    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 161
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v3, Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct {v3}, Lorg/tartarus/snowball/ext/RussianStemmer;-><init>()V

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .line 162
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object v0, v1

    .line 170
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    return-object v3

    .line 164
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .end local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_1
    new-instance v2, Lorg/apache/lucene/analysis/ru/RussianLetterTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/ru/RussianLetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 165
    .restart local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 166
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v3, v0, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 167
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    .line 168
    iget-object v3, p0, Lorg/apache/lucene/analysis/ru/RussianAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 169
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_1
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v3, Lorg/tartarus/snowball/ext/RussianStemmer;

    invoke-direct {v3}, Lorg/tartarus/snowball/ext/RussianStemmer;-><init>()V

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .line 170
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_2
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_1
.end method
