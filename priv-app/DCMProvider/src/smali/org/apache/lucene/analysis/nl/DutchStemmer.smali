.class public Lorg/apache/lucene/analysis/nl/DutchStemmer;
.super Ljava/lang/Object;
.source "DutchStemmer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final locale:Ljava/util/Locale;


# instance fields
.field private _R1:I

.field private _R2:I

.field private _removedE:Z

.field private _stemDict:Ljava/util/Map;

.field private sb:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Ljava/util/Locale;

    const-string v1, "nl"

    const-string v2, "NL"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->locale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    .line 34
    return-void
.end method

.method private enEnding(Ljava/lang/StringBuilder;)Z
    .locals 9
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 83
    const/4 v7, 0x2

    new-array v1, v7, [Ljava/lang/String;

    const-string v7, "ene"

    aput-object v7, v1, v6

    const-string v7, "en"

    aput-object v7, v1, v5

    .line 84
    .local v1, "enend":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v7, v1

    if-lt v2, v7, :cond_0

    move v5, v6

    .line 97
    :goto_1
    return v5

    .line 85
    :cond_0
    aget-object v0, v1, v2

    .line 86
    .local v0, "end":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    sub-int v3, v7, v8

    .line 88
    .local v3, "index":I
    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 89
    iget v7, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    if-lt v3, v7, :cond_1

    .line 90
    add-int/lit8 v7, v3, -0x1

    invoke-direct {p0, p1, v7}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isValidEnEnding(Ljava/lang/StringBuilder;I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 92
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {p1, v3, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 93
    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->unDouble(Ljava/lang/StringBuilder;I)V

    goto :goto_1

    .line 84
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private getRIndex(Ljava/lang/StringBuilder;I)I
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "start"    # I

    .prologue
    .line 346
    if-nez p2, :cond_0

    .line 347
    const/4 p2, 0x1

    .line 348
    :cond_0
    move v0, p2

    .line 349
    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 355
    add-int/lit8 v1, v0, 0x1

    :goto_1
    return v1

    .line 351
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v1

    if-nez v1, :cond_2

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 352
    add-int/lit8 v1, v0, 0x1

    goto :goto_1

    .line 349
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isStemmable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 261
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 264
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 262
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    .line 261
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isValidEnEnding(Ljava/lang/StringBuilder;I)Z
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 323
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 324
    .local v0, "c":C
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v1

    .line 326
    :cond_1
    const/4 v2, 0x3

    if-lt v0, v2, :cond_0

    .line 329
    const/16 v2, 0x6d

    if-ne v0, v2, :cond_2

    add-int/lit8 v2, p2, -0x2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x67

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x65

    if-eq v2, v3, :cond_0

    .line 331
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isValidSEnding(Ljava/lang/StringBuilder;I)Z
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "index"    # I

    .prologue
    .line 312
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 313
    .local v0, "c":C
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x6a

    if-ne v0, v1, :cond_1

    .line 314
    :cond_0
    const/4 v1, 0x0

    .line 315
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isVowel(C)Z
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 393
    sparse-switch p1, :sswitch_data_0

    .line 405
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 402
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 393
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x79 -> :sswitch_0
        0xe8 -> :sswitch_0
    .end sparse-switch
.end method

.method private reStoreYandI(Ljava/lang/StringBuilder;)V
    .locals 5
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v4, 0x0

    .line 387
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "tmp":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {p1, v4, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 389
    const-string v1, "I"

    const-string v2, "i"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Y"

    const-string/jumbo v3, "y"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    return-void
.end method

.method private step1(Ljava/lang/StringBuilder;)V
    .locals 8
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 102
    iget v3, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    sub-int v1, v3, v4

    .line 109
    .local v1, "lengthR1":I
    const-string v3, "heden"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 110
    iget v3, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    iget v4, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    add-int/2addr v4, v1

    iget v5, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    iget v6, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    add-int/2addr v6, v1

    invoke-virtual {p1, v5, v6}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "heden"

    const-string v7, "heid"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v3, v4, v5}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 114
    :cond_2
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->enEnding(Ljava/lang/StringBuilder;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 117
    const-string v3, "se"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 118
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x2

    .local v0, "index":I
    iget v3, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    if-lt v0, v3, :cond_3

    .line 119
    add-int/lit8 v3, v0, -0x1

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isValidSEnding(Ljava/lang/StringBuilder;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 121
    add-int/lit8 v3, v0, 0x2

    invoke-virtual {p1, v0, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 124
    .end local v0    # "index":I
    :cond_3
    const-string v3, "s"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 125
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .restart local v0    # "index":I
    iget v3, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    if-lt v0, v3, :cond_0

    .line 126
    add-int/lit8 v3, v0, -0x1

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isValidSEnding(Ljava/lang/StringBuilder;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private step2(Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 138
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_removedE:Z

    .line 139
    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "s":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 143
    .local v0, "index":I
    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    if-lt v0, v2, :cond_0

    .line 144
    const-string v2, "e"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 145
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 146
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 147
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->unDouble(Ljava/lang/StringBuilder;)V

    .line 148
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_removedE:Z

    goto :goto_0
.end method

.method private step3a(Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 158
    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 161
    .local v1, "s":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x4

    .line 162
    .local v0, "index":I
    const-string v2, "heid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v0, v2, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x63

    if-eq v2, v3, :cond_0

    .line 163
    add-int/lit8 v2, v0, 0x4

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 164
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->enEnding(Ljava/lang/StringBuilder;)Z

    goto :goto_0
.end method

.method private step3b(Ljava/lang/StringBuilder;)V
    .locals 7
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v6, 0x65

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 183
    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lt v2, v5, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 186
    .local v1, "s":Ljava/lang/String;
    const/4 v0, 0x0

    .line 188
    .local v0, "index":I
    const-string v2, "end"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 189
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x3

    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v0, v2, :cond_6

    .line 190
    add-int/lit8 v2, v0, 0x3

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 191
    add-int/lit8 v2, v0, -0x2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v5, 0x69

    if-ne v2, v5, :cond_5

    .line 192
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v5, 0x67

    if-ne v2, v5, :cond_5

    .line 193
    add-int/lit8 v2, v0, -0x3

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-eq v2, v6, :cond_3

    move v2, v3

    :goto_1
    add-int/lit8 v5, v0, -0x2

    iget v6, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v5, v6, :cond_4

    :goto_2
    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    .line 194
    add-int/lit8 v0, v0, -0x2

    .line 195
    add-int/lit8 v2, v0, 0x2

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    move v2, v4

    .line 193
    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2

    .line 198
    :cond_5
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->unDouble(Ljava/lang/StringBuilder;I)V

    goto :goto_0

    .line 202
    :cond_6
    const-string v2, "ig"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 203
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x2

    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v0, v2, :cond_7

    .line 205
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-eq v2, v6, :cond_0

    .line 206
    add-int/lit8 v2, v0, 0x2

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 209
    :cond_7
    const-string v2, "lijk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 210
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x4

    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v0, v2, :cond_8

    .line 212
    add-int/lit8 v2, v0, 0x4

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 213
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->step2(Ljava/lang/StringBuilder;)V

    goto/16 :goto_0

    .line 216
    :cond_8
    const-string v2, "baar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 217
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x4

    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v0, v2, :cond_9

    .line 219
    add-int/lit8 v2, v0, 0x4

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 222
    :cond_9
    const-string v2, "bar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x3

    iget v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    if-lt v0, v2, :cond_0

    .line 225
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_removedE:Z

    if-eqz v2, :cond_0

    .line 226
    add-int/lit8 v2, v0, 0x3

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method

.method private step4(Ljava/lang/StringBuilder;)V
    .locals 7
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 238
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    const/4 v6, 0x4

    if-ge v5, v6, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 241
    .local v2, "end":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 242
    .local v0, "c":C
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 243
    .local v3, "v1":C
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 244
    .local v4, "v2":C
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 245
    .local v1, "d":C
    if-ne v3, v4, :cond_0

    .line 246
    const/16 v5, 0x49

    if-eq v1, v5, :cond_0

    .line 247
    const/16 v5, 0x69

    if-eq v3, v5, :cond_0

    .line 248
    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 249
    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v5

    if-nez v5, :cond_0

    .line 250
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v5

    if-nez v5, :cond_0

    .line 251
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p1, v5, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private storeYandI(Ljava/lang/StringBuilder;)V
    .locals 6
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v5, 0x79

    const/4 v4, 0x0

    const/16 v3, 0x59

    .line 359
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_0

    .line 360
    invoke-virtual {p1, v4, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 362
    :cond_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 364
    .local v1, "last":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_2

    .line 382
    if-lez v1, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_1

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 383
    invoke-virtual {p1, v1, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 384
    :cond_1
    return-void

    .line 365
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 364
    :cond_3
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 368
    :sswitch_0
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 369
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 371
    const/16 v2, 0x49

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 376
    :sswitch_1
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 377
    invoke-virtual {p1, v0, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 365
    nop

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_0
        0x79 -> :sswitch_1
    .end sparse-switch
.end method

.method private substitute(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 271
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 305
    return-void

    .line 272
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 271
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 276
    :sswitch_0
    const/16 v1, 0x61

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 282
    :sswitch_1
    const/16 v1, 0x65

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 288
    :sswitch_2
    const/16 v1, 0x75

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 294
    :sswitch_3
    const/16 v1, 0x69

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 300
    :sswitch_4
    const/16 v1, 0x6f

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 272
    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_3
        0xe1 -> :sswitch_0
        0xe4 -> :sswitch_0
        0xe9 -> :sswitch_1
        0xeb -> :sswitch_1
        0xef -> :sswitch_3
        0xf3 -> :sswitch_4
        0xf6 -> :sswitch_4
        0xfa -> :sswitch_2
        0xfc -> :sswitch_2
    .end sparse-switch
.end method

.method private unDouble(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    .line 335
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->unDouble(Ljava/lang/StringBuilder;I)V

    .line 336
    return-void
.end method

.method private unDouble(Ljava/lang/StringBuilder;I)V
    .locals 2
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "endIndex"    # I

    .prologue
    .line 339
    const/4 v1, 0x0

    invoke-virtual {p1, v1, p2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 340
    .local v0, "s":Ljava/lang/String;
    const-string v1, "kk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "tt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "nn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ff"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 341
    :cond_0
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1, v1, p2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 343
    :cond_1
    return-void
.end method


# virtual methods
.method setStemDictionary(Ljava/util/Map;)V
    .locals 0
    .param p1, "dict"    # Ljava/util/Map;

    .prologue
    .line 409
    iput-object p1, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_stemDict:Ljava/util/Map;

    .line 410
    return-void
.end method

.method public stem(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 55
    sget-object v0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 56
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->isStemmable(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    .end local p1    # "term":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 58
    .restart local p1    # "term":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_stemDict:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_stemDict:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_stemDict:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_stemDict:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object p1, v0

    goto :goto_0

    .line 62
    :cond_1
    const/4 p1, 0x0

    goto :goto_0

    .line 65
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->substitute(Ljava/lang/StringBuilder;)V

    .line 69
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->storeYandI(Ljava/lang/StringBuilder;)V

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->getRIndex(Ljava/lang/StringBuilder;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    .line 71
    const/4 v0, 0x3

    iget v1, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->step1(Ljava/lang/StringBuilder;)V

    .line 73
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->step2(Ljava/lang/StringBuilder;)V

    .line 74
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R1:I

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->getRIndex(Ljava/lang/StringBuilder;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->_R2:I

    .line 75
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->step3a(Ljava/lang/StringBuilder;)V

    .line 76
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->step3b(Ljava/lang/StringBuilder;)V

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->step4(Ljava/lang/StringBuilder;)V

    .line 78
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->reStoreYandI(Ljava/lang/StringBuilder;)V

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
