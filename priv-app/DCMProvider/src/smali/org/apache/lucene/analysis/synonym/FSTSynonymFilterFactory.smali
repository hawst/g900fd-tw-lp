.class final Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "FSTSynonymFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final expand:Z

.field private final format:Ljava/lang/String;

.field private final ignoreCase:Z

.field private map:Lorg/apache/lucene/analysis/synonym/SynonymMap;

.field private final synonyms:Ljava/lang/String;

.field private final tokenizerFactory:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 62
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->ignoreCase:Z

    .line 63
    const-string v0, "tokenizerFactory"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->tokenizerFactory:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->tokenizerFactory:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->assureMatchVersion()V

    .line 67
    :cond_0
    const-string v0, "synonyms"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    .line 68
    const-string v0, "format"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->format:Ljava/lang/String;

    .line 69
    const-string v0, "expand"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->expand:Z

    .line 70
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;)Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->ignoreCase:Z

    return v0
.end method

.method private loadSolrSynonyms(Lorg/apache/lucene/analysis/util/ResourceLoader;ZLorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/synonym/SynonymMap;
    .locals 8
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .param p2, "dedup"    # Z
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 114
    const-string v5, "UTF-8"

    invoke-static {v5}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v5

    .line 115
    sget-object v6, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v5, v6}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v5

    .line 116
    sget-object v6, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v5, v6}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 118
    .local v0, "decoder":Ljava/nio/charset/CharsetDecoder;
    new-instance v3, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->expand:Z

    invoke-direct {v3, p2, v5, p3}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;-><init>(ZZLorg/apache/lucene/analysis/Analyzer;)V

    .line 119
    .local v3, "parser":Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v4, "synonymFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 121
    invoke-virtual {v0}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 122
    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-interface {p1, v6}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-virtual {v3, v5}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->add(Ljava/io/Reader;)V

    .line 130
    :cond_0
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->build()Lorg/apache/lucene/analysis/synonym/SynonymMap;

    move-result-object v5

    return-object v5

    .line 124
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 125
    .local v2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 126
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 127
    new-instance v6, Ljava/io/InputStreamReader;

    invoke-interface {p1, v1}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-virtual {v3, v6}, Lorg/apache/lucene/analysis/synonym/SolrSynonymParser;->add(Ljava/io/Reader;)V

    goto :goto_0
.end method

.method private loadTokenizerFactory(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/TokenizerFactory;
    .locals 8
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .param p2, "cname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 159
    .local v1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "luceneMatchVersion"

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->getLuceneMatchVersion()Lorg/apache/lucene/util/Version;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/util/Version;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    const-class v5, Lorg/apache/lucene/analysis/util/TokenizerFactory;

    invoke-interface {p1, p2, v5}, Lorg/apache/lucene/analysis/util/ResourceLoader;->findClass(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v2

    .line 162
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/analysis/util/TokenizerFactory;>;"
    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/util/Map;

    aput-object v7, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/analysis/util/TokenizerFactory;

    .line 163
    .local v4, "tokFactory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    instance-of v5, v4, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;

    if-eqz v5, :cond_0

    .line 164
    move-object v0, v4

    check-cast v0, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;

    move-object v5, v0

    invoke-interface {v5, p1}, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;->inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :cond_0
    return-object v4

    .line 167
    .end local v4    # "tokFactory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    :catch_0
    move-exception v3

    .line 168
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method private loadWordnetSynonyms(Lorg/apache/lucene/analysis/util/ResourceLoader;ZLorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/synonym/SynonymMap;
    .locals 8
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .param p2, "dedup"    # Z
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 137
    const-string v5, "UTF-8"

    invoke-static {v5}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v5

    .line 138
    sget-object v6, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v5, v6}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v5

    .line 139
    sget-object v6, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v5, v6}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 141
    .local v0, "decoder":Ljava/nio/charset/CharsetDecoder;
    new-instance v3, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->expand:Z

    invoke-direct {v3, p2, v5, p3}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;-><init>(ZZLorg/apache/lucene/analysis/Analyzer;)V

    .line 142
    .local v3, "parser":Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 143
    .local v4, "synonymFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 144
    invoke-virtual {v0}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 145
    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-interface {p1, v6}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-virtual {v3, v5}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->add(Ljava/io/Reader;)V

    .line 153
    :cond_0
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->build()Lorg/apache/lucene/analysis/synonym/SynonymMap;

    move-result-object v5

    return-object v5

    .line 147
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 148
    .local v2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 149
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/nio/charset/CharsetDecoder;->reset()Ljava/nio/charset/CharsetDecoder;

    .line 150
    new-instance v6, Ljava/io/InputStreamReader;

    invoke-interface {p1, v1}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-virtual {v3, v6}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->add(Ljava/io/Reader;)V

    goto :goto_0
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->map:Lorg/apache/lucene/analysis/synonym/SynonymMap;

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap;->fst:Lorg/apache/lucene/util/fst/FST;

    if-nez v0, :cond_0

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    return-object p1

    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->map:Lorg/apache/lucene/analysis/synonym/SynonymMap;

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->ignoreCase:Z

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/synonym/SynonymMap;Z)V

    move-object p1, v0

    goto :goto_0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 6
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->tokenizerFactory:Ljava/lang/String;

    if-nez v3, :cond_1

    const/4 v2, 0x0

    .line 86
    .local v2, "factory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    :goto_0
    new-instance v0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;

    invoke-direct {v0, p0, v2}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;-><init>(Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;Lorg/apache/lucene/analysis/util/TokenizerFactory;)V

    .line 96
    .local v0, "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->format:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->format:Ljava/lang/String;

    const-string v4, "solr"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 98
    :cond_0
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3, v0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->loadSolrSynonyms(Lorg/apache/lucene/analysis/util/ResourceLoader;ZLorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/synonym/SynonymMap;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->map:Lorg/apache/lucene/analysis/synonym/SynonymMap;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_1
    return-void

    .line 84
    .end local v0    # "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    .end local v2    # "factory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->tokenizerFactory:Ljava/lang/String;

    invoke-direct {p0, p1, v3}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->loadTokenizerFactory(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/TokenizerFactory;

    move-result-object v2

    goto :goto_0

    .line 99
    .restart local v0    # "analyzer":Lorg/apache/lucene/analysis/Analyzer;
    .restart local v2    # "factory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    :cond_2
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->format:Ljava/lang/String;

    const-string/jumbo v4, "wordnet"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 100
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3, v0}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->loadWordnetSynonyms(Lorg/apache/lucene/analysis/util/ResourceLoader;ZLorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/synonym/SynonymMap;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->map:Lorg/apache/lucene/analysis/synonym/SynonymMap;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 105
    :catch_0
    move-exception v1

    .line 106
    .local v1, "e":Ljava/text/ParseException;
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Error parsing synonyms file:"

    invoke-direct {v3, v4, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 103
    .end local v1    # "e":Ljava/text/ParseException;
    :cond_3
    :try_start_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unrecognized synonyms format: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->format:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0
.end method
