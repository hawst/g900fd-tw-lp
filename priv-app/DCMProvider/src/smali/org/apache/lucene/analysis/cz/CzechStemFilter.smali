.class public final Lorg/apache/lucene/analysis/cz/CzechStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CzechStemFilter.java"


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final stemmer:Lorg/apache/lucene/analysis/cz/CzechStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 40
    new-instance v0, Lorg/apache/lucene/analysis/cz/CzechStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/cz/CzechStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->stemmer:Lorg/apache/lucene/analysis/cz/CzechStemmer;

    .line 41
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 46
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v1, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    iget-object v1, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    iget-object v1, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->stemmer:Lorg/apache/lucene/analysis/cz/CzechStemmer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/cz/CzechStemmer;->stem([CI)I

    move-result v0

    .line 53
    .local v0, "newlen":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/cz/CzechStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 55
    .end local v0    # "newlen":I
    :cond_0
    const/4 v1, 0x1

    .line 57
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
