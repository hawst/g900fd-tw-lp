.class public final Lorg/apache/lucene/analysis/core/KeywordAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "KeywordAnalyzer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 34
    new-instance v0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v1, Lorg/apache/lucene/analysis/core/KeywordTokenizer;

    invoke-direct {v1, p2}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;-><init>(Ljava/io/Reader;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;)V

    return-object v0
.end method
