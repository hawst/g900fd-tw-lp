.class public Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSetExceptions;
.super Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
.source "RSLPStemmerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "RuleWithSetExceptions"
.end annotation


# instance fields
.field protected final exceptions:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V
    .locals 5
    .param p1, "suffix"    # Ljava/lang/String;
    .param p2, "min"    # I
    .param p3, "replacement"    # Ljava/lang/String;
    .param p4, "exceptions"    # [Ljava/lang/String;

    .prologue
    .line 132
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 133
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p4

    if-lt v0, v1, :cond_0

    .line 137
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    .line 138
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    .line 137
    iput-object v1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSetExceptions;->exceptions:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 139
    return-void

    .line 134
    :cond_0
    aget-object v1, p4, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 135
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "useless exception \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p4, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' does not end with \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 133
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public matches([CI)Z
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v0, 0x0

    .line 143
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->matches([CI)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSetExceptions;->exceptions:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v1, p1, v0, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
