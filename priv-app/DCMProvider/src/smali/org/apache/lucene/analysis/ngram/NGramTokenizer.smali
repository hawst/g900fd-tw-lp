.class public final Lorg/apache/lucene/analysis/ngram/NGramTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "NGramTokenizer.java"


# static fields
.field public static final DEFAULT_MAX_NGRAM_SIZE:I = 0x2

.field public static final DEFAULT_MIN_NGRAM_SIZE:I = 0x1


# instance fields
.field private charsRead:I

.field private gramSize:I

.field private inLen:I

.field private inStr:Ljava/lang/String;

.field private maxGram:I

.field private minGram:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private pos:I

.field private started:Z

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 73
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;-><init>(Ljava/io/Reader;II)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;II)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "minGram"    # I
    .param p3, "maxGram"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 43
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 53
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->init(II)V

    .line 54
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;II)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "minGram"    # I
    .param p4, "maxGram"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 43
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 65
    invoke-direct {p0, p3, p4}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->init(II)V

    .line 66
    return-void
.end method

.method private init(II)V
    .locals 2
    .param p1, "minGram"    # I
    .param p2, "maxGram"    # I

    .prologue
    .line 77
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    if-le p1, p2, :cond_1

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must not be greater than maxGram"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    iput p1, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->minGram:I

    .line 84
    iput p2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->maxGram:I

    .line 85
    return-void
.end method


# virtual methods
.method public end()V
    .locals 2

    .prologue
    .line 144
    iget v1, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->correctOffset(I)I

    move-result v0

    .line 145
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 146
    return-void
.end method

.method public incrementToken()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x400

    const/4 v5, 0x1

    const/4 v10, -0x1

    const/4 v4, 0x0

    .line 90
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->clearAttributes()V

    .line 91
    iget-boolean v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->started:Z

    if-nez v6, :cond_5

    .line 92
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->started:Z

    .line 93
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->minGram:I

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    .line 94
    new-array v0, v11, [C

    .line 95
    .local v0, "chars":[C
    iput v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    .line 97
    :goto_0
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    array-length v7, v0

    if-lt v6, v7, :cond_3

    .line 104
    :cond_0
    new-instance v6, Ljava/lang/String;

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    invoke-direct {v6, v0, v4, v7}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inStr:Ljava/lang/String;

    .line 106
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    array-length v7, v0

    if-ne v6, v7, :cond_1

    .line 109
    new-array v3, v11, [C

    .line 111
    .local v3, "throwaway":[C
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->input:Ljava/io/Reader;

    array-length v7, v3

    invoke-virtual {v6, v3, v4, v7}, Ljava/io/Reader;->read([CII)I

    move-result v1

    .line 112
    .local v1, "inc":I
    if-ne v1, v10, :cond_4

    .line 119
    .end local v1    # "inc":I
    .end local v3    # "throwaway":[C
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inStr:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inLen:I

    .line 120
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inLen:I

    if-nez v6, :cond_5

    .line 138
    .end local v0    # "chars":[C
    :cond_2
    :goto_2
    return v4

    .line 98
    .restart local v0    # "chars":[C
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->input:Ljava/io/Reader;

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    array-length v8, v0

    iget v9, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    sub-int/2addr v8, v9

    invoke-virtual {v6, v0, v7, v8}, Ljava/io/Reader;->read([CII)I

    move-result v1

    .line 99
    .restart local v1    # "inc":I
    if-eq v1, v10, :cond_0

    .line 102
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    add-int/2addr v6, v1

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    goto :goto_0

    .line 115
    .restart local v3    # "throwaway":[C
    :cond_4
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    add-int/2addr v6, v1

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->charsRead:I

    goto :goto_1

    .line 125
    .end local v0    # "chars":[C
    .end local v1    # "inc":I
    .end local v3    # "throwaway":[C
    :cond_5
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    add-int/2addr v6, v7

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inLen:I

    if-le v6, v7, :cond_6

    .line 126
    iput v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    .line 127
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    .line 128
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->maxGram:I

    if-gt v6, v7, :cond_2

    .line 130
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    add-int/2addr v6, v7

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inLen:I

    if-gt v6, v7, :cond_2

    .line 134
    :cond_6
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    .line 135
    .local v2, "oldPos":I
    iget v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    .line 136
    iget-object v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->inStr:Ljava/lang/String;

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    add-int/2addr v7, v2

    invoke-interface {v4, v6, v2, v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 137
    iget-object v4, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->correctOffset(I)I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->gramSize:I

    add-int/2addr v7, v2

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->correctOffset(I)I

    move-result v7

    invoke-interface {v4, v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    move v4, v5

    .line 138
    goto :goto_2
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 150
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 151
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->started:Z

    .line 152
    iput v0, p0, Lorg/apache/lucene/analysis/ngram/NGramTokenizer;->pos:I

    .line 153
    return-void
.end method
