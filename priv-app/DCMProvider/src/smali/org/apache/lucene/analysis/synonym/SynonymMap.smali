.class public Lorg/apache/lucene/analysis/synonym/SynonymMap;
.super Ljava/lang/Object;
.source "SynonymMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;
    }
.end annotation


# static fields
.field public static final WORD_SEPARATOR:C


# instance fields
.field public final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field public final maxHorizontalContext:I

.field public final words:Lorg/apache/lucene/util/BytesRefHash;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/BytesRefHash;I)V
    .locals 0
    .param p2, "words"    # Lorg/apache/lucene/util/BytesRefHash;
    .param p3, "maxHorizontalContext"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Lorg/apache/lucene/util/BytesRefHash;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 58
    iput-object p2, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap;->words:Lorg/apache/lucene/util/BytesRefHash;

    .line 59
    iput p3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap;->maxHorizontalContext:I

    .line 60
    return-void
.end method
