.class public final Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "KeywordRepeatFilter.java"


# instance fields
.field private final keywordAttribute:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final posIncAttr:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private state:Lorg/apache/lucene/util/AttributeSource$State;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 36
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->keywordAttribute:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->posIncAttr:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 45
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 49
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->state:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v2, :cond_0

    .line 50
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->state:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 51
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->posIncAttr:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 52
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->keywordAttribute:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->setKeyword(Z)V

    .line 53
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->state:Lorg/apache/lucene/util/AttributeSource$State;

    .line 61
    :goto_0
    return v0

    .line 56
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->state:Lorg/apache/lucene/util/AttributeSource$State;

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->keywordAttribute:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->setKeyword(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 61
    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordRepeatFilter;->state:Lorg/apache/lucene/util/AttributeSource$State;

    .line 68
    return-void
.end method
