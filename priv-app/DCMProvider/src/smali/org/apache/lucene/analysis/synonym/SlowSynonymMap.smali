.class Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
.super Ljava/lang/Object;
.source "SlowSynonymMap.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final IGNORE_CASE:I = 0x2

.field static final INCLUDE_ORIG:I = 0x1


# instance fields
.field flags:I

.field public submap:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;",
            ">;"
        }
    .end annotation
.end field

.field public synonyms:[Lorg/apache/lucene/analysis/Token;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "ignoreCase"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    if-eqz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    .line 43
    :cond_0
    return-void
.end method

.method public static makeTokens(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/Token;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 107
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/Token;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 112
    return-object v1

    .line 107
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 109
    .local v2, "str":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/analysis/Token;

    const-string v4, "SYNONYM"

    invoke-direct {v0, v2, v5, v5, v4}, Lorg/apache/lucene/analysis/Token;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 110
    .local v0, "newTok":Lorg/apache/lucene/analysis/Token;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static mergeTokens(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/Token;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/Token;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/Token;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "lst1":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/Token;>;"
    .local p1, "lst2":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/Token;>;"
    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 125
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/analysis/Token;>;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_3

    .line 127
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 128
    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {v5, p0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 159
    :cond_2
    return-object v5

    .line 132
    :cond_3
    const/4 v2, 0x0

    .line 133
    .local v2, "pos":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 134
    .local v0, "iter1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/analysis/Token;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 135
    .local v1, "iter2":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/analysis/Token;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/analysis/Token;

    move-object v7, v9

    .line 136
    .local v7, "tok1":Lorg/apache/lucene/analysis/Token;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/analysis/Token;

    move-object v8, v9

    .line 137
    .local v8, "tok2":Lorg/apache/lucene/analysis/Token;
    :goto_1
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->getPositionIncrement()I

    move-result v3

    .line 138
    .local v3, "pos1":I
    :goto_2
    if-eqz v8, :cond_b

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->getPositionIncrement()I

    move-result v4

    .line 139
    .local v4, "pos2":I
    :cond_4
    :goto_3
    if-nez v7, :cond_5

    if-eqz v8, :cond_2

    .line 140
    :cond_5
    :goto_4
    if-eqz v7, :cond_6

    if-le v3, v4, :cond_c

    if-eqz v8, :cond_c

    .line 149
    :cond_6
    :goto_5
    if-eqz v8, :cond_4

    if-le v4, v3, :cond_7

    if-nez v7, :cond_4

    .line 150
    :cond_7
    new-instance v6, Lorg/apache/lucene/analysis/Token;

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->startOffset()I

    move-result v9

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v12

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->type()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v9, v12, v13}, Lorg/apache/lucene/analysis/Token;-><init>(IILjava/lang/String;)V

    .line 151
    .local v6, "tok":Lorg/apache/lucene/analysis/Token;
    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->buffer()[C

    move-result-object v9

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->length()I

    move-result v12

    invoke-virtual {v6, v9, v11, v12}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 152
    sub-int v9, v4, v2

    invoke-virtual {v6, v9}, Lorg/apache/lucene/analysis/Token;->setPositionIncrement(I)V

    .line 153
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    move v2, v4

    .line 155
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/analysis/Token;

    move-object v8, v9

    .line 156
    :goto_6
    if-eqz v8, :cond_10

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/Token;->getPositionIncrement()I

    move-result v9

    :goto_7
    add-int/2addr v4, v9

    goto :goto_5

    .end local v3    # "pos1":I
    .end local v4    # "pos2":I
    .end local v6    # "tok":Lorg/apache/lucene/analysis/Token;
    .end local v7    # "tok1":Lorg/apache/lucene/analysis/Token;
    .end local v8    # "tok2":Lorg/apache/lucene/analysis/Token;
    :cond_8
    move-object v7, v10

    .line 135
    goto :goto_0

    .restart local v7    # "tok1":Lorg/apache/lucene/analysis/Token;
    :cond_9
    move-object v8, v10

    .line 136
    goto :goto_1

    .restart local v8    # "tok2":Lorg/apache/lucene/analysis/Token;
    :cond_a
    move v3, v11

    .line 137
    goto :goto_2

    .restart local v3    # "pos1":I
    :cond_b
    move v4, v11

    .line 138
    goto :goto_3

    .line 141
    .restart local v4    # "pos2":I
    :cond_c
    new-instance v6, Lorg/apache/lucene/analysis/Token;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->startOffset()I

    move-result v9

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v12

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->type()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v9, v12, v13}, Lorg/apache/lucene/analysis/Token;-><init>(IILjava/lang/String;)V

    .line 142
    .restart local v6    # "tok":Lorg/apache/lucene/analysis/Token;
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->buffer()[C

    move-result-object v9

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->length()I

    move-result v12

    invoke-virtual {v6, v9, v11, v12}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 143
    sub-int v9, v3, v2

    invoke-virtual {v6, v9}, Lorg/apache/lucene/analysis/Token;->setPositionIncrement(I)V

    .line 144
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    move v2, v3

    .line 146
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/analysis/Token;

    move-object v7, v9

    .line 147
    :goto_8
    if-eqz v7, :cond_e

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/Token;->getPositionIncrement()I

    move-result v9

    :goto_9
    add-int/2addr v3, v9

    goto/16 :goto_4

    :cond_d
    move-object v7, v10

    .line 146
    goto :goto_8

    :cond_e
    move v9, v11

    .line 147
    goto :goto_9

    :cond_f
    move-object v8, v10

    .line 155
    goto :goto_6

    :cond_10
    move v9, v11

    .line 156
    goto :goto_7
.end method


# virtual methods
.method public add(Ljava/util/List;Ljava/util/List;ZZ)V
    .locals 9
    .param p3, "includeOrig"    # Z
    .param p4, "mergeExisting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/Token;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "singleMatch":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "replacement":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/Token;>;"
    move-object v0, p0

    .line 56
    .local v0, "currMap":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 73
    iget-object v4, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    if-eqz v4, :cond_3

    if-nez p4, :cond_3

    .line 74
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SynonymFilter: there is already a mapping for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 56
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 57
    .local v2, "str":Ljava/lang/String;
    iget-object v5, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    if-nez v5, :cond_1

    .line 60
    new-instance v5, Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v6, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    const/4 v7, 0x1

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->ignoreCase()Z

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    iput-object v5, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 63
    :cond_1
    iget-object v5, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get(Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    .line 64
    .local v1, "map":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    if-nez v1, :cond_2

    .line 65
    new-instance v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    .end local v1    # "map":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    invoke-direct {v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;-><init>()V

    .line 66
    .restart local v1    # "map":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    iget v5, v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    iget v6, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    and-int/lit8 v6, v6, 0x2

    or-int/2addr v5, v6

    iput v5, v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    .line 67
    iget-object v5, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v5, v2, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 76
    .end local v1    # "map":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .end local v2    # "str":Ljava/lang/String;
    :cond_3
    iget-object v4, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    if-nez v4, :cond_5

    move-object v3, p2

    .line 78
    .local v3, "superset":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/Token;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lorg/apache/lucene/analysis/Token;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/apache/lucene/analysis/Token;

    iput-object v4, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    .line 79
    if-eqz p3, :cond_4

    iget v4, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    .line 80
    :cond_4
    return-void

    .line 77
    .end local v3    # "superset":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/Token;>;"
    :cond_5
    iget-object v4, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4, p2}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->mergeTokens(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    goto :goto_1
.end method

.method public ignoreCase()Z
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public includeOrig()Z
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 86
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    if-eqz v2, :cond_1

    .line 87
    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 92
    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 93
    const-string v2, ",ORIG"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_0
    const-string v2, "],"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 98
    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 89
    .restart local v0    # "i":I
    :cond_2
    if-eqz v0, :cond_3

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
