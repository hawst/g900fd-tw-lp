.class public Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
.super Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;
.source "HyphenationTree.java"

# interfaces
.implements Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;


# instance fields
.field protected classmap:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

.field private transient ivalues:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

.field protected stoplist:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field protected vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->stoplist:Ljava/util/HashMap;

    .line 58
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->classmap:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    .line 59
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->alloc(I)I

    .line 61
    return-void
.end method


# virtual methods
.method public addClass(Ljava/lang/String;)V
    .locals 5
    .param p1, "chargroup"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 419
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 420
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 421
    .local v0, "equivChar":C
    const/4 v3, 0x2

    new-array v2, v3, [C

    .line 422
    .local v2, "key":[C
    const/4 v3, 0x1

    aput-char v4, v2, v3

    .line 423
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_1

    .line 428
    .end local v0    # "equivChar":C
    .end local v1    # "i":I
    .end local v2    # "key":[C
    :cond_0
    return-void

    .line 424
    .restart local v0    # "equivChar":C
    .restart local v1    # "i":I
    .restart local v2    # "key":[C
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    aput-char v3, v2, v4

    .line 425
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->classmap:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-virtual {v3, v2, v4, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert([CIC)V

    .line 423
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public addException(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 441
    .local p2, "hyphenatedword":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->stoplist:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    return-void
.end method

.method public addPattern(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;
    .param p2, "ivalue"    # Ljava/lang/String;

    .prologue
    .line 456
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->ivalues:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->find(Ljava/lang/String;)I

    move-result v0

    .line 457
    .local v0, "k":I
    if-gtz v0, :cond_0

    .line 458
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->packValues(Ljava/lang/String;)I

    move-result v0

    .line 459
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->ivalues:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    int-to-char v2, v0

    invoke-virtual {v1, p2, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->insert(Ljava/lang/String;C)V

    .line 461
    :cond_0
    int-to-char v1, v0

    invoke-virtual {p0, p1, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->insert(Ljava/lang/String;C)V

    .line 462
    return-void
.end method

.method public findPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "pat"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->find(Ljava/lang/String;)I

    move-result v0

    .line 142
    .local v0, "k":I
    if-ltz v0, :cond_0

    .line 143
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->unpackValues(I)Ljava/lang/String;

    move-result-object v1

    .line 145
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method protected getValues(I)[B
    .locals 7
    .param p1, "k"    # I

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v0, "buf":Ljava/lang/StringBuilder;
    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    add-int/lit8 v3, p1, 0x1

    .end local p1    # "k":I
    .local v3, "k":I
    invoke-virtual {v6, p1}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->get(I)B

    move-result v5

    .line 166
    .local v5, "v":B
    :goto_0
    if-nez v5, :cond_1

    .line 177
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    new-array v4, v6, [B

    .line 178
    .local v4, "res":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v4

    if-lt v2, v6, :cond_2

    .line 181
    return-object v4

    .line 167
    .end local v2    # "i":I
    .end local v4    # "res":[B
    :cond_1
    ushr-int/lit8 v6, v5, 0x4

    add-int/lit8 v6, v6, -0x1

    int-to-char v1, v6

    .line 168
    .local v1, "c":C
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 169
    and-int/lit8 v6, v5, 0xf

    int-to-char v1, v6

    .line 170
    if-eqz v1, :cond_0

    .line 173
    add-int/lit8 v6, v1, -0x1

    int-to-char v1, v6

    .line 174
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 175
    iget-object v6, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    add-int/lit8 p1, v3, 0x1

    .end local v3    # "k":I
    .restart local p1    # "k":I
    invoke-virtual {v6, v3}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->get(I)B

    move-result v5

    move v3, p1

    .end local p1    # "k":I
    .restart local v3    # "k":I
    goto :goto_0

    .line 179
    .end local v1    # "c":C
    .restart local v2    # "i":I
    .restart local v4    # "res":[B
    :cond_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v4, v2

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method protected hstrcmp([CI[CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "si"    # I
    .param p3, "t"    # [C
    .param p4, "ti"    # I

    .prologue
    const/4 v0, 0x0

    .line 152
    :goto_0
    aget-char v1, p1, p2

    aget-char v2, p3, p4

    if-eq v1, v2, :cond_1

    .line 157
    aget-char v1, p3, p4

    if-nez v1, :cond_2

    .line 160
    :cond_0
    :goto_1
    return v0

    .line 153
    :cond_1
    aget-char v1, p1, p2

    if-eqz v1, :cond_0

    .line 152
    add-int/lit8 p2, p2, 0x1

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    .line 160
    :cond_2
    aget-char v0, p1, p2

    aget-char v1, p3, p4

    sub-int/2addr v0, v1

    goto :goto_1
.end method

.method public hyphenate(Ljava/lang/String;II)Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;
    .locals 6
    .param p1, "word"    # Ljava/lang/String;
    .param p2, "remainCharCount"    # I
    .param p3, "pushCharCount"    # I

    .prologue
    .line 284
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 285
    .local v1, "w":[C
    const/4 v2, 0x0

    array-length v3, v1

    move-object v0, p0

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->hyphenate([CIIII)Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;

    move-result-object v0

    return-object v0
.end method

.method public hyphenate([CIIII)Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;
    .locals 22
    .param p1, "w"    # [C
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "remainCharCount"    # I
    .param p5, "pushCharCount"    # I

    .prologue
    .line 318
    add-int/lit8 v20, p3, 0x3

    move/from16 v0, v20

    new-array v0, v0, [C

    move-object/from16 v19, v0

    .line 321
    .local v19, "word":[C
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v5, v0, [C

    .line 322
    .local v5, "c":[C
    const/4 v8, 0x0

    .line 323
    .local v8, "iIgnoreAtBeginning":I
    move/from16 v9, p3

    .line 324
    .local v9, "iLength":I
    const/4 v4, 0x0

    .line 325
    .local v4, "bEndOfLetters":Z
    const/4 v7, 0x1

    .local v7, "i":I
    :goto_0
    move/from16 v0, p3

    if-le v7, v0, :cond_0

    .line 345
    move/from16 p3, v9

    .line 346
    add-int v20, p4, p5

    move/from16 v0, p3

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    .line 348
    const/16 v20, 0x0

    .line 403
    :goto_1
    return-object v20

    .line 326
    :cond_0
    const/16 v20, 0x0

    add-int v21, p2, v7

    add-int/lit8 v21, v21, -0x1

    aget-char v21, p1, v21

    aput-char v21, v5, v20

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->classmap:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->find([CI)I

    move-result v14

    .line 328
    .local v14, "nc":I
    if-gez v14, :cond_2

    .line 329
    add-int/lit8 v20, v8, 0x1

    move/from16 v0, v20

    if-ne v7, v0, :cond_1

    .line 331
    add-int/lit8 v8, v8, 0x1

    .line 336
    :goto_2
    add-int/lit8 v9, v9, -0x1

    .line 325
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 334
    :cond_1
    const/4 v4, 0x1

    goto :goto_2

    .line 338
    :cond_2
    if-nez v4, :cond_3

    .line 339
    sub-int v20, v7, v8

    int-to-char v0, v14

    move/from16 v21, v0

    aput-char v21, v19, v20

    goto :goto_3

    .line 341
    :cond_3
    const/16 v20, 0x0

    goto :goto_1

    .line 350
    .end local v14    # "nc":I
    :cond_4
    add-int/lit8 v20, p3, 0x1

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 351
    .local v17, "result":[I
    const/4 v12, 0x0

    .line 354
    .local v12, "k":I
    new-instance v18, Ljava/lang/String;

    const/16 v20, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    .line 355
    .local v18, "sw":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->stoplist:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->stoplist:Ljava/util/HashMap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 359
    .local v6, "hw":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v11, 0x0

    .line 360
    .local v11, "j":I
    const/4 v7, 0x0

    :goto_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-lt v7, v0, :cond_5

    .line 393
    .end local v6    # "hw":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v11    # "j":I
    :goto_5
    if-lez v12, :cond_a

    .line 395
    add-int/lit8 v20, v12, 0x2

    move/from16 v0, v20

    new-array v0, v0, [I

    move-object/from16 v16, v0

    .line 396
    .local v16, "res":[I
    const/16 v20, 0x0

    const/16 v21, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    move-object/from16 v2, v16

    move/from16 v3, v21

    invoke-static {v0, v1, v2, v3, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 399
    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v16, v20

    .line 400
    add-int/lit8 v20, v12, 0x1

    aput p3, v16, v20

    .line 401
    new-instance v20, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;-><init>([I)V

    goto/16 :goto_1

    .line 361
    .end local v16    # "res":[I
    .restart local v6    # "hw":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v11    # "j":I
    :cond_5
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    .line 364
    .local v15, "o":Ljava/lang/Object;
    instance-of v0, v15, Ljava/lang/String;

    move/from16 v20, v0

    if-eqz v20, :cond_6

    .line 365
    check-cast v15, Ljava/lang/String;

    .end local v15    # "o":Ljava/lang/Object;
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v20

    add-int v11, v11, v20

    .line 366
    move/from16 v0, p4

    if-lt v11, v0, :cond_6

    sub-int v20, p3, p5

    move/from16 v0, v20

    if-ge v11, v0, :cond_6

    .line 367
    add-int/lit8 v13, v12, 0x1

    .end local v12    # "k":I
    .local v13, "k":I
    add-int v20, v11, v8

    aput v20, v17, v12

    move v12, v13

    .line 360
    .end local v13    # "k":I
    .restart local v12    # "k":I
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 373
    .end local v6    # "hw":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v11    # "j":I
    :cond_7
    const/16 v20, 0x0

    const/16 v21, 0x2e

    aput-char v21, v19, v20

    .line 374
    add-int/lit8 v20, p3, 0x1

    const/16 v21, 0x2e

    aput-char v21, v19, v20

    .line 375
    add-int/lit8 v20, p3, 0x2

    const/16 v21, 0x0

    aput-char v21, v19, v20

    .line 376
    add-int/lit8 v20, p3, 0x3

    move/from16 v0, v20

    new-array v10, v0, [B

    .line 377
    .local v10, "il":[B
    const/4 v7, 0x0

    :goto_6
    add-int/lit8 v20, p3, 0x1

    move/from16 v0, v20

    if-lt v7, v0, :cond_8

    .line 385
    const/4 v7, 0x0

    move v13, v12

    .end local v12    # "k":I
    .restart local v13    # "k":I
    :goto_7
    move/from16 v0, p3

    if-lt v7, v0, :cond_9

    move v12, v13

    .end local v13    # "k":I
    .restart local v12    # "k":I
    goto :goto_5

    .line 378
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v7, v10}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->searchPatterns([CI[B)V

    .line 377
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 386
    .end local v12    # "k":I
    .restart local v13    # "k":I
    :cond_9
    add-int/lit8 v20, v7, 0x1

    aget-byte v20, v10, v20

    and-int/lit8 v20, v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    move/from16 v0, p4

    if-lt v7, v0, :cond_b

    .line 387
    sub-int v20, p3, p5

    move/from16 v0, v20

    if-gt v7, v0, :cond_b

    .line 388
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "k":I
    .restart local v12    # "k":I
    add-int v20, v7, v8

    aput v20, v17, v13

    .line 385
    :goto_8
    add-int/lit8 v7, v7, 0x1

    move v13, v12

    .end local v12    # "k":I
    .restart local v13    # "k":I
    goto :goto_7

    .line 403
    .end local v10    # "il":[B
    .end local v13    # "k":I
    .restart local v12    # "k":I
    :cond_a
    const/16 v20, 0x0

    goto/16 :goto_1

    .end local v12    # "k":I
    .restart local v10    # "il":[B
    .restart local v13    # "k":I
    :cond_b
    move v12, v13

    .end local v13    # "k":I
    .restart local v12    # "k":I
    goto :goto_8
.end method

.method public loadPatterns(Ljava/io/File;)V
    .locals 2
    .param p1, "f"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-virtual {p1}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    .line 115
    .local v0, "src":Lorg/xml/sax/InputSource;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->loadPatterns(Lorg/xml/sax/InputSource;)V

    .line 116
    return-void
.end method

.method public loadPatterns(Lorg/xml/sax/InputSource;)V
    .locals 2
    .param p1, "source"    # Lorg/xml/sax/InputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;-><init>(Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;)V

    .line 126
    .local v0, "pp":Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;
    new-instance v1, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-direct {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->ivalues:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    .line 128
    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parse(Lorg/xml/sax/InputSource;)V

    .line 132
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->trimToSize()V

    .line 133
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->trimToSize()V

    .line 134
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->classmap:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->trimToSize()V

    .line 137
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->ivalues:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;

    .line 138
    return-void
.end method

.method protected packValues(Ljava/lang/String;)I
    .locals 10
    .param p1, "values"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 74
    .local v3, "n":I
    and-int/lit8 v7, v3, 0x1

    if-ne v7, v9, :cond_0

    shr-int/lit8 v7, v3, 0x1

    add-int/lit8 v2, v7, 0x2

    .line 75
    .local v2, "m":I
    :goto_0
    iget-object v7, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    invoke-virtual {v7, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->alloc(I)I

    move-result v4

    .line 76
    .local v4, "offset":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->getArray()[B

    move-result-object v6

    .line 77
    .local v6, "va":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v3, :cond_1

    .line 86
    add-int/lit8 v7, v2, -0x1

    add-int/2addr v7, v4

    const/4 v8, 0x0

    aput-byte v8, v6, v7

    .line 87
    return v4

    .line 74
    .end local v0    # "i":I
    .end local v2    # "m":I
    .end local v4    # "offset":I
    .end local v6    # "va":[B
    :cond_0
    shr-int/lit8 v7, v3, 0x1

    add-int/lit8 v2, v7, 0x1

    goto :goto_0

    .line 78
    .restart local v0    # "i":I
    .restart local v2    # "m":I
    .restart local v4    # "offset":I
    .restart local v6    # "va":[B
    :cond_1
    shr-int/lit8 v1, v0, 0x1

    .line 79
    .local v1, "j":I
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v7

    add-int/lit8 v7, v7, -0x30

    add-int/lit8 v7, v7, 0x1

    and-int/lit8 v7, v7, 0xf

    int-to-byte v5, v7

    .line 80
    .local v5, "v":B
    and-int/lit8 v7, v0, 0x1

    if-ne v7, v9, :cond_2

    .line 81
    add-int v7, v1, v4

    add-int v8, v1, v4

    aget-byte v8, v6, v8

    or-int/2addr v8, v5

    int-to-byte v8, v8

    aput-byte v8, v6, v7

    .line 77
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 83
    :cond_2
    add-int v7, v1, v4

    shl-int/lit8 v8, v5, 0x4

    int-to-byte v8, v8

    aput-byte v8, v6, v7

    goto :goto_2
.end method

.method public printStats(Ljava/io/PrintStream;)V
    .locals 2
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 466
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Value space size = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 467
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 466
    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 468
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree;->printStats(Ljava/io/PrintStream;)V

    .line 470
    return-void
.end method

.method protected searchPatterns([CI[B)V
    .locals 11
    .param p1, "word"    # [C
    .param p2, "index"    # I
    .param p3, "il"    # [B

    .prologue
    const v10, 0xffff

    .line 212
    move v1, p2

    .line 214
    .local v1, "i":I
    aget-char v6, p1, v1

    .line 215
    .local v6, "sp":C
    iget-char v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->root:C

    .line 217
    .local v4, "p":C
    :cond_0
    :goto_0
    if-lez v4, :cond_1

    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->sc:[C

    array-length v8, v8

    if-lt v4, v8, :cond_2

    .line 269
    :cond_1
    return-void

    .line 218
    :cond_2
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->sc:[C

    aget-char v8, v8, v4

    if-ne v8, v10, :cond_4

    .line 219
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->kv:Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/compound/hyphenation/CharVector;->getArray()[C

    move-result-object v8

    iget-object v9, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->lo:[C

    aget-char v9, v9, v4

    invoke-virtual {p0, p1, v1, v8, v9}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->hstrcmp([CI[CI)I

    move-result v8

    if-nez v8, :cond_1

    .line 220
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->eq:[C

    aget-char v8, v8, v4

    invoke-virtual {p0, v8}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->getValues(I)[B

    move-result-object v7

    .line 221
    .local v7, "values":[B
    move v2, p2

    .line 222
    .local v2, "j":I
    const/4 v3, 0x0

    .local v3, "k":I
    :goto_1
    array-length v8, v7

    if-ge v3, v8, :cond_1

    .line 223
    array-length v8, p3

    if-ge v2, v8, :cond_3

    aget-byte v8, v7, v3

    aget-byte v9, p3, v2

    if-le v8, v9, :cond_3

    .line 224
    aget-byte v8, v7, v3

    aput-byte v8, p3, v2

    .line 226
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 222
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 231
    .end local v2    # "j":I
    .end local v3    # "k":I
    .end local v7    # "values":[B
    :cond_4
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->sc:[C

    aget-char v8, v8, v4

    sub-int v0, v6, v8

    .line 232
    .local v0, "d":I
    if-nez v0, :cond_7

    .line 233
    if-eqz v6, :cond_1

    .line 236
    add-int/lit8 v1, v1, 0x1

    aget-char v6, p1, v1

    .line 237
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->eq:[C

    aget-char v4, v8, v4

    .line 238
    move v5, v4

    .line 242
    .local v5, "q":C
    :goto_2
    if-lez v5, :cond_0

    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->sc:[C

    array-length v8, v8

    if-ge v5, v8, :cond_0

    .line 243
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->sc:[C

    aget-char v8, v8, v5

    if-eq v8, v10, :cond_0

    .line 246
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->sc:[C

    aget-char v8, v8, v5

    if-nez v8, :cond_6

    .line 247
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->eq:[C

    aget-char v8, v8, v5

    invoke-virtual {p0, v8}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->getValues(I)[B

    move-result-object v7

    .line 248
    .restart local v7    # "values":[B
    move v2, p2

    .line 249
    .restart local v2    # "j":I
    const/4 v3, 0x0

    .restart local v3    # "k":I
    :goto_3
    array-length v8, v7

    if-ge v3, v8, :cond_0

    .line 250
    array-length v8, p3

    if-ge v2, v8, :cond_5

    aget-byte v8, v7, v3

    aget-byte v9, p3, v2

    if-le v8, v9, :cond_5

    .line 251
    aget-byte v8, v7, v3

    aput-byte v8, p3, v2

    .line 253
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 249
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 257
    .end local v2    # "j":I
    .end local v3    # "k":I
    .end local v7    # "values":[B
    :cond_6
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->lo:[C

    aget-char v5, v8, v5

    goto :goto_2

    .line 266
    .end local v5    # "q":C
    :cond_7
    if-gez v0, :cond_8

    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->lo:[C

    aget-char v4, v8, v4

    :goto_4
    goto/16 :goto_0

    :cond_8
    iget-object v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->hi:[C

    aget-char v4, v8, v4

    goto :goto_4
.end method

.method protected unpackValues(I)Ljava/lang/String;
    .locals 5
    .param p1, "k"    # I

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v0, "buf":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    add-int/lit8 v2, p1, 0x1

    .end local p1    # "k":I
    .local v2, "k":I
    invoke-virtual {v4, p1}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->get(I)B

    move-result v3

    .line 93
    .local v3, "v":B
    :goto_0
    if-nez v3, :cond_1

    .line 104
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 94
    :cond_1
    ushr-int/lit8 v4, v3, 0x4

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, 0x30

    int-to-char v1, v4

    .line 95
    .local v1, "c":C
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 96
    and-int/lit8 v4, v3, 0xf

    int-to-char v1, v4

    .line 97
    if-eqz v1, :cond_0

    .line 100
    add-int/lit8 v4, v1, -0x1

    add-int/lit8 v4, v4, 0x30

    int-to-char v1, v4

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->vspace:Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;

    add-int/lit8 p1, v2, 0x1

    .end local v2    # "k":I
    .restart local p1    # "k":I
    invoke-virtual {v4, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->get(I)B

    move-result v3

    move v2, p1

    .end local p1    # "k":I
    .restart local v2    # "k":I
    goto :goto_0
.end method
