.class Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;
.super Lorg/apache/lucene/analysis/util/CharArrayMap;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/util/CharArrayMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UnmodifiableCharArrayMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/analysis/util/CharArrayMap",
        "<TV;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 616
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    .local p1, "map":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 617
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 621
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method createEntrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<TV;>.EntrySet;"
        }
    .end annotation

    .prologue
    .line 651
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;ZLorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;)V

    return-object v0
.end method

.method public put(Ljava/lang/CharSequence;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 636
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    .local p2, "val":Ljava/lang/Object;, "TV;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 626
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    .local p2, "val":Ljava/lang/Object;, "TV;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TV;)TV;"
        }
    .end annotation

    .prologue
    .line 641
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    .local p2, "val":Ljava/lang/Object;, "TV;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public put([CLjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CTV;)TV;"
        }
    .end annotation

    .prologue
    .line 631
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    .local p2, "val":Ljava/lang/Object;, "TV;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 646
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.UnmodifiableCharArrayMap<TV;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
