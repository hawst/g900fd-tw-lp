.class public Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerFactory;
.super Lorg/apache/lucene/analysis/util/TokenizerFactory;
.source "WikipediaTokenizerFactory.java"


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenizerFactory;-><init>(Ljava/util/Map;)V

    .line 41
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Tokenizer;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizerFactory;->create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;
    .locals 3
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;

    const/4 v1, 0x0

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    .line 49
    invoke-direct {v0, p1, p2, v1, v2}, Lorg/apache/lucene/analysis/wikipedia/WikipediaTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ILjava/util/Set;)V

    return-object v0
.end method
