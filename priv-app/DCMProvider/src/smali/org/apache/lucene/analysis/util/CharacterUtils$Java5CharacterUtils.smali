.class final Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;
.super Lorg/apache/lucene/analysis/util/CharacterUtils;
.source "CharacterUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/util/CharacterUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Java5CharacterUtils"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    const-class v0, Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Lorg/apache/lucene/analysis/util/CharacterUtils;-><init>()V

    .line 178
    return-void
.end method


# virtual methods
.method public codePointAt(Ljava/lang/CharSequence;I)I
    .locals 1
    .param p1, "seq"    # Ljava/lang/CharSequence;
    .param p2, "offset"    # I

    .prologue
    .line 187
    invoke-static {p1, p2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    return v0
.end method

.method public codePointAt([CI)I
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "offset"    # I

    .prologue
    .line 182
    invoke-static {p1, p2}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v0

    return v0
.end method

.method public codePointAt([CII)I
    .locals 1
    .param p1, "chars"    # [C
    .param p2, "offset"    # I
    .param p3, "limit"    # I

    .prologue
    .line 192
    invoke-static {p1, p2, p3}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v0

    return v0
.end method

.method public fill(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z
    .locals 8
    .param p1, "buffer"    # Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 197
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->buffer:[C
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$0(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)[C

    move-result-object v0

    .line 198
    .local v0, "charBuffer":[C
    invoke-static {p1, v5}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$1(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V

    .line 202
    iget-char v6, p1, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    if-eqz v6, :cond_1

    .line 203
    iget-char v6, p1, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    aput-char v6, v0, v5

    .line 204
    const/4 v1, 0x1

    .line 211
    .local v1, "offset":I
    :goto_0
    array-length v6, v0

    sub-int/2addr v6, v1

    .line 209
    invoke-virtual {p2, v0, v1, v6}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 212
    .local v2, "read":I
    if-ne v2, v7, :cond_3

    .line 213
    invoke-static {p1, v1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$2(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V

    .line 214
    iput-char v5, p1, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    .line 215
    if-eqz v1, :cond_2

    .line 244
    :cond_0
    :goto_1
    return v4

    .line 206
    .end local v1    # "offset":I
    .end local v2    # "read":I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "offset":I
    goto :goto_0

    .restart local v2    # "read":I
    :cond_2
    move v4, v5

    .line 215
    goto :goto_1

    .line 217
    :cond_3
    sget-boolean v6, Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    if-gtz v2, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 218
    :cond_4
    add-int v6, v2, v1

    invoke-static {p1, v6}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$2(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V

    .line 222
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    if-ne v6, v4, :cond_6

    .line 223
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget-char v6, v0, v6

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 226
    array-length v6, v0

    add-int/lit8 v6, v6, -0x1

    .line 224
    invoke-virtual {p2, v0, v4, v6}, Ljava/io/Reader;->read([CII)I

    move-result v3

    .line 227
    .local v3, "read2":I
    if-eq v3, v7, :cond_0

    .line 232
    sget-boolean v6, Lorg/apache/lucene/analysis/util/CharacterUtils$Java5CharacterUtils;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    if-gtz v3, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 234
    :cond_5
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    add-int/2addr v6, v3

    invoke-static {p1, v6}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$2(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V

    .line 237
    .end local v3    # "read2":I
    :cond_6
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    if-le v6, v4, :cond_7

    .line 238
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    aget-char v6, v0, v6

    invoke-static {v6}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 239
    # getter for: Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->length:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$3(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {p1, v5}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->access$2(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;I)V

    aget-char v5, v0, v5

    iput-char v5, p1, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    goto :goto_1

    .line 241
    :cond_7
    iput-char v5, p1, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->lastTrailingHighSurrogate:C

    goto :goto_1
.end method
