.class public Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;
.super Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
.source "RSLPStemmerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "RuleWithSuffixExceptions"
.end annotation


# instance fields
.field protected final exceptions:[[C


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1, "suffix"    # Ljava/lang/String;
    .param p2, "min"    # I
    .param p3, "replacement"    # Ljava/lang/String;
    .param p4, "exceptions"    # [Ljava/lang/String;

    .prologue
    .line 156
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 157
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p4

    if-lt v0, v1, :cond_0

    .line 161
    array-length v1, p4

    new-array v1, v1, [[C

    iput-object v1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;->exceptions:[[C

    .line 162
    const/4 v0, 0x0

    :goto_1
    array-length v1, p4

    if-lt v0, v1, :cond_2

    .line 164
    return-void

    .line 158
    :cond_0
    aget-object v1, p4, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 159
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "warning: useless exception \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p4, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' does not end with \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 157
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;->exceptions:[[C

    aget-object v2, p4, v0

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v1, v0

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public matches([CI)Z
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->matches([CI)Z

    move-result v2

    if-nez v2, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v1

    .line 171
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;->exceptions:[[C

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 175
    const/4 v1, 0x1

    goto :goto_0

    .line 172
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;->exceptions:[[C

    aget-object v2, v2, v0

    invoke-static {p1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CI[C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
