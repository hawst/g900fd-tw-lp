.class public final Lorg/apache/lucene/analysis/cn/ChineseFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "ChineseFilter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final STOP_WORDS:[Ljava/lang/String;


# instance fields
.field private stopTable:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 52
    const/16 v0, 0x1f

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 53
    const-string v2, "and"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "are"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "as"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "at"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "be"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "but"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "by"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 54
    const-string v2, "for"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "if"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "into"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "is"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 55
    const-string v2, "no"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "not"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "of"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "on"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "or"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "such"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 56
    const-string v2, "that"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "the"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "their"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "then"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "there"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "these"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 57
    const-string v2, "they"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "this"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "to"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "was"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "will"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "with"

    aput-object v2, v0, v1

    .line 52
    sput-object v0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->STOP_WORDS:[Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 63
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cn/ChineseFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 68
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    sget-object v2, Lorg/apache/lucene/analysis/cn/ChineseFilter;->STOP_WORDS:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->stopTable:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 69
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 74
    :cond_0
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v4

    if-nez v4, :cond_1

    move v2, v3

    .line 101
    :goto_1
    :pswitch_0
    return v2

    .line 75
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    .line 76
    .local v1, "text":[C
    iget-object v4, p0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 79
    .local v0, "termLength":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/cn/ChineseFilter;->stopTable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v4, v1, v3, v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v4

    if-nez v4, :cond_0

    .line 80
    aget-char v4, v1, v3

    invoke-static {v4}, Ljava/lang/Character;->getType(C)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 86
    :pswitch_2
    if-le v0, v2, :cond_0

    goto :goto_1

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
