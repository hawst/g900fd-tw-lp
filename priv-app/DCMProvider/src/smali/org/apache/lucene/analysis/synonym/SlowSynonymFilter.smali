.class final Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "SlowSynonymFilter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private buffer:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/util/AttributeSource;",
            ">;"
        }
    .end annotation
.end field

.field private exhausted:Z

.field private final map:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

.field private matched:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/util/AttributeSource;",
            ">;"
        }
    .end annotation
.end field

.field private replacement:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeSource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "map"    # Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 51
    if-nez p2, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "map is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->map:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    .line 56
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    .line 57
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    .line 58
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    .line 59
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    .line 60
    return-void
.end method

.method private copy(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/util/AttributeSource;)V
    .locals 0
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "source"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 251
    if-eq p1, p2, :cond_0

    .line 252
    invoke-virtual {p2, p1}, Lorg/apache/lucene/util/AttributeSource;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    .line 253
    :cond_0
    return-void
.end method

.method private match(Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;)Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .locals 8
    .param p1, "map"    # Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 218
    .local v0, "result":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    iget-object v4, p1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    if-eqz v4, :cond_2

    .line 219
    invoke-direct {p0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->nextTok()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v3

    .line 220
    .local v3, "tok":Lorg/apache/lucene/util/AttributeSource;
    if-eqz v3, :cond_2

    .line 222
    if-ne v3, p0, :cond_0

    .line 223
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v3

    .line 225
    :cond_0
    const-class v4, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 226
    .local v2, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    iget-object v4, p1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    .line 228
    .local v1, "subMap":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    if-eqz v1, :cond_1

    .line 230
    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->match(Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;)Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    move-result-object v0

    .line 233
    :cond_1
    if-eqz v0, :cond_4

    .line 234
    iget-object v4, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 243
    .end local v1    # "subMap":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .end local v2    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v3    # "tok":Lorg/apache/lucene/util/AttributeSource;
    :cond_2
    :goto_0
    if-nez v0, :cond_3

    iget-object v4, p1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    if-eqz v4, :cond_3

    .line 244
    move-object v0, p1

    .line 247
    :cond_3
    return-object v0

    .line 237
    .restart local v1    # "subMap":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .restart local v2    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .restart local v3    # "tok":Lorg/apache/lucene/util/AttributeSource;
    :cond_4
    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->pushTok(Lorg/apache/lucene/util/AttributeSource;)V

    goto :goto_0
.end method

.method private nextTok()Lorg/apache/lucene/util/AttributeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->buffer:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->buffer:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->buffer:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeSource;

    .line 205
    :goto_0
    return-object v0

    .line 201
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->exhausted:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 202
    goto :goto_0

    .line 204
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->exhausted:Z

    .line 205
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pushTok(Lorg/apache/lucene/util/AttributeSource;)V
    .locals 1
    .param p1, "t"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->buffer:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->buffer:Ljava/util/LinkedList;

    .line 212
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->buffer:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 213
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->replacement:Ljava/util/Iterator;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->replacement:Ljava/util/Iterator;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->replacement:Ljava/util/Iterator;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->copy(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/util/AttributeSource;)V

    .line 87
    const/16 v22, 0x1

    .line 112
    :goto_1
    return v22

    .line 91
    :cond_0
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->nextTok()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v4

    .line 92
    .local v4, "firstTok":Lorg/apache/lucene/util/AttributeSource;
    if-nez v4, :cond_1

    const/16 v22, 0x0

    goto :goto_1

    .line 93
    :cond_1
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v21

    check-cast v21, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 94
    .local v21, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->map:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-object/from16 v22, v0

    if-eqz v22, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->map:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->submap:Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-object/from16 v22, v0

    invoke-interface/range {v21 .. v21}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v23

    const/16 v24, 0x0

    invoke-interface/range {v21 .. v21}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v25

    invoke-virtual/range {v22 .. v25}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    move-object/from16 v20, v22

    .line 95
    .local v20, "result":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    :goto_2
    if-nez v20, :cond_3

    .line 96
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->copy(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/util/AttributeSource;)V

    .line 97
    const/16 v22, 0x1

    goto :goto_1

    .line 94
    .end local v20    # "result":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    :cond_2
    const/16 v20, 0x0

    goto :goto_2

    .line 101
    .restart local v20    # "result":Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    :cond_3
    move-object/from16 v0, p0

    if-ne v4, v0, :cond_4

    .line 102
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v4

    .line 105
    :cond_4
    new-instance v22, Ljava/util/LinkedList;

    invoke-direct/range {v22 .. v22}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    .line 107
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->match(Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;)Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    move-result-object v20

    .line 109
    if-nez v20, :cond_5

    .line 111
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->copy(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/util/AttributeSource;)V

    .line 112
    const/16 v22, 0x1

    goto :goto_1

    .line 116
    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedList;->size()I

    move-result v23

    add-int v22, v22, v23

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 122
    .local v5, "generated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/lucene/util/AttributeSource;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_7

    move-object v9, v4

    .line 123
    .local v9, "lastTok":Lorg/apache/lucene/util/AttributeSource;
    :goto_3
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->includeOrig()Z

    move-result v7

    .line 125
    .local v7, "includeOrig":Z
    if-eqz v7, :cond_8

    move-object/from16 v16, v4

    .line 126
    .local v16, "origTok":Lorg/apache/lucene/util/AttributeSource;
    :goto_4
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 127
    .local v3, "firstPosIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v14

    .line 128
    .local v14, "origPos":I
    const/16 v18, 0x0

    .line 129
    .local v18, "repPos":I
    const/16 v17, 0x0

    .line 131
    .local v17, "pos":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_5
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    if-lt v6, v0, :cond_9

    .line 164
    :cond_6
    :goto_6
    if-nez v16, :cond_e

    .line 180
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->replacement:Ljava/util/Iterator;

    goto/16 :goto_0

    .line 122
    .end local v3    # "firstPosIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v6    # "i":I
    .end local v7    # "includeOrig":Z
    .end local v9    # "lastTok":Lorg/apache/lucene/util/AttributeSource;
    .end local v14    # "origPos":I
    .end local v16    # "origTok":Lorg/apache/lucene/util/AttributeSource;
    .end local v17    # "pos":I
    .end local v18    # "repPos":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v9, v22

    goto :goto_3

    .line 125
    .restart local v7    # "includeOrig":Z
    .restart local v9    # "lastTok":Lorg/apache/lucene/util/AttributeSource;
    :cond_8
    const/16 v16, 0x0

    goto :goto_4

    .line 132
    .restart local v3    # "firstPosIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v6    # "i":I
    .restart local v14    # "origPos":I
    .restart local v16    # "origTok":Lorg/apache/lucene/util/AttributeSource;
    .restart local v17    # "pos":I
    .restart local v18    # "repPos":I
    :cond_9
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->synonyms:[Lorg/apache/lucene/analysis/Token;

    move-object/from16 v22, v0

    aget-object v19, v22, v6

    .line 133
    .local v19, "repTok":Lorg/apache/lucene/analysis/Token;
    invoke-virtual {v4}, Lorg/apache/lucene/util/AttributeSource;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v13

    .line 134
    .local v13, "newTok":Lorg/apache/lucene/util/AttributeSource;
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 135
    .local v12, "newTermAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 136
    .local v10, "newOffsetAtt":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 138
    .local v11, "newPosIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 140
    .local v8, "lastOffsetAtt":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v22

    invoke-interface {v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-interface {v10, v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 141
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/analysis/Token;->buffer()[C

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/analysis/Token;->length()I

    move-result v24

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-interface {v12, v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 142
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/analysis/Token;->getPositionIncrement()I

    move-result v22

    add-int v18, v18, v22

    .line 143
    if-nez v6, :cond_a

    move/from16 v18, v14

    .line 146
    :cond_a
    :goto_7
    if-eqz v16, :cond_b

    move/from16 v0, v18

    if-le v14, v0, :cond_c

    .line 158
    :cond_b
    sub-int v22, v18, v17

    move/from16 v0, v22

    invoke-interface {v11, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 159
    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    invoke-interface {v11}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v22

    add-int v17, v17, v22

    .line 131
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_5

    .line 147
    :cond_c
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 148
    .local v15, "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    sub-int v22, v14, v17

    move/from16 v0, v22

    invoke-interface {v15, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 149
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v22

    add-int v17, v17, v22

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_d

    const/16 v16, 0x0

    .line 152
    :goto_8
    if-eqz v16, :cond_a

    .line 153
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    .end local v15    # "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 154
    .restart local v15    # "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v22

    add-int v14, v14, v22

    goto :goto_7

    .line 151
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v16, v22

    goto :goto_8

    .line 165
    .end local v8    # "lastOffsetAtt":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v10    # "newOffsetAtt":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v11    # "newPosIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v12    # "newTermAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    .end local v13    # "newTok":Lorg/apache/lucene/util/AttributeSource;
    .end local v15    # "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v19    # "repTok":Lorg/apache/lucene/analysis/Token;
    :cond_e
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 166
    .restart local v15    # "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    sub-int v22, v14, v17

    move/from16 v0, v22

    invoke-interface {v15, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 167
    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v22

    add-int v17, v17, v22

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_f

    const/16 v16, 0x0

    .line 170
    :goto_9
    if-eqz v16, :cond_6

    .line 171
    const-class v22, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    .end local v15    # "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 172
    .restart local v15    # "origPosInc":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v22

    add-int v14, v14, v22

    goto/16 :goto_6

    .line 169
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->matched:Ljava/util/LinkedList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v16, v22

    goto :goto_9
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 258
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->replacement:Ljava/util/Iterator;

    .line 259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;->exhausted:Z

    .line 260
    return-void
.end method
