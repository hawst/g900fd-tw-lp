.class public Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
.super Ljava/lang/Object;
.source "CompoundWordTokenFilterBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CompoundToken"
.end annotation


# instance fields
.field public final endOffset:I

.field public final startOffset:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;

.field public final txt:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V
    .locals 5
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 152
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->this$0:Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iget-object v3, p1, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int v4, p2, p3

    invoke-interface {v3, p2, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->txt:Ljava/lang/CharSequence;

    .line 156
    iget-object v3, p1, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v2

    .line 157
    .local v2, "startOff":I
    iget-object v3, p1, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v0

    .line 159
    .local v0, "endOff":I
    sub-int v3, v0, v2

    iget-object v4, p1, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 162
    iput v2, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->startOffset:I

    .line 163
    iput v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->endOffset:I

    .line 169
    :goto_0
    return-void

    .line 165
    :cond_0
    add-int v1, v2, p2

    .line 166
    .local v1, "newStart":I
    iput v1, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->startOffset:I

    .line 167
    add-int v3, v1, p3

    iput v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->endOffset:I

    goto :goto_0
.end method
