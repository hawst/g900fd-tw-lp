.class public Lorg/apache/lucene/analysis/ar/ArabicStemmer;
.super Ljava/lang/Object;
.source "ArabicStemmer.java"


# static fields
.field public static final ALEF:C = '\u0627'

.field public static final BEH:C = '\u0628'

.field public static final FEH:C = '\u0641'

.field public static final HEH:C = '\u0647'

.field public static final KAF:C = '\u0643'

.field public static final LAM:C = '\u0644'

.field public static final NOON:C = '\u0646'

.field public static final TEH:C = '\u062a'

.field public static final TEH_MARBUTA:C = '\u0629'

.field public static final WAW:C = '\u0648'

.field public static final YEH:C = '\u064a'

.field public static final prefixes:[[C

.field public static final suffixes:[[C


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    const/4 v0, 0x7

    new-array v0, v0, [[C

    .line 49
    const-string/jumbo v1, "\u0627\u0644"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v3

    .line 50
    const-string/jumbo v1, "\u0648\u0627\u0644"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v4

    .line 51
    const-string/jumbo v1, "\u0628\u0627\u0644"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v5

    .line 52
    const-string/jumbo v1, "\u0643\u0627\u0644"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v6

    .line 53
    const-string/jumbo v1, "\u0641\u0627\u0644"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 54
    const-string/jumbo v2, "\u0644\u0644"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 55
    const-string/jumbo v2, "\u0648"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    .line 48
    sput-object v0, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->prefixes:[[C

    .line 58
    const/16 v0, 0xa

    new-array v0, v0, [[C

    .line 59
    const-string/jumbo v1, "\u0647\u0627"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v3

    .line 60
    const-string/jumbo v1, "\u0627\u0646"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v4

    .line 61
    const-string/jumbo v1, "\u0627\u062a"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v5

    .line 62
    const-string/jumbo v1, "\u0648\u0646"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v6

    .line 63
    const-string/jumbo v1, "\u064a\u0646"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    .line 64
    const-string/jumbo v2, "\u064a\u0647"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 65
    const-string/jumbo v2, "\u064a\u0629"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 66
    const-string/jumbo v2, "\u0647"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 67
    const-string/jumbo v2, "\u0629"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 68
    const-string/jumbo v2, "\u064a"

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    aput-object v2, v0, v1

    .line 58
    sput-object v0, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->suffixes:[[C

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method endsWithCheckLength([CI[C)Z
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I
    .param p3, "suffix"    # [C

    .prologue
    const/4 v1, 0x0

    .line 140
    array-length v2, p3

    add-int/lit8 v2, v2, 0x2

    if-ge p2, v2, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v1

    .line 143
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p3

    if-lt v0, v2, :cond_2

    .line 147
    const/4 v1, 0x1

    goto :goto_0

    .line 144
    :cond_2
    array-length v2, p3

    sub-int v2, p2, v2

    add-int/2addr v2, v0

    aget-char v2, p1, v2

    aget-char v3, p3, v0

    if-ne v2, v3, :cond_0

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method startsWithCheckLength([CI[C)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I
    .param p3, "prefix"    # [C

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 119
    array-length v3, p3

    if-ne v3, v2, :cond_1

    const/4 v3, 0x4

    if-ge p2, v3, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 121
    :cond_1
    array-length v3, p3

    add-int/lit8 v3, v3, 0x2

    if-lt p2, v3, :cond_0

    .line 124
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, p3

    if-lt v0, v3, :cond_2

    move v1, v2

    .line 128
    goto :goto_0

    .line 125
    :cond_2
    aget-char v3, p1, v0

    aget-char v4, p3, v0

    if-ne v3, v4, :cond_0

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public stem([CI)I
    .locals 0
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 79
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->stemPrefix([CI)I

    move-result p2

    .line 80
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->stemSuffix([CI)I

    move-result p2

    .line 82
    return p2
.end method

.method public stemPrefix([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 92
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->prefixes:[[C

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 95
    .end local p2    # "len":I
    :goto_1
    return p2

    .line 93
    .restart local p2    # "len":I
    :cond_0
    sget-object v1, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->prefixes:[[C

    aget-object v1, v1, v0

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->startsWithCheckLength([CI[C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    const/4 v1, 0x0

    sget-object v2, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->prefixes:[[C

    aget-object v2, v2, v0

    array-length v2, v2

    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_1

    .line 92
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public stemSuffix([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->suffixes:[[C

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 108
    return p2

    .line 106
    :cond_0
    sget-object v1, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->suffixes:[[C

    aget-object v1, v1, v0

    invoke-virtual {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->endsWithCheckLength([CI[C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    sget-object v1, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->suffixes:[[C

    aget-object v1, v1, v0

    array-length v1, v1

    sub-int v1, p2, v1

    sget-object v2, Lorg/apache/lucene/analysis/ar/ArabicStemmer;->suffixes:[[C

    aget-object v2, v2, v0

    array-length v2, v2

    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    .line 105
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
