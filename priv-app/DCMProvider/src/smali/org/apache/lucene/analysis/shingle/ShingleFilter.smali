.class public final Lorg/apache/lucene/analysis/shingle/ShingleFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "ShingleFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;,
        Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAX_SHINGLE_SIZE:I = 0x2

.field public static final DEFAULT_MIN_SHINGLE_SIZE:I = 0x2

.field public static final DEFAULT_TOKEN_TYPE:Ljava/lang/String; = "shingle"

.field public static final FILLER_TOKEN:[C

.field public static final TOKEN_SEPARATOR:Ljava/lang/String; = " "


# instance fields
.field private exhausted:Z

.field private gramBuilder:Ljava/lang/StringBuilder;

.field private gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

.field private inputWindow:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;",
            ">;"
        }
    .end annotation
.end field

.field private isNextInputStreamToken:Z

.field private isOutputHere:Z

.field private maxShingleSize:I

.field private minShingleSize:I

.field private nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

.field noShingleOutput:Z

.field private numFillerTokensToInsert:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private outputUnigrams:Z

.field private outputUnigramsIfNoShingles:Z

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final posLenAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private tokenSeparator:Ljava/lang/String;

.field private tokenType:Ljava/lang/String;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x5f

    aput-char v2, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->FILLER_TOKEN:[C

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    const/4 v0, 0x2

    .line 189
    invoke-direct {p0, p1, v0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;II)V

    .line 190
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;I)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "maxShingleSize"    # I

    .prologue
    .line 180
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;II)V

    .line 181
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;II)V
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "minShingleSize"    # I
    .param p3, "maxShingleSize"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 167
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 77
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramBuilder:Ljava/lang/StringBuilder;

    .line 93
    const-string v0, "shingle"

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->tokenType:Ljava/lang/String;

    .line 98
    const-string v0, " "

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->tokenSeparator:Ljava/lang/String;

    .line 104
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigrams:Z

    .line 109
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigramsIfNoShingles:Z

    .line 138
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isNextInputStreamToken:Z

    .line 144
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isOutputHere:Z

    .line 149
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->noShingleOutput:Z

    .line 151
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 152
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 153
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 154
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->posLenAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    .line 155
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 168
    invoke-virtual {p0, p3}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setMaxShingleSize(I)V

    .line 169
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setMinShingleSize(I)V

    .line 170
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/lang/String;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "tokenType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x2

    .line 200
    invoke-direct {p0, p1, v0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;II)V

    .line 201
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setTokenType(Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigrams:Z

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->minShingleSize:I

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->maxShingleSize:I

    return v0
.end method

.method private getNextToken(Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;)Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    .locals 6
    .param p1, "target"    # Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 346
    move-object v0, p1

    .line 347
    .local v0, "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    if-lez v1, :cond_1

    .line 348
    if-nez p1, :cond_0

    .line 349
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    .end local v0    # "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeSource;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;-><init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;Lorg/apache/lucene/util/AttributeSource;)V

    .line 354
    .restart local v0    # "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    :goto_0
    iget-object v1, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v2, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v2

    .line 355
    iget-object v3, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v3

    .line 354
    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 356
    iget-object v1, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    sget-object v2, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->FILLER_TOKEN:[C

    sget-object v3, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->FILLER_TOKEN:[C

    array-length v3, v3

    invoke-interface {v1, v2, v4, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 357
    iput-boolean v5, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->isFiller:Z

    .line 358
    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    .line 397
    :goto_1
    return-object v0

    .line 351
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    iget-object v2, p1, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->attSource:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/AttributeSource;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    goto :goto_0

    .line 359
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isNextInputStreamToken:Z

    if-eqz v1, :cond_3

    .line 360
    if-nez p1, :cond_2

    .line 361
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    .end local v0    # "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeSource;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;-><init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;Lorg/apache/lucene/util/AttributeSource;)V

    .line 365
    .restart local v0    # "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    :goto_2
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isNextInputStreamToken:Z

    .line 366
    iput-boolean v4, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->isFiller:Z

    goto :goto_1

    .line 363
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    iget-object v2, p1, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->attSource:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/AttributeSource;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    goto :goto_2

    .line 367
    :cond_3
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->exhausted:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 368
    if-nez p1, :cond_4

    .line 369
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    .end local v0    # "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;-><init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;Lorg/apache/lucene/util/AttributeSource;)V

    .line 373
    .restart local v0    # "newTarget":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    :goto_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v1

    if-le v1, v5, :cond_6

    .line 377
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->maxShingleSize:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 376
    iput v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    .line 379
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    if-nez v1, :cond_5

    .line 380
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    .line 384
    :goto_4
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isNextInputStreamToken:Z

    .line 386
    iget-object v1, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 387
    iget-object v1, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    sget-object v2, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->FILLER_TOKEN:[C

    sget-object v3, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->FILLER_TOKEN:[C

    array-length v3, v3

    invoke-interface {v1, v2, v4, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 388
    iput-boolean v5, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->isFiller:Z

    .line 389
    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    goto/16 :goto_1

    .line 371
    :cond_4
    iget-object v1, p1, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->attSource:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    goto :goto_3

    .line 382
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    goto :goto_4

    .line 391
    :cond_6
    iput-boolean v4, v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->isFiller:Z

    goto/16 :goto_1

    .line 394
    :cond_7
    const/4 v0, 0x0

    .line 395
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->exhausted:Z

    goto/16 :goto_1
.end method

.method private shiftInputWindow()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 408
    const/4 v0, 0x0

    .line 409
    .local v0, "firstToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 410
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "firstToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    check-cast v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    .line 412
    .restart local v0    # "firstToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->maxShingleSize:I

    if-lt v2, v3, :cond_3

    .line 429
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigramsIfNoShingles:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->noShingleOutput:Z

    if-eqz v2, :cond_2

    .line 430
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    # getter for: Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->minValue:I
    invoke-static {v2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->access$0(Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;)I

    move-result v2

    if-le v2, v4, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->minShingleSize:I

    if-ge v2, v3, :cond_2

    .line 431
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-static {v2, v4}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->access$1(Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;I)V

    .line 433
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->reset()V

    .line 434
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isOutputHere:Z

    .line 435
    return-void

    .line 413
    :cond_3
    if-eqz v0, :cond_4

    .line 414
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->getNextToken(Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;)Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 415
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 416
    const/4 v0, 0x0

    .line 420
    goto :goto_0

    .line 421
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->getNextToken(Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;)Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    move-result-object v1

    .line 422
    .local v1, "nextToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    if-eqz v1, :cond_1

    .line 423
    iget-object v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 283
    const/4 v5, 0x0

    .line 284
    .local v5, "tokenAvailable":Z
    const/4 v0, 0x0

    .line 285
    .local v0, "builtGramSize":I
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->atMinValue()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    iget-object v9, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getValue()I

    move-result v9

    if-ge v6, v9, :cond_4

    .line 286
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->shiftInputWindow()V

    .line 287
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 291
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    iget-object v9, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getValue()I

    move-result v9

    if-lt v6, v9, :cond_3

    .line 292
    const/4 v2, 0x1

    .line 293
    .local v2, "isAllFiller":Z
    const/4 v4, 0x0

    .line 294
    .local v4, "nextToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 295
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;>;"
    const/4 v1, 0x1

    .line 296
    .local v1, "gramNum":I
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getValue()I

    move-result v6

    .line 295
    if-lt v0, v6, :cond_5

    .line 315
    :cond_1
    if-nez v2, :cond_3

    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getValue()I

    move-result v6

    if-ne v0, v6, :cond_3

    .line 316
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    iget-object v6, v6, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->attSource:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v6, p0}, Lorg/apache/lucene/util/AttributeSource;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    .line 317
    iget-object v9, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget-boolean v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isOutputHere:Z

    if-eqz v6, :cond_a

    move v6, v7

    :goto_2
    invoke-interface {v9, v6}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 318
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v6

    iget-object v9, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramBuilder:Ljava/lang/StringBuilder;

    invoke-interface {v6, v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/StringBuilder;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 319
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getValue()I

    move-result v6

    if-le v6, v8, :cond_2

    .line 320
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iget-object v9, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->tokenType:Ljava/lang/String;

    invoke-interface {v6, v9}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 321
    iput-boolean v7, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->noShingleOutput:Z

    .line 323
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v7, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v7

    iget-object v9, v4, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v9

    invoke-interface {v6, v7, v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 324
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->posLenAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    invoke-interface {v6, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;->setPositionLength(I)V

    .line 325
    iput-boolean v8, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isOutputHere:Z

    .line 326
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->advance()V

    .line 327
    const/4 v5, 0x1

    .line 330
    .end local v1    # "gramNum":I
    .end local v2    # "isAllFiller":Z
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;>;"
    .end local v4    # "nextToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    :cond_3
    return v5

    .line 289
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getPreviousValue()I

    move-result v0

    goto/16 :goto_0

    .line 298
    .restart local v1    # "gramNum":I
    .restart local v2    # "isAllFiller":Z
    .restart local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;>;"
    .restart local v4    # "nextToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "nextToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    check-cast v4, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;

    .line 299
    .restart local v4    # "nextToken":Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;
    if-ge v0, v1, :cond_7

    .line 300
    if-lez v0, :cond_6

    .line 301
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramBuilder:Ljava/lang/StringBuilder;

    iget-object v9, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->tokenSeparator:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramBuilder:Ljava/lang/StringBuilder;

    iget-object v9, v4, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v9

    .line 304
    iget-object v10, v4, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v10

    .line 303
    invoke-virtual {v6, v9, v7, v10}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 305
    add-int/lit8 v0, v0, 0x1

    .line 307
    :cond_7
    if-eqz v2, :cond_9

    iget-boolean v6, v4, Lorg/apache/lucene/analysis/shingle/ShingleFilter$InputWindowToken;->isFiller:Z

    if-eqz v6, :cond_9

    .line 308
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->getValue()I

    move-result v6

    if-ne v1, v6, :cond_8

    .line 309
    iget-object v6, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->advance()V

    .line 297
    :cond_8
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 312
    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    :cond_a
    move v6, v8

    .line 317
    goto/16 :goto_2
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 439
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 440
    iget-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->reset()V

    .line 441
    iget-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->inputWindow:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 442
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->nextInputStreamToken:Lorg/apache/lucene/util/AttributeSource;

    .line 443
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isNextInputStreamToken:Z

    .line 444
    iput v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->numFillerTokensToInsert:I

    .line 445
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->isOutputHere:Z

    .line 446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->noShingleOutput:Z

    .line 447
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->exhausted:Z

    .line 448
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigramsIfNoShingles:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigrams:Z

    if-nez v0, :cond_0

    .line 450
    iget-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->minShingleSize:I

    invoke-static {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->access$1(Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;I)V

    .line 452
    :cond_0
    return-void
.end method

.method public setMaxShingleSize(I)V
    .locals 2
    .param p1, "maxShingleSize"    # I

    .prologue
    .line 246
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Max shingle size must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    iput p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->maxShingleSize:I

    .line 250
    return-void
.end method

.method public setMinShingleSize(I)V
    .locals 2
    .param p1, "minShingleSize"    # I

    .prologue
    .line 262
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 263
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Min shingle size must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 265
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->maxShingleSize:I

    if-le p1, v0, :cond_1

    .line 266
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 267
    const-string v1, "Min shingle size must be <= max shingle size"

    .line 266
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_1
    iput p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->minShingleSize:I

    .line 270
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;-><init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    .line 271
    return-void
.end method

.method public setOutputUnigrams(Z)V
    .locals 1
    .param p1, "outputUnigrams"    # Z

    .prologue
    .line 222
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigrams:Z

    .line 223
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    invoke-direct {v0, p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;-><init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->gramSize:Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;

    .line 224
    return-void
.end method

.method public setOutputUnigramsIfNoShingles(Z)V
    .locals 0
    .param p1, "outputUnigramsIfNoShingles"    # Z

    .prologue
    .line 237
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigramsIfNoShingles:Z

    .line 238
    return-void
.end method

.method public setTokenSeparator(Ljava/lang/String;)V
    .locals 0
    .param p1, "tokenSeparator"    # Ljava/lang/String;

    .prologue
    .line 278
    if-nez p1, :cond_0

    const-string p1, ""

    .end local p1    # "tokenSeparator":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->tokenSeparator:Ljava/lang/String;

    .line 279
    return-void
.end method

.method public setTokenType(Ljava/lang/String;)V
    .locals 0
    .param p1, "tokenType"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->tokenType:Ljava/lang/String;

    .line 212
    return-void
.end method
