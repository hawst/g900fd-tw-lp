.class public Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;
.super Ljava/lang/Object;
.source "HunspellDictionary.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;,
        Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;,
        Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;,
        Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;
    }
.end annotation


# static fields
.field private static final ALIAS_KEY:Ljava/lang/String; = "AF"

.field private static final FLAG_KEY:Ljava/lang/String; = "FLAG"

.field private static final IGNORE_CASE_DEFAULT:Z = false

.field private static final LONG_FLAG_TYPE:Ljava/lang/String; = "long"

.field static final NOFLAGS:Lorg/apache/lucene/analysis/hunspell/HunspellWord;

.field private static final NUM_FLAG_TYPE:Ljava/lang/String; = "num"

.field private static final PREFIX_CONDITION_REGEX_PATTERN:Ljava/lang/String; = "%s.*"

.field private static final PREFIX_KEY:Ljava/lang/String; = "PFX"

.field private static final STRICT_AFFIX_PARSING_DEFAULT:Z = true

.field private static final SUFFIX_CONDITION_REGEX_PATTERN:Ljava/lang/String; = ".*%s"

.field private static final SUFFIX_KEY:Ljava/lang/String; = "SFX"

.field private static final UTF8_FLAG_TYPE:Ljava/lang/String; = "UTF-8"


# instance fields
.field private aliasCount:I

.field private aliases:[Ljava/lang/String;

.field private flagParsingStrategy:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

.field private ignoreCase:Z

.field private prefixes:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;>;"
        }
    .end annotation
.end field

.field private suffixes:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;>;"
        }
    .end annotation
.end field

.field private final version:Lorg/apache/lucene/util/Version;

.field private words:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellWord;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/hunspell/HunspellWord;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->NOFLAGS:Lorg/apache/lucene/analysis/hunspell/HunspellWord;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;Lorg/apache/lucene/util/Version;)V
    .locals 2
    .param p1, "affix"    # Ljava/io/InputStream;
    .param p2, "dictionary"    # Ljava/io/InputStream;
    .param p3, "version"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 79
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/InputStream;

    aput-object p2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, v1}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;-><init>(Ljava/io/InputStream;Ljava/util/List;Lorg/apache/lucene/util/Version;Z)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/io/InputStream;Lorg/apache/lucene/util/Version;Z)V
    .locals 2
    .param p1, "affix"    # Ljava/io/InputStream;
    .param p2, "dictionary"    # Ljava/io/InputStream;
    .param p3, "version"    # Lorg/apache/lucene/util/Version;
    .param p4, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 95
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/InputStream;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;-><init>(Ljava/io/InputStream;Ljava/util/List;Lorg/apache/lucene/util/Version;Z)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/util/List;Lorg/apache/lucene/util/Version;Z)V
    .locals 6
    .param p1, "affix"    # Ljava/io/InputStream;
    .param p3, "version"    # Lorg/apache/lucene/util/Version;
    .param p4, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/util/List",
            "<",
            "Ljava/io/InputStream;",
            ">;",
            "Lorg/apache/lucene/util/Version;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 111
    .local p2, "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;-><init>(Ljava/io/InputStream;Ljava/util/List;Lorg/apache/lucene/util/Version;ZZ)V

    .line 112
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/util/List;Lorg/apache/lucene/util/Version;ZZ)V
    .locals 6
    .param p1, "affix"    # Ljava/io/InputStream;
    .param p3, "version"    # Lorg/apache/lucene/util/Version;
    .param p4, "ignoreCase"    # Z
    .param p5, "strictAffixParsing"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/util/List",
            "<",
            "Ljava/io/InputStream;",
            ">;",
            "Lorg/apache/lucene/util/Version;",
            "ZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .local p2, "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    const/4 v5, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v3, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;)V

    iput-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->flagParsingStrategy:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

    .line 60
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    .line 65
    iput v5, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliasCount:I

    .line 128
    iput-object p3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->version:Lorg/apache/lucene/util/Version;

    .line 129
    iput-boolean p4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    .line 130
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->getDictionaryEncoding(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "encoding":Ljava/lang/String;
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->getJavaEncoding(Ljava/lang/String;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 132
    .local v0, "decoder":Ljava/nio/charset/CharsetDecoder;
    invoke-direct {p0, p1, v0, p5}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->readAffixFile(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;Z)V

    .line 133
    new-instance v3, Lorg/apache/lucene/analysis/util/CharArrayMap;

    const v4, 0xffff

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    invoke-direct {v3, p3, v4, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    iput-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->words:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 134
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 137
    return-void

    .line 134
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/InputStream;

    .line 135
    .local v1, "dictionary":Ljava/io/InputStream;
    invoke-direct {p0, v1, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->readDictionaryFile(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    goto :goto_0
.end method

.method private getAliasValue(I)Ljava/lang/String;
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 412
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliases:[Ljava/lang/String;

    add-int/lit8 v2, p1, -0x1

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 413
    :catch_0
    move-exception v0

    .line 414
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad flag alias number:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private getDictionaryEncoding(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p1, "affix"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 282
    .local v1, "encoding":Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 284
    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .local v0, "ch":I
    if-gez v0, :cond_4

    .line 293
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x23

    if-eq v2, v3, :cond_3

    .line 295
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 297
    :cond_3
    if-gez v0, :cond_0

    .line 298
    new-instance v2, Ljava/text/ParseException;

    const-string v3, "Unexpected end of affix file."

    invoke-direct {v2, v3, v5}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 285
    :cond_4
    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    .line 288
    const/16 v2, 0xd

    if-eq v0, v2, :cond_1

    .line 289
    int-to-char v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 302
    :cond_5
    const-string v2, "SET "

    invoke-virtual {v1, v5, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 304
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 306
    :cond_6
    new-instance v2, Ljava/text/ParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The first non-comment line in the affix file must be a \'SET charset\', was: \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 307
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 306
    invoke-direct {v2, v3, v5}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2
.end method

.method private getFlagParsingStrategy(Ljava/lang/String;)Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;
    .locals 4
    .param p1, "flagLine"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 330
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 332
    .local v0, "flagType":Ljava/lang/String;
    const-string v1, "num"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    new-instance v1, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;)V

    .line 337
    :goto_0
    return-object v1

    .line 334
    :cond_0
    const-string v1, "UTF-8"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 335
    new-instance v1, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$SimpleFlagParsingStrategy;)V

    goto :goto_0

    .line 336
    :cond_1
    const-string v1, "long"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 337
    new-instance v1, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;)V

    goto :goto_0

    .line 340
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown flag type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getJavaEncoding(Ljava/lang/String;)Ljava/nio/charset/CharsetDecoder;
    .locals 2
    .param p1, "encoding"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-static {p1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    .line 320
    .local v0, "charset":Ljava/nio/charset/Charset;
    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v1

    return-object v1
.end method

.method private parseAffix(Lorg/apache/lucene/analysis/util/CharArrayMap;Ljava/lang/String;Ljava/io/LineNumberReader;Ljava/lang/String;Z)V
    .locals 17
    .param p2, "header"    # Ljava/lang/String;
    .param p3, "reader"    # Ljava/io/LineNumberReader;
    .param p4, "conditionPattern"    # Ljava/lang/String;
    .param p5, "strict"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/io/LineNumberReader;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 218
    .local p1, "affixes":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;>;"
    const-string v14, "\\s+"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 220
    .local v4, "args":[Ljava/lang/String;
    const/4 v14, 0x2

    aget-object v14, v4, v14

    const-string v15, "Y"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 222
    .local v6, "crossProduct":Z
    const/4 v14, 0x3

    aget-object v14, v4, v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 223
    .local v12, "numLines":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-lt v9, v12, :cond_0

    .line 269
    return-void

    .line 224
    :cond_0
    invoke-virtual/range {p3 .. p3}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .line 225
    .local v10, "line":Ljava/lang/String;
    const-string v14, "\\s+"

    invoke-virtual {v10, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 227
    .local v13, "ruleArgs":[Ljava/lang/String;
    array-length v14, v13

    const/4 v15, 0x5

    if-ge v14, v15, :cond_1

    .line 228
    if-eqz p5, :cond_4

    .line 229
    new-instance v14, Ljava/text/ParseException;

    const-string v15, "The affix file contains a rule with less than five elements"

    invoke-virtual/range {p3 .. p3}, Ljava/io/LineNumberReader;->getLineNumber()I

    move-result v16

    invoke-direct/range {v14 .. v16}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v14

    .line 234
    :cond_1
    new-instance v1, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;

    invoke-direct {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;-><init>()V

    .line 236
    .local v1, "affix":Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->flagParsingStrategy:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

    const/4 v15, 0x1

    aget-object v15, v13, v15

    invoke-virtual {v14, v15}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;->parseFlag(Ljava/lang/String;)C

    move-result v14

    invoke-virtual {v1, v14}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setFlag(C)V

    .line 237
    const/4 v14, 0x2

    aget-object v14, v13, v14

    const-string v15, "0"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    const-string v14, ""

    :goto_1
    invoke-virtual {v1, v14}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setStrip(Ljava/lang/String;)V

    .line 239
    const/4 v14, 0x3

    aget-object v2, v13, v14

    .line 241
    .local v2, "affixArg":Ljava/lang/String;
    const/16 v14, 0x2f

    invoke-virtual {v2, v14}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 242
    .local v8, "flagSep":I
    const/4 v14, -0x1

    if-eq v8, v14, :cond_6

    .line 243
    add-int/lit8 v14, v8, 0x1

    invoke-virtual {v2, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 245
    .local v7, "flagPart":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliasCount:I

    if-lez v14, :cond_2

    .line 246
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->getAliasValue(I)Ljava/lang/String;

    move-result-object v7

    .line 249
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->flagParsingStrategy:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

    invoke-virtual {v14, v7}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;->parseFlags(Ljava/lang/String;)[C

    move-result-object v3

    .line 250
    .local v3, "appendFlags":[C
    invoke-static {v3}, Ljava/util/Arrays;->sort([C)V

    .line 251
    invoke-virtual {v1, v3}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setAppendFlags([C)V

    .line 252
    const/4 v14, 0x0

    invoke-virtual {v2, v14, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v14}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setAppend(Ljava/lang/String;)V

    .line 257
    .end local v3    # "appendFlags":[C
    .end local v7    # "flagPart":Ljava/lang/String;
    :goto_2
    const/4 v14, 0x4

    aget-object v5, v13, v14

    .line 258
    .local v5, "condition":Ljava/lang/String;
    sget-object v14, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v5, v15, v16

    move-object/from16 v0, p4

    invoke-static {v14, v0, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v1, v5, v14}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setCondition(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual {v1, v6}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setCrossProduct(Z)V

    .line 261
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getAppend()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get(Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    .line 262
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    if-nez v11, :cond_3

    .line 263
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .restart local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getAppend()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v11}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    :cond_3
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    .end local v1    # "affix":Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    .end local v2    # "affixArg":Ljava/lang/String;
    .end local v5    # "condition":Ljava/lang/String;
    .end local v8    # "flagSep":I
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 237
    .restart local v1    # "affix":Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    :cond_5
    const/4 v14, 0x2

    aget-object v14, v13, v14

    goto :goto_1

    .line 254
    .restart local v2    # "affixArg":Ljava/lang/String;
    .restart local v8    # "flagSep":I
    :cond_6
    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->setAppend(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private parseAlias(Ljava/lang/String;)V
    .locals 6
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 400
    const-string v2, "\\s+"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 401
    .local v1, "ruleArgs":[Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliases:[Ljava/lang/String;

    if-nez v2, :cond_0

    .line 403
    aget-object v2, v1, v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 404
    .local v0, "count":I
    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliases:[Ljava/lang/String;

    .line 408
    .end local v0    # "count":I
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliases:[Ljava/lang/String;

    iget v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliasCount:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliasCount:I

    aget-object v4, v1, v5

    aput-object v4, v2, v3

    goto :goto_0
.end method

.method private readAffixFile(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;Z)V
    .locals 6
    .param p1, "affixStream"    # Ljava/io/InputStream;
    .param p2, "decoder"    # Ljava/nio/charset/CharsetDecoder;
    .param p3, "strict"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    .line 183
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap;

    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->version:Lorg/apache/lucene/util/Version;

    iget-boolean v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    invoke-direct {v0, v1, v5, v4}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->prefixes:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 184
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap;

    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->version:Lorg/apache/lucene/util/Version;

    iget-boolean v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    invoke-direct {v0, v1, v5, v4}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->suffixes:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 186
    new-instance v3, Ljava/io/LineNumberReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-direct {v3, v0}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    .line 187
    .local v3, "reader":Ljava/io/LineNumberReader;
    const/4 v2, 0x0

    .line 188
    .local v2, "line":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 201
    return-void

    .line 189
    :cond_1
    const-string v0, "AF"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->parseAlias(Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :cond_2
    const-string v0, "PFX"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->prefixes:Lorg/apache/lucene/analysis/util/CharArrayMap;

    const-string v4, "%s.*"

    move-object v0, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->parseAffix(Lorg/apache/lucene/analysis/util/CharArrayMap;Ljava/lang/String;Ljava/io/LineNumberReader;Ljava/lang/String;Z)V

    goto :goto_0

    .line 193
    :cond_3
    const-string v0, "SFX"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 194
    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->suffixes:Lorg/apache/lucene/analysis/util/CharArrayMap;

    const-string v4, ".*%s"

    move-object v0, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->parseAffix(Lorg/apache/lucene/analysis/util/CharArrayMap;Ljava/lang/String;Ljava/io/LineNumberReader;Ljava/lang/String;Z)V

    goto :goto_0

    .line 195
    :cond_4
    const-string v0, "FLAG"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->getFlagParsingStrategy(Ljava/lang/String;)Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->flagParsingStrategy:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

    goto :goto_0
.end method

.method private readDictionaryFile(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V
    .locals 11
    .param p1, "dictionary"    # Ljava/io/InputStream;
    .param p2, "decoder"    # Ljava/nio/charset/CharsetDecoder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    .line 351
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, p1, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/CharsetDecoder;)V

    invoke-direct {v7, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 353
    .local v7, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 354
    .local v5, "line":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 358
    .local v6, "numEntries":I
    :goto_0
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    .line 393
    return-void

    .line 362
    :cond_0
    const/16 v9, 0x2f

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 363
    .local v4, "flagSep":I
    if-ne v4, v10, :cond_3

    .line 364
    sget-object v8, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->NOFLAGS:Lorg/apache/lucene/analysis/hunspell/HunspellWord;

    .line 365
    .local v8, "wordForm":Lorg/apache/lucene/analysis/hunspell/HunspellWord;
    move-object v2, v5

    .line 386
    .local v2, "entry":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v9, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->words:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v9, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get(Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 387
    .local v1, "entries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellWord;>;"
    if-nez v1, :cond_2

    .line 388
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellWord;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 389
    .restart local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellWord;>;"
    iget-object v9, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->words:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v9, v2, v1}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    :cond_2
    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 369
    .end local v1    # "entries":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellWord;>;"
    .end local v2    # "entry":Ljava/lang/String;
    .end local v8    # "wordForm":Lorg/apache/lucene/analysis/hunspell/HunspellWord;
    :cond_3
    const/16 v9, 0x9

    invoke-virtual {v5, v9, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 370
    .local v0, "end":I
    if-ne v0, v10, :cond_4

    .line 371
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    .line 373
    :cond_4
    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v5, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 374
    .local v3, "flagPart":Ljava/lang/String;
    iget v9, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->aliasCount:I

    if-lez v9, :cond_5

    .line 375
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-direct {p0, v9}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->getAliasValue(I)Ljava/lang/String;

    move-result-object v3

    .line 378
    :cond_5
    new-instance v8, Lorg/apache/lucene/analysis/hunspell/HunspellWord;

    iget-object v9, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->flagParsingStrategy:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;

    invoke-virtual {v9, v3}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;->parseFlags(Ljava/lang/String;)[C

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/lucene/analysis/hunspell/HunspellWord;-><init>([C)V

    .line 379
    .restart local v8    # "wordForm":Lorg/apache/lucene/analysis/hunspell/HunspellWord;
    invoke-virtual {v8}, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->getFlags()[C

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->sort([C)V

    .line 380
    const/4 v9, 0x0

    invoke-virtual {v5, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 381
    .restart local v2    # "entry":Ljava/lang/String;
    iget-boolean v9, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    if-eqz v9, :cond_1

    .line 382
    sget-object v9, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v2, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public getVersion()Lorg/apache/lucene/util/Version;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->version:Lorg/apache/lucene/util/Version;

    return-object v0
.end method

.method public isIgnoreCase()Z
    .locals 1

    .prologue
    .line 508
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->ignoreCase:Z

    return v0
.end method

.method public lookupPrefix([CII)Ljava/util/List;
    .locals 1
    .param p1, "word"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CII)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->prefixes:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public lookupSuffix([CII)Ljava/util/List;
    .locals 1
    .param p1, "word"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CII)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->suffixes:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public lookupWord([CII)Ljava/util/List;
    .locals 1
    .param p1, "word"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CII)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellWord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->words:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
