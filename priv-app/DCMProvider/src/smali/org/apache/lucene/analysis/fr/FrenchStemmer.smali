.class public Lorg/apache/lucene/analysis/fr/FrenchStemmer;
.super Ljava/lang/Object;
.source "FrenchStemmer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final locale:Ljava/util/Locale;


# instance fields
.field private R0:Ljava/lang/String;

.field private R1:Ljava/lang/String;

.field private R2:Ljava/lang/String;

.field private RV:Ljava/lang/String;

.field private modified:Z

.field private sb:Ljava/lang/StringBuilder;

.field private suite:Z

.field private tb:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Ljava/util/Locale;

    const-string v1, "fr"

    const-string v2, "FR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->locale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->tb:Ljava/lang/StringBuilder;

    .line 34
    return-void
.end method

.method private deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "search"    # [Ljava/lang/String;
    .param p3, "prefix"    # Ljava/lang/String;
    .param p4, "without"    # Z

    .prologue
    const/4 v5, 0x1

    .line 419
    if-eqz p1, :cond_0

    .line 421
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_1

    .line 438
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 422
    .restart local v0    # "i":I
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 424
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    aget-object v4, p2, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 425
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 426
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 429
    :cond_2
    if-eqz p4, :cond_3

    aget-object v1, p2, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 431
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 432
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 433
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 421
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "search"    # [Ljava/lang/String;
    .param p3, "prefix"    # Ljava/lang/String;
    .param p4, "without"    # Z
    .param p5, "from"    # Ljava/lang/String;
    .param p6, "replace"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 451
    if-eqz p1, :cond_0

    .line 453
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_1

    .line 477
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 454
    .restart local v0    # "i":I
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 456
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    aget-object v4, p2, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 457
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 458
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 461
    :cond_2
    if-eqz p5, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 463
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    aget-object v4, p2, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3, p6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 464
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 465
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 468
    :cond_3
    if-eqz p4, :cond_4

    aget-object v1, p2, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 470
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 471
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 472
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto/16 :goto_1

    .line 453
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method private deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "suffix"    # [Ljava/lang/String;

    .prologue
    .line 511
    if-eqz p1, :cond_0

    .line 513
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_1

    .line 523
    .end local v0    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 514
    .restart local v0    # "i":I
    :cond_1
    aget-object v1, p2, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 516
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 517
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 518
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 513
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private deleteFromIfPrecededIn(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "search"    # [Ljava/lang/String;
    .param p3, "from"    # Ljava/lang/String;
    .param p4, "prefix"    # Ljava/lang/String;

    .prologue
    .line 357
    const/4 v0, 0x0

    .line 358
    .local v0, "found":Z
    if-eqz p1, :cond_0

    .line 360
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-lt v1, v2, :cond_1

    .line 373
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return v0

    .line 361
    .restart local v1    # "i":I
    :cond_1
    aget-object v2, p2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 363
    if-eqz p3, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p2, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 365
    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    aget-object v4, p2, v1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 366
    const/4 v0, 0x1

    .line 367
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 360
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private deleteFromIfTestVowelBeforeIn(Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 6
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "search"    # [Ljava/lang/String;
    .param p3, "vowel"    # Z
    .param p4, "from"    # Ljava/lang/String;

    .prologue
    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "found":Z
    if-eqz p1, :cond_0

    if-eqz p4, :cond_0

    .line 389
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p2

    if-lt v1, v3, :cond_1

    .line 407
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return v0

    .line 390
    .restart local v1    # "i":I
    :cond_1
    aget-object v3, p2, v1

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 392
    aget-object v3, p2, v1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v3, v4, :cond_2

    .line 394
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    aget-object v5, p2, v1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    .line 395
    .local v2, "test":Z
    if-ne v2, p3, :cond_2

    .line 397
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    aget-object v5, p2, v1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 398
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 399
    const/4 v0, 0x1

    .line 400
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 389
    .end local v2    # "test":Z
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private isStemmable(Ljava/lang/String;)Z
    .locals 5
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 689
    const/4 v2, 0x0

    .line 690
    .local v2, "upper":Z
    const/4 v1, -0x1

    .line 691
    .local v1, "first":I
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 711
    if-lez v1, :cond_3

    .line 714
    :cond_0
    :goto_1
    return v3

    .line 693
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isLetter(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 697
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 698
    if-nez v2, :cond_0

    .line 704
    move v1, v0

    .line 705
    const/4 v2, 0x1

    .line 691
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 714
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private isVowel(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 532
    sparse-switch p1, :sswitch_data_0

    .line 554
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 552
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 532
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x79 -> :sswitch_0
        0xe0 -> :sswitch_0
        0xe2 -> :sswitch_0
        0xe8 -> :sswitch_0
        0xe9 -> :sswitch_0
        0xea -> :sswitch_0
        0xeb -> :sswitch_0
        0xee -> :sswitch_0
        0xef -> :sswitch_0
        0xf4 -> :sswitch_0
        0xf9 -> :sswitch_0
        0xfb -> :sswitch_0
        0xfc -> :sswitch_0
    .end sparse-switch
.end method

.method private replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "search"    # [Ljava/lang/String;
    .param p3, "replace"    # Ljava/lang/String;

    .prologue
    .line 487
    const/4 v0, 0x0

    .line 488
    .local v0, "found":Z
    if-eqz p1, :cond_0

    .line 490
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p2

    if-lt v1, v2, :cond_1

    .line 501
    .end local v1    # "i":I
    :cond_0
    :goto_1
    return v0

    .line 491
    .restart local v1    # "i":I
    :cond_1
    aget-object v2, p2, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 493
    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    aget-object v4, p2, v1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4, p3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 495
    const/4 v0, 0x1

    .line 496
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_1

    .line 490
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private retrieveR(Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 7
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v4, 0x0

    const/4 v6, -0x1

    .line 566
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    .line 567
    .local v2, "len":I
    const/4 v3, -0x1

    .line 568
    .local v3, "pos":I
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 575
    :goto_1
    if-le v3, v6, :cond_0

    .line 577
    const/4 v1, -0x1

    .line 578
    .local v1, "consonne":I
    move v0, v3

    :goto_2
    if-lt v0, v2, :cond_3

    .line 585
    :goto_3
    if-le v1, v6, :cond_0

    add-int/lit8 v5, v1, 0x1

    if-ge v5, v2, :cond_0

    .line 586
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v4, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 591
    .end local v1    # "consonne":I
    :cond_0
    return-object v4

    .line 569
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v5

    invoke-direct {p0, v5}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 571
    move v3, v0

    .line 572
    goto :goto_1

    .line 568
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 579
    .restart local v1    # "consonne":I
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v5

    invoke-direct {p0, v5}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v5

    if-nez v5, :cond_4

    .line 581
    move v1, v0

    .line 582
    goto :goto_3

    .line 578
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private retrieveRV(Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 6
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x3

    .line 603
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 604
    .local v1, "len":I
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-le v4, v5, :cond_0

    .line 606
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 607
    invoke-virtual {p1, v5, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 626
    :cond_0
    :goto_0
    return-object v3

    .line 611
    :cond_1
    const/4 v2, 0x0

    .line 612
    .local v2, "pos":I
    const/4 v0, 0x1

    .local v0, "c":I
    :goto_1
    if-lt v0, v1, :cond_2

    .line 619
    :goto_2
    add-int/lit8 v4, v2, 0x1

    if-ge v4, v1, :cond_0

    .line 620
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v3, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 613
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-direct {p0, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 615
    move v2, v0

    .line 616
    goto :goto_2

    .line 612
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setStrings()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->retrieveRV(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->retrieveR(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R1:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R1:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->tb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->tb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 149
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->tb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R1:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->tb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->retrieveR(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    .line 154
    :goto_0
    return-void

    .line 153
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    goto :goto_0
.end method

.method private step1()V
    .locals 15

    .prologue
    .line 161
    const/16 v0, 0xa

    new-array v13, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "ances"

    aput-object v1, v13, v0

    const/4 v0, 0x1

    const-string v1, "iqUes"

    aput-object v1, v13, v0

    const/4 v0, 0x2

    const-string v1, "ismes"

    aput-object v1, v13, v0

    const/4 v0, 0x3

    const-string v1, "ables"

    aput-object v1, v13, v0

    const/4 v0, 0x4

    const-string v1, "istes"

    aput-object v1, v13, v0

    const/4 v0, 0x5

    const-string v1, "ance"

    aput-object v1, v13, v0

    const/4 v0, 0x6

    const-string v1, "iqUe"

    aput-object v1, v13, v0

    const/4 v0, 0x7

    const-string v1, "isme"

    aput-object v1, v13, v0

    const/16 v0, 0x8

    const-string v1, "able"

    aput-object v1, v13, v0

    const/16 v0, 0x9

    const-string v1, "iste"

    aput-object v1, v13, v0

    .line 162
    .local v13, "suffix":[Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    invoke-direct {p0, v0, v13}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "logies"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "logie"

    aput-object v4, v1, v3

    const-string v3, "log"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 165
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "usions"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "utions"

    aput-object v4, v1, v3

    const/4 v3, 0x2

    const-string v4, "usion"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string v4, "ution"

    aput-object v4, v1, v3

    const-string v3, "u"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 166
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ences"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ence"

    aput-object v4, v1, v3

    const-string v3, "ent"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 168
    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "atrices"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "ateurs"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "ations"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "atrice"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "ateur"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "ation"

    aput-object v1, v2, v0

    .line 169
    .local v2, "search":[Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const-string v3, "ic"

    const/4 v4, 0x1

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v6, "iqU"

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "ements"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "ement"

    aput-object v1, v5, v0

    const-string v6, "eus"

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v9, "eux"

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ements"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ement"

    aput-object v4, v1, v3

    const-string v3, "ativ"

    const/4 v4, 0x0

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 173
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ements"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ement"

    aput-object v4, v1, v3

    const-string v3, "iv"

    const/4 v4, 0x0

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 174
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ements"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ement"

    aput-object v4, v1, v3

    const-string v3, "abl"

    const/4 v4, 0x0

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 175
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ements"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ement"

    aput-object v4, v1, v3

    const-string v3, "iqU"

    const/4 v4, 0x0

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 177
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R1:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "issements"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "issement"

    aput-object v4, v1, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFromIfTestVowelBeforeIn(Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)Z

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ements"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ement"

    aput-object v4, v1, v3

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V

    .line 180
    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "it\u00e9s"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "it\u00e9"

    aput-object v1, v5, v0

    const-string v6, "abil"

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v9, "abl"

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "it\u00e9s"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "it\u00e9"

    aput-object v1, v5, v0

    const-string v6, "ic"

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v9, "iqU"

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "it\u00e9s"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "it\u00e9"

    aput-object v4, v1, v3

    const-string v3, "iv"

    const/4 v4, 0x1

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 184
    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "ifs"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "ives"

    aput-object v1, v5, v0

    const/4 v0, 0x2

    const-string v1, "if"

    aput-object v1, v5, v0

    const/4 v0, 0x3

    const-string v1, "ive"

    aput-object v1, v5, v0

    .line 185
    .local v5, "autre":[Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const-string v6, "icat"

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v9, "iqU"

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const-string v6, "at"

    const/4 v7, 0x1

    iget-object v8, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const-string v9, "iqU"

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "eaux"

    aput-object v4, v1, v3

    const-string v3, "eau"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 190
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R1:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "aux"

    aput-object v4, v1, v3

    const-string v3, "al"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 192
    iget-object v7, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v0, 0x2

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "euses"

    aput-object v1, v8, v0

    const/4 v0, 0x1

    const-string v1, "euse"

    aput-object v1, v8, v0

    const-string v9, ""

    const/4 v10, 0x1

    iget-object v11, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R1:Ljava/lang/String;

    const-string v12, "eux"

    move-object v6, p0

    invoke-direct/range {v6 .. v12}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFromElseReplace(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "eux"

    aput-object v4, v1, v3

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V

    .line 197
    const/4 v14, 0x0

    .line 198
    .local v14, "temp":Z
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "amment"

    aput-object v4, v1, v3

    const-string v3, "ant"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    .line 199
    if-eqz v14, :cond_0

    .line 200
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    .line 201
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "emment"

    aput-object v4, v1, v3

    const-string v3, "ent"

    invoke-direct {p0, v0, v1, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    .line 202
    if-eqz v14, :cond_1

    .line 203
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    .line 204
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ments"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "ment"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFromIfTestVowelBeforeIn(Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v14

    .line 205
    if-eqz v14, :cond_2

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    .line 208
    :cond_2
    return-void
.end method

.method private step2a()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 219
    const/16 v1, 0x24

    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, "\u00eemes"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "\u00eetes"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "iraIent"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "irait"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "irais"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "irai"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "iras"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ira"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 220
    const-string v2, "irent"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "iriez"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "irez"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "irions"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "irons"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "iront"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 221
    const-string v2, "issaIent"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "issais"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "issantes"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "issante"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "issants"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "issant"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 222
    const-string v2, "issait"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "issais"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "issions"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "issons"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "issiez"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "issez"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "issent"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 223
    const-string v2, "isses"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "isse"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "ir"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "is"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "\u00eet"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "ies"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "ie"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "i"

    aput-object v2, v0, v1

    .line 224
    .local v0, "search":[Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v3, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFromIfTestVowelBeforeIn(Ljava/lang/String;[Ljava/lang/String;ZLjava/lang/String;)Z

    move-result v1

    return v1
.end method

.method private step2b()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 233
    const/16 v2, 0x13

    new-array v1, v2, [Ljava/lang/String;

    const-string v2, "eraIent"

    aput-object v2, v1, v5

    const-string v2, "erais"

    aput-object v2, v1, v4

    const-string v2, "erait"

    aput-object v2, v1, v6

    const-string v2, "erai"

    aput-object v2, v1, v7

    const-string v2, "eras"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "erions"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "eriez"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    .line 234
    const-string v3, "erons"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "eront"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "erez"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "\u00e8rent"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "era"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "\u00e9es"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "iez"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    .line 235
    const-string/jumbo v3, "\u00e9e"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "\u00e9s"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "er"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "ez"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "\u00e9"

    aput-object v3, v1, v2

    .line 236
    .local v1, "suffix":[Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    invoke-direct {p0, v2, v1}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V

    .line 238
    const/16 v2, 0x19

    new-array v0, v2, [Ljava/lang/String;

    const-string v2, "assions"

    aput-object v2, v0, v5

    const-string v2, "assiez"

    aput-object v2, v0, v4

    const-string v2, "assent"

    aput-object v2, v0, v6

    const-string v2, "asses"

    aput-object v2, v0, v7

    const-string v2, "asse"

    aput-object v2, v0, v8

    const/4 v2, 0x5

    const-string v3, "aIent"

    aput-object v3, v0, v2

    const/4 v2, 0x6

    .line 239
    const-string v3, "antes"

    aput-object v3, v0, v2

    const/4 v2, 0x7

    const-string v3, "aIent"

    aput-object v3, v0, v2

    const/16 v2, 0x8

    const-string v3, "Aient"

    aput-object v3, v0, v2

    const/16 v2, 0x9

    const-string v3, "ante"

    aput-object v3, v0, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "\u00e2mes"

    aput-object v3, v0, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "\u00e2tes"

    aput-object v3, v0, v2

    const/16 v2, 0xc

    const-string v3, "ants"

    aput-object v3, v0, v2

    const/16 v2, 0xd

    const-string v3, "ant"

    aput-object v3, v0, v2

    const/16 v2, 0xe

    .line 240
    const-string v3, "ait"

    aput-object v3, v0, v2

    const/16 v2, 0xf

    const-string v3, "a\u00eet"

    aput-object v3, v0, v2

    const/16 v2, 0x10

    const-string v3, "ais"

    aput-object v3, v0, v2

    const/16 v2, 0x11

    const-string v3, "Ait"

    aput-object v3, v0, v2

    const/16 v2, 0x12

    const-string v3, "A\u00eet"

    aput-object v3, v0, v2

    const/16 v2, 0x13

    const-string v3, "Ais"

    aput-object v3, v0, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "\u00e2t"

    aput-object v3, v0, v2

    const/16 v2, 0x15

    const-string v3, "as"

    aput-object v3, v0, v2

    const/16 v2, 0x16

    const-string v3, "ai"

    aput-object v3, v0, v2

    const/16 v2, 0x17

    const-string v3, "Ai"

    aput-object v3, v0, v2

    const/16 v2, 0x18

    const-string v3, "a"

    aput-object v3, v0, v2

    .line 241
    .local v0, "search":[Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const-string v3, "e"

    invoke-direct {p0, v2, v0, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteButSuffixFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 243
    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    new-array v3, v4, [Ljava/lang/String;

    const-string v4, "ions"

    aput-object v4, v3, v5

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method private step3()V
    .locals 4

    .prologue
    .line 251
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 253
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 254
    .local v0, "ch":C
    const/16 v1, 0x59

    if-ne v0, v1, :cond_1

    .line 256
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x69

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 257
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    .line 265
    .end local v0    # "ch":C
    :cond_0
    :goto_0
    return-void

    .line 259
    .restart local v0    # "ch":C
    :cond_1
    const/16 v1, 0xe7

    if-ne v0, v1, :cond_0

    .line 261
    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x63

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 262
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    goto :goto_0
.end method

.method private step4()V
    .locals 9

    .prologue
    const/16 v5, 0x73

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 272
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-le v3, v7, :cond_0

    .line 274
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    .line 275
    .local v1, "ch":C
    if-ne v1, v5, :cond_0

    .line 277
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    .line 278
    .local v0, "b":C
    const/16 v3, 0x61

    if-eq v0, v3, :cond_0

    const/16 v3, 0x69

    if-eq v0, v3, :cond_0

    const/16 v3, 0x6f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x75

    if-eq v0, v3, :cond_0

    const/16 v3, 0xe8

    if-eq v0, v3, :cond_0

    if-eq v0, v5, :cond_0

    .line 280
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 281
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    .line 285
    .end local v0    # "b":C
    .end local v1    # "ch":C
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "ion"

    aput-object v5, v4, v8

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const-string v6, "s"

    invoke-direct {p0, v3, v4, v5, v6}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFromIfPrecededIn(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 286
    .local v2, "found":Z
    if-nez v2, :cond_1

    .line 287
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R2:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "ion"

    aput-object v5, v4, v8

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const-string v6, "t"

    invoke-direct {p0, v3, v4, v5, v6}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFromIfPrecededIn(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 289
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "I\u00e8re"

    aput-object v5, v4, v8

    const-string v5, "i\u00e8re"

    aput-object v5, v4, v7

    const/4 v5, 0x2

    const-string v6, "Ier"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "ier"

    aput-object v6, v4, v5

    const-string v5, "i"

    invoke-direct {p0, v3, v4, v5}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->replaceFrom(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    .line 290
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "e"

    aput-object v5, v4, v8

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFrom(Ljava/lang/String;[Ljava/lang/String;)V

    .line 291
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "\u00eb"

    aput-object v5, v4, v8

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v6, "gu"

    invoke-direct {p0, v3, v4, v5, v6}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->deleteFromIfPrecededIn(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 292
    return-void
.end method

.method private step5()V
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v1, "enn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v1, "onn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v1, "ett"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v1, "ell"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    const-string v1, "eill"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 304
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    .line 307
    :cond_1
    return-void
.end method

.method private step6()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 314
    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 316
    const/4 v4, 0x0

    .line 317
    .local v4, "seenVowel":Z
    const/4 v3, 0x0

    .line 318
    .local v3, "seenConson":Z
    const/4 v2, -0x1

    .line 319
    .local v2, "pos":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_0
    if-gt v1, v6, :cond_2

    .line 342
    :cond_0
    :goto_1
    if-le v2, v6, :cond_1

    if-eqz v3, :cond_1

    if-nez v4, :cond_1

    .line 343
    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    const/16 v6, 0x65

    invoke-virtual {v5, v2, v6}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 345
    .end local v1    # "i":I
    .end local v2    # "pos":I
    .end local v3    # "seenConson":Z
    .end local v4    # "seenVowel":Z
    :cond_1
    return-void

    .line 321
    .restart local v1    # "i":I
    .restart local v2    # "pos":I
    .restart local v3    # "seenConson":Z
    .restart local v4    # "seenVowel":Z
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->R0:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 322
    .local v0, "ch":C
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 324
    if-nez v4, :cond_4

    .line 326
    const/16 v5, 0xe9

    if-eq v0, v5, :cond_3

    const/16 v5, 0xe8

    if-ne v0, v5, :cond_4

    .line 328
    :cond_3
    move v2, v1

    .line 329
    goto :goto_1

    .line 332
    :cond_4
    const/4 v4, 0x1

    .line 319
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 336
    :cond_5
    if-nez v4, :cond_0

    .line 339
    const/4 v3, 0x1

    goto :goto_2
.end method

.method private treatVowels(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 9
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v8, 0x75

    const/16 v7, 0x71

    const/16 v6, 0x79

    const/16 v5, 0x59

    const/16 v4, 0x55

    .line 640
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 680
    return-object p1

    .line 641
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    .line 643
    .local v1, "ch":C
    if-nez v0, :cond_2

    .line 645
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 647
    if-ne v1, v6, :cond_1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 648
    invoke-virtual {p1, v0, v5}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 640
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 651
    :cond_2
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_4

    .line 653
    if-ne v1, v8, :cond_3

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v7, :cond_3

    .line 654
    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 655
    :cond_3
    if-ne v1, v6, :cond_1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 656
    invoke-virtual {p1, v0, v5}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 660
    :cond_4
    if-ne v1, v8, :cond_5

    .line 662
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v7, :cond_8

    .line 663
    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 667
    :cond_5
    :goto_2
    const/16 v2, 0x69

    if-ne v1, v2, :cond_6

    .line 669
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_6

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 670
    const/16 v2, 0x49

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 672
    :cond_6
    if-ne v1, v6, :cond_1

    .line 674
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-nez v2, :cond_7

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 675
    :cond_7
    invoke-virtual {p1, v0, v5}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 664
    :cond_8
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_5

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isVowel(C)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 665
    invoke-virtual {p1, v0, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_2
.end method


# virtual methods
.method protected stem(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->isStemmable(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    .line 134
    :goto_0
    return-object v0

    .line 99
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 102
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 103
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    .line 107
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    .line 109
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->treatVowels(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    .line 111
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->setStrings()V

    .line 113
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step1()V

    .line 115
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    if-eqz v0, :cond_2

    .line 117
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->RV:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step2a()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    .line 120
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    if-nez v0, :cond_2

    .line 121
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step2b()V

    .line 125
    :cond_2
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->modified:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->suite:Z

    if-eqz v0, :cond_4

    .line 126
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step3()V

    .line 130
    :goto_1
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step5()V

    .line 132
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step6()V

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 128
    :cond_4
    invoke-direct {p0}, Lorg/apache/lucene/analysis/fr/FrenchStemmer;->step4()V

    goto :goto_1
.end method
