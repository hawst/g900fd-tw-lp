.class public final Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "HindiNormalizationFilter.java"


# instance fields
.field private final keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final normalizer:Lorg/apache/lucene/analysis/hi/HindiNormalizer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 41
    new-instance v0, Lorg/apache/lucene/analysis/hi/HindiNormalizer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/hi/HindiNormalizer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->normalizer:Lorg/apache/lucene/analysis/hi/HindiNormalizer;

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 43
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 47
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v1, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->normalizer:Lorg/apache/lucene/analysis/hi/HindiNormalizer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    .line 54
    iget-object v3, p0, Lorg/apache/lucene/analysis/hi/HindiNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    .line 53
    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/hi/HindiNormalizer;->normalize([CI)I

    move-result v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 55
    :cond_0
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
