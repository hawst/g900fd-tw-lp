.class public final Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "PatternReplaceFilter.java"


# instance fields
.field private final all:Z

.field private final m:Ljava/util/regex/Matcher;

.field private final p:Ljava/util/regex/Pattern;

.field private final replacement:Ljava/lang/String;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/regex/Pattern;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "p"    # Ljava/util/regex/Pattern;
    .param p3, "replacement"    # Ljava/lang/String;
    .param p4, "all"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 44
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 63
    iput-object p2, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->p:Ljava/util/regex/Pattern;

    .line 64
    if-nez p3, :cond_0

    const-string p3, ""

    .end local p3    # "replacement":Ljava/lang/String;
    :cond_0
    iput-object p3, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->replacement:Ljava/lang/String;

    .line 65
    iput-boolean p4, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->all:Z

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->m:Ljava/util/regex/Matcher;

    .line 67
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 80
    :goto_0
    return v1

    .line 73
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->m:Ljava/util/regex/Matcher;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->m:Ljava/util/regex/Matcher;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->all:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->m:Ljava/util/regex/Matcher;

    iget-object v2, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->replacement:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "transformed":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 80
    .end local v0    # "transformed":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 76
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->m:Ljava/util/regex/Matcher;

    iget-object v2, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;->replacement:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
