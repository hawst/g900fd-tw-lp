.class public final Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "KeywordAttributeImpl.java"

# interfaces
.implements Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;


# instance fields
.field private keyword:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    .line 33
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 37
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 38
    .local v0, "attr":Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->setKeyword(Z)V

    .line 39
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p0, p1, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_2

    move v1, v2

    .line 51
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 52
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;

    .line 53
    .local v0, "other":Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    iget-boolean v4, v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1f

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x25

    goto :goto_0
.end method

.method public isKeyword()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    return v0
.end method

.method public setKeyword(Z)V
    .locals 0
    .param p1, "isKeyword"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttributeImpl;->keyword:Z

    .line 64
    return-void
.end method
