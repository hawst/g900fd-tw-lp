.class public Lorg/apache/lucene/analysis/fa/PersianNormalizer;
.super Ljava/lang/Object;
.source "PersianNormalizer.java"


# static fields
.field public static final FARSI_YEH:C = '\u06cc'

.field public static final HAMZA_ABOVE:C = '\u0654'

.field public static final HEH:C = '\u0647'

.field public static final HEH_GOAL:C = '\u06c1'

.field public static final HEH_YEH:C = '\u06c0'

.field public static final KAF:C = '\u0643'

.field public static final KEHEH:C = '\u06a9'

.field public static final YEH:C = '\u064a'

.field public static final YEH_BARREE:C = '\u06d2'


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public normalize([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 85
    return p2

    .line 64
    :cond_0
    aget-char v1, p1, v0

    sparse-switch v1, :sswitch_data_0

    .line 63
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :sswitch_0
    const/16 v1, 0x64a

    aput-char v1, p1, v0

    goto :goto_1

    .line 70
    :sswitch_1
    const/16 v1, 0x643

    aput-char v1, p1, v0

    goto :goto_1

    .line 74
    :sswitch_2
    const/16 v1, 0x647

    aput-char v1, p1, v0

    goto :goto_1

    .line 77
    :sswitch_3
    invoke-static {p1, v0, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 78
    add-int/lit8 v0, v0, -0x1

    .line 79
    goto :goto_1

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x654 -> :sswitch_3
        0x6a9 -> :sswitch_1
        0x6c0 -> :sswitch_2
        0x6c1 -> :sswitch_2
        0x6cc -> :sswitch_0
        0x6d2 -> :sswitch_0
    .end sparse-switch
.end method
