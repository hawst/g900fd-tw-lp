.class public Lorg/apache/lucene/analysis/core/StopFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "StopFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final enablePositionIncrements:Z

.field private final format:Ljava/lang/String;

.field private final ignoreCase:Z

.field private final stopWordFiles:Ljava/lang/String;

.field private stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 50
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->assureMatchVersion()V

    .line 51
    const-string/jumbo v0, "words"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWordFiles:Ljava/lang/String;

    .line 52
    const-string v0, "format"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->format:Ljava/lang/String;

    .line 53
    const-string v0, "ignoreCase"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->ignoreCase:Z

    .line 54
    const-string v0, "enablePositionIncrements"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->enablePositionIncrements:Z

    .line 55
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 87
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    iget-object v2, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 88
    .local v0, "stopFilter":Lorg/apache/lucene/analysis/core/StopFilter;
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->enablePositionIncrements:Z

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/core/StopFilter;->setEnablePositionIncrements(Z)V

    .line 89
    return-object v0
.end method

.method public getStopWords()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 4
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWordFiles:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "snowball"

    iget-object v1, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->format:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWordFiles:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->getSnowballWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 71
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWordFiles:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/core/StopFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0

    .line 69
    :cond_1
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    sget-object v2, Lorg/apache/lucene/analysis/core/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-boolean v3, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->ignoreCase:Z

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0
.end method

.method public isEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->enablePositionIncrements:Z

    return v0
.end method

.method public isIgnoreCase()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/core/StopFilterFactory;->ignoreCase:Z

    return v0
.end method
