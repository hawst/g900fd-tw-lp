.class public final Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "SingleTokenTokenStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private exhausted:Z

.field private singleToken:Lorg/apache/lucene/analysis/Token;

.field private final tokenAtt:Lorg/apache/lucene/util/AttributeImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Token;)V
    .locals 1
    .param p1, "token"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 37
    sget-object v0, Lorg/apache/lucene/analysis/Token;->TOKEN_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->exhausted:Z

    .line 39
    sget-boolean v0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->clone()Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->singleToken:Lorg/apache/lucene/analysis/Token;

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeImpl;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->tokenAtt:Lorg/apache/lucene/util/AttributeImpl;

    .line 43
    sget-boolean v0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->tokenAtt:Lorg/apache/lucene/util/AttributeImpl;

    instance-of v0, v0, Lorg/apache/lucene/analysis/Token;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_1
    return-void
.end method


# virtual methods
.method public getToken()Lorg/apache/lucene/analysis/Token;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->singleToken:Lorg/apache/lucene/analysis/Token;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/Token;->clone()Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    return-object v0
.end method

.method public final incrementToken()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 48
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->exhausted:Z

    if-eqz v1, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    .line 51
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->clearAttributes()V

    .line 52
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->singleToken:Lorg/apache/lucene/analysis/Token;

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->tokenAtt:Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/Token;->copyTo(Lorg/apache/lucene/util/AttributeImpl;)V

    .line 53
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->exhausted:Z

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->exhausted:Z

    .line 61
    return-void
.end method

.method public setToken(Lorg/apache/lucene/analysis/Token;)V
    .locals 1
    .param p1, "token"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 68
    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->clone()Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/SingleTokenTokenStream;->singleToken:Lorg/apache/lucene/analysis/Token;

    .line 69
    return-void
.end method
