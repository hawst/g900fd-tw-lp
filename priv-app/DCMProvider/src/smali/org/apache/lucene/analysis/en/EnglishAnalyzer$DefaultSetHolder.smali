.class Lorg/apache/lucene/analysis/en/EnglishAnalyzer$DefaultSetHolder;
.super Ljava/lang/Object;
.source "EnglishAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/en/EnglishAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultSetHolder"
.end annotation


# static fields
.field static final DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    sput-object v0, Lorg/apache/lucene/analysis/en/EnglishAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
