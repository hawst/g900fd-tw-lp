.class public final Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "ArabicAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/ar/ArabicAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "stopwords.txt"


# instance fields
.field private final stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 93
    sget-object v0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 105
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 122
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 124
    return-void
.end method

.method static synthetic access$0(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->loadStopwordSet(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 140
    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 141
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 142
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    :goto_0
    new-instance v0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 144
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v3, v0, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 146
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/ar/ArabicNormalizationFilter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/ar/ArabicNormalizationFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 147
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 148
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 150
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v4, Lorg/apache/lucene/analysis/ar/ArabicStemFilter;

    invoke-direct {v4, v0}, Lorg/apache/lucene/analysis/ar/ArabicStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    invoke-direct {v3, v2, v4}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v3

    .line 141
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .end local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_1
    new-instance v2, Lorg/apache/lucene/analysis/ar/ArabicLetterTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/ar/ArabicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/ar/ArabicLetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    goto :goto_0
.end method
