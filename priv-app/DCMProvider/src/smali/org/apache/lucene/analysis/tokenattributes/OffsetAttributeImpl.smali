.class public Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "OffsetAttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;


# instance fields
.field private endOffset:I

.field private startOffset:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    .line 64
    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    .line 65
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 3
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 90
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 91
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 92
    return-void
.end method

.method public endOffset()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p1, p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 74
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;

    .line 75
    .local v0, "o":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;
    iget v3, v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    iget v4, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "o":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;
    :cond_3
    move v1, v2

    .line 78
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    .line 84
    .local v0, "code":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    add-int v0, v1, v2

    .line 85
    return v0
.end method

.method public setOffset(II)V
    .locals 3
    .param p1, "startOffset"    # I
    .param p2, "endOffset"    # I

    .prologue
    .line 44
    if-ltz p1, :cond_0

    if-ge p2, p1, :cond_1

    .line 45
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startOffset must be non-negative, and endOffset must be >= startOffset, startOffset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 46
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",endOffset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    .line 50
    iput p2, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->endOffset:I

    .line 51
    return-void
.end method

.method public startOffset()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttributeImpl;->startOffset:I

    return v0
.end method
