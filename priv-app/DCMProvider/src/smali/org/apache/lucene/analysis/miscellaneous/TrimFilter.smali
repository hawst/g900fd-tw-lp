.class public final Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "TrimFilter.java"


# instance fields
.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field final updateOffsets:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Z)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "updateOffsets"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 33
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 34
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 39
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->updateOffsets:Z

    .line 40
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x20

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 44
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v9}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v9

    if-nez v9, :cond_0

    .line 78
    :goto_0
    return v7

    .line 46
    :cond_0
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v6

    .line 47
    .local v6, "termBuffer":[C
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    .line 50
    .local v2, "len":I
    if-nez v2, :cond_1

    move v7, v8

    .line 51
    goto :goto_0

    .line 53
    :cond_1
    const/4 v5, 0x0

    .line 54
    .local v5, "start":I
    const/4 v0, 0x0

    .line 55
    .local v0, "end":I
    const/4 v1, 0x0

    .line 59
    .local v1, "endOff":I
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v2, :cond_2

    aget-char v9, v6, v5

    if-le v9, v10, :cond_6

    .line 62
    :cond_2
    move v0, v2

    :goto_2
    if-lt v0, v5, :cond_3

    add-int/lit8 v9, v0, -0x1

    aget-char v9, v6, v9

    if-le v9, v10, :cond_7

    .line 65
    :cond_3
    if-gtz v5, :cond_4

    if-ge v0, v2, :cond_5

    .line 66
    :cond_4
    if-ge v5, v0, :cond_8

    .line 67
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    sub-int v10, v0, v5

    invoke-interface {v9, v6, v5, v10}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 71
    :goto_3
    iget-boolean v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->updateOffsets:Z

    if-eqz v9, :cond_5

    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v9

    iget-object v10, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v10

    sub-int/2addr v9, v10

    if-ne v2, v9, :cond_5

    .line 72
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v9

    add-int v4, v9, v5

    .line 73
    .local v4, "newStart":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v9

    if-ge v5, v0, :cond_9

    .end local v1    # "endOff":I
    :goto_4
    sub-int v3, v9, v1

    .line 74
    .local v3, "newEnd":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v7, v4, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .end local v3    # "newEnd":I
    .end local v4    # "newStart":I
    :cond_5
    move v7, v8

    .line 78
    goto :goto_0

    .line 59
    .restart local v1    # "endOff":I
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 63
    :cond_7
    add-int/lit8 v1, v1, 0x1

    .line 62
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 69
    :cond_8
    iget-object v9, p0, Lorg/apache/lucene/analysis/miscellaneous/TrimFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    goto :goto_3

    .restart local v4    # "newStart":I
    :cond_9
    move v1, v7

    .line 73
    goto :goto_4
.end method
