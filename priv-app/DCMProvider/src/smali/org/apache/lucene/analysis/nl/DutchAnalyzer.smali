.class public final Lorg/apache/lucene/analysis/nl/DutchAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "DutchAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "dutch_stop.txt"


# instance fields
.field private excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final matchVersion:Lorg/apache/lucene/util/Version;

.field private final origStemdict:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final stemdict:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

.field private final stoptable:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 3
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 131
    sget-object v0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v2, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-direct {p0, p1, v0, v1, v2}, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 137
    sget-object v1, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 138
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    sget-object v0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 140
    :goto_0
    invoke-direct {p0, p1, p2, v1, v0}, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 141
    return-void

    .line 140
    :cond_0
    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionTable"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 146
    .line 147
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 149
    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 150
    return-void

    .line 149
    :cond_0
    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayMap;->emptyMap()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArrayMap;)V
    .locals 8
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionTable"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Lorg/apache/lucene/analysis/util/CharArraySet;",
            "Lorg/apache/lucene/analysis/util/CharArraySet;",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "stemOverrideDict":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Ljava/lang/String;>;"
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 152
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 116
    sget-object v5, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    iput-object v5, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 153
    iput-object p1, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 154
    invoke-static {p1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stoptable:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 155
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 156
    invoke-virtual {p4}, Lorg/apache/lucene/analysis/util/CharArrayMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 157
    :cond_0
    iput-object v7, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stemdict:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    .line 158
    invoke-static {p1, p4}, Lorg/apache/lucene/analysis/util/CharArrayMap;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Map;)Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->unmodifiableMap(Lorg/apache/lucene/analysis/util/CharArrayMap;)Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->origStemdict:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 176
    :goto_0
    return-void

    .line 160
    :cond_1
    iput-object v7, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->origStemdict:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 162
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;

    invoke-direct {v0, v6}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;-><init>(Z)V

    .line 163
    .local v0, "builder":Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;
    invoke-virtual {p4}, Lorg/apache/lucene/analysis/util/CharArrayMap;->entrySet()Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntrySet;->iterator()Lorg/apache/lucene/analysis/util/CharArrayMap$EntryIterator;

    move-result-object v2

    .line 164
    .local v2, "iter":Lorg/apache/lucene/analysis/util/CharArrayMap$EntryIterator;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Ljava/lang/String;>.EntryIterator;"
    new-instance v4, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 165
    .local v4, "spare":Lorg/apache/lucene/util/CharsRef;
    :goto_1
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntryIterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 171
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->build()Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stemdict:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v1

    .line 173
    .local v1, "ex":Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "can not build stem dict"

    invoke-direct {v5, v6, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 166
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntryIterator;->nextKey()[C

    move-result-object v3

    .line 167
    .local v3, "nextKey":[C
    array-length v5, v3

    invoke-virtual {v4, v3, v6, v5}, Lorg/apache/lucene/util/CharsRef;->copyChars([CII)V

    .line 168
    invoke-virtual {v2}, Lorg/apache/lucene/analysis/util/CharArrayMap$EntryIterator;->currentValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;->add(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    goto :goto_1
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "aReader"    # Ljava/io/Reader;

    .prologue
    .line 190
    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 191
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 192
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 193
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 194
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stoptable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 195
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 196
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 197
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stemdict:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    if-eqz v3, :cond_1

    .line 198
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stemdict:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 199
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v3, Lorg/tartarus/snowball/ext/DutchStemmer;

    invoke-direct {v3}, Lorg/tartarus/snowball/ext/DutchStemmer;-><init>()V

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .line 200
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object v0, v1

    .line 208
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    return-object v3

    .line 202
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .end local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_2
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 203
    .restart local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 204
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->stoptable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v3, v0, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 205
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 206
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 207
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_1
    new-instance v1, Lorg/apache/lucene/analysis/nl/DutchStemFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/nl/DutchAnalyzer;->origStemdict:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/nl/DutchStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Map;)V

    .line 208
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_3
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_1
.end method
