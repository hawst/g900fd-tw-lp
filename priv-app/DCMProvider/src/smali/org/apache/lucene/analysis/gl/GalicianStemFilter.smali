.class public final Lorg/apache/lucene/analysis/gl/GalicianStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "GalicianStemFilter.java"


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final stemmer:Lorg/apache/lucene/analysis/gl/GalicianStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 38
    new-instance v0, Lorg/apache/lucene/analysis/gl/GalicianStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/gl/GalicianStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->stemmer:Lorg/apache/lucene/analysis/gl/GalicianStemmer;

    .line 39
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 40
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 44
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v2, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    iget-object v2, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v2

    if-nez v2, :cond_0

    .line 51
    iget-object v2, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 52
    .local v0, "len":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->stemmer:Lorg/apache/lucene/analysis/gl/GalicianStemmer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v4, v0, 0x1

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->stem([CI)I

    move-result v1

    .line 53
    .local v1, "newlen":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/gl/GalicianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 55
    .end local v0    # "len":I
    .end local v1    # "newlen":I
    :cond_0
    const/4 v2, 0x1

    .line 57
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
