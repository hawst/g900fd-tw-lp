.class public final Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;
.super Ljava/lang/Object;
.source "WordDelimiterIterator.java"


# static fields
.field public static final DEFAULT_WORD_DELIM_TABLE:[B

.field public static final DONE:I = -0x1


# instance fields
.field private final charTypeTable:[B

.field current:I

.field end:I

.field endBounds:I

.field private hasFinalPossessive:Z

.field length:I

.field private skipPossessive:Z

.field final splitOnCaseChange:Z

.field final splitOnNumerics:Z

.field startBounds:I

.field final stemEnglishPossessive:Z

.field text:[C


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x100

    .line 76
    new-array v2, v4, [B

    .line 77
    .local v2, "tab":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_0

    .line 93
    sput-object v2, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->DEFAULT_WORD_DELIM_TABLE:[B

    .line 94
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    .line 79
    .local v0, "code":B
    invoke-static {v1}, Ljava/lang/Character;->isLowerCase(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 80
    const/4 v3, 0x1

    int-to-byte v0, v3

    .line 88
    :cond_1
    :goto_1
    if-nez v0, :cond_2

    .line 89
    const/16 v0, 0x8

    .line 91
    :cond_2
    aput-byte v0, v2, v1

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_3
    invoke-static {v1}, Ljava/lang/Character;->isUpperCase(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 83
    const/4 v3, 0x2

    int-to-byte v0, v3

    .line 84
    goto :goto_1

    .line 85
    :cond_4
    invoke-static {v1}, Ljava/lang/Character;->isDigit(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 86
    const/4 v3, 0x4

    int-to-byte v0, v3

    goto :goto_1
.end method

.method constructor <init>([BZZZ)V
    .locals 1
    .param p1, "charTypeTable"    # [B
    .param p2, "splitOnCaseChange"    # Z
    .param p3, "splitOnNumerics"    # Z
    .param p4, "stemEnglishPossessive"    # Z

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->hasFinalPossessive:Z

    .line 71
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->skipPossessive:Z

    .line 105
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charTypeTable:[B

    .line 106
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->splitOnCaseChange:Z

    .line 107
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->splitOnNumerics:Z

    .line 108
    iput-boolean p4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->stemEnglishPossessive:Z

    .line 109
    return-void
.end method

.method private charType(I)I
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charTypeTable:[B

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 272
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charTypeTable:[B

    aget-byte v0, v0, p1

    .line 274
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->getType(I)B

    move-result v0

    goto :goto_0
.end method

.method private endsWithPossessive(I)Z
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 256
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->stemEnglishPossessive:Z

    if-eqz v0, :cond_2

    .line 257
    const/4 v0, 0x2

    if-le p1, v0, :cond_2

    .line 258
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    add-int/lit8 v1, p1, -0x2

    aget-char v0, v0, v1

    const/16 v1, 0x27

    if-ne v0, v1, :cond_2

    .line 259
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    add-int/lit8 v1, p1, -0x1

    aget-char v0, v0, v1

    const/16 v1, 0x73

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    add-int/lit8 v1, p1, -0x1

    aget-char v0, v0, v1

    const/16 v1, 0x53

    if-ne v0, v1, :cond_2

    .line 260
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    add-int/lit8 v1, p1, -0x3

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    aget-char v0, v0, p1

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isSubwordDelim(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getType(I)B
    .locals 1
    .param p0, "ch"    # I

    .prologue
    .line 284
    invoke-static {p0}, Ljava/lang/Character;->getType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 323
    :pswitch_0
    const/16 v0, 0x8

    :goto_0
    return v0

    .line 285
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 286
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 294
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 299
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 309
    :pswitch_5
    const/4 v0, 0x7

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private isBreak(II)Z
    .locals 2
    .param p1, "lastType"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 199
    and-int v1, p2, p1

    if-eqz v1, :cond_1

    .line 214
    :cond_0
    :goto_0
    return v0

    .line 203
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->splitOnCaseChange:Z

    if-nez v1, :cond_2

    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 206
    :cond_2
    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isUpper(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    :cond_3
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->splitOnNumerics:Z

    if-nez v1, :cond_5

    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isDigit(I)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_4
    invoke-static {p1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isDigit(I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isAlpha(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 214
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setBounds()V
    .locals 2

    .prologue
    .line 236
    :goto_0
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->length:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isSubwordDelim(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 240
    :cond_0
    :goto_1
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    add-int/lit8 v1, v1, -0x1

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isSubwordDelim(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 243
    :cond_1
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endsWithPossessive(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->hasFinalPossessive:Z

    .line 246
    :cond_2
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    .line 247
    return-void

    .line 237
    :cond_3
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    goto :goto_0

    .line 241
    :cond_4
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    goto :goto_1
.end method


# virtual methods
.method isSingleWord()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 223
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->hasFinalPossessive:Z

    if-eqz v2, :cond_2

    .line 224
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    if-ne v2, v3, :cond_1

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    add-int/lit8 v3, v3, -0x2

    if-ne v2, v3, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 224
    goto :goto_0

    .line 227
    :cond_2
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method next()I
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 117
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    .line 118
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    if-ne v3, v2, :cond_0

    .line 149
    :goto_0
    return v2

    .line 122
    :cond_0
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->skipPossessive:Z

    if-eqz v3, :cond_1

    .line 123
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    add-int/lit8 v3, v3, 0x2

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    .line 124
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->skipPossessive:Z

    .line 127
    :cond_1
    const/4 v0, 0x0

    .line 129
    .local v0, "lastType":I
    :goto_1
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    iget v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    if-ge v3, v4, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    iget v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    aget-char v3, v3, v4

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->isSubwordDelim(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 133
    :cond_2
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    iget v4, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    if-lt v3, v4, :cond_4

    .line 134
    iput v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    goto :goto_0

    .line 130
    :cond_3
    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    goto :goto_1

    .line 137
    :cond_4
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    :goto_2
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    if-lt v2, v3, :cond_7

    .line 145
    :cond_5
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_6

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    add-int/lit8 v2, v2, 0x2

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endsWithPossessive(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 146
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->skipPossessive:Z

    .line 149
    :cond_6
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    goto :goto_0

    .line 138
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    aget-char v2, v2, v3

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v1

    .line 139
    .local v1, "type":I
    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->isBreak(II)Z

    move-result v2

    if-nez v2, :cond_5

    .line 142
    move v0, v1

    .line 137
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    goto :goto_2
.end method

.method setText([CI)V
    .locals 1
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    const/4 v0, 0x0

    .line 182
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    .line 183
    iput p2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->endBounds:I

    iput p2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->length:I

    .line 184
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->startBounds:I

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    .line 185
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->hasFinalPossessive:Z

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->skipPossessive:Z

    .line 186
    invoke-direct {p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->setBounds()V

    .line 187
    return-void
.end method

.method type()I
    .locals 3

    .prologue
    .line 160
    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->end:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 161
    const/4 v0, 0x0

    .line 171
    :goto_0
    return v0

    .line 164
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->text:[C

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->current:I

    aget-char v1, v1, v2

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterIterator;->charType(I)I

    move-result v0

    .line 165
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 169
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
