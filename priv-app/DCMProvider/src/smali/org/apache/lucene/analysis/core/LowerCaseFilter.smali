.class public final Lorg/apache/lucene/analysis/core/LowerCaseFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "LowerCaseFilter.java"


# instance fields
.field private final charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 48
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 39
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 49
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 50
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    iget-object v2, p0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/util/CharacterUtils;->toLowerCase([CII)V

    .line 56
    const/4 v0, 0x1

    .line 58
    :cond_0
    return v0
.end method
