.class public abstract Lorg/apache/lucene/analysis/util/CharArrayIterator;
.super Ljava/lang/Object;
.source "CharArrayIterator.java"

# interfaces
.implements Ljava/text/CharacterIterator;


# static fields
.field public static final HAS_BUGGY_BREAKITERATORS:Z


# instance fields
.field private array:[C

.field private index:I

.field private length:I

.field private limit:I

.field private start:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 192
    :try_start_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/BreakIterator;->getSentenceInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    .line 193
    .local v0, "bi":Ljava/text/BreakIterator;
    const-string/jumbo v3, "\udb40\udc53"

    invoke-virtual {v0, v3}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 194
    invoke-virtual {v0}, Ljava/text/BreakIterator;->next()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    const/4 v2, 0x0

    .line 199
    .local v2, "v":Z
    :goto_0
    sput-boolean v2, Lorg/apache/lucene/analysis/util/CharArrayIterator;->HAS_BUGGY_BREAKITERATORS:Z

    .line 200
    return-void

    .line 196
    .end local v2    # "v":Z
    :catch_0
    move-exception v1

    .line 197
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x1

    .restart local v2    # "v":Z
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newSentenceInstance()Lorg/apache/lucene/analysis/util/CharArrayIterator;
    .locals 1

    .prologue
    .line 139
    sget-boolean v0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->HAS_BUGGY_BREAKITERATORS:Z

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayIterator$1;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharArrayIterator$1;-><init>()V

    .line 150
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayIterator$2;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharArrayIterator$2;-><init>()V

    goto :goto_0
.end method

.method public static newWordInstance()Lorg/apache/lucene/analysis/util/CharArrayIterator;
    .locals 1

    .prologue
    .line 165
    sget-boolean v0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->HAS_BUGGY_BREAKITERATORS:Z

    if-eqz v0, :cond_0

    .line 166
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayIterator$3;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharArrayIterator$3;-><init>()V

    .line 175
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayIterator$4;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/CharArrayIterator$4;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->clone()Lorg/apache/lucene/analysis/util/CharArrayIterator;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/analysis/util/CharArrayIterator;
    .locals 2

    .prologue
    .line 127
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/util/CharArrayIterator;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 128
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public current()C
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    if-ne v0, v1, :cond_0

    const v0, 0xffff

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->array:[C

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    aget-char v0, v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->jreBugWorkaround(C)C

    move-result v0

    goto :goto_0
.end method

.method public first()C
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    .line 72
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->current()C

    move-result v0

    return v0
.end method

.method public getBeginIndex()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public getEndIndex()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->length:I

    return v0
.end method

.method public getIndex()I
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->length:I

    return v0
.end method

.method public getStart()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    return v0
.end method

.method public getText()[C
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->array:[C

    return-object v0
.end method

.method protected abstract jreBugWorkaround(C)C
.end method

.method public last()C
    .locals 2

    .prologue
    .line 92
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    :goto_0
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    .line 93
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->current()C

    move-result v0

    return v0

    .line 92
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public next()C
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    if-lt v0, v1, :cond_0

    .line 99
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    .line 100
    const v0, 0xffff

    .line 102
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->current()C

    move-result v0

    goto :goto_0
.end method

.method public previous()C
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    if-ge v0, v1, :cond_0

    .line 109
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    .line 110
    const v0, 0xffff

    .line 112
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->current()C

    move-result v0

    goto :goto_0
.end method

.method public setIndex(I)C
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 118
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->getBeginIndex()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->getEndIndex()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal Position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    iget v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->current()C

    move-result v0

    return v0
.end method

.method public setText([CII)V
    .locals 1
    .param p1, "array"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 55
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->array:[C

    .line 56
    iput p2, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->start:I

    .line 57
    iput p2, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->index:I

    .line 58
    iput p3, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->length:I

    .line 59
    add-int v0, p2, p3

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharArrayIterator;->limit:I

    .line 60
    return-void
.end method
