.class public final Lorg/apache/lucene/analysis/synonym/SynonymFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "SynonymFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;,
        Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final TYPE_SYNONYM:Ljava/lang/String; = "SYNONYM"


# instance fields
.field private final bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field private captureCount:I

.field private finished:Z

.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private final futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

.field private final futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

.field private final ignoreCase:Z

.field private inputSkipCount:I

.field private lastEndOffset:I

.field private lastStartOffset:I

.field private nextRead:I

.field private nextWrite:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final posLenAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

.field private final rollBufferSize:I

.field private final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final scratchBytes:Lorg/apache/lucene/util/BytesRef;

.field private final scratchChars:Lorg/apache/lucene/util/CharsRef;

.field private final synonyms:Lorg/apache/lucene/analysis/synonym/SynonymMap;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const-class v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    .line 108
    return-void

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/synonym/SynonymMap;Z)V
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "synonyms"    # Lorg/apache/lucene/analysis/synonym/SynonymMap;
    .param p3, "ignoreCase"    # Z

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 119
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 120
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 121
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->posLenAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    .line 122
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 123
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 229
    new-instance v1, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 251
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    .line 252
    new-instance v1, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    .line 263
    iput-object p2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->synonyms:Lorg/apache/lucene/analysis/synonym/SynonymMap;

    .line 264
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->ignoreCase:Z

    .line 265
    iget-object v1, p2, Lorg/apache/lucene/analysis/synonym/SynonymMap;->fst:Lorg/apache/lucene/util/fst/FST;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 266
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 267
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    if-nez v1, :cond_0

    .line 268
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "fst must be non-null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 274
    :cond_0
    iget v1, p2, Lorg/apache/lucene/analysis/synonym/SynonymMap;->maxHorizontalContext:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollBufferSize:I

    .line 276
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollBufferSize:I

    new-array v1, v1, [Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    .line 277
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollBufferSize:I

    new-array v1, v1, [Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    .line 278
    const/4 v0, 0x0

    .local v0, "pos":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollBufferSize:I

    if-lt v0, v1, :cond_1

    .line 285
    new-instance v1, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v1}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 286
    return-void

    .line 279
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    new-instance v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;-><init>(Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;)V

    aput-object v2, v1, v0

    .line 280
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    new-instance v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    invoke-direct {v2}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;-><init>()V

    aput-object v2, v1, v0

    .line 278
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private addOutput(Lorg/apache/lucene/util/BytesRef;II)V
    .locals 19
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "matchInputLength"    # I
    .param p3, "matchEndOffset"    # I

    .prologue
    .line 455
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    move-object/from16 v0, p1

    iget-object v3, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 457
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v10

    .line 458
    .local v10, "code":I
    and-int/lit8 v2, v10, 0x1

    if-nez v2, :cond_0

    const/4 v13, 0x1

    .line 459
    .local v13, "keepOrig":Z
    :goto_0
    ushr-int/lit8 v11, v10, 0x1

    .line 461
    .local v11, "count":I
    const/4 v14, 0x0

    .local v14, "outputIDX":I
    :goto_1
    if-lt v14, v11, :cond_1

    .line 502
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    move/from16 v16, v0

    .line 503
    .local v16, "upto":I
    const/4 v12, 0x0

    .local v12, "idx":I
    :goto_2
    move/from16 v0, p2

    if-lt v12, v0, :cond_8

    .line 508
    return-void

    .line 458
    .end local v11    # "count":I
    .end local v12    # "idx":I
    .end local v13    # "keepOrig":Z
    .end local v14    # "outputIDX":I
    .end local v16    # "upto":I
    :cond_0
    const/4 v13, 0x0

    goto :goto_0

    .line 462
    .restart local v11    # "count":I
    .restart local v13    # "keepOrig":Z
    .restart local v14    # "outputIDX":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->synonyms:Lorg/apache/lucene/analysis/synonym/SynonymMap;

    iget-object v2, v2, Lorg/apache/lucene/analysis/synonym/SynonymMap;->words:Lorg/apache/lucene/util/BytesRefHash;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v3

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v17, v0

    .line 462
    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0}, Lorg/apache/lucene/util/BytesRefHash;->get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    .line 465
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V

    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    iget v4, v2, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 467
    .local v4, "lastStart":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    iget v2, v2, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v8, v4, v2

    .line 468
    .local v8, "chEnd":I
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    .line 469
    .local v15, "outputUpto":I
    move v9, v4

    .local v9, "chIDX":I
    :goto_3
    if-le v9, v8, :cond_2

    .line 461
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 470
    :cond_2
    if-eq v9, v8, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    iget-object v2, v2, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v2, v2, v9

    if-nez v2, :cond_7

    .line 471
    :cond_3
    sub-int v5, v9, v4

    .line 474
    .local v5, "outputLen":I
    sget-boolean v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    if-gtz v5, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v17, "output contains empty string: "

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 477
    :cond_4
    if-ne v9, v8, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    iget v2, v2, Lorg/apache/lucene/util/CharsRef;->offset:I

    if-ne v4, v2, :cond_6

    .line 482
    move/from16 v6, p3

    .line 483
    .local v6, "endOffset":I
    if-eqz v13, :cond_5

    move/from16 v7, p2

    .line 492
    .local v7, "posLen":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    aget-object v2, v2, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchChars:Lorg/apache/lucene/util/CharsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-virtual/range {v2 .. v7}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->add([CIIII)V

    .line 494
    add-int/lit8 v4, v9, 0x1

    .line 496
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v15

    .line 497
    sget-boolean v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    aget-object v2, v2, v15

    iget v2, v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v17, "outputUpto="

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v17, " vs nextWrite="

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 483
    .end local v7    # "posLen":I
    :cond_5
    const/4 v7, 0x1

    goto :goto_4

    .line 489
    .end local v6    # "endOffset":I
    :cond_6
    const/4 v6, -0x1

    .line 490
    .restart local v6    # "endOffset":I
    const/4 v7, 0x1

    .restart local v7    # "posLen":I
    goto :goto_4

    .line 469
    .end local v5    # "outputLen":I
    .end local v6    # "endOffset":I
    .end local v7    # "posLen":I
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3

    .line 504
    .end local v4    # "lastStart":I
    .end local v8    # "chEnd":I
    .end local v9    # "chIDX":I
    .end local v15    # "outputUpto":I
    .restart local v12    # "idx":I
    .restart local v16    # "upto":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    aget-object v2, v2, v16

    iget-boolean v3, v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->keepOrig:Z

    or-int/2addr v3, v13

    iput-boolean v3, v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->keepOrig:Z

    .line 505
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    aget-object v2, v2, v16

    const/4 v3, 0x1

    iput-boolean v3, v2, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->matched:Z

    .line 506
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v16

    .line 503
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2
.end method

.method private capture()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 289
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->captureCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->captureCount:I

    .line 291
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    aget-object v0, v1, v2

    .line 293
    .local v0, "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->state:Lorg/apache/lucene/util/AttributeSource$State;

    .line 294
    iput-boolean v4, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->consumed:Z

    .line 295
    iget-object v1, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->term:Lorg/apache/lucene/util/CharsRef;

    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v4, v3}, Lorg/apache/lucene/util/CharsRef;->copyChars([CII)V

    .line 297
    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    .line 300
    sget-boolean v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    iget v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 301
    :cond_0
    return-void
.end method

.method private parse()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    sget-boolean v17, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v17, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    move/from16 v17, v0

    if-eqz v17, :cond_0

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 320
    :cond_0
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    .line 323
    .local v9, "curNextRead":I
    const/4 v14, 0x0

    .line 324
    .local v14, "matchOutput":Lorg/apache/lucene/util/BytesRef;
    const/4 v13, 0x0

    .line 325
    .local v13, "matchInputLength":I
    const/4 v12, -0x1

    .line 327
    .local v12, "matchEndOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/util/BytesRef;

    .line 328
    .local v15, "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 330
    sget-boolean v17, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 332
    :cond_1
    const/16 v16, 0x0

    .line 342
    .local v16, "tokenCount":I
    :goto_0
    const/4 v11, 0x0

    .line 344
    .local v11, "inputEndOffset":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v9, v0, :cond_b

    .line 349
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->finished:Z

    move/from16 v17, v0

    if-eqz v17, :cond_5

    .line 432
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->finished:Z

    move/from16 v17, v0

    if-nez v17, :cond_3

    .line 434
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    .line 437
    :cond_3
    if-eqz v14, :cond_e

    .line 439
    move-object/from16 v0, p0

    iput v13, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    .line 440
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13, v12}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->addOutput(Lorg/apache/lucene/util/BytesRef;II)V

    .line 451
    :cond_4
    :goto_2
    return-void

    .line 353
    :cond_5
    sget-boolean v17, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v18, v0

    aget-object v17, v17, v18

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->consumed:Z

    move/from16 v17, v0

    if-nez v17, :cond_6

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 358
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v17

    if-eqz v17, :cond_a

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v6

    .line 360
    .local v6, "buffer":[C
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v7

    .line 361
    .local v7, "bufferLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v18, v0

    aget-object v10, v17, v18

    .line 362
    .local v10, "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v17

    move/from16 v0, v17

    iput v0, v10, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->startOffset:I

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->lastStartOffset:I

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v17

    move/from16 v0, v17

    iput v0, v10, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->endOffset:I

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->lastEndOffset:I

    .line 364
    iget v11, v10, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->endOffset:I

    .line 366
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_9

    .line 367
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->capture()V

    .line 387
    .end local v10    # "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    :goto_3
    add-int/lit8 v16, v16, 0x1

    .line 390
    const/4 v5, 0x0

    .line 391
    .local v5, "bufUpto":I
    :goto_4
    if-lt v5, v7, :cond_c

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "matchOutput":Lorg/apache/lucene/util/BytesRef;
    check-cast v14, Lorg/apache/lucene/util/BytesRef;

    .line 408
    .restart local v14    # "matchOutput":Lorg/apache/lucene/util/BytesRef;
    move/from16 v13, v16

    .line 409
    move v12, v11

    .line 415
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-object/from16 v21, v0

    invoke-virtual/range {v17 .. v21}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v17

    if-eqz v17, :cond_2

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    check-cast v15, Lorg/apache/lucene/util/BytesRef;

    .line 424
    .restart local v15    # "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    .line 425
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->capture()V

    .line 429
    :cond_8
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v9

    .line 335
    goto/16 :goto_0

    .line 369
    .end local v5    # "bufUpto":I
    .restart local v10    # "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    :cond_9
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v10, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->consumed:Z

    goto/16 :goto_3

    .line 375
    .end local v6    # "buffer":[C
    .end local v7    # "bufferLen":I
    .end local v10    # "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    :cond_a
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->finished:Z

    goto/16 :goto_1

    .line 381
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    move-object/from16 v17, v0

    aget-object v17, v17, v9

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->term:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v6, v0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 382
    .restart local v6    # "buffer":[C
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    move-object/from16 v17, v0

    aget-object v17, v17, v9

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->term:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v7, v0, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 383
    .restart local v7    # "bufferLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    move-object/from16 v17, v0

    aget-object v17, v17, v9

    move-object/from16 v0, v17

    iget v11, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->endOffset:I

    goto/16 :goto_3

    .line 392
    .restart local v5    # "bufUpto":I
    :cond_c
    invoke-static {v6, v5, v7}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v8

    .line 393
    .local v8, "codePoint":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->ignoreCase:Z

    move/from16 v17, v0

    if-eqz v17, :cond_d

    invoke-static {v8}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v17

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v17

    if-eqz v17, :cond_2

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->fst:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    check-cast v15, Lorg/apache/lucene/util/BytesRef;

    .line 401
    .restart local v15    # "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    invoke-static {v8}, Ljava/lang/Character;->charCount(I)I

    move-result v17

    add-int v5, v5, v17

    goto/16 :goto_4

    :cond_d
    move/from16 v17, v8

    .line 393
    goto :goto_5

    .line 441
    .end local v5    # "bufUpto":I
    .end local v6    # "buffer":[C
    .end local v7    # "bufferLen":I
    .end local v8    # "codePoint":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_f

    .line 445
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    goto/16 :goto_2

    .line 447
    :cond_f
    sget-boolean v17, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v17, :cond_4

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->finished:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17
.end method

.method private rollIncr(I)I
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 512
    add-int/lit8 p1, p1, 0x1

    .line 513
    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollBufferSize:I

    if-ne p1, v0, :cond_0

    .line 514
    const/4 p1, 0x0

    .line 516
    .end local p1    # "count":I
    :cond_0
    return p1
.end method


# virtual methods
.method getCaptureCount()I
    .locals 1

    .prologue
    .line 522
    iget v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->captureCount:I

    return v0
.end method

.method public incrementToken()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 534
    :goto_0
    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    if-nez v7, :cond_2

    .line 598
    iget-boolean v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->finished:Z

    if-eqz v7, :cond_b

    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    iget v8, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    if-ne v7, v8, :cond_b

    .line 601
    iget-object v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    iget v8, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    aget-object v3, v7, v8

    .line 602
    .local v3, "outputs":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
    iget v7, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    iget v8, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-ge v7, v8, :cond_a

    .line 603
    iget v4, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    .line 604
    .local v4, "posIncr":I
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->pullNext()Lorg/apache/lucene/util/CharsRef;

    move-result-object v2

    .line 605
    .local v2, "output":Lorg/apache/lucene/util/CharsRef;
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    aget-object v6, v6, v7

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->reset()V

    .line 606
    iget v6, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-nez v6, :cond_0

    .line 607
    iget v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    iput v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    .line 609
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->clearAttributes()V

    .line 611
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->lastStartOffset:I

    iget v8, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->lastEndOffset:I

    invoke-interface {v6, v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 612
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v7, v2, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v8, v2, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v9, v2, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-interface {v6, v7, v8, v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 613
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string v7, "SYNONYM"

    invoke-interface {v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 615
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v6, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 619
    .end local v2    # "output":Lorg/apache/lucene/util/CharsRef;
    .end local v4    # "posIncr":I
    :cond_1
    :goto_1
    return v5

    .line 541
    .end local v3    # "outputs":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    iget v8, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    aget-object v1, v7, v8

    .line 542
    .local v1, "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    iget-object v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    iget v8, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    aget-object v3, v7, v8

    .line 546
    .restart local v3    # "outputs":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
    iget-boolean v7, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->consumed:Z

    if-nez v7, :cond_7

    iget-boolean v7, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->keepOrig:Z

    if-nez v7, :cond_3

    iget-boolean v7, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->matched:Z

    if-nez v7, :cond_7

    .line 547
    :cond_3
    iget-object v7, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->state:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v7, :cond_5

    .line 550
    iget-object v7, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->state:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 556
    :cond_4
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->reset()V

    .line 557
    iget v7, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-lez v7, :cond_6

    .line 558
    iput v6, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    goto :goto_1

    .line 554
    :cond_5
    sget-boolean v7, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    if-eq v7, v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "inputSkipCount="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " nextRead="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 560
    :cond_6
    iget v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    .line 561
    iget v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    goto :goto_1

    .line 565
    :cond_7
    iget v7, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->upto:I

    iget v8, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-ge v7, v8, :cond_9

    .line 568
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->reset()V

    .line 569
    iget v4, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->posIncr:I

    .line 570
    .restart local v4    # "posIncr":I
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->pullNext()Lorg/apache/lucene/util/CharsRef;

    move-result-object v2

    .line 571
    .restart local v2    # "output":Lorg/apache/lucene/util/CharsRef;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->clearAttributes()V

    .line 572
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v7, v2, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v8, v2, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v9, v2, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-interface {v6, v7, v8, v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 573
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string v7, "SYNONYM"

    invoke-interface {v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 574
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->getLastEndOffset()I

    move-result v0

    .line 575
    .local v0, "endOffset":I
    const/4 v6, -0x1

    if-ne v0, v6, :cond_8

    .line 576
    iget v0, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->endOffset:I

    .line 578
    :cond_8
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v7, v1, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->startOffset:I

    invoke-interface {v6, v7, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 579
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v6, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 580
    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->posLenAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->getLastPosLength()I

    move-result v7

    invoke-interface {v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;->setPositionLength(I)V

    .line 581
    iget v6, v3, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->count:I

    if-nez v6, :cond_1

    .line 584
    iget v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    invoke-direct {p0, v6}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    .line 585
    iget v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    goto/16 :goto_1

    .line 592
    .end local v0    # "endOffset":I
    .end local v2    # "output":Lorg/apache/lucene/util/CharsRef;
    .end local v4    # "posIncr":I
    :cond_9
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->reset()V

    .line 593
    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    invoke-direct {p0, v7}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->rollIncr(I)I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    .line 594
    iget v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    goto/16 :goto_0

    .end local v1    # "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    :cond_a
    move v5, v6

    .line 619
    goto/16 :goto_1

    .line 624
    .end local v3    # "outputs":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
    :cond_b
    invoke-direct {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->parse()V

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 630
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 631
    iput v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->captureCount:I

    .line 632
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->finished:Z

    .line 633
    iput v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->inputSkipCount:I

    .line 634
    iput v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextWrite:I

    iput v2, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->nextRead:I

    .line 641
    iget-object v4, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureInputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_0

    .line 644
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter;->futureOutputs:[Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;

    array-length v4, v3

    :goto_1
    if-lt v2, v4, :cond_1

    .line 647
    return-void

    .line 641
    :cond_0
    aget-object v0, v4, v3

    .line 642
    .local v0, "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    invoke-virtual {v0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->reset()V

    .line 641
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 644
    .end local v0    # "input":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
    :cond_1
    aget-object v1, v3, v2

    .line 645
    .local v1, "output":Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;
    invoke-virtual {v1}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingOutputs;->reset()V

    .line 644
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
