.class public Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;
.super Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;
.source "DateRecognizerSinkFilter.java"


# static fields
.field public static final DATE_TYPE:Ljava/lang/String; = "date"


# instance fields
.field protected dateFormat:Ljava/text/DateFormat;

.field protected termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x2

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;-><init>(Ljava/text/DateFormat;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/text/DateFormat;)V
    .locals 0
    .param p1, "dateFormat"    # Ljava/text/DateFormat;

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;->dateFormat:Ljava/text/DateFormat;

    .line 51
    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 3
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 55
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    if-nez v1, :cond_0

    .line 56
    const-class v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v1, p0, Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 59
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;->dateFormat:Ljava/text/DateFormat;

    iget-object v2, p0, Lorg/apache/lucene/analysis/sinks/DateRecognizerSinkFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 60
    .local v0, "date":Ljava/util/Date;
    if-eqz v0, :cond_1

    .line 61
    const/4 v1, 0x1

    .line 67
    .end local v0    # "date":Ljava/util/Date;
    :goto_0
    return v1

    .line 63
    :catch_0
    move-exception v1

    .line 67
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
