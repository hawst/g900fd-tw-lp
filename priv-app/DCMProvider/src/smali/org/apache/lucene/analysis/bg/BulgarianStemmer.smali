.class public Lorg/apache/lucene/analysis/bg/BulgarianStemmer;
.super Ljava/lang/Object;
.source "BulgarianStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private removeArticle([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 82
    const/4 v0, 0x6

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u0438\u044f\u0442"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    add-int/lit8 p2, p2, -0x3

    .line 97
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 85
    .restart local p2    # "len":I
    :cond_1
    const/4 v0, 0x5

    if-le p2, v0, :cond_3

    .line 86
    const-string/jumbo v0, "\u044a\u0442"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 87
    const-string/jumbo v0, "\u0442\u043e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 88
    const-string/jumbo v0, "\u0442\u0435"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 89
    const-string/jumbo v0, "\u0442\u0430"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 90
    const-string/jumbo v0, "\u0438\u044f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    :cond_2
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 94
    :cond_3
    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    const-string/jumbo v0, "\u044f\u0442"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    add-int/lit8 p2, p2, -0x2

    goto :goto_0
.end method

.method private removePlural([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 101
    const/4 v0, 0x6

    if-le p2, v0, :cond_3

    .line 102
    const-string/jumbo v0, "\u043e\u0432\u0446\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    add-int/lit8 p2, p2, -0x3

    .line 141
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 104
    .restart local p2    # "len":I
    :cond_1
    const-string/jumbo v0, "\u043e\u0432\u0435"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 106
    :cond_2
    const-string/jumbo v0, "\u0435\u0432\u0435"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x439

    aput-char v1, p1, v0

    .line 108
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 112
    :cond_3
    const/4 v0, 0x5

    if-le p2, v0, :cond_8

    .line 113
    const-string/jumbo v0, "\u0438\u0449\u0430"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 114
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 115
    :cond_4
    const-string/jumbo v0, "\u0442\u0430"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 116
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 117
    :cond_5
    const-string/jumbo v0, "\u0446\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 118
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x43a

    aput-char v1, p1, v0

    .line 119
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 121
    :cond_6
    const-string/jumbo v0, "\u0437\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 122
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x433

    aput-char v1, p1, v0

    .line 123
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 126
    :cond_7
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    const/16 v1, 0x435

    if-ne v0, v1, :cond_8

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x438

    if-ne v0, v1, :cond_8

    .line 127
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x44f

    aput-char v1, p1, v0

    .line 128
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 132
    :cond_8
    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    .line 133
    const-string/jumbo v0, "\u0441\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 134
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x445

    aput-char v1, p1, v0

    .line 135
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 137
    :cond_9
    const-string/jumbo v0, "\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public stem([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x4

    .line 41
    if-ge p2, v1, :cond_0

    move v0, p2

    .line 72
    :goto_0
    return v0

    .line 44
    :cond_0
    if-le p2, v2, :cond_1

    const-string/jumbo v0, "\u0438\u0449\u0430"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    add-int/lit8 v0, p2, -0x3

    goto :goto_0

    .line 47
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/bg/BulgarianStemmer;->removeArticle([CI)I

    move-result p2

    .line 48
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/bg/BulgarianStemmer;->removePlural([CI)I

    move-result p2

    .line 50
    const/4 v0, 0x3

    if-le p2, v0, :cond_4

    .line 51
    const-string/jumbo v0, "\u044f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    add-int/lit8 p2, p2, -0x1

    .line 53
    :cond_2
    const-string/jumbo v0, "\u0430"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 54
    const-string/jumbo v0, "\u043e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    const-string/jumbo v0, "\u0435"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 56
    :cond_3
    add-int/lit8 p2, p2, -0x1

    .line 62
    :cond_4
    if-le p2, v1, :cond_5

    const-string/jumbo v0, "\u0435\u043d"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x43d

    aput-char v1, p1, v0

    .line 64
    add-int/lit8 p2, p2, -0x1

    .line 67
    :cond_5
    if-le p2, v2, :cond_6

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    const/16 v1, 0x44a

    if-ne v0, v1, :cond_6

    .line 68
    add-int/lit8 v0, p2, -0x2

    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    aput-char v1, p1, v0

    .line 69
    add-int/lit8 p2, p2, -0x1

    :cond_6
    move v0, p2

    .line 72
    goto :goto_0
.end method
