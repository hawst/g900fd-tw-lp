.class public final Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "LimitTokenPositionFilter.java"


# instance fields
.field private final consumeAllTokens:Z

.field private exhausted:Z

.field private final maxTokenPosition:I

.field private final posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private tokenPosition:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;I)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "maxTokenPosition"    # I

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "maxTokenPosition"    # I
    .param p3, "consumeAllTokens"    # Z

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 43
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->tokenPosition:I

    .line 44
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->exhausted:Z

    .line 45
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 70
    iput p2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->maxTokenPosition:I

    .line 71
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->consumeAllTokens:Z

    .line 72
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 76
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->exhausted:Z

    if-eqz v2, :cond_0

    .line 90
    :goto_0
    return v0

    .line 79
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->tokenPosition:I

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->tokenPosition:I

    .line 81
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->tokenPosition:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->maxTokenPosition:I

    if-gt v2, v3, :cond_1

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->consumeAllTokens:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_1

    .line 85
    :cond_2
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->exhausted:Z

    goto :goto_0

    .line 89
    :cond_3
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->exhausted:Z

    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 97
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->tokenPosition:I

    .line 98
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LimitTokenPositionFilter;->exhausted:Z

    .line 99
    return-void
.end method
