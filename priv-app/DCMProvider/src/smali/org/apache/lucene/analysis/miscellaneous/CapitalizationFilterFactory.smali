.class public Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "CapitalizationFilterFactory.java"


# static fields
.field public static final FORCE_FIRST_LETTER:Ljava/lang/String; = "forceFirstLetter"

.field public static final KEEP:Ljava/lang/String; = "keep"

.field public static final KEEP_IGNORE_CASE:Ljava/lang/String; = "keepIgnoreCase"

.field public static final MAX_TOKEN_LENGTH:Ljava/lang/String; = "maxTokenLength"

.field public static final MAX_WORD_COUNT:Ljava/lang/String; = "maxWordCount"

.field public static final MIN_WORD_LENGTH:Ljava/lang/String; = "minWordLength"

.field public static final OK_PREFIX:Ljava/lang/String; = "okPrefix"

.field public static final ONLY_FIRST_WORD:Ljava/lang/String; = "onlyFirstWord"


# instance fields
.field final forceFirstLetter:Z

.field keep:Lorg/apache/lucene/analysis/util/CharArraySet;

.field final maxTokenLength:I

.field final maxWordCount:I

.field final minWordLength:I

.field okPrefix:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<[C>;"
        }
    .end annotation
.end field

.field final onlyFirstWord:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const v8, 0x7fffffff

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 80
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 70
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->okPrefix:Ljava/util/Collection;

    .line 81
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->assureMatchVersion()V

    .line 82
    const-string v3, "keepIgnoreCase"

    invoke-virtual {p0, p1, v3, v6}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    .line 83
    .local v0, "ignoreCase":Z
    const-string v3, "keep"

    invoke-virtual {p0, p1, v3}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getSet(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 84
    .local v2, "k":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 85
    new-instance v3, Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    const/16 v5, 0xa

    invoke-direct {v3, v4, v5, v0}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    iput-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->keep:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 86
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->keep:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 89
    :cond_0
    const-string v3, "okPrefix"

    invoke-virtual {p0, p1, v3}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getSet(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    .line 90
    if-eqz v2, :cond_1

    .line 91
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->okPrefix:Ljava/util/Collection;

    .line 92
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 97
    :cond_1
    const-string v3, "minWordLength"

    invoke-virtual {p0, p1, v3, v6}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->minWordLength:I

    .line 98
    const-string v3, "maxWordCount"

    invoke-virtual {p0, p1, v3, v8}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->maxWordCount:I

    .line 99
    const-string v3, "maxTokenLength"

    invoke-virtual {p0, p1, v3, v8}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->maxTokenLength:I

    .line 100
    const-string v3, "onlyFirstWord"

    invoke-virtual {p0, p1, v3, v7}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->onlyFirstWord:Z

    .line 101
    const-string v3, "forceFirstLetter"

    invoke-virtual {p0, p1, v3, v7}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->forceFirstLetter:Z

    .line 102
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 103
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown parameters: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 92
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 93
    .local v1, "item":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->okPrefix:Ljava/util/Collection;

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 105
    .end local v1    # "item":Ljava/lang/String;
    :cond_3
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;
    .locals 9
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 109
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->onlyFirstWord:Z

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->keep:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 110
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->forceFirstLetter:Z

    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->okPrefix:Ljava/util/Collection;

    iget v6, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->minWordLength:I

    iget v7, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->maxWordCount:I

    iget v8, p0, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilterFactory;->maxTokenLength:I

    move-object v1, p1

    .line 109
    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/analysis/miscellaneous/CapitalizationFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;ZLorg/apache/lucene/analysis/util/CharArraySet;ZLjava/util/Collection;III)V

    return-object v0
.end method
