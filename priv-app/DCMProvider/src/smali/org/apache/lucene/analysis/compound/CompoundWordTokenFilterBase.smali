.class public abstract Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CompoundWordTokenFilterBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_SUBWORD_SIZE:I = 0xf

.field public static final DEFAULT_MIN_SUBWORD_SIZE:I = 0x2

.field public static final DEFAULT_MIN_WORD_SIZE:I = 0x5


# instance fields
.field private current:Lorg/apache/lucene/util/AttributeSource$State;

.field protected final dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

.field protected final maxSubwordSize:I

.field protected final minSubwordSize:I

.field protected final minWordSize:I

.field protected final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field protected final onlyLongestMatch:Z

.field private final posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field protected final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field protected final tokens:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->$assertionsDisabled:Z

    .line 60
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 8
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 80
    const/4 v4, 0x5

    const/4 v5, 0x2

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    .line 81
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p4, "minWordSize"    # I
    .param p5, "minSubwordSize"    # I
    .param p6, "maxSubwordSize"    # I
    .param p7, "onlyLongestMatch"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 69
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 70
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 71
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 86
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->tokens:Ljava/util/LinkedList;

    .line 87
    if-gez p4, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minWordSize cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    iput p4, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->minWordSize:I

    .line 91
    if-gez p5, :cond_1

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minSubwordSize cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_1
    iput p5, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->minSubwordSize:I

    .line 95
    if-gez p6, :cond_2

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxSubwordSize cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_2
    iput p6, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->maxSubwordSize:I

    .line 99
    iput-boolean p7, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->onlyLongestMatch:Z

    .line 100
    iput-object p3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 101
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;Z)V
    .locals 8
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p4, "onlyLongestMatch"    # Z

    .prologue
    .line 76
    const/4 v4, 0x5

    const/4 v5, 0x2

    const/16 v6, 0xf

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    .line 77
    return-void
.end method


# virtual methods
.method protected abstract decompose()V
.end method

.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->tokens:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 106
    sget-boolean v3, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->current:Lorg/apache/lucene/util/AttributeSource$State;

    if-nez v3, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 107
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->tokens:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .line 108
    .local v0, "token":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->current:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 109
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v3

    iget-object v4, v0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->txt:Ljava/lang/CharSequence;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 110
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v4, v0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->startOffset:I

    iget v5, v0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->endOffset:I

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 111
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v2}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 128
    .end local v0    # "token":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    :cond_1
    :goto_0
    return v1

    .line 115
    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->current:Lorg/apache/lucene/util/AttributeSource$State;

    .line 116
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 118
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->minWordSize:I

    if-lt v2, v3, :cond_1

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->decompose()V

    .line 121
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->tokens:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 122
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->current:Lorg/apache/lucene/util/AttributeSource$State;

    goto :goto_0

    :cond_3
    move v1, v2

    .line 128
    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 140
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->tokens:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;->current:Lorg/apache/lucene/util/AttributeSource$State;

    .line 142
    return-void
.end method
