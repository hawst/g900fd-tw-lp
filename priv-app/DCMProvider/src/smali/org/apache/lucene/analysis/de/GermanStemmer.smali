.class public Lorg/apache/lucene/analysis/de/GermanStemmer;
.super Ljava/lang/Object;
.source "GermanStemmer.java"


# static fields
.field private static final locale:Ljava/util/Locale;


# instance fields
.field private sb:Ljava/lang/StringBuilder;

.field private substCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Ljava/util/Locale;

    const-string v1, "de"

    const-string v2, "DE"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/analysis/de/GermanStemmer;->locale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    .line 32
    return-void
.end method

.method private isStemmable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 77
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 81
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 78
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 79
    const/4 v1, 0x0

    goto :goto_1

    .line 77
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private optimize(Ljava/lang/StringBuilder;)V
    .locals 2
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 135
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x5

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "erin*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 137
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/de/GermanStemmer;->strip(Ljava/lang/StringBuilder;)V

    .line 141
    :cond_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x7a

    if-ne v0, v1, :cond_1

    .line 142
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/16 v1, 0x78

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 144
    :cond_1
    return-void
.end method

.method private removeParticleDenotion(Ljava/lang/StringBuilder;)V
    .locals 3
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    .line 151
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    .line 152
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    if-lt v0, v1, :cond_1

    .line 159
    .end local v0    # "c":I
    :cond_0
    :goto_1
    return-void

    .line 153
    .restart local v0    # "c":I
    :cond_1
    add-int/lit8 v1, v0, 0x4

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "gege"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 154
    add-int/lit8 v1, v0, 0x2

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 152
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private resubstitute(Ljava/lang/StringBuilder;)V
    .locals 9
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v8, 0x73

    const/16 v7, 0x65

    const/4 v6, 0x2

    const/16 v5, 0x69

    .line 241
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 271
    return-void

    .line 242
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x2a

    if-ne v2, v3, :cond_2

    .line 243
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    .line 244
    .local v1, "x":C
    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 241
    .end local v1    # "x":C
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x24

    if-ne v2, v3, :cond_3

    .line 247
    invoke-virtual {p1, v0, v8}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 248
    add-int/lit8 v2, v0, 0x1

    new-array v3, v6, [C

    fill-array-data v3, :array_0

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v4, v6}, Ljava/lang/StringBuilder;->insert(I[CII)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 250
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0xa7

    if-ne v2, v3, :cond_4

    .line 251
    const/16 v2, 0x63

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 252
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x68

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 254
    :cond_4
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x25

    if-ne v2, v3, :cond_5

    .line 255
    invoke-virtual {p1, v0, v7}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 256
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2, v5}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 258
    :cond_5
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x26

    if-ne v2, v3, :cond_6

    .line 259
    invoke-virtual {p1, v0, v5}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 260
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2, v7}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 262
    :cond_6
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_7

    .line 263
    invoke-virtual {p1, v0, v5}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 264
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x67

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 266
    :cond_7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x21

    if-ne v2, v3, :cond_1

    .line 267
    invoke-virtual {p1, v0, v8}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 268
    add-int/lit8 v2, v0, 0x1

    const/16 v3, 0x74

    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 248
    nop

    :array_0
    .array-data 2
        0x63s
        0x68s
    .end array-data
.end method

.method private strip(Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v3, 0x4

    .line 94
    const/4 v0, 0x1

    .line 95
    .local v0, "doMore":Z
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x3

    if-gt v1, v2, :cond_1

    .line 126
    :cond_0
    return-void

    .line 96
    :cond_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/2addr v1, v2

    const/4 v2, 0x5

    if-le v1, v2, :cond_2

    .line 97
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "nd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 99
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 101
    :cond_2
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/2addr v1, v2

    if-le v1, v3, :cond_3

    .line 102
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "em"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 105
    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/2addr v1, v2

    if-le v1, v3, :cond_4

    .line 106
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "er"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 107
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 109
    :cond_4
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x65

    if-ne v1, v2, :cond_5

    .line 110
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 112
    :cond_5
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x73

    if-ne v1, v2, :cond_6

    .line 113
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 115
    :cond_6
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x6e

    if-ne v1, v2, :cond_7

    .line 116
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 119
    :cond_7
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x74

    if-ne v1, v2, :cond_8

    .line 120
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 123
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private substitute(Ljava/lang/StringBuilder;)V
    .locals 8
    .param p1, "buffer"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v7, 0x68

    const/16 v6, 0x65

    const/16 v5, 0x63

    const/16 v4, 0x69

    const/16 v3, 0x73

    .line 173
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    .line 174
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 232
    return-void

    .line 176
    :cond_0
    if-lez v0, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    if-ne v1, v2, :cond_3

    .line 177
    const/16 v1, 0x2a

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 196
    :cond_1
    :goto_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 198
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-ge v0, v1, :cond_7

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_7

    .line 199
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_7

    add-int/lit8 v1, v0, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v7, :cond_7

    .line 201
    const/16 v1, 0x24

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 202
    add-int/lit8 v1, v0, 0x1

    add-int/lit8 v2, v0, 0x3

    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 203
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    .line 174
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0xe4

    if-ne v1, v2, :cond_4

    .line 181
    const/16 v1, 0x61

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 183
    :cond_4
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0xf6

    if-ne v1, v2, :cond_5

    .line 184
    const/16 v1, 0x6f

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 186
    :cond_5
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0xfc

    if-ne v1, v2, :cond_6

    .line 187
    const/16 v1, 0x75

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    goto :goto_1

    .line 190
    :cond_6
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0xdf

    if-ne v1, v2, :cond_1

    .line 191
    invoke-virtual {p1, v0, v3}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 192
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1, v3}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 193
    iget v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    goto :goto_1

    .line 205
    :cond_7
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_8

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v7, :cond_8

    .line 206
    const/16 v1, 0xa7

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 207
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 208
    iget v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    goto :goto_2

    .line 210
    :cond_8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_9

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_9

    .line 211
    const/16 v1, 0x25

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 212
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 213
    iget v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    goto/16 :goto_2

    .line 215
    :cond_9
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_a

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v6, :cond_a

    .line 216
    const/16 v1, 0x26

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 217
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 218
    iget v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    goto/16 :goto_2

    .line 220
    :cond_a
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_b

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x67

    if-ne v1, v2, :cond_b

    .line 221
    const/16 v1, 0x23

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 222
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 223
    iget v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    goto/16 :goto_2

    .line 225
    :cond_b
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_2

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x74

    if-ne v1, v2, :cond_2

    .line 226
    const/16 v1, 0x21

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 227
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 228
    iget v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->substCount:I

    goto/16 :goto_2
.end method


# virtual methods
.method protected stem(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 55
    sget-object v0, Lorg/apache/lucene/analysis/de/GermanStemmer;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 56
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/de/GermanStemmer;->isStemmable(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    .end local p1    # "term":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 59
    .restart local p1    # "term":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, p1}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemmer;->substitute(Ljava/lang/StringBuilder;)V

    .line 63
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemmer;->strip(Ljava/lang/StringBuilder;)V

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemmer;->optimize(Ljava/lang/StringBuilder;)V

    .line 65
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemmer;->resubstitute(Ljava/lang/StringBuilder;)V

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemmer;->removeParticleDenotion(Ljava/lang/StringBuilder;)V

    .line 67
    iget-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemmer;->sb:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
