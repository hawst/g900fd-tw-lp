.class public final Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "IrishLowerCaseFilter.java"


# instance fields
.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 31
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 38
    return-void
.end method

.method private isUpperVowel(I)Z
    .locals 1
    .param p1, "v"    # I

    .prologue
    .line 68
    sparse-switch p1, :sswitch_data_0

    .line 82
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 80
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 68
    nop

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x45 -> :sswitch_0
        0x49 -> :sswitch_0
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0xc1 -> :sswitch_0
        0xc9 -> :sswitch_0
        0xcd -> :sswitch_0
        0xd3 -> :sswitch_0
        0xda -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 42
    iget-object v6, p0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 43
    iget-object v6, p0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 44
    .local v0, "chArray":[C
    iget-object v6, p0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 45
    .local v1, "chLen":I
    const/4 v3, 0x0

    .line 47
    .local v3, "idx":I
    if-le v1, v4, :cond_1

    aget-char v6, v0, v5

    const/16 v7, 0x6e

    if-eq v6, v7, :cond_0

    aget-char v5, v0, v5

    const/16 v6, 0x74

    if-ne v5, v6, :cond_1

    :cond_0
    aget-char v5, v0, v4

    invoke-direct {p0, v5}, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->isUpperVowel(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 48
    iget-object v5, p0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    .line 49
    move v2, v1

    .local v2, "i":I
    :goto_0
    if-gt v2, v4, :cond_2

    .line 52
    const/16 v5, 0x2d

    aput-char v5, v0, v4

    .line 53
    iget-object v5, p0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 54
    const/4 v3, 0x2

    .line 55
    add-int/lit8 v1, v1, 0x1

    .line 58
    .end local v2    # "i":I
    :cond_1
    move v2, v3

    .restart local v2    # "i":I
    :goto_1
    if-lt v2, v1, :cond_3

    .line 63
    .end local v0    # "chArray":[C
    .end local v1    # "chLen":I
    .end local v2    # "i":I
    .end local v3    # "idx":I
    :goto_2
    return v4

    .line 50
    .restart local v0    # "chArray":[C
    .restart local v1    # "chLen":I
    .restart local v2    # "i":I
    .restart local v3    # "idx":I
    :cond_2
    add-int/lit8 v5, v2, -0x1

    aget-char v5, v0, v5

    aput-char v5, v0, v2

    .line 49
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 59
    :cond_3
    aget-char v5, v0, v2

    invoke-static {v5}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v5

    invoke-static {v5, v0, v2}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v5

    add-int/2addr v2, v5

    goto :goto_1

    .end local v0    # "chArray":[C
    .end local v1    # "chLen":I
    .end local v2    # "i":I
    .end local v3    # "idx":I
    :cond_4
    move v4, v5

    .line 63
    goto :goto_2
.end method
