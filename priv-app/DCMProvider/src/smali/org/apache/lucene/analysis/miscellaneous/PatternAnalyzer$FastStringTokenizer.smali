.class final Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "PatternAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FastStringTokenizer"
.end annotation


# static fields
.field private static final locale:Ljava/util/Locale;


# instance fields
.field private final isLetter:Z

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private pos:I

.field private final stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private str:Ljava/lang/String;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final toLowerCase:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 396
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->locale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ZZLorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "isLetter"    # Z
    .param p3, "toLowerCase"    # Z
    .param p4, "stopWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 401
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 397
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 398
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 402
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->isLetter:Z

    .line 403
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->toLowerCase:Z

    .line 404
    iput-object p4, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 405
    return-void
.end method

.method private isStopWord(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 468
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTokenChar(CZ)Z
    .locals 1
    .param p1, "c"    # C
    .param p2, "isLetter"    # Z

    .prologue
    .line 464
    if-eqz p2, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final end()V
    .locals 4

    .prologue
    .line 459
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->str:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 460
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->correctOffset(I)I

    move-result v2

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->correctOffset(I)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 461
    return-void
.end method

.method public incrementToken()Z
    .locals 9

    .prologue
    .line 409
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->clearAttributes()V

    .line 411
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->str:Ljava/lang/String;

    .line 412
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 413
    .local v1, "len":I
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->pos:I

    .line 414
    .local v0, "i":I
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->isLetter:Z

    .line 416
    .local v2, "letter":Z
    const/4 v4, 0x0

    .line 420
    .local v4, "start":I
    :cond_0
    const/4 v5, 0x0

    .line 421
    .local v5, "text":Ljava/lang/String;
    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-direct {p0, v6, v2}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->isTokenChar(CZ)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 425
    :cond_1
    if-ge v0, v1, :cond_3

    .line 426
    move v4, v0

    .line 427
    :goto_1
    if-ge v0, v1, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-direct {p0, v6, v2}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->isTokenChar(CZ)Z

    move-result v6

    if-nez v6, :cond_6

    .line 431
    :cond_2
    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 432
    iget-boolean v6, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->toLowerCase:Z

    if-eqz v6, :cond_3

    sget-object v6, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->locale:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 444
    :cond_3
    if-eqz v5, :cond_4

    invoke-direct {p0, v5}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->isStopWord(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 446
    :cond_4
    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->pos:I

    .line 447
    if-nez v5, :cond_7

    .line 449
    const/4 v6, 0x0

    .line 453
    :goto_2
    return v6

    .line 422
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 428
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 451
    :cond_7
    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v6

    invoke-interface {v6, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 452
    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->correctOffset(I)I

    move-result v7

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->correctOffset(I)I

    move-result v8

    invoke-interface {v6, v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 453
    const/4 v6, 0x1

    goto :goto_2
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 473
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 474
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->input:Ljava/io/Reader;

    # invokes: Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toString(Ljava/io/Reader;)Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->access$0(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->str:Ljava/lang/String;

    .line 475
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;->pos:I

    .line 476
    return-void
.end method
