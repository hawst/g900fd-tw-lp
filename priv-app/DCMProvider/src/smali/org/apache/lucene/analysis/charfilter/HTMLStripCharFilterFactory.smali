.class public Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;
.super Lorg/apache/lucene/analysis/util/CharFilterFactory;
.source "HTMLStripCharFilterFactory.java"


# static fields
.field static final TAG_NAME_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field final escapedTags:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "[^\\s,]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;->TAG_NAME_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharFilterFactory;-><init>(Ljava/util/Map;)V

    .line 44
    const-string v0, "escapedTags"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;->getSet(Ljava/util/Map;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;->escapedTags:Ljava/util/Set;

    .line 45
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;->create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 53
    iget-object v1, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;->escapedTags:Ljava/util/Set;

    if-nez v1, :cond_0

    .line 54
    new-instance v0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;-><init>(Ljava/io/Reader;)V

    .line 58
    .local v0, "charFilter":Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;
    :goto_0
    return-object v0

    .line 56
    .end local v0    # "charFilter":Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilterFactory;->escapedTags:Ljava/util/Set;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;-><init>(Ljava/io/Reader;Ljava/util/Set;)V

    .restart local v0    # "charFilter":Lorg/apache/lucene/analysis/charfilter/HTMLStripCharFilter;
    goto :goto_0
.end method
