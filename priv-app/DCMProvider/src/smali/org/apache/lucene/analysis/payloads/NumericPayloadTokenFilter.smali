.class public Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "NumericPayloadTokenFilter.java"


# instance fields
.field private final payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field private thePayload:Lorg/apache/lucene/util/BytesRef;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private typeMatch:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;FLjava/lang/String;)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "payload"    # F
    .param p3, "typeMatch"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 38
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 39
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 43
    if-nez p3, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "typeMatch cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-static {p2}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeFloat(F)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->thePayload:Lorg/apache/lucene/util/BytesRef;

    .line 48
    iput-object p3, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->typeMatch:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->typeMatch:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iget-object v1, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;->thePayload:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 56
    :cond_0
    const/4 v0, 0x1

    .line 58
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
