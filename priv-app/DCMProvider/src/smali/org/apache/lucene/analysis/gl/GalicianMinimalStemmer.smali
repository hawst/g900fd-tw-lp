.class public Lorg/apache/lucene/analysis/gl/GalicianMinimalStemmer;
.super Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.source "GalicianMinimalStemmer.java"


# static fields
.field private static final pluralStep:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/analysis/gl/GalicianMinimalStemmer;

    const-string v1, "galician.rslp"

    invoke-static {v0, v1}, Lorg/apache/lucene/analysis/gl/GalicianMinimalStemmer;->parse(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "Plural"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 32
    sput-object v0, Lorg/apache/lucene/analysis/gl/GalicianMinimalStemmer;->pluralStep:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 36
    sget-object v0, Lorg/apache/lucene/analysis/gl/GalicianMinimalStemmer;->pluralStep:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result v0

    return v0
.end method
