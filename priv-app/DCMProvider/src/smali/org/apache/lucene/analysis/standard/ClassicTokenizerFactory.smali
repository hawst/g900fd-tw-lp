.class public Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;
.super Lorg/apache/lucene/analysis/util/TokenizerFactory;
.source "ClassicTokenizerFactory.java"


# instance fields
.field private final maxTokenLength:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenizerFactory;-><init>(Ljava/util/Map;)V

    .line 41
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;->assureMatchVersion()V

    .line 42
    const-string v0, "maxTokenLength"

    const/16 v1, 0xff

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;->maxTokenLength:I

    .line 43
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Tokenizer;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;->create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/standard/ClassicTokenizer;
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, p1, p2}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 51
    .local v0, "tokenizer":Lorg/apache/lucene/analysis/standard/ClassicTokenizer;
    iget v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerFactory;->maxTokenLength:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->setMaxTokenLength(I)V

    .line 52
    return-object v0
.end method
