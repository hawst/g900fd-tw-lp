.class public Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "FlagsAttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;


# instance fields
.field private flags:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    .line 27
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    .line 42
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 64
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    .line 65
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;
    iget v1, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->setFlags(I)V

    .line 66
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    if-ne p0, p1, :cond_1

    .line 54
    .end local p1    # "other":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 50
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_1
    instance-of v2, p1, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;

    if-eqz v2, :cond_2

    .line 51
    check-cast p1, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;

    .end local p1    # "other":Ljava/lang/Object;
    iget v2, p1, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    iget v3, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .restart local p1    # "other":Ljava/lang/Object;
    :cond_2
    move v0, v1

    .line 54
    goto :goto_0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    return v0
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 36
    iput p1, p0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttributeImpl;->flags:I

    .line 37
    return-void
.end method
