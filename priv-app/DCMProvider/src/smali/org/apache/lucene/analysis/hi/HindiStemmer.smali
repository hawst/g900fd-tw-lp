.class public Lorg/apache/lucene/analysis/hi/HindiStemmer;
.super Ljava/lang/Object;
.source "HindiStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 1
    .param p1, "buffer"    # [C
    .param p2, "len"    # I

    .prologue
    .line 34
    const/4 v0, 0x6

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u093e\u090f\u0902\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const-string/jumbo v0, "\u093e\u090f\u0902\u0917\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    const-string/jumbo v0, "\u093e\u090a\u0902\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    const-string/jumbo v0, "\u093e\u090a\u0902\u0917\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const-string/jumbo v0, "\u093e\u0907\u092f\u093e\u0901"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    const-string/jumbo v0, "\u093e\u0907\u092f\u094b\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    const-string/jumbo v0, "\u093e\u0907\u092f\u093e\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    :cond_0
    add-int/lit8 p2, p2, -0x5

    .line 119
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 45
    .restart local p2    # "len":I
    :cond_2
    const/4 v0, 0x5

    if-le p2, v0, :cond_4

    const-string/jumbo v0, "\u093e\u090f\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 46
    const-string/jumbo v0, "\u093e\u090f\u0917\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 47
    const-string/jumbo v0, "\u093e\u0913\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 48
    const-string/jumbo v0, "\u093e\u0913\u0917\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 49
    const-string/jumbo v0, "\u090f\u0902\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 50
    const-string/jumbo v0, "\u0947\u0902\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 51
    const-string/jumbo v0, "\u090f\u0902\u0917\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 52
    const-string/jumbo v0, "\u0947\u0902\u0917\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 53
    const-string/jumbo v0, "\u0942\u0902\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 54
    const-string/jumbo v0, "\u0942\u0902\u0917\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 55
    const-string/jumbo v0, "\u093e\u0924\u0940\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 56
    const-string/jumbo v0, "\u0928\u093e\u0913\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 57
    const-string/jumbo v0, "\u0928\u093e\u090f\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 58
    const-string/jumbo v0, "\u0924\u093e\u0913\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 59
    const-string/jumbo v0, "\u0924\u093e\u090f\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 60
    const-string/jumbo v0, "\u093f\u092f\u093e\u0901"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 61
    const-string/jumbo v0, "\u093f\u092f\u094b\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 62
    const-string/jumbo v0, "\u093f\u092f\u093e\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 64
    :cond_3
    add-int/lit8 p2, p2, -0x4

    goto/16 :goto_0

    .line 67
    :cond_4
    const/4 v0, 0x4

    if-le p2, v0, :cond_6

    const-string/jumbo v0, "\u093e\u0915\u0930"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 68
    const-string/jumbo v0, "\u093e\u0907\u090f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 69
    const-string/jumbo v0, "\u093e\u0908\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 70
    const-string/jumbo v0, "\u093e\u092f\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 71
    const-string/jumbo v0, "\u0947\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 72
    const-string/jumbo v0, "\u0947\u0917\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 73
    const-string/jumbo v0, "\u094b\u0917\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 74
    const-string/jumbo v0, "\u094b\u0917\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 75
    const-string/jumbo v0, "\u093e\u0928\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 76
    const-string/jumbo v0, "\u093e\u0928\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 77
    const-string/jumbo v0, "\u093e\u0924\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 78
    const-string/jumbo v0, "\u093e\u0924\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 79
    const-string/jumbo v0, "\u093e\u0924\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 80
    const-string/jumbo v0, "\u0924\u0940\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 81
    const-string/jumbo v0, "\u093e\u0913\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 82
    const-string/jumbo v0, "\u093e\u090f\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 83
    const-string/jumbo v0, "\u0941\u0913\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 84
    const-string/jumbo v0, "\u0941\u090f\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 85
    const-string/jumbo v0, "\u0941\u0906\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 87
    :cond_5
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 90
    :cond_6
    const/4 v0, 0x3

    if-le p2, v0, :cond_8

    const-string/jumbo v0, "\u0915\u0930"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 91
    const-string/jumbo v0, "\u093e\u0913"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 92
    const-string/jumbo v0, "\u093f\u090f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 93
    const-string/jumbo v0, "\u093e\u0908"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 94
    const-string/jumbo v0, "\u093e\u090f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 95
    const-string/jumbo v0, "\u0928\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 96
    const-string/jumbo v0, "\u0928\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 97
    const-string/jumbo v0, "\u0928\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 98
    const-string/jumbo v0, "\u0924\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 99
    const-string/jumbo v0, "\u0940\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 100
    const-string/jumbo v0, "\u0924\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 101
    const-string/jumbo v0, "\u0924\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 102
    const-string/jumbo v0, "\u093e\u0901"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 103
    const-string/jumbo v0, "\u093e\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 104
    const-string/jumbo v0, "\u094b\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 105
    const-string/jumbo v0, "\u0947\u0902"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 107
    :cond_7
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 110
    :cond_8
    const/4 v0, 0x2

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u094b"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 111
    const-string/jumbo v0, "\u0947"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 112
    const-string/jumbo v0, "\u0942"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 113
    const-string/jumbo v0, "\u0941"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 114
    const-string/jumbo v0, "\u0940"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 115
    const-string/jumbo v0, "\u093f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 116
    const-string/jumbo v0, "\u093e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 118
    :cond_9
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0
.end method
