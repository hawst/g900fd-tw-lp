.class public Lorg/apache/lucene/analysis/fr/FrenchMinimalStemmer;
.super Ljava/lang/Object;
.source "FrenchMinimalStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 64
    const/4 v0, 0x6

    if-ge p2, v0, :cond_0

    move v0, p2

    .line 78
    :goto_0
    return v0

    .line 67
    :cond_0
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_2

    .line 68
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    const/16 v1, 0x61

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    const/16 v1, 0x75

    if-ne v0, v1, :cond_1

    .line 69
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x6c

    aput-char v1, p1, v0

    .line 70
    :cond_1
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 73
    :cond_2
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_3

    add-int/lit8 p2, p2, -0x1

    .line 74
    :cond_3
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x72

    if-ne v0, v1, :cond_4

    add-int/lit8 p2, p2, -0x1

    .line 75
    :cond_4
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_5

    add-int/lit8 p2, p2, -0x1

    .line 76
    :cond_5
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0xe9

    if-ne v0, v1, :cond_6

    add-int/lit8 p2, p2, -0x1

    .line 77
    :cond_6
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v0, v1, :cond_7

    add-int/lit8 p2, p2, -0x1

    :cond_7
    move v0, p2

    .line 78
    goto :goto_0
.end method
