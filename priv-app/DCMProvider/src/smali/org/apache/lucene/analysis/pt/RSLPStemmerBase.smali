.class public abstract Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.super Ljava/lang/Object;
.source "RSLPStemmerBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;,
        Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSetExceptions;,
        Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;,
        Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final excPattern:Ljava/util/regex/Pattern;

.field private static final headerPattern:Ljava/util/regex/Pattern;

.field private static final repPattern:Ljava/util/regex/Pattern;

.field private static final stripPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->$assertionsDisabled:Z

    .line 265
    const-string v0, "^\\{\\s*\"([^\"]*)\",\\s*([0-9]+),\\s*(0|1),\\s*\\{(.*)\\},\\s*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 264
    sput-object v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->headerPattern:Ljava/util/regex/Pattern;

    .line 267
    const-string v0, "^\\{\\s*\"([^\"]*)\",\\s*([0-9]+)\\s*\\}\\s*(,|(\\}\\s*;))$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 266
    sput-object v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->stripPattern:Ljava/util/regex/Pattern;

    .line 269
    const-string v0, "^\\{\\s*\"([^\"]*)\",\\s*([0-9]+),\\s*\"([^\"]*)\"\\}\\s*(,|(\\}\\s*;))$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 268
    sput-object v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->repPattern:Ljava/util/regex/Pattern;

    .line 271
    const-string v0, "^\\{\\s*\"([^\"]*)\",\\s*([0-9]+),\\s*\"([^\"]*)\",\\s*\\{(.*)\\}\\s*\\}\\s*(,|(\\}\\s*;))$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 270
    sput-object v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->excPattern:Ljava/util/regex/Pattern;

    .line 271
    return-void

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static parse(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .param p1, "resource"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;>;"
    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 250
    .local v1, "is":Ljava/io/InputStream;
    new-instance v2, Ljava/io/LineNumberReader;

    new-instance v6, Ljava/io/InputStreamReader;

    const-string v7, "UTF-8"

    invoke-direct {v6, v1, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v6}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    .line 251
    .local v2, "r":Ljava/io/LineNumberReader;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 253
    .local v5, "steps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;>;"
    :goto_0
    invoke-static {v2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->readLine(Ljava/io/LineNumberReader;)Ljava/lang/String;

    move-result-object v4

    .local v4, "step":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 257
    invoke-virtual {v2}, Ljava/io/LineNumberReader;->close()V

    .line 258
    return-object v5

    .line 254
    :cond_0
    invoke-static {v2, v4}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->parseStep(Ljava/io/LineNumberReader;Ljava/lang/String;)Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    move-result-object v3

    .line 255
    .local v3, "s":Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;
    iget-object v6, v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->name:Ljava/lang/String;

    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 259
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "r":Ljava/io/LineNumberReader;
    .end local v3    # "s":Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;
    .end local v4    # "step":Ljava/lang/String;
    .end local v5    # "steps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;>;"
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/io/IOException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method

.method private static parseList(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 324
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 325
    const/4 v1, 0x0

    .line 329
    :cond_0
    return-object v1

    .line 326
    :cond_1
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "list":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 328
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->parseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 327
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static parseRules(Ljava/io/LineNumberReader;I)[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
    .locals 12
    .param p0, "r"    # Ljava/io/LineNumberReader;
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 288
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v2, "rules":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;>;"
    :cond_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->readLine(Ljava/io/LineNumberReader;)Ljava/lang/String;

    move-result-object v0

    .local v0, "line":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 320
    const/4 v3, 0x0

    :goto_0
    return-object v3

    .line 291
    :cond_1
    sget-object v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->stripPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 292
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 293
    new-instance v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    :goto_1
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 318
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    goto :goto_0

    .line 295
    :cond_2
    sget-object v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->repPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 296
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 297
    new-instance v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 299
    :cond_3
    sget-object v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->excPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 300
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 301
    if-nez p1, :cond_4

    .line 302
    new-instance v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 303
    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 304
    invoke-virtual {v1, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 305
    invoke-virtual {v1, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->parseList(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSuffixExceptions;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V

    .line 302
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 307
    :cond_4
    new-instance v3, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSetExceptions;

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    .line 308
    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 309
    invoke-virtual {v1, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    .line 310
    invoke-virtual {v1, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->parseList(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$RuleWithSetExceptions;-><init>(Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;)V

    .line 307
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 313
    :cond_5
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Illegal Step rule specified at line "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/LineNumberReader;->getLineNumber()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private static parseStep(Ljava/io/LineNumberReader;Ljava/lang/String;)Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;
    .locals 9
    .param p0, "r"    # Ljava/io/LineNumberReader;
    .param p1, "header"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    .line 274
    sget-object v6, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->headerPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 275
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_0

    .line 276
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Illegal Step header specified at line "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/LineNumberReader;->getLineNumber()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 278
    :cond_0
    sget-boolean v6, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    if-eq v6, v7, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 279
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    .local v2, "name":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 281
    .local v1, "min":I
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 282
    .local v5, "type":I
    invoke-virtual {v0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->parseList(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 283
    .local v4, "suffixes":[Ljava/lang/String;
    invoke-static {p0, v5}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;->parseRules(Ljava/io/LineNumberReader;I)[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    move-result-object v3

    .line 284
    .local v3, "rules":[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
    new-instance v6, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-direct {v6, v2, v3, v1, v4}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;-><init>(Ljava/lang/String;[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;I[Ljava/lang/String;)V

    return-object v6
.end method

.method private static parseString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 333
    const/4 v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static readLine(Ljava/io/LineNumberReader;)Ljava/lang/String;
    .locals 4
    .param p0, "r"    # Ljava/io/LineNumberReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    const/4 v0, 0x0

    .line 338
    .local v0, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v1, v0

    .line 343
    .end local v0    # "line":Ljava/lang/String;
    .local v1, "line":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 339
    .end local v1    # "line":Ljava/lang/String;
    .restart local v0    # "line":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x23

    if-eq v2, v3, :cond_0

    move-object v1, v0

    .line 341
    .end local v0    # "line":Ljava/lang/String;
    .restart local v1    # "line":Ljava/lang/String;
    goto :goto_0
.end method
