.class final Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "SlowSynonymFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final expand:Z

.field private final ignoreCase:Z

.field private synMap:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

.field private final synonyms:Ljava/lang/String;

.field private final tf:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 54
    const-string v0, "synonyms"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->synonyms:Ljava/lang/String;

    .line 55
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->ignoreCase:Z

    .line 56
    const-string v0, "expand"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->expand:Z

    .line 58
    const-string v0, "tokenizerFactory"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->tf:Ljava/lang/String;

    .line 59
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    return-void
.end method

.method private static getSynList(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/analysis/util/TokenizerFactory;)Ljava/util/List;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;
    .param p2, "tokFactory"    # Lorg/apache/lucene/analysis/util/TokenizerFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/util/TokenizerFactory;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    const/4 v4, 0x0

    invoke-static {p0, p1, v4}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->splitSmart(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    .line 146
    .local v0, "strList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v1, "synList":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 152
    return-object v1

    .line 147
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 148
    .local v3, "toks":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 149
    const/4 v5, 0x1

    invoke-static {v3, v5}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->splitWS(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v2

    .line 150
    .local v2, "tokList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    .end local v2    # "tokList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-static {v3, p2}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->splitByTokenizer(Ljava/lang/String;Lorg/apache/lucene/analysis/util/TokenizerFactory;)Ljava/util/List;

    move-result-object v2

    goto :goto_1
.end method

.method private static loadTokenizer(Lorg/apache/lucene/analysis/util/TokenizerFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1
    .param p0, "tokFactory"    # Lorg/apache/lucene/analysis/util/TokenizerFactory;
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/util/TokenizerFactory;->create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/Tokenizer;

    move-result-object v0

    return-object v0
.end method

.method private loadTokenizerFactory(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/TokenizerFactory;
    .locals 8
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .param p2, "cname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 173
    .local v1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "luceneMatchVersion"

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getLuceneMatchVersion()Lorg/apache/lucene/util/Version;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/util/Version;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const-class v5, Lorg/apache/lucene/analysis/util/TokenizerFactory;

    invoke-interface {p1, p2, v5}, Lorg/apache/lucene/analysis/util/ResourceLoader;->findClass(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v2

    .line 176
    .local v2, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/apache/lucene/analysis/util/TokenizerFactory;>;"
    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/util/Map;

    aput-object v7, v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/analysis/util/TokenizerFactory;

    .line 177
    .local v4, "tokFactory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    instance-of v5, v4, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;

    if-eqz v5, :cond_0

    .line 178
    move-object v0, v4

    check-cast v0, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;

    move-object v5, v0

    invoke-interface {v5, p1}, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;->inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_0
    return-object v4

    .line 181
    .end local v4    # "tokFactory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    :catch_0
    move-exception v3

    .line 182
    .local v3, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method static parseRules(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;Ljava/lang/String;Ljava/lang/String;ZLorg/apache/lucene/analysis/util/TokenizerFactory;)V
    .locals 15
    .param p1, "map"    # Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .param p2, "mappingSep"    # Ljava/lang/String;
    .param p3, "synSep"    # Ljava/lang/String;
    .param p4, "expansion"    # Z
    .param p5, "tokFactory"    # Lorg/apache/lucene/analysis/util/TokenizerFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lorg/apache/lucene/analysis/util/TokenizerFactory;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    .local p0, "rules":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 100
    .local v2, "count":I
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 140
    return-void

    .line 100
    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 106
    .local v6, "rule":Ljava/lang/String;
    const/4 v10, 0x0

    move-object/from16 v0, p2

    invoke-static {v6, v0, v10}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->splitSmart(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v5

    .line 111
    .local v5, "mapping":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    const/4 v12, 0x2

    if-le v10, v12, :cond_2

    .line 112
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid Synonym Rule:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 113
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v10

    const/4 v12, 0x2

    if-ne v10, v12, :cond_4

    .line 114
    const/4 v10, 0x0

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v10, v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getSynList(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/analysis/util/TokenizerFactory;)Ljava/util/List;

    move-result-object v7

    .line 115
    .local v7, "source":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v10, 0x1

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v10, v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getSynList(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/analysis/util/TokenizerFactory;)Ljava/util/List;

    move-result-object v8

    .line 128
    .local v8, "target":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    :goto_0
    const/4 v4, 0x0

    .line 129
    .local v4, "includeOrig":Z
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 130
    .local v3, "fromToks":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    add-int/lit8 v2, v2, 0x1

    .line 131
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 133
    .local v9, "toToks":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v9}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->makeTokens(Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    .line 135
    const/4 v14, 0x1

    .line 132
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13, v4, v14}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;->add(Ljava/util/List;Ljava/util/List;ZZ)V

    goto :goto_1

    .line 117
    .end local v3    # "fromToks":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "includeOrig":Z
    .end local v7    # "source":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v8    # "target":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v9    # "toToks":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    const/4 v10, 0x0

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v10, v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getSynList(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/analysis/util/TokenizerFactory;)Ljava/util/List;

    move-result-object v7

    .line 118
    .restart local v7    # "source":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    if-eqz p4, :cond_5

    .line 120
    move-object v8, v7

    .line 121
    .restart local v8    # "target":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    goto :goto_0

    .line 123
    .end local v8    # "target":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_5
    new-instance v8, Ljava/util/ArrayList;

    const/4 v10, 0x1

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    .restart local v8    # "target":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static splitByTokenizer(Ljava/lang/String;Lorg/apache/lucene/analysis/util/TokenizerFactory;)Ljava/util/List;
    .locals 5
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "tokFactory"    # Lorg/apache/lucene/analysis/util/TokenizerFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/util/TokenizerFactory;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 157
    .local v0, "reader":Ljava/io/StringReader;
    invoke-static {p1, v0}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->loadTokenizer(Lorg/apache/lucene/analysis/util/TokenizerFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    .line 158
    .local v3, "ts":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v2, "tokList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    const-class v4, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 161
    .local v1, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 166
    invoke-virtual {v0}, Ljava/io/StringReader;->close()V

    .line 168
    return-object v2

    .line 162
    :cond_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 163
    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 165
    .end local v1    # "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    :catchall_0
    move-exception v4

    .line 166
    invoke-virtual {v0}, Ljava/io/StringReader;->close()V

    .line 167
    throw v4
.end method

.method public static splitSmart(Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "separator"    # Ljava/lang/String;
    .param p2, "decode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    new-instance v2, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 249
    .local v2, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 250
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "pos":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .local v1, "end":I
    move v4, v3

    .line 251
    .end local v3    # "pos":I
    .local v4, "pos":I
    :goto_0
    if-lt v4, v1, :cond_2

    move v3, v4

    .line 280
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 281
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    :cond_1
    return-object v2

    .line 252
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :cond_2
    invoke-virtual {p0, p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 253
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 254
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    new-instance v5, Ljava/lang/StringBuilder;

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 257
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int v3, v4, v6

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    move v4, v3

    .line 258
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_0

    .line 261
    :cond_4
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 262
    .local v0, "ch":C
    const/16 v6, 0x5c

    if-ne v0, v6, :cond_7

    .line 263
    if-nez p2, :cond_5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 264
    :cond_5
    if-ge v3, v1, :cond_0

    .line 265
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 266
    if-eqz p2, :cond_6

    .line 267
    sparse-switch v0, :sswitch_data_0

    :cond_6
    move v3, v4

    .line 277
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_7
    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v4, v3

    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_0

    .line 268
    :sswitch_0
    const/16 v0, 0xa

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 269
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_1
    const/16 v0, 0x9

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 270
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_2
    const/16 v0, 0xd

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 271
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_3
    const/16 v0, 0x8

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 272
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_4
    const/16 v0, 0xc

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 267
    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_3
        0x66 -> :sswitch_4
        0x6e -> :sswitch_0
        0x72 -> :sswitch_2
        0x74 -> :sswitch_1
    .end sparse-switch
.end method

.method public static splitWS(Ljava/lang/String;Z)Ljava/util/List;
    .locals 7
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "decode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    new-instance v2, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 200
    .local v2, "lst":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 201
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "pos":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .local v1, "end":I
    move v4, v3

    .line 202
    .end local v3    # "pos":I
    .local v4, "pos":I
    :goto_0
    if-lt v4, v1, :cond_2

    move v3, v4

    .line 230
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 231
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    :cond_1
    return-object v2

    .line 203
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :cond_2
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 204
    .local v0, "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 205
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_7

    .line 206
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    new-instance v5, Ljava/lang/StringBuilder;

    .end local v5    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    move v4, v3

    .line 209
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_0

    .line 212
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_3
    const/16 v6, 0x5c

    if-ne v0, v6, :cond_6

    .line 213
    if-nez p1, :cond_4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 214
    :cond_4
    if-ge v3, v1, :cond_0

    .line 215
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 216
    if-eqz p1, :cond_5

    .line 217
    sparse-switch v0, :sswitch_data_0

    :cond_5
    move v3, v4

    .line 227
    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    :cond_6
    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_7
    move v4, v3

    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_0

    .line 218
    :sswitch_0
    const/16 v0, 0xa

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 219
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_1
    const/16 v0, 0x9

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 220
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_2
    const/16 v0, 0xd

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 221
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_3
    const/16 v0, 0x8

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 222
    .end local v3    # "pos":I
    .restart local v4    # "pos":I
    :sswitch_4
    const/16 v0, 0xc

    move v3, v4

    .end local v4    # "pos":I
    .restart local v3    # "pos":I
    goto :goto_1

    .line 217
    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_3
        0x66 -> :sswitch_4
        0x6e -> :sswitch_0
        0x72 -> :sswitch_2
        0x74 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 195
    new-instance v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->synMap:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;)V

    return-object v0
.end method

.method public getSynonymMap()Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->synMap:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 6
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    const/4 v5, 0x0

    .line 66
    .local v5, "tokFactory":Lorg/apache/lucene/analysis/util/TokenizerFactory;
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->tf:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->tf:Ljava/lang/String;

    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->loadTokenizerFactory(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/TokenizerFactory;

    move-result-object v5

    .line 70
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->synonyms:Ljava/lang/String;

    invoke-virtual {p0, v1, p1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->loadRules(Ljava/lang/String;Lorg/apache/lucene/analysis/util/ResourceLoader;)Ljava/lang/Iterable;

    move-result-object v0

    .line 72
    .local v0, "wlist":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/String;>;"
    new-instance v1, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    iget-boolean v2, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->ignoreCase:Z

    invoke-direct {v1, v2}, Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;-><init>(Z)V

    iput-object v1, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->synMap:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->synMap:Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;

    const-string v2, "=>"

    const-string v3, ","

    iget-boolean v4, p0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->expand:Z

    invoke-static/range {v0 .. v5}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->parseRules(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/synonym/SlowSynonymMap;Ljava/lang/String;Ljava/lang/String;ZLorg/apache/lucene/analysis/util/TokenizerFactory;)V

    .line 74
    return-void
.end method

.method protected loadRules(Ljava/lang/String;Lorg/apache/lucene/analysis/util/ResourceLoader;)Ljava/lang/Iterable;
    .locals 7
    .param p1, "synonyms"    # Ljava/lang/String;
    .param p2, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/analysis/util/ResourceLoader;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v4, 0x0

    .line 81
    .local v4, "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .local v3, "synonymFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 83
    invoke-virtual {p0, p2, p1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 92
    :cond_0
    return-object v4

    .line 85
    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 86
    .local v1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .restart local v4    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p2, v6}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 89
    .local v2, "lines":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
