.class public Lorg/apache/lucene/analysis/cz/CzechStemmer;
.super Ljava/lang/Object;
.source "CzechStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private normalize([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v2, 0x6b

    .line 138
    const-string/jumbo v0, "\u010dt"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x63

    aput-char v1, p1, v0

    .line 140
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    .line 171
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 144
    .restart local p2    # "len":I
    :cond_1
    const-string/jumbo v0, "\u0161t"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 145
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x73

    aput-char v1, p1, v0

    .line 146
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    goto :goto_0

    .line 150
    :cond_2
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    .line 161
    const/4 v0, 0x1

    if-le p2, v0, :cond_3

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_3

    .line 162
    add-int/lit8 v0, p2, -0x2

    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    aput-char v1, p1, v0

    .line 163
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 153
    :sswitch_0
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    goto :goto_0

    .line 157
    :sswitch_1
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x68

    aput-char v1, p1, v0

    goto :goto_0

    .line 166
    :cond_3
    const/4 v0, 0x2

    if-le p2, v0, :cond_0

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    const/16 v1, 0x16f

    if-ne v0, v1, :cond_0

    .line 167
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x6f

    aput-char v1, p1, v0

    goto :goto_0

    .line 150
    nop

    :sswitch_data_0
    .sparse-switch
        0x63 -> :sswitch_0
        0x7a -> :sswitch_1
        0x10d -> :sswitch_0
        0x17e -> :sswitch_1
    .end sparse-switch
.end method

.method private removeCase([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 54
    const/4 v0, 0x7

    if-le p2, v0, :cond_1

    const-string v0, "atech"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    add-int/lit8 p2, p2, -0x5

    .line 124
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 57
    .restart local p2    # "len":I
    :cond_1
    const/4 v0, 0x6

    if-le p2, v0, :cond_3

    .line 58
    const-string/jumbo v0, "\u011btem"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59
    const-string v0, "etem"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 60
    const-string v0, "at\u016fm"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    :cond_2
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 63
    :cond_3
    const/4 v0, 0x5

    if-le p2, v0, :cond_5

    .line 64
    const-string v0, "ech"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 65
    const-string v0, "ich"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 66
    const-string/jumbo v0, "\u00edch"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 67
    const-string/jumbo v0, "\u00e9ho"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 68
    const-string/jumbo v0, "\u011bmi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 69
    const-string v0, "emi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 70
    const-string/jumbo v0, "\u00e9mu"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 71
    const-string/jumbo v0, "\u011bte"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 72
    const-string v0, "ete"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 73
    const-string/jumbo v0, "\u011bti"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 74
    const-string v0, "eti"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 75
    const-string/jumbo v0, "\u00edho"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 76
    const-string v0, "iho"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 77
    const-string/jumbo v0, "\u00edmi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 78
    const-string/jumbo v0, "\u00edmu"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 79
    const-string v0, "imu"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 80
    const-string/jumbo v0, "\u00e1ch"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 81
    const-string v0, "ata"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 82
    const-string v0, "aty"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 83
    const-string/jumbo v0, "\u00fdch"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 84
    const-string v0, "ama"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 85
    const-string v0, "ami"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 86
    const-string v0, "ov\u00e9"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 87
    const-string v0, "ovi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 88
    const-string/jumbo v0, "\u00fdmi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89
    :cond_4
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 91
    :cond_5
    const/4 v0, 0x4

    if-le p2, v0, :cond_7

    .line 92
    const-string v0, "em"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 93
    const-string v0, "es"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 94
    const-string/jumbo v0, "\u00e9m"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 95
    const-string/jumbo v0, "\u00edm"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 96
    const-string/jumbo v0, "\u016fm"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 97
    const-string v0, "at"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 98
    const-string/jumbo v0, "\u00e1m"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 99
    const-string v0, "os"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 100
    const-string v0, "us"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 101
    const-string/jumbo v0, "\u00fdm"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 102
    const-string v0, "mi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 103
    const-string v0, "ou"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 104
    :cond_6
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 106
    :cond_7
    const/4 v0, 0x3

    if-le p2, v0, :cond_0

    .line 107
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 120
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 107
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x79 -> :sswitch_0
        0xe1 -> :sswitch_0
        0xe9 -> :sswitch_0
        0xed -> :sswitch_0
        0xfd -> :sswitch_0
        0x11b -> :sswitch_0
        0x16f -> :sswitch_0
    .end sparse-switch
.end method

.method private removePossessives([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 128
    const/4 v0, 0x5

    if-le p2, v0, :cond_1

    .line 129
    const-string v0, "ov"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const-string v0, "in"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const-string/jumbo v0, "\u016fv"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    :cond_0
    add-int/lit8 p2, p2, -0x2

    .line 134
    .end local p2    # "len":I
    :cond_1
    return p2
.end method


# virtual methods
.method public stem([CI)I
    .locals 0
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/cz/CzechStemmer;->removeCase([CI)I

    move-result p2

    .line 46
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/cz/CzechStemmer;->removePossessives([CI)I

    move-result p2

    .line 47
    if-lez p2, :cond_0

    .line 48
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/cz/CzechStemmer;->normalize([CI)I

    move-result p2

    .line 50
    :cond_0
    return p2
.end method
