.class Lorg/apache/lucene/analysis/fa/PersianAnalyzer$DefaultSetHolder;
.super Ljava/lang/Object;
.source "PersianAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/fa/PersianAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultSetHolder"
.end annotation


# static fields
.field static final DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 78
    const/4 v1, 0x0

    :try_start_0
    const-class v2, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;

    const-string v3, "stopwords.txt"

    const-string v4, "#"

    # invokes: Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->loadStopwordSet(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->access$0(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/analysis/fa/PersianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 82
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to load default stopword set"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
