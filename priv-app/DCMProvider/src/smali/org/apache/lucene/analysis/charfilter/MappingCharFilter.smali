.class public Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;
.super Lorg/apache/lucene/analysis/charfilter/BaseCharFilter;
.source "MappingCharFilter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

.field private final cachedRootArcs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;>;"
        }
    .end annotation
.end field

.field private final fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private inputOff:I

.field private final map:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation
.end field

.field private final outputs:Lorg/apache/lucene/util/fst/Outputs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Outputs",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation
.end field

.field private replacement:Lorg/apache/lucene/util/CharsRef;

.field private replacementPointer:I

.field private final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;Ljava/io/Reader;)V
    .locals 1
    .param p1, "normMap"    # Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 55
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/charfilter/BaseCharFilter;-><init>(Ljava/io/Reader;)V

    .line 42
    invoke-static {}, Lorg/apache/lucene/util/fst/CharSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/CharSequenceOutputs;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 45
    new-instance v0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->reset(Ljava/io/Reader;)V

    .line 58
    iget-object v0, p1, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->map:Lorg/apache/lucene/util/fst/FST;

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->map:Lorg/apache/lucene/util/fst/FST;

    .line 59
    iget-object v0, p1, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->cachedRootArcs:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->cachedRootArcs:Ljava/util/Map;

    .line 61
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->map:Lorg/apache/lucene/util/fst/FST;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->map:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    goto :goto_0
.end method


# virtual methods
.method public read()I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacement:Lorg/apache/lucene/util/CharsRef;

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacementPointer:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacement:Lorg/apache/lucene/util/CharsRef;

    iget v15, v15, Lorg/apache/lucene/util/CharsRef;->length:I

    if-ge v14, v15, :cond_1

    .line 84
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacement:Lorg/apache/lucene/util/CharsRef;

    iget-object v14, v14, Lorg/apache/lucene/util/CharsRef;->chars:[C

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacement:Lorg/apache/lucene/util/CharsRef;

    iget v15, v15, Lorg/apache/lucene/util/CharsRef;->offset:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacementPointer:I

    move/from16 v16, v0

    add-int/lit8 v17, v16, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacementPointer:I

    add-int v15, v15, v16

    aget-char v13, v14, v15

    .line 174
    :cond_0
    :goto_1
    return v13

    .line 99
    :cond_1
    const/4 v8, -0x1

    .line 100
    .local v8, "lastMatchLen":I
    const/4 v7, 0x0

    .line 102
    .local v7, "lastMatch":Lorg/apache/lucene/util/CharsRef;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    invoke-virtual {v14, v15}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->get(I)I

    move-result v6

    .line 103
    .local v6, "firstCH":I
    const/4 v14, -0x1

    if-eq v6, v14, :cond_3

    .line 104
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->cachedRootArcs:Ljava/util/Map;

    int-to-char v15, v6

    invoke-static {v15}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/util/fst/FST$Arc;

    .line 105
    .local v2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    if-eqz v2, :cond_3

    .line 106
    invoke-static {v2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 108
    sget-boolean v14, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->$assertionsDisabled:Z

    if-nez v14, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v14

    if-nez v14, :cond_2

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 109
    :cond_2
    const/4 v8, 0x1

    .line 110
    iget-object v7, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    .end local v7    # "lastMatch":Lorg/apache/lucene/util/CharsRef;
    check-cast v7, Lorg/apache/lucene/util/CharsRef;

    .line 143
    .end local v2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    .restart local v7    # "lastMatch":Lorg/apache/lucene/util/CharsRef;
    :cond_3
    if-eqz v7, :cond_8

    .line 144
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    add-int/2addr v14, v8

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    .line 147
    iget v14, v7, Lorg/apache/lucene/util/CharsRef;->length:I

    sub-int v4, v8, v14

    .line 149
    .local v4, "diff":I
    if-eqz v4, :cond_4

    .line 150
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->getLastCumulativeDiff()I

    move-result v12

    .line 151
    .local v12, "prevCumulativeDiff":I
    if-lez v4, :cond_7

    .line 153
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    sub-int/2addr v14, v4

    sub-int/2addr v14, v12

    add-int v15, v12, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->addOffCorrectMap(II)V

    .line 165
    .end local v12    # "prevCumulativeDiff":I
    :cond_4
    move-object/from16 v0, p0

    iput-object v7, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacement:Lorg/apache/lucene/util/CharsRef;

    .line 166
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacementPointer:I

    goto/16 :goto_0

    .line 112
    .end local v4    # "diff":I
    .restart local v2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    :cond_5
    const/4 v9, 0x0

    .line 113
    .local v9, "lookahead":I
    iget-object v10, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v10, Lorg/apache/lucene/util/CharsRef;

    .line 115
    .local v10, "output":Lorg/apache/lucene/util/CharsRef;
    :goto_2
    add-int/lit8 v9, v9, 0x1

    .line 117
    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 119
    move v8, v9

    .line 120
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v14, v2, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v14, Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v15, v10, v14}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "lastMatch":Lorg/apache/lucene/util/CharsRef;
    check-cast v7, Lorg/apache/lucene/util/CharsRef;

    .line 125
    .restart local v7    # "lastMatch":Lorg/apache/lucene/util/CharsRef;
    :cond_6
    invoke-static {v2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 129
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    add-int/2addr v15, v9

    invoke-virtual {v14, v15}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->get(I)I

    move-result v3

    .line 130
    .local v3, "ch":I
    const/4 v14, -0x1

    if-eq v3, v14, :cond_3

    .line 133
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->map:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v14, v3, v2, v15, v0}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 137
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v14, v2, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v14, Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v15, v10, v14}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "output":Lorg/apache/lucene/util/CharsRef;
    check-cast v10, Lorg/apache/lucene/util/CharsRef;

    .line 114
    .restart local v10    # "output":Lorg/apache/lucene/util/CharsRef;
    goto :goto_2

    .line 158
    .end local v2    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    .end local v3    # "ch":I
    .end local v9    # "lookahead":I
    .end local v10    # "output":Lorg/apache/lucene/util/CharsRef;
    .restart local v4    # "diff":I
    .restart local v12    # "prevCumulativeDiff":I
    :cond_7
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    sub-int v11, v14, v12

    .line 159
    .local v11, "outputStart":I
    const/4 v5, 0x0

    .local v5, "extraIDX":I
    :goto_3
    neg-int v14, v4

    if-ge v5, v14, :cond_4

    .line 160
    add-int v14, v11, v5

    sub-int v15, v12, v5

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->addOffCorrectMap(II)V

    .line 159
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 169
    .end local v4    # "diff":I
    .end local v5    # "extraIDX":I
    .end local v11    # "outputStart":I
    .end local v12    # "prevCumulativeDiff":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    invoke-virtual {v14, v15}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->get(I)I

    move-result v13

    .line 170
    .local v13, "ret":I
    const/4 v14, -0x1

    if-eq v13, v14, :cond_0

    .line 171
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    .line 172
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    invoke-virtual {v14, v15}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->freeBefore(I)V

    goto/16 :goto_1
.end method

.method public read([CII)I
    .locals 5
    .param p1, "cbuf"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 181
    const/4 v2, 0x0

    .line 182
    .local v2, "numRead":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    add-int v4, p2, p3

    if-lt v1, v4, :cond_2

    .line 189
    :cond_0
    if-nez v2, :cond_1

    move v2, v3

    .end local v2    # "numRead":I
    :cond_1
    return v2

    .line 183
    .restart local v2    # "numRead":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->read()I

    move-result v0

    .line 184
    .local v0, "c":I
    if-eq v0, v3, :cond_0

    .line 185
    int-to-char v4, v0

    aput-char v4, p1, v1

    .line 186
    add-int/lit8 v2, v2, 0x1

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->reset()V

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->buffer:Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->reset(Ljava/io/Reader;)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->replacement:Lorg/apache/lucene/util/CharsRef;

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;->inputOff:I

    .line 74
    return-void
.end method
