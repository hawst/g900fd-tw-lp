.class public Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "ShingleFilterFactory.java"


# instance fields
.field private final maxShingleSize:I

.field private final minShingleSize:I

.field private final outputUnigrams:Z

.field private final outputUnigramsIfNoShingles:Z

.field private final tokenSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x2

    .line 45
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 46
    const-string v0, "maxShingleSize"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->maxShingleSize:I

    .line 47
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->maxShingleSize:I

    if-ge v0, v1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid maxShingleSize ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->maxShingleSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") - must be at least 2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    const-string v0, "minShingleSize"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->minShingleSize:I

    .line 51
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->minShingleSize:I

    if-ge v0, v1, :cond_1

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid minShingleSize ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->minShingleSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") - must be at least 2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->minShingleSize:I

    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->maxShingleSize:I

    if-le v0, v1, :cond_2

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 56
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid minShingleSize ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->minShingleSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") - must be no greater than maxShingleSize ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->maxShingleSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_2
    const-string v0, "outputUnigrams"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->outputUnigrams:Z

    .line 59
    const-string v0, "outputUnigramsIfNoShingles"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->outputUnigramsIfNoShingles:Z

    .line 60
    const-string v0, "tokenSeparator"

    const-string v1, " "

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->tokenSeparator:Ljava/lang/String;

    .line 61
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_3
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/shingle/ShingleFilter;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 68
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->minShingleSize:I

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->maxShingleSize:I

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;II)V

    .line 69
    .local v0, "r":Lorg/apache/lucene/analysis/shingle/ShingleFilter;
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->outputUnigrams:Z

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setOutputUnigrams(Z)V

    .line 70
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->outputUnigramsIfNoShingles:Z

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setOutputUnigramsIfNoShingles(Z)V

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilterFactory;->tokenSeparator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setTokenSeparator(Ljava/lang/String;)V

    .line 72
    return-object v0
.end method
