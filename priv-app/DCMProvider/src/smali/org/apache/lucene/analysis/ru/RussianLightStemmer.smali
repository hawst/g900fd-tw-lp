.class public Lorg/apache/lucene/analysis/ru/RussianLightStemmer;
.super Ljava/lang/Object;
.source "RussianLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private normalize([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 72
    const/4 v0, 0x3

    if-le p2, v0, :cond_0

    .line 73
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    .line 78
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 75
    .restart local p2    # "len":I
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 76
    :sswitch_1
    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    const/16 v1, 0x43d

    if-ne v0, v1, :cond_0

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x438 -> :sswitch_0
        0x43d -> :sswitch_1
        0x44c -> :sswitch_0
    .end sparse-switch
.end method

.method private removeCase([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 82
    const/4 v0, 0x6

    if-le p2, v0, :cond_2

    .line 83
    const-string/jumbo v0, "\u0438\u044f\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const-string/jumbo v0, "\u043e\u044f\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    :cond_0
    add-int/lit8 p2, p2, -0x4

    .line 151
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 87
    .restart local p2    # "len":I
    :cond_2
    const/4 v0, 0x5

    if-le p2, v0, :cond_4

    .line 88
    const-string/jumbo v0, "\u0438\u044f\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 89
    const-string/jumbo v0, "\u0438\u044f\u0445"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 90
    const-string/jumbo v0, "\u043e\u044f\u0445"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 91
    const-string/jumbo v0, "\u044f\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 92
    const-string/jumbo v0, "\u043e\u044f\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 93
    const-string/jumbo v0, "\u043e\u044c\u0432"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 94
    const-string/jumbo v0, "\u0430\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 95
    const-string/jumbo v0, "\u0435\u0433\u043e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 96
    const-string/jumbo v0, "\u0435\u043c\u0443"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 97
    const-string/jumbo v0, "\u0435\u0440\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 98
    const-string/jumbo v0, "\u0438\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 99
    const-string/jumbo v0, "\u043e\u0433\u043e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 100
    const-string/jumbo v0, "\u043e\u043c\u0443"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 101
    const-string/jumbo v0, "\u044b\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 102
    const-string/jumbo v0, "\u043e\u0435\u0432"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 103
    :cond_3
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 105
    :cond_4
    const/4 v0, 0x4

    if-le p2, v0, :cond_6

    .line 106
    const-string/jumbo v0, "\u0430\u044f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 107
    const-string/jumbo v0, "\u044f\u044f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 108
    const-string/jumbo v0, "\u044f\u0445"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 109
    const-string/jumbo v0, "\u044e\u044e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 110
    const-string/jumbo v0, "\u0430\u0445"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 111
    const-string/jumbo v0, "\u0435\u044e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 112
    const-string/jumbo v0, "\u0438\u0445"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 113
    const-string/jumbo v0, "\u0438\u044f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 114
    const-string/jumbo v0, "\u0438\u044e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 115
    const-string/jumbo v0, "\u044c\u0432"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 116
    const-string/jumbo v0, "\u043e\u044e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 117
    const-string/jumbo v0, "\u0443\u044e"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 118
    const-string/jumbo v0, "\u044f\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 119
    const-string/jumbo v0, "\u044b\u0445"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 120
    const-string/jumbo v0, "\u0435\u044f"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 121
    const-string/jumbo v0, "\u0430\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 122
    const-string/jumbo v0, "\u0435\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 123
    const-string/jumbo v0, "\u0435\u0439"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 124
    const-string/jumbo v0, "\u0451\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 125
    const-string/jumbo v0, "\u0435\u0432"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 126
    const-string/jumbo v0, "\u0438\u0439"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 127
    const-string/jumbo v0, "\u0438\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 128
    const-string/jumbo v0, "\u043e\u0435"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 129
    const-string/jumbo v0, "\u043e\u0439"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 130
    const-string/jumbo v0, "\u043e\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 131
    const-string/jumbo v0, "\u043e\u0432"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 132
    const-string/jumbo v0, "\u044b\u0435"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 133
    const-string/jumbo v0, "\u044b\u0439"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 134
    const-string/jumbo v0, "\u044b\u043c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 135
    const-string/jumbo v0, "\u043c\u0438"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 136
    :cond_5
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 138
    :cond_6
    const/4 v0, 0x3

    if-le p2, v0, :cond_1

    .line 139
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 148
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 139
    nop

    :sswitch_data_0
    .sparse-switch
        0x430 -> :sswitch_0
        0x435 -> :sswitch_0
        0x438 -> :sswitch_0
        0x439 -> :sswitch_0
        0x43e -> :sswitch_0
        0x443 -> :sswitch_0
        0x44b -> :sswitch_0
        0x44c -> :sswitch_0
        0x44f -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public stem([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/ru/RussianLightStemmer;->removeCase([CI)I

    move-result p2

    .line 68
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/ru/RussianLightStemmer;->normalize([CI)I

    move-result v0

    return v0
.end method
