.class public Lorg/apache/lucene/analysis/el/GreekStemmer;
.super Ljava/lang/Object;
.source "GreekStemmer.java"


# static fields
.field private static final exc12a:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc12b:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc13:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc14:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc15a:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc15b:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc16:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc17:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc18:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc19:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc4:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc6:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc7:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc8a:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc8b:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final exc9:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 199
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    .line 200
    const-string/jumbo v3, "\u03b8"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b4"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b5\u03bb"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b3\u03b1\u03bb"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03bd"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03c0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03b9\u03b4"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "\u03c0\u03b1\u03c1"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 199
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc4:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 225
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x24

    new-array v2, v2, [Ljava/lang/String;

    .line 226
    const-string/jumbo v3, "\u03b1\u03bb"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b1\u03b4"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b5\u03bd\u03b4"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b1\u03bc\u03b1\u03bd"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b1\u03bc\u03bc\u03bf\u03c7\u03b1\u03bb"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b7\u03b8"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03b1\u03bd\u03b7\u03b8"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 227
    const-string/jumbo v4, "\u03b1\u03bd\u03c4\u03b9\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "\u03c6\u03c5\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03b2\u03c1\u03c9\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "\u03b3\u03b5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "\u03b5\u03be\u03c9\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "\u03ba\u03b1\u03bb\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "\u03ba\u03b1\u03bb\u03bb\u03b9\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "\u03ba\u03b1\u03c4\u03b1\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    .line 228
    const-string/jumbo v4, "\u03bc\u03bf\u03c5\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "\u03bc\u03c0\u03b1\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string/jumbo v4, "\u03bc\u03c0\u03b1\u03b3\u03b9\u03b1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string/jumbo v4, "\u03bc\u03c0\u03bf\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "\u03bc\u03c0\u03bf\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "\u03bd\u03b9\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "\u03be\u03b9\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string/jumbo v4, "\u03c3\u03c5\u03bd\u03bf\u03bc\u03b7\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    .line 229
    const-string/jumbo v4, "\u03c0\u03b5\u03c4\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string/jumbo v4, "\u03c0\u03b9\u03c4\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string/jumbo v4, "\u03c0\u03b9\u03ba\u03b1\u03bd\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string/jumbo v4, "\u03c0\u03bb\u03b9\u03b1\u03c4\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string/jumbo v4, "\u03c0\u03bf\u03c3\u03c4\u03b5\u03bb\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string/jumbo v4, "\u03c0\u03c1\u03c9\u03c4\u03bf\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string/jumbo v4, "\u03c3\u03b5\u03c1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    .line 230
    const-string/jumbo v4, "\u03c3\u03c5\u03bd\u03b1\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string/jumbo v4, "\u03c4\u03c3\u03b1\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string/jumbo v4, "\u03c5\u03c0\u03bf\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string/jumbo v4, "\u03c6\u03b9\u03bb\u03bf\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0x22

    const-string/jumbo v4, "\u03c6\u03c5\u03bb\u03bf\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0x23

    const-string/jumbo v4, "\u03c7\u03b1\u03c3"

    aput-object v4, v2, v3

    .line 226
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 225
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc6:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 250
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/String;

    .line 251
    const-string/jumbo v3, "\u03b1\u03bd\u03b1\u03c0"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b1\u03c0\u03bf\u03b8"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b1\u03c0\u03bf\u03ba"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b1\u03c0\u03bf\u03c3\u03c4"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b2\u03bf\u03c5\u03b2"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03be\u03b5\u03b8"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03bf\u03c5\u03bb"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 252
    const-string/jumbo v4, "\u03c0\u03b5\u03b8"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "\u03c0\u03b9\u03ba\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03c0\u03bf\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "\u03c3\u03b9\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "\u03c7"

    aput-object v4, v2, v3

    .line 251
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 250
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc7:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 277
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    new-array v2, v7, [Ljava/lang/String;

    .line 278
    const-string/jumbo v3, "\u03c4\u03c1"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c4\u03c3"

    aput-object v3, v2, v6

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 277
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc8a:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 281
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x60

    new-array v2, v2, [Ljava/lang/String;

    .line 282
    const-string/jumbo v3, "\u03b2\u03b5\u03c4\u03b5\u03c1"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b2\u03bf\u03c5\u03bb\u03ba"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b2\u03c1\u03b1\u03c7\u03bc"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b3"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b4\u03c1\u03b1\u03b4\u03bf\u03c5\u03bc"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b8"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03ba\u03b1\u03bb\u03c0\u03bf\u03c5\u03b6"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 283
    const-string/jumbo v4, "\u03ba\u03b1\u03c3\u03c4\u03b5\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "\u03ba\u03bf\u03c1\u03bc\u03bf\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03bb\u03b1\u03bf\u03c0\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "\u03bc\u03c9\u03b1\u03bc\u03b5\u03b8"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "\u03bc\u03bf\u03c5\u03c3\u03bf\u03c5\u03bb\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "\u03bf\u03c5\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    .line 284
    const-string/jumbo v4, "\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "\u03c0\u03b5\u03bb\u03b5\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string/jumbo v4, "\u03c0\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string/jumbo v4, "\u03c0\u03bf\u03bb\u03b9\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "\u03c0\u03bf\u03c1\u03c4\u03bf\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "\u03c3\u03b1\u03c1\u03b1\u03ba\u03b1\u03c4\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "\u03c3\u03bf\u03c5\u03bb\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    .line 285
    const-string/jumbo v4, "\u03c4\u03c3\u03b1\u03c1\u03bb\u03b1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string/jumbo v4, "\u03bf\u03c1\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string/jumbo v4, "\u03c4\u03c3\u03b9\u03b3\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string/jumbo v4, "\u03c4\u03c3\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string/jumbo v4, "\u03c6\u03c9\u03c4\u03bf\u03c3\u03c4\u03b5\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string/jumbo v4, "\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string/jumbo v4, "\u03c8\u03c5\u03c7\u03bf\u03c0\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string/jumbo v4, "\u03b1\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    .line 286
    const-string/jumbo v4, "\u03bf\u03c1\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string/jumbo v4, "\u03b3\u03b1\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string/jumbo v4, "\u03b3\u03b5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string/jumbo v4, "\u03b4\u03b5\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x22

    const-string/jumbo v4, "\u03b4\u03b9\u03c0\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x23

    const-string/jumbo v4, "\u03b1\u03bc\u03b5\u03c1\u03b9\u03ba\u03b1\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0x24

    const-string/jumbo v4, "\u03bf\u03c5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x25

    const-string/jumbo v4, "\u03c0\u03b9\u03b8"

    aput-object v4, v2, v3

    const/16 v3, 0x26

    .line 287
    const-string/jumbo v4, "\u03c0\u03bf\u03c5\u03c1\u03b9\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x27

    const-string/jumbo v4, "\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x28

    const-string/jumbo v4, "\u03b6\u03c9\u03bd\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x29

    const-string/jumbo v4, "\u03b9\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    const-string/jumbo v4, "\u03ba\u03b1\u03c3\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    const-string/jumbo v4, "\u03ba\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    const-string/jumbo v4, "\u03bb\u03b9\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    const-string/jumbo v4, "\u03bb\u03bf\u03c5\u03b8\u03b7\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    const-string/jumbo v4, "\u03bc\u03b1\u03b9\u03bd\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    .line 288
    const-string/jumbo v4, "\u03bc\u03b5\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x30

    const-string/jumbo v4, "\u03c3\u03b9\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x31

    const-string/jumbo v4, "\u03c3\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x32

    const-string/jumbo v4, "\u03c3\u03c4\u03b5\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x33

    const-string/jumbo v4, "\u03c4\u03c1\u03b1\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x34

    const-string/jumbo v4, "\u03c4\u03c3\u03b1\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x35

    const-string/jumbo v4, "\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x36

    const-string/jumbo v4, "\u03b5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x37

    const-string/jumbo v4, "\u03b1\u03b4\u03b1\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x38

    .line 289
    const-string/jumbo v4, "\u03b1\u03b8\u03b9\u03b3\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x39

    const-string/jumbo v4, "\u03b1\u03bc\u03b7\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x3a

    const-string/jumbo v4, "\u03b1\u03bd\u03b9\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x3b

    const-string/jumbo v4, "\u03b1\u03bd\u03bf\u03c1\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x3c

    const-string/jumbo v4, "\u03b1\u03c0\u03b7\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x3d

    const-string/jumbo v4, "\u03b1\u03c0\u03b9\u03b8"

    aput-object v4, v2, v3

    const/16 v3, 0x3e

    const-string/jumbo v4, "\u03b1\u03c4\u03c3\u03b9\u03b3\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x3f

    const-string/jumbo v4, "\u03b2\u03b1\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x40

    .line 290
    const-string/jumbo v4, "\u03b2\u03b1\u03c3\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x41

    const-string/jumbo v4, "\u03b2\u03b1\u03b8\u03c5\u03b3\u03b1\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x42

    const-string/jumbo v4, "\u03b2\u03b9\u03bf\u03bc\u03b7\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x43

    const-string/jumbo v4, "\u03b2\u03c1\u03b1\u03c7\u03c5\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x44

    const-string/jumbo v4, "\u03b4\u03b9\u03b1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x45

    const-string/jumbo v4, "\u03b4\u03b9\u03b1\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x46

    const-string/jumbo v4, "\u03b5\u03bd\u03bf\u03c1\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0x47

    .line 291
    const-string/jumbo v4, "\u03b8\u03c5\u03c3"

    aput-object v4, v2, v3

    const/16 v3, 0x48

    const-string/jumbo v4, "\u03ba\u03b1\u03c0\u03bd\u03bf\u03b2\u03b9\u03bf\u03bc\u03b7\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x49

    const-string/jumbo v4, "\u03ba\u03b1\u03c4\u03b1\u03b3\u03b1\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x4a

    const-string/jumbo v4, "\u03ba\u03bb\u03b9\u03b2"

    aput-object v4, v2, v3

    const/16 v3, 0x4b

    const-string/jumbo v4, "\u03ba\u03bf\u03b9\u03bb\u03b1\u03c1\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x4c

    const-string/jumbo v4, "\u03bb\u03b9\u03b2"

    aput-object v4, v2, v3

    const/16 v3, 0x4d

    .line 292
    const-string/jumbo v4, "\u03bc\u03b5\u03b3\u03bb\u03bf\u03b2\u03b9\u03bf\u03bc\u03b7\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x4e

    const-string/jumbo v4, "\u03bc\u03b9\u03ba\u03c1\u03bf\u03b2\u03b9\u03bf\u03bc\u03b7\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x4f

    const-string/jumbo v4, "\u03bd\u03c4\u03b1\u03b2"

    aput-object v4, v2, v3

    const/16 v3, 0x50

    const-string/jumbo v4, "\u03be\u03b7\u03c1\u03bf\u03ba\u03bb\u03b9\u03b2"

    aput-object v4, v2, v3

    const/16 v3, 0x51

    const-string/jumbo v4, "\u03bf\u03bb\u03b9\u03b3\u03bf\u03b4\u03b1\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0x52

    .line 293
    const-string/jumbo v4, "\u03bf\u03bb\u03bf\u03b3\u03b1\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x53

    const-string/jumbo v4, "\u03c0\u03b5\u03bd\u03c4\u03b1\u03c1\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x54

    const-string/jumbo v4, "\u03c0\u03b5\u03c1\u03b7\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x55

    const-string/jumbo v4, "\u03c0\u03b5\u03c1\u03b9\u03c4\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x56

    const-string/jumbo v4, "\u03c0\u03bb\u03b1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x57

    const-string/jumbo v4, "\u03c0\u03bf\u03bb\u03c5\u03b4\u03b1\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x58

    const-string/jumbo v4, "\u03c0\u03bf\u03bb\u03c5\u03bc\u03b7\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x59

    .line 294
    const-string/jumbo v4, "\u03c3\u03c4\u03b5\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x5a

    const-string/jumbo v4, "\u03c4\u03b1\u03b2"

    aput-object v4, v2, v3

    const/16 v3, 0x5b

    const-string/jumbo v4, "\u03c4\u03b5\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x5c

    const-string/jumbo v4, "\u03c5\u03c0\u03b5\u03c1\u03b7\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x5d

    const-string/jumbo v4, "\u03c5\u03c0\u03bf\u03ba\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x5e

    const-string/jumbo v4, "\u03c7\u03b1\u03bc\u03b7\u03bb\u03bf\u03b4\u03b1\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x5f

    const-string/jumbo v4, "\u03c8\u03b7\u03bb\u03bf\u03c4\u03b1\u03b2"

    aput-object v4, v2, v3

    .line 282
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 281
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc8b:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 340
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x19

    new-array v2, v2, [Ljava/lang/String;

    .line 341
    const-string/jumbo v3, "\u03b1\u03b2\u03b1\u03c1"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b2\u03b5\u03bd"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b5\u03bd\u03b1\u03c1"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b1\u03b2\u03c1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b1\u03b4"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b1\u03b8"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03b1\u03bd"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "\u03b1\u03c0\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 342
    const-string/jumbo v4, "\u03b2\u03b1\u03c1\u03bf\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03bd\u03c4\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "\u03c3\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "\u03ba\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "\u03bc\u03c0\u03bf\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "\u03bd\u03b9\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "\u03c0\u03b1\u03b3"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string/jumbo v4, "\u03c0\u03b1\u03c1\u03b1\u03ba\u03b1\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "\u03c3\u03b5\u03c1\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    .line 343
    const-string/jumbo v4, "\u03c3\u03ba\u03b5\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string/jumbo v4, "\u03c3\u03c5\u03c1\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "\u03c4\u03bf\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "\u03c5"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string/jumbo v4, "\u03b5\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string/jumbo v4, "\u03b8\u03b1\u03c1\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string/jumbo v4, "\u03b8"

    aput-object v4, v2, v3

    .line 341
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 340
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc9:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 428
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    .line 429
    const-string/jumbo v3, "\u03c0"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b1\u03c0"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03c3\u03c5\u03bc\u03c0"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b1\u03c3\u03c5\u03bc\u03c0"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b1\u03ba\u03b1\u03c4\u03b1\u03c0"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b1\u03bc\u03b5\u03c4\u03b1\u03bc\u03c6"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 428
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc12a:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 432
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    .line 433
    const-string/jumbo v3, "\u03b1\u03bb"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b1\u03c1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b5\u03ba\u03c4\u03b5\u03bb"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b6"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03bc"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03be"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03c0\u03b1\u03c1\u03b1\u03ba\u03b1\u03bb"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "\u03b1\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "\u03c0\u03c1\u03bf"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03bd\u03b9\u03c3"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 432
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc12b:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 452
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    .line 453
    const-string/jumbo v3, "\u03b4\u03b9\u03b1\u03b8"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03b8"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03c0\u03b1\u03c1\u03b1\u03ba\u03b1\u03c4\u03b1\u03b8"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03c0\u03c1\u03bf\u03c3\u03b8"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03c3\u03c5\u03bd\u03b8"

    aput-object v3, v2, v9

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 452
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc13:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 486
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x19

    new-array v2, v2, [Ljava/lang/String;

    .line 487
    const-string/jumbo v3, "\u03c6\u03b1\u03c1\u03bc\u03b1\u03ba"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c7\u03b1\u03b4"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b1\u03b3\u03ba"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b1\u03bd\u03b1\u03c1\u03c1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b2\u03c1\u03bf\u03bc"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b5\u03ba\u03bb\u03b9\u03c0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03bb\u03b1\u03bc\u03c0\u03b9\u03b4"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 488
    const-string/jumbo v4, "\u03bb\u03b5\u03c7"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03c0\u03b1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "\u03bc\u03b5\u03b4"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "\u03bc\u03b5\u03c3\u03b1\u03b6"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "\u03c5\u03c0\u03bf\u03c4\u03b5\u03b9\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string/jumbo v4, "\u03b1\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "\u03b1\u03b9\u03b8"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    .line 489
    const-string/jumbo v4, "\u03b1\u03bd\u03b7\u03ba"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string/jumbo v4, "\u03b4\u03b5\u03c3\u03c0\u03bf\u03b6"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "\u03b5\u03bd\u03b4\u03b9\u03b1\u03c6\u03b5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "\u03b4\u03b5"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "\u03b4\u03b5\u03c5\u03c4\u03b5\u03c1\u03b5\u03c5"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string/jumbo v4, "\u03ba\u03b1\u03b8\u03b1\u03c1\u03b5\u03c5"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string/jumbo v4, "\u03c0\u03bb\u03b5"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    .line 490
    const-string/jumbo v4, "\u03c4\u03c3\u03b1"

    aput-object v4, v2, v3

    .line 487
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 486
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc14:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 524
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x2c

    new-array v2, v2, [Ljava/lang/String;

    .line 525
    const-string/jumbo v3, "\u03b1\u03b2\u03b1\u03c3\u03c4"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c0\u03bf\u03bb\u03c5\u03c6"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b1\u03b4\u03b7\u03c6"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03c0\u03b1\u03bc\u03c6"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03c1"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b1\u03c3\u03c0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03b1\u03c6"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "\u03b1\u03bc\u03b1\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    .line 526
    const-string/jumbo v4, "\u03b1\u03bc\u03b1\u03bb\u03bb\u03b9"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03b1\u03bd\u03c5\u03c3\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "\u03b1\u03c0\u03b5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "\u03b1\u03c3\u03c0\u03b1\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "\u03b1\u03c7\u03b1\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "\u03b4\u03b5\u03c1\u03b2\u03b5\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string/jumbo v4, "\u03b4\u03c1\u03bf\u03c3\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    .line 527
    const-string/jumbo v4, "\u03be\u03b5\u03c6"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string/jumbo v4, "\u03bd\u03b5\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string/jumbo v4, "\u03bd\u03bf\u03bc\u03bf\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string/jumbo v4, "\u03bf\u03bb\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string/jumbo v4, "\u03bf\u03bc\u03bf\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string/jumbo v4, "\u03c0\u03c1\u03bf\u03c3\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    const-string/jumbo v4, "\u03c0\u03c1\u03bf\u03c3\u03c9\u03c0\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string/jumbo v4, "\u03c3\u03c5\u03bc\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    .line 528
    const-string/jumbo v4, "\u03c3\u03c5\u03bd\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string/jumbo v4, "\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string/jumbo v4, "\u03c5\u03c0\u03bf\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string/jumbo v4, "\u03c7\u03b1\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    const-string/jumbo v4, "\u03b1\u03b5\u03b9\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string/jumbo v4, "\u03b1\u03b9\u03bc\u03bf\u03c3\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string/jumbo v4, "\u03b1\u03bd\u03c5\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    const-string/jumbo v4, "\u03b1\u03c0\u03bf\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    .line 529
    const-string/jumbo v4, "\u03b1\u03c1\u03c4\u03b9\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string/jumbo v4, "\u03b4\u03b9\u03b1\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string/jumbo v4, "\u03b5\u03bd"

    aput-object v4, v2, v3

    const/16 v3, 0x22

    const-string/jumbo v4, "\u03b5\u03c0\u03b9\u03c4"

    aput-object v4, v2, v3

    const/16 v3, 0x23

    const-string/jumbo v4, "\u03ba\u03c1\u03bf\u03ba\u03b1\u03bb\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x24

    const-string/jumbo v4, "\u03c3\u03b9\u03b4\u03b7\u03c1\u03bf\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x25

    const-string/jumbo v4, "\u03bb"

    aput-object v4, v2, v3

    const/16 v3, 0x26

    const-string/jumbo v4, "\u03bd\u03b1\u03c5"

    aput-object v4, v2, v3

    const/16 v3, 0x27

    .line 530
    const-string/jumbo v4, "\u03bf\u03c5\u03bb\u03b1\u03bc"

    aput-object v4, v2, v3

    const/16 v3, 0x28

    const-string/jumbo v4, "\u03bf\u03c5\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x29

    const-string/jumbo v4, "\u03c0"

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    const-string/jumbo v4, "\u03c4\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    const-string/jumbo v4, "\u03bc"

    aput-object v4, v2, v3

    .line 525
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 524
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc15a:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 533
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    new-array v2, v7, [Ljava/lang/String;

    .line 534
    const-string/jumbo v3, "\u03c8\u03bf\u03c6"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03bd\u03b1\u03c5\u03bb\u03bf\u03c7"

    aput-object v3, v2, v6

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 533
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc15b:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 570
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    .line 571
    const-string/jumbo v3, "\u03bd"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c7\u03b5\u03c1\u03c3\u03bf\u03bd"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b4\u03c9\u03b4\u03b5\u03ba\u03b1\u03bd"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03b5\u03c1\u03b7\u03bc\u03bf\u03bd"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03bc\u03b5\u03b3\u03b1\u03bb\u03bf\u03bd"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b5\u03c0\u03c4\u03b1\u03bd"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 570
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc16:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 590
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    .line 591
    const-string/jumbo v3, "\u03b1\u03c3\u03b2"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c3\u03b2"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03b1\u03c7\u03c1"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03c7\u03c1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b1\u03c0\u03bb"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b1\u03b5\u03b9\u03bc\u03bd"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03b4\u03c5\u03c3\u03c7\u03c1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "\u03b5\u03c5\u03c7\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "\u03ba\u03bf\u03b9\u03bd\u03bf\u03c7\u03c1"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "\u03c0\u03b1\u03bb\u03b9\u03bc\u03c8"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 590
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc17:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 604
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/String;

    .line 605
    const-string/jumbo v3, "\u03bd"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03c3\u03c0\u03b9"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03c3\u03c4\u03c1\u03b1\u03b2\u03bf\u03bc\u03bf\u03c5\u03c4\u03c3"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03ba\u03b1\u03ba\u03bf\u03bc\u03bf\u03c5\u03c4\u03c3"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b5\u03be\u03c9\u03bd"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 604
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc18:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 628
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    .line 629
    const-string/jumbo v3, "\u03c0\u03b1\u03c1\u03b1\u03c3\u03bf\u03c5\u03c3"

    aput-object v3, v2, v5

    const-string/jumbo v3, "\u03c6"

    aput-object v3, v2, v6

    const-string/jumbo v3, "\u03c7"

    aput-object v3, v2, v7

    const-string/jumbo v3, "\u03c9\u03c1\u03b9\u03bf\u03c0\u03bb"

    aput-object v3, v2, v8

    const-string/jumbo v3, "\u03b1\u03b6"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "\u03b1\u03bb\u03bb\u03bf\u03c3\u03bf\u03c5\u03c3"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "\u03b1\u03c3\u03bf\u03c5\u03c3"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 628
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    sput-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc19:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 630
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private endsWith([CILjava/lang/String;)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I
    .param p3, "suffix"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 777
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    .line 778
    .local v1, "suffixLen":I
    if-le v1, p2, :cond_1

    .line 784
    :cond_0
    :goto_0
    return v2

    .line 780
    :cond_1
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_2

    .line 784
    const/4 v2, 0x1

    goto :goto_0

    .line 781
    :cond_2
    sub-int v3, v1, v0

    sub-int v3, p2, v3

    aget-char v3, p1, v3

    invoke-virtual {p3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_0

    .line 780
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private endsWithVowel([CI)Z
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v0, 0x0

    .line 788
    if-nez p2, :cond_0

    .line 800
    :goto_0
    return v0

    .line 790
    :cond_0
    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 798
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 790
    :sswitch_data_0
    .sparse-switch
        0x3b1 -> :sswitch_0
        0x3b5 -> :sswitch_0
        0x3b7 -> :sswitch_0
        0x3b9 -> :sswitch_0
        0x3bf -> :sswitch_0
        0x3c5 -> :sswitch_0
        0x3c9 -> :sswitch_0
    .end sparse-switch
.end method

.method private endsWithVowelNoY([CI)Z
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v0, 0x0

    .line 805
    if-nez p2, :cond_0

    .line 816
    :goto_0
    return v0

    .line 807
    :cond_0
    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 814
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 807
    :sswitch_data_0
    .sparse-switch
        0x3b1 -> :sswitch_0
        0x3b5 -> :sswitch_0
        0x3b7 -> :sswitch_0
        0x3b9 -> :sswitch_0
        0x3bf -> :sswitch_0
        0x3c9 -> :sswitch_0
    .end sparse-switch
.end method

.method private rule0([CI)I
    .locals 6
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x7

    const/4 v1, 0x6

    .line 70
    const/16 v0, 0x9

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03ba\u03b1\u03b8\u03b5\u03c3\u03c4\u03c9\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    const-string/jumbo v0, "\u03ba\u03b1\u03b8\u03b5\u03c3\u03c4\u03c9\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    :cond_0
    add-int/lit8 p2, p2, -0x4

    .line 139
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 74
    .restart local p2    # "len":I
    :cond_2
    if-le p2, v5, :cond_4

    const-string/jumbo v0, "\u03b3\u03b5\u03b3\u03bf\u03bd\u03bf\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 75
    const-string/jumbo v0, "\u03b3\u03b5\u03b3\u03bf\u03bd\u03bf\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 76
    :cond_3
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 78
    :cond_4
    if-le p2, v5, :cond_5

    const-string/jumbo v0, "\u03ba\u03b1\u03b8\u03b5\u03c3\u03c4\u03c9\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 79
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 81
    :cond_5
    if-le p2, v2, :cond_7

    const-string/jumbo v0, "\u03c4\u03b1\u03c4\u03bf\u03b3\u03b9\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 82
    const-string/jumbo v0, "\u03c4\u03b1\u03c4\u03bf\u03b3\u03b9\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 83
    :cond_6
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 85
    :cond_7
    if-le p2, v2, :cond_8

    const-string/jumbo v0, "\u03b3\u03b5\u03b3\u03bf\u03bd\u03bf\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 86
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 88
    :cond_8
    if-le p2, v2, :cond_9

    const-string/jumbo v0, "\u03ba\u03b1\u03b8\u03b5\u03c3\u03c4\u03c9\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 89
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 91
    :cond_9
    if-le p2, v1, :cond_a

    const-string/jumbo v0, "\u03c3\u03ba\u03b1\u03b3\u03b9\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 92
    :cond_a
    const-string/jumbo v0, "\u03c3\u03ba\u03b1\u03b3\u03b9\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 93
    const-string/jumbo v0, "\u03bf\u03bb\u03bf\u03b3\u03b9\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 94
    const-string/jumbo v0, "\u03bf\u03bb\u03bf\u03b3\u03b9\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 95
    const-string/jumbo v0, "\u03ba\u03c1\u03b5\u03b1\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 96
    const-string/jumbo v0, "\u03ba\u03c1\u03b5\u03b1\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 97
    const-string/jumbo v0, "\u03c0\u03b5\u03c1\u03b1\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 98
    const-string/jumbo v0, "\u03c0\u03b5\u03c1\u03b1\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 99
    const-string/jumbo v0, "\u03c4\u03b5\u03c1\u03b1\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 100
    const-string/jumbo v0, "\u03c4\u03b5\u03c1\u03b1\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 101
    :cond_b
    add-int/lit8 p2, p2, -0x4

    goto/16 :goto_0

    .line 103
    :cond_c
    if-le p2, v1, :cond_d

    const-string/jumbo v0, "\u03c4\u03b1\u03c4\u03bf\u03b3\u03b9\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 104
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 106
    :cond_d
    if-le p2, v1, :cond_e

    const-string/jumbo v0, "\u03b3\u03b5\u03b3\u03bf\u03bd\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 107
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 109
    :cond_e
    if-le p2, v4, :cond_10

    const-string/jumbo v0, "\u03c6\u03b1\u03b3\u03b9\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 110
    const-string/jumbo v0, "\u03c6\u03b1\u03b3\u03b9\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 111
    const-string/jumbo v0, "\u03c3\u03bf\u03b3\u03b9\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 112
    const-string/jumbo v0, "\u03c3\u03bf\u03b3\u03b9\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 113
    :cond_f
    add-int/lit8 p2, p2, -0x4

    goto/16 :goto_0

    .line 115
    :cond_10
    if-le p2, v4, :cond_12

    const-string/jumbo v0, "\u03c3\u03ba\u03b1\u03b3\u03b9\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 116
    const-string/jumbo v0, "\u03bf\u03bb\u03bf\u03b3\u03b9\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 117
    const-string/jumbo v0, "\u03ba\u03c1\u03b5\u03b1\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 118
    const-string/jumbo v0, "\u03c0\u03b5\u03c1\u03b1\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 119
    const-string/jumbo v0, "\u03c4\u03b5\u03c1\u03b1\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 120
    :cond_11
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 122
    :cond_12
    if-le p2, v3, :cond_14

    const-string/jumbo v0, "\u03c6\u03b1\u03b3\u03b9\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 123
    const-string/jumbo v0, "\u03c3\u03bf\u03b3\u03b9\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 124
    const-string/jumbo v0, "\u03c6\u03c9\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 125
    const-string/jumbo v0, "\u03c6\u03c9\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 126
    :cond_13
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 128
    :cond_14
    if-le p2, v3, :cond_16

    const-string/jumbo v0, "\u03ba\u03c1\u03b5\u03b1\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 129
    const-string/jumbo v0, "\u03c0\u03b5\u03c1\u03b1\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 130
    const-string/jumbo v0, "\u03c4\u03b5\u03c1\u03b1\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 131
    :cond_15
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 133
    :cond_16
    const/4 v0, 0x3

    if-le p2, v0, :cond_17

    const-string/jumbo v0, "\u03c6\u03c9\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 134
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 136
    :cond_17
    const/4 v0, 0x2

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03c6\u03c9\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0
.end method

.method private rule1([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 143
    const/4 v0, 0x4

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03b1\u03b4\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u03b1\u03b4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :cond_0
    add-int/lit8 p2, p2, -0x4

    .line 145
    const-string/jumbo v0, "\u03bf\u03ba"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 146
    const-string/jumbo v0, "\u03bc\u03b1\u03bc"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 147
    const-string/jumbo v0, "\u03bc\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    const-string/jumbo v0, "\u03bc\u03c0\u03b1\u03bc\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    const-string/jumbo v0, "\u03c0\u03b1\u03c4\u03b5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    const-string/jumbo v0, "\u03b3\u03b9\u03b1\u03b3\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    const-string/jumbo v0, "\u03bd\u03c4\u03b1\u03bd\u03c4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    const-string/jumbo v0, "\u03ba\u03c5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    const-string/jumbo v0, "\u03b8\u03b5\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    const-string/jumbo v0, "\u03c0\u03b5\u03b8\u03b5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    add-int/lit8 p2, p2, 0x2

    .line 157
    :cond_1
    return p2
.end method

.method private rule10([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 393
    const/4 v0, 0x5

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03bf\u03bd\u03c4\u03b1\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u03c9\u03bd\u03c4\u03b1\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 394
    :cond_0
    add-int/lit8 p2, p2, -0x5

    .line 395
    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    const-string/jumbo v0, "\u03b1\u03c1\u03c7"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    add-int/lit8 p2, p2, 0x3

    .line 397
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x3bf

    aput-char v1, p1, v0

    .line 399
    :cond_1
    const-string/jumbo v0, "\u03ba\u03c1\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 400
    add-int/lit8 p2, p2, 0x3

    .line 401
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x3c9

    aput-char v1, p1, v0

    .line 405
    :cond_2
    return p2
.end method

.method private rule11([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v1, 0x2

    .line 409
    const/4 v0, 0x6

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03bf\u03bc\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    add-int/lit8 p2, p2, -0x6

    .line 411
    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "\u03bf\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    add-int/lit8 p2, p2, 0x5

    .line 425
    :cond_0
    :goto_0
    return p2

    .line 414
    :cond_1
    const/4 v0, 0x7

    if-le p2, v0, :cond_0

    const-string/jumbo v0, "\u03b9\u03bf\u03bc\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    add-int/lit8 p2, p2, -0x7

    .line 416
    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "\u03bf\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    add-int/lit8 p2, p2, 0x5

    .line 418
    add-int/lit8 v0, p2, -0x5

    const/16 v1, 0x3bf

    aput-char v1, p1, v0

    .line 419
    add-int/lit8 v0, p2, -0x4

    const/16 v1, 0x3bc

    aput-char v1, p1, v0

    .line 420
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x3b1

    aput-char v1, p1, v0

    .line 421
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x3c3

    aput-char v1, p1, v0

    .line 422
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x3c4

    aput-char v1, p1, v0

    goto :goto_0
.end method

.method private rule12([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v1, 0x0

    .line 437
    const/4 v0, 0x5

    if-le p2, v0, :cond_0

    const-string/jumbo v0, "\u03b9\u03b5\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    add-int/lit8 p2, p2, -0x5

    .line 439
    sget-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc12a:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v0, p1, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    add-int/lit8 p2, p2, 0x4

    .line 443
    :cond_0
    const/4 v0, 0x4

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03b5\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 444
    add-int/lit8 p2, p2, -0x4

    .line 445
    sget-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc12b:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v0, p1, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 446
    add-int/lit8 p2, p2, 0x3

    .line 449
    :cond_1
    return p2
.end method

.method private rule13([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 457
    const/4 v1, 0x6

    if-le p2, v1, :cond_4

    const-string/jumbo v1, "\u03b7\u03b8\u03b7\u03ba\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 458
    add-int/lit8 p2, p2, -0x6

    .line 463
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 465
    .local v0, "removed":Z
    const/4 v1, 0x4

    if-le p2, v1, :cond_6

    const-string/jumbo v1, "\u03b7\u03ba\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 466
    add-int/lit8 p2, p2, -0x4

    .line 467
    const/4 v0, 0x1

    .line 473
    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc13:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-nez v1, :cond_2

    .line 474
    const-string/jumbo v1, "\u03c3\u03ba\u03c9\u03bb"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 475
    const-string/jumbo v1, "\u03c3\u03ba\u03bf\u03c5\u03bb"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 476
    const-string/jumbo v1, "\u03bd\u03b1\u03c1\u03b8"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 477
    const-string/jumbo v1, "\u03c3\u03c6"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 478
    const-string/jumbo v1, "\u03bf\u03b8"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 479
    const-string/jumbo v1, "\u03c0\u03b9\u03b8"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 480
    :cond_2
    add-int/lit8 p2, p2, 0x2

    .line 483
    :cond_3
    return p2

    .line 459
    .end local v0    # "removed":Z
    :cond_4
    const/4 v1, 0x5

    if-le p2, v1, :cond_0

    const-string/jumbo v1, "\u03b7\u03b8\u03b7\u03ba\u03b1"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "\u03b7\u03b8\u03b7\u03ba\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 460
    :cond_5
    add-int/lit8 p2, p2, -0x5

    goto :goto_0

    .line 468
    .restart local v0    # "removed":Z
    :cond_6
    const/4 v1, 0x3

    if-le p2, v1, :cond_1

    const-string/jumbo v1, "\u03b7\u03ba\u03b1"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "\u03b7\u03ba\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 469
    :cond_7
    add-int/lit8 p2, p2, -0x3

    .line 470
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private rule14([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 494
    const/4 v0, 0x0

    .line 496
    .local v0, "removed":Z
    const/4 v1, 0x5

    if-le p2, v1, :cond_3

    const-string/jumbo v1, "\u03bf\u03c5\u03c3\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 497
    add-int/lit8 p2, p2, -0x5

    .line 498
    const/4 v0, 0x1

    .line 504
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc14:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-nez v1, :cond_1

    .line 505
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowel([CI)Z

    move-result v1

    if-nez v1, :cond_1

    .line 506
    const-string/jumbo v1, "\u03c0\u03bf\u03b4\u03b1\u03c1"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 507
    const-string/jumbo v1, "\u03b2\u03bb\u03b5\u03c0"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 508
    const-string/jumbo v1, "\u03c0\u03b1\u03bd\u03c4\u03b1\u03c7"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 509
    const-string/jumbo v1, "\u03c6\u03c1\u03c5\u03b4"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 510
    const-string/jumbo v1, "\u03bc\u03b1\u03bd\u03c4\u03b9\u03bb"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 511
    const-string/jumbo v1, "\u03bc\u03b1\u03bb\u03bb"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 512
    const-string/jumbo v1, "\u03ba\u03c5\u03bc\u03b1\u03c4"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 513
    const-string/jumbo v1, "\u03bb\u03b1\u03c7"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 514
    const-string/jumbo v1, "\u03bb\u03b7\u03b3"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 515
    const-string/jumbo v1, "\u03c6\u03b1\u03b3"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 516
    const-string/jumbo v1, "\u03bf\u03bc"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 517
    const-string/jumbo v1, "\u03c0\u03c1\u03c9\u03c4"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 518
    :cond_1
    add-int/lit8 p2, p2, 0x3

    .line 521
    :cond_2
    return p2

    .line 499
    :cond_3
    const/4 v1, 0x4

    if-le p2, v1, :cond_0

    const-string/jumbo v1, "\u03bf\u03c5\u03c3\u03b1"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "\u03bf\u03c5\u03c3\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    :cond_4
    add-int/lit8 p2, p2, -0x4

    .line 501
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private rule15([CI)I
    .locals 6
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 538
    const/4 v2, 0x0

    .line 539
    .local v2, "removed":Z
    const/4 v5, 0x4

    if-le p2, v5, :cond_2

    const-string/jumbo v5, "\u03b1\u03b3\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 540
    add-int/lit8 p2, p2, -0x4

    .line 541
    const/4 v2, 0x1

    .line 547
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 548
    sget-object v5, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc15a:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v5, p1, v3, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v5

    if-nez v5, :cond_4

    .line 549
    const-string/jumbo v5, "\u03bf\u03c6"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 550
    const-string/jumbo v5, "\u03c0\u03b5\u03bb"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 551
    const-string/jumbo v5, "\u03c7\u03bf\u03c1\u03c4"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 552
    const-string/jumbo v5, "\u03bb\u03bb"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 553
    const-string/jumbo v5, "\u03c3\u03c6"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 554
    const-string/jumbo v5, "\u03c1\u03c0"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 555
    const-string/jumbo v5, "\u03c6\u03c1"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 556
    const-string/jumbo v5, "\u03c0\u03c1"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 557
    const-string/jumbo v5, "\u03bb\u03bf\u03c7"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 558
    const-string/jumbo v5, "\u03c3\u03bc\u03b7\u03bd"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    move v0, v3

    .line 560
    .local v0, "cond1":Z
    :goto_1
    sget-object v5, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc15b:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v5, p1, v3, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v5

    if-nez v5, :cond_5

    .line 561
    const-string/jumbo v5, "\u03ba\u03bf\u03bb\u03bb"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    move v1, v3

    .line 563
    .local v1, "cond2":Z
    :goto_2
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 564
    add-int/lit8 p2, p2, 0x2

    .line 567
    .end local v0    # "cond1":Z
    .end local v1    # "cond2":Z
    :cond_1
    return p2

    .line 542
    :cond_2
    const/4 v5, 0x3

    if-le p2, v5, :cond_0

    const-string/jumbo v5, "\u03b1\u03b3\u03b1"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string/jumbo v5, "\u03b1\u03b3\u03b5"

    invoke-direct {p0, p1, p2, v5}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 543
    :cond_3
    add-int/lit8 p2, p2, -0x3

    .line 544
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_4
    move v0, v4

    .line 548
    goto :goto_1

    .restart local v0    # "cond1":Z
    :cond_5
    move v1, v4

    .line 560
    goto :goto_2
.end method

.method private rule16([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 575
    const/4 v0, 0x0

    .line 576
    .local v0, "removed":Z
    const/4 v1, 0x4

    if-le p2, v1, :cond_2

    const-string/jumbo v1, "\u03b7\u03c3\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 577
    add-int/lit8 p2, p2, -0x4

    .line 578
    const/4 v0, 0x1

    .line 584
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc16:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 585
    add-int/lit8 p2, p2, 0x2

    .line 587
    :cond_1
    return p2

    .line 579
    :cond_2
    const/4 v1, 0x3

    if-le p2, v1, :cond_0

    const-string/jumbo v1, "\u03b7\u03c3\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "\u03b7\u03c3\u03b1"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 580
    :cond_3
    add-int/lit8 p2, p2, -0x3

    .line 581
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rule17([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 595
    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    const-string/jumbo v0, "\u03b7\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    add-int/lit8 p2, p2, -0x4

    .line 597
    sget-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc17:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    add-int/lit8 p2, p2, 0x3

    .line 601
    :cond_0
    return p2
.end method

.method private rule18([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 609
    const/4 v0, 0x0

    .line 611
    .local v0, "removed":Z
    const/4 v1, 0x6

    if-le p2, v1, :cond_3

    const-string/jumbo v1, "\u03b7\u03c3\u03bf\u03c5\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "\u03b7\u03b8\u03bf\u03c5\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 612
    :cond_0
    add-int/lit8 p2, p2, -0x6

    .line 613
    const/4 v0, 0x1

    .line 619
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc18:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 620
    add-int/lit8 p2, p2, 0x3

    .line 621
    add-int/lit8 v1, p2, -0x3

    const/16 v2, 0x3bf

    aput-char v2, p1, v1

    .line 622
    add-int/lit8 v1, p2, -0x2

    const/16 v2, 0x3c5

    aput-char v2, p1, v1

    .line 623
    add-int/lit8 v1, p2, -0x1

    const/16 v2, 0x3bd

    aput-char v2, p1, v1

    .line 625
    :cond_2
    return p2

    .line 614
    :cond_3
    const/4 v1, 0x4

    if-le p2, v1, :cond_1

    const-string/jumbo v1, "\u03bf\u03c5\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 615
    add-int/lit8 p2, p2, -0x4

    .line 616
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rule19([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 633
    const/4 v0, 0x0

    .line 635
    .local v0, "removed":Z
    const/4 v1, 0x6

    if-le p2, v1, :cond_3

    const-string/jumbo v1, "\u03b7\u03c3\u03bf\u03c5\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "\u03b7\u03b8\u03bf\u03c5\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 636
    :cond_0
    add-int/lit8 p2, p2, -0x6

    .line 637
    const/4 v0, 0x1

    .line 643
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc19:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 644
    add-int/lit8 p2, p2, 0x3

    .line 645
    add-int/lit8 v1, p2, -0x3

    const/16 v2, 0x3bf

    aput-char v2, p1, v1

    .line 646
    add-int/lit8 v1, p2, -0x2

    const/16 v2, 0x3c5

    aput-char v2, p1, v1

    .line 647
    add-int/lit8 v1, p2, -0x1

    const/16 v2, 0x3bc

    aput-char v2, p1, v1

    .line 649
    :cond_2
    return p2

    .line 638
    :cond_3
    const/4 v1, 0x4

    if-le p2, v1, :cond_1

    const-string/jumbo v1, "\u03bf\u03c5\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 639
    add-int/lit8 p2, p2, -0x4

    .line 640
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rule2([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 161
    const/4 v0, 0x4

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03b5\u03b4\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u03b5\u03b4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    :cond_0
    add-int/lit8 p2, p2, -0x4

    .line 163
    const-string/jumbo v0, "\u03bf\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 164
    const-string/jumbo v0, "\u03b9\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    const-string/jumbo v0, "\u03b5\u03bc\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    const-string/jumbo v0, "\u03c5\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    const-string/jumbo v0, "\u03b3\u03b7\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    const-string/jumbo v0, "\u03b4\u03b1\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    const-string/jumbo v0, "\u03ba\u03c1\u03b1\u03c3\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 170
    const-string/jumbo v0, "\u03bc\u03b9\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    :cond_1
    add-int/lit8 p2, p2, 0x2

    .line 173
    :cond_2
    return p2
.end method

.method private rule20([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 653
    const/4 v0, 0x5

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03bc\u03b1\u03c4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u03bc\u03b1\u03c4\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 654
    :cond_0
    add-int/lit8 p2, p2, -0x3

    .line 657
    :cond_1
    :goto_0
    return p2

    .line 655
    :cond_2
    const/4 v0, 0x4

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03bc\u03b1\u03c4\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656
    add-int/lit8 p2, p2, -0x2

    goto :goto_0
.end method

.method private rule21([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 661
    const/16 v0, 0x9

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03b9\u03bf\u03bd\u03c4\u03bf\u03c5\u03c3\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    add-int/lit8 p2, p2, -0x9

    .line 757
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 664
    .restart local p2    # "len":I
    :cond_1
    const/16 v0, 0x8

    if-le p2, v0, :cond_3

    const-string/jumbo v0, "\u03b9\u03bf\u03bc\u03b1\u03c3\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 665
    const-string/jumbo v0, "\u03b9\u03bf\u03c3\u03b1\u03c3\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 666
    const-string/jumbo v0, "\u03b9\u03bf\u03c5\u03bc\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 667
    const-string/jumbo v0, "\u03bf\u03bd\u03c4\u03bf\u03c5\u03c3\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 668
    :cond_2
    add-int/lit8 p2, p2, -0x8

    goto :goto_0

    .line 670
    :cond_3
    const/4 v0, 0x7

    if-le p2, v0, :cond_5

    const-string/jumbo v0, "\u03b9\u03b5\u03bc\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 671
    const-string/jumbo v0, "\u03b9\u03b5\u03c3\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 672
    const-string/jumbo v0, "\u03b9\u03bf\u03bc\u03bf\u03c5\u03bd\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 673
    const-string/jumbo v0, "\u03b9\u03bf\u03c3\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 674
    const-string/jumbo v0, "\u03b9\u03bf\u03c3\u03bf\u03c5\u03bd\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 675
    const-string/jumbo v0, "\u03b9\u03bf\u03c5\u03bd\u03c4\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 676
    const-string/jumbo v0, "\u03b9\u03bf\u03c5\u03bd\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 677
    const-string/jumbo v0, "\u03b7\u03b8\u03b7\u03ba\u03b1\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 678
    const-string/jumbo v0, "\u03bf\u03bc\u03b1\u03c3\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 679
    const-string/jumbo v0, "\u03bf\u03c3\u03b1\u03c3\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 680
    const-string/jumbo v0, "\u03bf\u03c5\u03bc\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 681
    :cond_4
    add-int/lit8 p2, p2, -0x7

    goto/16 :goto_0

    .line 683
    :cond_5
    const/4 v0, 0x6

    if-le p2, v0, :cond_7

    const-string/jumbo v0, "\u03b9\u03bf\u03bc\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 684
    const-string/jumbo v0, "\u03b9\u03bf\u03bd\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 685
    const-string/jumbo v0, "\u03b9\u03bf\u03c3\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 686
    const-string/jumbo v0, "\u03b7\u03b8\u03b5\u03b9\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 687
    const-string/jumbo v0, "\u03b7\u03b8\u03b7\u03ba\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 688
    const-string/jumbo v0, "\u03bf\u03bc\u03bf\u03c5\u03bd\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 689
    const-string/jumbo v0, "\u03bf\u03c3\u03b1\u03c3\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 690
    const-string/jumbo v0, "\u03bf\u03c3\u03bf\u03c5\u03bd\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 691
    const-string/jumbo v0, "\u03bf\u03c5\u03bd\u03c4\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 692
    const-string/jumbo v0, "\u03bf\u03c5\u03bd\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 693
    const-string/jumbo v0, "\u03bf\u03c5\u03c3\u03b1\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 694
    :cond_6
    add-int/lit8 p2, p2, -0x6

    goto/16 :goto_0

    .line 696
    :cond_7
    const/4 v0, 0x5

    if-le p2, v0, :cond_9

    const-string/jumbo v0, "\u03b1\u03b3\u03b1\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 697
    const-string/jumbo v0, "\u03b9\u03b5\u03bc\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 698
    const-string/jumbo v0, "\u03b9\u03b5\u03c4\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 699
    const-string/jumbo v0, "\u03b9\u03b5\u03c3\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 700
    const-string/jumbo v0, "\u03b9\u03bf\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 701
    const-string/jumbo v0, "\u03b9\u03bf\u03c5\u03bc\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 702
    const-string/jumbo v0, "\u03b7\u03b8\u03b5\u03b9\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 703
    const-string/jumbo v0, "\u03b7\u03b8\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 704
    const-string/jumbo v0, "\u03b7\u03ba\u03b1\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 705
    const-string/jumbo v0, "\u03b7\u03c3\u03b1\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 706
    const-string/jumbo v0, "\u03b7\u03c3\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 707
    const-string/jumbo v0, "\u03bf\u03bc\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 708
    const-string/jumbo v0, "\u03bf\u03bd\u03c4\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 709
    const-string/jumbo v0, "\u03bf\u03bd\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 710
    const-string/jumbo v0, "\u03bf\u03c3\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 711
    const-string/jumbo v0, "\u03bf\u03c5\u03bc\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 712
    const-string/jumbo v0, "\u03bf\u03c5\u03c3\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 713
    :cond_8
    add-int/lit8 p2, p2, -0x5

    goto/16 :goto_0

    .line 715
    :cond_9
    const/4 v0, 0x4

    if-le p2, v0, :cond_b

    const-string/jumbo v0, "\u03b1\u03b3\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 716
    const-string/jumbo v0, "\u03b1\u03bc\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 717
    const-string/jumbo v0, "\u03b1\u03c3\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 718
    const-string/jumbo v0, "\u03b1\u03c4\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 719
    const-string/jumbo v0, "\u03b5\u03b9\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 720
    const-string/jumbo v0, "\u03b5\u03c3\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 721
    const-string/jumbo v0, "\u03b5\u03c4\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 722
    const-string/jumbo v0, "\u03b7\u03b4\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 723
    const-string/jumbo v0, "\u03b7\u03b4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 724
    const-string/jumbo v0, "\u03b7\u03b8\u03b5\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 725
    const-string/jumbo v0, "\u03b7\u03ba\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 726
    const-string/jumbo v0, "\u03b7\u03c3\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 727
    const-string/jumbo v0, "\u03b7\u03c3\u03b5\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 728
    const-string/jumbo v0, "\u03b7\u03c3\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 729
    const-string/jumbo v0, "\u03bf\u03bc\u03b1\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 730
    const-string/jumbo v0, "\u03bf\u03c4\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 731
    :cond_a
    add-int/lit8 p2, p2, -0x4

    goto/16 :goto_0

    .line 733
    :cond_b
    const/4 v0, 0x3

    if-le p2, v0, :cond_d

    const-string/jumbo v0, "\u03b1\u03b5\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 734
    const-string/jumbo v0, "\u03b5\u03b9\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 735
    const-string/jumbo v0, "\u03b7\u03b8\u03c9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 736
    const-string/jumbo v0, "\u03b7\u03c3\u03c9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 737
    const-string/jumbo v0, "\u03bf\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 738
    const-string/jumbo v0, "\u03bf\u03c5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 739
    :cond_c
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 741
    :cond_d
    const/4 v0, 0x2

    if-le p2, v0, :cond_f

    const-string/jumbo v0, "\u03b1\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 742
    const-string/jumbo v0, "\u03b1\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 743
    const-string/jumbo v0, "\u03b1\u03c9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 744
    const-string/jumbo v0, "\u03b5\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 745
    const-string/jumbo v0, "\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 746
    const-string/jumbo v0, "\u03b7\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 747
    const-string/jumbo v0, "\u03bf\u03b9"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 748
    const-string/jumbo v0, "\u03bf\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 749
    const-string/jumbo v0, "\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 750
    const-string/jumbo v0, "\u03c5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 751
    const-string/jumbo v0, "\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 752
    :cond_e
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 754
    :cond_f
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowel([CI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 755
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0
.end method

.method private rule22([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 761
    const-string/jumbo v0, "\u03b5\u03c3\u03c4\u03b5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 762
    const-string/jumbo v0, "\u03b5\u03c3\u03c4\u03b1\u03c4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 763
    :cond_0
    add-int/lit8 p2, p2, -0x5

    .line 773
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 765
    .restart local p2    # "len":I
    :cond_2
    const-string/jumbo v0, "\u03bf\u03c4\u03b5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 766
    const-string/jumbo v0, "\u03bf\u03c4\u03b1\u03c4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 767
    const-string/jumbo v0, "\u03c5\u03c4\u03b5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 768
    const-string/jumbo v0, "\u03c5\u03c4\u03b1\u03c4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 769
    const-string/jumbo v0, "\u03c9\u03c4\u03b5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 770
    const-string/jumbo v0, "\u03c9\u03c4\u03b1\u03c4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 771
    :cond_3
    add-int/lit8 p2, p2, -0x4

    goto :goto_0
.end method

.method private rule3([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 177
    const/4 v0, 0x5

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03bf\u03c5\u03b4\u03b5\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u03bf\u03c5\u03b4\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    :cond_0
    add-int/lit8 p2, p2, -0x5

    .line 179
    const-string/jumbo v0, "\u03b1\u03c1\u03ba"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 180
    const-string/jumbo v0, "\u03ba\u03b1\u03bb\u03b9\u03b1\u03ba"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 181
    const-string/jumbo v0, "\u03c0\u03b5\u03c4\u03b1\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 182
    const-string/jumbo v0, "\u03bb\u03b9\u03c7"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    const-string/jumbo v0, "\u03c0\u03bb\u03b5\u03be"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 184
    const-string/jumbo v0, "\u03c3\u03ba"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    const-string/jumbo v0, "\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 186
    const-string/jumbo v0, "\u03c6\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    const-string/jumbo v0, "\u03c6\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 188
    const-string/jumbo v0, "\u03b2\u03b5\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 189
    const-string/jumbo v0, "\u03bb\u03bf\u03c5\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    const-string/jumbo v0, "\u03c7\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    const-string/jumbo v0, "\u03c3\u03c0"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    const-string/jumbo v0, "\u03c4\u03c1\u03b1\u03b3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    const-string/jumbo v0, "\u03c6\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    :cond_1
    add-int/lit8 p2, p2, 0x3

    .line 196
    :cond_2
    return p2
.end method

.method private rule4([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 204
    const/4 v0, 0x3

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03b5\u03c9\u03c3"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "\u03b5\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    :cond_0
    add-int/lit8 p2, p2, -0x3

    .line 206
    sget-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc4:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    add-int/lit8 p2, p2, 0x1

    .line 209
    :cond_1
    return p2
.end method

.method private rule5([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 213
    const/4 v0, 0x2

    if-le p2, v0, :cond_1

    const-string/jumbo v0, "\u03b9\u03b1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 214
    add-int/lit8 p2, p2, -0x2

    .line 215
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowel([CI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    add-int/lit8 p2, p2, 0x1

    .line 222
    :cond_0
    :goto_0
    return p2

    .line 217
    :cond_1
    const/4 v0, 0x3

    if-le p2, v0, :cond_0

    const-string/jumbo v0, "\u03b9\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "\u03b9\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    :cond_2
    add-int/lit8 p2, p2, -0x3

    .line 219
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowel([CI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method private rule6([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 234
    const/4 v0, 0x0

    .line 235
    .local v0, "removed":Z
    const/4 v1, 0x3

    if-le p2, v1, :cond_4

    const-string/jumbo v1, "\u03b9\u03ba\u03b1"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "\u03b9\u03ba\u03bf"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 236
    :cond_0
    add-int/lit8 p2, p2, -0x3

    .line 237
    const/4 v0, 0x1

    .line 243
    :cond_1
    :goto_0
    if-eqz v0, :cond_3

    .line 244
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowel([CI)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc6:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 245
    :cond_2
    add-int/lit8 p2, p2, 0x2

    .line 247
    :cond_3
    return p2

    .line 238
    :cond_4
    const/4 v1, 0x4

    if-le p2, v1, :cond_1

    const-string/jumbo v1, "\u03b9\u03ba\u03bf\u03c5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "\u03b9\u03ba\u03c9\u03bd"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    :cond_5
    add-int/lit8 p2, p2, -0x4

    .line 240
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rule7([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v1, 0x5

    .line 256
    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "\u03b1\u03b3\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    add-int/lit8 v0, p2, -0x1

    .line 274
    :goto_0
    return v0

    .line 259
    :cond_0
    const/4 v0, 0x7

    if-le p2, v0, :cond_3

    const-string/jumbo v0, "\u03b7\u03b8\u03b7\u03ba\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    add-int/lit8 p2, p2, -0x7

    .line 268
    :cond_1
    :goto_1
    const/4 v0, 0x3

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 269
    add-int/lit8 p2, p2, -0x3

    .line 270
    sget-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc7:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    add-int/lit8 p2, p2, 0x2

    :cond_2
    move v0, p2

    .line 274
    goto :goto_0

    .line 261
    :cond_3
    const/4 v0, 0x6

    if-le p2, v0, :cond_4

    const-string/jumbo v0, "\u03bf\u03c5\u03c3\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 262
    add-int/lit8 p2, p2, -0x6

    goto :goto_1

    .line 263
    :cond_4
    if-le p2, v1, :cond_1

    const-string/jumbo v0, "\u03b1\u03b3\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 264
    const-string/jumbo v0, "\u03b7\u03c3\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 265
    const-string/jumbo v0, "\u03b7\u03ba\u03b1\u03bc\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    :cond_5
    add-int/lit8 p2, p2, -0x5

    goto :goto_1
.end method

.method private rule8([CI)I
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v4, 0x3b1

    const/4 v3, 0x0

    .line 298
    const/4 v0, 0x0

    .line 300
    .local v0, "removed":Z
    const/16 v1, 0x8

    if-le p2, v1, :cond_4

    const-string/jumbo v1, "\u03b9\u03bf\u03c5\u03bd\u03c4\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 301
    add-int/lit8 p2, p2, -0x8

    .line 302
    const/4 v0, 0x1

    .line 321
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc8a:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v1, p1, v3, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 323
    add-int/lit8 p2, p2, 0x4

    .line 324
    add-int/lit8 v1, p2, -0x4

    aput-char v4, p1, v1

    .line 325
    add-int/lit8 v1, p2, -0x3

    const/16 v2, 0x3b3

    aput-char v2, p1, v1

    .line 326
    add-int/lit8 v1, p2, -0x2

    aput-char v4, p1, v1

    .line 327
    add-int/lit8 v1, p2, -0x1

    const/16 v2, 0x3bd

    aput-char v2, p1, v1

    .line 330
    :cond_1
    const/4 v1, 0x3

    if-le p2, v1, :cond_3

    const-string/jumbo v1, "\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 331
    add-int/lit8 p2, p2, -0x3

    .line 332
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowelNoY([CI)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc8b:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v1, p1, v3, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 333
    :cond_2
    add-int/lit8 p2, p2, 0x2

    .line 337
    :cond_3
    return p2

    .line 303
    :cond_4
    const/4 v1, 0x7

    if-le p2, v1, :cond_5

    const-string/jumbo v1, "\u03b9\u03bf\u03bd\u03c4\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 304
    :cond_5
    const-string/jumbo v1, "\u03bf\u03c5\u03bd\u03c4\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 305
    const-string/jumbo v1, "\u03b7\u03b8\u03b7\u03ba\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 306
    :cond_6
    add-int/lit8 p2, p2, -0x7

    .line 307
    const/4 v0, 0x1

    .line 308
    goto :goto_0

    :cond_7
    const/4 v1, 0x6

    if-le p2, v1, :cond_8

    const-string/jumbo v1, "\u03b9\u03bf\u03c4\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 309
    :cond_8
    const-string/jumbo v1, "\u03bf\u03bd\u03c4\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 310
    const-string/jumbo v1, "\u03bf\u03c5\u03c3\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 311
    :cond_9
    add-int/lit8 p2, p2, -0x6

    .line 312
    const/4 v0, 0x1

    .line 313
    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x5

    if-le p2, v1, :cond_b

    const-string/jumbo v1, "\u03b1\u03b3\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 314
    :cond_b
    const-string/jumbo v1, "\u03b7\u03c3\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 315
    const-string/jumbo v1, "\u03bf\u03c4\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 316
    const-string/jumbo v1, "\u03b7\u03ba\u03b1\u03bd\u03b5"

    invoke-direct {p0, p1, p2, v1}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 317
    :cond_c
    add-int/lit8 p2, p2, -0x5

    .line 318
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private rule9([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 347
    const/4 v0, 0x5

    if-le p2, v0, :cond_0

    const-string/jumbo v0, "\u03b7\u03c3\u03b5\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    add-int/lit8 p2, p2, -0x5

    .line 350
    :cond_0
    const/4 v0, 0x3

    if-le p2, v0, :cond_2

    const-string/jumbo v0, "\u03b5\u03c4\u03b5"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 351
    add-int/lit8 p2, p2, -0x3

    .line 352
    sget-object v0, Lorg/apache/lucene/analysis/el/GreekStemmer;->exc9:Lorg/apache/lucene/analysis/util/CharArraySet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWithVowelNoY([CI)Z

    move-result v0

    if-nez v0, :cond_1

    .line 354
    const-string/jumbo v0, "\u03bf\u03b4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 355
    const-string/jumbo v0, "\u03b1\u03b9\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 356
    const-string/jumbo v0, "\u03c6\u03bf\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 357
    const-string/jumbo v0, "\u03c4\u03b1\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 358
    const-string/jumbo v0, "\u03b4\u03b9\u03b1\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 359
    const-string/jumbo v0, "\u03c3\u03c7"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 360
    const-string/jumbo v0, "\u03b5\u03bd\u03b4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 361
    const-string/jumbo v0, "\u03b5\u03c5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 362
    const-string/jumbo v0, "\u03c4\u03b9\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 363
    const-string/jumbo v0, "\u03c5\u03c0\u03b5\u03c1\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    const-string/jumbo v0, "\u03c1\u03b1\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 365
    const-string/jumbo v0, "\u03b5\u03bd\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 366
    const-string/jumbo v0, "\u03c1\u03bf\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 367
    const-string/jumbo v0, "\u03c3\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 368
    const-string/jumbo v0, "\u03c0\u03c5\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 369
    const-string/jumbo v0, "\u03b1\u03b9\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 370
    const-string/jumbo v0, "\u03c3\u03c5\u03bd\u03b4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 371
    const-string/jumbo v0, "\u03c3\u03c5\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 372
    const-string/jumbo v0, "\u03c3\u03c5\u03bd\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 373
    const-string/jumbo v0, "\u03c7\u03c9\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 374
    const-string/jumbo v0, "\u03c0\u03bf\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 375
    const-string/jumbo v0, "\u03b2\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 376
    const-string/jumbo v0, "\u03ba\u03b1\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 377
    const-string/jumbo v0, "\u03b5\u03c5\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 378
    const-string/jumbo v0, "\u03b5\u03ba\u03b8"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    const-string/jumbo v0, "\u03bd\u03b5\u03c4"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 380
    const-string/jumbo v0, "\u03c1\u03bf\u03bd"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    const-string/jumbo v0, "\u03b1\u03c1\u03ba"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 382
    const-string/jumbo v0, "\u03b2\u03b1\u03c1"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 383
    const-string/jumbo v0, "\u03b2\u03bf\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 384
    const-string/jumbo v0, "\u03c9\u03c6\u03b5\u03bb"

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/el/GreekStemmer;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 385
    :cond_1
    add-int/lit8 p2, p2, 0x2

    .line 389
    :cond_2
    return p2
.end method


# virtual methods
.method public stem([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 36
    const/4 v1, 0x4

    if-ge p2, v1, :cond_0

    move v1, p2

    .line 66
    :goto_0
    return v1

    .line 39
    :cond_0
    move v0, p2

    .line 41
    .local v0, "origLen":I
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule0([CI)I

    move-result p2

    .line 42
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule1([CI)I

    move-result p2

    .line 43
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule2([CI)I

    move-result p2

    .line 44
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule3([CI)I

    move-result p2

    .line 45
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule4([CI)I

    move-result p2

    .line 46
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule5([CI)I

    move-result p2

    .line 47
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule6([CI)I

    move-result p2

    .line 48
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule7([CI)I

    move-result p2

    .line 49
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule8([CI)I

    move-result p2

    .line 50
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule9([CI)I

    move-result p2

    .line 51
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule10([CI)I

    move-result p2

    .line 52
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule11([CI)I

    move-result p2

    .line 53
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule12([CI)I

    move-result p2

    .line 54
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule13([CI)I

    move-result p2

    .line 55
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule14([CI)I

    move-result p2

    .line 56
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule15([CI)I

    move-result p2

    .line 57
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule16([CI)I

    move-result p2

    .line 58
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule17([CI)I

    move-result p2

    .line 59
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule18([CI)I

    move-result p2

    .line 60
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule19([CI)I

    move-result p2

    .line 61
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule20([CI)I

    move-result p2

    .line 63
    if-ne p2, v0, :cond_1

    .line 64
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule21([CI)I

    move-result p2

    .line 66
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/el/GreekStemmer;->rule22([CI)I

    move-result v1

    goto :goto_0
.end method
