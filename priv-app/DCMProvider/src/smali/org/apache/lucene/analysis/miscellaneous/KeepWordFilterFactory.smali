.class public Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "KeepWordFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final enablePositionIncrements:Z

.field private final ignoreCase:Z

.field private final wordFiles:Ljava/lang/String;

.field private words:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 48
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->assureMatchVersion()V

    .line 49
    const-string/jumbo v0, "words"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->wordFiles:Ljava/lang/String;

    .line 50
    const-string v0, "ignoreCase"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->ignoreCase:Z

    .line 51
    const-string v0, "enablePositionIncrements"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->enablePositionIncrements:Z

    .line 52
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->words:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-nez v0, :cond_0

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    return-object p1

    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->enablePositionIncrements:Z

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->words:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public getWords()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->words:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->wordFiles:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->wordFiles:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->words:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 62
    :cond_0
    return-void
.end method

.method public isEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 65
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->enablePositionIncrements:Z

    return v0
.end method

.method public isIgnoreCase()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilterFactory;->ignoreCase:Z

    return v0
.end method
