.class public Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;
.super Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;
.source "TokenRangeSinkFilter.java"


# instance fields
.field private count:I

.field private lower:I

.field private upper:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "lower"    # I
    .param p2, "upper"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;-><init>()V

    .line 34
    iput p1, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->lower:I

    .line 35
    iput p2, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->upper:I

    .line 36
    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 2
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 42
    :try_start_0
    iget v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    iget v1, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->lower:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    iget v1, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->upper:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v0, v1, :cond_0

    .line 47
    iget v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    .line 43
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    .line 47
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    .line 45
    const/4 v0, 0x0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    .line 47
    iget v1, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    .line 48
    throw v0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/sinks/TokenRangeSinkFilter;->count:I

    .line 54
    return-void
.end method
