.class public Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "DictionaryCompoundWordTokenFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final dictFile:Ljava/lang/String;

.field private dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final maxSubwordSize:I

.field private final minSubwordSize:I

.field private final minWordSize:I

.field private final onlyLongestMatch:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 51
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->assureMatchVersion()V

    .line 52
    const-string v0, "dictionary"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->dictFile:Ljava/lang/String;

    .line 53
    const-string v0, "minWordSize"

    const/4 v1, 0x5

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->minWordSize:I

    .line 54
    const-string v0, "minSubwordSize"

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->minSubwordSize:I

    .line 55
    const-string v0, "maxSubwordSize"

    const/16 v1, 0xf

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->maxSubwordSize:I

    .line 56
    const-string v0, "onlyLongestMatch"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->onlyLongestMatch:Z

    .line 57
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 8
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-nez v0, :cond_0

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    return-object p1

    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->minWordSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->minSubwordSize:I

    iget v6, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->maxSubwordSize:I

    iget-boolean v7, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->onlyLongestMatch:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    move-object p1, v0

    goto :goto_0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->dictFile:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilterFactory;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 65
    return-void
.end method
