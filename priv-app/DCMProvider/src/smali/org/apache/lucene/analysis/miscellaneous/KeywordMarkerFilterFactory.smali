.class public Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "KeywordMarkerFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# static fields
.field public static final PATTERN:Ljava/lang/String; = "pattern"

.field public static final PROTECTED_TOKENS:Ljava/lang/String; = "protected"


# instance fields
.field private final ignoreCase:Z

.field private pattern:Ljava/util/regex/Pattern;

.field private protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final stringPattern:Ljava/lang/String;

.field private final wordFiles:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 52
    const-string v0, "protected"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->wordFiles:Ljava/lang/String;

    .line 53
    const-string v0, "pattern"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->stringPattern:Ljava/lang/String;

    .line 54
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->ignoreCase:Z

    .line 55
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 76
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->pattern:Ljava/util/regex/Pattern;

    if-eqz v1, :cond_0

    .line 77
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->pattern:Ljava/util/regex/Pattern;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/regex/Pattern;)V

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    .local v0, "input":Lorg/apache/lucene/analysis/TokenStream;
    move-object p1, v0

    .line 79
    .end local v0    # "input":Lorg/apache/lucene/analysis/TokenStream;
    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v1, :cond_1

    .line 80
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "input":Lorg/apache/lucene/analysis/TokenStream;
    move-object p1, v0

    .line 82
    .end local v0    # "input":Lorg/apache/lucene/analysis/TokenStream;
    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    return-object p1
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->wordFiles:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->wordFiles:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 65
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->stringPattern:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 66
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->ignoreCase:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->stringPattern:Ljava/lang/String;

    const/16 v1, 0x42

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->pattern:Ljava/util/regex/Pattern;

    .line 68
    :cond_1
    return-void

    .line 66
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->stringPattern:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    goto :goto_0
.end method

.method public isIgnoreCase()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilterFactory;->ignoreCase:Z

    return v0
.end method
