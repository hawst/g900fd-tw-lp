.class final Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;
.super Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;
.source "CharArrayMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/util/CharArrayMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EmptyCharArrayMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap",
        "<TV;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    const/4 v2, 0x0

    .line 662
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/util/CharArrayMap$UnmodifiableCharArrayMap;-><init>(Lorg/apache/lucene/analysis/util/CharArrayMap;)V

    .line 663
    return-void
.end method


# virtual methods
.method public containsKey(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;

    .prologue
    .line 674
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 675
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 676
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 681
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 682
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 683
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public containsKey([CII)Z
    .locals 1
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 667
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 668
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 669
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public get(Ljava/lang/CharSequence;)Ljava/lang/Object;
    .locals 1
    .param p1, "cs"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 695
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 696
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 697
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 702
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 703
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 704
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public get([CII)Ljava/lang/Object;
    .locals 1
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CII)TV;"
        }
    .end annotation

    .prologue
    .line 688
    .local p0, "this":Lorg/apache/lucene/analysis/util/CharArrayMap$EmptyCharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<TV;>.EmptyCharArrayMap<TV;>;"
    if-nez p1, :cond_0

    .line 689
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 690
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
