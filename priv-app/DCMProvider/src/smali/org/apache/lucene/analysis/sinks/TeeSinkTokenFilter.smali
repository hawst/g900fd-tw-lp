.class public final Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "TeeSinkTokenFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;,
        Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    }
.end annotation


# static fields
.field private static final ACCEPT_ALL_FILTER:Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;


# instance fields
.field private final sinks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 243
    new-instance v0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$1;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->ACCEPT_ALL_FILTER:Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

    .line 248
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    .line 85
    return-void
.end method


# virtual methods
.method public addSinkTokenStream(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;)V
    .locals 3
    .param p1, "sink"    # Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;

    .prologue
    .line 112
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->getAttributeFactory()Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->getAttributeFactory()Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The supplied sink is not compatible to this tee"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 116
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/util/AttributeSource;->getAttributeImplsIterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/util/AttributeImpl;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 119
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    return-void

    .line 117
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/util/AttributeImpl;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->addAttributeImpl(Lorg/apache/lucene/util/AttributeImpl;)V

    goto :goto_0
.end method

.method public consumeAllTokens()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->incrementToken()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    return-void
.end method

.method public final end()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->end()V

    .line 157
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v0

    .line 158
    .local v0, "finalState":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v3, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 164
    return-void

    .line 158
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 159
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;

    .line 160
    .local v2, "sink":Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    if-eqz v2, :cond_0

    .line 161
    # invokes: Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->setFinalState(Lorg/apache/lucene/util/AttributeSource$State;)V
    invoke-static {v2, v0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->access$3(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V

    goto :goto_0
.end method

.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    iget-object v3, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 136
    const/4 v2, 0x0

    .line 137
    .local v2, "state":Lorg/apache/lucene/util/AttributeSource$State;
    iget-object v3, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 148
    const/4 v3, 0x1

    .line 151
    .end local v2    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :goto_1
    return v3

    .line 137
    .restart local v2    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 138
    .local v0, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;>;"
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;

    .line 139
    .local v1, "sink":Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    if-eqz v1, :cond_0

    .line 140
    # invokes: Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->accept(Lorg/apache/lucene/util/AttributeSource;)Z
    invoke-static {v1, p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->access$1(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    if-nez v2, :cond_2

    .line 142
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v2

    .line 144
    :cond_2
    # invokes: Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->addState(Lorg/apache/lucene/util/AttributeSource$State;)V
    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->access$2(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V

    goto :goto_0

    .line 151
    .end local v0    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;>;"
    .end local v1    # "sink":Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    .end local v2    # "state":Lorg/apache/lucene/util/AttributeSource$State;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public newSinkTokenStream()Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->ACCEPT_ALL_FILTER:Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->newSinkTokenStream(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;)Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;

    move-result-object v0

    return-object v0
.end method

.method public newSinkTokenStream(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;)Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    .locals 3
    .param p1, "filter"    # Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

    .prologue
    .line 100
    new-instance v0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;)V

    .line 101
    .local v0, "sink":Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;->sinks:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    return-object v0
.end method
