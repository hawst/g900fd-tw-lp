.class public final Lorg/apache/lucene/analysis/core/KeywordTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "KeywordTokenizer.java"


# static fields
.field public static final DEFAULT_BUFFER_SIZE:I = 0x100


# instance fields
.field private done:Z

.field private finalOffset:I

.field private offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 40
    const/16 v0, 0x100

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;-><init>(Ljava/io/Reader;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;I)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "bufferSize"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->done:Z

    .line 36
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 45
    if-gtz p2, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0, p2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 49
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;I)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "bufferSize"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->done:Z

    .line 36
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 53
    if-gtz p3, :cond_0

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 57
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->finalOffset:I

    iget v2, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->finalOffset:I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 85
    return-void
.end method

.method public final incrementToken()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 61
    iget-boolean v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->done:Z

    if-nez v5, :cond_2

    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->clearAttributes()V

    .line 63
    iput-boolean v3, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->done:Z

    .line 64
    const/4 v2, 0x0

    .line 65
    .local v2, "upto":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 67
    .local v0, "buffer":[C
    :cond_0
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->input:Ljava/io/Reader;

    array-length v6, v0

    sub-int/2addr v6, v2

    invoke-virtual {v5, v0, v2, v6}, Ljava/io/Reader;->read([CII)I

    move-result v1

    .line 68
    .local v1, "length":I
    const/4 v5, -0x1

    if-ne v1, v5, :cond_1

    .line 73
    iget-object v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 74
    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->correctOffset(I)I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->finalOffset:I

    .line 75
    iget-object v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->correctOffset(I)I

    move-result v4

    iget v6, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->finalOffset:I

    invoke-interface {v5, v4, v6}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 78
    .end local v0    # "buffer":[C
    .end local v1    # "length":I
    .end local v2    # "upto":I
    :goto_1
    return v3

    .line 69
    .restart local v0    # "buffer":[C
    .restart local v1    # "length":I
    .restart local v2    # "upto":I
    :cond_1
    add-int/2addr v2, v1

    .line 70
    array-length v5, v0

    if-ne v2, v5, :cond_0

    .line 71
    iget-object v5, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    array-length v6, v0

    add-int/lit8 v6, v6, 0x1

    invoke-interface {v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    .line 66
    goto :goto_0

    .end local v0    # "buffer":[C
    .end local v1    # "length":I
    .end local v2    # "upto":I
    :cond_2
    move v3, v4

    .line 78
    goto :goto_1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/KeywordTokenizer;->done:Z

    .line 90
    return-void
.end method
