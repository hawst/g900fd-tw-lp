.class public abstract Lorg/apache/lucene/analysis/TokenFilter;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "TokenFilter.java"


# instance fields
.field protected final input:Lorg/apache/lucene/analysis/TokenStream;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 0
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/analysis/TokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    .line 35
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/analysis/TokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 59
    return-void
.end method

.method public end()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/analysis/TokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 47
    return-void
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/analysis/TokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 71
    return-void
.end method
