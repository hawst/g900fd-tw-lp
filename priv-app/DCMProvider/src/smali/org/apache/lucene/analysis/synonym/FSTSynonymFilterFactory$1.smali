.class Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "FSTSynonymFilterFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;

.field private final synthetic val$factory:Lorg/apache/lucene/analysis/util/TokenizerFactory;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;Lorg/apache/lucene/analysis/util/TokenizerFactory;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;->this$0:Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;

    iput-object p2, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;->val$factory:Lorg/apache/lucene/analysis/util/TokenizerFactory;

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 89
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;->val$factory:Lorg/apache/lucene/analysis/util/TokenizerFactory;

    if-nez v2, :cond_0

    new-instance v1, Lorg/apache/lucene/analysis/core/WhitespaceTokenizer;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_43:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/analysis/core/WhitespaceTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 90
    .local v1, "tokenizer":Lorg/apache/lucene/analysis/Tokenizer;
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;->this$0:Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;

    # getter for: Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->ignoreCase:Z
    invoke-static {v2}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;->access$0(Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_43:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 91
    .local v0, "stream":Lorg/apache/lucene/analysis/TokenStream;
    :goto_1
    new-instance v2, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v2, v1, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v2

    .line 89
    .end local v0    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .end local v1    # "tokenizer":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory$1;->val$factory:Lorg/apache/lucene/analysis/util/TokenizerFactory;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/analysis/util/TokenizerFactory;->create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/Tokenizer;

    move-result-object v1

    goto :goto_0

    .restart local v1    # "tokenizer":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_1
    move-object v0, v1

    .line 90
    goto :goto_1
.end method
