.class public Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;
.super Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;
.source "DictionaryCompoundWordTokenFilter.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 60
    if-nez p3, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "dictionary cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p4, "minWordSize"    # I
    .param p5, "minSubwordSize"    # I
    .param p6, "maxSubwordSize"    # I
    .param p7, "onlyLongestMatch"    # Z

    .prologue
    .line 88
    invoke-direct/range {p0 .. p7}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    .line 89
    if-nez p3, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "dictionary cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method protected decompose()V
    .locals 6

    .prologue
    .line 96
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    .line 97
    .local v2, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->minSubwordSize:I

    sub-int v4, v2, v4

    if-le v0, v4, :cond_0

    .line 121
    return-void

    .line 98
    :cond_0
    const/4 v3, 0x0

    .line 99
    .local v3, "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    iget v1, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->minSubwordSize:I

    .local v1, "j":I
    :goto_1
    iget v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->maxSubwordSize:I

    if-le v1, v4, :cond_3

    .line 117
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->onlyLongestMatch:Z

    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    .line 118
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->tokens:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :cond_3
    add-int v4, v0, v1

    if-gt v4, v2, :cond_1

    .line 103
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v5, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v5

    invoke-virtual {v4, v5, v0, v1}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 104
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->onlyLongestMatch:Z

    if-eqz v4, :cond_6

    .line 105
    if-eqz v3, :cond_5

    .line 106
    iget-object v4, v3, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->txt:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ge v4, v1, :cond_4

    .line 107
    new-instance v3, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .end local v3    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    invoke-direct {v3, p0, v0, v1}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    .line 99
    .restart local v3    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 110
    :cond_5
    new-instance v3, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .end local v3    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    invoke-direct {v3, p0, v0, v1}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    .line 112
    .restart local v3    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    goto :goto_2

    .line 113
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/DictionaryCompoundWordTokenFilter;->tokens:Ljava/util/LinkedList;

    new-instance v5, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    invoke-direct {v5, p0, v0, v1}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
