.class public Lorg/apache/lucene/analysis/ru/RussianLetterTokenizer;
.super Lorg/apache/lucene/analysis/util/CharTokenizer;
.source "RussianLetterTokenizer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final DIGIT_0:I = 0x30

.field private static final DIGIT_9:I = 0x39


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 72
    return-void
.end method


# virtual methods
.method protected isTokenChar(I)Z
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 80
    invoke-static {p1}, Ljava/lang/Character;->isLetter(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x30

    if-lt p1, v0, :cond_0

    const/16 v0, 0x39

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
