.class public Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;
.super Ljava/lang/Object;
.source "HunspellStemmer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    }
.end annotation


# static fields
.field private static final RECURSION_CAP:I = 0x2


# instance fields
.field private charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

.field private final dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

.field private final segment:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;)V
    .locals 1
    .param p1, "dictionary"    # Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->segment:Ljava/lang/StringBuilder;

    .line 45
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_40:Lorg/apache/lucene/util/Version;

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    .line 54
    return-void
.end method

.method private hasCrossCheckedFlag(C[C)Z
    .locals 1
    .param p1, "flag"    # C
    .param p2, "flags"    # [C

    .prologue
    .line 212
    if-eqz p2, :cond_0

    invoke-static {p2, p1}, Ljava/util/Arrays;->binarySearch([CC)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private stem([CI[CI)Ljava/util/List;
    .locals 15
    .param p1, "word"    # [C
    .param p2, "length"    # I
    .param p3, "flags"    # [C
    .param p4, "recursionDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CI[CI)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v8, "stems":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    move/from16 v0, p2

    if-lt v3, v0, :cond_0

    .line 139
    add-int/lit8 v3, p2, -0x1

    :goto_1
    if-gez v3, :cond_5

    .line 164
    return-object v8

    .line 118
    :cond_0
    iget-object v12, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    sub-int v13, p2, v3

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v3, v13}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->lookupSuffix([CII)Ljava/util/List;

    move-result-object v11

    .line 119
    .local v11, "suffixes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    if-nez v11, :cond_2

    .line 117
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 123
    :cond_2
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_3
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;

    .line 124
    .local v10, "suffix":Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    invoke-virtual {v10}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getFlag()C

    move-result v13

    move-object/from16 v0, p3

    invoke-direct {p0, v13, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->hasCrossCheckedFlag(C[C)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 125
    invoke-virtual {v10}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getAppend()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    sub-int v1, p2, v13

    .line 127
    .local v1, "deAffixedLength":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v10}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getStrip()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 129
    .local v9, "strippedWord":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v14

    move/from16 v0, p4

    invoke-virtual {p0, v13, v14, v10, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->applyAffix([CILorg/apache/lucene/analysis/hunspell/HunspellAffix;I)Ljava/util/List;

    move-result-object v7

    .line 130
    .local v7, "stemList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_4

    .line 134
    invoke-interface {v8, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 130
    :cond_4
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    .line 131
    .local v6, "stem":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    invoke-virtual {v6, v10}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->addSuffix(Lorg/apache/lucene/analysis/hunspell/HunspellAffix;)V

    goto :goto_3

    .line 140
    .end local v1    # "deAffixedLength":I
    .end local v6    # "stem":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    .end local v7    # "stemList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    .end local v9    # "strippedWord":Ljava/lang/String;
    .end local v10    # "suffix":Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    .end local v11    # "suffixes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    :cond_5
    iget-object v12, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v13, v3}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->lookupPrefix([CII)Ljava/util/List;

    move-result-object v5

    .line 141
    .local v5, "prefixes":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellAffix;>;"
    if-nez v5, :cond_7

    .line 139
    :cond_6
    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_1

    .line 145
    :cond_7
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_8
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;

    .line 146
    .local v4, "prefix":Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getFlag()C

    move-result v13

    move-object/from16 v0, p3

    invoke-direct {p0, v13, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->hasCrossCheckedFlag(C[C)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 147
    invoke-virtual {v4}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getAppend()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    .line 148
    .local v2, "deAffixedStart":I
    sub-int v1, p2, v2

    .line 150
    .restart local v1    # "deAffixedLength":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getStrip()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 151
    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v2, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 152
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 154
    .restart local v9    # "strippedWord":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v14

    move/from16 v0, p4

    invoke-virtual {p0, v13, v14, v4, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->applyAffix([CILorg/apache/lucene/analysis/hunspell/HunspellAffix;I)Ljava/util/List;

    move-result-object v7

    .line 155
    .restart local v7    # "stemList":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_9

    .line 159
    invoke-interface {v8, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    .line 155
    :cond_9
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    .line 156
    .restart local v6    # "stem":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    invoke-virtual {v6, v4}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->addPrefix(Lorg/apache/lucene/analysis/hunspell/HunspellAffix;)V

    goto :goto_5
.end method


# virtual methods
.method public applyAffix([CILorg/apache/lucene/analysis/hunspell/HunspellAffix;I)Ljava/util/List;
    .locals 6
    .param p1, "strippedWord"    # [C
    .param p2, "length"    # I
    .param p3, "affix"    # Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
    .param p4, "recursionDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CI",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 177
    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->isIgnoreCase()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    array-length v4, p1

    invoke-virtual {v3, p1, v5, v4}, Lorg/apache/lucene/analysis/util/CharacterUtils;->toLowerCase([CII)V

    .line 180
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->segment:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 181
    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->segment:Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1, v5, p2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 182
    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->segment:Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->checkCondition(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 183
    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    .line 201
    :cond_1
    :goto_0
    return-object v1

    .line 186
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .local v1, "stems":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-virtual {v3, p1, v5, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->lookupWord([CII)Ljava/util/List;

    move-result-object v2

    .line 189
    .local v2, "words":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellWord;>;"
    if-eqz v2, :cond_4

    .line 190
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    .line 197
    :cond_4
    invoke-virtual {p3}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->isCrossProduct()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    if-ge p4, v3, :cond_1

    .line 198
    invoke-virtual {p3}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getAppendFlags()[C

    move-result-object v3

    add-int/lit8 p4, p4, 0x1

    invoke-direct {p0, p1, p2, v3, p4}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->stem([CI[CI)Ljava/util/List;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 190
    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/hunspell/HunspellWord;

    .line 191
    .local v0, "hunspellWord":Lorg/apache/lucene/analysis/hunspell/HunspellWord;
    invoke-virtual {p3}, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->getFlag()C

    move-result v4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/analysis/hunspell/HunspellWord;->hasFlag(C)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 192
    new-instance v4, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    invoke-direct {v4, p1, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;-><init>([CI)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public stem(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "word"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->stem([CI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public stem([CI)Ljava/util/List;
    .locals 3
    .param p1, "word"    # [C
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CI)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v0, "stems":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-virtual {v1, p1, v2, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->lookupWord([CII)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 75
    new-instance v1, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    invoke-direct {v1, p1, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;-><init>([CI)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, v2}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->stem([CI[CI)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 78
    return-object v0
.end method

.method public uniqueStems([CI)Ljava/util/List;
    .locals 8
    .param p1, "word"    # [C
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CI)",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 88
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v2, "stems":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    new-instance v3, Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->getVersion()Lorg/apache/lucene/util/Version;

    move-result-object v4

    const/16 v5, 0x8

    iget-object v6, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->isIgnoreCase()Z

    move-result v6

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 90
    .local v3, "terms":Lorg/apache/lucene/analysis/util/CharArraySet;
    iget-object v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-virtual {v4, p1, v7, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;->lookupWord([CII)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 91
    new-instance v4, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    invoke-direct {v4, p1, p2}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;-><init>([CI)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-virtual {v3, p1}, Lorg/apache/lucene/analysis/util/CharArraySet;->add([C)Z

    .line 94
    :cond_0
    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, v4, v7}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;->stem([CI[CI)Ljava/util/List;

    move-result-object v0

    .line 95
    .local v0, "otherStems":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 101
    return-object v2

    .line 95
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;

    .line 96
    .local v1, "s":Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
    # getter for: Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stem:[C
    invoke-static {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->access$0(Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;)[C

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 97
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    # getter for: Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stem:[C
    invoke-static {v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->access$0(Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;)[C

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;->add([C)Z

    goto :goto_0
.end method
