.class public final Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "PersianNormalizationFilter.java"


# instance fields
.field private final normalizer:Lorg/apache/lucene/analysis/fa/PersianNormalizer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 33
    new-instance v0, Lorg/apache/lucene/analysis/fa/PersianNormalizer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/fa/PersianNormalizer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->normalizer:Lorg/apache/lucene/analysis/fa/PersianNormalizer;

    .line 34
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 38
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v1, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->normalizer:Lorg/apache/lucene/analysis/fa/PersianNormalizer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    .line 44
    iget-object v3, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    .line 43
    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/fa/PersianNormalizer;->normalize([CI)I

    move-result v0

    .line 45
    .local v0, "newlen":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 46
    const/4 v1, 0x1

    .line 48
    .end local v0    # "newlen":I
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
