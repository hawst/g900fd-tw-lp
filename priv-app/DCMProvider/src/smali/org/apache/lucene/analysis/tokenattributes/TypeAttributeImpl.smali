.class public Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "TypeAttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# instance fields
.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const-string/jumbo v0, "word"

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;-><init>(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 48
    const-string/jumbo v0, "word"

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 72
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 73
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 58
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;

    .line 59
    .local v0, "o":Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;
    iget-object v3, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    iget-object v2, v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .end local v0    # "o":Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;
    :cond_3
    move v1, v2

    .line 62
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public type()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttributeImpl;->type:Ljava/lang/String;

    return-object v0
.end method
