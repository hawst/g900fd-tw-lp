.class public Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "TokenOffsetPayloadTokenFilter.java"


# instance fields
.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 36
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 41
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 45
    iget-object v3, p0, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 46
    const/16 v3, 0x8

    new-array v0, v3, [B

    .line 47
    .local v0, "data":[B
    iget-object v3, p0, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v3

    invoke-static {v3, v0, v2}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeInt(I[BI)[B

    .line 48
    iget-object v2, p0, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v2

    const/4 v3, 0x4

    invoke-static {v2, v0, v3}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeInt(I[BI)[B

    .line 49
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, v0}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 50
    .local v1, "payload":Lorg/apache/lucene/util/BytesRef;
    iget-object v2, p0, Lorg/apache/lucene/analysis/payloads/TokenOffsetPayloadTokenFilter;->payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 51
    const/4 v2, 0x1

    .line 53
    .end local v0    # "data":[B
    .end local v1    # "payload":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    return v2
.end method
