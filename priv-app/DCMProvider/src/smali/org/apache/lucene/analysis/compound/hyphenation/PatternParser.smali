.class public Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "PatternParser.java"


# static fields
.field static final ELEM_CLASSES:I = 0x1

.field static final ELEM_EXCEPTIONS:I = 0x2

.field static final ELEM_HYPHEN:I = 0x4

.field static final ELEM_PATTERNS:I = 0x3


# instance fields
.field consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

.field currElement:I

.field errMsg:Ljava/lang/String;

.field exception:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field hyphenChar:C

.field parser:Lorg/xml/sax/XMLReader;

.field token:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    .line 67
    invoke-static {}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->createParser()Lorg/xml/sax/XMLReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parser:Lorg/xml/sax/XMLReader;

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parser:Lorg/xml/sax/XMLReader;

    invoke-interface {v0, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 69
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parser:Lorg/xml/sax/XMLReader;

    invoke-interface {v0, p0}, Lorg/xml/sax/XMLReader;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V

    .line 70
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parser:Lorg/xml/sax/XMLReader;

    invoke-interface {v0, p0}, Lorg/xml/sax/XMLReader;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    .line 71
    const/16 v0, 0x2d

    iput-char v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->hyphenChar:C

    .line 73
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;)V
    .locals 0
    .param p1, "consumer"    # Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;-><init>()V

    .line 77
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    .line 78
    return-void
.end method

.method static createParser()Lorg/xml/sax/XMLReader;
    .locals 5

    .prologue
    .line 126
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    .line 127
    .local v1, "factory":Ljavax/xml/parsers/SAXParserFactory;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/SAXParserFactory;->setNamespaceAware(Z)V

    .line 128
    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t create XMLReader: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected static getInterletterValues(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "pat"    # Ljava/lang/String;

    .prologue
    .line 237
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    .local v2, "il":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "a"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 239
    .local v4, "word":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 240
    .local v3, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v3, :cond_0

    .line 249
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 241
    :cond_0
    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 242
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 243
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 244
    add-int/lit8 v1, v1, 0x1

    .line 240
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 246
    :cond_1
    const/16 v5, 0x30

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private getLocationString(Lorg/xml/sax/SAXParseException;)Ljava/lang/String;
    .locals 5
    .param p1, "ex"    # Lorg/xml/sax/SAXParseException;

    .prologue
    const/16 v4, 0x3a

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 378
    .local v1, "str":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lorg/xml/sax/SAXParseException;->getSystemId()Ljava/lang/String;

    move-result-object v2

    .line 379
    .local v2, "systemId":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 380
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 381
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 382
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 384
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    .end local v0    # "index":I
    :cond_1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 387
    invoke-virtual {p1}, Lorg/xml/sax/SAXParseException;->getLineNumber()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 388
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 389
    invoke-virtual {p1}, Lorg/xml/sax/SAXParseException;->getColumnNumber()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 391
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected static getPattern(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "word"    # Ljava/lang/String;

    .prologue
    .line 180
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    .local v2, "pat":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 182
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 187
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 183
    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-nez v3, :cond_1

    .line 184
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 182
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 5
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    .line 347
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 348
    .local v0, "chars":Ljava/lang/StringBuffer;
    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 349
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->readToken(Ljava/lang/StringBuffer;)Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "word":Ljava/lang/String;
    :goto_0
    if-nez v1, :cond_0

    .line 370
    return-void

    .line 352
    :cond_0
    iget v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    packed-switch v2, :pswitch_data_0

    .line 367
    :goto_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->readToken(Ljava/lang/StringBuffer;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 354
    :pswitch_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    invoke-interface {v2, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;->addClass(Ljava/lang/String;)V

    goto :goto_1

    .line 357
    :pswitch_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->normalizeException(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    .line 359
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->getExceptionWord(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v4

    .line 360
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 359
    invoke-interface {v3, v4, v2}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;->addException(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 361
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 364
    :pswitch_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    invoke-static {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->getPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->getInterletterValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;->addPattern(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "local"    # Ljava/lang/String;
    .param p3, "raw"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 310
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 311
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "word":Ljava/lang/String;
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    packed-switch v1, :pswitch_data_0

    .line 329
    :goto_0
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    if-eq v1, v5, :cond_0

    .line 330
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 333
    .end local v0    # "word":Ljava/lang/String;
    :cond_0
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    if-ne v1, v5, :cond_1

    .line 334
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    .line 339
    :goto_1
    return-void

    .line 314
    .restart local v0    # "word":Ljava/lang/String;
    :pswitch_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;->addClass(Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :pswitch_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->normalizeException(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    .line 319
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->getExceptionWord(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    .line 320
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 319
    invoke-interface {v2, v3, v1}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;->addException(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 323
    :pswitch_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    invoke-static {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->getPattern(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->getInterletterValues(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;->addPattern(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 336
    .end local v0    # "word":Ljava/lang/String;
    :cond_1
    iput v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    goto :goto_1

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getExceptionWord(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, "ex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    .local v2, "res":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 233
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 224
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 225
    .local v1, "item":Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 226
    check-cast v1, Ljava/lang/String;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .restart local v1    # "item":Ljava/lang/Object;
    :cond_2
    move-object v3, v1

    .line 228
    check-cast v3, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;

    iget-object v3, v3, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->noBreak:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 229
    check-cast v1, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;

    .end local v1    # "item":Ljava/lang/Object;
    iget-object v3, v1, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;->noBreak:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method protected normalizeException(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<*>;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "ex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<*>;"
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 191
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .local v6, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v3, v8, :cond_0

    .line 218
    return-object v6

    .line 193
    :cond_0
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 194
    .local v4, "item":Ljava/lang/Object;
    instance-of v8, v4, Ljava/lang/String;

    if-eqz v8, :cond_4

    move-object v7, v4

    .line 195
    check-cast v7, Ljava/lang/String;

    .line 196
    .local v7, "str":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 197
    .local v0, "buf":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v5, v8, :cond_2

    .line 211
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_1

    .line 212
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .end local v5    # "j":I
    .end local v7    # "str":Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 198
    .restart local v0    # "buf":Ljava/lang/StringBuilder;
    .restart local v5    # "j":I
    .restart local v7    # "str":Ljava/lang/String;
    :cond_2
    invoke-virtual {v7, v5}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 199
    .local v1, "c":C
    iget-char v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->hyphenChar:C

    if-eq v1, v8, :cond_3

    .line 200
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 197
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 202
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 204
    const/4 v8, 0x1

    new-array v2, v8, [C

    .line 205
    .local v2, "h":[C
    iget-char v8, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->hyphenChar:C

    aput-char v8, v2, v10

    .line 208
    new-instance v8, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v2}, Ljava/lang/String;-><init>([C)V

    invoke-direct {v8, v9, v11, v11}, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 215
    .end local v0    # "buf":Ljava/lang/StringBuilder;
    .end local v1    # "c":C
    .end local v2    # "h":[C
    .end local v5    # "j":I
    .end local v7    # "str":Ljava/lang/String;
    :cond_4
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public parse(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-virtual {p1}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    .line 102
    .local v0, "src":Lorg/xml/sax/InputSource;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parse(Lorg/xml/sax/InputSource;)V

    .line 103
    return-void
.end method

.method public parse(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parse(Lorg/xml/sax/InputSource;)V

    .line 92
    return-void
.end method

.method public parse(Lorg/xml/sax/InputSource;)V
    .locals 2
    .param p1, "source"    # Lorg/xml/sax/InputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->parser:Lorg/xml/sax/XMLReader;

    invoke-interface {v1, p1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Lorg/xml/sax/SAXException;
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected readToken(Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 7
    .param p1, "chars"    # Ljava/lang/StringBuffer;

    .prologue
    const/4 v6, 0x0

    .line 136
    const/4 v2, 0x0

    .line 138
    .local v2, "space":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lt v1, v4, :cond_1

    .line 145
    :cond_0
    if-eqz v2, :cond_3

    .line 147
    move v0, v1

    .local v0, "countr":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lt v0, v4, :cond_2

    .line 150
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 151
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 152
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "word":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 176
    .end local v3    # "word":Ljava/lang/String;
    :goto_2
    return-object v3

    .line 139
    .end local v0    # "countr":I
    :cond_1
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    const/4 v2, 0x1

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 148
    .restart local v0    # "countr":I
    :cond_2
    sub-int v4, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    .end local v0    # "countr":I
    :cond_3
    const/4 v2, 0x0

    .line 158
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lt v1, v4, :cond_4

    .line 164
    :goto_4
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    move v0, v1

    .restart local v0    # "countr":I
    :goto_5
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lt v0, v4, :cond_6

    .line 169
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 170
    if-eqz v2, :cond_7

    .line 171
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 172
    .restart local v3    # "word":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_2

    .line 159
    .end local v0    # "countr":I
    .end local v3    # "word":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 160
    const/4 v2, 0x1

    .line 161
    goto :goto_4

    .line 158
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 167
    .restart local v0    # "countr":I
    :cond_6
    sub-int v4, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 175
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    .line 176
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public resolveEntity(Ljava/lang/String;Ljava/lang/String;)Lorg/xml/sax/InputSource;
    .locals 3
    .param p1, "publicId"    # Ljava/lang/String;
    .param p2, "systemId"    # Ljava/lang/String;

    .prologue
    .line 259
    if-eqz p2, :cond_0

    const-string v0, "(?i).*\\bhyphenation.dtd\\b.*"

    invoke-virtual {p2, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    :cond_0
    const-string v0, "hyphenation-info"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263
    :cond_1
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "hyphenation.dtd"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    .line 265
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setConsumer(Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;)V
    .locals 0
    .param p1, "consumer"    # Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    .prologue
    .line 81
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->consumer:Lorg/apache/lucene/analysis/compound/hyphenation/PatternConsumer;

    .line 82
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "local"    # Ljava/lang/String;
    .param p3, "raw"    # Ljava/lang/String;
    .param p4, "attrs"    # Lorg/xml/sax/Attributes;

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 279
    const-string v1, "hyphen-char"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    const-string v1, "value"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "h":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 282
    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->hyphenChar:C

    .line 299
    .end local v0    # "h":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 300
    return-void

    .line 284
    :cond_1
    const-string v1, "classes"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 285
    iput v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    goto :goto_0

    .line 286
    :cond_2
    const-string v1, "patterns"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 287
    const/4 v1, 0x3

    iput v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    goto :goto_0

    .line 288
    :cond_3
    const-string v1, "exceptions"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 289
    const/4 v1, 0x2

    iput v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    .line 290
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    goto :goto_0

    .line 291
    :cond_4
    const-string v1, "hyphen"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 293
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->token:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->exception:Ljava/util/ArrayList;

    new-instance v2, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;

    const-string v3, "pre"

    invoke-interface {p4, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "no"

    invoke-interface {p4, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 296
    const-string v5, "post"

    invoke-interface {p4, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphen;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    const/4 v1, 0x4

    iput v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/PatternParser;->currElement:I

    goto :goto_0
.end method
