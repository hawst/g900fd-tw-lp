.class public abstract Lorg/apache/lucene/analysis/payloads/AbstractEncoder;
.super Ljava/lang/Object;
.source "AbstractEncoder.java"

# interfaces
.implements Lorg/apache/lucene/analysis/payloads/PayloadEncoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public encode([C)Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .param p1, "buffer"    # [C

    .prologue
    .line 31
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/payloads/AbstractEncoder;->encode([CII)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method
