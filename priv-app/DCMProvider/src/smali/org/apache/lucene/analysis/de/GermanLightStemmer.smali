.class public Lorg/apache/lucene/analysis/de/GermanLightStemmer;
.super Ljava/lang/Object;
.source "GermanLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private stEnding(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 90
    packed-switch p1, :pswitch_data_0

    .line 101
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 100
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private step1([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v3, 0x3

    const/16 v2, 0x65

    .line 106
    const/4 v0, 0x5

    if-le p2, v0, :cond_1

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    if-ne v0, v2, :cond_1

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    const/16 v1, 0x72

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x6e

    if-ne v0, v1, :cond_1

    .line 107
    add-int/lit8 p2, p2, -0x3

    .line 123
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 109
    .restart local p2    # "len":I
    :cond_1
    const/4 v0, 0x4

    if-le p2, v0, :cond_2

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    if-ne v0, v2, :cond_2

    .line 110
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    packed-switch v0, :pswitch_data_0

    .line 117
    :cond_2
    :pswitch_0
    if-le p2, v3, :cond_3

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    if-ne v0, v2, :cond_3

    .line 118
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 114
    :pswitch_1
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 120
    :cond_3
    if-le p2, v3, :cond_0

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanLightStemmer;->stEnding(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x6d
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private step2([CI)I
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v4, 0x74

    const/16 v3, 0x73

    const/16 v1, 0x65

    const/4 v2, 0x4

    .line 127
    const/4 v0, 0x5

    if-le p2, v0, :cond_1

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_1

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    if-ne v0, v4, :cond_1

    .line 128
    add-int/lit8 p2, p2, -0x3

    .line 136
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 130
    .restart local p2    # "len":I
    :cond_1
    if-le p2, v2, :cond_3

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x6e

    if-ne v0, v1, :cond_3

    .line 131
    :cond_2
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 133
    :cond_3
    if-le p2, v2, :cond_0

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_0

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    if-ne v0, v4, :cond_0

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/de/GermanLightStemmer;->stEnding(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    add-int/lit8 p2, p2, -0x2

    goto :goto_0
.end method


# virtual methods
.method public stem([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 65
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 85
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/de/GermanLightStemmer;->step1([CI)I

    move-result p2

    .line 86
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/de/GermanLightStemmer;->step2([CI)I

    move-result v1

    return v1

    .line 66
    :cond_0
    aget-char v1, p1, v0

    packed-switch v1, :pswitch_data_0

    .line 65
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :pswitch_1
    const/16 v1, 0x61

    aput-char v1, p1, v0

    goto :goto_1

    .line 74
    :pswitch_2
    const/16 v1, 0x6f

    aput-char v1, p1, v0

    goto :goto_1

    .line 78
    :pswitch_3
    const/16 v1, 0x69

    aput-char v1, p1, v0

    goto :goto_1

    .line 82
    :pswitch_4
    const/16 v1, 0x75

    aput-char v1, p1, v0

    goto :goto_1

    .line 66
    :pswitch_data_0
    .packed-switch 0xe0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
