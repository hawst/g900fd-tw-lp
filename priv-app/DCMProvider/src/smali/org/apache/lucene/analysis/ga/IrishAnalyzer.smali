.class public final Lorg/apache/lucene/analysis/ga/IrishAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "IrishAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/ga/IrishAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field private static final DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "stopwords.txt"

.field private static final HYPHENATIONS:Lorg/apache/lucene/analysis/util/CharArraySet;


# instance fields
.field private final stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 48
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    new-array v2, v7, [Ljava/lang/String;

    .line 50
    const-string v3, "d"

    aput-object v3, v2, v5

    const-string v3, "m"

    aput-object v3, v2, v4

    const-string v3, "b"

    aput-object v3, v2, v6

    .line 49
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 48
    invoke-direct {v0, v1, v2, v4}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    .line 47
    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 59
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    new-array v2, v7, [Ljava/lang/String;

    .line 61
    const-string v3, "h"

    aput-object v3, v2, v5

    const-string v3, "n"

    aput-object v3, v2, v4

    const-string v3, "t"

    aput-object v3, v2, v6

    .line 60
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 59
    invoke-direct {v0, v1, v2, v4}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    .line 58
    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->HYPHENATIONS:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 62
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 95
    sget-object v0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 105
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 119
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 121
    return-void
.end method

.method static synthetic access$0(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->loadStopwordSet(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 138
    new-instance v3, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v3, v4, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 139
    .local v3, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v4, v3}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 140
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v2, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v5, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->HYPHENATIONS:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v2, v4, v0, v5}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 141
    .local v2, "s":Lorg/apache/lucene/analysis/core/StopFilter;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lorg/apache/lucene/analysis/core/StopFilter;->setEnablePositionIncrements(Z)V

    .line 142
    move-object v0, v2

    .line 143
    new-instance v1, Lorg/apache/lucene/analysis/util/ElisionFilter;

    sget-object v4, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v4}, Lorg/apache/lucene/analysis/util/ElisionFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 144
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/ga/IrishLowerCaseFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 145
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v5, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v4, v0, v5}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 146
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v4, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 147
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v4, p0, Lorg/apache/lucene/analysis/ga/IrishAnalyzer;->stemExclusionSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 148
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v4, Lorg/tartarus/snowball/ext/IrishStemmer;

    invoke-direct {v4}, Lorg/tartarus/snowball/ext/IrishStemmer;-><init>()V

    invoke-direct {v1, v0, v4}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .line 149
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v4, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v4, v3, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v4

    :cond_0
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0
.end method
