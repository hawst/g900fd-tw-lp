.class public Lorg/apache/lucene/analysis/pt/PortugueseMinimalStemmer;
.super Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.source "PortugueseMinimalStemmer.java"


# static fields
.field private static final pluralStep:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/analysis/pt/PortugueseMinimalStemmer;

    const-string v1, "portuguese.rslp"

    invoke-static {v0, v1}, Lorg/apache/lucene/analysis/pt/PortugueseMinimalStemmer;->parse(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "Plural"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 33
    sput-object v0, Lorg/apache/lucene/analysis/pt/PortugueseMinimalStemmer;->pluralStep:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 37
    sget-object v0, Lorg/apache/lucene/analysis/pt/PortugueseMinimalStemmer;->pluralStep:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result v0

    return v0
.end method
