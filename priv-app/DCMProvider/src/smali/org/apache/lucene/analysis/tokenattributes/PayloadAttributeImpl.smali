.class public Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
.super Lorg/apache/lucene/util/AttributeImpl;
.source "PayloadAttributeImpl.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;


# instance fields
.field private payload:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/util/AttributeImpl;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 37
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 52
    return-void
.end method

.method public clone()Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lorg/apache/lucene/util/AttributeImpl;->clone()Lorg/apache/lucene/util/AttributeImpl;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;

    .line 57
    .local v0, "clone":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 60
    :cond_0
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/util/AttributeImpl;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->clone()Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;

    move-result-object v0

    return-object v0
.end method

.method public copyTo(Lorg/apache/lucene/util/AttributeImpl;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/AttributeImpl;

    .prologue
    .line 88
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 89
    .local v0, "t":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 90
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 69
    :cond_1
    instance-of v3, p1, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 70
    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;

    .line 71
    .local v0, "o":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    iget-object v3, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_4

    .line 72
    :cond_2
    iget-object v3, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    .line 75
    :cond_4
    iget-object v1, v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .end local v0    # "o":Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;
    :cond_5
    move v1, v2

    .line 78
    goto :goto_0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public setPayload(Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "payload"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 46
    iput-object p1, p0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttributeImpl;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 47
    return-void
.end method
