.class public final Lorg/apache/lucene/analysis/core/LowerCaseTokenizer;
.super Lorg/apache/lucene/analysis/core/LetterTokenizer;
.source "LowerCaseTokenizer.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/core/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/core/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 75
    return-void
.end method


# virtual methods
.method protected normalize(I)I
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 81
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v0

    return v0
.end method
