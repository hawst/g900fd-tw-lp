.class public final Lorg/apache/lucene/analysis/fa/PersianAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "PersianAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/fa/PersianAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "stopwords.txt"

.field public static final STOPWORDS_COMMENT:Ljava/lang/String; = "#"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 92
    sget-object v0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 105
    return-void
.end method

.method static synthetic access$0(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-static {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->loadStopwordSet(ZLjava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 121
    iget-object v3, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 122
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 126
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    :goto_0
    new-instance v0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 127
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/ar/ArabicNormalizationFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/ar/ArabicNormalizationFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 129
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/fa/PersianNormalizationFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 134
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v4, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v5, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v6, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    invoke-direct {v3, v2, v4}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v3

    .line 124
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .end local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_0
    new-instance v2, Lorg/apache/lucene/analysis/ar/ArabicLetterTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/ar/ArabicLetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .restart local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    goto :goto_0
.end method

.method protected initReader(Ljava/lang/String;Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/analysis/fa/PersianAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    new-instance v0, Lorg/apache/lucene/analysis/fa/PersianCharFilter;

    invoke-direct {v0, p2}, Lorg/apache/lucene/analysis/fa/PersianCharFilter;-><init>(Ljava/io/Reader;)V

    move-object p2, v0

    .line 142
    .end local p2    # "reader":Ljava/io/Reader;
    :cond_0
    return-object p2
.end method
