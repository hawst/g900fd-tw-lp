.class public Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
.super Ljava/lang/Object;
.source "RSLPStemmerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Rule"
.end annotation


# instance fields
.field protected final min:I

.field protected final replacement:[C

.field protected final suffix:[C


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "suffix"    # Ljava/lang/String;
    .param p2, "min"    # I
    .param p3, "replacement"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->suffix:[C

    .line 102
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->replacement:[C

    .line 103
    iput p2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->min:I

    .line 104
    return-void
.end method


# virtual methods
.method public matches([CI)Z
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->suffix:[C

    array-length v0, v0

    sub-int v0, p2, v0

    iget v1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->min:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->suffix:[C

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CI[C)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public replace([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->replacement:[C

    array-length v0, v0

    if-lez v0, :cond_0

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->replacement:[C

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->suffix:[C

    array-length v2, v2

    sub-int v2, p2, v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->replacement:[C

    array-length v3, v3

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->suffix:[C

    array-length v0, v0

    sub-int v0, p2, v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->replacement:[C

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method
