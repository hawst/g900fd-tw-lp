.class public Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "CommonGramsFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final commonWordFiles:Ljava/lang/String;

.field private commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final format:Ljava/lang/String;

.field private final ignoreCase:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 48
    const-string/jumbo v0, "words"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWordFiles:Ljava/lang/String;

    .line 49
    const-string v0, "format"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->format:Ljava/lang/String;

    .line 50
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->ignoreCase:Z

    .line 51
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenFilter;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 79
    new-instance v0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 80
    .local v0, "commonGrams":Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;
    return-object v0
.end method

.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenFilter;

    move-result-object v0

    return-object v0
.end method

.method public getCommonWords()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWordFiles:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 59
    const-string v0, "snowball"

    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->format:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWordFiles:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->getSnowballWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 67
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWordFiles:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0

    .line 65
    :cond_1
    sget-object v0, Lorg/apache/lucene/analysis/core/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0
.end method

.method public isIgnoreCase()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilterFactory;->ignoreCase:Z

    return v0
.end method
