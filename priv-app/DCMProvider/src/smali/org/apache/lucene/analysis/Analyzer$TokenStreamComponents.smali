.class public Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.super Ljava/lang/Object;
.source "Analyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/Analyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TokenStreamComponents"
.end annotation


# instance fields
.field protected final sink:Lorg/apache/lucene/analysis/TokenStream;

.field protected final source:Lorg/apache/lucene/analysis/Tokenizer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Tokenizer;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/analysis/Tokenizer;

    .prologue
    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    iput-object p1, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    .line 234
    iput-object p1, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->sink:Lorg/apache/lucene/analysis/TokenStream;

    .line 235
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 0
    .param p1, "source"    # Lorg/apache/lucene/analysis/Tokenizer;
    .param p2, "result"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput-object p1, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    .line 223
    iput-object p2, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->sink:Lorg/apache/lucene/analysis/TokenStream;

    .line 224
    return-void
.end method


# virtual methods
.method public getTokenStream()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->sink:Lorg/apache/lucene/analysis/TokenStream;

    return-object v0
.end method

.method public getTokenizer()Lorg/apache/lucene/analysis/Tokenizer;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    return-object v0
.end method

.method protected setReader(Ljava/io/Reader;)V
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->source:Lorg/apache/lucene/analysis/Tokenizer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Tokenizer;->setReader(Ljava/io/Reader;)V

    .line 248
    return-void
.end method
