.class public Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;
.super Lorg/apache/lucene/analysis/charfilter/BaseCharFilter;
.source "PatternReplaceCharFilter.java"


# static fields
.field public static final DEFAULT_MAX_BLOCK_CHARS:I = 0x2710
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final pattern:Ljava/util/regex/Pattern;

.field private final replacement:Ljava/lang/String;

.field private transformedInput:Ljava/io/Reader;


# direct methods
.method public constructor <init>(Ljava/util/regex/Pattern;Ljava/lang/String;ILjava/lang/String;Ljava/io/Reader;)V
    .locals 0
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "replacement"    # Ljava/lang/String;
    .param p3, "maxBlockChars"    # I
    .param p4, "blockDelimiter"    # Ljava/lang/String;
    .param p5, "in"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p5}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/io/Reader;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/io/Reader;)V
    .locals 0
    .param p1, "pattern"    # Ljava/util/regex/Pattern;
    .param p2, "replacement"    # Ljava/lang/String;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 59
    invoke-direct {p0, p3}, Lorg/apache/lucene/analysis/charfilter/BaseCharFilter;-><init>(Ljava/io/Reader;)V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->pattern:Ljava/util/regex/Pattern;

    .line 61
    iput-object p2, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->replacement:Ljava/lang/String;

    .line 62
    return-void
.end method

.method private fill()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    .local v0, "buffered":Ljava/lang/StringBuilder;
    const/16 v3, 0x400

    new-array v2, v3, [C

    .line 83
    .local v2, "temp":[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v3, v2}, Ljava/io/Reader;->read([C)I

    move-result v1

    .local v1, "cnt":I
    :goto_0
    if-gtz v1, :cond_0

    .line 86
    new-instance v3, Ljava/io/StringReader;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->processPattern(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->transformedInput:Ljava/io/Reader;

    .line 87
    return-void

    .line 84
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 83
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v3, v2}, Ljava/io/Reader;->read([C)I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method protected correct(I)I
    .locals 2
    .param p1, "currentOff"    # I

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/charfilter/BaseCharFilter;->correct(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method processPattern(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 12
    .param p1, "input"    # Ljava/lang/CharSequence;

    .prologue
    .line 107
    iget-object v10, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v10, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 109
    .local v7, "m":Ljava/util/regex/Matcher;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 110
    .local v2, "cumulativeOutput":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 111
    .local v1, "cumulative":I
    const/4 v5, 0x0

    .line 112
    .local v5, "lastMatchEnd":I
    :cond_0
    :goto_0
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-nez v10, :cond_1

    .line 145
    invoke-virtual {v7, v2}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 146
    return-object v2

    .line 113
    :cond_1
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->end()I

    move-result v10

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->start()I

    move-result v11

    sub-int v3, v10, v11

    .line 114
    .local v3, "groupSize":I
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->start()I

    move-result v10

    sub-int v9, v10, v5

    .line 115
    .local v9, "skippedSize":I
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    .line 117
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    add-int v6, v10, v9

    .line 118
    .local v6, "lengthBeforeReplacement":I
    iget-object v10, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->replacement:Ljava/lang/String;

    invoke-virtual {v7, v2, v10}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 121
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    sub-int v8, v10, v6

    .line 123
    .local v8, "replacementSize":I
    if-eq v3, v8, :cond_0

    .line 124
    if-ge v8, v3, :cond_2

    .line 129
    sub-int v10, v3, v8

    add-int/2addr v1, v10

    .line 130
    add-int v0, v6, v8

    .line 132
    .local v0, "atIndex":I
    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->addOffCorrectMap(II)V

    goto :goto_0

    .line 136
    .end local v0    # "atIndex":I
    :cond_2
    move v4, v3

    .local v4, "i":I
    :goto_1
    if-ge v4, v8, :cond_0

    .line 137
    add-int v10, v6, v4

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v10, v1}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->addOffCorrectMap(II)V

    .line 136
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->transformedInput:Ljava/io/Reader;

    if-nez v0, :cond_0

    .line 92
    invoke-direct {p0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->fill()V

    .line 95
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->transformedInput:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->read()I

    move-result v0

    return v0
.end method

.method public read([CII)I
    .locals 1
    .param p1, "cbuf"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->transformedInput:Ljava/io/Reader;

    if-nez v0, :cond_0

    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->fill()V

    .line 77
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceCharFilter;->transformedInput:Ljava/io/Reader;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/Reader;->read([CII)I

    move-result v0

    return v0
.end method
