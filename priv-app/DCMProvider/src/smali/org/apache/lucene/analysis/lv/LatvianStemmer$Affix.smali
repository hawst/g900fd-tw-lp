.class Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;
.super Ljava/lang/Object;
.source "LatvianStemmer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/lv/LatvianStemmer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Affix"
.end annotation


# instance fields
.field affix:[C

.field palatalizes:Z

.field vc:I


# direct methods
.method constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "affix"    # Ljava/lang/String;
    .param p2, "vc"    # I
    .param p3, "palatalizes"    # Z

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->affix:[C

    .line 82
    iput p2, p0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->vc:I

    .line 83
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->palatalizes:Z

    .line 84
    return-void
.end method
