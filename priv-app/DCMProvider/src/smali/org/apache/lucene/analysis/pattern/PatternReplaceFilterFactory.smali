.class public Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "PatternReplaceFilterFactory.java"


# instance fields
.field final pattern:Ljava/util/regex/Pattern;

.field final replaceAll:Z

.field final replacement:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 48
    const-string v0, "pattern"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->getPattern(Ljava/util/Map;Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->pattern:Ljava/util/regex/Pattern;

    .line 49
    const-string v0, "replacement"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->replacement:Ljava/lang/String;

    .line 50
    const-string v0, "all"

    const-string v1, "replace"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "all"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "first"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const-string v3, "all"

    invoke-virtual {p0, p1, v1, v2, v3}, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;Ljava/util/Collection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->replaceAll:Z

    .line 51
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->pattern:Ljava/util/regex/Pattern;

    iget-object v2, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->replacement:Ljava/lang/String;

    iget-boolean v3, p0, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilterFactory;->replaceAll:Z

    invoke-direct {v0, p1, v1, v2, v3}, Lorg/apache/lucene/analysis/pattern/PatternReplaceFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/regex/Pattern;Ljava/lang/String;Z)V

    return-object v0
.end method
