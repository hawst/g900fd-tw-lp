.class public final Lorg/apache/lucene/analysis/util/RollingCharBuffer;
.super Ljava/lang/Object;
.source "RollingCharBuffer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffer:[C

.field private count:I

.field private end:Z

.field private nextPos:I

.field private nextWrite:I

.field private reader:Ljava/io/Reader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/16 v0, 0x200

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    .line 34
    return-void
.end method

.method private getIndex(I)I
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 113
    iget v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    iget v2, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    sub-int/2addr v2, p1

    sub-int v0, v1, v2

    .line 114
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v1, v1

    add-int/2addr v0, v1

    .line 117
    sget-boolean v1, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 119
    :cond_0
    return v0
.end method

.method private inBounds(I)Z
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 109
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    iget v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public freeBefore(I)V
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 146
    sget-boolean v1, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 147
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    if-le p1, v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 148
    :cond_1
    iget v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    sub-int v0, v1, p1

    .line 149
    .local v0, "newCount":I
    sget-boolean v1, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    if-le v0, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "newCount="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " count="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 150
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v1, v1

    if-le v0, v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "newCount="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " buf.length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 151
    :cond_3
    iput v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    .line 152
    return-void
.end method

.method public get(I)I
    .locals 9
    .param p1, "pos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v0, -0x1

    .line 68
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    if-ne p1, v4, :cond_4

    .line 69
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->end:Z

    if-eqz v4, :cond_0

    .line 103
    :goto_0
    return v0

    .line 72
    :cond_0
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v5, v5

    if-ne v4, v5, :cond_1

    .line 74
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    add-int/lit8 v4, v4, 0x1

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v4

    new-array v1, v4, [C

    .line 76
    .local v1, "newBuffer":[C
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v6, v6

    iget v7, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    sub-int/2addr v6, v7

    invoke-static {v4, v5, v1, v8, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v5, v5

    iget v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    sub-int/2addr v5, v6

    iget v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    invoke-static {v4, v8, v1, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 78
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    iput v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    .line 79
    iput-object v1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    .line 81
    .end local v1    # "newBuffer":[C
    :cond_1
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v5, v5

    if-ne v4, v5, :cond_2

    .line 82
    iput v8, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    .line 85
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    iget v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    iget v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    sub-int v3, v4, v5

    .line 86
    .local v3, "toRead":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->reader:Ljava/io/Reader;

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    iget v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    invoke-virtual {v4, v5, v6, v3}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 87
    .local v2, "readCount":I
    if-ne v2, v0, :cond_3

    .line 88
    const/4 v4, 0x1

    iput-boolean v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->end:Z

    goto :goto_0

    .line 91
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    aget-char v0, v4, v5

    .line 92
    .local v0, "ch":I
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    .line 93
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    .line 94
    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    goto :goto_0

    .line 98
    .end local v0    # "ch":I
    .end local v2    # "readCount":I
    .end local v3    # "toRead":I
    :cond_4
    sget-boolean v4, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    if-lt p1, v4, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 101
    :cond_5
    sget-boolean v4, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    iget v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    sub-int/2addr v4, p1

    iget v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    if-le v4, v5, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "nextPos="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pos="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " count="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 103
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->getIndex(I)I

    move-result v5

    aget-char v0, v4, v5

    goto/16 :goto_0
.end method

.method public get(II)[C
    .locals 8
    .param p1, "posStart"    # I
    .param p2, "length"    # I

    .prologue
    const/4 v7, 0x0

    .line 123
    sget-boolean v4, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 124
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->inBounds(I)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "posStart="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 127
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->getIndex(I)I

    move-result v3

    .line 128
    .local v3, "startIndex":I
    add-int v4, p1, p2

    invoke-direct {p0, v4}, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->getIndex(I)I

    move-result v0

    .line 131
    .local v0, "endIndex":I
    new-array v2, p2, [C

    .line 132
    .local v2, "result":[C
    if-lt v0, v3, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    if-ge p2, v4, :cond_2

    .line 133
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    sub-int v5, v0, v3

    invoke-static {v4, v3, v2, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    :goto_0
    return-object v2

    .line 136
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v4, v4

    sub-int v1, v4, v3

    .line 137
    .local v1, "part1":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    invoke-static {v4, v3, v2, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 138
    iget-object v4, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->buffer:[C

    array-length v5, v5

    sub-int/2addr v5, v3

    sub-int v6, p2, v1

    invoke-static {v4, v7, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public reset(Ljava/io/Reader;)V
    .locals 1
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    const/4 v0, 0x0

    .line 54
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->reader:Ljava/io/Reader;

    .line 55
    iput v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextPos:I

    .line 56
    iput v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->nextWrite:I

    .line 57
    iput v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->count:I

    .line 58
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/util/RollingCharBuffer;->end:Z

    .line 59
    return-void
.end method
