.class public Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;
.super Ljava/lang/Object;
.source "NormalizeCharMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final pendingPairs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->pendingPairs:Ljava/util/Map;

    .line 79
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "match"    # Ljava/lang/String;
    .param p2, "replacement"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot match the empty string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->pendingPairs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "match \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" was already added"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->pendingPairs:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method

.method public build()Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;
    .locals 10

    .prologue
    .line 110
    :try_start_0
    invoke-static {}, Lorg/apache/lucene/util/fst/CharSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/CharSequenceOutputs;

    move-result-object v4

    .line 111
    .local v4, "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<Lorg/apache/lucene/util/CharsRef;>;"
    new-instance v0, Lorg/apache/lucene/util/fst/Builder;

    sget-object v6, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE2:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    invoke-direct {v0, v6, v4}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 112
    .local v0, "builder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/CharsRef;>;"
    new-instance v5, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 113
    .local v5, "scratch":Lorg/apache/lucene/util/IntsRef;
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->pendingPairs:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 117
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    move-result-object v3

    .line 118
    .local v3, "map":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/CharsRef;>;"
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->pendingPairs:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    new-instance v6, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    const/4 v7, 0x0

    invoke-direct {v6, v3, v7}, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;-><init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;)V

    return-object v6

    .line 113
    .end local v3    # "map":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/CharsRef;>;"
    :cond_0
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 114
    .local v1, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-static {v6, v5}, Lorg/apache/lucene/util/fst/Util;->toUTF16(Ljava/lang/CharSequence;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v8

    .line 115
    new-instance v9, Lorg/apache/lucene/util/CharsRef;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v9, v6}, Lorg/apache/lucene/util/CharsRef;-><init>(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v0, v8, v9}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 119
    .end local v0    # "builder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/CharsRef;>;"
    .end local v1    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "outputs":Lorg/apache/lucene/util/fst/Outputs;, "Lorg/apache/lucene/util/fst/Outputs<Lorg/apache/lucene/util/CharsRef;>;"
    .end local v5    # "scratch":Lorg/apache/lucene/util/IntsRef;
    :catch_0
    move-exception v2

    .line 121
    .local v2, "ioe":Ljava/io/IOException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method
