.class public Lorg/apache/lucene/analysis/payloads/FloatEncoder;
.super Lorg/apache/lucene/analysis/payloads/AbstractEncoder;
.source "FloatEncoder.java"

# interfaces
.implements Lorg/apache/lucene/analysis/payloads/PayloadEncoder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/analysis/payloads/AbstractEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public encode([CII)Lorg/apache/lucene/util/BytesRef;
    .locals 4
    .param p1, "buffer"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 32
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 33
    .local v1, "payload":F
    invoke-static {v1}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeFloat(F)[B

    move-result-object v0

    .line 34
    .local v0, "bytes":[B
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2, v0}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 35
    .local v2, "result":Lorg/apache/lucene/util/BytesRef;
    return-object v2
.end method
