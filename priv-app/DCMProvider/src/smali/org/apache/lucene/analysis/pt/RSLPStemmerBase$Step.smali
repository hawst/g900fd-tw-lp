.class public Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;
.super Ljava/lang/Object;
.source "RSLPStemmerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "Step"
.end annotation


# instance fields
.field protected final min:I

.field protected final name:Ljava/lang/String;

.field protected final rules:[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

.field protected final suffixes:[[C


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;I[Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "rules"    # [Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
    .param p3, "min"    # I
    .param p4, "suffixes"    # [Ljava/lang/String;

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-object p1, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->name:Ljava/lang/String;

    .line 197
    iput-object p2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->rules:[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    .line 198
    if-nez p3, :cond_0

    .line 199
    const p3, 0x7fffffff

    .line 200
    array-length v3, p2

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_3

    .line 203
    :cond_0
    iput p3, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->min:I

    .line 205
    if-eqz p4, :cond_1

    array-length v2, p4

    if-nez v2, :cond_4

    .line 206
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->suffixes:[[C

    .line 212
    :cond_2
    return-void

    .line 200
    :cond_3
    aget-object v1, p2, v2

    .line 201
    .local v1, "r":Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
    iget v4, v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->min:I

    iget-object v5, v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->suffix:[C

    array-length v5, v5

    add-int/2addr v4, v5

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 200
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 208
    .end local v1    # "r":Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;
    :cond_4
    array-length v2, p4

    new-array v2, v2, [[C

    iput-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->suffixes:[[C

    .line 209
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p4

    if-ge v0, v2, :cond_2

    .line 210
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->suffixes:[[C

    aget-object v3, p4, v0

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    aput-object v3, v2, v0

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public apply([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 218
    iget v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->min:I

    if-ge p2, v2, :cond_1

    .line 238
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 221
    .restart local p2    # "len":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->suffixes:[[C

    if-eqz v2, :cond_2

    .line 222
    const/4 v0, 0x0

    .line 224
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->suffixes:[[C

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 230
    :goto_2
    if-eqz v0, :cond_0

    .line 233
    .end local v0    # "found":Z
    .end local v1    # "i":I
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->rules:[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 234
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->rules:[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->matches([CI)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 235
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->rules:[Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Rule;->replace([CI)I

    move-result p2

    goto :goto_0

    .line 225
    .restart local v0    # "found":Z
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->suffixes:[[C

    aget-object v2, v2, v1

    invoke-static {p1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CI[C)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 226
    const/4 v0, 0x1

    .line 227
    goto :goto_2

    .line 224
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 233
    .end local v0    # "found":Z
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method
