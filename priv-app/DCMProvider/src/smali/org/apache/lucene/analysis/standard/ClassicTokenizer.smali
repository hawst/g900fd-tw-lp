.class public final Lorg/apache/lucene/analysis/standard/ClassicTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "ClassicTokenizer.java"


# static fields
.field public static final ACRONYM:I = 0x2

.field public static final ACRONYM_DEP:I = 0x8

.field public static final ALPHANUM:I = 0x0

.field public static final APOSTROPHE:I = 0x1

.field public static final CJ:I = 0x7

.field public static final COMPANY:I = 0x3

.field public static final EMAIL:I = 0x4

.field public static final HOST:I = 0x5

.field public static final NUM:I = 0x6

.field public static final TOKEN_TYPES:[Ljava/lang/String;


# instance fields
.field private maxTokenLength:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 67
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 68
    const-string v2, "<ALPHANUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 69
    const-string v2, "<APOSTROPHE>"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 70
    const-string v2, "<ACRONYM>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 71
    const-string v2, "<COMPANY>"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 72
    const-string v2, "<EMAIL>"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 73
    const-string v2, "<HOST>"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 74
    const-string v2, "<NUM>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 75
    const-string v2, "<CJ>"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 76
    const-string v2, "<ACRONYM_DEP>"

    aput-object v2, v0, v1

    .line 67
    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    .line 77
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 101
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 79
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 119
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 120
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 121
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 122
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 102
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    .line 109
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 79
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 119
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 120
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 121
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 122
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 110
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->init(Lorg/apache/lucene/util/Version;)V

    .line 111
    return-void
.end method

.method private init(Lorg/apache/lucene/util/Version;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 114
    new-instance v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    .line 115
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 164
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->correctOffset(I)I

    move-result v0

    .line 165
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 166
    return-void
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    return v0
.end method

.method public final incrementToken()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->clearAttributes()V

    .line 132
    const/4 v0, 0x1

    .line 135
    .local v0, "posIncr":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getNextToken()I

    move-result v2

    .line 137
    .local v2, "tokenType":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 138
    const/4 v3, 0x0

    .line 153
    :goto_1
    return v3

    .line 141
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yylength()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    if-gt v3, v4, :cond_2

    .line 142
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 143
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V

    .line 144
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yychar()I

    move-result v1

    .line 145
    .local v1, "start":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->correctOffset(I)I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->correctOffset(I)I

    move-result v5

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 147
    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 148
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 149
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 153
    :goto_2
    const/4 v3, 0x1

    goto :goto_1

    .line 151
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    sget-object v4, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    goto :goto_2

    .line 157
    .end local v1    # "start":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 134
    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->scanner:Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->input:Ljava/io/Reader;

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;->yyreset(Ljava/io/Reader;)V

    .line 171
    return-void
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 84
    iput p1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->maxTokenLength:I

    .line 85
    return-void
.end method
