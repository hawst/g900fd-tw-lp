.class public Lorg/apache/lucene/analysis/no/NorwegianLightStemmer;
.super Ljava/lang/Object;
.source "NorwegianLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 5
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x5

    const/4 v2, 0x4

    .line 68
    if-le p2, v2, :cond_0

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_0

    .line 69
    add-int/lit8 p2, p2, -0x1

    .line 72
    :cond_0
    if-le p2, v4, :cond_3

    .line 73
    const-string v0, "heter"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    const-string v0, "heten"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    :cond_1
    add-int/lit8 p2, p2, -0x5

    .line 117
    .end local p2    # "len":I
    :cond_2
    :goto_0
    return p2

    .line 77
    .restart local p2    # "len":I
    :cond_3
    if-le p2, v3, :cond_5

    .line 78
    const-string v0, "dom"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 79
    const-string v0, "het"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 80
    :cond_4
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 82
    :cond_5
    if-le p2, v4, :cond_7

    .line 83
    const-string v0, "elser"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 84
    const-string v0, "elsen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 85
    :cond_6
    add-int/lit8 p2, p2, -0x5

    goto :goto_0

    .line 87
    :cond_7
    const/4 v0, 0x6

    if-le p2, v0, :cond_9

    .line 88
    const-string v0, "ende"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 89
    const-string v0, "else"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 90
    const-string v0, "este"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 91
    const-string v0, "eren"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 92
    :cond_8
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 94
    :cond_9
    if-le p2, v3, :cond_b

    .line 95
    const-string v0, "ere"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 96
    const-string v0, "est"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 97
    const-string v0, "ene"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 99
    :cond_a
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 101
    :cond_b
    if-le p2, v2, :cond_d

    .line 102
    const-string v0, "er"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 103
    const-string v0, "en"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 104
    const-string v0, "et"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 105
    const-string v0, "st"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 106
    const-string v0, "te"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 107
    :cond_c
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 109
    :cond_d
    const/4 v0, 0x3

    if-le p2, v0, :cond_2

    .line 110
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 114
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 110
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x6e -> :sswitch_0
    .end sparse-switch
.end method
