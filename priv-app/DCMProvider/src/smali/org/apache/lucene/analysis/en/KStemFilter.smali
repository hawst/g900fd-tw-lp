.class public final Lorg/apache/lucene/analysis/en/KStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "KStemFilter.java"


# instance fields
.field private final keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final stemmer:Lorg/apache/lucene/analysis/en/KStemmer;

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 50
    new-instance v0, Lorg/apache/lucene/analysis/en/KStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/en/KStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/KStemmer;

    .line 51
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/en/KStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 52
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/en/KStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 56
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    const/4 v2, 0x0

    .line 73
    :goto_0
    return v2

    .line 67
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    .line 68
    .local v1, "term":[C
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 69
    .local v0, "len":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/KStemmer;

    invoke-virtual {v2, v1, v0}, Lorg/apache/lucene/analysis/en/KStemmer;->stem([CI)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/en/KStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/KStemmer;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/en/KStemmer;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 73
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
