.class public Lorg/apache/lucene/analysis/standard/StandardFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "StandardFilter.java"


# static fields
.field private static final ACRONYM_TYPE:Ljava/lang/String;

.field private static final APOSTROPHE_TYPE:Ljava/lang/String;


# instance fields
.field private final matchVersion:Lorg/apache/lucene/util/Version;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/StandardFilter;->APOSTROPHE_TYPE:Ljava/lang/String;

    .line 40
    sget-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/standard/StandardFilter;->ACRONYM_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 35
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 43
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 44
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/standard/StandardFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 37
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    .line 51
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/StandardFilter;->incrementTokenClassic()Z

    move-result v0

    goto :goto_0
.end method

.method public final incrementTokenClassic()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v7

    if-nez v7, :cond_0

    .line 56
    const/4 v7, 0x0

    .line 79
    :goto_0
    return v7

    .line 59
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 60
    .local v0, "buffer":[C
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 61
    .local v1, "bufferLength":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v4

    .line 63
    .local v4, "type":Ljava/lang/String;
    sget-object v7, Lorg/apache/lucene/analysis/standard/StandardFilter;->APOSTROPHE_TYPE:Ljava/lang/String;

    if-ne v4, v7, :cond_3

    .line 64
    const/4 v7, 0x2

    if-lt v1, v7, :cond_3

    .line 65
    add-int/lit8 v7, v1, -0x2

    aget-char v7, v0, v7

    const/16 v8, 0x27

    if-ne v7, v8, :cond_3

    .line 66
    add-int/lit8 v7, v1, -0x1

    aget-char v7, v0, v7

    const/16 v8, 0x73

    if-eq v7, v8, :cond_1

    add-int/lit8 v7, v1, -0x1

    aget-char v7, v0, v7

    const/16 v8, 0x53

    if-ne v7, v8, :cond_3

    .line 68
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v8, v1, -0x2

    invoke-interface {v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 79
    :cond_2
    :goto_1
    const/4 v7, 0x1

    goto :goto_0

    .line 69
    :cond_3
    sget-object v7, Lorg/apache/lucene/analysis/standard/StandardFilter;->ACRONYM_TYPE:Ljava/lang/String;

    if-ne v4, v7, :cond_2

    .line 70
    const/4 v5, 0x0

    .line 71
    .local v5, "upto":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v6, v5

    .end local v5    # "upto":I
    .local v6, "upto":I
    :goto_2
    if-lt v3, v1, :cond_4

    .line 76
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/StandardFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    goto :goto_1

    .line 72
    :cond_4
    aget-char v2, v0, v3

    .line 73
    .local v2, "c":C
    const/16 v7, 0x2e

    if-eq v2, v7, :cond_5

    .line 74
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    aput-char v2, v0, v6

    .line 71
    :goto_3
    add-int/lit8 v3, v3, 0x1

    move v6, v5

    .end local v5    # "upto":I
    .restart local v6    # "upto":I
    goto :goto_2

    :cond_5
    move v5, v6

    .end local v6    # "upto":I
    .restart local v5    # "upto":I
    goto :goto_3
.end method
