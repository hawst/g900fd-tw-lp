.class public final Lorg/apache/lucene/analysis/core/WhitespaceTokenizer;
.super Lorg/apache/lucene/analysis/util/CharTokenizer;
.source "WhitespaceTokenizer.java"


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/CharTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 66
    return-void
.end method


# virtual methods
.method protected isTokenChar(I)Z
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 72
    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
