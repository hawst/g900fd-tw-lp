.class Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;
.super Ljava/lang/Object;
.source "ClassicTokenizerImpl.java"

# interfaces
.implements Lorg/apache/lucene/analysis/standard/StandardTokenizerInterface;


# static fields
.field public static final ACRONYM:I = 0x2

.field public static final ACRONYM_DEP:I = 0x8

.field public static final ALPHANUM:I = 0x0

.field public static final APOSTROPHE:I = 0x1

.field public static final CJ:I = 0x7

.field public static final COMPANY:I = 0x3

.field public static final EMAIL:I = 0x4

.field public static final HOST:I = 0x5

.field public static final NUM:I = 0x6

.field public static final TOKEN_TYPES:[Ljava/lang/String;

.field public static final YYEOF:I = -0x1

.field public static final YYINITIAL:I = 0x0

.field private static final ZZ_ACTION:[I

.field private static final ZZ_ACTION_PACKED_0:Ljava/lang/String; = "\u0001\u0000\u0001\u0001\u0003\u0002\u0001\u0003\u0001\u0001\u000b\u0000\u0001\u0002\u0003\u0004\u0002\u0000\u0001\u0005\u0001\u0000\u0001\u0005\u0003\u0004\u0006\u0005\u0001\u0006\u0001\u0004\u0002\u0007\u0001\u0008\u0001\u0000\u0001\u0008\u0003\u0000\u0002\u0008\u0001\t\u0001\n\u0001\u0004"

.field private static final ZZ_ATTRIBUTE:[I

.field private static final ZZ_ATTRIBUTE_PACKED_0:Ljava/lang/String; = "\u0001\u0000\u0001\t\u0003\u0001\u0001\t\u0001\u0001\u000b\u0000\u0004\u0001\u0002\u0000\u0001\u0001\u0001\u0000\u000f\u0001\u0001\u0000\u0001\u0001\u0003\u0000\u0005\u0001"

.field private static final ZZ_BUFFERSIZE:I = 0x1000

.field private static final ZZ_CMAP:[C

.field private static final ZZ_CMAP_PACKED:Ljava/lang/String; = "\t\u0000\u0001\u0000\u0001\r\u0001\u0000\u0001\u0000\u0001\u000c\u0012\u0000\u0001\u0000\u0005\u0000\u0001\u0005\u0001\u0003\u0004\u0000\u0001\t\u0001\u0007\u0001\u0004\u0001\t\n\u0002\u0006\u0000\u0001\u0006\u001a\n\u0004\u0000\u0001\u0008\u0001\u0000\u001a\n/\u0000\u0001\n\n\u0000\u0001\n\u0004\u0000\u0001\n\u0005\u0000\u0017\n\u0001\u0000\u001f\n\u0001\u0000\u0128\n\u0002\u0000\u0012\n\u001c\u0000^\n\u0002\u0000\t\n\u0002\u0000\u0007\n\u000e\u0000\u0002\n\u000e\u0000\u0005\n\t\u0000\u0001\n\u008b\u0000\u0001\n\u000b\u0000\u0001\n\u0001\u0000\u0003\n\u0001\u0000\u0001\n\u0001\u0000\u0014\n\u0001\u0000,\n\u0001\u0000\u0008\n\u0002\u0000\u001a\n\u000c\u0000\u0082\n\n\u00009\n\u0002\u0000\u0002\n\u0002\u0000\u0002\n\u0003\u0000&\n\u0002\u0000\u0002\n7\u0000&\n\u0002\u0000\u0001\n\u0007\u0000\'\nH\u0000\u001b\n\u0005\u0000\u0003\n.\u0000\u001a\n\u0005\u0000\u000b\n\u0015\u0000\n\u0002\u0007\u0000c\n\u0001\u0000\u0001\n\u000f\u0000\u0002\n\t\u0000\n\u0002\u0003\n\u0013\u0000\u0001\n\u0001\u0000\u001b\nS\u0000&\n\u015f\u00005\n\u0003\u0000\u0001\n\u0012\u0000\u0001\n\u0007\u0000\n\n\u0004\u0000\n\u0002\u0015\u0000\u0008\n\u0002\u0000\u0002\n\u0002\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0001\n\u0003\u0000\u0004\n\"\u0000\u0002\n\u0001\u0000\u0003\n\u0004\u0000\n\u0002\u0002\n\u0013\u0000\u0006\n\u0004\u0000\u0002\n\u0002\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0002\n\u0001\u0000\u0002\n\u0001\u0000\u0002\n\u001f\u0000\u0004\n\u0001\u0000\u0001\n\u0007\u0000\n\u0002\u0002\u0000\u0003\n\u0010\u0000\u0007\n\u0001\u0000\u0001\n\u0001\u0000\u0003\n\u0001\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0002\n\u0001\u0000\u0005\n\u0003\u0000\u0001\n\u0012\u0000\u0001\n\u000f\u0000\u0001\n\u0005\u0000\n\u0002\u0015\u0000\u0008\n\u0002\u0000\u0002\n\u0002\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0002\n\u0002\u0000\u0004\n\u0003\u0000\u0001\n\u001e\u0000\u0002\n\u0001\u0000\u0003\n\u0004\u0000\n\u0002\u0015\u0000\u0006\n\u0003\u0000\u0003\n\u0001\u0000\u0004\n\u0003\u0000\u0002\n\u0001\u0000\u0001\n\u0001\u0000\u0002\n\u0003\u0000\u0002\n\u0003\u0000\u0003\n\u0003\u0000\u0008\n\u0001\u0000\u0003\n-\u0000\t\u0002\u0015\u0000\u0008\n\u0001\u0000\u0003\n\u0001\u0000\u0017\n\u0001\u0000\n\n\u0001\u0000\u0005\n&\u0000\u0002\n\u0004\u0000\n\u0002\u0015\u0000\u0008\n\u0001\u0000\u0003\n\u0001\u0000\u0017\n\u0001\u0000\n\n\u0001\u0000\u0005\n$\u0000\u0001\n\u0001\u0000\u0002\n\u0004\u0000\n\u0002\u0015\u0000\u0008\n\u0001\u0000\u0003\n\u0001\u0000\u0017\n\u0001\u0000\u0010\n&\u0000\u0002\n\u0004\u0000\n\u0002\u0015\u0000\u0012\n\u0003\u0000\u0018\n\u0001\u0000\t\n\u0001\u0000\u0001\n\u0002\u0000\u0007\n9\u0000\u0001\u00010\n\u0001\u0001\u0002\n\u000c\u0001\u0007\n\t\u0001\n\u0002\'\u0000\u0002\n\u0001\u0000\u0001\n\u0002\u0000\u0002\n\u0001\u0000\u0001\n\u0002\u0000\u0001\n\u0006\u0000\u0004\n\u0001\u0000\u0007\n\u0001\u0000\u0003\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0002\u0000\u0002\n\u0001\u0000\u0004\n\u0001\u0000\u0002\n\t\u0000\u0001\n\u0002\u0000\u0005\n\u0001\u0000\u0001\n\t\u0000\n\u0002\u0002\u0000\u0002\n\"\u0000\u0001\n\u001f\u0000\n\u0002\u0016\u0000\u0008\n\u0001\u0000\"\n\u001d\u0000\u0004\nt\u0000\"\n\u0001\u0000\u0005\n\u0001\u0000\u0002\n\u0015\u0000\n\u0002\u0006\u0000\u0006\nJ\u0000&\n\n\u0000\'\n\t\u0000Z\n\u0005\u0000D\n\u0005\u0000R\n\u0006\u0000\u0007\n\u0001\u0000?\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\'\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u001f\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\u0007\n\u0001\u0000\u0017\n\u0001\u0000\u001f\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\'\n\u0001\u0000\u0013\n\u000e\u0000\t\u0002.\u0000U\n\u000c\u0000\u026c\n\u0002\u0000\u0008\n\n\u0000\u001a\n\u0005\u0000K\n\u0095\u00004\n,\u0000\n\u0002&\u0000\n\u0002\u0006\u0000X\n\u0008\u0000)\n\u0557\u0000\u009c\n\u0004\u0000Z\n\u0006\u0000\u0016\n\u0002\u0000\u0006\n\u0002\u0000&\n\u0002\u0000\u0006\n\u0002\u0000\u0008\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u001f\n\u0002\u00005\n\u0001\u0000\u0007\n\u0001\u0000\u0001\n\u0003\u0000\u0003\n\u0001\u0000\u0007\n\u0003\u0000\u0004\n\u0002\u0000\u0006\n\u0004\u0000\r\n\u0005\u0000\u0003\n\u0001\u0000\u0007\n\u0082\u0000\u0001\n\u0082\u0000\u0001\n\u0004\u0000\u0001\n\u0002\u0000\n\n\u0001\u0000\u0001\n\u0003\u0000\u0005\n\u0006\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0001\u0000\u0003\n\u0001\u0000\u0007\n\u0ecb\u0000\u0002\n*\u0000\u0005\n\n\u0000\u0001\u000bT\u000b\u0008\u000b\u0002\u000b\u0002\u000bZ\u000b\u0001\u000b\u0003\u000b\u0006\u000b(\u000b\u0003\u000b\u0001\u0000^\n\u0011\u0000\u0018\n8\u0000\u0010\u000b\u0100\u0000\u0080\u000b\u0080\u0000\u19b6\u000b\n\u000b@\u0000\u51a6\u000bZ\u000b\u048d\n\u0773\u0000\u2ba4\n\u215c\u0000\u012e\u000b\u00d2\u000b\u0007\n\u000c\u0000\u0005\n\u0005\u0000\u0001\n\u0001\u0000\n\n\u0001\u0000\r\n\u0001\u0000\u0005\n\u0001\u0000\u0001\n\u0001\u0000\u0002\n\u0001\u0000\u0002\n\u0001\u0000l\n!\u0000\u016b\n\u0012\u0000@\n\u0002\u00006\n(\u0000\u000c\nt\u0000\u0003\n\u0001\u0000\u0001\n\u0001\u0000\u0087\n\u0013\u0000\n\u0002\u0007\u0000\u001a\n\u0006\u0000\u001a\n\n\u0000\u0001\u000b:\u000b\u001f\n\u0003\u0000\u0006\n\u0002\u0000\u0006\n\u0002\u0000\u0006\n\u0002\u0000\u0003\n#\u0000"

.field private static final ZZ_ERROR_MSG:[Ljava/lang/String;

.field private static final ZZ_LEXSTATE:[I

.field private static final ZZ_NO_MATCH:I = 0x1

.field private static final ZZ_PUSHBACK_2BIG:I = 0x2

.field private static final ZZ_ROWMAP:[I

.field private static final ZZ_ROWMAP_PACKED_0:Ljava/lang/String; = "\u0000\u0000\u0000\u000e\u0000\u001c\u0000*\u00008\u0000\u000e\u0000F\u0000T\u0000b\u0000p\u0000~\u0000\u008c\u0000\u009a\u0000\u00a8\u0000\u00b6\u0000\u00c4\u0000\u00d2\u0000\u00e0\u0000\u00ee\u0000\u00fc\u0000\u010a\u0000\u0118\u0000\u0126\u0000\u0134\u0000\u0142\u0000\u0150\u0000\u015e\u0000\u016c\u0000\u017a\u0000\u0188\u0000\u0196\u0000\u01a4\u0000\u01b2\u0000\u01c0\u0000\u01ce\u0000\u01dc\u0000\u01ea\u0000\u01f8\u0000\u00d2\u0000\u0206\u0000\u0214\u0000\u0222\u0000\u0230\u0000\u023e\u0000\u024c\u0000\u025a\u0000T\u0000\u008c\u0000\u0268\u0000\u0276\u0000\u0284"

.field private static final ZZ_TRANS:[I

.field private static final ZZ_TRANS_PACKED_0:Ljava/lang/String; = "\u0001\u0002\u0001\u0003\u0001\u0004\u0007\u0002\u0001\u0005\u0001\u0006\u0001\u0007\u0001\u0002\u000f\u0000\u0002\u0003\u0001\u0000\u0001\u0008\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u0003\u0004\u0000\u0001\u0003\u0001\u0004\u0001\u0000\u0001\u000c\u0001\u0000\u0001\t\u0002\r\u0001\u000e\u0001\u0004\u0004\u0000\u0001\u0003\u0001\u0004\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0002\n\u0001\u000b\u0001\u0013\u0010\u0000\u0001\u0002\u0001\u0000\u0001\u0014\u0001\u0015\u0007\u0000\u0001\u0016\u0004\u0000\u0002\u0017\u0007\u0000\u0001\u0017\u0004\u0000\u0001\u0018\u0001\u0019\u0007\u0000\u0001\u001a\u0005\u0000\u0001\u001b\u0007\u0000\u0001\u000b\u0004\u0000\u0001\u001c\u0001\u001d\u0007\u0000\u0001\u001e\u0004\u0000\u0001\u001f\u0001 \u0007\u0000\u0001!\u0004\u0000\u0001\"\u0001#\u0007\u0000\u0001$\r\u0000\u0001%\u0004\u0000\u0001\u0014\u0001\u0015\u0007\u0000\u0001&\r\u0000\u0001\'\u0004\u0000\u0002\u0017\u0007\u0000\u0001(\u0004\u0000\u0001\u0003\u0001\u0004\u0001\u000f\u0001\u0008\u0001\u0011\u0001\u0012\u0002\n\u0001\u000b\u0001\u0013\u0004\u0000\u0002\u0014\u0001\u0000\u0001)\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0014\u0004\u0000\u0001\u0014\u0001\u0015\u0001\u0000\u0001+\u0001\u0000\u0001\t\u0002,\u0001-\u0001\u0015\u0004\u0000\u0001\u0014\u0001\u0015\u0001\u0000\u0001)\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0016\u0004\u0000\u0002\u0017\u0001\u0000\u0001.\u0002\u0000\u0001.\u0002\u0000\u0001\u0017\u0004\u0000\u0002\u0018\u0001\u0000\u0001*\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0018\u0004\u0000\u0001\u0018\u0001\u0019\u0001\u0000\u0001,\u0001\u0000\u0001\t\u0002,\u0001-\u0001\u0019\u0004\u0000\u0001\u0018\u0001\u0019\u0001\u0000\u0001*\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u001a\u0005\u0000\u0001\u001b\u0001\u0000\u0001-\u0002\u0000\u0003-\u0001\u001b\u0004\u0000\u0002\u001c\u0001\u0000\u0001/\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u001c\u0004\u0000\u0001\u001c\u0001\u001d\u0001\u0000\u00010\u0001\u0000\u0001\t\u0002\r\u0001\u000e\u0001\u001d\u0004\u0000\u0001\u001c\u0001\u001d\u0001\u0000\u0001/\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u001e\u0004\u0000\u0002\u001f\u0001\u0000\u0001\n\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u001f\u0004\u0000\u0001\u001f\u0001 \u0001\u0000\u0001\r\u0001\u0000\u0001\t\u0002\r\u0001\u000e\u0001 \u0004\u0000\u0001\u001f\u0001 \u0001\u0000\u0001\n\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001!\u0004\u0000\u0002\"\u0001\u0000\u0001\u000b\u0002\u0000\u0003\u000b\u0001\"\u0004\u0000\u0001\"\u0001#\u0001\u0000\u0001\u000e\u0002\u0000\u0003\u000e\u0001#\u0004\u0000\u0001\"\u0001#\u0001\u0000\u0001\u000b\u0002\u0000\u0003\u000b\u0001$\u0006\u0000\u0001\u000f\u0006\u0000\u0001%\u0004\u0000\u0001\u0014\u0001\u0015\u0001\u0000\u00011\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0016\u0004\u0000\u0002\u0017\u0001\u0000\u0001.\u0002\u0000\u0001.\u0002\u0000\u0001(\u0004\u0000\u0002\u0014\u0007\u0000\u0001\u0014\u0004\u0000\u0002\u0018\u0007\u0000\u0001\u0018\u0004\u0000\u0002\u001c\u0007\u0000\u0001\u001c\u0004\u0000\u0002\u001f\u0007\u0000\u0001\u001f\u0004\u0000\u0002\"\u0007\u0000\u0001\"\u0004\u0000\u00022\u0007\u0000\u00012\u0004\u0000\u0002\u0014\u0007\u0000\u00013\u0004\u0000\u00022\u0001\u0000\u0001.\u0002\u0000\u0001.\u0002\u0000\u00012\u0004\u0000\u0002\u0014\u0001\u0000\u00011\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0014\u0003\u0000"

.field private static final ZZ_UNKNOWN_ERROR:I


# instance fields
.field private yychar:I

.field private yycolumn:I

.field private yyline:I

.field private zzAtBOL:Z

.field private zzAtEOF:Z

.field private zzBuffer:[C

.field private zzCurrentPos:I

.field private zzEOFDone:Z

.field private zzEndRead:I

.field private zzLexicalState:I

.field private zzMarkedPos:I

.field private zzReader:Ljava/io/Reader;

.field private zzStartRead:I

.field private zzState:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 56
    new-array v0, v3, [I

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_LEXSTATE:[I

    .line 126
    const-string v0, "\t\u0000\u0001\u0000\u0001\r\u0001\u0000\u0001\u0000\u0001\u000c\u0012\u0000\u0001\u0000\u0005\u0000\u0001\u0005\u0001\u0003\u0004\u0000\u0001\t\u0001\u0007\u0001\u0004\u0001\t\n\u0002\u0006\u0000\u0001\u0006\u001a\n\u0004\u0000\u0001\u0008\u0001\u0000\u001a\n/\u0000\u0001\n\n\u0000\u0001\n\u0004\u0000\u0001\n\u0005\u0000\u0017\n\u0001\u0000\u001f\n\u0001\u0000\u0128\n\u0002\u0000\u0012\n\u001c\u0000^\n\u0002\u0000\t\n\u0002\u0000\u0007\n\u000e\u0000\u0002\n\u000e\u0000\u0005\n\t\u0000\u0001\n\u008b\u0000\u0001\n\u000b\u0000\u0001\n\u0001\u0000\u0003\n\u0001\u0000\u0001\n\u0001\u0000\u0014\n\u0001\u0000,\n\u0001\u0000\u0008\n\u0002\u0000\u001a\n\u000c\u0000\u0082\n\n\u00009\n\u0002\u0000\u0002\n\u0002\u0000\u0002\n\u0003\u0000&\n\u0002\u0000\u0002\n7\u0000&\n\u0002\u0000\u0001\n\u0007\u0000\'\nH\u0000\u001b\n\u0005\u0000\u0003\n.\u0000\u001a\n\u0005\u0000\u000b\n\u0015\u0000\n\u0002\u0007\u0000c\n\u0001\u0000\u0001\n\u000f\u0000\u0002\n\t\u0000\n\u0002\u0003\n\u0013\u0000\u0001\n\u0001\u0000\u001b\nS\u0000&\n\u015f\u00005\n\u0003\u0000\u0001\n\u0012\u0000\u0001\n\u0007\u0000\n\n\u0004\u0000\n\u0002\u0015\u0000\u0008\n\u0002\u0000\u0002\n\u0002\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0001\n\u0003\u0000\u0004\n\"\u0000\u0002\n\u0001\u0000\u0003\n\u0004\u0000\n\u0002\u0002\n\u0013\u0000\u0006\n\u0004\u0000\u0002\n\u0002\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0002\n\u0001\u0000\u0002\n\u0001\u0000\u0002\n\u001f\u0000\u0004\n\u0001\u0000\u0001\n\u0007\u0000\n\u0002\u0002\u0000\u0003\n\u0010\u0000\u0007\n\u0001\u0000\u0001\n\u0001\u0000\u0003\n\u0001\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0002\n\u0001\u0000\u0005\n\u0003\u0000\u0001\n\u0012\u0000\u0001\n\u000f\u0000\u0001\n\u0005\u0000\n\u0002\u0015\u0000\u0008\n\u0002\u0000\u0002\n\u0002\u0000\u0016\n\u0001\u0000\u0007\n\u0001\u0000\u0002\n\u0002\u0000\u0004\n\u0003\u0000\u0001\n\u001e\u0000\u0002\n\u0001\u0000\u0003\n\u0004\u0000\n\u0002\u0015\u0000\u0006\n\u0003\u0000\u0003\n\u0001\u0000\u0004\n\u0003\u0000\u0002\n\u0001\u0000\u0001\n\u0001\u0000\u0002\n\u0003\u0000\u0002\n\u0003\u0000\u0003\n\u0003\u0000\u0008\n\u0001\u0000\u0003\n-\u0000\t\u0002\u0015\u0000\u0008\n\u0001\u0000\u0003\n\u0001\u0000\u0017\n\u0001\u0000\n\n\u0001\u0000\u0005\n&\u0000\u0002\n\u0004\u0000\n\u0002\u0015\u0000\u0008\n\u0001\u0000\u0003\n\u0001\u0000\u0017\n\u0001\u0000\n\n\u0001\u0000\u0005\n$\u0000\u0001\n\u0001\u0000\u0002\n\u0004\u0000\n\u0002\u0015\u0000\u0008\n\u0001\u0000\u0003\n\u0001\u0000\u0017\n\u0001\u0000\u0010\n&\u0000\u0002\n\u0004\u0000\n\u0002\u0015\u0000\u0012\n\u0003\u0000\u0018\n\u0001\u0000\t\n\u0001\u0000\u0001\n\u0002\u0000\u0007\n9\u0000\u0001\u00010\n\u0001\u0001\u0002\n\u000c\u0001\u0007\n\t\u0001\n\u0002\'\u0000\u0002\n\u0001\u0000\u0001\n\u0002\u0000\u0002\n\u0001\u0000\u0001\n\u0002\u0000\u0001\n\u0006\u0000\u0004\n\u0001\u0000\u0007\n\u0001\u0000\u0003\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0002\u0000\u0002\n\u0001\u0000\u0004\n\u0001\u0000\u0002\n\t\u0000\u0001\n\u0002\u0000\u0005\n\u0001\u0000\u0001\n\t\u0000\n\u0002\u0002\u0000\u0002\n\"\u0000\u0001\n\u001f\u0000\n\u0002\u0016\u0000\u0008\n\u0001\u0000\"\n\u001d\u0000\u0004\nt\u0000\"\n\u0001\u0000\u0005\n\u0001\u0000\u0002\n\u0015\u0000\n\u0002\u0006\u0000\u0006\nJ\u0000&\n\n\u0000\'\n\t\u0000Z\n\u0005\u0000D\n\u0005\u0000R\n\u0006\u0000\u0007\n\u0001\u0000?\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\'\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u001f\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\u0007\n\u0001\u0000\u0017\n\u0001\u0000\u001f\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0002\u0000\u0007\n\u0001\u0000\'\n\u0001\u0000\u0013\n\u000e\u0000\t\u0002.\u0000U\n\u000c\u0000\u026c\n\u0002\u0000\u0008\n\n\u0000\u001a\n\u0005\u0000K\n\u0095\u00004\n,\u0000\n\u0002&\u0000\n\u0002\u0006\u0000X\n\u0008\u0000)\n\u0557\u0000\u009c\n\u0004\u0000Z\n\u0006\u0000\u0016\n\u0002\u0000\u0006\n\u0002\u0000&\n\u0002\u0000\u0006\n\u0002\u0000\u0008\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u001f\n\u0002\u00005\n\u0001\u0000\u0007\n\u0001\u0000\u0001\n\u0003\u0000\u0003\n\u0001\u0000\u0007\n\u0003\u0000\u0004\n\u0002\u0000\u0006\n\u0004\u0000\r\n\u0005\u0000\u0003\n\u0001\u0000\u0007\n\u0082\u0000\u0001\n\u0082\u0000\u0001\n\u0004\u0000\u0001\n\u0002\u0000\n\n\u0001\u0000\u0001\n\u0003\u0000\u0005\n\u0006\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u0001\n\u0001\u0000\u0004\n\u0001\u0000\u0003\n\u0001\u0000\u0007\n\u0ecb\u0000\u0002\n*\u0000\u0005\n\n\u0000\u0001\u000bT\u000b\u0008\u000b\u0002\u000b\u0002\u000bZ\u000b\u0001\u000b\u0003\u000b\u0006\u000b(\u000b\u0003\u000b\u0001\u0000^\n\u0011\u0000\u0018\n8\u0000\u0010\u000b\u0100\u0000\u0080\u000b\u0080\u0000\u19b6\u000b\n\u000b@\u0000\u51a6\u000bZ\u000b\u048d\n\u0773\u0000\u2ba4\n\u215c\u0000\u012e\u000b\u00d2\u000b\u0007\n\u000c\u0000\u0005\n\u0005\u0000\u0001\n\u0001\u0000\n\n\u0001\u0000\r\n\u0001\u0000\u0005\n\u0001\u0000\u0001\n\u0001\u0000\u0002\n\u0001\u0000\u0002\n\u0001\u0000l\n!\u0000\u016b\n\u0012\u0000@\n\u0002\u00006\n(\u0000\u000c\nt\u0000\u0003\n\u0001\u0000\u0001\n\u0001\u0000\u0087\n\u0013\u0000\n\u0002\u0007\u0000\u001a\n\u0006\u0000\u001a\n\n\u0000\u0001\u000b:\u000b\u001f\n\u0003\u0000\u0006\n\u0002\u0000\u0006\n\u0002\u0000\u0006\n\u0002\u0000\u0003\n#\u0000"

    invoke-static {v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackCMap(Ljava/lang/String;)[C

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_CMAP:[C

    .line 131
    invoke-static {}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackAction()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ACTION:[I

    .line 162
    invoke-static {}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackRowMap()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ROWMAP:[I

    .line 194
    invoke-static {}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackTrans()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_TRANS:[I

    .line 265
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 266
    const-string v2, "Unkown internal scanner error"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 267
    const-string v2, "Error: could not match input"

    aput-object v2, v0, v1

    .line 268
    const-string v1, "Error: pushback value was too large"

    aput-object v1, v0, v3

    .line 265
    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ERROR_MSG:[Ljava/lang/String;

    .line 274
    invoke-static {}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackAttribute()[I

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ATTRIBUTE:[I

    .line 360
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->TOKEN_TYPES:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1, "in"    # Ljava/io/Reader;

    .prologue
    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzLexicalState:I

    .line 310
    const/16 v0, 0x1000

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    .line 340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzAtBOL:Z

    .line 385
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzReader:Ljava/io/Reader;

    .line 386
    return-void
.end method

.method private zzRefill()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 419
    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    if-lez v5, :cond_0

    .line 420
    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    .line 421
    iget-object v7, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    .line 422
    iget v8, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    iget v9, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v8, v9

    .line 420
    invoke-static {v5, v6, v7, v3, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 425
    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    iget v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 426
    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    iget v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    .line 427
    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    iget v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    .line 428
    iput v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    .line 432
    :cond_0
    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    array-length v6, v6

    if-lt v5, v6, :cond_1

    .line 434
    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    mul-int/lit8 v5, v5, 0x2

    new-array v1, v5, [C

    .line 435
    .local v1, "newBuffer":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget-object v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    array-length v6, v6

    invoke-static {v5, v3, v1, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 436
    iput-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    .line 440
    .end local v1    # "newBuffer":[C
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzReader:Ljava/io/Reader;

    iget-object v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget v7, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 441
    iget-object v8, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    array-length v8, v8

    iget v9, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    sub-int/2addr v8, v9

    .line 440
    invoke-virtual {v5, v6, v7, v8}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 443
    .local v2, "numRead":I
    if-lez v2, :cond_2

    .line 444
    iget v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    add-int/2addr v4, v2

    iput v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 459
    :goto_0
    return v3

    .line 448
    :cond_2
    if-nez v2, :cond_4

    .line 449
    iget-object v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzReader:Ljava/io/Reader;

    invoke-virtual {v5}, Ljava/io/Reader;->read()I

    move-result v0

    .line 450
    .local v0, "c":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_3

    move v3, v4

    .line 451
    goto :goto_0

    .line 453
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget v5, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    int-to-char v6, v0

    aput-char v6, v4, v5

    goto :goto_0

    .end local v0    # "c":I
    :cond_4
    move v3, v4

    .line 459
    goto :goto_0
.end method

.method private zzScanError(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 570
    :try_start_0
    sget-object v2, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ERROR_MSG:[Ljava/lang/String;

    aget-object v1, v2, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    .local v1, "message":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/Error;

    invoke-direct {v2, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v2

    .line 572
    .end local v1    # "message":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 573
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v2, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ERROR_MSG:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v1, v2, v3

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0
.end method

.method private static zzUnpackAction(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 147
    const/4 v1, 0x0

    .line 148
    .local v1, "i":I
    move v3, p1

    .line 149
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v2, v1

    .line 150
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 155
    return v3

    .line 151
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 152
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 153
    .local v6, "value":I
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackAction()[I
    .locals 3

    .prologue
    .line 140
    const/16 v2, 0x33

    new-array v1, v2, [I

    .line 141
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 142
    .local v0, "offset":I
    const-string v2, "\u0001\u0000\u0001\u0001\u0003\u0002\u0001\u0003\u0001\u0001\u000b\u0000\u0001\u0002\u0003\u0004\u0002\u0000\u0001\u0005\u0001\u0000\u0001\u0005\u0003\u0004\u0006\u0005\u0001\u0006\u0001\u0004\u0002\u0007\u0001\u0008\u0001\u0000\u0001\u0008\u0003\u0000\u0002\u0008\u0001\t\u0001\n\u0001\u0004"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackAction(Ljava/lang/String;I[I)I

    move-result v0

    .line 143
    return-object v1
.end method

.method private static zzUnpackAttribute(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 288
    const/4 v1, 0x0

    .line 289
    .local v1, "i":I
    move v3, p1

    .line 290
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v2, v1

    .line 291
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 296
    return v3

    .line 292
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 293
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 294
    .local v6, "value":I
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackAttribute()[I
    .locals 3

    .prologue
    .line 281
    const/16 v2, 0x33

    new-array v1, v2, [I

    .line 282
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 283
    .local v0, "offset":I
    const-string v2, "\u0001\u0000\u0001\t\u0003\u0001\u0001\t\u0001\u0001\u000b\u0000\u0004\u0001\u0002\u0000\u0001\u0001\u0001\u0000\u000f\u0001\u0001\u0000\u0001\u0001\u0003\u0000\u0005\u0001"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackAttribute(Ljava/lang/String;I[I)I

    move-result v0

    .line 284
    return-object v1
.end method

.method private static zzUnpackCMap(Ljava/lang/String;)[C
    .locals 8
    .param p0, "packed"    # Ljava/lang/String;

    .prologue
    .line 397
    const/high16 v7, 0x10000

    new-array v5, v7, [C

    .line 398
    .local v5, "map":[C
    const/4 v1, 0x0

    .line 399
    .local v1, "i":I
    const/4 v3, 0x0

    .local v3, "j":I
    move v2, v1

    .line 400
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    const/16 v7, 0x482

    if-lt v2, v7, :cond_0

    .line 405
    return-object v5

    .line 401
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 402
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 403
    .local v6, "value":C
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput-char v6, v5, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackRowMap(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 181
    const/4 v1, 0x0

    .line 182
    .local v1, "i":I
    move v3, p1

    .line 183
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v4, v3

    .end local v3    # "j":I
    .local v4, "j":I
    move v2, v1

    .line 184
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 188
    return v4

    .line 185
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    shl-int/lit8 v0, v6, 0x10

    .line 186
    .local v0, "high":I
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "j":I
    .restart local v3    # "j":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    or-int/2addr v6, v0

    aput v6, p2, v4

    move v4, v3

    .end local v3    # "j":I
    .restart local v4    # "j":I
    goto :goto_0
.end method

.method private static zzUnpackRowMap()[I
    .locals 3

    .prologue
    .line 174
    const/16 v2, 0x33

    new-array v1, v2, [I

    .line 175
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 176
    .local v0, "offset":I
    const-string v2, "\u0000\u0000\u0000\u000e\u0000\u001c\u0000*\u00008\u0000\u000e\u0000F\u0000T\u0000b\u0000p\u0000~\u0000\u008c\u0000\u009a\u0000\u00a8\u0000\u00b6\u0000\u00c4\u0000\u00d2\u0000\u00e0\u0000\u00ee\u0000\u00fc\u0000\u010a\u0000\u0118\u0000\u0126\u0000\u0134\u0000\u0142\u0000\u0150\u0000\u015e\u0000\u016c\u0000\u017a\u0000\u0188\u0000\u0196\u0000\u01a4\u0000\u01b2\u0000\u01c0\u0000\u01ce\u0000\u01dc\u0000\u01ea\u0000\u01f8\u0000\u00d2\u0000\u0206\u0000\u0214\u0000\u0222\u0000\u0230\u0000\u023e\u0000\u024c\u0000\u025a\u0000T\u0000\u008c\u0000\u0268\u0000\u0276\u0000\u0284"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackRowMap(Ljava/lang/String;I[I)I

    move-result v0

    .line 177
    return-object v1
.end method

.method private static zzUnpackTrans(Ljava/lang/String;I[I)I
    .locals 7
    .param p0, "packed"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "result"    # [I

    .prologue
    .line 246
    const/4 v1, 0x0

    .line 247
    .local v1, "i":I
    move v3, p1

    .line 248
    .local v3, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .local v5, "l":I
    move v2, v1

    .line 249
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 255
    return v3

    .line 250
    :cond_0
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 251
    .local v0, "count":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 252
    .local v6, "value":I
    add-int/lit8 v6, v6, -0x1

    .line 253
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "j":I
    .local v4, "j":I
    aput v6, p2, v3

    add-int/lit8 v0, v0, -0x1

    if-gtz v0, :cond_1

    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_0

    .end local v3    # "j":I
    .restart local v4    # "j":I
    :cond_1
    move v3, v4

    .end local v4    # "j":I
    .restart local v3    # "j":I
    goto :goto_1
.end method

.method private static zzUnpackTrans()[I
    .locals 3

    .prologue
    .line 239
    const/16 v2, 0x292

    new-array v1, v2, [I

    .line 240
    .local v1, "result":[I
    const/4 v0, 0x0

    .line 241
    .local v0, "offset":I
    const-string v2, "\u0001\u0002\u0001\u0003\u0001\u0004\u0007\u0002\u0001\u0005\u0001\u0006\u0001\u0007\u0001\u0002\u000f\u0000\u0002\u0003\u0001\u0000\u0001\u0008\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u0003\u0004\u0000\u0001\u0003\u0001\u0004\u0001\u0000\u0001\u000c\u0001\u0000\u0001\t\u0002\r\u0001\u000e\u0001\u0004\u0004\u0000\u0001\u0003\u0001\u0004\u0001\u000f\u0001\u0010\u0001\u0011\u0001\u0012\u0002\n\u0001\u000b\u0001\u0013\u0010\u0000\u0001\u0002\u0001\u0000\u0001\u0014\u0001\u0015\u0007\u0000\u0001\u0016\u0004\u0000\u0002\u0017\u0007\u0000\u0001\u0017\u0004\u0000\u0001\u0018\u0001\u0019\u0007\u0000\u0001\u001a\u0005\u0000\u0001\u001b\u0007\u0000\u0001\u000b\u0004\u0000\u0001\u001c\u0001\u001d\u0007\u0000\u0001\u001e\u0004\u0000\u0001\u001f\u0001 \u0007\u0000\u0001!\u0004\u0000\u0001\"\u0001#\u0007\u0000\u0001$\r\u0000\u0001%\u0004\u0000\u0001\u0014\u0001\u0015\u0007\u0000\u0001&\r\u0000\u0001\'\u0004\u0000\u0002\u0017\u0007\u0000\u0001(\u0004\u0000\u0001\u0003\u0001\u0004\u0001\u000f\u0001\u0008\u0001\u0011\u0001\u0012\u0002\n\u0001\u000b\u0001\u0013\u0004\u0000\u0002\u0014\u0001\u0000\u0001)\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0014\u0004\u0000\u0001\u0014\u0001\u0015\u0001\u0000\u0001+\u0001\u0000\u0001\t\u0002,\u0001-\u0001\u0015\u0004\u0000\u0001\u0014\u0001\u0015\u0001\u0000\u0001)\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0016\u0004\u0000\u0002\u0017\u0001\u0000\u0001.\u0002\u0000\u0001.\u0002\u0000\u0001\u0017\u0004\u0000\u0002\u0018\u0001\u0000\u0001*\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0018\u0004\u0000\u0001\u0018\u0001\u0019\u0001\u0000\u0001,\u0001\u0000\u0001\t\u0002,\u0001-\u0001\u0019\u0004\u0000\u0001\u0018\u0001\u0019\u0001\u0000\u0001*\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u001a\u0005\u0000\u0001\u001b\u0001\u0000\u0001-\u0002\u0000\u0003-\u0001\u001b\u0004\u0000\u0002\u001c\u0001\u0000\u0001/\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u001c\u0004\u0000\u0001\u001c\u0001\u001d\u0001\u0000\u00010\u0001\u0000\u0001\t\u0002\r\u0001\u000e\u0001\u001d\u0004\u0000\u0001\u001c\u0001\u001d\u0001\u0000\u0001/\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u001e\u0004\u0000\u0002\u001f\u0001\u0000\u0001\n\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001\u001f\u0004\u0000\u0001\u001f\u0001 \u0001\u0000\u0001\r\u0001\u0000\u0001\t\u0002\r\u0001\u000e\u0001 \u0004\u0000\u0001\u001f\u0001 \u0001\u0000\u0001\n\u0001\u0000\u0001\t\u0002\n\u0001\u000b\u0001!\u0004\u0000\u0002\"\u0001\u0000\u0001\u000b\u0002\u0000\u0003\u000b\u0001\"\u0004\u0000\u0001\"\u0001#\u0001\u0000\u0001\u000e\u0002\u0000\u0003\u000e\u0001#\u0004\u0000\u0001\"\u0001#\u0001\u0000\u0001\u000b\u0002\u0000\u0003\u000b\u0001$\u0006\u0000\u0001\u000f\u0006\u0000\u0001%\u0004\u0000\u0001\u0014\u0001\u0015\u0001\u0000\u00011\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0016\u0004\u0000\u0002\u0017\u0001\u0000\u0001.\u0002\u0000\u0001.\u0002\u0000\u0001(\u0004\u0000\u0002\u0014\u0007\u0000\u0001\u0014\u0004\u0000\u0002\u0018\u0007\u0000\u0001\u0018\u0004\u0000\u0002\u001c\u0007\u0000\u0001\u001c\u0004\u0000\u0002\u001f\u0007\u0000\u0001\u001f\u0004\u0000\u0002\"\u0007\u0000\u0001\"\u0004\u0000\u00022\u0007\u0000\u00012\u0004\u0000\u0002\u0014\u0007\u0000\u00013\u0004\u0000\u00022\u0001\u0000\u0001.\u0002\u0000\u0001.\u0002\u0000\u00012\u0004\u0000\u0002\u0014\u0001\u0000\u00011\u0001\u0000\u0001\t\u0002*\u0001\u0000\u0001\u0014\u0003\u0000"

    invoke-static {v2, v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzUnpackTrans(Ljava/lang/String;I[I)I

    move-result v0

    .line 242
    return-object v1
.end method


# virtual methods
.method public getNextToken()I
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 611
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 612
    .local v9, "zzEndReadL":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    .line 613
    .local v5, "zzBufferL":[C
    sget-object v6, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_CMAP:[C

    .line 615
    .local v6, "zzCMapL":[C
    sget-object v14, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_TRANS:[I

    .line 616
    .local v14, "zzTransL":[I
    sget-object v13, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ROWMAP:[I

    .line 617
    .local v13, "zzRowMapL":[I
    sget-object v3, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ATTRIBUTE:[I

    .line 620
    .local v3, "zzAttrL":[I
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    .line 622
    .local v11, "zzMarkedPosL":I
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yychar:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    move/from16 v16, v0

    sub-int v16, v11, v16

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yychar:I

    .line 624
    const/4 v2, -0x1

    .line 626
    .local v2, "zzAction":I
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    move v7, v11

    .line 628
    .local v7, "zzCurrentPosL":I
    sget-object v15, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_LEXSTATE:[I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzLexicalState:I

    move/from16 v16, v0

    aget v15, v15, v16

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    .line 631
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    aget v4, v3, v15

    .line 632
    .local v4, "zzAttributes":I
    and-int/lit8 v15, v4, 0x1

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 633
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    move v8, v7

    .line 640
    .end local v7    # "zzCurrentPosL":I
    .local v8, "zzCurrentPosL":I
    :goto_1
    if-ge v8, v9, :cond_1

    .line 641
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    aget-char v10, v5, v8

    .line 664
    .local v10, "zzInput":I
    :goto_2
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    aget v15, v13, v15

    aget-char v16, v6, v10

    add-int v15, v15, v16

    aget v12, v14, v15

    .line 665
    .local v12, "zzNext":I
    const/4 v15, -0x1

    if-ne v12, v15, :cond_4

    .line 679
    .end local v12    # "zzNext":I
    :cond_0
    :goto_3
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    .line 681
    if-gez v2, :cond_6

    .end local v2    # "zzAction":I
    :goto_4
    packed-switch v2, :pswitch_data_0

    .line 723
    const/4 v15, -0x1

    if-ne v10, v15, :cond_7

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ne v15, v0, :cond_7

    .line 724
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzAtEOF:Z

    .line 725
    const/4 v15, -0x1

    :goto_5
    return v15

    .line 642
    .end local v7    # "zzCurrentPosL":I
    .end local v10    # "zzInput":I
    .restart local v2    # "zzAction":I
    .restart local v8    # "zzCurrentPosL":I
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzAtEOF:Z

    if-eqz v15, :cond_2

    .line 643
    const/4 v10, -0x1

    .restart local v10    # "zzInput":I
    move v7, v8

    .line 644
    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    goto :goto_3

    .line 648
    .end local v7    # "zzCurrentPosL":I
    .end local v10    # "zzInput":I
    .restart local v8    # "zzCurrentPosL":I
    :cond_2
    move-object/from16 v0, p0

    iput v8, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    .line 649
    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    .line 650
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzRefill()Z

    move-result v1

    .line 652
    .local v1, "eof":Z
    move-object/from16 v0, p0

    iget v7, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    .line 653
    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    .line 654
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    .line 655
    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 656
    if-eqz v1, :cond_3

    .line 657
    const/4 v10, -0x1

    .line 658
    .restart local v10    # "zzInput":I
    goto :goto_3

    .line 661
    .end local v10    # "zzInput":I
    :cond_3
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "zzCurrentPosL":I
    .restart local v8    # "zzCurrentPosL":I
    aget-char v10, v5, v7

    .restart local v10    # "zzInput":I
    move v7, v8

    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    goto :goto_2

    .line 666
    .end local v1    # "eof":Z
    .restart local v12    # "zzNext":I
    :cond_4
    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    .line 668
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    aget v4, v3, v15

    .line 669
    and-int/lit8 v15, v4, 0x1

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 670
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzState:I

    .line 671
    move v11, v7

    .line 672
    and-int/lit8 v15, v4, 0x8

    const/16 v16, 0x8

    move/from16 v0, v16

    if-eq v15, v0, :cond_0

    .end local v10    # "zzInput":I
    .end local v12    # "zzNext":I
    :cond_5
    move v8, v7

    .end local v7    # "zzCurrentPosL":I
    .restart local v8    # "zzCurrentPosL":I
    goto/16 :goto_1

    .line 681
    .end local v8    # "zzCurrentPosL":I
    .restart local v7    # "zzCurrentPosL":I
    .restart local v10    # "zzInput":I
    :cond_6
    sget-object v15, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->ZZ_ACTION:[I

    aget v2, v15, v2

    goto :goto_4

    .line 687
    .end local v2    # "zzAction":I
    :pswitch_1
    const/4 v15, 0x0

    goto :goto_5

    .line 691
    :pswitch_2
    const/4 v15, 0x7

    goto :goto_5

    .line 695
    :pswitch_3
    const/4 v15, 0x5

    goto :goto_5

    .line 699
    :pswitch_4
    const/4 v15, 0x6

    goto :goto_5

    .line 703
    :pswitch_5
    const/4 v15, 0x1

    goto :goto_5

    .line 707
    :pswitch_6
    const/4 v15, 0x3

    goto :goto_5

    .line 711
    :pswitch_7
    const/16 v15, 0x8

    goto :goto_5

    .line 715
    :pswitch_8
    const/4 v15, 0x2

    goto :goto_5

    .line 719
    :pswitch_9
    const/4 v15, 0x4

    goto :goto_5

    .line 728
    :cond_7
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzScanError(I)V

    goto/16 :goto_0

    .line 681
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final getText(Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;)V
    .locals 4
    .param p1, "t"    # Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .prologue
    .line 373
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    iget v2, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    iget v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v2, v3

    invoke-interface {p1, v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 374
    return-void
.end method

.method public final yybegin(I)V
    .locals 0
    .param p1, "newState"    # I

    .prologue
    .line 516
    iput p1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzLexicalState:I

    .line 517
    return-void
.end method

.method public final yychar()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yychar:I

    return v0
.end method

.method public final yycharat(I)C
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 540
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    add-int/2addr v1, p1

    aget-char v0, v0, v1

    return v0
.end method

.method public final yyclose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 467
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzAtEOF:Z

    .line 468
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 470
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzReader:Ljava/io/Reader;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzReader:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 472
    :cond_0
    return-void
.end method

.method public final yylength()I
    .locals 2

    .prologue
    .line 549
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    iget v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public yypushback(I)V
    .locals 1
    .param p1, "number"    # I

    .prologue
    .line 589
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yylength()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 590
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzScanError(I)V

    .line 592
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    .line 593
    return-void
.end method

.method public final yyreset(Ljava/io/Reader;)V
    .locals 3
    .param p1, "reader"    # Ljava/io/Reader;

    .prologue
    const/16 v2, 0x1000

    const/4 v1, 0x0

    .line 489
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzReader:Ljava/io/Reader;

    .line 490
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzAtBOL:Z

    .line 491
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzAtEOF:Z

    .line 492
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEOFDone:Z

    .line 493
    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzEndRead:I

    .line 494
    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzCurrentPos:I

    .line 495
    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yycolumn:I

    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yychar:I

    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->yyline:I

    .line 496
    iput v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzLexicalState:I

    .line 497
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    array-length v0, v0

    if-le v0, v2, :cond_0

    .line 498
    new-array v0, v2, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    .line 499
    :cond_0
    return-void
.end method

.method public final yystate()I
    .locals 1

    .prologue
    .line 506
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzLexicalState:I

    return v0
.end method

.method public final yytext()Ljava/lang/String;
    .locals 5

    .prologue
    .line 524
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzBuffer:[C

    iget v2, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    iget v3, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzMarkedPos:I

    iget v4, p0, Lorg/apache/lucene/analysis/standard/ClassicTokenizerImpl;->zzStartRead:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method
