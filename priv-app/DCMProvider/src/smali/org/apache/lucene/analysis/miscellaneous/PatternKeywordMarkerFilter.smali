.class public final Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;
.super Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilter;
.source "PatternKeywordMarkerFilter.java"


# instance fields
.field private final matcher:Ljava/util/regex/Matcher;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method protected constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/regex/Pattern;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/miscellaneous/KeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 32
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 47
    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;->matcher:Ljava/util/regex/Matcher;

    .line 48
    return-void
.end method


# virtual methods
.method protected isKeyword()Z
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;->matcher:Ljava/util/regex/Matcher;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 53
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternKeywordMarkerFilter;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method
