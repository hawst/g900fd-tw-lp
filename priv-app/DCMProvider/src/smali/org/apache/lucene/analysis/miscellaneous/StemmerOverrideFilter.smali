.class public final Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "StemmerOverrideFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$Builder;,
        Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;
    }
.end annotation


# instance fields
.field private final fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private final keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final spare:Lorg/apache/lucene/util/CharsRef;

.field private final stemmerOverrideMap:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "stemmerOverrideMap"    # Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 44
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 45
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 47
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 48
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    .line 60
    iput-object p2, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->stemmerOverrideMap:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    .line 61
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 62
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 66
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 67
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    if-nez v3, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v2

    .line 71
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v3

    if-nez v3, :cond_0

    .line 72
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->stemmerOverrideMap:Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v7, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v3, v4, v5, v6, v7}, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->get([CILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 73
    .local v1, "stem":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_0

    .line 74
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    iput-object v0, v3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 75
    .local v0, "buffer":[C
    iget-object v3, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v5, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v3, v4, v5, v6}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 76
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    iget-object v3, v3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    if-eq v3, v0, :cond_2

    .line 77
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    iget v5, v5, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    iget v6, v6, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-interface {v3, v4, v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 79
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->spare:Lorg/apache/lucene/util/CharsRef;

    iget v4, v4, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 80
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v3, v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->setKeyword(Z)V

    goto :goto_0

    .line 85
    .end local v0    # "buffer":[C
    .end local v1    # "stem":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
