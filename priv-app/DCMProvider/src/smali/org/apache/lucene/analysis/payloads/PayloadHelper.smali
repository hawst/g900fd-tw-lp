.class public Lorg/apache/lucene/analysis/payloads/PayloadHelper;
.super Ljava/lang/Object;
.source "PayloadHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeFloat([B)F
    .locals 1
    .param p0, "bytes"    # [B

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->decodeFloat([BI)F

    move-result v0

    return v0
.end method

.method public static final decodeFloat([BI)F
    .locals 1
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 66
    invoke-static {p0, p1}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->decodeInt([BI)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public static final decodeInt([BI)I
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 70
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 71
    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    .line 70
    or-int/2addr v0, v1

    .line 71
    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    .line 70
    or-int/2addr v0, v1

    return v0
.end method

.method public static encodeFloat(F)[B
    .locals 2
    .param p0, "payload"    # F

    .prologue
    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeFloat(F[BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static encodeFloat(F[BI)[B
    .locals 1
    .param p0, "payload"    # F
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 31
    invoke-static {p0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v0, p1, p2}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeInt(I[BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static encodeInt(I)[B
    .locals 2
    .param p0, "payload"    # I

    .prologue
    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/analysis/payloads/PayloadHelper;->encodeInt(I[BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static encodeInt(I[BI)[B
    .locals 2
    .param p0, "payload"    # I
    .param p1, "data"    # [B
    .param p2, "offset"    # I

    .prologue
    .line 39
    shr-int/lit8 v0, p0, 0x18

    int-to-byte v0, v0

    aput-byte v0, p1, p2

    .line 40
    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p0, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 41
    add-int/lit8 v0, p2, 0x2

    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 42
    add-int/lit8 v0, p2, 0x3

    int-to-byte v1, p0

    aput-byte v1, p1, v0

    .line 43
    return-object p1
.end method
