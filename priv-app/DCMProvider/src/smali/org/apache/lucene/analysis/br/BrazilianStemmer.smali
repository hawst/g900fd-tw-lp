.class public Lorg/apache/lucene/analysis/br/BrazilianStemmer;
.super Ljava/lang/Object;
.source "BrazilianStemmer.java"


# static fields
.field private static final locale:Ljava/util/Locale;


# instance fields
.field private CT:Ljava/lang/String;

.field private R1:Ljava/lang/String;

.field private R2:Ljava/lang/String;

.field private RV:Ljava/lang/String;

.field private TERM:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->locale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method private changeTerm(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 242
    const-string v1, ""

    .line 245
    .local v1, "r":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 246
    const/4 v2, 0x0

    .line 282
    :goto_0
    return-object v2

    .line 249
    :cond_0
    sget-object v2, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 250
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_1

    move-object v2, v1

    .line 282
    goto :goto_0

    .line 251
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xe1

    if-eq v2, v3, :cond_2

    .line 252
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xe2

    if-eq v2, v3, :cond_2

    .line 253
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xe3

    if-ne v2, v3, :cond_3

    .line 254
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 250
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 256
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xe9

    if-eq v2, v3, :cond_4

    .line 257
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xea

    if-ne v2, v3, :cond_5

    .line 258
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "e"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 260
    :cond_5
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xed

    if-ne v2, v3, :cond_6

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "i"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 263
    :cond_6
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xf3

    if-eq v2, v3, :cond_7

    .line 264
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xf4

    if-eq v2, v3, :cond_7

    .line 265
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xf5

    if-ne v2, v3, :cond_8

    .line 266
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "o"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 268
    :cond_8
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xfa

    if-eq v2, v3, :cond_9

    .line 269
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xfc

    if-ne v2, v3, :cond_a

    .line 270
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "u"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 272
    :cond_a
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xe7

    if-ne v2, v3, :cond_b

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "c"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 275
    :cond_b
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xf1

    if-ne v2, v3, :cond_c

    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 279
    :cond_c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2
.end method

.method private createCT(Ljava/lang/String;)V
    .locals 7
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x27

    const/16 v5, 0x22

    const/16 v4, 0x21

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 365
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->changeTerm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    .line 367
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v3, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v5, :cond_2

    .line 371
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v6, :cond_2

    .line 372
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_2

    .line 373
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_2

    .line 374
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_2

    .line 375
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_2

    .line 376
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_2

    .line 377
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v4, :cond_3

    .line 379
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    .line 382
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 385
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_4

    .line 386
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_4

    .line 387
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_4

    .line 388
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_4

    .line 389
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_4

    .line 390
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v4, :cond_4

    .line 391
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v6, :cond_4

    .line 392
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v5, :cond_0

    .line 394
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private getR1(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 132
    if-nez p1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-object v2

    .line 137
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .line 138
    .local v0, "i":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-lt v1, v0, :cond_4

    .line 144
    :cond_2
    if-ge v1, v0, :cond_0

    .line 149
    :goto_2
    if-lt v1, v0, :cond_5

    .line 155
    :cond_3
    if-ge v1, v0, :cond_0

    .line 159
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 139
    :cond_4
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-nez v3, :cond_2

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 150
    :cond_5
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private getRV(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 184
    if-nez p1, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-object v2

    .line 188
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .line 192
    .local v0, "i":I
    if-lez v0, :cond_4

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-nez v3, :cond_4

    .line 194
    const/4 v1, 0x2

    .local v1, "j":I
    :goto_1
    if-lt v1, v0, :cond_3

    .line 200
    :cond_2
    if-ge v1, v0, :cond_4

    .line 201
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 195
    :cond_3
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-nez v3, :cond_2

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 208
    .end local v1    # "j":I
    :cond_4
    if-le v0, v4, :cond_7

    .line 209
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 210
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 212
    const/4 v1, 0x2

    .restart local v1    # "j":I
    :goto_2
    if-lt v1, v0, :cond_6

    .line 218
    :cond_5
    if-ge v1, v0, :cond_7

    .line 219
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 213
    :cond_6
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isVowel(C)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 225
    .end local v1    # "j":I
    :cond_7
    const/4 v3, 0x2

    if-le v0, v3, :cond_0

    .line 226
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private isIndexable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStemmable(Ljava/lang/String;)Z
    .locals 2
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 87
    const/4 v0, 0x0

    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 93
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 89
    :cond_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    const/4 v1, 0x0

    goto :goto_1

    .line 87
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private isVowel(C)Z
    .locals 1
    .param p1, "value"    # C

    .prologue
    .line 111
    const/16 v0, 0x61

    if-eq p1, v0, :cond_0

    .line 112
    const/16 v0, 0x65

    if-eq p1, v0, :cond_0

    .line 113
    const/16 v0, 0x69

    if-eq p1, v0, :cond_0

    .line 114
    const/16 v0, 0x6f

    if-eq p1, v0, :cond_0

    .line 115
    const/16 v0, 0x75

    if-eq p1, v0, :cond_0

    .line 111
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "toRemove"    # Ljava/lang/String;

    .prologue
    .line 335
    if-eqz p1, :cond_0

    .line 336
    if-eqz p2, :cond_0

    .line 337
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 341
    .end local p1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "value":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "toReplace"    # Ljava/lang/String;
    .param p3, "changeTo"    # Ljava/lang/String;

    .prologue
    .line 313
    if-eqz p1, :cond_0

    .line 314
    if-eqz p2, :cond_0

    .line 315
    if-nez p3, :cond_1

    .line 324
    .end local p1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 319
    .restart local p1    # "value":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "vvalue":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 324
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private step1()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 407
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 558
    :cond_0
    :goto_0
    return v0

    .line 410
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "uciones"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "uciones"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 411
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "uciones"

    const-string v3, "u"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 415
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x6

    if-lt v2, v3, :cond_a

    .line 416
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "imentos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "imentos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 417
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "imentos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 419
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "amentos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "amentos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 420
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "amentos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 422
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "adores"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "adores"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 423
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "adores"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 425
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "adoras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "adoras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 426
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "adoras"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 428
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "logias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "logias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 429
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "logias"

    const-string v3, "log"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 431
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "encias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "encias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 432
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "encias"

    const-string v3, "ente"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 434
    :cond_8
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "amente"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R1:Ljava/lang/String;

    const-string v3, "amente"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 435
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "amente"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 437
    :cond_9
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "idades"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "idades"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 438
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "idades"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 443
    :cond_a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x5

    if-lt v2, v3, :cond_15

    .line 444
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "acoes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "acoes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 445
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "acoes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 447
    :cond_b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "imento"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "imento"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 448
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "imento"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 450
    :cond_c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "amento"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "amento"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 451
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "amento"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 453
    :cond_d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "adora"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "adora"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 454
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "adora"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 456
    :cond_e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ismos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ismos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 457
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ismos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 459
    :cond_f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "istas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "istas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 460
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "istas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 462
    :cond_10
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "logia"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "logia"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 463
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "logia"

    const-string v3, "log"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 465
    :cond_11
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ucion"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ucion"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 466
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ucion"

    const-string v3, "u"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 468
    :cond_12
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "encia"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "encia"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 469
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "encia"

    const-string v3, "ente"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 471
    :cond_13
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "mente"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "mente"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 472
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "mente"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 474
    :cond_14
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "idade"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "idade"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 475
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "idade"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 480
    :cond_15
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_23

    .line 481
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "acao"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "acao"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 482
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "acao"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 484
    :cond_16
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ezas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ezas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 485
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ezas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 487
    :cond_17
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "icos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "icos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 488
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "icos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 490
    :cond_18
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "icas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "icas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 491
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "icas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 493
    :cond_19
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ismo"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ismo"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 494
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ismo"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 496
    :cond_1a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "avel"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "avel"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 497
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "avel"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 499
    :cond_1b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ivel"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ivel"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 500
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ivel"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 502
    :cond_1c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ista"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ista"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 503
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ista"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 505
    :cond_1d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "osos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "osos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 506
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "osos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 508
    :cond_1e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "osas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "osas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 509
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "osas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 511
    :cond_1f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ador"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ador"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 512
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ador"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 514
    :cond_20
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ivas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ivas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 515
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ivas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 517
    :cond_21
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ivos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ivos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 518
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ivos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 520
    :cond_22
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "iras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 521
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 522
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "iras"

    const-string v4, "e"

    invoke-direct {p0, v2, v3, v4}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffixPreceded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 523
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iras"

    const-string v3, "ir"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 528
    :cond_23
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    .line 529
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "eza"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_24

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "eza"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 530
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eza"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 532
    :cond_24
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ico"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_25

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ico"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 533
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ico"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 535
    :cond_25
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ica"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ica"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 536
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ica"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 538
    :cond_26
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "oso"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "oso"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 539
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "oso"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 541
    :cond_27
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "osa"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "osa"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 542
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "osa"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 544
    :cond_28
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "iva"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "iva"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 545
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iva"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 547
    :cond_29
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ivo"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2a

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    const-string v3, "ivo"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 548
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ivo"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 550
    :cond_2a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ira"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 551
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ira"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 552
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v3, "ira"

    const-string v4, "e"

    invoke-direct {p0, v2, v3, v4}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffixPreceded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 553
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ira"

    const-string v3, "ir"

    invoke-direct {p0, v0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->replaceSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0
.end method

.method private step2()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 571
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 941
    :cond_0
    :goto_0
    return v0

    .line 574
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x7

    if-lt v2, v3, :cond_7

    .line 575
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "issemos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 576
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "issemos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 578
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "essemos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 579
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "essemos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 581
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "assemos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 582
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "assemos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 584
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ariamos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 585
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ariamos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 587
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eriamos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 588
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eriamos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto :goto_0

    .line 590
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iriamos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 591
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iriamos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 596
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x6

    if-lt v2, v3, :cond_14

    .line 597
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iremos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 598
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iremos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 600
    :cond_8
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eremos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 601
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eremos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 603
    :cond_9
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aremos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 604
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aremos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 606
    :cond_a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "avamos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 607
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "avamos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 609
    :cond_b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iramos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 610
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iramos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 612
    :cond_c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eramos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 613
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eramos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 615
    :cond_d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aramos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 616
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aramos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 618
    :cond_e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "asseis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 619
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "asseis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 621
    :cond_f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "esseis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 622
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "esseis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 624
    :cond_10
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "isseis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 625
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "isseis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 627
    :cond_11
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "arieis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 628
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "arieis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 630
    :cond_12
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "erieis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 631
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "erieis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 633
    :cond_13
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irieis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 634
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irieis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 640
    :cond_14
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x5

    if-lt v2, v3, :cond_2f

    .line 641
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irmos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 642
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irmos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 644
    :cond_15
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iamos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 645
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iamos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 647
    :cond_16
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "armos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 648
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "armos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 650
    :cond_17
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ermos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 651
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ermos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 653
    :cond_18
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "areis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 654
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "areis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 656
    :cond_19
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ereis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 657
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ereis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 659
    :cond_1a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ireis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 660
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ireis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 662
    :cond_1b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "asses"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 663
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "asses"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 665
    :cond_1c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "esses"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 666
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "esses"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 668
    :cond_1d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "isses"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 669
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "isses"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 671
    :cond_1e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "astes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 672
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "astes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 674
    :cond_1f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "assem"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 675
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "assem"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 677
    :cond_20
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "essem"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 678
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "essem"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 680
    :cond_21
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "issem"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 681
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "issem"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 683
    :cond_22
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ardes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 684
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ardes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 686
    :cond_23
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "erdes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 687
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "erdes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 689
    :cond_24
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irdes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 690
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irdes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 692
    :cond_25
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ariam"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 693
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ariam"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 695
    :cond_26
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eriam"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 696
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eriam"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 698
    :cond_27
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iriam"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 699
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iriam"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 701
    :cond_28
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "arias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 702
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "arias"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 704
    :cond_29
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "erias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 705
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "erias"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 707
    :cond_2a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 708
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irias"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 710
    :cond_2b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "estes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 711
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "estes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 713
    :cond_2c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "istes"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 714
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "istes"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 716
    :cond_2d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "areis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 717
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "areis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 719
    :cond_2e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aveis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 720
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aveis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 725
    :cond_2f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_58

    .line 726
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aria"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 727
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aria"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 729
    :cond_30
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eria"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 730
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eria"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 732
    :cond_31
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iria"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 733
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iria"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 735
    :cond_32
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "asse"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 736
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "asse"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 738
    :cond_33
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "esse"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 739
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "esse"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 741
    :cond_34
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "isse"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 742
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "isse"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 744
    :cond_35
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aste"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 745
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aste"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 747
    :cond_36
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "este"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 748
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "este"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 750
    :cond_37
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iste"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 751
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iste"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 753
    :cond_38
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "arei"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_39

    .line 754
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "arei"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 756
    :cond_39
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "erei"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 757
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "erei"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 759
    :cond_3a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irei"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 760
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irei"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 762
    :cond_3b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aram"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 763
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aram"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 765
    :cond_3c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eram"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 766
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eram"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 768
    :cond_3d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iram"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3e

    .line 769
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iram"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 771
    :cond_3e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "avam"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3f

    .line 772
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "avam"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 774
    :cond_3f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "arem"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_40

    .line 775
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "arem"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 777
    :cond_40
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "erem"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_41

    .line 778
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "erem"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 780
    :cond_41
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irem"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_42

    .line 781
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irem"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 783
    :cond_42
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ando"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_43

    .line 784
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ando"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 786
    :cond_43
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "endo"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_44

    .line 787
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "endo"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 789
    :cond_44
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "indo"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_45

    .line 790
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "indo"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 792
    :cond_45
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "arao"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_46

    .line 793
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "arao"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 795
    :cond_46
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "erao"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 796
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "erao"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 798
    :cond_47
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "irao"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_48

    .line 799
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "irao"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 801
    :cond_48
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "adas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_49

    .line 802
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "adas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 804
    :cond_49
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "idas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 805
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "idas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 807
    :cond_4a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "aras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4b

    .line 808
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "aras"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 810
    :cond_4b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 811
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eras"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 813
    :cond_4c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 814
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iras"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 816
    :cond_4d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "avas"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 817
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "avas"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 819
    :cond_4e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ares"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4f

    .line 820
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ares"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 822
    :cond_4f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eres"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_50

    .line 823
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eres"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 825
    :cond_50
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ires"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_51

    .line 826
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ires"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 828
    :cond_51
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ados"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_52

    .line 829
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ados"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 831
    :cond_52
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "idos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_53

    .line 832
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "idos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 834
    :cond_53
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "amos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_54

    .line 835
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "amos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 837
    :cond_54
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "emos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_55

    .line 838
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "emos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 840
    :cond_55
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "imos"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_56

    .line 841
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "imos"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 843
    :cond_56
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iras"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_57

    .line 844
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iras"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 846
    :cond_57
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ieis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_58

    .line 847
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ieis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 852
    :cond_58
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_65

    .line 853
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ada"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_59

    .line 854
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ada"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 856
    :cond_59
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ida"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5a

    .line 857
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ida"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 859
    :cond_5a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ara"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 860
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ara"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 862
    :cond_5b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "era"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5c

    .line 863
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "era"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 865
    :cond_5c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ira"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5d

    .line 866
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ava"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 868
    :cond_5d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iam"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5e

    .line 869
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iam"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 871
    :cond_5e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ado"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 872
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ado"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 874
    :cond_5f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ido"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_60

    .line 875
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ido"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 877
    :cond_60
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ias"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_61

    .line 878
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ias"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 880
    :cond_61
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ais"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_62

    .line 881
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ais"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 883
    :cond_62
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eis"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_63

    .line 884
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eis"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 886
    :cond_63
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ira"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_64

    .line 887
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ira"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 889
    :cond_64
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ear"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_65

    .line 890
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ear"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 895
    :cond_65
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    .line 896
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ia"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_66

    .line 897
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ia"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 899
    :cond_66
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ei"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_67

    .line 900
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ei"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 902
    :cond_67
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "am"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_68

    .line 903
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "am"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 905
    :cond_68
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "em"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_69

    .line 906
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "em"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 908
    :cond_69
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ar"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6a

    .line 909
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ar"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 911
    :cond_6a
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "er"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6b

    .line 912
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "er"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 914
    :cond_6b
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ir"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6c

    .line 915
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ir"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 917
    :cond_6c
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "as"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6d

    .line 918
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "as"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 920
    :cond_6d
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "es"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6e

    .line 921
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "es"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 923
    :cond_6e
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "is"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6f

    .line 924
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "is"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 926
    :cond_6f
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "eu"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_70

    .line 927
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "eu"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 929
    :cond_70
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iu"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_71

    .line 930
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iu"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 932
    :cond_71
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "iu"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 933
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "iu"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0

    .line 935
    :cond_72
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v3, "ou"

    invoke-direct {p0, v2, v3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 936
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v2, "ou"

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    move v0, v1

    goto/16 :goto_0
.end method

.method private step3()V
    .locals 3

    .prologue
    .line 949
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 955
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "i"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "i"

    const-string v2, "c"

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffixPreceded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 952
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "i"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0
.end method

.method private step4()V
    .locals 2

    .prologue
    .line 965
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 980
    :cond_0
    :goto_0
    return-void

    .line 967
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "os"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 968
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "os"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 970
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "a"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 971
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "a"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 973
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "i"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 974
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "i"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 976
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "o"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 977
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "o"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0
.end method

.method private step5()V
    .locals 3

    .prologue
    .line 991
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return-void

    .line 993
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "e"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 994
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "e"

    const-string v2, "gu"

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffixPreceded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 995
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "e"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    .line 996
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "u"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 1000
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    const-string v1, "e"

    const-string v2, "ci"

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffixPreceded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1001
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "e"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    .line 1002
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "i"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 1006
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    const-string v1, "e"

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0
.end method

.method private suffix(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "suffix"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 293
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 301
    :cond_0
    :goto_0
    return v0

    .line 297
    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 301
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private suffixPreceded(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "suffix"    # Ljava/lang/String;
    .param p3, "preceded"    # Ljava/lang/String;

    .prologue
    .line 351
    if-eqz p1, :cond_0

    .line 352
    if-eqz p2, :cond_0

    .line 353
    if-eqz p3, :cond_0

    .line 354
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 355
    :cond_0
    const/4 v0, 0x0

    .line 358
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->removeSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->suffix(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public log()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1016
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " (TERM = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->TERM:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1017
    const-string v1, " (CT = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1018
    const-string v1, " (RV = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1019
    const-string v1, " (R1 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1020
    const-string v1, " (R2 = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1016
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected stem(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x0

    .line 51
    .local v0, "altered":Z
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->createCT(Ljava/lang/String;)V

    .line 53
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isIndexable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 54
    const/4 v1, 0x0

    .line 78
    :goto_0
    return-object v1

    .line 56
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->isStemmable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 57
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->getR1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R1:Ljava/lang/String;

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R1:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->getR1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->R2:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->getRV(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->RV:Ljava/lang/String;

    .line 63
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->TERM:Ljava/lang/String;

    .line 65
    invoke-direct {p0}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->step1()Z

    move-result v0

    .line 66
    if-nez v0, :cond_2

    .line 67
    invoke-direct {p0}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->step2()Z

    move-result v0

    .line 70
    :cond_2
    if-eqz v0, :cond_3

    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->step3()V

    .line 76
    :goto_1
    invoke-direct {p0}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->step5()V

    .line 78
    iget-object v1, p0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->CT:Ljava/lang/String;

    goto :goto_0

    .line 73
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->step4()V

    goto :goto_1
.end method
