.class public Lorg/apache/lucene/analysis/util/StemmerUtil;
.super Ljava/lang/Object;
.source "StemmerUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static delete([CII)I
    .locals 2
    .param p0, "s"    # [C
    .param p1, "pos"    # I
    .param p2, "len"    # I

    .prologue
    .line 90
    if-ge p1, p2, :cond_0

    .line 91
    add-int/lit8 v0, p1, 0x1

    sub-int v1, p2, p1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v0, p0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    :cond_0
    add-int/lit8 v0, p2, -0x1

    return v0
.end method

.method public static deleteN([CIII)I
    .locals 1
    .param p0, "s"    # [C
    .param p1, "pos"    # I
    .param p2, "len"    # I
    .param p3, "nChars"    # I

    .prologue
    .line 107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 109
    return p2

    .line 108
    :cond_0
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static endsWith([CILjava/lang/String;)Z
    .locals 5
    .param p0, "s"    # [C
    .param p1, "len"    # I
    .param p2, "suffix"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 53
    .local v1, "suffixLen":I
    if-le v1, p1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v2

    .line 55
    :cond_1
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_2

    .line 59
    const/4 v2, 0x1

    goto :goto_0

    .line 56
    :cond_2
    sub-int v3, v1, v0

    sub-int v3, p1, v3

    aget-char v3, p0, v3

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_0

    .line 55
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public static endsWith([CI[C)Z
    .locals 5
    .param p0, "s"    # [C
    .param p1, "len"    # I
    .param p2, "suffix"    # [C

    .prologue
    const/4 v2, 0x0

    .line 71
    array-length v1, p2

    .line 72
    .local v1, "suffixLen":I
    if-le v1, p1, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v2

    .line 74
    :cond_1
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-gez v0, :cond_2

    .line 78
    const/4 v2, 0x1

    goto :goto_0

    .line 75
    :cond_2
    sub-int v3, v1, v0

    sub-int v3, p1, v3

    aget-char v3, p0, v3

    aget-char v4, p2, v0

    if-ne v3, v4, :cond_0

    .line 74
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public static startsWith([CILjava/lang/String;)Z
    .locals 5
    .param p0, "s"    # [C
    .param p1, "len"    # I
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 35
    .local v1, "prefixLen":I
    if-le v1, p1, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v2

    .line 37
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_2

    .line 40
    const/4 v2, 0x1

    goto :goto_0

    .line 38
    :cond_2
    aget-char v3, p0, v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_0

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
