.class public Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;
.super Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;
.source "HyphenationCompoundWordTokenFilter.java"


# instance fields
.field private hyphenator:Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;)V
    .locals 7
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "hyphenator"    # Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    .prologue
    .line 126
    const/4 v4, 0x5

    const/4 v5, 0x2

    .line 127
    const/16 v6, 0xf

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;III)V

    .line 128
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;III)V
    .locals 9
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "hyphenator"    # Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    .param p4, "minWordSize"    # I
    .param p5, "minSubwordSize"    # I
    .param p6, "maxSubwordSize"    # I

    .prologue
    .line 113
    const/4 v4, 0x0

    .line 114
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    .line 115
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 9
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "hyphenator"    # Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    .param p4, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 67
    const/4 v5, 0x5

    .line 68
    const/4 v6, 0x2

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    .line 69
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V
    .locals 8
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "hyphenator"    # Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    .param p4, "dictionary"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p5, "minWordSize"    # I
    .param p6, "minSubwordSize"    # I
    .param p7, "maxSubwordSize"    # I
    .param p8, "onlyLongestMatch"    # Z

    .prologue
    .line 97
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    .line 98
    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    .line 100
    iput-object p3, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->hyphenator:Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    .line 101
    return-void
.end method

.method public static getHyphenationTree(Ljava/io/File;)Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    .locals 2
    .param p0, "hyphenationFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-virtual {p0}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->getHyphenationTree(Lorg/xml/sax/InputSource;)Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    move-result-object v0

    return-object v0
.end method

.method public static getHyphenationTree(Ljava/lang/String;)Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    .locals 1
    .param p0, "hyphenationFilename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p0}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->getHyphenationTree(Lorg/xml/sax/InputSource;)Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    move-result-object v0

    return-object v0
.end method

.method public static getHyphenationTree(Lorg/xml/sax/InputSource;)Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    .locals 1
    .param p0, "hyphenationSource"    # Lorg/xml/sax/InputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;-><init>()V

    .line 164
    .local v0, "tree":Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->loadPatterns(Lorg/xml/sax/InputSource;)V

    .line 165
    return-object v0
.end method


# virtual methods
.method protected decompose()V
    .locals 14

    .prologue
    const/4 v4, 0x1

    .line 171
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->hyphenator:Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;->hyphenate([CIIII)Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;

    move-result-object v7

    .line 173
    .local v7, "hyphens":Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;
    if-nez v7, :cond_1

    .line 235
    :cond_0
    return-void

    .line 177
    :cond_1
    invoke-virtual {v7}, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;->getHyphenationPoints()[I

    move-result-object v6

    .line 179
    .local v6, "hyp":[I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v0, v6

    if-ge v8, v0, :cond_0

    .line 180
    array-length v0, v6

    sub-int v12, v0, v8

    .line 181
    .local v12, "remaining":I
    aget v13, v6, v8

    .line 182
    .local v13, "start":I
    const/4 v10, 0x0

    .line 183
    .local v10, "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    const/4 v9, 0x1

    .local v9, "j":I
    :goto_1
    if-lt v9, v12, :cond_4

    .line 231
    :cond_2
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->onlyLongestMatch:Z

    if-eqz v0, :cond_3

    if-eqz v10, :cond_3

    .line 232
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->tokens:Ljava/util/LinkedList;

    invoke-virtual {v0, v10}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 184
    :cond_4
    add-int v0, v8, v9

    aget v0, v6, v0

    sub-int v11, v0, v13

    .line 188
    .local v11, "partLength":I
    iget v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->maxSubwordSize:I

    if-gt v11, v0, :cond_2

    .line 194
    iget v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->minSubwordSize:I

    if-ge v11, v0, :cond_6

    .line 183
    :cond_5
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 201
    :cond_6
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    invoke-virtual {v0, v1, v13, v11}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 202
    :cond_7
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->onlyLongestMatch:Z

    if-eqz v0, :cond_9

    .line 203
    if-eqz v10, :cond_8

    .line 204
    iget-object v0, v10, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->txt:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge v0, v11, :cond_5

    .line 205
    new-instance v10, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .end local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    invoke-direct {v10, p0, v13, v11}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    .line 207
    .restart local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    goto :goto_2

    .line 208
    :cond_8
    new-instance v10, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .end local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    invoke-direct {v10, p0, v13, v11}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    .line 210
    .restart local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    goto :goto_2

    .line 211
    :cond_9
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->tokens:Ljava/util/LinkedList;

    new-instance v1, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    invoke-direct {v1, p0, v13, v11}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 213
    :cond_a
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    add-int/lit8 v2, v11, -0x1

    invoke-virtual {v0, v1, v13, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 218
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->onlyLongestMatch:Z

    if-eqz v0, :cond_c

    .line 219
    if-eqz v10, :cond_b

    .line 220
    iget-object v0, v10, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;->txt:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v1, v11, -0x1

    if-ge v0, v1, :cond_5

    .line 221
    new-instance v10, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .end local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    add-int/lit8 v0, v11, -0x1

    invoke-direct {v10, p0, v13, v0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    .line 223
    .restart local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    goto :goto_2

    .line 224
    :cond_b
    new-instance v10, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    .end local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    add-int/lit8 v0, v11, -0x1

    invoke-direct {v10, p0, v13, v0}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    .line 226
    .restart local v10    # "longestMatchToken":Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;
    goto :goto_2

    .line 227
    :cond_c
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->tokens:Ljava/util/LinkedList;

    new-instance v1, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;

    add-int/lit8 v2, v11, -0x1

    invoke-direct {v1, p0, v13, v2}, Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase$CompoundToken;-><init>(Lorg/apache/lucene/analysis/compound/CompoundWordTokenFilterBase;II)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method
