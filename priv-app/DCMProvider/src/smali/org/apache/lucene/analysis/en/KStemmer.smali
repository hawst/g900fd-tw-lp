.class public Lorg/apache/lucene/analysis/en/KStemmer;
.super Ljava/lang/Object;
.source "KStemmer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    }
.end annotation


# static fields
.field private static final MaxWordLen:I = 0x32

.field private static ation:[C

.field private static final countryNationality:[[Ljava/lang/String;

.field private static final dict_ht:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static final directConflations:[[Ljava/lang/String;

.field private static final exceptionWords:[Ljava/lang/String;

.field private static ication:[C

.field private static ition:[C

.field private static ization:[C

.field private static final properNouns:[Ljava/lang/String;

.field private static final supplementDict:[Ljava/lang/String;


# instance fields
.field private j:I

.field private k:I

.field matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

.field result:Ljava/lang/String;

.field private final word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 75
    const/16 v0, 0x29

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "aide"

    aput-object v1, v0, v4

    const-string v1, "bathe"

    aput-object v1, v0, v5

    const-string v1, "caste"

    aput-object v1, v0, v6

    .line 76
    const-string v1, "cute"

    aput-object v1, v0, v7

    const-string v1, "dame"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "dime"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "doge"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "done"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "dune"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "envelope"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "gage"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 77
    const-string v2, "grille"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "grippe"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "lobe"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "mane"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "mare"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "nape"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "node"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "pane"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 78
    const-string v2, "pate"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "plane"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "pope"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "programme"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "quite"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "ripe"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "rote"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "rune"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 79
    const-string v2, "sage"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "severe"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "shoppe"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "sine"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "slime"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "snipe"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "steppe"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "suite"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 80
    const-string v2, "swinge"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "tare"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "tine"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "tope"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "tripe"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "twine"

    aput-object v2, v0, v1

    .line 75
    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->exceptionWords:[Ljava/lang/String;

    .line 82
    const/16 v0, 0x28

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "aging"

    aput-object v2, v1, v4

    const-string v2, "age"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    .line 83
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "going"

    aput-object v2, v1, v4

    const-string v2, "go"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "goes"

    aput-object v2, v1, v4

    const-string v2, "go"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "lying"

    aput-object v2, v1, v4

    const-string v2, "lie"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "using"

    aput-object v2, v1, v4

    const-string v2, "use"

    aput-object v2, v1, v5

    aput-object v1, v0, v8

    const/4 v1, 0x5

    .line 84
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "owing"

    aput-object v3, v2, v4

    const-string v3, "owe"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "suing"

    aput-object v3, v2, v4

    const-string v3, "sue"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "dying"

    aput-object v3, v2, v4

    const-string v3, "die"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "tying"

    aput-object v3, v2, v4

    const-string v3, "tie"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 85
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "vying"

    aput-object v3, v2, v4

    const-string/jumbo v3, "vie"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "aged"

    aput-object v3, v2, v4

    const-string v3, "age"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "used"

    aput-object v3, v2, v4

    const-string v3, "use"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "vied"

    aput-object v3, v2, v4

    const-string/jumbo v3, "vie"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 86
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "cued"

    aput-object v3, v2, v4

    const-string v3, "cue"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "died"

    aput-object v3, v2, v4

    const-string v3, "die"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "eyed"

    aput-object v3, v2, v4

    const-string v3, "eye"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "hued"

    aput-object v3, v2, v4

    const-string v3, "hue"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 87
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "iced"

    aput-object v3, v2, v4

    const-string v3, "ice"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "lied"

    aput-object v3, v2, v4

    const-string v3, "lie"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "owed"

    aput-object v3, v2, v4

    const-string v3, "owe"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "sued"

    aput-object v3, v2, v4

    const-string v3, "sue"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 88
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "toed"

    aput-object v3, v2, v4

    const-string v3, "toe"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "tied"

    aput-object v3, v2, v4

    const-string v3, "tie"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "does"

    aput-object v3, v2, v4

    const-string v3, "do"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "doing"

    aput-object v3, v2, v4

    const-string v3, "do"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 89
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "aeronautical"

    aput-object v3, v2, v4

    const-string v3, "aeronautics"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "mathematical"

    aput-object v3, v2, v4

    const-string v3, "mathematics"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 90
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "political"

    aput-object v3, v2, v4

    const-string v3, "politics"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "metaphysical"

    aput-object v3, v2, v4

    const-string v3, "metaphysics"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 91
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "cylindrical"

    aput-object v3, v2, v4

    const-string v3, "cylinder"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "nazism"

    aput-object v3, v2, v4

    const-string v3, "nazi"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 92
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ambiguity"

    aput-object v3, v2, v4

    const-string v3, "ambiguous"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "barbarity"

    aput-object v3, v2, v4

    const-string v3, "barbarous"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 93
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "credulity"

    aput-object v3, v2, v4

    const-string v3, "credulous"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "generosity"

    aput-object v3, v2, v4

    const-string v3, "generous"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 94
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "spontaneity"

    aput-object v3, v2, v4

    const-string v3, "spontaneous"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "unanimity"

    aput-object v3, v2, v4

    const-string v3, "unanimous"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 95
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "voracity"

    aput-object v3, v2, v4

    const-string/jumbo v3, "voracious"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "fled"

    aput-object v3, v2, v4

    const-string v3, "flee"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "miscarriage"

    aput-object v3, v2, v4

    const-string v3, "miscarry"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    .line 82
    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->directConflations:[[Ljava/lang/String;

    .line 97
    const/16 v0, 0x95

    new-array v0, v0, [[Ljava/lang/String;

    .line 98
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "afghan"

    aput-object v2, v1, v4

    const-string v2, "afghanistan"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "african"

    aput-object v2, v1, v4

    const-string v2, "africa"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    .line 99
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "albanian"

    aput-object v2, v1, v4

    const-string v2, "albania"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "algerian"

    aput-object v2, v1, v4

    const-string v2, "algeria"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    .line 100
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "american"

    aput-object v2, v1, v4

    const-string v2, "america"

    aput-object v2, v1, v5

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "andorran"

    aput-object v3, v2, v4

    const-string v3, "andorra"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "angolan"

    aput-object v3, v2, v4

    const-string v3, "angola"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 101
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "arabian"

    aput-object v3, v2, v4

    const-string v3, "arabia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "argentine"

    aput-object v3, v2, v4

    const-string v3, "argentina"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 102
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "armenian"

    aput-object v3, v2, v4

    const-string v3, "armenia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "asian"

    aput-object v3, v2, v4

    const-string v3, "asia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "australian"

    aput-object v3, v2, v4

    const-string v3, "australia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 103
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "austrian"

    aput-object v3, v2, v4

    const-string v3, "austria"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "azerbaijani"

    aput-object v3, v2, v4

    const-string v3, "azerbaijan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 104
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "azeri"

    aput-object v3, v2, v4

    const-string v3, "azerbaijan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "bangladeshi"

    aput-object v3, v2, v4

    const-string v3, "bangladesh"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 105
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "belgian"

    aput-object v3, v2, v4

    const-string v3, "belgium"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "bermudan"

    aput-object v3, v2, v4

    const-string v3, "bermuda"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "bolivian"

    aput-object v3, v2, v4

    const-string v3, "bolivia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 106
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "bosnian"

    aput-object v3, v2, v4

    const-string v3, "bosnia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "botswanan"

    aput-object v3, v2, v4

    const-string v3, "botswana"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 107
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "brazilian"

    aput-object v3, v2, v4

    const-string v3, "brazil"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "british"

    aput-object v3, v2, v4

    const-string v3, "britain"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 108
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "bulgarian"

    aput-object v3, v2, v4

    const-string v3, "bulgaria"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "burmese"

    aput-object v3, v2, v4

    const-string v3, "burma"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 109
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "californian"

    aput-object v3, v2, v4

    const-string v3, "california"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "cambodian"

    aput-object v3, v2, v4

    const-string v3, "cambodia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 110
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "canadian"

    aput-object v3, v2, v4

    const-string v3, "canada"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "chadian"

    aput-object v3, v2, v4

    const-string v3, "chad"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "chilean"

    aput-object v3, v2, v4

    const-string v3, "chile"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 111
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "chinese"

    aput-object v3, v2, v4

    const-string v3, "china"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "colombian"

    aput-object v3, v2, v4

    const-string v3, "colombia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "croat"

    aput-object v3, v2, v4

    const-string v3, "croatia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 112
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "croatian"

    aput-object v3, v2, v4

    const-string v3, "croatia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "cuban"

    aput-object v3, v2, v4

    const-string v3, "cuba"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "cypriot"

    aput-object v3, v2, v4

    const-string v3, "cyprus"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 113
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "czechoslovakian"

    aput-object v3, v2, v4

    const-string v3, "czechoslovakia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "danish"

    aput-object v3, v2, v4

    const-string v3, "denmark"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 114
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "egyptian"

    aput-object v3, v2, v4

    const-string v3, "egypt"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "equadorian"

    aput-object v3, v2, v4

    const-string v3, "equador"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 115
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "eritrean"

    aput-object v3, v2, v4

    const-string v3, "eritrea"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "estonian"

    aput-object v3, v2, v4

    const-string v3, "estonia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 116
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ethiopian"

    aput-object v3, v2, v4

    const-string v3, "ethiopia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "european"

    aput-object v3, v2, v4

    const-string v3, "europe"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "fijian"

    aput-object v3, v2, v4

    const-string v3, "fiji"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 117
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "filipino"

    aput-object v3, v2, v4

    const-string v3, "philippines"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "finnish"

    aput-object v3, v2, v4

    const-string v3, "finland"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    .line 118
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "french"

    aput-object v3, v2, v4

    const-string v3, "france"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x30

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "gambian"

    aput-object v3, v2, v4

    const-string v3, "gambia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x31

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "georgian"

    aput-object v3, v2, v4

    const-string v3, "georgia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 119
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "german"

    aput-object v3, v2, v4

    const-string v3, "germany"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ghanian"

    aput-object v3, v2, v4

    const-string v3, "ghana"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "greek"

    aput-object v3, v2, v4

    const-string v3, "greece"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 120
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "grenadan"

    aput-object v3, v2, v4

    const-string v3, "grenada"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x36

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "guamian"

    aput-object v3, v2, v4

    const-string v3, "guam"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 121
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "guatemalan"

    aput-object v3, v2, v4

    const-string v3, "guatemala"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x38

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "guinean"

    aput-object v3, v2, v4

    const-string v3, "guinea"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x39

    .line 122
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "guyanan"

    aput-object v3, v2, v4

    const-string v3, "guyana"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "haitian"

    aput-object v3, v2, v4

    const-string v3, "haiti"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "hawaiian"

    aput-object v3, v2, v4

    const-string v3, "hawaii"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    .line 123
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "holland"

    aput-object v3, v2, v4

    const-string v3, "dutch"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "honduran"

    aput-object v3, v2, v4

    const-string v3, "honduras"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "hungarian"

    aput-object v3, v2, v4

    const-string v3, "hungary"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    .line 124
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "icelandic"

    aput-object v3, v2, v4

    const-string v3, "iceland"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x40

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "indonesian"

    aput-object v3, v2, v4

    const-string v3, "indonesia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x41

    .line 125
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "iranian"

    aput-object v3, v2, v4

    const-string v3, "iran"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x42

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "iraqi"

    aput-object v3, v2, v4

    const-string v3, "iraq"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x43

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "iraqui"

    aput-object v3, v2, v4

    const-string v3, "iraq"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 126
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "irish"

    aput-object v3, v2, v4

    const-string v3, "ireland"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x45

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "israeli"

    aput-object v3, v2, v4

    const-string v3, "israel"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x46

    .line 127
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "italian"

    aput-object v3, v2, v4

    const-string v3, "italy"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x47

    .line 128
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "jamaican"

    aput-object v3, v2, v4

    const-string v3, "jamaica"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x48

    .line 129
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "japanese"

    aput-object v3, v2, v4

    const-string v3, "japan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x49

    .line 130
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "jordanian"

    aput-object v3, v2, v4

    const-string v3, "jordan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    .line 131
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "kampuchean"

    aput-object v3, v2, v4

    const-string v3, "cambodia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    .line 132
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "kenyan"

    aput-object v3, v2, v4

    const-string v3, "kenya"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    .line 133
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "korean"

    aput-object v3, v2, v4

    const-string v3, "korea"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    .line 134
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "kuwaiti"

    aput-object v3, v2, v4

    const-string v3, "kuwait"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    .line 135
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "lankan"

    aput-object v3, v2, v4

    const-string v3, "lanka"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    .line 136
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "laotian"

    aput-object v3, v2, v4

    const-string v3, "laos"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x50

    .line 137
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "latvian"

    aput-object v3, v2, v4

    const-string v3, "latvia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x51

    .line 138
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "lebanese"

    aput-object v3, v2, v4

    const-string v3, "lebanon"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x52

    .line 139
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "liberian"

    aput-object v3, v2, v4

    const-string v3, "liberia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x53

    .line 140
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "libyan"

    aput-object v3, v2, v4

    const-string v3, "libya"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x54

    .line 141
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "lithuanian"

    aput-object v3, v2, v4

    const-string v3, "lithuania"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x55

    .line 142
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "macedonian"

    aput-object v3, v2, v4

    const-string v3, "macedonia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x56

    .line 143
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "madagascan"

    aput-object v3, v2, v4

    const-string v3, "madagascar"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x57

    .line 144
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "malaysian"

    aput-object v3, v2, v4

    const-string v3, "malaysia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x58

    .line 145
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "maltese"

    aput-object v3, v2, v4

    const-string v3, "malta"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x59

    .line 146
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "mauritanian"

    aput-object v3, v2, v4

    const-string v3, "mauritania"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    .line 147
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "mexican"

    aput-object v3, v2, v4

    const-string v3, "mexico"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    .line 148
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "micronesian"

    aput-object v3, v2, v4

    const-string v3, "micronesia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    .line 149
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "moldovan"

    aput-object v3, v2, v4

    const-string v3, "moldova"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    .line 150
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "monacan"

    aput-object v3, v2, v4

    const-string v3, "monaco"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    .line 151
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "mongolian"

    aput-object v3, v2, v4

    const-string v3, "mongolia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    .line 152
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "montenegran"

    aput-object v3, v2, v4

    const-string v3, "montenegro"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x60

    .line 153
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "moroccan"

    aput-object v3, v2, v4

    const-string v3, "morocco"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x61

    .line 154
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "myanmar"

    aput-object v3, v2, v4

    const-string v3, "burma"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x62

    .line 155
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "namibian"

    aput-object v3, v2, v4

    const-string v3, "namibia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x63

    .line 156
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "nepalese"

    aput-object v3, v2, v4

    const-string v3, "nepal"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x64

    .line 158
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "nicaraguan"

    aput-object v3, v2, v4

    const-string v3, "nicaragua"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x65

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "nigerian"

    aput-object v3, v2, v4

    const-string v3, "nigeria"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x66

    .line 159
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "norwegian"

    aput-object v3, v2, v4

    const-string v3, "norway"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x67

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "omani"

    aput-object v3, v2, v4

    const-string v3, "oman"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x68

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "pakistani"

    aput-object v3, v2, v4

    const-string v3, "pakistan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x69

    .line 160
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "panamanian"

    aput-object v3, v2, v4

    const-string v3, "panama"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "papuan"

    aput-object v3, v2, v4

    const-string v3, "papua"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    .line 161
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "paraguayan"

    aput-object v3, v2, v4

    const-string v3, "paraguay"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "peruvian"

    aput-object v3, v2, v4

    const-string v3, "peru"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    .line 162
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "portuguese"

    aput-object v3, v2, v4

    const-string v3, "portugal"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "romanian"

    aput-object v3, v2, v4

    const-string v3, "romania"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    .line 163
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "rumania"

    aput-object v3, v2, v4

    const-string v3, "romania"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x70

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "rumanian"

    aput-object v3, v2, v4

    const-string v3, "romania"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x71

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "russian"

    aput-object v3, v2, v4

    const-string v3, "russia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x72

    .line 164
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "rwandan"

    aput-object v3, v2, v4

    const-string v3, "rwanda"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x73

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "samoan"

    aput-object v3, v2, v4

    const-string v3, "samoa"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x74

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "scottish"

    aput-object v3, v2, v4

    const-string v3, "scotland"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x75

    .line 165
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "serb"

    aput-object v3, v2, v4

    const-string v3, "serbia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x76

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "serbian"

    aput-object v3, v2, v4

    const-string v3, "serbia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x77

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "siam"

    aput-object v3, v2, v4

    const-string v3, "thailand"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x78

    .line 166
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "siamese"

    aput-object v3, v2, v4

    const-string v3, "thailand"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x79

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "slovakia"

    aput-object v3, v2, v4

    const-string v3, "slovak"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "slovakian"

    aput-object v3, v2, v4

    const-string v3, "slovak"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    .line 167
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "slovenian"

    aput-object v3, v2, v4

    const-string v3, "slovenia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "somali"

    aput-object v3, v2, v4

    const-string v3, "somalia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    .line 168
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "somalian"

    aput-object v3, v2, v4

    const-string v3, "somalia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "spanish"

    aput-object v3, v2, v4

    const-string v3, "spain"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "swedish"

    aput-object v3, v2, v4

    const-string v3, "sweden"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x80

    .line 169
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "swiss"

    aput-object v3, v2, v4

    const-string v3, "switzerland"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x81

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "syrian"

    aput-object v3, v2, v4

    const-string v3, "syria"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x82

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "taiwanese"

    aput-object v3, v2, v4

    const-string v3, "taiwan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x83

    .line 170
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "tanzanian"

    aput-object v3, v2, v4

    const-string v3, "tanzania"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x84

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "texan"

    aput-object v3, v2, v4

    const-string v3, "texas"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x85

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "thai"

    aput-object v3, v2, v4

    const-string v3, "thailand"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x86

    .line 171
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "tunisian"

    aput-object v3, v2, v4

    const-string v3, "tunisia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x87

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "turkish"

    aput-object v3, v2, v4

    const-string v3, "turkey"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x88

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ugandan"

    aput-object v3, v2, v4

    const-string v3, "uganda"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x89

    .line 172
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "ukrainian"

    aput-object v3, v2, v4

    const-string v3, "ukraine"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "uruguayan"

    aput-object v3, v2, v4

    const-string v3, "uruguay"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    .line 173
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "uzbek"

    aput-object v3, v2, v4

    const-string v3, "uzbekistan"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "venezuelan"

    aput-object v3, v2, v4

    const-string v3, "venezuela"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    .line 174
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "vietnamese"

    aput-object v3, v2, v4

    const-string/jumbo v3, "viet"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "virginian"

    aput-object v3, v2, v4

    const-string/jumbo v3, "virginia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "yemeni"

    aput-object v3, v2, v4

    const-string/jumbo v3, "yemen"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x90

    .line 175
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "yugoslav"

    aput-object v3, v2, v4

    const-string/jumbo v3, "yugoslavia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x91

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "yugoslavian"

    aput-object v3, v2, v4

    const-string/jumbo v3, "yugoslavia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x92

    .line 176
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "zambian"

    aput-object v3, v2, v4

    const-string/jumbo v3, "zambia"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x93

    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "zealander"

    aput-object v3, v2, v4

    const-string/jumbo v3, "zealand"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x94

    .line 177
    new-array v2, v6, [Ljava/lang/String;

    const-string/jumbo v3, "zimbabwean"

    aput-object v3, v2, v4

    const-string/jumbo v3, "zimbabwe"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    .line 97
    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->countryNationality:[[Ljava/lang/String;

    .line 179
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "aids"

    aput-object v1, v0, v4

    const-string v1, "applicator"

    aput-object v1, v0, v5

    .line 180
    const-string v1, "capacitor"

    aput-object v1, v0, v6

    const-string v1, "digitize"

    aput-object v1, v0, v7

    const-string v1, "electromagnet"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "ellipsoid"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "exosphere"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 181
    const-string v2, "extensible"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ferromagnet"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "graphics"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "hydromagnet"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "polygraph"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 182
    const-string v2, "toroid"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "superconduct"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "backscatter"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "connectionism"

    aput-object v2, v0, v1

    .line 179
    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->supplementDict:[Ljava/lang/String;

    .line 184
    const/16 v0, 0xfd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "abrams"

    aput-object v1, v0, v4

    const-string v1, "achilles"

    aput-object v1, v0, v5

    .line 185
    const-string v1, "acropolis"

    aput-object v1, v0, v6

    const-string v1, "adams"

    aput-object v1, v0, v7

    const-string v1, "agnes"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "aires"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "alexander"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "alexis"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "alfred"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 186
    const-string v2, "algiers"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "alps"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "amadeus"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ames"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "amos"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "andes"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "angeles"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 187
    const-string v2, "annapolis"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "antilles"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "aquarius"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "archimedes"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "arkansas"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "asher"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 188
    const-string v2, "ashly"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "athens"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "atkins"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "atlantis"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "avis"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "bahamas"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "bangor"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 189
    const-string v2, "barbados"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "barger"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "bering"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "brahms"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "brandeis"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "brussels"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 190
    const-string v2, "bruxelles"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "cairns"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "camoros"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "camus"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "carlos"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "celts"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "chalker"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 191
    const-string v2, "charles"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "cheops"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "ching"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "christmas"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "cocos"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "collins"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    .line 192
    const-string v2, "columbus"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "confucius"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "conners"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "connolly"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "copernicus"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "cramer"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 193
    const-string v2, "cyclops"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "cygnus"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "cyprus"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "dallas"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "damascus"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "daniels"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "davies"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    .line 194
    const-string v2, "davis"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "decker"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "denning"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "dennis"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "descartes"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "dickens"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "doris"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    .line 195
    const-string v2, "douglas"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "downs"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "dreyfus"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "dukakis"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "dulles"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "dumfries"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    .line 196
    const-string v2, "ecclesiastes"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "edwards"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "emily"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "erasmus"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "euphrates"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "evans"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    .line 197
    const-string v2, "everglades"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "fairbanks"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "federales"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "fisher"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "fitzsimmons"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    .line 198
    const-string v2, "fleming"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "forbes"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "fowler"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "france"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "francis"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "goering"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    .line 199
    const-string v2, "goodling"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "goths"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "grenadines"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "guiness"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "hades"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "harding"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    .line 200
    const-string v2, "harris"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "hastings"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "hawkes"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "hawking"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "hayes"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "heights"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    .line 201
    const-string v2, "hercules"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "himalayas"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "hippocrates"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "hobbs"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "holmes"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "honduras"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    .line 202
    const-string v2, "hopkins"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "hughes"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "humphreys"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "illinois"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "indianapolis"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    .line 203
    const-string v2, "inverness"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "iris"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "iroquois"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "irving"

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "isaacs"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "italy"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "james"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    .line 204
    const-string v2, "jarvis"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "jeffreys"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "jesus"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "jones"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "josephus"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "judas"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "julius"

    aput-object v2, v0, v1

    const/16 v1, 0x80

    .line 205
    const-string v2, "kansas"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "keynes"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "kipling"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "kiwanis"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "lansing"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "laos"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "leeds"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    .line 206
    const-string v2, "levis"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "leviticus"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "lewis"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "louis"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    const-string v2, "maccabees"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "madras"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    .line 207
    const-string v2, "maimonides"

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "maldive"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "massachusetts"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "matthews"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "mauritius"

    aput-object v2, v0, v1

    const/16 v1, 0x92

    .line 208
    const-string v2, "memphis"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "mercedes"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    const-string v2, "midas"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "mingus"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "minneapolis"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "mohammed"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    .line 209
    const-string v2, "moines"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "morris"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "moses"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "myers"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "myknos"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "nablus"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    const-string v2, "nanjing"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    .line 210
    const-string v2, "nantes"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "naples"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "neal"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "netherlands"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "nevis"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "nostradamus"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    .line 211
    const-string v2, "oedipus"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "olympus"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "orleans"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "orly"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    const-string v2, "papas"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "paris"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "parker"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    .line 212
    const-string v2, "pauling"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "peking"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "pershing"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "peter"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "peters"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "philippines"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    .line 213
    const-string v2, "phineas"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    const-string v2, "pisces"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "pryor"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "pythagoras"

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "queens"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "rabelais"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    .line 214
    const-string v2, "ramses"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "reynolds"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "rhesus"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, "rhodes"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "richards"

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "robins"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    .line 215
    const-string v2, "rodgers"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "rogers"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "rubens"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "sagittarius"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "seychelles"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "socrates"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    .line 216
    const-string v2, "texas"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "thames"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "thomas"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "tiberias"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "tunis"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string v2, "venus"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    const-string/jumbo v2, "vilnius"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    .line 217
    const-string/jumbo v2, "wales"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string/jumbo v2, "warner"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string/jumbo v2, "wilkins"

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string/jumbo v2, "williams"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string/jumbo v2, "wyoming"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string/jumbo v2, "xmas"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string/jumbo v2, "yonkers"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    .line 218
    const-string/jumbo v2, "zeus"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string v2, "frances"

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string v2, "aarhus"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string v2, "adonis"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    const-string v2, "andrews"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string v2, "angus"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string v2, "antares"

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    .line 219
    const-string v2, "aquinas"

    aput-object v2, v0, v1

    const/16 v1, 0xda

    const-string v2, "arcturus"

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    const-string v2, "ares"

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    const-string v2, "artemis"

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    const-string v2, "augustus"

    aput-object v2, v0, v1

    const/16 v1, 0xde

    const-string v2, "ayers"

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    .line 220
    const-string v2, "barnabas"

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    const-string v2, "barnes"

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    const-string v2, "becker"

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    const-string v2, "bejing"

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    const-string v2, "biggs"

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    const-string v2, "billings"

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    const-string v2, "boeing"

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    .line 221
    const-string v2, "boris"

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    const-string v2, "borroughs"

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    const-string v2, "briggs"

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    const-string v2, "buenos"

    aput-object v2, v0, v1

    const/16 v1, 0xea

    const-string v2, "calais"

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    const-string v2, "caracas"

    aput-object v2, v0, v1

    const/16 v1, 0xec

    const-string v2, "cassius"

    aput-object v2, v0, v1

    const/16 v1, 0xed

    .line 222
    const-string v2, "cerberus"

    aput-object v2, v0, v1

    const/16 v1, 0xee

    const-string v2, "ceres"

    aput-object v2, v0, v1

    const/16 v1, 0xef

    const-string v2, "cervantes"

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    const-string v2, "chantilly"

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    const-string v2, "chartres"

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    const-string v2, "chester"

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    .line 223
    const-string v2, "connally"

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    const-string v2, "conner"

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    const-string v2, "coors"

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    const-string v2, "cummings"

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    const-string v2, "curtis"

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    const-string v2, "daedalus"

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    .line 224
    const-string v2, "dionysus"

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    const-string v2, "dobbs"

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    const-string v2, "dolores"

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    const-string v2, "edmonds"

    aput-object v2, v0, v1

    .line 184
    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->properNouns:[Ljava/lang/String;

    .line 236
    invoke-static {}, Lorg/apache/lucene/analysis/en/KStemmer;->initializeDictHash()Lorg/apache/lucene/analysis/util/CharArrayMap;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->dict_ht:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 997
    const-string v0, "ization"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->ization:[C

    .line 998
    const-string v0, "ition"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->ition:[C

    .line 999
    const-string v0, "ation"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->ation:[C

    .line 1000
    const-string v0, "ication"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->ication:[C

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 1359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    new-instance v0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    .line 563
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 1359
    return-void
.end method

.method private alEndings()V
    .locals 5

    .prologue
    const/16 v4, 0x69

    .line 1253
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1255
    .local v0, "old_k":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    .line 1314
    :cond_0
    :goto_0
    return-void

    .line 1256
    :cond_1
    const/16 v1, 0x61

    const/16 v2, 0x6c

    invoke-direct {p0, v1, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1257
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1258
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1259
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1262
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->doubleC(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1263
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1264
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1265
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1266
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1269
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1270
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1271
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1272
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1274
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1275
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "um"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1277
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1278
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1280
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1281
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "al"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1282
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1284
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x63

    if-ne v1, v2, :cond_3

    .line 1285
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1286
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x2

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1287
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1289
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1290
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const/16 v2, 0x79

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1291
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1292
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1294
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1295
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ic"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1296
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1300
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto/16 :goto_0

    .line 1304
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1305
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1306
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1307
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1308
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ial"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1309
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1310
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto/16 :goto_0
.end method

.method private aspect()V
    .locals 5

    .prologue
    const/16 v4, 0x65

    .line 686
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v1

    const/4 v2, 0x5

    if-gt v1, v2, :cond_1

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    const/16 v1, 0x69

    const/16 v2, 0x6e

    const/16 v3, 0x67

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->vowelInStem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 692
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 693
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 694
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 696
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->wordInDict()Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    move-result-object v0

    .line 697
    .local v0, "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    if-eqz v0, :cond_2

    .line 698
    iget-boolean v1, v0, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;->exception:Z

    if-eqz v1, :cond_0

    .line 703
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 704
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 706
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 709
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->doubleC(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 710
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 711
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 712
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 713
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 720
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 721
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 737
    :cond_3
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_4

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->isCons(I)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->isCons(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 738
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 739
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    goto/16 :goto_0

    .line 744
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 745
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 746
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private bleEndings()V
    .locals 6

    .prologue
    const/16 v5, 0x65

    .line 933
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 936
    .local v0, "old_k":I
    const/16 v2, 0x62

    const/16 v3, 0x6c

    invoke-direct {p0, v2, v3, v5}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 937
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x61

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    const/16 v3, 0x69

    if-eq v2, v3, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-void

    .line 938
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    .line 939
    .local v1, "word_char":C
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 940
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 941
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 942
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->doubleC(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 943
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 944
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 945
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 946
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 947
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 949
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 950
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 951
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 952
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 953
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 954
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v3, "ate"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 956
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 957
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 958
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 959
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 960
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v3, "ble"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 961
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private doubleC(I)Z
    .locals 4
    .param p1, "i"    # I

    .prologue
    const/4 v0, 0x0

    .line 664
    const/4 v1, 0x1

    if-ge p1, v1, :cond_1

    .line 667
    :cond_0
    :goto_0
    return v0

    .line 666
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    if-ne v1, v2, :cond_0

    .line 667
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/en/KStemmer;->isCons(I)Z

    move-result v0

    goto :goto_0
.end method

.method private endsIn(CC)Z
    .locals 3
    .param p1, "a"    # C
    .param p2, "b"    # C

    .prologue
    const/4 v0, 0x0

    .line 443
    const/4 v1, 0x2

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    if-le v1, v2, :cond_1

    .line 449
    :cond_0
    :goto_0
    return v0

    .line 445
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p2, :cond_0

    .line 446
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    .line 447
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private endsIn(CCC)Z
    .locals 3
    .param p1, "a"    # C
    .param p2, "b"    # C
    .param p3, "c"    # C

    .prologue
    const/4 v0, 0x0

    .line 453
    const/4 v1, 0x3

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    if-le v1, v2, :cond_1

    .line 459
    :cond_0
    :goto_0
    return v0

    .line 454
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p2, :cond_0

    .line 455
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p3, :cond_0

    .line 456
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v0, v0, -0x3

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    .line 457
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private endsIn(CCCC)Z
    .locals 3
    .param p1, "a"    # C
    .param p2, "b"    # C
    .param p3, "c"    # C
    .param p4, "d"    # C

    .prologue
    const/4 v0, 0x0

    .line 463
    const/4 v1, 0x4

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    if-le v1, v2, :cond_1

    .line 469
    :cond_0
    :goto_0
    return v0

    .line 464
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p2, :cond_0

    .line 465
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p3, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, p4, :cond_0

    .line 466
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    .line 467
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private endsIn([C)Z
    .locals 6
    .param p1, "s"    # [C

    .prologue
    const/4 v3, 0x0

    .line 431
    array-length v4, p1

    iget v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    if-le v4, v5, :cond_1

    .line 439
    :cond_0
    :goto_0
    return v3

    .line 433
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v4

    array-length v5, p1

    sub-int v1, v4, v5

    .line 434
    .local v1, "r":I
    iget v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    iput v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    .line 435
    move v2, v1

    .local v2, "r1":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, p1

    if-lt v0, v4, :cond_2

    .line 438
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    .line 439
    const/4 v3, 0x1

    goto :goto_0

    .line 436
    :cond_2
    aget-char v4, p1, v0

    iget-object v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v5, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_0

    .line 435
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private erAndOrEndings()V
    .locals 8

    .prologue
    const/16 v7, 0x69

    const/16 v6, 0x72

    const/16 v5, 0x65

    .line 1122
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1124
    .local v0, "old_k":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    if-eq v2, v6, :cond_1

    .line 1180
    :cond_0
    :goto_0
    return-void

    .line 1128
    :cond_1
    const/16 v2, 0x7a

    invoke-direct {p0, v7, v2, v5, v6}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCCC)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1132
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, 0x4

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1133
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1134
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 1138
    :cond_2
    invoke-direct {p0, v5, v6}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v2

    if-nez v2, :cond_3

    const/16 v2, 0x6f

    invoke-direct {p0, v2, v6}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1139
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    .line 1140
    .local v1, "word_char":C
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->doubleC(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1141
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1142
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1143
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1144
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1147
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v7, :cond_5

    .line 1148
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    const/16 v4, 0x79

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1149
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1150
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1151
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1153
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3, v7}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1154
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1157
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    if-ne v2, v5, :cond_6

    .line 1158
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1159
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1160
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1161
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1164
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1165
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1166
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1167
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1168
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1169
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1170
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1171
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1172
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1173
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1174
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1175
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v6}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1176
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private finalChar()C
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v0

    return v0
.end method

.method private icEndings()V
    .locals 3

    .prologue
    .line 973
    const/16 v0, 0x69

    const/16 v1, 0x63

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 975
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v1, "al"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 976
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 977
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 994
    :cond_0
    :goto_0
    return-void

    .line 979
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x79

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 980
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 981
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 982
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 984
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x65

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 985
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 987
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 988
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 989
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 990
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v1, "ic"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 991
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto :goto_0
.end method

.method private static initializeDictHash()Lorg/apache/lucene/analysis/util/CharArrayMap;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0x3e8

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 283
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 284
    sget-object v5, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    .line 283
    invoke-direct {v1, v5, v7, v8}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 286
    .local v1, "d":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;>;"
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArrayMap;

    .end local v1    # "d":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;>;"
    sget-object v5, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v5, v7, v8}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 287
    .restart local v1    # "d":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->exceptionWords:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_0

    .line 297
    const/4 v4, 0x0

    :goto_1
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->directConflations:[[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_2

    .line 307
    const/4 v4, 0x0

    :goto_2
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->countryNationality:[[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_4

    .line 317
    new-instance v2, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    const/4 v5, 0x0

    invoke-direct {v2, v5, v8}, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;-><init>(Ljava/lang/String;Z)V

    .line 320
    .local v2, "defaultEntry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData1;->data:[Ljava/lang/String;

    .line 322
    .local v0, "array":[Ljava/lang/String;
    const/4 v4, 0x0

    :goto_3
    array-length v5, v0

    if-lt v4, v5, :cond_6

    .line 331
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData2;->data:[Ljava/lang/String;

    .line 332
    const/4 v4, 0x0

    :goto_4
    array-length v5, v0

    if-lt v4, v5, :cond_8

    .line 341
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData3;->data:[Ljava/lang/String;

    .line 342
    const/4 v4, 0x0

    :goto_5
    array-length v5, v0

    if-lt v4, v5, :cond_a

    .line 351
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData4;->data:[Ljava/lang/String;

    .line 352
    const/4 v4, 0x0

    :goto_6
    array-length v5, v0

    if-lt v4, v5, :cond_c

    .line 361
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData5;->data:[Ljava/lang/String;

    .line 362
    const/4 v4, 0x0

    :goto_7
    array-length v5, v0

    if-lt v4, v5, :cond_e

    .line 371
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData6;->data:[Ljava/lang/String;

    .line 372
    const/4 v4, 0x0

    :goto_8
    array-length v5, v0

    if-lt v4, v5, :cond_10

    .line 381
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemData7;->data:[Ljava/lang/String;

    .line 382
    const/4 v4, 0x0

    :goto_9
    array-length v5, v0

    if-lt v4, v5, :cond_12

    .line 391
    const/4 v4, 0x0

    :goto_a
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemData8;->data:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_14

    .line 400
    const/4 v4, 0x0

    :goto_b
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->supplementDict:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_16

    .line 409
    const/4 v4, 0x0

    :goto_c
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->properNouns:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_18

    .line 418
    return-object v1

    .line 288
    .end local v0    # "array":[Ljava/lang/String;
    .end local v2    # "defaultEntry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    :cond_0
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->exceptionWords:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 289
    new-instance v3, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->exceptionWords:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-direct {v3, v5, v6}, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;-><init>(Ljava/lang/String;Z)V

    .line 290
    .local v3, "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->exceptionWords:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 292
    .end local v3    # "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    :cond_1
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/lucene/analysis/en/KStemmer;->exceptionWords:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 293
    const-string v7, "] already in dictionary 1"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 292
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 298
    :cond_2
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->directConflations:[[Ljava/lang/String;

    aget-object v5, v5, v4

    aget-object v5, v5, v8

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 299
    new-instance v3, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->directConflations:[[Ljava/lang/String;

    aget-object v5, v5, v4

    aget-object v5, v5, v6

    invoke-direct {v3, v5, v8}, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;-><init>(Ljava/lang/String;Z)V

    .line 300
    .restart local v3    # "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->directConflations:[[Ljava/lang/String;

    aget-object v5, v5, v4

    aget-object v5, v5, v8

    invoke-virtual {v1, v5, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 302
    .end local v3    # "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    :cond_3
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/lucene/analysis/en/KStemmer;->directConflations:[[Ljava/lang/String;

    aget-object v7, v7, v4

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 303
    const-string v7, "] already in dictionary 2"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 302
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 308
    :cond_4
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->countryNationality:[[Ljava/lang/String;

    aget-object v5, v5, v4

    aget-object v5, v5, v8

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 309
    new-instance v3, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->countryNationality:[[Ljava/lang/String;

    aget-object v5, v5, v4

    aget-object v5, v5, v6

    invoke-direct {v3, v5, v8}, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;-><init>(Ljava/lang/String;Z)V

    .line 310
    .restart local v3    # "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->countryNationality:[[Ljava/lang/String;

    aget-object v5, v5, v4

    aget-object v5, v5, v8

    invoke-virtual {v1, v5, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 312
    .end local v3    # "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    :cond_5
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/lucene/analysis/en/KStemmer;->countryNationality:[[Ljava/lang/String;

    aget-object v7, v7, v4

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 313
    const-string v7, "] already in dictionary 3"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 312
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 323
    .restart local v0    # "array":[Ljava/lang/String;
    .restart local v2    # "defaultEntry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    :cond_6
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 324
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    .line 326
    :cond_7
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 327
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 326
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 333
    :cond_8
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 334
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    .line 336
    :cond_9
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 337
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 336
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 343
    :cond_a
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 344
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5

    .line 346
    :cond_b
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 347
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 346
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 353
    :cond_c
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 354
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_6

    .line 356
    :cond_d
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 357
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 356
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 363
    :cond_e
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 364
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    .line 366
    :cond_f
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 367
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 366
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 373
    :cond_10
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 374
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    .line 376
    :cond_11
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 377
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 376
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 383
    :cond_12
    aget-object v5, v0, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 384
    aget-object v5, v0, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_9

    .line 386
    :cond_13
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 387
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 386
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 392
    :cond_14
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemData8;->data:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    .line 393
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemData8;->data:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    .line 395
    :cond_15
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/lucene/analysis/en/KStemData8;->data:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 396
    const-string v7, "] already in dictionary 4"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 395
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 401
    :cond_16
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->supplementDict:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_17

    .line 402
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->supplementDict:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    .line 404
    :cond_17
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/lucene/analysis/en/KStemmer;->supplementDict:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 405
    const-string v7, "] already in dictionary 5"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 404
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 410
    :cond_18
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->properNouns:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5}, Lorg/apache/lucene/analysis/util/CharArrayMap;->containsKey(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 411
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->properNouns:[Ljava/lang/String;

    aget-object v5, v5, v4

    invoke-virtual {v1, v5, v2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_c

    .line 413
    :cond_19
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Warning: Entry ["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/lucene/analysis/en/KStemmer;->properNouns:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 414
    const-string v7, "] already in dictionary 6"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 413
    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private ionEndings()V
    .locals 5

    .prologue
    const/16 v4, 0x65

    .line 1008
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1009
    .local v0, "old_k":I
    const/16 v1, 0x69

    const/16 v2, 0x6f

    const/16 v3, 0x6e

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1114
    :cond_0
    :goto_0
    return-void

    .line 1013
    :cond_1
    sget-object v1, Lorg/apache/lucene/analysis/en/KStemmer;->ization:[C

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn([C)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1017
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1018
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1019
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1020
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 1024
    :cond_2
    sget-object v1, Lorg/apache/lucene/analysis/en/KStemmer;->ition:[C

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn([C)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1025
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1026
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1027
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1028
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1035
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1036
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ition"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1037
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1074
    :cond_3
    :goto_1
    sget-object v1, Lorg/apache/lucene/analysis/en/KStemmer;->ication:[C

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn([C)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1075
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1076
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const/16 v2, 0x79

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1077
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1078
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1085
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1086
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ication"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1087
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1093
    :cond_4
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x3

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    .line 1095
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1096
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1097
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1098
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1101
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1102
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1103
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1107
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1108
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ion"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1109
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0

    .line 1039
    :cond_5
    sget-object v1, Lorg/apache/lucene/analysis/en/KStemmer;->ation:[C

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn([C)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1040
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1041
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1042
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1043
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1046
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1047
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1051
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1052
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1054
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1058
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1059
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1062
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1063
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ation"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1064
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_1
.end method

.method private isAlpha(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 422
    const/16 v0, 0x61

    if-lt p1, v0, :cond_0

    const/16 v0, 0x7a

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCons(I)Z
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 272
    iget-object v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v0

    .line 274
    .local v0, "ch":C
    const/16 v3, 0x61

    if-eq v0, v3, :cond_0

    const/16 v3, 0x65

    if-eq v0, v3, :cond_0

    const/16 v3, 0x69

    if-eq v0, v3, :cond_0

    const/16 v3, 0x6f

    if-eq v0, v3, :cond_0

    const/16 v3, 0x75

    if-ne v0, v3, :cond_1

    .line 276
    :cond_0
    :goto_0
    return v1

    .line 275
    :cond_1
    const/16 v3, 0x79

    if-ne v0, v3, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    .line 276
    :cond_3
    add-int/lit8 v3, p1, -0x1

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->isCons(I)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method private isVowel(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 266
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/en/KStemmer;->isCons(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private ismEndings()V
    .locals 3

    .prologue
    .line 858
    const/16 v0, 0x69

    const/16 v1, 0x73

    const/16 v2, 0x6d

    invoke-direct {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 862
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 863
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 864
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    .line 866
    :cond_0
    return-void
.end method

.method private ityEndings()V
    .locals 6

    .prologue
    const/16 v5, 0x6c

    const/16 v4, 0x65

    const/16 v3, 0x69

    .line 759
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 761
    .local v0, "old_k":I
    const/16 v1, 0x74

    const/16 v2, 0x79

    invoke-direct {p0, v3, v1, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 762
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 763
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 764
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 814
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 766
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 767
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 768
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 769
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ty"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 770
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 775
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_2

    .line 776
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 777
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "le"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 778
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 779
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 784
    :cond_2
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x76

    if-ne v1, v2, :cond_3

    .line 785
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 786
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 787
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 788
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto/16 :goto_0

    .line 792
    :cond_3
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x61

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v5, :cond_4

    .line 793
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 794
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 795
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto/16 :goto_0

    .line 806
    :cond_4
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 809
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 810
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private iveEndings()V
    .locals 6

    .prologue
    const/16 v5, 0x76

    const/16 v4, 0x65

    .line 1322
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1324
    .local v0, "old_k":I
    const/16 v1, 0x69

    invoke-direct {p0, v1, v5, v4}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1325
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1326
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1327
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1356
    :cond_0
    :goto_0
    return-void

    .line 1329
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1330
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1331
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1332
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1333
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ive"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1334
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x61

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x74

    if-ne v1, v2, :cond_2

    .line 1335
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1336
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1337
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1338
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1339
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1340
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1342
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ative"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1343
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1347
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    const/16 v3, 0x6f

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1348
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    const/16 v3, 0x6e

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1349
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1351
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1352
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1353
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private izeEndings()V
    .locals 5

    .prologue
    const/16 v3, 0x69

    const/16 v4, 0x65

    .line 886
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 888
    .local v0, "old_k":I
    const/16 v1, 0x7a

    invoke-direct {p0, v3, v1, v4}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 889
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 890
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 891
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 910
    :cond_0
    :goto_0
    return-void

    .line 892
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 894
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->doubleC(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 895
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 896
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 897
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 898
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 901
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 902
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 903
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 904
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 905
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 906
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ize"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 907
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto :goto_0
.end method

.method private lookup()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 576
    sget-object v0, Lorg/apache/lucene/analysis/en/KStemmer;->dict_ht:Lorg/apache/lucene/analysis/util/CharArrayMap;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->getArray()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->size()I

    move-result v3

    invoke-virtual {v0, v2, v1, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 577
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private lyEndings()V
    .locals 7

    .prologue
    const/16 v6, 0x6c

    const/16 v5, 0x65

    const/16 v4, 0x61

    const/16 v3, 0x79

    .line 1189
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1191
    .local v0, "old_k":I
    invoke-direct {p0, v6, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1193
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1195
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return-void

    .line 1196
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1198
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1199
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1201
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1203
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-eq v1, v6, :cond_0

    .line 1213
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ly"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1214
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1216
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x62

    if-ne v1, v2, :cond_3

    .line 1225
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2, v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 1226
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto :goto_0

    .line 1230
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_4

    .line 1231
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1232
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1233
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1234
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1235
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1236
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ily"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 1237
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1240
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 1242
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private matched()Z
    .locals 1

    .prologue
    .line 1401
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private mentEndings()V
    .locals 5

    .prologue
    .line 871
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 873
    .local v0, "old_k":I
    const/16 v1, 0x6d

    const/16 v2, 0x65

    const/16 v3, 0x6e

    const/16 v4, 0x74

    invoke-direct {p0, v1, v2, v3, v4}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCCC)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 874
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 875
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 876
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 881
    :cond_0
    :goto_0
    return-void

    .line 877
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v2, "ment"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 878
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto :goto_0
.end method

.method private nceEndings()V
    .locals 5

    .prologue
    const/16 v4, 0x65

    .line 818
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 821
    .local v0, "old_k":I
    const/16 v2, 0x6e

    const/16 v3, 0x63

    invoke-direct {p0, v2, v3, v4}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 822
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    .line 823
    .local v1, "word_char":C
    if-eq v1, v4, :cond_1

    const/16 v2, 0x61

    if-eq v1, v2, :cond_1

    .line 839
    .end local v1    # "word_char":C
    :cond_0
    :goto_0
    return-void

    .line 824
    .restart local v1    # "word_char":C
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 825
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 826
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 827
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 828
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 832
    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 833
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v2

    if-nez v2, :cond_0

    .line 834
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 835
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const-string v3, "nce"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 836
    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto :goto_0
.end method

.method private ncyEndings()V
    .locals 5

    .prologue
    const/16 v4, 0x65

    const/16 v3, 0x63

    .line 915
    const/16 v0, 0x6e

    const/16 v1, 0x79

    invoke-direct {p0, v0, v3, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v0

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x61

    if-eq v0, v1, :cond_1

    .line 928
    :cond_0
    :goto_0
    return-void

    .line 917
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x2

    const/16 v2, 0x74

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 918
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 919
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 921
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v0

    if-nez v0, :cond_0

    .line 923
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 924
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v0, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 925
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 926
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0
.end method

.method private nessEndings()V
    .locals 3

    .prologue
    const/16 v2, 0x73

    .line 844
    const/16 v0, 0x6e

    const/16 v1, 0x65

    invoke-direct {p0, v0, v1, v2, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCCC)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 848
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 849
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 850
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x69

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    const/16 v2, 0x79

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setCharAt(IC)V

    .line 851
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    .line 853
    :cond_1
    return-void
.end method

.method private pastTense()V
    .locals 5

    .prologue
    const/16 v4, 0x65

    const/16 v3, 0x64

    .line 588
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    const/16 v1, 0x69

    invoke-direct {p0, v1, v4, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 591
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 592
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 593
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 595
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 596
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 597
    const-string/jumbo v1, "y"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->setSuffix(Ljava/lang/String;)V

    .line 598
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 603
    :cond_2
    invoke-direct {p0, v4, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->vowelInStem()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 605
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 606
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 608
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->wordInDict()Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    move-result-object v0

    .line 609
    .local v0, "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    if-eqz v0, :cond_3

    iget-boolean v1, v0, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;->exception:Z

    if-eqz v1, :cond_0

    .line 616
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 617
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 618
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 627
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->doubleC(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 628
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 629
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 630
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 631
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v3, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 632
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 633
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto/16 :goto_0

    .line 641
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x75

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x6e

    if-ne v1, v2, :cond_5

    .line 642
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 643
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 644
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0

    .line 654
    :cond_5
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 655
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 656
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    goto/16 :goto_0
.end method

.method private penultChar()C
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v0

    return v0
.end method

.method private plural()V
    .locals 5

    .prologue
    const/16 v4, 0x65

    const/16 v3, 0x73

    .line 490
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_0

    .line 491
    const/16 v1, 0x69

    invoke-direct {p0, v1, v4, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 492
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 493
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 494
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 496
    :cond_1
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 497
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 498
    const-string/jumbo v1, "y"

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/en/KStemmer;->setSuffix(Ljava/lang/String;)V

    .line 499
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 500
    :cond_2
    invoke-direct {p0, v4, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CC)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 502
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 503
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 518
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    if-lez v1, :cond_5

    .line 519
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->charAt(I)C

    move-result v1

    if-eq v1, v3, :cond_5

    .line 518
    :cond_3
    const/4 v0, 0x1

    .line 520
    .local v0, "tryE":Z
    :goto_1
    if-eqz v0, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 524
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 525
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 526
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    move-result v1

    if-nez v1, :cond_0

    .line 529
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 530
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 532
    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto :goto_0

    .line 518
    .end local v0    # "tryE":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 535
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->penultChar()C

    move-result v1

    if-eq v1, v3, :cond_0

    const/16 v1, 0x6f

    const/16 v2, 0x75

    invoke-direct {p0, v1, v2, v3}, Lorg/apache/lucene/analysis/en/KStemmer;->endsIn(CCC)Z

    move-result v1

    if-nez v1, :cond_0

    .line 538
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 539
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 540
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lookup()Z

    goto/16 :goto_0
.end method

.method private setSuff(Ljava/lang/String;I)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "len"    # I

    .prologue
    .line 552
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    iget v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->setLength(I)V

    .line 553
    const/4 v0, 0x0

    .local v0, "l":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 556
    iget v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/2addr v1, p2

    iput v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 557
    return-void

    .line 554
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 553
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setSuffix(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 547
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/en/KStemmer;->setSuff(Ljava/lang/String;I)V

    .line 548
    return-void
.end method

.method private stemLength()I
    .locals 1

    .prologue
    .line 427
    iget v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->j:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private vowelInStem()Z
    .locals 2

    .prologue
    .line 671
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->stemLength()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 674
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 672
    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/en/KStemmer;->isVowel(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    .line 671
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private wordInDict()Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    .locals 5

    .prologue
    .line 479
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 485
    :cond_0
    :goto_0
    return-object v0

    .line 480
    :cond_1
    sget-object v1, Lorg/apache/lucene/analysis/en/KStemmer;->dict_ht:Lorg/apache/lucene/analysis/util/CharArrayMap;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->getArray()[C

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 481
    .local v0, "e":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;->exception:Z

    if-nez v1, :cond_0

    .line 482
    iput-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    goto :goto_0
.end method


# virtual methods
.method asCharSequence()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1377
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->result:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->result:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    goto :goto_0
.end method

.method asString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1371
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 1372
    .local v0, "s":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1373
    .end local v0    # "s":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "s":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method getChars()[C
    .locals 1

    .prologue
    .line 1385
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->getArray()[C

    move-result-object v0

    return-object v0
.end method

.method getLength()I
    .locals 1

    .prologue
    .line 1389
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->length()I

    move-result v0

    return v0
.end method

.method getString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1381
    iget-object v0, p0, Lorg/apache/lucene/analysis/en/KStemmer;->result:Ljava/lang/String;

    return-object v0
.end method

.method stem(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "term"    # Ljava/lang/String;

    .prologue
    .line 1362
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lorg/apache/lucene/analysis/en/KStemmer;->stem([CI)Z

    move-result v0

    .line 1363
    .local v0, "changed":Z
    if-nez v0, :cond_0

    .line 1364
    .end local p1    # "term":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "term":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->asString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method stem([CI)Z
    .locals 8
    .param p1, "term"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1409
    iput-object v7, p0, Lorg/apache/lucene/analysis/en/KStemmer;->result:Ljava/lang/String;

    .line 1411
    add-int/lit8 v5, p2, -0x1

    iput v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    .line 1412
    iget v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    if-le v5, v3, :cond_0

    iget v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->k:I

    const/16 v6, 0x31

    if-lt v5, v6, :cond_2

    :cond_0
    move v3, v4

    .line 1522
    :cond_1
    :goto_0
    return v3

    .line 1418
    :cond_2
    sget-object v5, Lorg/apache/lucene/analysis/en/KStemmer;->dict_ht:Lorg/apache/lucene/analysis/util/CharArrayMap;

    invoke-virtual {v5, p1, v4, p2}, Lorg/apache/lucene/analysis/util/CharArrayMap;->get([CII)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 1419
    .local v1, "entry":Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;
    if-eqz v1, :cond_4

    .line 1420
    iget-object v5, v1, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;->root:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 1421
    iget-object v4, v1, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;->root:Ljava/lang/String;

    iput-object v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->result:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v3, v4

    .line 1424
    goto :goto_0

    .line 1435
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v5}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->reset()V

    .line 1437
    iget-object v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    add-int/lit8 v6, p2, 0xa

    invoke-virtual {v5, v6}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->reserve(I)V

    .line 1438
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-lt v2, p2, :cond_6

    .line 1446
    iput-object v7, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 1459
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->plural()V

    .line 1460
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1501
    :cond_5
    :goto_2
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/KStemmer;->matchedEntry:Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    .line 1502
    if-eqz v1, :cond_1

    .line 1503
    iget-object v4, v1, Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;->root:Ljava/lang/String;

    iput-object v4, p0, Lorg/apache/lucene/analysis/en/KStemmer;->result:Ljava/lang/String;

    goto :goto_0

    .line 1439
    :cond_6
    aget-char v0, p1, v2

    .line 1440
    .local v0, "ch":C
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/en/KStemmer;->isAlpha(C)Z

    move-result v5

    if-nez v5, :cond_7

    move v3, v4

    goto :goto_0

    .line 1443
    :cond_7
    iget-object v5, p0, Lorg/apache/lucene/analysis/en/KStemmer;->word:Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 1438
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1461
    .end local v0    # "ch":C
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->pastTense()V

    .line 1462
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1463
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->aspect()V

    .line 1464
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1465
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->ityEndings()V

    .line 1466
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1467
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->nessEndings()V

    .line 1468
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1469
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->ionEndings()V

    .line 1470
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1471
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->erAndOrEndings()V

    .line 1472
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1473
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->lyEndings()V

    .line 1474
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1475
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->alEndings()V

    .line 1476
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1477
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->wordInDict()Lorg/apache/lucene/analysis/en/KStemmer$DictEntry;

    move-result-object v1

    .line 1478
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->iveEndings()V

    .line 1479
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1480
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->izeEndings()V

    .line 1481
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1482
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->mentEndings()V

    .line 1483
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1484
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->bleEndings()V

    .line 1485
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1486
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->ismEndings()V

    .line 1487
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1488
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->icEndings()V

    .line 1489
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1490
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->ncyEndings()V

    .line 1491
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1492
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->nceEndings()V

    .line 1493
    invoke-direct {p0}, Lorg/apache/lucene/analysis/en/KStemmer;->matched()Z

    goto/16 :goto_2
.end method
