.class public abstract enum Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;
.super Ljava/lang/Enum;
.source "EdgeNGramTokenFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "Side"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BACK:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

.field public static final enum FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side$1;

    const-string v1, "FRONT"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side$1;-><init>(Ljava/lang/String;I)V

    .line 43
    sput-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    .line 48
    new-instance v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side$2;

    const-string v1, "BACK"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side$2;-><init>(Ljava/lang/String;I)V

    .line 49
    sput-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->BACK:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    sget-object v1, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->BACK:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    aput-object v1, v0, v3

    sput-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->ENUM$VALUES:[Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getSide(Ljava/lang/String;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;
    .locals 1
    .param p0, "sideName"    # Ljava/lang/String;

    .prologue
    .line 58
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    .line 64
    :goto_0
    return-object v0

    .line 61
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->BACK:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->BACK:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    goto :goto_0

    .line 64
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->ENUM$VALUES:[Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public abstract getLabel()Ljava/lang/String;
.end method
