.class public Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "EdgeNGramFilterFactory.java"


# instance fields
.field private final maxGramSize:I

.field private final minGramSize:I

.field private final side:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x1

    .line 41
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 42
    const-string v0, "minGramSize"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->minGramSize:I

    .line 43
    const-string v0, "maxGramSize"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->maxGramSize:I

    .line 44
    const-string v0, "side"

    sget-object v1, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->side:Ljava/lang/String;

    .line 45
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 52
    new-instance v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->side:Ljava/lang/String;

    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->minGramSize:I

    iget v3, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramFilterFactory;->maxGramSize:I

    invoke-direct {v0, p1, v1, v2, v3}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/lang/String;II)V

    return-object v0
.end method
