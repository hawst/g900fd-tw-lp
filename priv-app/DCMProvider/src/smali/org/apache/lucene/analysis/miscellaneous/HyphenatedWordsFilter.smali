.class public final Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "HyphenatedWordsFilter.java"


# instance fields
.field private exhausted:Z

.field private final hyphenated:Ljava/lang/StringBuilder;

.field private lastEndOffset:I

.field private final offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private savedState:Lorg/apache/lucene/util/AttributeSource$State;

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 57
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 58
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    .line 62
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->exhausted:Z

    .line 63
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->lastEndOffset:I

    .line 72
    return-void
.end method

.method private unhyphenate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 133
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 134
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 136
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    .line 137
    .local v1, "term":[C
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 138
    .local v0, "length":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    if-le v0, v2, :cond_0

    .line 139
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v1

    .line 142
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5, v0, v1, v5}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 143
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 144
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->lastEndOffset:I

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 145
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 146
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x2d

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 79
    :goto_0
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->exhausted:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v4

    if-nez v4, :cond_2

    .line 102
    :cond_0
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->exhausted:Z

    .line 104
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v4, :cond_5

    .line 107
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->unhyphenate()V

    .line 112
    :cond_1
    :goto_1
    return v2

    .line 80
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 81
    .local v0, "term":[C
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 82
    .local v1, "termLength":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->lastEndOffset:I

    .line 84
    if-lez v1, :cond_4

    add-int/lit8 v4, v1, -0x1

    aget-char v4, v0, v4

    if-ne v4, v6, :cond_4

    .line 87
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    if-nez v4, :cond_3

    .line 88
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 90
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v0, v3, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 91
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v4, :cond_1

    .line 96
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v3, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 97
    invoke-direct {p0}, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->unhyphenate()V

    goto :goto_1

    .end local v0    # "term":[C
    .end local v1    # "termLength":I
    :cond_5
    move v2, v3

    .line 112
    goto :goto_1
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->hyphenated:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 123
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->exhausted:Z

    .line 124
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/HyphenatedWordsFilter;->lastEndOffset:I

    .line 125
    return-void
.end method
