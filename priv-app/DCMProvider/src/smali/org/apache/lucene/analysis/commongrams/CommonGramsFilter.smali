.class public final Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CommonGramsFilter.java"


# static fields
.field public static final GRAM_TYPE:Ljava/lang/String; = "gram"

.field private static final SEPARATOR:C = '_'


# instance fields
.field private final buffer:Ljava/lang/StringBuilder;

.field private final commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private lastStartOffset:I

.field private lastWasCommon:Z

.field private final offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final posLenAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

.field private savedState:Lorg/apache/lucene/util/AttributeSource$State;

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "commonWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 82
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    .line 62
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 63
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 64
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 65
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 66
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->posLenAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    .line 83
    iput-object p3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 84
    return-void
.end method

.method private gramToken()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 165
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    invoke-virtual {v3, v4, v6, v5}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 166
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v0

    .line 168
    .local v0, "endOffset":I
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->clearAttributes()V

    .line 170
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 171
    .local v1, "length":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    .line 172
    .local v2, "termText":[C
    array-length v3, v2

    if-le v1, v3, :cond_0

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v2

    .line 176
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6, v1, v2, v6}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 177
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 178
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 179
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->posLenAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;->setPositionLength(I)V

    .line 180
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v4, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->lastStartOffset:I

    invoke-interface {v3, v4, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 181
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string v4, "gram"

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 182
    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 183
    return-void
.end method

.method private isCommon()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 147
    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->commonWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private saveTermBuffer()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 156
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->lastStartOffset:I

    .line 158
    invoke-direct {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->isCommon()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->lastWasCommon:Z

    .line 159
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 105
    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 107
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->saveTermBuffer()V

    .line 125
    :goto_0
    return v0

    .line 110
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-nez v1, :cond_1

    .line 111
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->lastWasCommon:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->isCommon()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 119
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 120
    invoke-direct {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->gramToken()V

    goto :goto_0

    .line 124
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->saveTermBuffer()V

    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 134
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->lastWasCommon:Z

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->savedState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/analysis/commongrams/CommonGramsFilter;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 137
    return-void
.end method
