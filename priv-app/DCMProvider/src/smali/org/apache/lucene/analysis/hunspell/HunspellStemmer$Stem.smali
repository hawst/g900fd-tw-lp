.class public Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;
.super Ljava/lang/Object;
.source "HunspellStemmer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/hunspell/HunspellStemmer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Stem"
.end annotation


# instance fields
.field private final prefixes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;"
        }
    .end annotation
.end field

.field private final stem:[C

.field private final stemLength:I

.field private final suffixes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([CI)V
    .locals 1
    .param p1, "stem"    # [C
    .param p2, "stemLength"    # I

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->prefixes:Ljava/util/List;

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->suffixes:Ljava/util/List;

    .line 232
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stem:[C

    .line 233
    iput p2, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stemLength:I

    .line 234
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;)[C
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stem:[C

    return-object v0
.end method


# virtual methods
.method public addPrefix(Lorg/apache/lucene/analysis/hunspell/HunspellAffix;)V
    .locals 2
    .param p1, "prefix"    # Lorg/apache/lucene/analysis/hunspell/HunspellAffix;

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->prefixes:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 244
    return-void
.end method

.method public addSuffix(Lorg/apache/lucene/analysis/hunspell/HunspellAffix;)V
    .locals 1
    .param p1, "suffix"    # Lorg/apache/lucene/analysis/hunspell/HunspellAffix;

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->suffixes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    return-void
.end method

.method public getPrefixes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->prefixes:Ljava/util/List;

    return-object v0
.end method

.method public getStem()[C
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stem:[C

    return-object v0
.end method

.method public getStemLength()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stemLength:I

    return v0
.end method

.method public getStemString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 291
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stem:[C

    const/4 v2, 0x0

    iget v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->stemLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public getSuffixes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/analysis/hunspell/HunspellAffix;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemmer$Stem;->suffixes:Ljava/util/List;

    return-object v0
.end method
