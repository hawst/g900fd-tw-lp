.class public final Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;
.super Ljava/lang/Object;
.source "StemmerOverrideFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StemmerOverrideMap"
.end annotation


# instance fields
.field private final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final ignoreCase:Z


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/fst/FST;Z)V
    .locals 0
    .param p2, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 105
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->ignoreCase:Z

    .line 106
    return-void
.end method


# virtual methods
.method get([CILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/BytesRef;
    .locals 6
    .param p1, "buffer"    # [C
    .param p2, "bufferLen"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([CI",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Lorg/apache/lucene/util/fst/FST$BytesReader;",
            ")",
            "Lorg/apache/lucene/util/BytesRef;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    .local p3, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .local p4, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, v4, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/util/BytesRef;

    .line 124
    .local v3, "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    const/4 v2, 0x0

    .line 125
    .local v2, "matchOutput":Lorg/apache/lucene/util/BytesRef;
    const/4 v0, 0x0

    .line 126
    .local v0, "bufUpto":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v4, p3}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 127
    :goto_0
    if-lt v0, p2, :cond_1

    .line 135
    invoke-virtual {p3}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 136
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v5, v4, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v4, p3, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v4, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5, v3, v4}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "matchOutput":Lorg/apache/lucene/util/BytesRef;
    check-cast v2, Lorg/apache/lucene/util/BytesRef;

    .restart local v2    # "matchOutput":Lorg/apache/lucene/util/BytesRef;
    :cond_0
    move-object v4, v2

    .line 138
    :goto_1
    return-object v4

    .line 128
    :cond_1
    invoke-static {p1, v0, p2}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v1

    .line 129
    .local v1, "codePoint":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->ignoreCase:Z

    if-eqz v4, :cond_2

    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    :goto_2
    invoke-virtual {v5, v4, p3, p3, p4}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    if-nez v4, :cond_3

    .line 130
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    move v4, v1

    .line 129
    goto :goto_2

    .line 132
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v5, v4, Lorg/apache/lucene/util/fst/FST;->outputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v4, p3, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v4, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5, v3, v4}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    check-cast v3, Lorg/apache/lucene/util/BytesRef;

    .line 133
    .restart local v3    # "pendingOutput":Lorg/apache/lucene/util/BytesRef;
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0
.end method

.method getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/StemmerOverrideFilter$StemmerOverrideMap;->fst:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    goto :goto_0
.end method
