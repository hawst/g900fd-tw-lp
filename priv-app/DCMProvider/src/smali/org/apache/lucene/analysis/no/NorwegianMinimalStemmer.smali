.class public Lorg/apache/lucene/analysis/no/NorwegianMinimalStemmer;
.super Ljava/lang/Object;
.source "NorwegianMinimalStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v2, 0x4

    .line 66
    if-le p2, v2, :cond_0

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_0

    .line 67
    add-int/lit8 p2, p2, -0x1

    .line 69
    :cond_0
    const/4 v0, 0x5

    if-le p2, v0, :cond_2

    .line 70
    const-string v0, "ene"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    add-int/lit8 p2, p2, -0x3

    .line 88
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 74
    .restart local p2    # "len":I
    :cond_2
    if-le p2, v2, :cond_4

    .line 75
    const-string v0, "er"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 76
    const-string v0, "en"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 77
    const-string v0, "et"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    :cond_3
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 81
    :cond_4
    const/4 v0, 0x3

    if-le p2, v0, :cond_1

    .line 82
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 85
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 82
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
    .end sparse-switch
.end method
