.class public final Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "GreekLowerCaseFilter.java"


# instance fields
.field private final charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 49
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 38
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 50
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 51
    return-void
.end method

.method private lowerCase(I)I
    .locals 1
    .param p1, "codepoint"    # I

    .prologue
    .line 69
    sparse-switch p1, :sswitch_data_0

    .line 125
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v0

    :goto_0
    return v0

    .line 77
    :sswitch_0
    const/16 v0, 0x3c3

    goto :goto_0

    .line 85
    :sswitch_1
    const/16 v0, 0x3b1

    goto :goto_0

    .line 89
    :sswitch_2
    const/16 v0, 0x3b5

    goto :goto_0

    .line 93
    :sswitch_3
    const/16 v0, 0x3b7

    goto :goto_0

    .line 100
    :sswitch_4
    const/16 v0, 0x3b9

    goto :goto_0

    .line 107
    :sswitch_5
    const/16 v0, 0x3c5

    goto :goto_0

    .line 111
    :sswitch_6
    const/16 v0, 0x3bf

    goto :goto_0

    .line 115
    :sswitch_7
    const/16 v0, 0x3c9

    goto :goto_0

    .line 122
    :sswitch_8
    const/16 v0, 0x3c2

    goto :goto_0

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        0x386 -> :sswitch_1
        0x388 -> :sswitch_2
        0x389 -> :sswitch_3
        0x38a -> :sswitch_4
        0x38c -> :sswitch_6
        0x38e -> :sswitch_5
        0x38f -> :sswitch_7
        0x390 -> :sswitch_4
        0x3a2 -> :sswitch_8
        0x3aa -> :sswitch_4
        0x3ab -> :sswitch_5
        0x3ac -> :sswitch_1
        0x3ad -> :sswitch_2
        0x3ae -> :sswitch_3
        0x3af -> :sswitch_4
        0x3b0 -> :sswitch_5
        0x3c2 -> :sswitch_0
        0x3ca -> :sswitch_4
        0x3cb -> :sswitch_5
        0x3cc -> :sswitch_6
        0x3cd -> :sswitch_5
        0x3ce -> :sswitch_7
    .end sparse-switch
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v3, p0, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 56
    iget-object v3, p0, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 57
    .local v0, "chArray":[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 58
    .local v1, "chLen":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 62
    const/4 v3, 0x1

    .line 64
    .end local v0    # "chArray":[C
    .end local v1    # "chLen":I
    .end local v2    # "i":I
    :goto_1
    return v3

    .line 60
    .restart local v0    # "chArray":[C
    .restart local v1    # "chLen":I
    .restart local v2    # "i":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    invoke-virtual {v3, v0, v2}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CI)I

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/el/GreekLowerCaseFilter;->lowerCase(I)I

    move-result v3

    invoke-static {v3, v0, v2}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    .line 64
    .end local v0    # "chArray":[C
    .end local v1    # "chLen":I
    .end local v2    # "i":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method
