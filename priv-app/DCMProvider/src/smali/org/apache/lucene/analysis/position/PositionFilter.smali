.class public final Lorg/apache/lucene/analysis/position/PositionFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "PositionFilter.java"


# instance fields
.field private firstTokenPositioned:Z

.field private posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final positionIncrement:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/position/PositionFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;I)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "positionIncrement"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->firstTokenPositioned:Z

    .line 38
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/position/PositionFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 60
    if-gez p2, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "positionIncrement may not be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    iput p2, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->positionIncrement:I

    .line 64
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->firstTokenPositioned:Z

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v2, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->positionIncrement:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 76
    :goto_0
    return v0

    .line 72
    :cond_0
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->firstTokenPositioned:Z

    goto :goto_0

    .line 76
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/position/PositionFilter;->firstTokenPositioned:Z

    .line 84
    return-void
.end method
