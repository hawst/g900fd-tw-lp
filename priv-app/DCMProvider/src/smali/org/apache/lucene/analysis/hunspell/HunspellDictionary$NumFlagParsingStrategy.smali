.class Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;
.super Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;
.source "HunspellDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NumFlagParsingStrategy"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 460
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;)V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;)V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$NumFlagParsingStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public parseFlags(Ljava/lang/String;)[C
    .locals 6
    .param p1, "rawFlags"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 467
    .local v2, "rawFlagParts":[Ljava/lang/String;
    array-length v3, v2

    new-array v0, v3, [C

    .line 469
    .local v0, "flags":[C
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-lt v1, v3, :cond_0

    .line 474
    return-object v0

    .line 471
    :cond_0
    aget-object v3, v2, v1

    const-string v4, "[^0-9]"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-char v3, v3

    aput-char v3, v0, v1

    .line 469
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
