.class public Lorg/apache/lucene/analysis/id/IndonesianStemmer;
.super Ljava/lang/Object;
.source "IndonesianStemmer.java"


# static fields
.field private static final REMOVED_BER:I = 0x20

.field private static final REMOVED_DI:I = 0x4

.field private static final REMOVED_KE:I = 0x1

.field private static final REMOVED_MENG:I = 0x8

.field private static final REMOVED_PE:I = 0x40

.field private static final REMOVED_PENG:I = 0x2

.field private static final REMOVED_TER:I = 0x10


# instance fields
.field private flags:I

.field private numSyllables:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isVowel(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 78
    sparse-switch p1, :sswitch_data_0

    .line 86
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 84
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 78
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
    .end sparse-switch
.end method

.method private removeFirstOrderPrefix([CI)I
    .locals 6
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    const/16 v5, 0x73

    const/4 v4, 0x2

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 116
    const-string v0, "meng"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 118
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 119
    invoke-static {p1, v1, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    .line 203
    .end local p2    # "length":I
    :cond_0
    :goto_0
    return p2

    .line 122
    .restart local p2    # "length":I
    :cond_1
    const-string v0, "meny"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-le p2, v3, :cond_2

    aget-char v0, p1, v3

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 124
    aput-char v5, p1, v2

    .line 125
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 126
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 129
    :cond_2
    const-string v0, "men"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 131
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 132
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 135
    :cond_3
    const-string v0, "mem"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 136
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 137
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 138
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 141
    :cond_4
    const-string v0, "me"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 142
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 143
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 144
    invoke-static {p1, v1, p2, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 147
    :cond_5
    const-string v0, "peng"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 148
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 149
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 150
    invoke-static {p1, v1, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 153
    :cond_6
    const-string v0, "peny"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-le p2, v3, :cond_7

    aget-char v0, p1, v3

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 154
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 155
    aput-char v5, p1, v2

    .line 156
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 157
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 160
    :cond_7
    const-string v0, "peny"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 161
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 162
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 163
    invoke-static {p1, v1, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 166
    :cond_8
    const-string v0, "pen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    if-le p2, v2, :cond_9

    aget-char v0, p1, v2

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 167
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 168
    const/16 v0, 0x74

    aput-char v0, p1, v4

    .line 169
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 170
    invoke-static {p1, v1, p2, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 173
    :cond_9
    const-string v0, "pen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 174
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 175
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 176
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 179
    :cond_a
    const-string v0, "pem"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 180
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 181
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 182
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 185
    :cond_b
    const-string v0, "di"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 186
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 187
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 188
    invoke-static {p1, v1, p2, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 191
    :cond_c
    const-string v0, "ter"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 192
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 193
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 194
    invoke-static {p1, v1, p2, v2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0

    .line 197
    :cond_d
    const-string v0, "ke"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 199
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 200
    invoke-static {p1, v1, p2, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0
.end method

.method private removeParticle([CI)I
    .locals 1
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    .line 91
    const-string v0, "kah"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    const-string v0, "lah"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const-string v0, "pun"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 95
    add-int/lit8 p2, p2, -0x3

    .line 98
    .end local p2    # "length":I
    :cond_1
    return p2
.end method

.method private removePossessivePronoun([CI)I
    .locals 1
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    .line 102
    const-string v0, "ku"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mu"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 104
    add-int/lit8 p2, p2, -0x2

    .line 112
    .end local p2    # "length":I
    :cond_1
    :goto_0
    return p2

    .line 107
    .restart local p2    # "length":I
    :cond_2
    const-string v0, "nya"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 109
    add-int/lit8 p2, p2, -0x3

    goto :goto_0
.end method

.method private removeSecondOrderPrefix([CI)I
    .locals 7
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 207
    const-string v0, "ber"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 209
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 210
    invoke-static {p1, v2, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    .line 242
    .end local p2    # "length":I
    :cond_0
    :goto_0
    return p2

    .line 213
    .restart local p2    # "length":I
    :cond_1
    if-ne p2, v6, :cond_2

    const-string v0, "belajar"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 215
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 216
    invoke-static {p1, v2, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 219
    :cond_2
    const-string v0, "be"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-le p2, v5, :cond_3

    .line 220
    aget-char v0, p1, v4

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_3

    aget-char v0, p1, v3

    const/16 v1, 0x65

    if-ne v0, v1, :cond_3

    aget-char v0, p1, v5

    const/16 v1, 0x72

    if-ne v0, v1, :cond_3

    .line 221
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 222
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 223
    invoke-static {p1, v2, p2, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 226
    :cond_3
    const-string v0, "per"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 227
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 228
    invoke-static {p1, v2, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 231
    :cond_4
    if-ne p2, v6, :cond_5

    const-string v0, "pelajar"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 232
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 233
    invoke-static {p1, v2, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto :goto_0

    .line 236
    :cond_5
    const-string v0, "pe"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->startsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 238
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 239
    invoke-static {p1, v2, p2, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->deleteN([CIII)I

    move-result p2

    goto/16 :goto_0
.end method

.method private removeSuffix([CI)I
    .locals 1
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    .line 246
    const-string v0, "kan"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 247
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 248
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 249
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_1

    .line 250
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 251
    add-int/lit8 p2, p2, -0x3

    .line 270
    .end local p2    # "length":I
    :cond_0
    :goto_0
    return p2

    .line 254
    .restart local p2    # "length":I
    :cond_1
    const-string v0, "an"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 255
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_2

    .line 256
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_2

    .line 257
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_2

    .line 258
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 259
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 262
    :cond_2
    const-string v0, "i"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const-string v0, "si"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    .line 265
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 266
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 267
    iget v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 268
    add-int/lit8 p2, p2, -0x1

    goto :goto_0
.end method

.method private stemDerivational([CI)I
    .locals 3
    .param p1, "text"    # [C
    .param p2, "length"    # I

    .prologue
    const/4 v2, 0x2

    .line 63
    move v0, p2

    .line 64
    .local v0, "oldLength":I
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_0

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removeFirstOrderPrefix([CI)I

    move-result p2

    .line 65
    :cond_0
    if-eq v0, p2, :cond_3

    .line 66
    move v0, p2

    .line 67
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_1

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removeSuffix([CI)I

    move-result p2

    .line 68
    :cond_1
    if-eq v0, p2, :cond_2

    .line 69
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_2

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removeSecondOrderPrefix([CI)I

    move-result p2

    .line 74
    :cond_2
    :goto_0
    return p2

    .line 71
    :cond_3
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_4

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removeSecondOrderPrefix([CI)I

    move-result p2

    .line 72
    :cond_4
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_2

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removeSuffix([CI)I

    move-result p2

    goto :goto_0
.end method


# virtual methods
.method public stem([CIZ)I
    .locals 3
    .param p1, "text"    # [C
    .param p2, "length"    # I
    .param p3, "stemDerivational"    # Z

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 48
    iput v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->flags:I

    .line 49
    iput v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 50
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_3

    .line 54
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_0

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removeParticle([CI)I

    move-result p2

    .line 55
    :cond_0
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    if-le v1, v2, :cond_1

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->removePossessivePronoun([CI)I

    move-result p2

    .line 57
    :cond_1
    if-eqz p3, :cond_2

    .line 58
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->stemDerivational([CI)I

    move-result p2

    .line 59
    :cond_2
    return p2

    .line 51
    :cond_3
    aget-char v1, p1, v0

    invoke-direct {p0, v1}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->isVowel(C)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 52
    iget v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->numSyllables:I

    .line 50
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
