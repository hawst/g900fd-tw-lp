.class public final Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CJKBigramFilter.java"


# static fields
.field public static final DOUBLE_TYPE:Ljava/lang/String; = "<DOUBLE>"

.field public static final HAN:I = 0x1

.field public static final HANGUL:I = 0x8

.field private static final HANGUL_TYPE:Ljava/lang/String;

.field private static final HAN_TYPE:Ljava/lang/String;

.field public static final HIRAGANA:I = 0x2

.field private static final HIRAGANA_TYPE:Ljava/lang/String;

.field public static final KATAKANA:I = 0x4

.field private static final KATAKANA_TYPE:Ljava/lang/String;

.field private static final NO:Ljava/lang/Object;

.field public static final SINGLE_TYPE:Ljava/lang/String; = "<SINGLE>"


# instance fields
.field buffer:[I

.field bufferLen:I

.field private final doHan:Ljava/lang/Object;

.field private final doHangul:Ljava/lang/Object;

.field private final doHiragana:Ljava/lang/Object;

.field private final doKatakana:Ljava/lang/Object;

.field endOffset:[I

.field private exhausted:Z

.field index:I

.field lastEndOffset:I

.field private loneState:Lorg/apache/lucene/util/AttributeSource$State;

.field private ngramState:Z

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final outputUnigrams:Z

.field private final posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final posLengthAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

.field startOffset:[I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->HAN_TYPE:Ljava/lang/String;

    .line 66
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v1, 0xb

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->HIRAGANA_TYPE:Ljava/lang/String;

    .line 67
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v1, 0xc

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->KATAKANA_TYPE:Ljava/lang/String;

    .line 68
    sget-object v0, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->TOKEN_TYPES:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->HANGUL_TYPE:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->NO:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 108
    const/16 v0, 0xf

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;I)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;I)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "flags"    # I

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V

    .line 117
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;IZ)V
    .locals 2
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "flags"    # I
    .param p3, "outputUnigrams"    # Z

    .prologue
    const/16 v1, 0x8

    .line 129
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 83
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 84
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 85
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 86
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 87
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->posLengthAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    .line 90
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    .line 91
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    .line 92
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    .line 130
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->NO:Ljava/lang/Object;

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doHan:Ljava/lang/Object;

    .line 131
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->NO:Ljava/lang/Object;

    :goto_1
    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doHiragana:Ljava/lang/Object;

    .line 132
    and-int/lit8 v0, p2, 0x4

    if-nez v0, :cond_2

    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->NO:Ljava/lang/Object;

    :goto_2
    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doKatakana:Ljava/lang/Object;

    .line 133
    and-int/lit8 v0, p2, 0x8

    if-nez v0, :cond_3

    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->NO:Ljava/lang/Object;

    :goto_3
    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doHangul:Ljava/lang/Object;

    .line 134
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->outputUnigrams:Z

    .line 135
    return-void

    .line 130
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->HAN_TYPE:Ljava/lang/String;

    goto :goto_0

    .line 131
    :cond_1
    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->HIRAGANA_TYPE:Ljava/lang/String;

    goto :goto_1

    .line 132
    :cond_2
    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->KATAKANA_TYPE:Ljava/lang/String;

    goto :goto_2

    .line 133
    :cond_3
    sget-object v0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->HANGUL_TYPE:Ljava/lang/String;

    goto :goto_3
.end method

.method private doNext()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 233
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->loneState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v2, :cond_1

    .line 234
    iget-object v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->loneState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 235
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->loneState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 238
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->exhausted:Z

    if-eqz v2, :cond_2

    move v0, v1

    .line 239
    goto :goto_0

    .line 240
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_0

    .line 243
    iput-boolean v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->exhausted:Z

    move v0, v1

    .line 244
    goto :goto_0
.end method

.method private flushBigram()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 300
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->clearAttributes()V

    .line 301
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v2

    .line 302
    .local v2, "termBuffer":[C
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    iget v4, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    aget v3, v3, v4

    invoke-static {v3, v2, v7}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v0

    .line 303
    .local v0, "len1":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    iget v4, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    invoke-static {v3, v2, v0}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v3

    add-int v1, v0, v3

    .line 304
    .local v1, "len2":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 305
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    iget v5, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    aget v4, v4, v5

    iget-object v5, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    iget v6, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    add-int/lit8 v6, v6, 0x1

    aget v5, v5, v6

    invoke-interface {v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 306
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string v4, "<DOUBLE>"

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 308
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->outputUnigrams:Z

    if-eqz v3, :cond_0

    .line 309
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->posIncAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v7}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 310
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->posLengthAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;->setPositionLength(I)V

    .line 312
    :cond_0
    iget v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    .line 313
    return-void
.end method

.method private flushUnigram()V
    .locals 6

    .prologue
    .line 322
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->clearAttributes()V

    .line 323
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v1

    .line 324
    .local v1, "termBuffer":[C
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    iget v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    aget v2, v2, v3

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v0

    .line 325
    .local v0, "len":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 326
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    iget v4, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    aget v3, v3, v4

    iget-object v4, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    iget v5, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    aget v4, v4, v5

    invoke-interface {v2, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 327
    iget-object v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    const-string v3, "<SINGLE>"

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 328
    iget v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    .line 329
    return-void
.end method

.method private hasBufferedBigram()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 335
    iget v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    iget v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    sub-int/2addr v1, v2

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasBufferedUnigram()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 344
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->outputUnigrams:Z

    if-eqz v2, :cond_2

    .line 346
    iget v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    iget v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    sub-int/2addr v2, v3

    if-ne v2, v0, :cond_1

    .line 349
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 346
    goto :goto_0

    .line 349
    :cond_2
    iget v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    if-ne v2, v0, :cond_3

    iget v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private refill()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 255
    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    const/16 v10, 0x40

    if-le v9, v10, :cond_0

    .line 256
    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    add-int/lit8 v4, v9, -0x1

    .line 257
    .local v4, "last":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    iget-object v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    aget v10, v10, v4

    aput v10, v9, v11

    .line 258
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    iget-object v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    aget v10, v10, v4

    aput v10, v9, v11

    .line 259
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    iget-object v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    aget v10, v10, v4

    aput v10, v9, v11

    .line 260
    const/4 v9, 0x1

    iput v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    .line 261
    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    sub-int/2addr v9, v4

    iput v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    .line 264
    .end local v4    # "last":I
    :cond_0
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v8

    .line 265
    .local v8, "termBuffer":[C
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    .line 266
    .local v5, "len":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v7

    .line 267
    .local v7, "start":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v9}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v2

    .line 269
    .local v2, "end":I
    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    add-int v6, v9, v5

    .line 270
    .local v6, "newSize":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    invoke-static {v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v9

    iput-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    .line 271
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    invoke-static {v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v9

    iput-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    .line 272
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    invoke-static {v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v9

    iput-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    .line 273
    iput v2, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->lastEndOffset:I

    .line 275
    sub-int v9, v2, v7

    if-eq v9, v5, :cond_3

    .line 277
    const/4 v3, 0x0

    .local v3, "i":I
    const/4 v0, 0x0

    .local v0, "cp":I
    :goto_0
    if-lt v3, v5, :cond_2

    .line 293
    :cond_1
    return-void

    .line 278
    :cond_2
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    iget v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    invoke-static {v8, v3, v5}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v0

    .end local v0    # "cp":I
    aput v0, v9, v10

    .line 279
    .restart local v0    # "cp":I
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    iget v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    aput v7, v9, v10

    .line 280
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    iget v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    aput v2, v9, v10

    .line 281
    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    .line 277
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v9

    add-int/2addr v3, v9

    goto :goto_0

    .line 285
    .end local v0    # "cp":I
    .end local v3    # "i":I
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "i":I
    const/4 v0, 0x0

    .restart local v0    # "cp":I
    const/4 v1, 0x0

    .local v1, "cpLen":I
    :goto_1
    if-ge v3, v5, :cond_1

    .line 286
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->buffer:[I

    iget v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    invoke-static {v8, v3, v5}, Ljava/lang/Character;->codePointAt([CII)I

    move-result v0

    .end local v0    # "cp":I
    aput v0, v9, v10

    .line 287
    .restart local v0    # "cp":I
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    .line 288
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->startOffset:[I

    iget v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    aput v7, v9, v10

    .line 289
    iget-object v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->endOffset:[I

    iget v10, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    add-int/2addr v7, v1

    aput v7, v9, v10

    .line 290
    iget v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    .line 285
    add-int/2addr v3, v1

    goto :goto_1
.end method


# virtual methods
.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 145
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->hasBufferedBigram()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 150
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->outputUnigrams:Z

    if-eqz v3, :cond_3

    .line 158
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->ngramState:Z

    if-eqz v3, :cond_1

    .line 159
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->flushBigram()V

    .line 164
    :goto_1
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->ngramState:Z

    if-eqz v3, :cond_2

    :goto_2
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->ngramState:Z

    .line 222
    :cond_0
    :goto_3
    return v2

    .line 161
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->flushUnigram()V

    .line 162
    iget v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    goto :goto_1

    :cond_2
    move v1, v2

    .line 164
    goto :goto_2

    .line 166
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->flushBigram()V

    goto :goto_3

    .line 169
    :cond_4
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doNext()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "type":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doHan:Ljava/lang/Object;

    if-eq v0, v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doHiragana:Ljava/lang/Object;

    if-eq v0, v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doKatakana:Ljava/lang/Object;

    if-eq v0, v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->doHangul:Ljava/lang/Object;

    if-ne v0, v3, :cond_8

    .line 180
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->lastEndOffset:I

    if-eq v3, v4, :cond_7

    .line 181
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->hasBufferedUnigram()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 187
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->loneState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 188
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->flushUnigram()V

    goto :goto_3

    .line 191
    :cond_6
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    .line 192
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    .line 194
    :cond_7
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->refill()V

    goto :goto_0

    .line 199
    :cond_8
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->hasBufferedUnigram()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->captureState()Lorg/apache/lucene/util/AttributeSource$State;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->loneState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 206
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->flushUnigram()V

    goto :goto_3

    .line 218
    .end local v0    # "type":Ljava/lang/String;
    :cond_9
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->hasBufferedUnigram()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 219
    invoke-direct {p0}, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->flushUnigram()V

    goto :goto_3

    :cond_a
    move v2, v1

    .line 222
    goto :goto_3
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 355
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 356
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->bufferLen:I

    .line 357
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->index:I

    .line 358
    iput v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->lastEndOffset:I

    .line 359
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->loneState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 360
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->exhausted:Z

    .line 361
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/cjk/CJKBigramFilter;->ngramState:Z

    .line 362
    return-void
.end method
