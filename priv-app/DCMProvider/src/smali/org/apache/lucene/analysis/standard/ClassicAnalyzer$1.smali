.class Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;
.super Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.source "ClassicAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;

.field private final synthetic val$src:Lorg/apache/lucene/analysis/standard/ClassicTokenizer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/ClassicTokenizer;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/analysis/Tokenizer;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;

    iput-object p4, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    .line 115
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-void
.end method


# virtual methods
.method protected setReader(Ljava/io/Reader;)V
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;

    # getter for: Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I
    invoke-static {v1}, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->access$0(Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->setMaxTokenLength(I)V

    .line 119
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->setReader(Ljava/io/Reader;)V

    .line 120
    return-void
.end method
