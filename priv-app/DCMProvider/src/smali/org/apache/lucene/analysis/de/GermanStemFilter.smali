.class public final Lorg/apache/lucene/analysis/de/GermanStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "GermanStemFilter.java"


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private stemmer:Lorg/apache/lucene/analysis/de/GermanStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 47
    new-instance v0, Lorg/apache/lucene/analysis/de/GermanStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/de/GermanStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->stemmer:Lorg/apache/lucene/analysis/de/GermanStemmer;

    .line 49
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 50
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/de/GermanStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 59
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v2, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    iget-object v2, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "term":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v2

    if-nez v2, :cond_0

    .line 70
    iget-object v2, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->stemmer:Lorg/apache/lucene/analysis/de/GermanStemmer;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/analysis/de/GermanStemmer;->stem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "s":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v2

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 75
    .end local v0    # "s":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    .line 77
    .end local v1    # "term":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setStemmer(Lorg/apache/lucene/analysis/de/GermanStemmer;)V
    .locals 0
    .param p1, "stemmer"    # Lorg/apache/lucene/analysis/de/GermanStemmer;

    .prologue
    .line 86
    if-eqz p1, :cond_0

    .line 87
    iput-object p1, p0, Lorg/apache/lucene/analysis/de/GermanStemFilter;->stemmer:Lorg/apache/lucene/analysis/de/GermanStemmer;

    .line 89
    :cond_0
    return-void
.end method
