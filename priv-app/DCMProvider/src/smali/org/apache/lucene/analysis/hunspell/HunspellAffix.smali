.class public Lorg/apache/lucene/analysis/hunspell/HunspellAffix;
.super Ljava/lang/Object;
.source "HunspellAffix.java"


# instance fields
.field private append:Ljava/lang/String;

.field private appendFlags:[C

.field private condition:Ljava/lang/String;

.field private conditionPattern:Ljava/util/regex/Pattern;

.field private crossProduct:Z

.field private flag:C

.field private strip:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public checkCondition(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->conditionPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public getAppend()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->append:Ljava/lang/String;

    return-object v0
.end method

.method public getAppendFlags()[C
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->appendFlags:[C

    return-object v0
.end method

.method public getCondition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->condition:Ljava/lang/String;

    return-object v0
.end method

.method public getFlag()C
    .locals 1

    .prologue
    .line 128
    iget-char v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->flag:C

    return v0
.end method

.method public getStrip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->strip:Ljava/lang/String;

    return-object v0
.end method

.method public isCrossProduct()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->crossProduct:Z

    return v0
.end method

.method public setAppend(Ljava/lang/String;)V
    .locals 0
    .param p1, "append"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->append:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setAppendFlags([C)V
    .locals 0
    .param p1, "appendFlags"    # [C

    .prologue
    .line 81
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->appendFlags:[C

    .line 82
    return-void
.end method

.method public setCondition(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "condition"    # Ljava/lang/String;
    .param p2, "pattern"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->condition:Ljava/lang/String;

    .line 119
    invoke-static {p2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->conditionPattern:Ljava/util/regex/Pattern;

    .line 120
    return-void
.end method

.method public setCrossProduct(Z)V
    .locals 0
    .param p1, "crossProduct"    # Z

    .prologue
    .line 155
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->crossProduct:Z

    .line 156
    return-void
.end method

.method public setFlag(C)V
    .locals 0
    .param p1, "flag"    # C

    .prologue
    .line 137
    iput-char p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->flag:C

    .line 138
    return-void
.end method

.method public setStrip(Ljava/lang/String;)V
    .locals 0
    .param p1, "strip"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellAffix;->strip:Ljava/lang/String;

    .line 100
    return-void
.end method
