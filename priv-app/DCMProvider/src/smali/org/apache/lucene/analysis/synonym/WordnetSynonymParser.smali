.class public Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;
.super Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;
.source "WordnetSynonymParser.java"


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private final expand:Z


# direct methods
.method public constructor <init>(ZZLorg/apache/lucene/analysis/Analyzer;)V
    .locals 0
    .param p1, "dedup"    # Z
    .param p2, "expand"    # Z
    .param p3, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;-><init>(Z)V

    .line 41
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->expand:Z

    .line 42
    iput-object p3, p0, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 43
    return-void
.end method

.method private addInternal([Lorg/apache/lucene/util/CharsRef;I)V
    .locals 5
    .param p1, "synset"    # [Lorg/apache/lucene/util/CharsRef;
    .param p2, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 96
    const/4 v2, 0x1

    if-gt p2, v2, :cond_1

    .line 111
    :cond_0
    return-void

    .line 100
    :cond_1
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->expand:Z

    if-eqz v2, :cond_3

    .line 101
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 102
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-lt v1, p2, :cond_2

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_2
    aget-object v2, p1, v0

    aget-object v3, p1, v1

    invoke-virtual {p0, v2, v3, v4}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->add(Lorg/apache/lucene/util/CharsRef;Lorg/apache/lucene/util/CharsRef;Z)V

    .line 102
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 107
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, p2, :cond_0

    .line 108
    aget-object v2, p1, v0

    aget-object v3, p1, v4

    invoke-virtual {p0, v2, v3, v4}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->add(Lorg/apache/lucene/util/CharsRef;Lorg/apache/lucene/util/CharsRef;Z)V

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private parseSynonym(Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;
    .locals 6
    .param p1, "line"    # Ljava/lang/String;
    .param p2, "reuse"    # Lorg/apache/lucene/util/CharsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x27

    .line 84
    if-nez p2, :cond_0

    .line 85
    new-instance p2, Lorg/apache/lucene/util/CharsRef;

    .end local p2    # "reuse":Lorg/apache/lucene/util/CharsRef;
    const/16 v3, 0x8

    invoke-direct {p2, v3}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    .line 88
    .restart local p2    # "reuse":Lorg/apache/lucene/util/CharsRef;
    :cond_0
    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v1, v3, 0x1

    .line 89
    .local v1, "start":I
    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 91
    .local v0, "end":I
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\'\'"

    const-string v5, "\'"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "text":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-static {v3, v2, p2}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->analyze(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public add(Ljava/io/Reader;)V
    .locals 11
    .param p1, "in"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Ljava/io/LineNumberReader;

    invoke-direct {v0, p1}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    .line 48
    .local v0, "br":Ljava/io/LineNumberReader;
    const/4 v5, 0x0

    .line 49
    .local v5, "line":Ljava/lang/String;
    :try_start_0
    const-string v4, ""

    .line 50
    .local v4, "lastSynSetID":Ljava/lang/String;
    const/16 v9, 0x8

    new-array v7, v9, [Lorg/apache/lucene/util/CharsRef;

    .line 51
    .local v7, "synset":[Lorg/apache/lucene/util/CharsRef;
    const/4 v8, 0x0

    .line 53
    .local v8, "synsetSize":I
    :goto_0
    invoke-virtual {v0}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    .line 73
    invoke-direct {p0, v7, v8}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->addInternal([Lorg/apache/lucene/util/CharsRef;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    invoke-virtual {v0}, Ljava/io/LineNumberReader;->close()V

    .line 81
    return-void

    .line 54
    :cond_0
    const/4 v9, 0x2

    const/16 v10, 0xb

    :try_start_1
    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 56
    .local v6, "synSetID":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 57
    invoke-direct {p0, v7, v8}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->addInternal([Lorg/apache/lucene/util/CharsRef;I)V

    .line 58
    const/4 v8, 0x0

    .line 61
    :cond_1
    array-length v9, v7

    add-int/lit8 v10, v8, 0x1

    if-gt v9, v10, :cond_2

    .line 62
    array-length v9, v7

    mul-int/lit8 v9, v9, 0x2

    new-array v3, v9, [Lorg/apache/lucene/util/CharsRef;

    .line 63
    .local v3, "larger":[Lorg/apache/lucene/util/CharsRef;
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v9, v3, v10, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    move-object v7, v3

    .line 67
    .end local v3    # "larger":[Lorg/apache/lucene/util/CharsRef;
    :cond_2
    aget-object v9, v7, v8

    invoke-direct {p0, v5, v9}, Lorg/apache/lucene/analysis/synonym/WordnetSynonymParser;->parseSynonym(Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v9

    aput-object v9, v7, v8
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    add-int/lit8 v8, v8, 0x1

    .line 69
    move-object v4, v6

    goto :goto_0

    .line 74
    .end local v4    # "lastSynSetID":Ljava/lang/String;
    .end local v6    # "synSetID":Ljava/lang/String;
    .end local v7    # "synset":[Lorg/apache/lucene/util/CharsRef;
    .end local v8    # "synsetSize":I
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    new-instance v2, Ljava/text/ParseException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid synonym rule at line "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/LineNumberReader;->getLineNumber()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v2, v9, v10}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 76
    .local v2, "ex":Ljava/text/ParseException;
    invoke-virtual {v2, v1}, Ljava/text/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 77
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 78
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "ex":Ljava/text/ParseException;
    :catchall_0
    move-exception v9

    .line 79
    invoke-virtual {v0}, Ljava/io/LineNumberReader;->close()V

    .line 80
    throw v9
.end method
