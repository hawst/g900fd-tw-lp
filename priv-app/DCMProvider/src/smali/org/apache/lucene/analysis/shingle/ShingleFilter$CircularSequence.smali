.class Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;
.super Ljava/lang/Object;
.source "ShingleFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/shingle/ShingleFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CircularSequence"
.end annotation


# instance fields
.field private minValue:I

.field private previousValue:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/shingle/ShingleFilter;

.field private value:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)V
    .locals 1

    .prologue
    .line 469
    iput-object p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->this$0:Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 470
    # getter for: Lorg/apache/lucene/analysis/shingle/ShingleFilter;->outputUnigrams:Z
    invoke-static {p1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->access$0(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->minValue:I

    .line 471
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->reset()V

    .line 472
    return-void

    .line 470
    :cond_0
    # getter for: Lorg/apache/lucene/analysis/shingle/ShingleFilter;->minShingleSize:I
    invoke-static {p1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->access$1(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;)I
    .locals 1

    .prologue
    .line 467
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->minValue:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;I)V
    .locals 0

    .prologue
    .line 467
    iput p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->minValue:I

    return-void
.end method


# virtual methods
.method public advance()V
    .locals 2

    .prologue
    .line 491
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->previousValue:I

    .line 492
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 493
    iget-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->this$0:Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    # getter for: Lorg/apache/lucene/analysis/shingle/ShingleFilter;->minShingleSize:I
    invoke-static {v0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->access$1(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    .line 499
    :goto_0
    return-void

    .line 494
    :cond_0
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->this$0:Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    # getter for: Lorg/apache/lucene/analysis/shingle/ShingleFilter;->maxShingleSize:I
    invoke-static {v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->access$2(Lorg/apache/lucene/analysis/shingle/ShingleFilter;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 495
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->reset()V

    goto :goto_0

    .line 497
    :cond_1
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    goto :goto_0
.end method

.method public atMinValue()Z
    .locals 2

    .prologue
    .line 523
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->minValue:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPreviousValue()I
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->previousValue:I

    return v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->minValue:I

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->value:I

    iput v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleFilter$CircularSequence;->previousValue:I

    .line 511
    return-void
.end method
