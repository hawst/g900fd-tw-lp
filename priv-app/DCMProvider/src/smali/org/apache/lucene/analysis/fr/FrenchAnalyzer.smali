.class public final Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "FrenchAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/fr/FrenchAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "french_stop.txt"

.field private static final FRENCH_STOP_WORDS:[Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private final excltable:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 73
    const/16 v0, 0xd9

    new-array v0, v0, [Ljava/lang/String;

    .line 74
    const-string v1, "a"

    aput-object v1, v0, v4

    const-string v1, "afin"

    aput-object v1, v0, v5

    const-string v1, "ai"

    aput-object v1, v0, v6

    const-string v1, "ainsi"

    aput-object v1, v0, v7

    const-string v1, "apr\u00e8s"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "attendu"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "au"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "aujourd"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "auquel"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "aussi"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 75
    const-string v2, "autre"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "autres"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "aux"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "auxquelles"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "auxquels"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "avait"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "avant"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "avec"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "avoir"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 76
    const-string v2, "c"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "car"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ce"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "ceci"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "cela"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "celle"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "celles"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "celui"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "cependant"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "certain"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 77
    const-string v2, "certaine"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "certaines"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "certains"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "ces"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "cet"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "cette"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "ceux"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "chez"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "ci"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 78
    const-string v2, "combien"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "comme"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "comment"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "concernant"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "contre"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "d"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "dans"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "debout"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    .line 79
    const-string v2, "dedans"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "dehors"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "del\u00e0"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "depuis"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "derri\u00e8re"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "des"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "d\u00e9sormais"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "desquelles"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 80
    const-string v2, "desquels"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "dessous"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "dessus"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "devant"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "devers"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "devra"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "divers"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "diverse"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    .line 81
    const-string v2, "diverses"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "doit"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "donc"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "dont"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "du"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "duquel"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "durant"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "d\u00e8s"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "elle"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "elles"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    .line 82
    const-string v2, "en"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "entre"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "environ"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "est"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "et"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "etc"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "etre"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "eu"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "eux"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "except\u00e9"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "hormis"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    .line 83
    const-string v2, "hors"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "h\u00e9las"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "hui"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "il"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "ils"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "je"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "jusqu"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "jusque"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "la"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "laquelle"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    .line 84
    const-string v2, "le"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "lequel"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "les"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "lesquelles"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "lesquels"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "leur"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "leurs"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "lorsque"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "lui"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "l\u00e0"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    .line 85
    const-string v2, "ma"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "mais"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "malgr\u00e9"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "me"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "merci"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "mes"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "mien"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "mienne"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "miennes"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "miens"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "moi"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    .line 86
    const-string v2, "moins"

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "mon"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "moyennant"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "m\u00eame"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "m\u00eames"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "ne"

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    const-string v2, "ni"

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    const-string v2, "non"

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    const-string v2, "nos"

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    const-string v2, "notre"

    aput-object v2, v0, v1

    const/16 v1, 0x80

    .line 87
    const-string v2, "nous"

    aput-object v2, v0, v1

    const/16 v1, 0x81

    const-string v2, "n\u00e9anmoins"

    aput-object v2, v0, v1

    const/16 v1, 0x82

    const-string v2, "n\u00f4tre"

    aput-object v2, v0, v1

    const/16 v1, 0x83

    const-string v2, "n\u00f4tres"

    aput-object v2, v0, v1

    const/16 v1, 0x84

    const-string v2, "on"

    aput-object v2, v0, v1

    const/16 v1, 0x85

    const-string v2, "ont"

    aput-object v2, v0, v1

    const/16 v1, 0x86

    const-string v2, "ou"

    aput-object v2, v0, v1

    const/16 v1, 0x87

    const-string v2, "outre"

    aput-object v2, v0, v1

    const/16 v1, 0x88

    const-string v2, "o\u00f9"

    aput-object v2, v0, v1

    const/16 v1, 0x89

    const-string v2, "par"

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    const-string v2, "parmi"

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    .line 88
    const-string v2, "partant"

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    const-string v2, "pas"

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    const-string v2, "pass\u00e9"

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    const-string v2, "pendant"

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    const-string v2, "plein"

    aput-object v2, v0, v1

    const/16 v1, 0x90

    const-string v2, "plus"

    aput-object v2, v0, v1

    const/16 v1, 0x91

    const-string v2, "plusieurs"

    aput-object v2, v0, v1

    const/16 v1, 0x92

    const-string v2, "pour"

    aput-object v2, v0, v1

    const/16 v1, 0x93

    const-string v2, "pourquoi"

    aput-object v2, v0, v1

    const/16 v1, 0x94

    .line 89
    const-string v2, "proche"

    aput-object v2, v0, v1

    const/16 v1, 0x95

    const-string v2, "pr\u00e8s"

    aput-object v2, v0, v1

    const/16 v1, 0x96

    const-string v2, "puisque"

    aput-object v2, v0, v1

    const/16 v1, 0x97

    const-string v2, "qu"

    aput-object v2, v0, v1

    const/16 v1, 0x98

    const-string v2, "quand"

    aput-object v2, v0, v1

    const/16 v1, 0x99

    const-string v2, "que"

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    const-string v2, "quel"

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    const-string v2, "quelle"

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    const-string v2, "quelles"

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    const-string v2, "quels"

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    .line 90
    const-string v2, "qui"

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    const-string v2, "quoi"

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    const-string v2, "quoique"

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    const-string v2, "revoici"

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    const-string v2, "revoil\u00e0"

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    const-string v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    const-string v2, "sa"

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    const-string v2, "sans"

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    const-string v2, "sauf"

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    const-string v2, "se"

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    const-string v2, "selon"

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    .line 91
    const-string v2, "seront"

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    const-string v2, "ses"

    aput-object v2, v0, v1

    const/16 v1, 0xab

    const-string v2, "si"

    aput-object v2, v0, v1

    const/16 v1, 0xac

    const-string v2, "sien"

    aput-object v2, v0, v1

    const/16 v1, 0xad

    const-string v2, "sienne"

    aput-object v2, v0, v1

    const/16 v1, 0xae

    const-string v2, "siennes"

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    const-string v2, "siens"

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    const-string v2, "sinon"

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    const-string v2, "soi"

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    const-string v2, "soit"

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    .line 92
    const-string v2, "son"

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    const-string v2, "sont"

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    const-string v2, "sous"

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    const-string v2, "suivant"

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    const-string v2, "sur"

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    const-string v2, "ta"

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    const-string v2, "te"

    aput-object v2, v0, v1

    const/16 v1, 0xba

    const-string v2, "tes"

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    const-string v2, "tien"

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    const-string v2, "tienne"

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    const-string v2, "tiennes"

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    .line 93
    const-string v2, "tiens"

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    const-string v2, "toi"

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    const-string v2, "ton"

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    const-string v2, "tous"

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    const-string v2, "tout"

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    const-string v2, "toute"

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    const-string v2, "toutes"

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    const-string v2, "tu"

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    const-string v2, "un"

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    const-string v2, "une"

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    const-string v2, "va"

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    const-string/jumbo v2, "vers"

    aput-object v2, v0, v1

    const/16 v1, 0xca

    .line 94
    const-string/jumbo v2, "voici"

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    const-string/jumbo v2, "voil\u00e0"

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    const-string/jumbo v2, "vos"

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    const-string/jumbo v2, "votre"

    aput-object v2, v0, v1

    const/16 v1, 0xce

    const-string/jumbo v2, "vous"

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    const-string/jumbo v2, "vu"

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    const-string/jumbo v2, "v\u00f4tre"

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    const-string/jumbo v2, "v\u00f4tres"

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    const-string/jumbo v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    const-string/jumbo v2, "\u00e0"

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    const-string/jumbo v2, "\u00e7a"

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    const-string/jumbo v2, "\u00e8s"

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    .line 95
    const-string/jumbo v2, "\u00e9t\u00e9"

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    const-string/jumbo v2, "\u00eatre"

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    const-string/jumbo v2, "\u00f4"

    aput-object v2, v0, v1

    .line 73
    sput-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->FRENCH_STOP_WORDS:[Ljava/lang/String;

    .line 103
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/String;

    .line 104
    const-string v3, "l"

    aput-object v3, v2, v4

    const-string v3, "m"

    aput-object v3, v2, v5

    const-string v3, "t"

    aput-object v3, v2, v6

    const-string v3, "qu"

    aput-object v3, v2, v7

    const-string v3, "n"

    aput-object v3, v2, v8

    const/4 v3, 0x5

    const-string v4, "s"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "j"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "d"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "c"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "jusqu"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "quoiqu"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "lorsqu"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "puisqu"

    aput-object v4, v2, v3

    .line 103
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    .line 102
    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 104
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 142
    .line 143
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 144
    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 145
    return-void

    .line 144
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET_30:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 156
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 157
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclutionSet"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 171
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 173
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    .line 172
    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 174
    return-void
.end method

.method static synthetic access$0()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->FRENCH_STOP_WORDS:[Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 191
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 192
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 193
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 194
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/util/ElisionFilter;

    sget-object v3, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/util/ElisionFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 195
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v1}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 196
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v3, v0, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 197
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 198
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 199
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    new-instance v1, Lorg/apache/lucene/analysis/fr/FrenchLightStemFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/fr/FrenchLightStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 204
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_1
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 213
    :goto_2
    return-object v3

    .line 202
    :cond_0
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    new-instance v3, Lorg/tartarus/snowball/ext/FrenchStemmer;

    invoke-direct {v3}, Lorg/tartarus/snowball/ext/FrenchStemmer;-><init>()V

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_1

    .line 206
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .end local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    :cond_1
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 207
    .restart local v2    # "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 208
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v3, v0, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 209
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 210
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->excltable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 211
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_3
    new-instance v1, Lorg/apache/lucene/analysis/fr/FrenchStemFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/fr/FrenchStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 213
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v4, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v5, p0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v4, v5, v1}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    invoke-direct {v3, v2, v4}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_2

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_2
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_3

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_3
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0
.end method
