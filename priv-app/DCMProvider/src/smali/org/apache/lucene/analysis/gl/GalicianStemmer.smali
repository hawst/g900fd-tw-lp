.class public Lorg/apache/lucene/analysis/gl/GalicianStemmer;
.super Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.source "GalicianStemmer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final adverb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final augmentative:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final noun:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final plural:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final unification:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final verb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final vowel:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const-class v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;

    invoke-virtual {v1}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->$assertionsDisabled:Z

    .line 34
    const-class v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;

    const-string v2, "galician.rslp"

    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->parse(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 35
    .local v0, "steps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;>;"
    const-string v1, "Plural"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->plural:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 36
    const-string v1, "Unification"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->unification:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 37
    const-string v1, "Adverb"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->adverb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 38
    const-string v1, "Augmentative"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->augmentative:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 39
    const-string v1, "Noun"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->noun:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 40
    const-string v1, "Verb"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->verb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 41
    const-string v1, "Vowel"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->vowel:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 42
    return-void

    .line 30
    .end local v0    # "steps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 50
    sget-boolean v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    array-length v2, p1

    add-int/lit8 v3, p2, 0x1

    if-ge v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "this stemmer requires an oversized array of at least 1"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 52
    :cond_0
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->plural:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 53
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->unification:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 54
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->adverb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 58
    :cond_1
    move v1, p2

    .line 59
    .local v1, "oldlen":I
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->augmentative:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 60
    if-ne p2, v1, :cond_1

    .line 62
    move v1, p2

    .line 63
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->noun:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 64
    if-ne p2, v1, :cond_2

    .line 65
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->verb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 68
    :cond_2
    sget-object v2, Lorg/apache/lucene/analysis/gl/GalicianStemmer;->vowel:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 71
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_3

    .line 81
    return p2

    .line 72
    :cond_3
    aget-char v2, p1, v0

    sparse-switch v2, :sswitch_data_0

    .line 71
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :sswitch_0
    const/16 v2, 0x61

    aput-char v2, p1, v0

    goto :goto_1

    .line 75
    :sswitch_1
    const/16 v2, 0x65

    aput-char v2, p1, v0

    goto :goto_1

    .line 76
    :sswitch_2
    const/16 v2, 0x69

    aput-char v2, p1, v0

    goto :goto_1

    .line 77
    :sswitch_3
    const/16 v2, 0x6f

    aput-char v2, p1, v0

    goto :goto_1

    .line 78
    :sswitch_4
    const/16 v2, 0x75

    aput-char v2, p1, v0

    goto :goto_1

    .line 72
    :sswitch_data_0
    .sparse-switch
        0xe1 -> :sswitch_0
        0xe9 -> :sswitch_1
        0xea -> :sswitch_1
        0xed -> :sswitch_2
        0xf3 -> :sswitch_3
        0xfa -> :sswitch_4
    .end sparse-switch
.end method
