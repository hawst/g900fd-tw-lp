.class public final Lorg/apache/lucene/analysis/pattern/PatternTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "PatternTokenizer.java"


# instance fields
.field final buffer:[C

.field private final group:I

.field private index:I

.field private final matcher:Ljava/util/regex/Matcher;

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final pattern:Ljava/util/regex/Pattern;

.field private final str:Ljava/lang/StringBuilder;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/io/Reader;Ljava/util/regex/Pattern;I)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;
    .param p3, "group"    # I

    .prologue
    .line 69
    sget-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {p0, v0, p1, p2, p3}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;Ljava/util/regex/Pattern;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;Ljava/util/regex/Pattern;I)V
    .locals 3
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "pattern"    # Ljava/util/regex/Pattern;
    .param p4, "group"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 57
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 58
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    .line 149
    const/16 v0, 0x2000

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->buffer:[C

    .line 75
    iput-object p3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->pattern:Ljava/util/regex/Pattern;

    .line 76
    iput p4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->group:I

    .line 80
    const-string v0, ""

    invoke-virtual {p3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    .line 83
    if-ltz p4, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v0

    if-le p4, v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid group specified: pattern only has: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " capturing groups"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    return-void
.end method

.method private fillBuffer(Ljava/lang/StringBuilder;Ljava/io/Reader;)V
    .locals 3
    .param p1, "sb"    # Ljava/lang/StringBuilder;
    .param p2, "input"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 153
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->buffer:[C

    invoke-virtual {p2, v1}, Ljava/io/Reader;->read([C)I

    move-result v0

    .local v0, "len":I
    if-gtz v0, :cond_0

    .line 156
    return-void

    .line 154
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->buffer:[C

    invoke-virtual {p1, v1, v2, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public end()V
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v0

    .line 137
    .local v0, "ofs":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 138
    return-void
.end method

.method public incrementToken()Z
    .locals 7

    .prologue
    const v6, 0x7fffffff

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 90
    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    iget-object v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 130
    :goto_0
    return v1

    .line 91
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->clearAttributes()V

    .line 92
    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->group:I

    if-ltz v3, :cond_5

    .line 95
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_2

    .line 104
    iput v6, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    goto :goto_0

    .line 96
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->group:I

    invoke-virtual {v3, v4}, Ljava/util/regex/Matcher;->start(I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    .line 97
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->group:I

    invoke-virtual {v3, v4}, Ljava/util/regex/Matcher;->end(I)I

    move-result v0

    .line 98
    .local v0, "endIndex":I
    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    if-eq v3, v0, :cond_1

    .line 99
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    invoke-interface {v1, v3, v4, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 100
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v3

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v4

    invoke-interface {v1, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    move v1, v2

    .line 101
    goto :goto_0

    .line 111
    .end local v0    # "endIndex":I
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    sub-int/2addr v3, v4

    if-lez v3, :cond_4

    .line 113
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    iget-object v5, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->start()I

    move-result v5

    invoke-interface {v1, v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 114
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v4

    invoke-interface {v1, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 115
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    move v1, v2

    .line 116
    goto/16 :goto_0

    .line 119
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    .line 110
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_3

    .line 122
    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    sub-int/2addr v3, v4

    if-nez v3, :cond_6

    .line 123
    iput v6, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    goto/16 :goto_0

    .line 127
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    iget v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    iget-object v5, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    invoke-interface {v1, v3, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 128
    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v3, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->correctOffset(I)I

    move-result v4

    invoke-interface {v1, v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 129
    iput v6, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    move v1, v2

    .line 130
    goto/16 :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->input:Ljava/io/Reader;

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->fillBuffer(Ljava/lang/StringBuilder;Ljava/io/Reader;)V

    .line 143
    iget-object v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    iget-object v1, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->str:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->reset(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/pattern/PatternTokenizer;->index:I

    .line 145
    return-void
.end method
