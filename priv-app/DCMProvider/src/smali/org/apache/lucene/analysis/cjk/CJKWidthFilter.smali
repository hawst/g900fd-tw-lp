.class public final Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "CJKWidthFilter.java"


# static fields
.field private static final KANA_COMBINE_HALF_VOICED:[B

.field private static final KANA_COMBINE_VOICED:[B

.field private static final KANA_NORM:[C


# instance fields
.field private termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x2c

    const/16 v6, 0x29

    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 47
    const/16 v0, 0x3b

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->KANA_NORM:[C

    .line 88
    const/16 v0, 0x58

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 89
    const/16 v2, 0x4e

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x7

    aput-byte v3, v0, v1

    const/16 v1, 0x9

    aput-byte v3, v0, v1

    const/16 v1, 0xb

    aput-byte v3, v0, v1

    const/16 v1, 0xd

    aput-byte v3, v0, v1

    const/16 v1, 0xf

    aput-byte v3, v0, v1

    const/16 v1, 0x11

    aput-byte v3, v0, v1

    const/16 v1, 0x13

    aput-byte v3, v0, v1

    const/16 v1, 0x15

    aput-byte v3, v0, v1

    const/16 v1, 0x17

    aput-byte v3, v0, v1

    const/16 v1, 0x19

    .line 90
    aput-byte v3, v0, v1

    const/16 v1, 0x1b

    aput-byte v3, v0, v1

    const/16 v1, 0x1e

    aput-byte v3, v0, v1

    const/16 v1, 0x20

    aput-byte v3, v0, v1

    const/16 v1, 0x22

    aput-byte v3, v0, v1

    aput-byte v3, v0, v6

    aput-byte v3, v0, v7

    const/16 v1, 0x2f

    aput-byte v3, v0, v1

    const/16 v1, 0x32

    .line 91
    aput-byte v3, v0, v1

    const/16 v1, 0x35

    aput-byte v3, v0, v1

    const/16 v1, 0x49

    .line 92
    aput-byte v5, v0, v1

    const/16 v1, 0x4a

    aput-byte v5, v0, v1

    const/16 v1, 0x4b

    aput-byte v5, v0, v1

    const/16 v1, 0x4c

    aput-byte v5, v0, v1

    const/16 v1, 0x57

    aput-byte v3, v0, v1

    .line 88
    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->KANA_COMBINE_VOICED:[B

    .line 95
    const/16 v0, 0x58

    new-array v0, v0, [B

    .line 97
    aput-byte v4, v0, v6

    aput-byte v4, v0, v7

    const/16 v1, 0x2f

    aput-byte v4, v0, v1

    const/16 v1, 0x32

    .line 98
    aput-byte v4, v0, v1

    const/16 v1, 0x35

    aput-byte v4, v0, v1

    .line 95
    sput-object v0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->KANA_COMBINE_HALF_VOICED:[B

    .line 100
    return-void

    .line 47
    nop

    :array_0
    .array-data 2
        0x30fbs
        0x30f2s
        0x30a1s
        0x30a3s
        0x30a5s
        0x30a7s
        0x30a9s
        0x30e3s
        0x30e5s
        0x30e7s
        0x30c3s
        0x30fcs
        0x30a2s
        0x30a4s
        0x30a6s
        0x30a8s
        0x30aas
        0x30abs
        0x30ads
        0x30afs
        0x30b1s
        0x30b3s
        0x30b5s
        0x30b7s
        0x30b9s
        0x30bbs
        0x30bds
        0x30bfs
        0x30c1s
        0x30c4s
        0x30c6s
        0x30c8s
        0x30cas
        0x30cbs
        0x30ccs
        0x30cds
        0x30ces
        0x30cfs
        0x30d2s
        0x30d5s
        0x30d8s
        0x30dbs
        0x30des
        0x30dfs
        0x30e0s
        0x30e1s
        0x30e2s
        0x30e4s
        0x30e6s
        0x30e8s
        0x30e9s
        0x30eas
        0x30ebs
        0x30ecs
        0x30eds
        0x30efs
        0x30f3s
        0x3099s
        0x309as
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 39
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 59
    return-void
.end method

.method private static combine([CIC)Z
    .locals 6
    .param p0, "text"    # [C
    .param p1, "pos"    # I
    .param p2, "ch"    # C

    .prologue
    const/4 v2, 0x0

    .line 104
    add-int/lit8 v1, p1, -0x1

    aget-char v0, p0, v1

    .line 105
    .local v0, "prev":C
    const/16 v1, 0x30a6

    if-lt v0, v1, :cond_2

    const/16 v1, 0x30fd

    if-gt v0, v1, :cond_2

    .line 106
    add-int/lit8 v3, p1, -0x1

    aget-char v4, p0, v3

    const v1, 0xff9f

    if-ne p2, v1, :cond_0

    .line 107
    sget-object v1, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->KANA_COMBINE_HALF_VOICED:[B

    add-int/lit16 v5, v0, -0x30a6

    aget-byte v1, v1, v5

    .line 106
    :goto_0
    add-int/2addr v1, v4

    int-to-char v1, v1

    aput-char v1, p0, v3

    .line 109
    add-int/lit8 v1, p1, -0x1

    aget-char v1, p0, v1

    if-eq v1, v0, :cond_1

    const/4 v1, 0x1

    .line 111
    :goto_1
    return v1

    .line 108
    :cond_0
    sget-object v1, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->KANA_COMBINE_VOICED:[B

    add-int/lit16 v5, v0, -0x30a6

    aget-byte v1, v1, v5

    goto :goto_0

    :cond_1
    move v1, v2

    .line 109
    goto :goto_1

    :cond_2
    move v1, v2

    .line 111
    goto :goto_1
.end method


# virtual methods
.method public incrementToken()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v8, 0xff9f

    const v7, 0xff65

    .line 63
    iget-object v5, p0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 64
    iget-object v5, p0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    .line 65
    .local v4, "text":[C
    iget-object v5, p0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    .line 66
    .local v3, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 80
    iget-object v5, p0, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 81
    const/4 v5, 0x1

    .line 83
    .end local v2    # "i":I
    .end local v3    # "length":I
    .end local v4    # "text":[C
    :goto_1
    return v5

    .line 67
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    .restart local v4    # "text":[C
    :cond_0
    aget-char v0, v4, v2

    .line 68
    .local v0, "ch":C
    const v5, 0xff01

    if-lt v0, v5, :cond_1

    const v5, 0xff5e

    if-gt v0, v5, :cond_1

    .line 70
    aget-char v5, v4, v2

    const v6, 0xfee0

    sub-int/2addr v5, v6

    int-to-char v5, v5

    aput-char v5, v4, v2

    move v1, v2

    .line 66
    .end local v2    # "i":I
    .restart local v1    # "i":I
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 71
    :cond_1
    if-lt v0, v7, :cond_4

    if-gt v0, v8, :cond_4

    .line 73
    const v5, 0xff9e

    if-eq v0, v5, :cond_2

    if-ne v0, v8, :cond_3

    :cond_2
    if-lez v2, :cond_3

    invoke-static {v4, v2, v0}, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->combine([CIC)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 74
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-static {v4, v2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result v3

    .line 75
    goto :goto_2

    .line 76
    .end local v1    # "i":I
    .restart local v2    # "i":I
    :cond_3
    sget-object v5, Lorg/apache/lucene/analysis/cjk/CJKWidthFilter;->KANA_NORM:[C

    sub-int v6, v0, v7

    aget-char v5, v5, v6

    aput-char v5, v4, v2

    :cond_4
    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_2

    .line 83
    .end local v0    # "ch":C
    .end local v1    # "i":I
    .end local v3    # "length":I
    .end local v4    # "text":[C
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method
