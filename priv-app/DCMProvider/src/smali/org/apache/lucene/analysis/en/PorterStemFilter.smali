.class public final Lorg/apache/lucene/analysis/en/PorterStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "PorterStemFilter.java"


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final stemmer:Lorg/apache/lucene/analysis/en/PorterStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 57
    new-instance v0, Lorg/apache/lucene/analysis/en/PorterStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/en/PorterStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/PorterStemmer;

    .line 58
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/en/PorterStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 59
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/en/PorterStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 63
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    :goto_0
    return v0

    .line 70
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/PorterStemmer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/en/PorterStemmer;->stem([CII)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v2, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/PorterStemmer;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/en/PorterStemmer;->getResultBuffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/en/PorterStemFilter;->stemmer:Lorg/apache/lucene/analysis/en/PorterStemmer;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/en/PorterStemmer;->getResultLength()I

    move-result v3

    invoke-interface {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 72
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
