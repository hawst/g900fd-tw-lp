.class Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;
.super Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;
.source "HunspellDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DoubleASCIIFlagParsingStrategy"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;-><init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$FlagParsingStrategy;)V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;)V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary$DoubleASCIIFlagParsingStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public parseFlags(Ljava/lang/String;)[C
    .locals 7
    .param p1, "rawFlags"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 491
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    .line 492
    new-array v2, v6, [C

    .line 503
    :goto_0
    return-object v2

    .line 495
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 496
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 501
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    new-array v2, v4, [C

    .line 502
    .local v2, "flags":[C
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v6, v4, v2, v6}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    goto :goto_0

    .line 497
    .end local v2    # "flags":[C
    :cond_1
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    add-int/2addr v4, v5

    int-to-char v1, v4

    .line 498
    .local v1, "cookedFlag":C
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 496
    add-int/lit8 v3, v3, 0x2

    goto :goto_1
.end method
