.class public Lorg/apache/lucene/analysis/it/ItalianLightStemmer;
.super Ljava/lang/Object;
.source "ItalianLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v3, 0x68

    const/16 v2, 0x69

    .line 65
    const/4 v1, 0x6

    if-ge p2, v1, :cond_0

    .line 115
    .end local p2    # "len":I
    :goto_0
    return p2

    .line 68
    .restart local p2    # "len":I
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, p2, :cond_1

    .line 92
    add-int/lit8 v1, p2, -0x1

    aget-char v1, p1, v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 104
    :sswitch_0
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_6

    .line 105
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 69
    :cond_1
    aget-char v1, p1, v0

    packed-switch v1, :pswitch_data_0

    .line 68
    :goto_2
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 73
    :pswitch_1
    const/16 v1, 0x61

    aput-char v1, p1, v0

    goto :goto_2

    .line 77
    :pswitch_2
    const/16 v1, 0x6f

    aput-char v1, p1, v0

    goto :goto_2

    .line 81
    :pswitch_3
    const/16 v1, 0x65

    aput-char v1, p1, v0

    goto :goto_2

    .line 85
    :pswitch_4
    const/16 v1, 0x75

    aput-char v1, p1, v0

    goto :goto_2

    .line 89
    :pswitch_5
    aput-char v2, p1, v0

    goto :goto_2

    .line 94
    :sswitch_1
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-eq v1, v2, :cond_2

    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v3, :cond_3

    .line 95
    :cond_2
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 97
    :cond_3
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 99
    :sswitch_2
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-eq v1, v3, :cond_4

    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_5

    .line 100
    :cond_4
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 102
    :cond_5
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 107
    :cond_6
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 109
    :sswitch_3
    add-int/lit8 v1, p2, -0x2

    aget-char v1, p1, v1

    if-ne v1, v2, :cond_7

    .line 110
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 112
    :cond_7
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 92
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_1
        0x69 -> :sswitch_2
        0x6f -> :sswitch_3
    .end sparse-switch

    .line 69
    :pswitch_data_0
    .packed-switch 0xe0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
