.class public final Lorg/apache/lucene/analysis/util/ElisionFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "ElisionFilter.java"


# instance fields
.field private final articles:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "articles"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 35
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/ElisionFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 44
    iput-object p2, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->articles:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 45
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 52
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v6}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 53
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v3

    .line 54
    .local v3, "termBuffer":[C
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    .line 56
    .local v4, "termLength":I
    const/4 v2, -0x1

    .line 57
    .local v2, "index":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_2

    .line 66
    :goto_1
    if-ltz v2, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->articles:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v6, v3, v5, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 67
    iget-object v5, p0, Lorg/apache/lucene/analysis/util/ElisionFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v6, v2, 0x1

    add-int/lit8 v7, v2, 0x1

    sub-int v7, v4, v7

    invoke-interface {v5, v3, v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 70
    :cond_0
    const/4 v5, 0x1

    .line 72
    .end local v1    # "i":I
    .end local v2    # "index":I
    .end local v3    # "termBuffer":[C
    .end local v4    # "termLength":I
    :cond_1
    return v5

    .line 58
    .restart local v1    # "i":I
    .restart local v2    # "index":I
    .restart local v3    # "termBuffer":[C
    .restart local v4    # "termLength":I
    :cond_2
    aget-char v0, v3, v1

    .line 59
    .local v0, "ch":C
    const/16 v6, 0x27

    if-eq v0, v6, :cond_3

    const/16 v6, 0x2019

    if-ne v0, v6, :cond_4

    .line 60
    :cond_3
    move v2, v1

    .line 61
    goto :goto_1

    .line 57
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
