.class public Lorg/apache/lucene/analysis/pt/PortugueseStemmer;
.super Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;
.source "PortugueseStemmer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final adverb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final augmentative:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final feminine:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final noun:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final plural:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final verb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

.field private static final vowel:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const-class v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;

    invoke-virtual {v1}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->$assertionsDisabled:Z

    .line 32
    const-class v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;

    const-string v2, "portuguese.rslp"

    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->parse(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 33
    .local v0, "steps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;>;"
    const-string v1, "Plural"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->plural:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 34
    const-string v1, "Feminine"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->feminine:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 35
    const-string v1, "Adverb"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->adverb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 36
    const-string v1, "Augmentative"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->augmentative:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 37
    const-string v1, "Noun"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->noun:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 38
    const-string v1, "Verb"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->verb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 39
    const-string v1, "Vowel"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    sput-object v1, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->vowel:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    .line 40
    return-void

    .line 28
    .end local v0    # "steps":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 48
    sget-boolean v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    array-length v2, p1

    add-int/lit8 v3, p2, 0x1

    if-ge v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    const-string v3, "this stemmer requires an oversized array of at least 1"

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 50
    :cond_0
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->plural:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 51
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->adverb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 52
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->feminine:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 53
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->augmentative:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 55
    move v1, p2

    .line 56
    .local v1, "oldlen":I
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->noun:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 58
    if-ne p2, v1, :cond_1

    .line 59
    move v1, p2

    .line 61
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->verb:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 63
    if-ne p2, v1, :cond_1

    .line 64
    sget-object v2, Lorg/apache/lucene/analysis/pt/PortugueseStemmer;->vowel:Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/analysis/pt/RSLPStemmerBase$Step;->apply([CI)I

    move-result p2

    .line 69
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_2

    .line 100
    return p2

    .line 70
    :cond_2
    aget-char v2, p1, v0

    packed-switch v2, :pswitch_data_0

    .line 69
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :pswitch_1
    const/16 v2, 0x61

    aput-char v2, p1, v0

    goto :goto_1

    .line 77
    :pswitch_2
    const/16 v2, 0x63

    aput-char v2, p1, v0

    goto :goto_1

    .line 81
    :pswitch_3
    const/16 v2, 0x65

    aput-char v2, p1, v0

    goto :goto_1

    .line 85
    :pswitch_4
    const/16 v2, 0x69

    aput-char v2, p1, v0

    goto :goto_1

    .line 86
    :pswitch_5
    const/16 v2, 0x6e

    aput-char v2, p1, v0

    goto :goto_1

    .line 91
    :pswitch_6
    const/16 v2, 0x6f

    aput-char v2, p1, v0

    goto :goto_1

    .line 95
    :pswitch_7
    const/16 v2, 0x75

    aput-char v2, p1, v0

    goto :goto_1

    .line 97
    :pswitch_8
    const/16 v2, 0x79

    aput-char v2, p1, v0

    goto :goto_1

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0xe0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method
