.class public Lorg/apache/lucene/analysis/ar/ArabicLetterTokenizer;
.super Lorg/apache/lucene/analysis/core/LetterTokenizer;
.source "ArabicLetterTokenizer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Ljava/io/Reader;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/core/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "in"    # Ljava/io/Reader;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/core/LetterTokenizer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 70
    return-void
.end method


# virtual methods
.method protected isTokenChar(I)Z
    .locals 2
    .param p1, "c"    # I

    .prologue
    .line 78
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/core/LetterTokenizer;->isTokenChar(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
