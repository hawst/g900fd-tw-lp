.class public abstract Lorg/apache/lucene/analysis/util/CharTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "CharTokenizer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final IO_BUFFER_SIZE:I = 0x1000

.field private static final MAX_WORD_LEN:I = 0xff


# instance fields
.field private bufferIndex:I

.field private final charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

.field private dataLen:I

.field private finalOffset:I

.field private final ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

.field private offset:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lorg/apache/lucene/analysis/util/CharTokenizer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/util/CharTokenizer;->$assertionsDisabled:Z

    .line 100
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 98
    iput v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    iput v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    iput v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    .line 102
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 103
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 106
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharacterUtils;->newCharacterBuffer(I)Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    .line 78
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 79
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p3, "input"    # Ljava/io/Reader;

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 98
    iput v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    iput v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    iput v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    .line 102
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 103
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/CharTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 106
    const/16 v0, 0x1000

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharacterUtils;->newCharacterBuffer(I)Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    .line 94
    invoke-static {p1}, Lorg/apache/lucene/analysis/util/CharacterUtils;->getInstance(Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharacterUtils;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    .line 95
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    iget v2, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 179
    return-void
.end method

.method public final incrementToken()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, -0x1

    const/4 v6, 0x0

    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/CharTokenizer;->clearAttributes()V

    .line 128
    const/4 v4, 0x0

    .line 129
    .local v4, "length":I
    const/4 v5, -0x1

    .line 130
    .local v5, "start":I
    const/4 v3, -0x1

    .line 131
    .local v3, "end":I
    iget-object v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 133
    .local v0, "buffer":[C
    :cond_0
    iget v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    iget v8, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    if-lt v7, v8, :cond_3

    .line 134
    iget v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    iget v8, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    add-int/2addr v7, v8

    iput v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    .line 135
    iget-object v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    iget-object v8, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    iget-object v9, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/analysis/util/CharacterUtils;->fill(Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;Ljava/io/Reader;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 136
    iput v6, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    .line 137
    if-lez v4, :cond_1

    .line 168
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 169
    sget-boolean v6, Lorg/apache/lucene/analysis/util/CharTokenizer;->$assertionsDisabled:Z

    if-nez v6, :cond_8

    if-ne v5, v10, :cond_8

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 140
    :cond_1
    iget v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/util/CharTokenizer;->correctOffset(I)I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    .line 171
    :goto_1
    return v6

    .line 144
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->getLength()I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    .line 145
    iput v6, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    .line 148
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->charUtils:Lorg/apache/lucene/analysis/util/CharacterUtils;

    iget-object v8, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->getBuffer()[C

    move-result-object v8

    iget v9, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/analysis/util/CharacterUtils;->codePointAt([CI)I

    move-result v1

    .line 149
    .local v1, "c":I
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    .line 150
    .local v2, "charCount":I
    iget v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    add-int/2addr v7, v2

    iput v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    .line 152
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/util/CharTokenizer;->isTokenChar(I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 153
    if-nez v4, :cond_6

    .line 154
    sget-boolean v7, Lorg/apache/lucene/analysis/util/CharTokenizer;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    if-eq v5, v10, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 155
    :cond_4
    iget v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    iget v8, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    add-int/2addr v7, v8

    sub-int v5, v7, v2

    .line 156
    move v3, v5

    .line 160
    :cond_5
    :goto_2
    add-int/2addr v3, v2

    .line 161
    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/util/CharTokenizer;->normalize(I)I

    move-result v7

    invoke-static {v7, v0, v4}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v7

    add-int/2addr v4, v7

    .line 162
    const/16 v7, 0xff

    if-lt v4, v7, :cond_0

    goto :goto_0

    .line 157
    :cond_6
    array-length v7, v0

    add-int/lit8 v7, v7, -0x1

    if-lt v4, v7, :cond_5

    .line 158
    iget-object v7, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v8, v4, 0x2

    invoke-interface {v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    goto :goto_2

    .line 164
    :cond_7
    if-lez v4, :cond_0

    goto :goto_0

    .line 170
    .end local v1    # "c":I
    .end local v2    # "charCount":I
    :cond_8
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v5}, Lorg/apache/lucene/analysis/util/CharTokenizer;->correctOffset(I)I

    move-result v7

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/util/CharTokenizer;->correctOffset(I)I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    invoke-interface {v6, v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 171
    const/4 v6, 0x1

    goto :goto_1
.end method

.method protected abstract isTokenChar(I)Z
.end method

.method protected normalize(I)I
    .locals 0
    .param p1, "c"    # I

    .prologue
    .line 122
    return p1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 183
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->bufferIndex:I

    .line 184
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->offset:I

    .line 185
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->dataLen:I

    .line 186
    iput v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->finalOffset:I

    .line 187
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/CharTokenizer;->ioBuffer:Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/util/CharacterUtils$CharacterBuffer;->reset()V

    .line 188
    return-void
.end method
