.class public final Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "EdgeNGramTokenFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAX_GRAM_SIZE:I = 0x1

.field public static final DEFAULT_MIN_GRAM_SIZE:I = 0x1

.field public static final DEFAULT_SIDE:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;


# instance fields
.field private curGramSize:I

.field private curTermBuffer:[C

.field private curTermLength:I

.field private hasIllegalOffsets:Z

.field private isFirstToken:Z

.field private final maxGram:I

.field private final minGram:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private savePosIncr:I

.field private side:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private tokEnd:I

.field private tokStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    sput-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->DEFAULT_SIDE:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    .line 37
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/lang/String;II)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "sideLabel"    # Ljava/lang/String;
    .param p3, "minGram"    # I
    .param p4, "maxGram"    # I

    .prologue
    .line 121
    invoke-static {p2}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->getSide(Ljava/lang/String;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;II)V

    .line 122
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;II)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "side"    # Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;
    .param p3, "minGram"    # I
    .param p4, "maxGram"    # I

    .prologue
    const/4 v1, 0x1

    .line 93
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 78
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->isFirstToken:Z

    .line 80
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 81
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 82
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 95
    if-nez p2, :cond_0

    .line 96
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sideLabel must be either front or back"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_0
    if-ge p3, v1, :cond_1

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    if-le p3, p4, :cond_2

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must not be greater than maxGram"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_2
    iput p3, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->minGram:I

    .line 108
    iput p4, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->maxGram:I

    .line 109
    iput-object p2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->side:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    .line 110
    return-void
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 127
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermBuffer:[C

    if-nez v2, :cond_1

    .line 128
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    :goto_1
    return v4

    .line 131
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    invoke-virtual {v2}, [C->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [C

    iput-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermBuffer:[C

    .line 132
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermLength:I

    .line 133
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->minGram:I

    iput v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    .line 134
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokStart:I

    .line 135
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokEnd:I

    .line 138
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokStart:I

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermLength:I

    add-int/2addr v2, v5

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokEnd:I

    if-eq v2, v5, :cond_3

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->hasIllegalOffsets:Z

    .line 139
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->savePosIncr:I

    .line 142
    :cond_1
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->maxGram:I

    if-gt v2, v5, :cond_7

    .line 143
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermLength:I

    if-gt v2, v5, :cond_7

    .line 145
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->side:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    sget-object v5, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter$Side;

    if-ne v2, v5, :cond_4

    move v1, v4

    .line 146
    .local v1, "start":I
    :goto_3
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    add-int v0, v1, v2

    .line 147
    .local v0, "end":I
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->clearAttributes()V

    .line 148
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->hasIllegalOffsets:Z

    if-eqz v2, :cond_5

    .line 149
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokStart:I

    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokEnd:I

    invoke-interface {v2, v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 154
    :goto_4
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->minGram:I

    if-ne v2, v5, :cond_6

    .line 156
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->isFirstToken:Z

    if-nez v2, :cond_2

    .line 157
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->savePosIncr:I

    invoke-interface {v2, v5}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 162
    :cond_2
    :goto_5
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermBuffer:[C

    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    invoke-interface {v2, v5, v1, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 163
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    .line 164
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->isFirstToken:Z

    move v4, v3

    .line 165
    goto/16 :goto_1

    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_3
    move v2, v4

    .line 138
    goto :goto_2

    .line 145
    :cond_4
    iget v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermLength:I

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curGramSize:I

    sub-int v1, v2, v5

    goto :goto_3

    .line 151
    .restart local v0    # "end":I
    .restart local v1    # "start":I
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokStart:I

    add-int/2addr v5, v1

    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->tokStart:I

    add-int/2addr v6, v0

    invoke-interface {v2, v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto :goto_4

    .line 160
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_5

    .line 168
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_7
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermBuffer:[C

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->curTermBuffer:[C

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenFilter;->isFirstToken:Z

    .line 177
    return-void
.end method
