.class Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;
.super Ljava/lang/Object;
.source "TernaryTree.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Item"
.end annotation


# instance fields
.field child:C

.field parent:C

.field final synthetic this$1:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 477
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->this$1:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 478
    iput-char v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    .line 479
    iput-char v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    .line 480
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;CC)V
    .locals 0
    .param p2, "p"    # C
    .param p3, "c"    # C

    .prologue
    .line 482
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->this$1:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483
    iput-char p2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    .line 484
    iput-char p3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    .line 485
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;
    .locals 4

    .prologue
    .line 489
    new-instance v0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->this$1:Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;

    iget-char v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->parent:C

    iget-char v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;->child:C

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator$Item;-><init>(Lorg/apache/lucene/analysis/compound/hyphenation/TernaryTree$Iterator;CC)V

    return-object v0
.end method
