.class public final Lorg/apache/lucene/analysis/core/StopFilter;
.super Lorg/apache/lucene/analysis/util/FilteringTokenFilter;
.source "StopFilter.java"


# instance fields
.field private final stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "stopWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;)V

    .line 44
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/core/StopFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/StopFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 61
    iput-object p3, p0, Lorg/apache/lucene/analysis/core/StopFilter;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 62
    return-void
.end method

.method public static makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/List",
            "<*>;)",
            "Lorg/apache/lucene/analysis/util/CharArraySet;"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "stopWords":Ljava/util/List;, "Ljava/util/List<*>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/analysis/core/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static makeStopSet(Lorg/apache/lucene/util/Version;Ljava/util/List;Z)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 2
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "ignoreCase"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Ljava/util/List",
            "<*>;Z)",
            "Lorg/apache/lucene/analysis/util/CharArraySet;"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "stopWords":Ljava/util/List;, "Ljava/util/List<*>;"
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, p0, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 116
    .local v0, "stopSet":Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/util/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 117
    return-object v0
.end method

.method public static varargs makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "stopWords"    # [Ljava/lang/String;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/analysis/core/StopFilter;->makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static makeStopSet(Lorg/apache/lucene/util/Version;[Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 2
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "stopWords"    # [Ljava/lang/String;
    .param p2, "ignoreCase"    # Z

    .prologue
    .line 102
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    array-length v1, p1

    invoke-direct {v0, p0, v1, p2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    .line 103
    .local v0, "stopSet":Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/util/CharArraySet;->addAll(Ljava/util/Collection;)Z

    .line 104
    return-object v0
.end method


# virtual methods
.method protected accept()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 125
    iget-object v1, p0, Lorg/apache/lucene/analysis/core/StopFilter;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v2, p0, Lorg/apache/lucene/analysis/core/StopFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/core/StopFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
