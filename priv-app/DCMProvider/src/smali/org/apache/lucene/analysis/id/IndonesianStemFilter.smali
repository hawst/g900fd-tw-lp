.class public final Lorg/apache/lucene/analysis/id/IndonesianStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "IndonesianStemFilter.java"


# instance fields
.field private final keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private final stemDerivational:Z

.field private final stemmer:Lorg/apache/lucene/analysis/id/IndonesianStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Z)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Z)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "stemDerivational"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 31
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 32
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 33
    new-instance v0, Lorg/apache/lucene/analysis/id/IndonesianStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->stemmer:Lorg/apache/lucene/analysis/id/IndonesianStemmer;

    .line 51
    iput-boolean p2, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->stemDerivational:Z

    .line 52
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    iget-object v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->keywordAtt:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    iget-object v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->stemmer:Lorg/apache/lucene/analysis/id/IndonesianStemmer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    iget-boolean v4, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->stemDerivational:Z

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/id/IndonesianStemmer;->stem([CIZ)I

    move-result v0

    .line 60
    .local v0, "newlen":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/id/IndonesianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 62
    .end local v0    # "newlen":I
    :cond_0
    const/4 v1, 0x1

    .line 64
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
