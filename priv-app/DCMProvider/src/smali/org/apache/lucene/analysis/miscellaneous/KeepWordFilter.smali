.class public final Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;
.super Lorg/apache/lucene/analysis/util/FilteringTokenFilter;
.source "KeepWordFilter.java"


# instance fields
.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final words:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "words"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;)V

    .line 33
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 39
    iput-object p3, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;->words:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 40
    return-void
.end method


# virtual methods
.method public accept()Z
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;->words:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/KeepWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->contains([CII)Z

    move-result v0

    return v0
.end method
