.class public Lorg/apache/lucene/analysis/util/ElisionFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "ElisionFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/MultiTermAwareComponent;
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private articles:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final articlesFile:Ljava/lang/String;

.field private final ignoreCase:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 46
    const-string v0, "articles"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->articlesFile:Ljava/lang/String;

    .line 47
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->ignoreCase:Z

    .line 48
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/util/ElisionFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/util/ElisionFilter;
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 64
    new-instance v0, Lorg/apache/lucene/analysis/util/ElisionFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->articles:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/util/ElisionFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    return-object v0
.end method

.method public getMultiTermComponent()Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
    .locals 0

    .prologue
    .line 69
    return-object p0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->articlesFile:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 56
    sget-object v0, Lorg/apache/lucene/analysis/fr/FrenchAnalyzer;->DEFAULT_ARTICLES:Lorg/apache/lucene/analysis/util/CharArraySet;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->articles:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->articlesFile:Ljava/lang/String;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->ignoreCase:Z

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/ElisionFilterFactory;->articles:Lorg/apache/lucene/analysis/util/CharArraySet;

    goto :goto_0
.end method
