.class public Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;
.super Ljava/lang/Object;
.source "Hyphenation.java"


# instance fields
.field private hyphenPoints:[I


# direct methods
.method constructor <init>([I)V
    .locals 0
    .param p1, "points"    # [I

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;->hyphenPoints:[I

    .line 34
    return-void
.end method


# virtual methods
.method public getHyphenationPoints()[I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;->hyphenPoints:[I

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/Hyphenation;->hyphenPoints:[I

    array-length v0, v0

    return v0
.end method
