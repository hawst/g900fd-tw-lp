.class public Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;
.super Ljava/lang/Object;
.source "ByteVector.java"


# static fields
.field private static final DEFAULT_BLOCK_SIZE:I = 0x800


# instance fields
.field private array:[B

.field private blockSize:I

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x800

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;-><init>(I)V

    .line 47
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "capacity"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-lez p1, :cond_0

    .line 51
    iput p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    .line 55
    :goto_0
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    .line 57
    return-void

    .line 53
    :cond_0
    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    goto :goto_0
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "a"    # [B

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    .line 61
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    .line 63
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "a"    # [B
    .param p2, "capacity"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    if-lez p2, :cond_0

    .line 67
    iput p2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    .line 71
    :goto_0
    iput-object p1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    .line 73
    return-void

    .line 69
    :cond_0
    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    goto :goto_0
.end method


# virtual methods
.method public alloc(I)I
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 105
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    .line 106
    .local v1, "index":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    array-length v2, v3

    .line 107
    .local v2, "len":I
    iget v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    add-int/2addr v3, p1

    if-lt v3, v2, :cond_0

    .line 108
    iget v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->blockSize:I

    add-int/2addr v3, v2

    new-array v0, v3, [B

    .line 109
    .local v0, "aux":[B
    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    invoke-static {v3, v4, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 110
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    .line 112
    .end local v0    # "aux":[B
    :cond_0
    iget v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    add-int/2addr v3, p1

    iput v3, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    .line 113
    return v1
.end method

.method public capacity()I
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    array-length v0, v0

    return v0
.end method

.method public get(I)B
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public getArray()[B
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    return v0
.end method

.method public put(IB)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "val"    # B

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    aput-byte p2, v0, p1

    .line 95
    return-void
.end method

.method public trimToSize()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 118
    iget v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    new-array v0, v1, [B

    .line 119
    .local v0, "aux":[B
    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    iget v2, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->n:I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 120
    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/hyphenation/ByteVector;->array:[B

    .line 122
    .end local v0    # "aux":[B
    :cond_0
    return-void
.end method
