.class public Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "HyphenationCompoundWordTokenFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final dictFile:Ljava/lang/String;

.field private dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final encoding:Ljava/lang/String;

.field private final hypFile:Ljava/lang/String;

.field private hyphenator:Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

.field private final maxSubwordSize:I

.field private final minSubwordSize:I

.field private final minWordSize:I

.field private final onlyLongestMatch:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 74
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->assureMatchVersion()V

    .line 75
    const-string v0, "dictionary"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->dictFile:Ljava/lang/String;

    .line 76
    const-string v0, "encoding"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->encoding:Ljava/lang/String;

    .line 77
    const-string v0, "hyphenator"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->hypFile:Ljava/lang/String;

    .line 78
    const-string v0, "minWordSize"

    const/4 v1, 0x5

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->minWordSize:I

    .line 79
    const-string v0, "minSubwordSize"

    const/4 v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->minSubwordSize:I

    .line 80
    const-string v0, "maxSubwordSize"

    const/16 v1, 0xf

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->maxSubwordSize:I

    .line 81
    const-string v0, "onlyLongestMatch"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->onlyLongestMatch:Z

    .line 82
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;
    .locals 9
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 107
    new-instance v0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    iget-object v3, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->hyphenator:Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    iget-object v4, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget v5, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->minWordSize:I

    iget v6, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->minSubwordSize:I

    iget v7, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->maxSubwordSize:I

    iget-boolean v8, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->onlyLongestMatch:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;Lorg/apache/lucene/analysis/util/CharArraySet;IIIZ)V

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 6
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 89
    const/4 v1, 0x0

    .line 91
    .local v1, "stream":Ljava/io/InputStream;
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->dictFile:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 92
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->dictFile:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->dictionary:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 95
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->hypFile:Ljava/lang/String;

    invoke-interface {p1, v2}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 96
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 97
    .local v0, "is":Lorg/xml/sax/InputSource;
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->encoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/xml/sax/InputSource;->setEncoding(Ljava/lang/String;)V

    .line 98
    iget-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->hypFile:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lorg/xml/sax/InputSource;->setSystemId(Ljava/lang/String;)V

    .line 99
    invoke-static {v0}, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilter;->getHyphenationTree(Lorg/xml/sax/InputSource;)Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/compound/HyphenationCompoundWordTokenFilterFactory;->hyphenator:Lorg/apache/lucene/analysis/compound/hyphenation/HyphenationTree;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    new-array v2, v5, [Ljava/io/Closeable;

    .line 101
    aput-object v1, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 103
    return-void

    .line 100
    .end local v0    # "is":Lorg/xml/sax/InputSource;
    :catchall_0
    move-exception v2

    new-array v3, v5, [Ljava/io/Closeable;

    .line 101
    aput-object v1, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 102
    throw v2
.end method
