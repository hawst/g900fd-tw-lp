.class public final Lorg/apache/lucene/analysis/nl/DutchStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "DutchStemFilter.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "_in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 53
    new-instance v0, Lorg/apache/lucene/analysis/nl/DutchStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/nl/DutchStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

    .line 55
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 56
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 60
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/util/Map;)V
    .locals 1
    .param p1, "_in"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/analysis/TokenStream;",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p2, "stemdictionary":Ljava/util/Map;, "Ljava/util/Map<**>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 67
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->setStemDictionary(Ljava/util/Map;)V

    .line 68
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    iget-object v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "term":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v2

    if-nez v2, :cond_0

    .line 80
    iget-object v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->stem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "s":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    iget-object v2, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v2

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 85
    .end local v0    # "s":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x1

    .line 87
    .end local v1    # "term":Ljava/lang/String;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setStemDictionary(Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "dict":Ljava/util/HashMap;, "Ljava/util/HashMap<**>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/nl/DutchStemmer;->setStemDictionary(Ljava/util/Map;)V

    .line 107
    :cond_0
    return-void
.end method

.method public setStemmer(Lorg/apache/lucene/analysis/nl/DutchStemmer;)V
    .locals 0
    .param p1, "stemmer"    # Lorg/apache/lucene/analysis/nl/DutchStemmer;

    .prologue
    .line 95
    if-eqz p1, :cond_0

    .line 96
    iput-object p1, p0, Lorg/apache/lucene/analysis/nl/DutchStemFilter;->stemmer:Lorg/apache/lucene/analysis/nl/DutchStemmer;

    .line 98
    :cond_0
    return-void
.end method
