.class public Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "TypeTokenFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final enablePositionIncrements:Z

.field private stopTypes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final stopTypesFiles:Ljava/lang/String;

.field private final useWhitelist:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 51
    const-string v0, "types"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->stopTypesFiles:Ljava/lang/String;

    .line 52
    const-string v0, "enablePositionIncrements"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->enablePositionIncrements:Z

    .line 53
    const-string v0, "useWhitelist"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->useWhitelist:Z

    .line 54
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 81
    new-instance v0, Lorg/apache/lucene/analysis/core/TypeTokenFilter;

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->enablePositionIncrements:Z

    iget-object v2, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->stopTypes:Ljava/util/Set;

    iget-boolean v3, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->useWhitelist:Z

    invoke-direct {v0, v1, p1, v2, v3}, Lorg/apache/lucene/analysis/core/TypeTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;Ljava/util/Set;Z)V

    return-object v0
.end method

.method public getStopTypes()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->stopTypes:Ljava/util/Set;

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 5
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v3, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->stopTypesFiles:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 62
    .local v1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 63
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->stopTypes:Ljava/util/Set;

    .line 64
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 69
    :cond_0
    return-void

    .line 64
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, v4}, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 66
    .local v2, "typesLines":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->stopTypes:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public isEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/core/TypeTokenFilterFactory;->enablePositionIncrements:Z

    return v0
.end method
