.class public Lorg/apache/lucene/analysis/util/OpenStringBuilder;
.super Ljava/lang/Object;
.source "OpenStringBuilder.java"

# interfaces
.implements Ljava/lang/Appendable;
.implements Ljava/lang/CharSequence;


# instance fields
.field protected buf:[C

.field protected len:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;-><init>(I)V

    .line 29
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-array v0, p1, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    .line 33
    return-void
.end method

.method public constructor <init>([CI)V
    .locals 0
    .param p1, "arr"    # [C
    .param p2, "len"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->set([CI)V

    .line 37
    return-void
.end method


# virtual methods
.method public append(C)Ljava/lang/Appendable;
    .locals 0
    .param p1, "c"    # C

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->write(C)V

    .line 69
    return-object p0
.end method

.method public append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;
    .locals 2
    .param p1, "csq"    # Ljava/lang/CharSequence;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;

    move-result-object v0

    return-object v0
.end method

.method public append(Ljava/lang/CharSequence;II)Ljava/lang/Appendable;
    .locals 2
    .param p1, "csq"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 59
    sub-int v1, p3, p2

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->reserve(I)V

    .line 60
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 63
    return-object p0

    .line 61
    :cond_0
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public capacity()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    array-length v0, v0

    return v0
.end method

.method public charAt(I)C
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    aget-char v0, v0, p1

    return v0
.end method

.method public flush()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public getArray()[C
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    return-object v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    return v0
.end method

.method public reserve(I)V
    .locals 2
    .param p1, "num"    # I

    .prologue
    .line 104
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    array-length v1, v1

    if-le v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->resize(I)V

    .line 105
    :cond_0
    return-void
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    .line 140
    return-void
.end method

.method protected resize(I)V
    .locals 4
    .param p1, "len"    # I

    .prologue
    const/4 v3, 0x0

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    array-length v1, v1

    shl-int/lit8 v1, v1, 0x1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-array v0, v1, [C

    .line 99
    .local v0, "newbuf":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->size()I

    move-result v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    iput-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    .line 101
    return-void
.end method

.method public set([CI)V
    .locals 0
    .param p1, "arr"    # [C
    .param p2, "end"    # I

    .prologue
    .line 42
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    .line 43
    iput p2, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    .line 44
    return-void
.end method

.method public setCharAt(IC)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "ch"    # C

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    aput-char p2, v0, p1

    .line 79
    return-void
.end method

.method public setLength(I)V
    .locals 0
    .param p1, "len"    # I

    .prologue
    .line 39
    iput p1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    return v0
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 83
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toCharArray()[C
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->size()I

    move-result v1

    new-array v0, v1, [C

    .line 144
    .local v0, "newbuf":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->size()I

    move-result v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 145
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 150
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->size()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method public unsafeWrite(C)V
    .locals 3
    .param p1, "b"    # C

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    iget v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    aput-char p1, v0, v1

    .line 88
    return-void
.end method

.method public unsafeWrite(I)V
    .locals 1
    .param p1, "b"    # I

    .prologue
    .line 90
    int-to-char v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    return-void
.end method

.method public unsafeWrite([CII)V
    .locals 2
    .param p1, "b"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    iget v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    .line 95
    return-void
.end method

.method public write(C)V
    .locals 2
    .param p1, "b"    # C

    .prologue
    .line 108
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    iget-object v1, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 109
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->resize(I)V

    .line 111
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite(C)V

    .line 112
    return-void
.end method

.method public write(I)V
    .locals 1
    .param p1, "b"    # I

    .prologue
    .line 114
    int-to-char v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->write(C)V

    return-void
.end method

.method public write(Ljava/lang/String;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 130
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->reserve(I)V

    .line 131
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    iget v3, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    invoke-virtual {p1, v0, v1, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    .line 132
    iget v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    .line 133
    return-void
.end method

.method public final write(Lorg/apache/lucene/analysis/util/OpenStringBuilder;)V
    .locals 3
    .param p1, "arr"    # Lorg/apache/lucene/analysis/util/OpenStringBuilder;

    .prologue
    .line 126
    iget-object v0, p1, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->buf:[C

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->len:I

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->write([CII)V

    .line 127
    return-void
.end method

.method public final write([C)V
    .locals 2
    .param p1, "b"    # [C

    .prologue
    .line 117
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->write([CII)V

    .line 118
    return-void
.end method

.method public write([CII)V
    .locals 0
    .param p1, "b"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 121
    invoke-virtual {p0, p3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->reserve(I)V

    .line 122
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/analysis/util/OpenStringBuilder;->unsafeWrite([CII)V

    .line 123
    return-void
.end method
