.class public final Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;
.super Lorg/apache/lucene/analysis/AnalyzerWrapper;
.source "QueryAutoStopWordAnalyzer.java"


# static fields
.field public static final defaultMaxDocFreqPercent:F = 0.4f


# instance fields
.field private final delegate:Lorg/apache/lucene/analysis/Analyzer;

.field private final matchVersion:Lorg/apache/lucene/util/Version;

.field private final stopWordsPerField:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    const v0, 0x3ecccccd    # 0.4f

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;F)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;F)V
    .locals 6
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "maxPercentDocs"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-static {p3}, Lorg/apache/lucene/index/MultiFields;->getIndexedFields(Lorg/apache/lucene/index/IndexReader;)Ljava/util/Collection;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;Ljava/util/Collection;F)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;I)V
    .locals 6
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p4, "maxDocFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {p3}, Lorg/apache/lucene/index/MultiFields;->getIndexedFields(Lorg/apache/lucene/index/IndexReader;)Ljava/util/Collection;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;Ljava/util/Collection;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;Ljava/util/Collection;F)V
    .locals 6
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p5, "maxPercentDocs"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/IndexReader;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;F)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    .local p4, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p3}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p5

    float-to-int v5, v0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;Ljava/util/Collection;I)V

    .line 131
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/IndexReader;Ljava/util/Collection;I)V
    .locals 8
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "delegate"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "indexReader"    # Lorg/apache/lucene/index/IndexReader;
    .param p5, "maxDocFreq"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/Version;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/IndexReader;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    .local p4, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/apache/lucene/analysis/AnalyzerWrapper;-><init>()V

    .line 49
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->stopWordsPerField:Ljava/util/Map;

    .line 151
    iput-object p1, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 152
    iput-object p2, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    .line 154
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 170
    return-void

    .line 154
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 155
    .local v0, "field":Ljava/lang/String;
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 156
    .local v2, "stopWords":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p3, v0}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v4

    .line 157
    .local v4, "terms":Lorg/apache/lucene/index/Terms;
    new-instance v1, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 158
    .local v1, "spare":Lorg/apache/lucene/util/CharsRef;
    if-eqz v4, :cond_2

    .line 159
    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v3

    .line 161
    .local v3, "te":Lorg/apache/lucene/index/TermsEnum;
    :cond_1
    :goto_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    .local v5, "text":Lorg/apache/lucene/util/BytesRef;
    if-nez v5, :cond_3

    .line 168
    .end local v3    # "te":Lorg/apache/lucene/index/TermsEnum;
    .end local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->stopWordsPerField:Ljava/util/Map;

    invoke-interface {v7, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 162
    .restart local v3    # "te":Lorg/apache/lucene/index/TermsEnum;
    .restart local v5    # "text":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    invoke-virtual {v3}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v7

    if-le v7, p5, :cond_1

    .line 163
    invoke-static {v5, v1}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/CharsRef;)V

    .line 164
    invoke-virtual {v1}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public getStopWords(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 196
    iget-object v1, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->stopWordsPerField:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 197
    .local v0, "stopWords":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getStopWords()[Lorg/apache/lucene/index/Term;
    .locals 7

    .prologue
    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 207
    .local v0, "allStopWords":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Term;>;"
    iget-object v4, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->stopWordsPerField:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 213
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lorg/apache/lucene/index/Term;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lorg/apache/lucene/index/Term;

    return-object v4

    .line 207
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 208
    .local v1, "fieldName":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->stopWordsPerField:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 209
    .local v2, "stopWords":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 210
    .local v3, "text":Ljava/lang/String;
    new-instance v6, Lorg/apache/lucene/index/Term;

    invoke-direct {v6, v1, v3}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 174
    iget-object v0, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->delegate:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method protected wrapComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "components"    # Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    .prologue
    .line 179
    iget-object v2, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->stopWordsPerField:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 180
    .local v1, "stopWords":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v1, :cond_0

    .line 185
    .end local p2    # "components":Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    :goto_0
    return-object p2

    .line 183
    .restart local p2    # "components":Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v2, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenStream()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    .line 184
    new-instance v4, Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v5, p0, Lorg/apache/lucene/analysis/query/QueryAutoStopWordAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v1, v6}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    .line 183
    invoke-direct {v0, v2, v3, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 185
    .local v0, "stopFilter":Lorg/apache/lucene/analysis/core/StopFilter;
    new-instance v2, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenizer()Lorg/apache/lucene/analysis/Tokenizer;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object p2, v2

    goto :goto_0
.end method
