.class public Lorg/apache/lucene/analysis/fa/PersianCharFilterFactory;
.super Lorg/apache/lucene/analysis/util/CharFilterFactory;
.source "PersianCharFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/MultiTermAwareComponent;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharFilterFactory;-><init>(Ljava/util/Map;)V

    .line 44
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/fa/PersianCharFilterFactory;->create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/CharFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Ljava/io/Reader;)Lorg/apache/lucene/analysis/CharFilter;
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 51
    new-instance v0, Lorg/apache/lucene/analysis/fa/PersianCharFilter;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/fa/PersianCharFilter;-><init>(Ljava/io/Reader;)V

    return-object v0
.end method

.method public getMultiTermComponent()Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
    .locals 0

    .prologue
    .line 56
    return-object p0
.end method
