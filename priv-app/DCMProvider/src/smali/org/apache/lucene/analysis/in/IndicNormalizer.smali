.class public Lorg/apache/lucene/analysis/in/IndicNormalizer;
.super Ljava/lang/Object;
.source "IndicNormalizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;
    }
.end annotation


# static fields
.field private static final decompositions:[[I

.field private static final scripts:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "Ljava/lang/Character$UnicodeBlock;",
            "Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x4

    const/4 v9, 0x5

    .line 46
    new-instance v4, Ljava/util/IdentityHashMap;

    const/16 v5, 0x9

    invoke-direct {v4, v5}, Ljava/util/IdentityHashMap;-><init>(I)V

    .line 45
    sput-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    .line 53
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x900

    invoke-direct {v6, v12, v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x980

    invoke-direct {v6, v13, v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0xa00

    invoke-direct {v6, v10, v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x8

    const/16 v8, 0xa80

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x10

    const/16 v8, 0xb00

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x20

    const/16 v8, 0xb80

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x40

    const/16 v8, 0xc00

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x80

    const/16 v8, 0xc80

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    new-instance v6, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    const/16 v7, 0x100

    const/16 v8, 0xd00

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const/16 v4, 0x48

    new-array v4, v4, [[I

    .line 79
    new-array v5, v9, [I

    aput v9, v5, v11

    const/16 v6, 0x3e

    aput v6, v5, v12

    const/16 v6, 0x45

    aput v6, v5, v13

    const/4 v6, 0x3

    const/16 v7, 0x11

    aput v7, v5, v6

    sget-object v6, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v6}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v6

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    or-int/2addr v6, v7

    aput v6, v5, v10

    aput-object v5, v4, v11

    .line 81
    new-array v5, v9, [I

    aput v9, v5, v11

    const/16 v6, 0x3e

    aput v6, v5, v12

    const/16 v6, 0x46

    aput v6, v5, v13

    const/4 v6, 0x3

    const/16 v7, 0x12

    aput v7, v5, v6

    sget-object v6, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v6}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v6

    aput v6, v5, v10

    aput-object v5, v4, v12

    .line 83
    new-array v5, v9, [I

    aput v9, v5, v11

    const/16 v6, 0x3e

    aput v6, v5, v12

    const/16 v6, 0x47

    aput v6, v5, v13

    const/4 v6, 0x3

    const/16 v7, 0x13

    aput v7, v5, v6

    sget-object v6, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v6}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v6

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    or-int/2addr v6, v7

    aput v6, v5, v10

    aput-object v5, v4, v13

    const/4 v5, 0x3

    .line 85
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x3e

    aput v7, v6, v12

    const/16 v7, 0x48

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x14

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    .line 87
    new-array v5, v9, [I

    aput v9, v5, v11

    const/16 v6, 0x3e

    aput v6, v5, v12

    const/4 v6, -0x1

    aput v6, v5, v13

    const/4 v6, 0x3

    const/4 v7, 0x6

    aput v7, v5, v6

    sget-object v6, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v6}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v6

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    or-int/2addr v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    or-int/2addr v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    or-int/2addr v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    or-int/2addr v6, v7

    aput v6, v5, v10

    aput-object v5, v4, v10

    .line 89
    new-array v5, v9, [I

    aput v9, v5, v11

    const/16 v6, 0x45

    aput v6, v5, v12

    const/4 v6, -0x1

    aput v6, v5, v13

    const/4 v6, 0x3

    const/16 v7, 0x72

    aput v7, v5, v6

    sget-object v6, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v6}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v6

    aput v6, v5, v10

    aput-object v5, v4, v9

    const/4 v5, 0x6

    .line 91
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x45

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xd

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/4 v5, 0x7

    .line 93
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x46

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    aput v10, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x8

    .line 95
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x47

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xf

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x9

    .line 97
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x48

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x10

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0xa

    .line 99
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x49

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x11

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0xb

    .line 101
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x4a

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x12

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0xc

    .line 103
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x4b

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x13

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0xd

    .line 105
    new-array v6, v9, [I

    aput v9, v6, v11

    const/16 v7, 0x4c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x14

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0xe

    .line 107
    new-array v6, v9, [I

    const/4 v7, 0x6

    aput v7, v6, v11

    const/16 v7, 0x45

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x11

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0xf

    .line 109
    new-array v6, v9, [I

    const/4 v7, 0x6

    aput v7, v6, v11

    const/16 v7, 0x46

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x12

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x10

    .line 111
    new-array v6, v9, [I

    const/4 v7, 0x6

    aput v7, v6, v11

    const/16 v7, 0x47

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x13

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x11

    .line 113
    new-array v6, v9, [I

    const/4 v7, 0x6

    aput v7, v6, v11

    const/16 v7, 0x48

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x14

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x12

    .line 115
    new-array v6, v9, [I

    const/4 v7, 0x7

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x8

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x13

    .line 117
    new-array v6, v9, [I

    const/16 v7, 0x9

    aput v7, v6, v11

    const/16 v7, 0x41

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xa

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x14

    .line 119
    new-array v6, v9, [I

    const/16 v7, 0x9

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xa

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x15

    .line 121
    new-array v6, v9, [I

    const/16 v7, 0xe

    aput v7, v6, v11

    const/16 v7, 0x46

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x10

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x16

    .line 123
    new-array v6, v9, [I

    const/16 v7, 0xf

    aput v7, v6, v11

    const/16 v7, 0x45

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xd

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x17

    .line 125
    new-array v6, v9, [I

    const/16 v7, 0xf

    aput v7, v6, v11

    const/16 v7, 0x46

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xe

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x18

    .line 127
    new-array v6, v9, [I

    const/16 v7, 0xf

    aput v7, v6, v11

    const/16 v7, 0x47

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x10

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x19

    .line 129
    new-array v6, v9, [I

    const/16 v7, 0xf

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x10

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    .line 131
    new-array v6, v9, [I

    const/16 v7, 0x12

    aput v7, v6, v11

    const/16 v7, 0x3e

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x13

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    .line 133
    new-array v6, v9, [I

    const/16 v7, 0x12

    aput v7, v6, v11

    const/16 v7, 0x4c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x14

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x1c

    .line 135
    new-array v6, v9, [I

    const/16 v7, 0x12

    aput v7, v6, v11

    const/16 v7, 0x55

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x13

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x1d

    .line 137
    new-array v6, v9, [I

    const/16 v7, 0x12

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x14

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x1e

    .line 139
    new-array v6, v9, [I

    const/16 v7, 0x13

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x14

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x1f

    .line 141
    new-array v6, v9, [I

    const/16 v7, 0x15

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x58

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x20

    .line 143
    new-array v6, v9, [I

    const/16 v7, 0x16

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x59

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x21

    .line 145
    new-array v6, v9, [I

    const/16 v7, 0x17

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x5a

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x22

    .line 147
    new-array v6, v9, [I

    const/16 v7, 0x1c

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x5b

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x23

    .line 149
    new-array v6, v9, [I

    const/16 v7, 0x21

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x5c

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x24

    .line 151
    new-array v6, v9, [I

    const/16 v7, 0x22

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x5d

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x25

    .line 153
    new-array v6, v9, [I

    const/16 v7, 0x23

    aput v7, v6, v11

    const/16 v7, 0x4d

    aput v7, v6, v12

    const/16 v7, 0xff

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x7a

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x26

    .line 155
    new-array v6, v9, [I

    const/16 v7, 0x24

    aput v7, v6, v11

    const/16 v7, 0x4d

    aput v7, v6, v12

    const/16 v7, 0xff

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4e

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x27

    .line 157
    new-array v6, v9, [I

    const/16 v7, 0x28

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x29

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x28

    .line 159
    new-array v6, v9, [I

    const/16 v7, 0x28

    aput v7, v6, v11

    const/16 v7, 0x4d

    aput v7, v6, v12

    const/16 v7, 0xff

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x7b

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x29

    .line 161
    new-array v6, v9, [I

    const/16 v7, 0x2b

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x5e

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x2a

    .line 163
    new-array v6, v9, [I

    const/16 v7, 0x2f

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x5f

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x2b

    .line 165
    new-array v6, v9, [I

    const/16 v7, 0x2c

    aput v7, v6, v11

    const/16 v7, 0x41

    aput v7, v6, v12

    const/16 v7, 0x41

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xb

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x2c

    .line 167
    new-array v6, v9, [I

    const/16 v7, 0x30

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x31

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x2d

    .line 169
    new-array v6, v9, [I

    const/16 v7, 0x30

    aput v7, v6, v11

    const/16 v7, 0x4d

    aput v7, v6, v12

    const/16 v7, 0xff

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x7c

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x2e

    .line 171
    new-array v6, v9, [I

    const/16 v7, 0x32

    aput v7, v6, v11

    const/16 v7, 0x4d

    aput v7, v6, v12

    const/16 v7, 0xff

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x7d

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x2f

    .line 173
    new-array v6, v9, [I

    const/16 v7, 0x33

    aput v7, v6, v11

    const/16 v7, 0x3c

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x34

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x30

    .line 175
    new-array v6, v9, [I

    const/16 v7, 0x33

    aput v7, v6, v11

    const/16 v7, 0x4d

    aput v7, v6, v12

    const/16 v7, 0xff

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x7e

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x31

    .line 177
    new-array v6, v9, [I

    const/16 v7, 0x35

    aput v7, v6, v11

    const/16 v7, 0x41

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x2e

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x32

    .line 179
    new-array v6, v9, [I

    const/16 v7, 0x3e

    aput v7, v6, v11

    const/16 v7, 0x45

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x49

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x33

    .line 181
    new-array v6, v9, [I

    const/16 v7, 0x3e

    aput v7, v6, v11

    const/16 v7, 0x46

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4a

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x34

    .line 183
    new-array v6, v9, [I

    const/16 v7, 0x3e

    aput v7, v6, v11

    const/16 v7, 0x47

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4b

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x35

    .line 185
    new-array v6, v9, [I

    const/16 v7, 0x3e

    aput v7, v6, v11

    const/16 v7, 0x48

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4c

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->DEVANAGARI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->GUJARATI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x36

    .line 187
    new-array v6, v9, [I

    const/16 v7, 0x3f

    aput v7, v6, v11

    const/16 v7, 0x55

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x40

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x37

    .line 189
    new-array v6, v9, [I

    const/16 v7, 0x41

    aput v7, v6, v11

    const/16 v7, 0x41

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x42

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x38

    .line 191
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x3e

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4a

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x39

    .line 193
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x42

    aput v7, v6, v12

    const/16 v7, 0x55

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4b

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x3a

    .line 195
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x42

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4a

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x3b

    .line 197
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x46

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x48

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x3c

    .line 199
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x55

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x47

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x3d

    .line 201
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x56

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x48

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TELUGU:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x3e

    .line 203
    new-array v6, v9, [I

    const/16 v7, 0x46

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4c

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x3f

    .line 205
    new-array v6, v9, [I

    const/16 v7, 0x47

    aput v7, v6, v11

    const/16 v7, 0x3e

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4b

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->TAMIL:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->MALAYALAM:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x40

    .line 207
    new-array v6, v9, [I

    const/16 v7, 0x47

    aput v7, v6, v11

    const/16 v7, 0x57

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4c

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->BENGALI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    sget-object v8, Ljava/lang/Character$UnicodeBlock;->ORIYA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v8}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v8

    or-int/2addr v7, v8

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x41

    .line 209
    new-array v6, v9, [I

    const/16 v7, 0x4a

    aput v7, v6, v11

    const/16 v7, 0x55

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x4b

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->KANNADA:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x42

    .line 211
    new-array v6, v9, [I

    const/16 v7, 0x72

    aput v7, v6, v11

    const/16 v7, 0x3f

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/4 v8, 0x7

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x43

    .line 213
    new-array v6, v9, [I

    const/16 v7, 0x72

    aput v7, v6, v11

    const/16 v7, 0x40

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x8

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x44

    .line 215
    new-array v6, v9, [I

    const/16 v7, 0x72

    aput v7, v6, v11

    const/16 v7, 0x47

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xf

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x45

    .line 217
    new-array v6, v9, [I

    const/16 v7, 0x73

    aput v7, v6, v11

    const/16 v7, 0x41

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x9

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x46

    .line 219
    new-array v6, v9, [I

    const/16 v7, 0x73

    aput v7, v6, v11

    const/16 v7, 0x42

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0xa

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    const/16 v5, 0x47

    .line 221
    new-array v6, v9, [I

    const/16 v7, 0x73

    aput v7, v6, v11

    const/16 v7, 0x4b

    aput v7, v6, v12

    const/4 v7, -0x1

    aput v7, v6, v13

    const/4 v7, 0x3

    const/16 v8, 0x13

    aput v8, v6, v7

    sget-object v7, Ljava/lang/Character$UnicodeBlock;->GURMUKHI:Ljava/lang/Character$UnicodeBlock;

    invoke-static {v7}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->flag(Ljava/lang/Character$UnicodeBlock;)I

    move-result v7

    aput v7, v6, v10

    aput-object v6, v4, v5

    .line 77
    sput-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    .line 225
    sget-object v4, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    invoke-virtual {v4}, Ljava/util/IdentityHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v2, "i":I
    .local v3, "sd":Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 234
    return-void

    .line 225
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "sd":Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;
    check-cast v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    .line 226
    .restart local v3    # "sd":Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;
    new-instance v5, Ljava/util/BitSet;

    .end local v2    # "i":I
    const/16 v6, 0x7f

    invoke-direct {v5, v6}, Ljava/util/BitSet;-><init>(I)V

    iput-object v5, v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->decompMask:Ljava/util/BitSet;

    .line 227
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_0
    sget-object v5, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    array-length v5, v5

    if-ge v2, v5, :cond_0

    .line 228
    sget-object v5, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v5, v5, v2

    aget v0, v5, v11

    .line 229
    .local v0, "ch":I
    sget-object v5, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v5, v5, v2

    aget v1, v5, v10

    .line 230
    .local v1, "flags":I
    iget v5, v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->flag:I

    and-int/2addr v5, v1

    if-eqz v5, :cond_2

    .line 231
    iget-object v5, v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->decompMask:Ljava/util/BitSet;

    invoke-virtual {v5, v0}, Ljava/util/BitSet;->set(I)V

    .line 227
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private compose(ILjava/lang/Character$UnicodeBlock;Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;[CII)I
    .locals 9
    .param p1, "ch0"    # I
    .param p2, "block0"    # Ljava/lang/Character$UnicodeBlock;
    .param p3, "sd"    # Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;
    .param p4, "text"    # [C
    .param p5, "pos"    # I
    .param p6, "len"    # I

    .prologue
    .line 262
    add-int/lit8 v6, p5, 0x1

    if-lt v6, p6, :cond_0

    move v5, p6

    .line 292
    .end local p6    # "len":I
    .local v5, "len":I
    :goto_0
    return v5

    .line 265
    .end local v5    # "len":I
    .restart local p6    # "len":I
    :cond_0
    add-int/lit8 v6, p5, 0x1

    aget-char v6, p4, v6

    iget v7, p3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->base:I

    sub-int v2, v6, v7

    .line 266
    .local v2, "ch1":I
    add-int/lit8 v6, p5, 0x1

    aget-char v6, p4, v6

    invoke-static {v6}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 267
    .local v0, "block1":Ljava/lang/Character$UnicodeBlock;
    if-eq v0, p2, :cond_1

    move v5, p6

    .line 268
    .end local p6    # "len":I
    .restart local v5    # "len":I
    goto :goto_0

    .line 270
    .end local v5    # "len":I
    .restart local p6    # "len":I
    :cond_1
    const/4 v3, -0x1

    .line 272
    .local v3, "ch2":I
    add-int/lit8 v6, p5, 0x2

    if-ge v6, p6, :cond_2

    .line 273
    add-int/lit8 v6, p5, 0x2

    aget-char v6, p4, v6

    iget v7, p3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->base:I

    sub-int v3, v6, v7

    .line 274
    add-int/lit8 v6, p5, 0x2

    aget-char v6, p4, v6

    invoke-static {v6}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    .line 275
    .local v1, "block2":Ljava/lang/Character$UnicodeBlock;
    add-int/lit8 v6, p5, 0x2

    aget-char v6, p4, v6

    const/16 v7, 0x200d

    if-ne v6, v7, :cond_3

    .line 276
    const/16 v3, 0xff

    .line 281
    .end local v1    # "block2":Ljava/lang/Character$UnicodeBlock;
    :cond_2
    :goto_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    array-length v6, v6

    if-lt v4, v6, :cond_4

    move v5, p6

    .line 292
    .end local p6    # "len":I
    .restart local v5    # "len":I
    goto :goto_0

    .line 277
    .end local v4    # "i":I
    .end local v5    # "len":I
    .restart local v1    # "block2":Ljava/lang/Character$UnicodeBlock;
    .restart local p6    # "len":I
    :cond_3
    if-eq v1, v0, :cond_2

    .line 278
    const/4 v3, -0x1

    goto :goto_1

    .line 282
    .end local v1    # "block2":Ljava/lang/Character$UnicodeBlock;
    .restart local v4    # "i":I
    :cond_4
    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v6, v6, v4

    const/4 v7, 0x0

    aget v6, v6, v7

    if-ne v6, p1, :cond_7

    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v6, v6, v4

    const/4 v7, 0x4

    aget v6, v6, v7

    iget v7, p3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->flag:I

    and-int/2addr v6, v7

    if-eqz v6, :cond_7

    .line 283
    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v6, v6, v4

    const/4 v7, 0x1

    aget v6, v6, v7

    if-ne v6, v2, :cond_7

    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v6, v6, v4

    const/4 v7, 0x2

    aget v6, v6, v7

    if-ltz v6, :cond_5

    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v6, v6, v4

    const/4 v7, 0x2

    aget v6, v6, v7

    if-ne v6, v3, :cond_7

    .line 284
    :cond_5
    iget v6, p3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->base:I

    sget-object v7, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v7, v7, v4

    const/4 v8, 0x3

    aget v7, v7, v8

    add-int/2addr v6, v7

    int-to-char v6, v6

    aput-char v6, p4, p5

    .line 285
    add-int/lit8 v6, p5, 0x1

    invoke-static {p4, v6, p6}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p6

    .line 286
    sget-object v6, Lorg/apache/lucene/analysis/in/IndicNormalizer;->decompositions:[[I

    aget-object v6, v6, v4

    const/4 v7, 0x2

    aget v6, v6, v7

    if-ltz v6, :cond_6

    .line 287
    add-int/lit8 v6, p5, 0x1

    invoke-static {p4, v6, p6}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p6

    :cond_6
    move v5, p6

    .line 288
    .end local p6    # "len":I
    .restart local v5    # "len":I
    goto/16 :goto_0

    .line 281
    .end local v5    # "len":I
    .restart local p6    # "len":I
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method private static flag(Ljava/lang/Character$UnicodeBlock;)I
    .locals 1
    .param p0, "ub"    # Ljava/lang/Character$UnicodeBlock;

    .prologue
    .line 49
    sget-object v0, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, p0}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    iget v0, v0, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->flag:I

    return v0
.end method


# virtual methods
.method public normalize([CI)I
    .locals 7
    .param p1, "text"    # [C
    .param p2, "len"    # I

    .prologue
    .line 245
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, p2, :cond_0

    .line 254
    return p2

    .line 246
    :cond_0
    aget-char v0, p1, v5

    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    .line 247
    .local v2, "block":Ljava/lang/Character$UnicodeBlock;
    sget-object v0, Lorg/apache/lucene/analysis/in/IndicNormalizer;->scripts:Ljava/util/IdentityHashMap;

    invoke-virtual {v0, v2}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;

    .line 248
    .local v3, "sd":Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;
    if-eqz v3, :cond_1

    .line 249
    aget-char v0, p1, v5

    iget v4, v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->base:I

    sub-int v1, v0, v4

    .line 250
    .local v1, "ch":I
    iget-object v0, v3, Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;->decompMask:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v4, p1

    move v6, p2

    .line 251
    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/in/IndicNormalizer;->compose(ILjava/lang/Character$UnicodeBlock;Lorg/apache/lucene/analysis/in/IndicNormalizer$ScriptData;[CII)I

    move-result p2

    .line 245
    .end local v1    # "ch":I
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method
