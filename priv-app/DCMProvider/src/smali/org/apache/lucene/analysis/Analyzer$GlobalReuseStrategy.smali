.class public final Lorg/apache/lucene/analysis/Analyzer$GlobalReuseStrategy;
.super Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;
.source "Analyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/Analyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GlobalReuseStrategy"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;-><init>()V

    return-void
.end method


# virtual methods
.method public getReusableComponents(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 358
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Analyzer$GlobalReuseStrategy;->getStoredValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    return-object v0
.end method

.method public setReusableComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)V
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "components"    # Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    .prologue
    .line 363
    invoke-virtual {p0, p2}, Lorg/apache/lucene/analysis/Analyzer$GlobalReuseStrategy;->setStoredValue(Ljava/lang/Object;)V

    .line 364
    return-void
.end method
