.class public final Lorg/apache/lucene/analysis/NumericTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "NumericTokenStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/NumericTokenStream$NumericAttributeFactory;,
        Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;,
        Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttributeImpl;
    }
.end annotation


# static fields
.field public static final TOKEN_TYPE_FULL_PREC:Ljava/lang/String; = "fullPrecNumeric"

.field public static final TOKEN_TYPE_LOWER_PREC:Ljava/lang/String; = "lowerPrecNumeric"


# instance fields
.field private final numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final precisionStep:I

.field private final typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private valSize:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 225
    sget-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/analysis/NumericTokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;I)V

    .line 226
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "precisionStep"    # I

    .prologue
    .line 234
    sget-object v0, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/NumericTokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;I)V

    .line 235
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;I)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "precisionStep"    # I

    .prologue
    .line 245
    new-instance v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericAttributeFactory;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericAttributeFactory;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 323
    const-class v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    .line 324
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 325
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/NumericTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 327
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    .line 246
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "precisionStep must be >=1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    iput p2, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    .line 249
    iget-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    neg-int v1, p2

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->setShift(I)V

    .line 250
    return-void
.end method


# virtual methods
.method public getPrecisionStep()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    return v0
.end method

.method public incrementToken()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 305
    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    if-nez v1, :cond_0

    .line 306
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call set???Value() before usage"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 309
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/NumericTokenStream;->clearAttributes()V

    .line 311
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->incShift()I

    move-result v0

    .line 312
    .local v0, "shift":I
    iget-object v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    if-nez v0, :cond_1

    const-string v1, "fullPrecNumeric"

    :goto_0
    invoke-interface {v4, v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 313
    iget-object v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    if-nez v0, :cond_2

    move v1, v2

    :goto_1
    invoke-interface {v4, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 314
    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    if-ge v0, v1, :cond_3

    :goto_2
    return v2

    .line 312
    :cond_1
    const-string v1, "lowerPrecNumeric"

    goto :goto_0

    :cond_2
    move v1, v3

    .line 313
    goto :goto_1

    :cond_3
    move v2, v3

    .line 314
    goto :goto_2
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 298
    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call set???Value() before usage"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    iget v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    neg-int v1, v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->setShift(I)V

    .line 301
    return-void
.end method

.method public setDoubleValue(D)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 7
    .param p1, "value"    # D

    .prologue
    .line 281
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    invoke-static {p1, p2}, Lorg/apache/lucene/util/NumericUtils;->doubleToSortableLong(D)J

    move-result-wide v2

    const/16 v4, 0x40

    iput v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    neg-int v6, v0

    invoke-interface/range {v1 .. v6}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->init(JIII)V

    .line 282
    return-object p0
.end method

.method public setFloatValue(F)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 7
    .param p1, "value"    # F

    .prologue
    .line 292
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    invoke-static {p1}, Lorg/apache/lucene/util/NumericUtils;->floatToSortableInt(F)I

    move-result v0

    int-to-long v2, v0

    const/16 v4, 0x20

    iput v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    neg-int v6, v0

    invoke-interface/range {v1 .. v6}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->init(JIII)V

    .line 293
    return-object p0
.end method

.method public setIntValue(I)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 7
    .param p1, "value"    # I

    .prologue
    .line 270
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    int-to-long v2, p1

    const/16 v4, 0x20

    iput v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    neg-int v6, v0

    invoke-interface/range {v1 .. v6}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->init(JIII)V

    .line 271
    return-object p0
.end method

.method public setLongValue(J)Lorg/apache/lucene/analysis/NumericTokenStream;
    .locals 7
    .param p1, "value"    # J

    .prologue
    .line 259
    iget-object v1, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->numericAtt:Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;

    const/16 v4, 0x40

    iput v4, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->valSize:I

    iget v5, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    iget v0, p0, Lorg/apache/lucene/analysis/NumericTokenStream;->precisionStep:I

    neg-int v6, v0

    move-wide v2, p1

    invoke-interface/range {v1 .. v6}, Lorg/apache/lucene/analysis/NumericTokenStream$NumericTermAttribute;->init(JIII)V

    .line 260
    return-object p0
.end method
