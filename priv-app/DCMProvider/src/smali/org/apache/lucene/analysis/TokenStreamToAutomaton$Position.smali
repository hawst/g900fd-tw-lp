.class Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
.super Ljava/lang/Object;
.source "TokenStreamToAutomaton.java"

# interfaces
.implements Lorg/apache/lucene/util/RollingBuffer$Resettable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/TokenStreamToAutomaton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Position"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/util/RollingBuffer$Resettable;"
    }
.end annotation


# instance fields
.field arriving:Lorg/apache/lucene/util/automaton/State;

.field leaving:Lorg/apache/lucene/util/automaton/State;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;-><init>()V

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    iput-object v0, p0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    .line 60
    iput-object v0, p0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    .line 61
    return-void
.end method
