.class public final Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "EdgeNGramTokenizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;
    }
.end annotation


# static fields
.field public static final DEFAULT_MAX_GRAM_SIZE:I = 0x1

.field public static final DEFAULT_MIN_GRAM_SIZE:I = 0x1

.field public static final DEFAULT_SIDE:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;


# instance fields
.field private charsRead:I

.field private gramSize:I

.field private inLen:I

.field private inStr:Ljava/lang/String;

.field private maxGram:I

.field private minGram:I

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private side:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

.field private started:Z

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    sput-object v0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->DEFAULT_SIDE:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;Ljava/lang/String;II)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "sideLabel"    # Ljava/lang/String;
    .param p3, "minGram"    # I
    .param p4, "maxGram"    # I

    .prologue
    .line 119
    invoke-static {p2}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;->getSide(Ljava/lang/String;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;-><init>(Ljava/io/Reader;Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V

    .line 120
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "side"    # Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;
    .param p3, "minGram"    # I
    .param p4, "maxGram"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 40
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 41
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 93
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->init(Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;Ljava/lang/String;II)V
    .locals 6
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "sideLabel"    # Ljava/lang/String;
    .param p4, "minGram"    # I
    .param p5, "maxGram"    # I

    .prologue
    .line 132
    invoke-static {p3}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;->getSide(Ljava/lang/String;)Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V

    .line 133
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V
    .locals 1
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "side"    # Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;
    .param p4, "minGram"    # I
    .param p5, "maxGram"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 40
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 41
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 107
    invoke-direct {p0, p3, p4, p5}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->init(Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V

    .line 108
    return-void
.end method

.method private init(Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;II)V
    .locals 2
    .param p1, "side"    # Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;
    .param p2, "minGram"    # I
    .param p3, "maxGram"    # I

    .prologue
    .line 136
    if-nez p1, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sideLabel must be either front or back"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    const/4 v0, 0x1

    if-ge p2, v0, :cond_1

    .line 141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    if-le p2, p3, :cond_2

    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "minGram must not be greater than maxGram"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_2
    iput p2, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->minGram:I

    .line 149
    iput p3, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->maxGram:I

    .line 150
    iput-object p1, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->side:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    .line 151
    return-void
.end method


# virtual methods
.method public end()V
    .locals 2

    .prologue
    .line 218
    iget v1, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->correctOffset(I)I

    move-result v0

    .line 219
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 220
    return-void
.end method

.method public incrementToken()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x400

    const/4 v10, -0x1

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 156
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->clearAttributes()V

    .line 158
    iget-boolean v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->started:Z

    if-nez v6, :cond_6

    .line 159
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->started:Z

    .line 160
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->minGram:I

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    .line 161
    new-array v0, v11, [C

    .line 162
    .local v0, "chars":[C
    iput v3, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    .line 164
    :goto_0
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    array-length v7, v0

    if-lt v6, v7, :cond_3

    .line 172
    :cond_0
    new-instance v6, Ljava/lang/String;

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    invoke-direct {v6, v0, v3, v7}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inStr:Ljava/lang/String;

    .line 174
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    array-length v7, v0

    if-ne v6, v7, :cond_1

    .line 177
    new-array v4, v11, [C

    .line 179
    .local v4, "throwaway":[C
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->input:Ljava/io/Reader;

    array-length v7, v4

    invoke-virtual {v6, v4, v3, v7}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 180
    .local v2, "inc":I
    if-ne v2, v10, :cond_4

    .line 187
    .end local v2    # "inc":I
    .end local v4    # "throwaway":[C
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inStr:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inLen:I

    .line 188
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inLen:I

    if-nez v6, :cond_5

    .line 212
    .end local v0    # "chars":[C
    :cond_2
    :goto_2
    return v3

    .line 165
    .restart local v0    # "chars":[C
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->input:Ljava/io/Reader;

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    array-length v8, v0

    iget v9, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    sub-int/2addr v8, v9

    invoke-virtual {v6, v0, v7, v8}, Ljava/io/Reader;->read([CII)I

    move-result v2

    .line 166
    .restart local v2    # "inc":I
    if-eq v2, v10, :cond_0

    .line 169
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    add-int/2addr v6, v2

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    goto :goto_0

    .line 183
    .restart local v4    # "throwaway":[C
    :cond_4
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    add-int/2addr v6, v2

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->charsRead:I

    goto :goto_1

    .line 191
    .end local v2    # "inc":I
    .end local v4    # "throwaway":[C
    :cond_5
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v6, v5}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 197
    .end local v0    # "chars":[C
    :goto_3
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inLen:I

    if-gt v6, v7, :cond_2

    .line 202
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->maxGram:I

    if-gt v6, v7, :cond_2

    .line 207
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->side:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    sget-object v7, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;->FRONT:Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer$Side;

    if-ne v6, v7, :cond_7

    .line 208
    .local v3, "start":I
    :goto_4
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    add-int v1, v3, v6

    .line 209
    .local v1, "end":I
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inStr:Ljava/lang/String;

    invoke-interface {v6, v7, v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/CharSequence;II)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 210
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->correctOffset(I)I

    move-result v7

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->correctOffset(I)I

    move-result v8

    invoke-interface {v6, v7, v8}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 211
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    move v3, v5

    .line 212
    goto :goto_2

    .line 193
    .end local v1    # "end":I
    .end local v3    # "start":I
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v6, v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_3

    .line 207
    :cond_7
    iget v6, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->inLen:I

    iget v7, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->gramSize:I

    sub-int v3, v6, v7

    goto :goto_4
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/ngram/EdgeNGramTokenizer;->started:Z

    .line 226
    return-void
.end method
