.class public Lorg/apache/lucene/analysis/fa/PersianCharFilter;
.super Lorg/apache/lucene/analysis/CharFilter;
.source "PersianCharFilter.java"


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "in"    # Ljava/io/Reader;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/CharFilter;-><init>(Ljava/io/Reader;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected correct(I)I
    .locals 0
    .param p1, "currentOff"    # I

    .prologue
    .line 62
    return p1
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v1, p0, Lorg/apache/lucene/analysis/fa/PersianCharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    .line 53
    .local v0, "ch":I
    const/16 v1, 0x200c

    if-ne v0, v1, :cond_0

    .line 54
    const/16 v0, 0x20

    .line 56
    .end local v0    # "ch":I
    :cond_0
    return v0
.end method

.method public read([CII)I
    .locals 4
    .param p1, "cbuf"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v2, p0, Lorg/apache/lucene/analysis/fa/PersianCharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/Reader;->read([CII)I

    move-result v0

    .line 38
    .local v0, "charsRead":I
    if-lez v0, :cond_0

    .line 39
    add-int v1, p2, v0

    .line 40
    .local v1, "end":I
    :goto_0
    if-lt p2, v1, :cond_1

    .line 46
    .end local v1    # "end":I
    :cond_0
    return v0

    .line 41
    .restart local v1    # "end":I
    :cond_1
    aget-char v2, p1, p2

    const/16 v3, 0x200c

    if-ne v2, v3, :cond_2

    .line 42
    const/16 v2, 0x20

    aput-char v2, p1, p2

    .line 43
    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method
