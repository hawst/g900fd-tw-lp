.class public final Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "ReverseStringFilter.java"


# static fields
.field public static final INFORMATION_SEPARATOR_MARKER:C = '\u001f'

.field private static final NOMARKER:C = '\uffff'

.field public static final PUA_EC00_MARKER:C = '\uec00'

.field public static final RTL_DIRECTION_MARKER:C = '\u200f'

.field public static final START_OF_HEADING_MARKER:C = '\u0001'


# instance fields
.field private final marker:C

.field private final matchVersion:Lorg/apache/lucene/util/Version;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 81
    const v0, 0xffff

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;C)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;C)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "marker"    # C

    .prologue
    .line 97
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 45
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 98
    iput-object p1, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 99
    iput-char p3, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->marker:C

    .line 100
    return-void
.end method

.method public static reverse(Lorg/apache/lucene/util/Version;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 128
    .local v0, "charInput":[C
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->reverse(Lorg/apache/lucene/util/Version;[CII)V

    .line 129
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    return-object v1
.end method

.method public static reverse(Lorg/apache/lucene/util/Version;[C)V
    .locals 2
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "buffer"    # [C

    .prologue
    .line 138
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->reverse(Lorg/apache/lucene/util/Version;[CII)V

    .line 139
    return-void
.end method

.method public static reverse(Lorg/apache/lucene/util/Version;[CI)V
    .locals 1
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "buffer"    # [C
    .param p2, "len"    # I

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->reverse(Lorg/apache/lucene/util/Version;[CII)V

    .line 152
    return-void
.end method

.method public static reverse(Lorg/apache/lucene/util/Version;[CII)V
    .locals 14
    .param p0, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p1, "buffer"    # [C
    .param p2, "start"    # I
    .param p3, "len"    # I

    .prologue
    .line 179
    sget-object v12, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p0, v12}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 180
    invoke-static/range {p1 .. p3}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->reverseUnicode3([CII)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    const/4 v12, 0x2

    move/from16 v0, p3

    if-lt v0, v12, :cond_0

    .line 186
    add-int v12, p2, p3

    add-int/lit8 v3, v12, -0x1

    .line 187
    .local v3, "end":I
    aget-char v6, p1, p2

    .line 188
    .local v6, "frontHigh":C
    aget-char v5, p1, v3

    .line 189
    .local v5, "endLow":C
    const/4 v2, 0x1

    .local v2, "allowFrontSur":Z
    const/4 v1, 0x1

    .line 190
    .local v1, "allowEndSur":Z
    shr-int/lit8 v12, p3, 0x1

    add-int v9, p2, v12

    .line 191
    .local v9, "mid":I
    move/from16 v8, p2

    .end local v2    # "allowFrontSur":Z
    .local v8, "i":I
    :goto_1
    if-lt v8, v9, :cond_3

    .line 235
    and-int/lit8 v12, p3, 0x1

    const/4 v13, 0x1

    if-ne v12, v13, :cond_0

    if-eqz v2, :cond_2

    if-nez v1, :cond_0

    .line 237
    :cond_2
    if-eqz v2, :cond_a

    .end local v5    # "endLow":C
    :goto_2
    aput-char v5, p1, v3

    goto :goto_0

    .line 192
    .restart local v5    # "endLow":C
    :cond_3
    add-int/lit8 v12, v8, 0x1

    aget-char v7, p1, v12

    .line 193
    .local v7, "frontLow":C
    add-int/lit8 v12, v3, -0x1

    aget-char v4, p1, v12

    .line 194
    .local v4, "endHigh":C
    if-eqz v2, :cond_5

    .line 195
    invoke-static {v6, v7}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 194
    const/4 v11, 0x1

    .line 196
    .local v11, "surAtFront":Z
    :goto_3
    if-eqz v11, :cond_4

    const/4 v12, 0x3

    move/from16 v0, p3

    if-lt v0, v12, :cond_0

    .line 200
    :cond_4
    if-eqz v1, :cond_6

    .line 201
    invoke-static {v4, v5}, Ljava/lang/Character;->isSurrogatePair(CC)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 200
    const/4 v10, 0x1

    .line 202
    .local v10, "surAtEnd":Z
    :goto_4
    const/4 v1, 0x1

    move v2, v1

    .line 203
    .local v2, "allowFrontSur":I
    if-ne v11, v10, :cond_8

    .line 204
    if-eqz v11, :cond_7

    .line 206
    aput-char v7, p1, v3

    .line 207
    add-int/lit8 v3, v3, -0x1

    aput-char v6, p1, v3

    .line 208
    aput-char v4, p1, v8

    .line 209
    add-int/lit8 v8, v8, 0x1

    aput-char v5, p1, v8

    .line 210
    add-int/lit8 v12, v8, 0x1

    aget-char v6, p1, v12

    .line 211
    add-int/lit8 v12, v3, -0x1

    aget-char v5, p1, v12

    .line 191
    .end local v2    # "allowFrontSur":I
    :goto_5
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 194
    .end local v10    # "surAtEnd":Z
    .end local v11    # "surAtFront":Z
    :cond_5
    const/4 v11, 0x0

    goto :goto_3

    .line 200
    .restart local v11    # "surAtFront":Z
    :cond_6
    const/4 v10, 0x0

    goto :goto_4

    .line 214
    .restart local v2    # "allowFrontSur":I
    .restart local v10    # "surAtEnd":Z
    :cond_7
    aput-char v6, p1, v3

    .line 215
    aput-char v5, p1, v8

    .line 216
    move v6, v7

    .line 217
    move v5, v4

    .line 219
    goto :goto_5

    .line 220
    :cond_8
    if-eqz v11, :cond_9

    .line 222
    aput-char v7, p1, v3

    .line 223
    aput-char v5, p1, v8

    .line 224
    move v5, v4

    .line 225
    const/4 v2, 0x0

    .line 226
    .local v2, "allowFrontSur":Z
    goto :goto_5

    .line 228
    .local v2, "allowFrontSur":I
    :cond_9
    aput-char v6, p1, v3

    .line 229
    aput-char v4, p1, v8

    .line 230
    move v6, v7

    .line 231
    const/4 v1, 0x0

    goto :goto_5

    .end local v2    # "allowFrontSur":I
    .end local v4    # "endHigh":C
    .end local v7    # "frontLow":C
    .end local v10    # "surAtEnd":Z
    .end local v11    # "surAtFront":Z
    :cond_a
    move v5, v6

    .line 237
    goto :goto_2
.end method

.method private static reverseUnicode3([CII)V
    .locals 4
    .param p0, "buffer"    # [C
    .param p1, "start"    # I
    .param p2, "len"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 159
    const/4 v3, 0x1

    if-gt p2, v3, :cond_1

    .line 166
    :cond_0
    return-void

    .line 160
    :cond_1
    shr-int/lit8 v2, p2, 0x1

    .line 161
    .local v2, "num":I
    move v1, p1

    .local v1, "i":I
    :goto_0
    add-int v3, p1, v2

    if-ge v1, v3, :cond_0

    .line 162
    aget-char v0, p0, v1

    .line 163
    .local v0, "c":C
    mul-int/lit8 v3, p1, 0x2

    add-int/2addr v3, p2

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-char v3, p0, v3

    aput-char v3, p0, v1

    .line 164
    mul-int/lit8 v3, p1, 0x2

    add-int/2addr v3, p2

    sub-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    aput-char v0, p0, v3

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-object v2, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    iget-object v2, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 106
    .local v0, "len":I
    iget-char v2, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->marker:C

    const v3, 0xffff

    if-eq v2, v3, :cond_0

    .line 107
    add-int/lit8 v0, v0, 0x1

    .line 108
    iget-object v2, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 109
    iget-object v2, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    iget-char v4, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->marker:C

    aput-char v4, v2, v3

    .line 111
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v3, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v3

    invoke-static {v2, v3, v1, v0}, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->reverse(Lorg/apache/lucene/util/Version;[CII)V

    .line 112
    iget-object v1, p0, Lorg/apache/lucene/analysis/reverse/ReverseStringFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 113
    const/4 v1, 0x1

    .line 115
    .end local v0    # "len":I
    :cond_1
    return v1
.end method
