.class public abstract Lorg/apache/lucene/analysis/CharFilter;
.super Ljava/io/Reader;
.source "CharFilter.java"


# instance fields
.field protected final input:Ljava/io/Reader;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 0
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Ljava/io/Reader;-><init>(Ljava/lang/Object;)V

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Ljava/io/Reader;

    .line 54
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 66
    return-void
.end method

.method protected abstract correct(I)I
.end method

.method public final correctOffset(I)I
    .locals 2
    .param p1, "currentOff"    # I

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/CharFilter;->correct(I)I

    move-result v0

    .line 82
    .local v0, "corrected":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Ljava/io/Reader;

    instance-of v1, v1, Lorg/apache/lucene/analysis/CharFilter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/analysis/CharFilter;->input:Ljava/io/Reader;

    check-cast v1, Lorg/apache/lucene/analysis/CharFilter;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/analysis/CharFilter;->correctOffset(I)I

    move-result v0

    .end local v0    # "corrected":I
    :cond_0
    return v0
.end method
