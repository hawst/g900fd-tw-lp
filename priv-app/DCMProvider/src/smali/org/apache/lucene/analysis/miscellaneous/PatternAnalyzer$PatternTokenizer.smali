.class final Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "PatternAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PatternTokenizer"
.end annotation


# static fields
.field private static final locale:Ljava/util/Locale;


# instance fields
.field private matcher:Ljava/util/regex/Matcher;

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final pattern:Ljava/util/regex/Pattern;

.field private pos:I

.field private str:Ljava/lang/String;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private final toLowerCase:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 327
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->locale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;Ljava/util/regex/Pattern;Z)V
    .locals 1
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;
    .param p3, "toLowerCase"    # Z

    .prologue
    .line 332
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Ljava/io/Reader;)V

    .line 326
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->pos:I

    .line 328
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 329
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 333
    iput-object p2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->pattern:Ljava/util/regex/Pattern;

    .line 334
    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    .line 335
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->toLowerCase:Z

    .line 336
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 2

    .prologue
    .line 368
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->str:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->correctOffset(I)I

    move-result v0

    .line 369
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 370
    return-void
.end method

.method public final incrementToken()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 340
    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    if-nez v5, :cond_0

    .line 361
    :goto_0
    return v4

    .line 341
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->clearAttributes()V

    .line 343
    :cond_1
    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->pos:I

    .line 345
    .local v2, "start":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    .line 346
    .local v1, "isMatch":Z
    if-eqz v1, :cond_3

    .line 347
    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->start()I

    move-result v0

    .line 348
    .local v0, "end":I
    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->pos:I

    .line 354
    :goto_1
    if-eq v2, v0, :cond_4

    .line 355
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->str:Ljava/lang/String;

    invoke-virtual {v4, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 356
    .local v3, "text":Ljava/lang/String;
    iget-boolean v4, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->toLowerCase:Z

    if-eqz v4, :cond_2

    sget-object v4, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->locale:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 357
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v4

    invoke-interface {v4, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 358
    iget-object v4, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->correctOffset(I)I

    move-result v5

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->correctOffset(I)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 359
    const/4 v4, 0x1

    goto :goto_0

    .line 350
    .end local v0    # "end":I
    .end local v3    # "text":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->str:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    .line 351
    .restart local v0    # "end":I
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    goto :goto_1

    .line 361
    :cond_4
    if-nez v1, :cond_1

    goto :goto_0
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 374
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 375
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->input:Ljava/io/Reader;

    # invokes: Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toString(Ljava/io/Reader;)Ljava/lang/String;
    invoke-static {v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->access$0(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->str:Ljava/lang/String;

    .line 376
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->pattern:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->str:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->matcher:Ljava/util/regex/Matcher;

    .line 377
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;->pos:I

    .line 378
    return-void
.end method
