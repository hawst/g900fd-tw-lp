.class public Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;
.super Ljava/lang/Object;
.source "HungarianLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isVowel(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 229
    sparse-switch p1, :sswitch_data_0

    .line 236
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 235
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 229
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x79 -> :sswitch_0
    .end sparse-switch
.end method

.method private normalize([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 218
    const/4 v0, 0x3

    if-le p2, v0, :cond_0

    .line 219
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    .line 225
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 223
    .restart local p2    # "len":I
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 219
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
    .end sparse-switch
.end method

.method private removeCase([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 90
    const/4 v0, 0x6

    if-le p2, v0, :cond_1

    const-string v0, "kent"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    add-int/lit8 p2, p2, -0x4

    .line 141
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 93
    .restart local p2    # "len":I
    :cond_1
    const/4 v0, 0x5

    if-le p2, v0, :cond_5

    .line 94
    const-string v0, "nak"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 95
    const-string v0, "nek"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 96
    const-string v0, "val"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    const-string v0, "vel"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 98
    const-string v0, "ert"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 99
    const-string v0, "rol"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 100
    const-string v0, "ban"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 101
    const-string v0, "ben"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    const-string v0, "bol"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 103
    const-string v0, "nal"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 104
    const-string v0, "nel"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 105
    const-string v0, "hoz"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    const-string v0, "hez"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 107
    const-string v0, "tol"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    :cond_2
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 110
    :cond_3
    const-string v0, "al"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "el"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 111
    :cond_4
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_5

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    add-int/lit8 v1, p2, -0x4

    aget-char v1, p1, v1

    if-ne v0, v1, :cond_5

    .line 112
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 116
    :cond_5
    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    .line 117
    const-string v0, "at"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 118
    const-string v0, "et"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 119
    const-string v0, "ot"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 120
    const-string v0, "va"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 121
    const-string v0, "ve"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 122
    const-string v0, "ra"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 123
    const-string v0, "re"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 124
    const-string v0, "ba"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 125
    const-string v0, "be"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 126
    const-string v0, "ul"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 127
    const-string v0, "ig"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 128
    :cond_6
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 130
    :cond_7
    const-string v0, "on"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "en"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_9

    .line 131
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 133
    :cond_9
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 137
    :sswitch_0
    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    add-int/lit8 v1, p2, -0x3

    aget-char v1, p1, v1

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 135
    :sswitch_1
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 133
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x6e -> :sswitch_1
        0x74 -> :sswitch_1
    .end sparse-switch
.end method

.method private removePlural([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 207
    const/4 v0, 0x3

    if-le p2, v0, :cond_1

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x6b

    if-ne v0, v1, :cond_1

    .line 208
    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    .line 212
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 214
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 211
    .restart local p2    # "len":I
    :sswitch_0
    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 208
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x6f -> :sswitch_0
    .end sparse-switch
.end method

.method private removePossessive([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 145
    const/4 v0, 0x6

    if-le p2, v0, :cond_4

    .line 146
    add-int/lit8 v0, p2, -0x5

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    const-string v0, "atok"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const-string v0, "otok"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    const-string v0, "etek"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    :cond_0
    add-int/lit8 p2, p2, -0x4

    .line 202
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 152
    .restart local p2    # "len":I
    :cond_2
    const-string v0, "itek"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "itok"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 153
    :cond_3
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 156
    :cond_4
    const/4 v0, 0x5

    if-le p2, v0, :cond_8

    .line 157
    add-int/lit8 v0, p2, -0x4

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_6

    .line 158
    const-string v0, "unk"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 159
    const-string v0, "tok"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 160
    const-string v0, "tek"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 161
    :cond_5
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 163
    :cond_6
    add-int/lit8 v0, p2, -0x4

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "juk"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 164
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 166
    :cond_7
    const-string v0, "ink"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 167
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 170
    :cond_8
    const/4 v0, 0x4

    if-le p2, v0, :cond_e

    .line 171
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_a

    .line 172
    const-string v0, "am"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 173
    const-string v0, "em"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 174
    const-string v0, "om"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 175
    const-string v0, "ad"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 176
    const-string v0, "ed"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 177
    const-string v0, "od"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 178
    const-string v0, "uk"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 179
    :cond_9
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 181
    :cond_a
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 182
    const-string v0, "nk"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 183
    const-string v0, "ja"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 184
    const-string v0, "je"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 185
    :cond_b
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 187
    :cond_c
    const-string v0, "im"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 188
    const-string v0, "id"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 189
    const-string v0, "ik"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 190
    :cond_d
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 193
    :cond_e
    const/4 v0, 0x3

    if-le p2, v0, :cond_1

    .line 194
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 196
    :sswitch_0
    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 198
    :sswitch_1
    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 199
    :sswitch_2
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 194
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x64 -> :sswitch_1
        0x65 -> :sswitch_0
        0x69 -> :sswitch_2
        0x6d -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public stem([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 83
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->removeCase([CI)I

    move-result p2

    .line 84
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->removePossessive([CI)I

    move-result p2

    .line 85
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->removePlural([CI)I

    move-result p2

    .line 86
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/hu/HungarianLightStemmer;->normalize([CI)I

    move-result v1

    return v1

    .line 67
    :cond_0
    aget-char v1, p1, v0

    sparse-switch v1, :sswitch_data_0

    .line 66
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :sswitch_0
    const/16 v1, 0x61

    aput-char v1, p1, v0

    goto :goto_1

    .line 70
    :sswitch_1
    const/16 v1, 0x65

    aput-char v1, p1, v0

    goto :goto_1

    .line 71
    :sswitch_2
    const/16 v1, 0x69

    aput-char v1, p1, v0

    goto :goto_1

    .line 75
    :sswitch_3
    const/16 v1, 0x6f

    aput-char v1, p1, v0

    goto :goto_1

    .line 80
    :sswitch_4
    const/16 v1, 0x75

    aput-char v1, p1, v0

    goto :goto_1

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0xe1 -> :sswitch_0
        0xe9 -> :sswitch_1
        0xeb -> :sswitch_1
        0xed -> :sswitch_2
        0xf3 -> :sswitch_3
        0xf5 -> :sswitch_3
        0xf6 -> :sswitch_3
        0xfa -> :sswitch_4
        0xfb -> :sswitch_4
        0xfc -> :sswitch_4
        0x151 -> :sswitch_3
        0x169 -> :sswitch_4
        0x171 -> :sswitch_4
    .end sparse-switch
.end method
