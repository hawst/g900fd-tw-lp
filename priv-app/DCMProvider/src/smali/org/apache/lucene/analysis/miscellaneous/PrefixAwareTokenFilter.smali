.class public Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "PrefixAwareTokenFilter.java"


# instance fields
.field private flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

.field private offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private p_flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

.field private p_offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private p_payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field private p_posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private p_termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private p_typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field private posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private prefix:Lorg/apache/lucene/analysis/TokenStream;

.field private prefixExhausted:Z

.field private previousPrefixToken:Lorg/apache/lucene/analysis/Token;

.field private reusableToken:Lorg/apache/lucene/analysis/Token;

.field private suffix:Lorg/apache/lucene/analysis/TokenStream;

.field private termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "prefix"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "suffix"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 62
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 82
    new-instance v0, Lorg/apache/lucene/analysis/Token;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/Token;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->previousPrefixToken:Lorg/apache/lucene/analysis/Token;

    .line 83
    new-instance v0, Lorg/apache/lucene/analysis/Token;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/Token;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->reusableToken:Lorg/apache/lucene/analysis/Token;

    .line 63
    iput-object p2, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    .line 64
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefixExhausted:Z

    .line 67
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 68
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 69
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 70
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 71
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 72
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    .line 74
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 75
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 76
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 77
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 78
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 79
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    .line 80
    return-void
.end method

.method private getNextPrefixInputToken(Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;
    .locals 3
    .param p1, "token"    # Lorg/apache/lucene/analysis/Token;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 134
    .end local p1    # "token":Lorg/apache/lucene/analysis/Token;
    :goto_0
    return-object p1

    .line 128
    .restart local p1    # "token":Lorg/apache/lucene/analysis/Token;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setPositionIncrement(I)V

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->getFlags()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setFlags(I)V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/analysis/Token;->setOffset(II)V

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setType(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->p_payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method private getNextSuffixInputToken(Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;
    .locals 3
    .param p1, "token"    # Lorg/apache/lucene/analysis/Token;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 145
    .end local p1    # "token":Lorg/apache/lucene/analysis/Token;
    :goto_0
    return-object p1

    .line 139
    .restart local p1    # "token":Lorg/apache/lucene/analysis/Token;
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lorg/apache/lucene/analysis/Token;->copyBuffer([CII)V

    .line 140
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setPositionIncrement(I)V

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->getFlags()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setFlags(I)V

    .line 142
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/analysis/Token;->setOffset(II)V

    .line 143
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setType(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-interface {v0}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/analysis/Token;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method private setCurrentToken(Lorg/apache/lucene/analysis/Token;)V
    .locals 4
    .param p1, "token"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 116
    if-nez p1, :cond_0

    .line 124
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->clearAttributes()V

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->buffer()[C

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->length()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->getPositionIncrement()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->flagsAtt:Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->getFlags()I

    move-result v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/FlagsAttribute;->setFlags(I)V

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->startOffset()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->type()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->payloadAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 170
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 171
    return-void
.end method

.method public end()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 164
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 165
    return-void
.end method

.method public getPrefix()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    return-object v0
.end method

.method public getSuffix()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    return-object v0
.end method

.method public final incrementToken()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 89
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefixExhausted:Z

    if-nez v3, :cond_0

    .line 90
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->reusableToken:Lorg/apache/lucene/analysis/Token;

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->getNextPrefixInputToken(Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    .line 91
    .local v0, "nextToken":Lorg/apache/lucene/analysis/Token;
    if-nez v0, :cond_1

    .line 92
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefixExhausted:Z

    .line 105
    .end local v0    # "nextToken":Lorg/apache/lucene/analysis/Token;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->reusableToken:Lorg/apache/lucene/analysis/Token;

    invoke-direct {p0, v3}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->getNextSuffixInputToken(Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    .line 106
    .restart local v0    # "nextToken":Lorg/apache/lucene/analysis/Token;
    if-nez v0, :cond_3

    .line 107
    const/4 v2, 0x0

    .line 112
    :goto_0
    return v2

    .line 94
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->previousPrefixToken:Lorg/apache/lucene/analysis/Token;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/analysis/Token;->reinit(Lorg/apache/lucene/analysis/Token;)V

    .line 96
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->previousPrefixToken:Lorg/apache/lucene/analysis/Token;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/Token;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 97
    .local v1, "p":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_2

    .line 98
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->previousPrefixToken:Lorg/apache/lucene/analysis/Token;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->clone()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/analysis/Token;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 100
    :cond_2
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->setCurrentToken(Lorg/apache/lucene/analysis/Token;)V

    goto :goto_0

    .line 110
    .end local v1    # "p":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->previousPrefixToken:Lorg/apache/lucene/analysis/Token;

    invoke-virtual {p0, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->updateSuffixToken(Lorg/apache/lucene/analysis/Token;Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;

    move-result-object v0

    .line 111
    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->setCurrentToken(Lorg/apache/lucene/analysis/Token;)V

    goto :goto_0
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 176
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    if-eqz v0, :cond_0

    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefixExhausted:Z

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 180
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v0}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 185
    :cond_1
    return-void
.end method

.method public setPrefix(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 0
    .param p1, "prefix"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 192
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->prefix:Lorg/apache/lucene/analysis/TokenStream;

    .line 193
    return-void
.end method

.method public setSuffix(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 0
    .param p1, "suffix"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 200
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/PrefixAwareTokenFilter;->suffix:Lorg/apache/lucene/analysis/TokenStream;

    .line 201
    return-void
.end method

.method public updateSuffixToken(Lorg/apache/lucene/analysis/Token;Lorg/apache/lucene/analysis/Token;)Lorg/apache/lucene/analysis/Token;
    .locals 3
    .param p1, "suffixToken"    # Lorg/apache/lucene/analysis/Token;
    .param p2, "lastPrefixToken"    # Lorg/apache/lucene/analysis/Token;

    .prologue
    .line 156
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->startOffset()I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/lucene/analysis/Token;->endOffset()I

    move-result v2

    add-int/2addr v1, v2

    .line 156
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/analysis/Token;->setOffset(II)V

    .line 158
    return-object p1
.end method
