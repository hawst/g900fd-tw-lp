.class public final Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "GermanNormalizationFilter.java"


# static fields
.field private static final N:I = 0x0

.field private static final U:I = 0x2

.field private static final V:I = 0x1


# instance fields
.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 49
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 53
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x73

    const/4 v6, 0x2

    const/4 v7, 0x1

    .line 57
    iget-object v8, p0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v8}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 58
    const/4 v5, 0x0

    .line 59
    .local v5, "state":I
    iget-object v8, p0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v8}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 60
    .local v0, "buffer":[C
    iget-object v8, p0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v8}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    .line 61
    .local v4, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_0
    if-lt v3, v4, :cond_0

    .line 106
    iget-object v6, p0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 109
    .end local v0    # "buffer":[C
    .end local v3    # "i":I
    .end local v4    # "length":I
    .end local v5    # "state":I
    :goto_1
    return v7

    .line 62
    .restart local v0    # "buffer":[C
    .restart local v3    # "i":I
    .restart local v4    # "length":I
    .restart local v5    # "state":I
    :cond_0
    aget-char v1, v0, v3

    .line 63
    .local v1, "c":C
    sparse-switch v1, :sswitch_data_0

    .line 103
    const/4 v5, 0x0

    move v2, v3

    .line 61
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 66
    :sswitch_0
    const/4 v5, 0x2

    move v2, v3

    .line 67
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 69
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_1
    if-nez v5, :cond_1

    move v5, v6

    :goto_3
    move v2, v3

    .line 70
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    :cond_1
    move v5, v7

    .line 69
    goto :goto_3

    .line 72
    :sswitch_2
    if-ne v5, v6, :cond_4

    .line 73
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    invoke-static {v0, v3, v4}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result v4

    .line 74
    :goto_4
    const/4 v5, 0x1

    .line 75
    goto :goto_2

    .line 79
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_3
    const/4 v5, 0x1

    move v2, v3

    .line 80
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 82
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_4
    const/16 v8, 0x61

    aput-char v8, v0, v3

    .line 83
    const/4 v5, 0x1

    move v2, v3

    .line 84
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 86
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_5
    const/16 v8, 0x6f

    aput-char v8, v0, v3

    .line 87
    const/4 v5, 0x1

    move v2, v3

    .line 88
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 90
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_6
    const/16 v8, 0x75

    aput-char v8, v0, v3

    .line 91
    const/4 v5, 0x1

    move v2, v3

    .line 92
    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 94
    .end local v2    # "i":I
    .restart local v3    # "i":I
    :sswitch_7
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    aput-char v10, v0, v3

    .line 95
    iget-object v8, p0, Lorg/apache/lucene/analysis/de/GermanNormalizationFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v9, v4, 0x1

    invoke-interface {v8, v9}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    move-result-object v0

    .line 96
    if-ge v2, v4, :cond_2

    .line 97
    add-int/lit8 v8, v2, 0x1

    sub-int v9, v4, v2

    invoke-static {v0, v2, v0, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 98
    :cond_2
    aput-char v10, v0, v2

    .line 99
    add-int/lit8 v4, v4, 0x1

    .line 100
    const/4 v5, 0x0

    .line 101
    goto :goto_2

    .line 109
    .end local v0    # "buffer":[C
    .end local v1    # "c":C
    .end local v2    # "i":I
    .end local v4    # "length":I
    .end local v5    # "state":I
    :cond_3
    const/4 v7, 0x0

    goto :goto_1

    .restart local v0    # "buffer":[C
    .restart local v1    # "c":C
    .restart local v3    # "i":I
    .restart local v4    # "length":I
    .restart local v5    # "state":I
    :cond_4
    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_4

    .line 63
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_2
        0x69 -> :sswitch_3
        0x6f -> :sswitch_0
        0x71 -> :sswitch_3
        0x75 -> :sswitch_1
        0x79 -> :sswitch_3
        0xdf -> :sswitch_7
        0xe4 -> :sswitch_4
        0xf6 -> :sswitch_5
        0xfc -> :sswitch_6
    .end sparse-switch
.end method
