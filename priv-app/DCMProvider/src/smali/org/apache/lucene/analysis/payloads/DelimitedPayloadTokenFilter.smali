.class public final Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "DelimitedPayloadTokenFilter.java"


# static fields
.field public static final DEFAULT_DELIMITER:C = '|'


# instance fields
.field private final delimiter:C

.field private final encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

.field private final payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;CLorg/apache/lucene/analysis/payloads/PayloadEncoder;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p2, "delimiter"    # C
    .param p3, "encoder"    # Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 42
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 43
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    .line 49
    iput-char p2, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->delimiter:C

    .line 50
    iput-object p3, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    .line 51
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 55
    iget-object v4, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v4}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 56
    iget-object v4, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 57
    .local v0, "buffer":[C
    iget-object v4, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    .line 58
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 66
    iget-object v4, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 68
    .end local v0    # "buffer":[C
    .end local v1    # "i":I
    .end local v2    # "length":I
    :goto_1
    return v3

    .line 59
    .restart local v0    # "buffer":[C
    .restart local v1    # "i":I
    .restart local v2    # "length":I
    :cond_0
    aget-char v4, v0, v1

    iget-char v5, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->delimiter:C

    if-ne v4, v5, :cond_1

    .line 60
    iget-object v4, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->payAtt:Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;

    iget-object v5, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    add-int/lit8 v6, v1, 0x1

    add-int/lit8 v7, v1, 0x1

    sub-int v7, v2, v7

    invoke-interface {v5, v0, v6, v7}, Lorg/apache/lucene/analysis/payloads/PayloadEncoder;->encode([CII)Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    invoke-interface {v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/PayloadAttribute;->setPayload(Lorg/apache/lucene/util/BytesRef;)V

    .line 61
    iget-object v4, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4, v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    goto :goto_1

    .line 58
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    .end local v0    # "buffer":[C
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
