.class public final Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;
.super Lorg/apache/lucene/analysis/AnalyzerWrapper;
.source "ShingleAnalyzerWrapper.java"


# instance fields
.field private final defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

.field private final maxShingleSize:I

.field private final minShingleSize:I

.field private final outputUnigrams:Z

.field private final outputUnigramsIfNoShingles:Z

.field private final tokenSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;)V
    .locals 1
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;

    .prologue
    .line 41
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;-><init>(Lorg/apache/lucene/analysis/Analyzer;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;I)V
    .locals 1
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "maxShingleSize"    # I

    .prologue
    .line 45
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;-><init>(Lorg/apache/lucene/analysis/Analyzer;II)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;II)V
    .locals 7
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "minShingleSize"    # I
    .param p3, "maxShingleSize"    # I

    .prologue
    .line 49
    const-string v4, " "

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;-><init>(Lorg/apache/lucene/analysis/Analyzer;IILjava/lang/String;ZZ)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/analysis/Analyzer;IILjava/lang/String;ZZ)V
    .locals 2
    .param p1, "defaultAnalyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "minShingleSize"    # I
    .param p3, "maxShingleSize"    # I
    .param p4, "tokenSeparator"    # Ljava/lang/String;
    .param p5, "outputUnigrams"    # Z
    .param p6, "outputUnigramsIfNoShingles"    # Z

    .prologue
    const/4 v0, 0x2

    .line 67
    invoke-direct {p0}, Lorg/apache/lucene/analysis/AnalyzerWrapper;-><init>()V

    .line 74
    iput-object p1, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 76
    if-ge p3, v0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Max shingle size must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    iput p3, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->maxShingleSize:I

    .line 81
    if-ge p2, v0, :cond_1

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Min shingle size must be >= 2"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_1
    if-le p2, p3, :cond_2

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 86
    const-string v1, "Min shingle size must be <= max shingle size"

    .line 85
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_2
    iput p2, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->minShingleSize:I

    .line 90
    if-nez p4, :cond_3

    const-string p4, ""

    .end local p4    # "tokenSeparator":Ljava/lang/String;
    :cond_3
    iput-object p4, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->tokenSeparator:Ljava/lang/String;

    .line 91
    iput-boolean p5, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->outputUnigrams:Z

    .line 92
    iput-boolean p6, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->outputUnigramsIfNoShingles:Z

    .line 93
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    const/4 v0, 0x2

    .line 99
    invoke-direct {p0, p1, v0, v0}, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;-><init>(Lorg/apache/lucene/util/Version;II)V

    .line 100
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;II)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "minShingleSize"    # I
    .param p3, "maxShingleSize"    # I

    .prologue
    .line 106
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;-><init>(Lorg/apache/lucene/util/Version;)V

    invoke-direct {p0, v0, p2, p3}, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;-><init>(Lorg/apache/lucene/analysis/Analyzer;II)V

    .line 107
    return-void
.end method


# virtual methods
.method public getMaxShingleSize()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->maxShingleSize:I

    return v0
.end method

.method public getMinShingleSize()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->minShingleSize:I

    return v0
.end method

.method public getTokenSeparator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->tokenSeparator:Ljava/lang/String;

    return-object v0
.end method

.method protected getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->defaultAnalyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public isOutputUnigrams()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->outputUnigrams:Z

    return v0
.end method

.method public isOutputUnigramsIfNoShingles()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->outputUnigramsIfNoShingles:Z

    return v0
.end method

.method protected wrapComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 4
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "components"    # Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    .prologue
    .line 146
    new-instance v0, Lorg/apache/lucene/analysis/shingle/ShingleFilter;

    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenStream()Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->minShingleSize:I

    iget v3, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->maxShingleSize:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;II)V

    .line 147
    .local v0, "filter":Lorg/apache/lucene/analysis/shingle/ShingleFilter;
    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->minShingleSize:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setMinShingleSize(I)V

    .line 148
    iget v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->maxShingleSize:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setMaxShingleSize(I)V

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->tokenSeparator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setTokenSeparator(Ljava/lang/String;)V

    .line 150
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->outputUnigrams:Z

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setOutputUnigrams(Z)V

    .line 151
    iget-boolean v1, p0, Lorg/apache/lucene/analysis/shingle/ShingleAnalyzerWrapper;->outputUnigramsIfNoShingles:Z

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/shingle/ShingleFilter;->setOutputUnigramsIfNoShingles(Z)V

    .line 152
    new-instance v1, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-virtual {p2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->getTokenizer()Lorg/apache/lucene/analysis/Tokenizer;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v1
.end method
