.class public Lorg/apache/lucene/analysis/lv/LatvianStemmer;
.super Ljava/lang/Object;
.source "LatvianStemmer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;
    }
.end annotation


# static fields
.field static final affixes:[Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 53
    const/16 v0, 0x26

    new-array v0, v0, [Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    .line 54
    new-instance v1, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v2, "ajiem"

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v1, v0, v4

    new-instance v1, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v2, "ajai"

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v1, v0, v5

    .line 55
    new-instance v1, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v2, "ajam"

    invoke-direct {v1, v2, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v1, v0, v6

    new-instance v1, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v2, "aj\u0101m"

    invoke-direct {v1, v2, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v1, v0, v3

    const/4 v1, 0x4

    .line 56
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "ajos"

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "aj\u0101s"

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 57
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "iem"

    invoke-direct {v2, v3, v6, v5}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "aj\u0101"

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 58
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "ais"

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "ai"

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 59
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "ei"

    invoke-direct {v2, v3, v6, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0101m"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 60
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "am"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0113m"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 61
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u012bm"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "im"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 62
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "um"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "us"

    invoke-direct {v2, v3, v5, v5}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 63
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "as"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0101s"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 64
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "es"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "os"

    invoke-direct {v2, v3, v5, v5}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 65
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "ij"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u012bs"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 66
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0113s"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "is"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 67
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "ie"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "u"

    invoke-direct {v2, v3, v5, v5}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 68
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "a"

    invoke-direct {v2, v3, v5, v5}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "i"

    invoke-direct {v2, v3, v5, v5}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 69
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "e"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0101"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 70
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0113"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u012b"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 71
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u016b"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "o"

    invoke-direct {v2, v3, v5, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 72
    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string v3, "s"

    invoke-direct {v2, v3, v4, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-instance v2, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    const-string/jumbo v3, "\u0161"

    invoke-direct {v2, v3, v4, v4}, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;-><init>(Ljava/lang/String;IZ)V

    aput-object v2, v0, v1

    .line 53
    sput-object v0, Lorg/apache/lucene/analysis/lv/LatvianStemmer;->affixes:[Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private numVowels([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 163
    const/4 v1, 0x0

    .line 164
    .local v1, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 172
    return v1

    .line 165
    :cond_0
    aget-char v2, p1, v0

    sparse-switch v2, :sswitch_data_0

    .line 164
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :sswitch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 165
    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x101 -> :sswitch_0
        0x113 -> :sswitch_0
        0x12b -> :sswitch_0
        0x16b -> :sswitch_0
    .end sparse-switch
.end method

.method private unpalatalize([CI)I
    .locals 6
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v5, 0x7a

    const/16 v4, 0x73

    const/16 v3, 0x6e

    const/16 v2, 0x6c

    .line 99
    aget-char v0, p1, p2

    const/16 v1, 0x75

    if-ne v0, v1, :cond_1

    .line 101
    const-string v0, "k\u0161"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    add-int/lit8 p2, p2, 0x1

    .line 103
    add-int/lit8 v0, p2, -0x2

    aput-char v4, p1, v0

    .line 104
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x74

    aput-char v1, p1, v0

    move v0, p2

    .line 155
    :goto_0
    return v0

    .line 108
    :cond_0
    const-string/jumbo v0, "\u0146\u0146"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    add-int/lit8 v0, p2, -0x2

    aput-char v3, p1, v0

    .line 110
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    move v0, p2

    .line 111
    goto :goto_0

    .line 116
    :cond_1
    const-string v0, "pj"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "bj"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 117
    const-string v0, "mj"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "vj"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    :cond_2
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 120
    :cond_3
    const-string/jumbo v0, "\u0161\u0146"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    add-int/lit8 v0, p2, -0x2

    aput-char v4, p1, v0

    .line 122
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    move v0, p2

    .line 123
    goto :goto_0

    .line 124
    :cond_4
    const-string/jumbo v0, "\u017e\u0146"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 125
    add-int/lit8 v0, p2, -0x2

    aput-char v5, p1, v0

    .line 126
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    move v0, p2

    .line 127
    goto :goto_0

    .line 128
    :cond_5
    const-string/jumbo v0, "\u0161\u013c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 129
    add-int/lit8 v0, p2, -0x2

    aput-char v4, p1, v0

    .line 130
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    move v0, p2

    .line 131
    goto :goto_0

    .line 132
    :cond_6
    const-string/jumbo v0, "\u017e\u013c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 133
    add-int/lit8 v0, p2, -0x2

    aput-char v5, p1, v0

    .line 134
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    move v0, p2

    .line 135
    goto/16 :goto_0

    .line 136
    :cond_7
    const-string/jumbo v0, "\u013c\u0146"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 137
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 138
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    move v0, p2

    .line 139
    goto/16 :goto_0

    .line 140
    :cond_8
    const-string/jumbo v0, "\u013c\u013c"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 141
    add-int/lit8 v0, p2, -0x2

    aput-char v2, p1, v0

    .line 142
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    move v0, p2

    .line 143
    goto/16 :goto_0

    .line 144
    :cond_9
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x10d

    if-ne v0, v1, :cond_a

    .line 145
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x63

    aput-char v1, p1, v0

    move v0, p2

    .line 146
    goto/16 :goto_0

    .line 147
    :cond_a
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x13c

    if-ne v0, v1, :cond_b

    .line 148
    add-int/lit8 v0, p2, -0x1

    aput-char v2, p1, v0

    move v0, p2

    .line 149
    goto/16 :goto_0

    .line 150
    :cond_b
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x146

    if-ne v0, v1, :cond_c

    .line 151
    add-int/lit8 v0, p2, -0x1

    aput-char v3, p1, v0

    move v0, p2

    .line 152
    goto/16 :goto_0

    :cond_c
    move v0, p2

    .line 155
    goto/16 :goto_0
.end method


# virtual methods
.method public stem([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/lv/LatvianStemmer;->numVowels([CI)I

    move-result v2

    .line 42
    .local v2, "numVowels":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v3, Lorg/apache/lucene/analysis/lv/LatvianStemmer;->affixes:[Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    move v3, p2

    .line 50
    :goto_1
    return v3

    .line 43
    :cond_0
    sget-object v3, Lorg/apache/lucene/analysis/lv/LatvianStemmer;->affixes:[Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;

    aget-object v0, v3, v1

    .line 44
    .local v0, "affix":Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;
    iget v3, v0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->vc:I

    if-le v2, v3, :cond_2

    iget-object v3, v0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->affix:[C

    array-length v3, v3

    add-int/lit8 v3, v3, 0x3

    if-lt p2, v3, :cond_2

    iget-object v3, v0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->affix:[C

    invoke-static {p1, p2, v3}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CI[C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 45
    iget-object v3, v0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->affix:[C

    array-length v3, v3

    sub-int/2addr p2, v3

    .line 46
    iget-boolean v3, v0, Lorg/apache/lucene/analysis/lv/LatvianStemmer$Affix;->palatalizes:Z

    if-eqz v3, :cond_1

    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/lv/LatvianStemmer;->unpalatalize([CI)I

    move-result v3

    goto :goto_1

    :cond_1
    move v3, p2

    goto :goto_1

    .line 42
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
