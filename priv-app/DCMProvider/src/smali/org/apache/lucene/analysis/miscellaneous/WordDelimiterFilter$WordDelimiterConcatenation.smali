.class final Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;
.super Ljava/lang/Object;
.source "WordDelimiterFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "WordDelimiterConcatenation"
.end annotation


# instance fields
.field final buffer:Ljava/lang/StringBuilder;

.field endOffset:I

.field startOffset:I

.field subwordCount:I

.field final synthetic this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

.field type:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)V
    .locals 1

    .prologue
    .line 508
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 509
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    return-void
.end method


# virtual methods
.method append([CII)V
    .locals 1
    .param p1, "text"    # [C
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 523
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 524
    iget v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->subwordCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->subwordCount:I

    .line 525
    return-void
.end method

.method clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 564
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 565
    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->subwordCount:I

    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->type:I

    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->endOffset:I

    iput v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->startOffset:I

    .line 566
    return-void
.end method

.method isEmpty()Z
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method write()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 531
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->clearAttributes()V

    .line 532
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$0(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 533
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$0(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 535
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$0(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 537
    .local v0, "termbuffer":[C
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v1, v4, v2, v0, v4}, Ljava/lang/StringBuilder;->getChars(II[CI)V

    .line 538
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$0(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 540
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->hasIllegalOffsets:Z
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$1(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 541
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$2(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedStartOffset:I
    invoke-static {v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$3(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedEndOffset:I
    invoke-static {v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$4(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 546
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->posIncAttribute:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$5(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    const/4 v3, 0x1

    # invokes: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->position(Z)I
    invoke-static {v2, v3}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$6(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;Z)I

    move-result v2

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 547
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->typeAttribute:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$7(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->savedType:Ljava/lang/String;
    invoke-static {v2}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$8(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->setType(Ljava/lang/String;)V

    .line 548
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    invoke-static {v1, v4}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$9(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;I)V

    .line 549
    return-void

    .line 544
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->this$0:Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;

    # getter for: Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    invoke-static {v1}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;->access$2(Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter;)Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->startOffset:I

    iget v3, p0, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->endOffset:I

    invoke-interface {v1, v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto :goto_0
.end method

.method writeAndClear()V
    .locals 0

    .prologue
    .line 572
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->write()V

    .line 573
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/WordDelimiterFilter$WordDelimiterConcatenation;->clear()V

    .line 574
    return-void
.end method
