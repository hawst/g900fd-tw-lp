.class public Lorg/apache/lucene/analysis/TokenStreamToAutomaton;
.super Ljava/lang/Object;
.source "TokenStreamToAutomaton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;,
        Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Positions;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final HOLE:I = 0x101

.field public static final POS_SEP:I = 0x100


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;->$assertionsDisabled:Z

    .line 82
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method private static addHoles(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/RollingBuffer;I)V
    .locals 7
    .param p0, "startState"    # Lorg/apache/lucene/util/automaton/State;
    .param p2, "pos"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/automaton/State;",
            "Lorg/apache/lucene/util/RollingBuffer",
            "<",
            "Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "positions":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;>;"
    const/16 v6, 0x100

    .line 205
    invoke-virtual {p1, p2}, Lorg/apache/lucene/util/RollingBuffer;->get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;

    .line 206
    .local v0, "posData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    add-int/lit8 v2, p2, -0x1

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/RollingBuffer;->get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;

    .line 208
    .local v1, "prevPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    :goto_0
    iget-object v2, v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    if-eqz v2, :cond_1

    .line 231
    :cond_0
    return-void

    .line 209
    :cond_1
    iget-object v2, v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    if-nez v2, :cond_2

    .line 210
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    iput-object v2, v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    .line 211
    iget-object v2, v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    iget-object v4, v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v3, v6, v4}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 213
    :cond_2
    iget-object v2, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    if-nez v2, :cond_3

    .line 214
    const/4 v2, 0x1

    if-ne p2, v2, :cond_4

    .line 215
    iput-object p0, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    .line 219
    :goto_1
    iget-object v2, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    if-eqz v2, :cond_3

    .line 220
    iget-object v2, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    iget-object v4, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v3, v6, v4}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 223
    :cond_3
    iget-object v2, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    new-instance v3, Lorg/apache/lucene/util/automaton/Transition;

    const/16 v4, 0x101

    iget-object v5, v0, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 224
    add-int/lit8 p2, p2, -0x1

    .line 225
    if-lez p2, :cond_0

    .line 228
    move-object v0, v1

    .line 229
    add-int/lit8 v2, p2, -0x1

    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/RollingBuffer;->get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v1

    .end local v1    # "prevPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    check-cast v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;

    .restart local v1    # "prevPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    goto :goto_0

    .line 217
    :cond_4
    new-instance v2, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    goto :goto_1
.end method


# virtual methods
.method protected changeToken(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 75
    return-object p1
.end method

.method public toAutomaton(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/util/automaton/Automaton;
    .locals 25
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    new-instance v2, Lorg/apache/lucene/util/automaton/Automaton;

    invoke-direct {v2}, Lorg/apache/lucene/util/automaton/Automaton;-><init>()V

    .line 90
    .local v2, "a":Lorg/apache/lucene/util/automaton/Automaton;
    const/4 v4, 0x1

    .line 92
    .local v4, "deterministic":Z
    const-class v21, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v20

    check-cast v20, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;

    .line 93
    .local v20, "termBytesAtt":Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;
    const-class v21, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 94
    .local v14, "posIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    const-class v21, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v15

    check-cast v15, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;

    .line 95
    .local v15, "posLengthAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;
    const-class v21, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 97
    .local v10, "offsetAtt":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    invoke-interface/range {v20 .. v20}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->getBytesRef()Lorg/apache/lucene/util/BytesRef;

    move-result-object v18

    .line 99
    .local v18, "term":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 104
    new-instance v16, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Positions;

    const/16 v21, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Positions;-><init>(Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Positions;)V

    .line 106
    .local v16, "positions":Lorg/apache/lucene/util/RollingBuffer;, "Lorg/apache/lucene/util/RollingBuffer<Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;>;"
    const/4 v11, -0x1

    .line 107
    .local v11, "pos":I
    const/4 v12, 0x0

    .line 108
    .local v12, "posData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    const/4 v8, 0x0

    .line 109
    .local v8, "maxOffset":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v21

    if-nez v21, :cond_1

    .line 168
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 169
    const/4 v7, 0x0

    .line 170
    .local v7, "endState":Lorg/apache/lucene/util/automaton/State;
    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v21

    move/from16 v0, v21

    if-le v0, v8, :cond_0

    .line 171
    new-instance v7, Lorg/apache/lucene/util/automaton/State;

    .end local v7    # "endState":Lorg/apache/lucene/util/automaton/State;
    invoke-direct {v7}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    .line 172
    .restart local v7    # "endState":Lorg/apache/lucene/util/automaton/State;
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    .line 175
    :cond_0
    add-int/lit8 v11, v11, 0x1

    .line 176
    :goto_1
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/util/RollingBuffer;->getMaxPos()I

    move-result v21

    move/from16 v0, v21

    if-le v11, v0, :cond_b

    .line 189
    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/automaton/Automaton;->setDeterministic(Z)V

    .line 190
    return-object v2

    .line 110
    .end local v7    # "endState":Lorg/apache/lucene/util/automaton/State;
    :cond_1
    invoke-interface {v14}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v13

    .line 111
    .local v13, "posInc":I
    sget-boolean v21, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;->$assertionsDisabled:Z

    if-nez v21, :cond_2

    const/16 v21, -0x1

    move/from16 v0, v21

    if-gt v11, v0, :cond_2

    if-gtz v13, :cond_2

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 113
    :cond_2
    if-lez v13, :cond_8

    .line 116
    add-int/2addr v11, v13

    .line 118
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lorg/apache/lucene/util/RollingBuffer;->get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v12

    .end local v12    # "posData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    check-cast v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;

    .line 119
    .restart local v12    # "posData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    sget-boolean v21, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;->$assertionsDisabled:Z

    if-nez v21, :cond_3

    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 121
    :cond_3
    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    if-nez v21, :cond_7

    .line 123
    if-nez v11, :cond_6

    .line 125
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->getInitialState()Lorg/apache/lucene/util/automaton/State;

    move-result-object v21

    move-object/from16 v0, v21

    iput-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    .line 141
    :cond_4
    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lorg/apache/lucene/util/RollingBuffer;->freeBefore(I)V

    .line 149
    :goto_3
    invoke-interface {v15}, Lorg/apache/lucene/analysis/tokenattributes/PositionLengthAttribute;->getPositionLength()I

    move-result v21

    add-int v5, v11, v21

    .line 151
    .local v5, "endPos":I
    invoke-interface/range {v20 .. v20}, Lorg/apache/lucene/analysis/tokenattributes/TermToBytesRefAttribute;->fillBytesRef()I

    .line 152
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;->changeToken(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v19

    .line 153
    .local v19, "term2":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Lorg/apache/lucene/util/RollingBuffer;->get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;

    .line 154
    .local v6, "endPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    iget-object v0, v6, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    if-nez v21, :cond_5

    .line 155
    new-instance v21, Lorg/apache/lucene/util/automaton/State;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    move-object/from16 v0, v21

    iput-object v0, v6, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    .line 158
    :cond_5
    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v17, v0

    .line 159
    .local v17, "state":Lorg/apache/lucene/util/automaton/State;
    const/4 v3, 0x0

    .local v3, "byteIDX":I
    :goto_4
    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v3, v0, :cond_9

    .line 165
    invoke-interface {v10}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v21

    move/from16 v0, v21

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v8

    goto/16 :goto_0

    .line 129
    .end local v3    # "byteIDX":I
    .end local v5    # "endPos":I
    .end local v6    # "endPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    .end local v17    # "state":Lorg/apache/lucene/util/automaton/State;
    .end local v19    # "term2":Lorg/apache/lucene/util/BytesRef;
    :cond_6
    new-instance v21, Lorg/apache/lucene/util/automaton/State;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    move-object/from16 v0, v21

    iput-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    .line 130
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->getInitialState()Lorg/apache/lucene/util/automaton/State;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-static {v0, v1, v11}, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;->addHoles(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/RollingBuffer;I)V

    goto :goto_2

    .line 133
    :cond_7
    new-instance v21, Lorg/apache/lucene/util/automaton/State;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    move-object/from16 v0, v21

    iput-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    .line 134
    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    new-instance v22, Lorg/apache/lucene/util/automaton/Transition;

    const/16 v23, 0x100

    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->leaving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v24, v0

    invoke-direct/range {v22 .. v24}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 135
    const/16 v21, 0x1

    move/from16 v0, v21

    if-le v13, v0, :cond_4

    .line 138
    invoke-virtual {v2}, Lorg/apache/lucene/util/automaton/Automaton;->getInitialState()Lorg/apache/lucene/util/automaton/State;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-static {v0, v1, v11}, Lorg/apache/lucene/analysis/TokenStreamToAutomaton;->addHoles(Lorg/apache/lucene/util/automaton/State;Lorg/apache/lucene/util/RollingBuffer;I)V

    goto/16 :goto_2

    .line 146
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 160
    .restart local v3    # "byteIDX":I
    .restart local v5    # "endPos":I
    .restart local v6    # "endPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    .restart local v17    # "state":Lorg/apache/lucene/util/automaton/State;
    .restart local v19    # "term2":Lorg/apache/lucene/util/BytesRef;
    :cond_9
    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ne v3, v0, :cond_a

    iget-object v9, v6, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    .line 161
    .local v9, "nextState":Lorg/apache/lucene/util/automaton/State;
    :goto_5
    new-instance v21, Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v22, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v23, v0

    add-int v23, v23, v3

    aget-byte v22, v22, v23

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v9}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 162
    move-object/from16 v17, v9

    .line 159
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    .line 160
    .end local v9    # "nextState":Lorg/apache/lucene/util/automaton/State;
    :cond_a
    new-instance v9, Lorg/apache/lucene/util/automaton/State;

    invoke-direct {v9}, Lorg/apache/lucene/util/automaton/State;-><init>()V

    goto :goto_5

    .line 177
    .end local v3    # "byteIDX":I
    .end local v5    # "endPos":I
    .end local v6    # "endPosData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    .end local v13    # "posInc":I
    .end local v17    # "state":Lorg/apache/lucene/util/automaton/State;
    .end local v19    # "term2":Lorg/apache/lucene/util/BytesRef;
    .restart local v7    # "endState":Lorg/apache/lucene/util/automaton/State;
    :cond_b
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lorg/apache/lucene/util/RollingBuffer;->get(I)Lorg/apache/lucene/util/RollingBuffer$Resettable;

    move-result-object v12

    .end local v12    # "posData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    check-cast v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;

    .line 178
    .restart local v12    # "posData":Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;
    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    if-eqz v21, :cond_c

    .line 179
    if-eqz v7, :cond_d

    .line 180
    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    new-instance v22, Lorg/apache/lucene/util/automaton/Transition;

    const/16 v23, 0x100

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/util/automaton/Transition;-><init>(ILorg/apache/lucene/util/automaton/State;)V

    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/util/automaton/State;->addTransition(Lorg/apache/lucene/util/automaton/Transition;)V

    .line 185
    :cond_c
    :goto_6
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .line 182
    :cond_d
    iget-object v0, v12, Lorg/apache/lucene/analysis/TokenStreamToAutomaton$Position;->arriving:Lorg/apache/lucene/util/automaton/State;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/util/automaton/State;->setAccept(Z)V

    goto :goto_6
.end method
