.class public final Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "TeeSinkTokenFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SinkTokenStream"
.end annotation


# instance fields
.field private final cachedStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field

.field private filter:Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

.field private finalState:Lorg/apache/lucene/util/AttributeSource$State;

.field private it:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/util/AttributeSource$State;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;)V
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;
    .param p2, "filter"    # Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

    .prologue
    .line 195
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;)V

    .line 189
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    .line 196
    iput-object p2, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->filter:Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

    .line 197
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource;Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;)V

    return-void
.end method

.method private accept(Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 1
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->filter:Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;->accept(Lorg/apache/lucene/util/AttributeSource;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->accept(Lorg/apache/lucene/util/AttributeSource;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->addState(Lorg/apache/lucene/util/AttributeSource$State;)V

    return-void
.end method

.method static synthetic access$3(Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->setFinalState(Lorg/apache/lucene/util/AttributeSource$State;)V

    return-void
.end method

.method private addState(Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    .line 205
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The tee must be consumed before sinks are consumed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    return-void
.end method

.method private setFinalState(Lorg/apache/lucene/util/AttributeSource$State;)V
    .locals 0
    .param p1, "finalState"    # Lorg/apache/lucene/util/AttributeSource$State;

    .prologue
    .line 211
    iput-object p1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    .line 212
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->finalState:Lorg/apache/lucene/util/AttributeSource$State;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 235
    :cond_0
    return-void
.end method

.method public final incrementToken()Z
    .locals 2

    .prologue
    .line 217
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    if-nez v1, :cond_0

    .line 218
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    .line 221
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 222
    const/4 v1, 0x0

    .line 227
    :goto_0
    return v1

    .line 225
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/AttributeSource$State;

    .line 226
    .local v0, "state":Lorg/apache/lucene/util/AttributeSource$State;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->restoreState(Lorg/apache/lucene/util/AttributeSource$State;)V

    .line 227
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->cachedStates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkTokenStream;->it:Ljava/util/Iterator;

    .line 240
    return-void
.end method
