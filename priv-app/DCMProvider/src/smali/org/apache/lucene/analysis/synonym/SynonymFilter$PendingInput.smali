.class Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;
.super Ljava/lang/Object;
.source "SynonymFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/synonym/SynonymFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingInput"
.end annotation


# instance fields
.field consumed:Z

.field endOffset:I

.field keepOrig:Z

.field matched:Z

.field startOffset:I

.field state:Lorg/apache/lucene/util/AttributeSource$State;

.field final term:Lorg/apache/lucene/util/CharsRef;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->term:Lorg/apache/lucene/util/CharsRef;

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->consumed:Z

    .line 136
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;-><init>()V

    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->state:Lorg/apache/lucene/util/AttributeSource$State;

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->consumed:Z

    .line 148
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->keepOrig:Z

    .line 149
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilter$PendingInput;->matched:Z

    .line 150
    return-void
.end method
