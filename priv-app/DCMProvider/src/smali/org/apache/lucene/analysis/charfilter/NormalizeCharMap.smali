.class public Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;
.super Ljava/lang/Object;
.source "NormalizeCharMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final cachedRootArcs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Character;",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;>;"
        }
    .end annotation
.end field

.field final map:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "map":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/CharsRef;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->cachedRootArcs:Ljava/util/Map;

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->map:Lorg/apache/lucene/util/fst/FST;

    .line 48
    if-eqz p1, :cond_1

    .line 51
    :try_start_0
    new-instance v2, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v2}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 52
    .local v2, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    .line 53
    .local v0, "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    invoke-virtual {p1, v2}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 54
    invoke-static {v2}, Lorg/apache/lucene/util/fst/FST;->targetHasArcs(Lorg/apache/lucene/util/fst/FST$Arc;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 55
    iget-wide v4, v2, Lorg/apache/lucene/util/fst/FST$Arc;->target:J

    invoke-virtual {p1, v4, v5, v2, v0}, Lorg/apache/lucene/util/fst/FST;->readFirstRealTargetArc(JLorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 57
    :goto_0
    sget-boolean v3, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget v3, v2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    .end local v0    # "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v2    # "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    :catch_0
    move-exception v1

    .line 68
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 58
    .end local v1    # "ioe":Ljava/io/IOException;
    .restart local v0    # "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .restart local v2    # "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    :cond_0
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->cachedRootArcs:Ljava/util/Map;

    iget v4, v2, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    int-to-char v4, v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    invoke-virtual {v5, v2}, Lorg/apache/lucene/util/fst/FST$Arc;->copyFrom(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST$Arc;->isLast()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 71
    .end local v0    # "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v2    # "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    :cond_1
    return-void

    .line 62
    .restart local v0    # "fstReader":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .restart local v2    # "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/CharsRef;>;"
    :cond_2
    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/util/fst/FST;->readNextRealArc(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    return-void
.end method
