.class public Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;
.super Lorg/apache/lucene/analysis/Tokenizer;
.source "PathHierarchyTokenizer.java"


# static fields
.field private static final DEFAULT_BUFFER_SIZE:I = 0x400

.field public static final DEFAULT_DELIMITER:C = '/'

.field public static final DEFAULT_SKIP:I


# instance fields
.field private charsRead:I

.field private final delimiter:C

.field private endDelimiter:Z

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final replacement:C

.field private resultToken:Ljava/lang/StringBuilder;

.field private final skip:I

.field private skipped:I

.field private startPosition:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    const/16 v3, 0x2f

    .line 47
    const/16 v2, 0x400

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CC)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "delimiter"    # C
    .param p3, "replacement"    # C

    .prologue
    .line 59
    const/16 v2, 0x400

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CCI)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "delimiter"    # C
    .param p3, "replacement"    # C
    .param p4, "skip"    # I

    .prologue
    .line 63
    const/16 v2, 0x400

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;I)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "skip"    # I

    .prologue
    const/16 v3, 0x2f

    .line 51
    const/16 v2, 0x400

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;IC)V
    .locals 6
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "bufferSize"    # I
    .param p3, "delimiter"    # C

    .prologue
    .line 55
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Ljava/io/Reader;ICCI)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ICCI)V
    .locals 7
    .param p1, "input"    # Ljava/io/Reader;
    .param p2, "bufferSize"    # I
    .param p3, "delimiter"    # C
    .param p4, "replacement"    # C
    .param p5, "skip"    # I

    .prologue
    .line 71
    sget-object v1, Lorg/apache/lucene/util/AttributeSource$AttributeFactory;->DEFAULT_ATTRIBUTE_FACTORY:Lorg/apache/lucene/util/AttributeSource$AttributeFactory;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ICCI)V

    .line 72
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;CCI)V
    .locals 7
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "delimiter"    # C
    .param p4, "replacement"    # C
    .param p5, "skip"    # I

    .prologue
    .line 67
    const/16 v3, 0x400

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ICCI)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;ICCI)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;
    .param p3, "bufferSize"    # I
    .param p4, "delimiter"    # C
    .param p5, "replacement"    # C
    .param p6, "skip"    # I

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/Tokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V

    .line 99
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 100
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 101
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 102
    iput v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    .line 103
    iput v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    .line 104
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->endDelimiter:Z

    .line 107
    iput v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->charsRead:I

    .line 77
    if-gez p3, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bufferSize cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    if-gez p6, :cond_1

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "skip cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v0, p3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->resizeBuffer(I)[C

    .line 85
    iput-char p4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->delimiter:C

    .line 86
    iput-char p5, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->replacement:C

    .line 87
    iput p6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skip:I

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p3}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    .line 89
    return-void
.end method


# virtual methods
.method public final end()V
    .locals 2

    .prologue
    .line 196
    iget v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->charsRead:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->correctOffset(I)I

    move-result v0

    .line 197
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 198
    return-void
.end method

.method public final incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 112
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->clearAttributes()V

    .line 113
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/StringBuilder;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 114
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 115
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v4}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 120
    :goto_0
    const/4 v2, 0x0

    .line 121
    .local v2, "length":I
    const/4 v0, 0x0

    .line 122
    .local v0, "added":Z
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->endDelimiter:Z

    if-eqz v3, :cond_0

    .line 123
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-char v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->replacement:C

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 124
    add-int/lit8 v2, v2, 0x1

    .line 125
    iput-boolean v5, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->endDelimiter:Z

    .line 126
    const/4 v0, 0x1

    .line 130
    :cond_0
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v3}, Ljava/io/Reader;->read()I

    move-result v1

    .line 131
    .local v1, "c":I
    if-ltz v1, :cond_2

    .line 132
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->charsRead:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->charsRead:I

    .line 148
    if-nez v0, :cond_7

    .line 149
    const/4 v0, 0x1

    .line 150
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    .line 151
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    iget v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skip:I

    if-le v3, v6, :cond_6

    .line 152
    iget-object v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-char v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->delimiter:C

    if-ne v1, v3, :cond_5

    iget-char v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->replacement:C

    :goto_2
    invoke-interface {v6, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 153
    add-int/lit8 v2, v2, 0x1

    .line 154
    goto :goto_1

    .line 118
    .end local v0    # "added":Z
    .end local v1    # "c":I
    .end local v2    # "length":I
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v5}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_0

    .line 134
    .restart local v0    # "added":Z
    .restart local v1    # "c":I
    .restart local v2    # "length":I
    :cond_2
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    iget v4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skip:I

    if-le v3, v4, :cond_4

    .line 135
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/2addr v2, v3

    .line 136
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 137
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->correctOffset(I)I

    move-result v4

    iget v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    add-int/2addr v6, v2

    invoke-virtual {p0, v6}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->correctOffset(I)I

    move-result v6

    invoke-interface {v3, v4, v6}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 138
    if-eqz v0, :cond_3

    .line 139
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 140
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    invoke-virtual {v3, v4, v5, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 190
    .end local v0    # "added":Z
    :cond_3
    :goto_3
    return v0

    .restart local v0    # "added":Z
    :cond_4
    move v0, v5

    .line 145
    goto :goto_3

    .line 152
    :cond_5
    int-to-char v3, v1

    goto :goto_2

    .line 156
    :cond_6
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    goto :goto_1

    .line 160
    :cond_7
    iget-char v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->delimiter:C

    if-ne v1, v3, :cond_a

    .line 161
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    iget v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skip:I

    if-le v3, v6, :cond_8

    .line 162
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->endDelimiter:Z

    .line 185
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/2addr v2, v3

    .line 186
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v3, v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 187
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    invoke-virtual {p0, v6}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->correctOffset(I)I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    add-int/2addr v7, v2

    invoke-virtual {p0, v7}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->correctOffset(I)I

    move-result v7

    invoke-interface {v3, v6, v7}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 188
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 189
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    iget-object v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v6

    invoke-virtual {v3, v6, v5, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    move v0, v4

    .line 190
    goto :goto_3

    .line 165
    :cond_8
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    .line 166
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    iget v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skip:I

    if-le v3, v6, :cond_9

    .line 167
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-char v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->replacement:C

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 168
    add-int/lit8 v2, v2, 0x1

    .line 169
    goto/16 :goto_1

    .line 171
    :cond_9
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    goto/16 :goto_1

    .line 175
    :cond_a
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    iget v6, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skip:I

    if-le v3, v6, :cond_b

    .line 176
    iget-object v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    int-to-char v6, v1

    invoke-interface {v3, v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(C)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 177
    add-int/lit8 v2, v2, 0x1

    .line 178
    goto/16 :goto_1

    .line 180
    :cond_b
    iget v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    goto/16 :goto_1
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-super {p0}, Lorg/apache/lucene/analysis/Tokenizer;->reset()V

    .line 203
    iget-object v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->resultToken:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 204
    iput v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->charsRead:I

    .line 205
    iput-boolean v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->endDelimiter:Z

    .line 206
    iput v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->skipped:I

    .line 207
    iput v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;->startPosition:I

    .line 208
    return-void
.end method
