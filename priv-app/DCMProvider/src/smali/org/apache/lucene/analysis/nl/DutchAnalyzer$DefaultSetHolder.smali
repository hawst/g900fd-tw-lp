.class Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;
.super Ljava/lang/Object;
.source "DutchAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/nl/DutchAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultSetHolder"
.end annotation


# static fields
.field static final DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 91
    :try_start_0
    const-class v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    .line 92
    const-string v2, "dutch_stop.txt"

    sget-object v3, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    .line 91
    invoke-static {v1, v2, v3}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/lang/Class;Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v1

    .line 92
    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    .line 91
    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STOP_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArrayMap;

    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/util/CharArrayMap;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    sput-object v1, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    .line 100
    sget-object v1, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    const-string v2, "fiets"

    const-string v3, "fiets"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v1, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    const-string v2, "bromfiets"

    const-string v3, "bromfiets"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v1, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    const-string v2, "ei"

    const-string v3, "eier"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v1, Lorg/apache/lucene/analysis/nl/DutchAnalyzer$DefaultSetHolder;->DEFAULT_STEM_DICT:Lorg/apache/lucene/analysis/util/CharArrayMap;

    const-string v2, "kind"

    const-string v3, "kinder"

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 96
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to load default stopword set"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
