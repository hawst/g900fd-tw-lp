.class public final Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "EnglishPossessiveFilter.java"


# instance fields
.field private matchVersion:Lorg/apache/lucene/util/Version;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 47
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_35:Lorg/apache/lucene/util/Version;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "version"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 51
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 39
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 53
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    const/4 v2, 0x0

    .line 71
    :goto_0
    return v2

    .line 61
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 62
    .local v0, "buffer":[C
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v1

    .line 64
    .local v1, "bufferLength":I
    const/4 v2, 0x2

    if-lt v1, v2, :cond_3

    .line 65
    add-int/lit8 v2, v1, -0x2

    aget-char v2, v0, v2

    const/16 v3, 0x27

    if-eq v2, v3, :cond_1

    .line 66
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v3, Lorg/apache/lucene/util/Version;->LUCENE_36:Lorg/apache/lucene/util/Version;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v2

    if-eqz v2, :cond_3

    add-int/lit8 v2, v1, -0x2

    aget-char v2, v0, v2

    const/16 v3, 0x2019

    if-eq v2, v3, :cond_1

    add-int/lit8 v2, v1, -0x2

    aget-char v2, v0, v2

    const v3, 0xff07

    if-ne v2, v3, :cond_3

    .line 67
    :cond_1
    add-int/lit8 v2, v1, -0x1

    aget-char v2, v0, v2

    const/16 v3, 0x73

    if-eq v2, v3, :cond_2

    add-int/lit8 v2, v1, -0x1

    aget-char v2, v0, v2

    const/16 v3, 0x53

    if-ne v2, v3, :cond_3

    .line 68
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    add-int/lit8 v3, v1, -0x2

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 71
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method
