.class public final Lorg/apache/lucene/analysis/br/BrazilianStemFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "BrazilianStemFilter.java"


# instance fields
.field private exclusions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<*>;"
        }
    .end annotation
.end field

.field private final keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

.field private stemmer:Lorg/apache/lucene/analysis/br/BrazilianStemmer;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 44
    new-instance v0, Lorg/apache/lucene/analysis/br/BrazilianStemmer;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->stemmer:Lorg/apache/lucene/analysis/br/BrazilianStemmer;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->exclusions:Ljava/util/Set;

    .line 46
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 47
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    .line 56
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->toString()Ljava/lang/String;

    move-result-object v1

    .line 63
    .local v1, "term":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->keywordAttr:Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/KeywordAttribute;->isKeyword()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->exclusions:Ljava/util/Set;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->exclusions:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 64
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->stemmer:Lorg/apache/lucene/analysis/br/BrazilianStemmer;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/analysis/br/BrazilianStemmer;->stem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "s":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 67
    iget-object v2, p0, Lorg/apache/lucene/analysis/br/BrazilianStemFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setEmpty()Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    move-result-object v2

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 69
    .end local v0    # "s":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x1

    .line 71
    .end local v1    # "term":Ljava/lang/String;
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
