.class public Lorg/apache/lucene/analysis/util/WordlistLoader;
.super Ljava/lang/Object;
.source "WordlistLoader.java"


# static fields
.field private static final INITIAL_CAPACITY:I = 0x10


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;
    .locals 1
    .param p0, "reader"    # Ljava/io/Reader;

    .prologue
    .line 246
    instance-of v0, p0, Ljava/io/BufferedReader;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/BufferedReader;

    .end local p0    # "reader":Ljava/io/Reader;
    :goto_0
    return-object p0

    .line 247
    .restart local p0    # "reader":Ljava/io/Reader;
    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static getLines(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/util/List;
    .locals 8
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "charset"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/nio/charset/Charset;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 216
    const/4 v0, 0x0

    .line 218
    .local v0, "input":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 220
    .local v2, "success":Z
    :try_start_0
    invoke-static {p0, p1}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/io/InputStream;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 222
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v1, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "word":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_1

    .line 234
    const/4 v2, 0x1

    .line 237
    if-eqz v2, :cond_3

    new-array v4, v7, [Ljava/io/Closeable;

    .line 238
    aput-object v0, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 235
    :goto_1
    return-object v1

    .line 225
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const v5, 0xfeff

    if-ne v4, v5, :cond_2

    .line 226
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 228
    :cond_2
    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 229
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 231
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 232
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236
    .end local v1    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 237
    if-eqz v2, :cond_4

    new-array v5, v7, [Ljava/io/Closeable;

    .line 238
    aput-object v0, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 242
    :goto_2
    throw v4

    .line 239
    .restart local v1    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "word":Ljava/lang/String;
    :cond_3
    new-array v4, v7, [Ljava/io/Closeable;

    .line 240
    aput-object v0, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 239
    .end local v1    # "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "word":Ljava/lang/String;
    :cond_4
    new-array v5, v7, [Ljava/io/Closeable;

    .line 240
    aput-object v0, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2
.end method

.method public static getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 8
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "result"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 144
    const/4 v0, 0x0

    .line 146
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 147
    const/4 v3, 0x0

    .line 148
    .local v3, "line":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_1

    .line 155
    new-array v5, v6, [Ljava/io/Closeable;

    .line 156
    aput-object v0, v5, v7

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 158
    return-object p1

    .line 149
    :cond_1
    const/16 v5, 0x7c

    :try_start_1
    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 150
    .local v1, "comment":I
    if-ltz v1, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 151
    :cond_2
    const-string v5, "\\s+"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "words":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v4

    if-ge v2, v5, :cond_0

    .line 153
    aget-object v5, v4, v2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    aget-object v5, v4, v2

    invoke-virtual {p1, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;->add(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 155
    .end local v1    # "comment":I
    .end local v2    # "i":I
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "words":[Ljava/lang/String;
    :catchall_0
    move-exception v5

    new-array v6, v6, [Ljava/io/Closeable;

    .line 156
    aput-object v0, v6, v7

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 157
    throw v5
.end method

.method public static getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 3
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {p0, v0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getStemDict(Ljava/io/Reader;Lorg/apache/lucene/analysis/util/CharArrayMap;)Lorg/apache/lucene/analysis/util/CharArrayMap;
    .locals 7
    .param p0, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/Reader;",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/analysis/util/CharArrayMap",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "result":Lorg/apache/lucene/analysis/util/CharArrayMap;, "Lorg/apache/lucene/analysis/util/CharArrayMap<Ljava/lang/String;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 190
    const/4 v0, 0x0

    .line 192
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 194
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 198
    new-array v3, v6, [Ljava/io/Closeable;

    .line 199
    aput-object v0, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 201
    return-object p1

    .line 195
    :cond_0
    :try_start_1
    const-string v3, "\t"

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 196
    .local v2, "wordstem":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/analysis/util/CharArrayMap;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 198
    .end local v1    # "line":Ljava/lang/String;
    .end local v2    # "wordstem":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    new-array v4, v6, [Ljava/io/Closeable;

    .line 199
    aput-object v0, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 200
    throw v3
.end method

.method public static getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 5
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "comment"    # Ljava/lang/String;
    .param p2, "result"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 110
    const/4 v0, 0x0

    .line 112
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 113
    const/4 v1, 0x0

    .line 114
    .local v1, "word":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_1

    .line 120
    new-array v2, v3, [Ljava/io/Closeable;

    .line 121
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 123
    return-object p2

    .line 115
    :cond_1
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;->add(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120
    .end local v1    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v2

    new-array v3, v3, [Ljava/io/Closeable;

    .line 121
    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 122
    throw v2
.end method

.method public static getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 3
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "comment"    # Ljava/lang/String;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {v0, p2, v1, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getWordSet(Ljava/io/Reader;Ljava/lang/String;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method

.method public static getWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 5
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "result"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 55
    const/4 v0, 0x0

    .line 57
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {p0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getBufferedReader(Ljava/io/Reader;)Ljava/io/BufferedReader;

    move-result-object v0

    .line 58
    const/4 v1, 0x0

    .line 59
    .local v1, "word":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    .line 63
    new-array v2, v3, [Ljava/io/Closeable;

    .line 64
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 66
    return-object p1

    .line 60
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;->add(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    .end local v1    # "word":Ljava/lang/String;
    :catchall_0
    move-exception v2

    new-array v3, v3, [Ljava/io/Closeable;

    .line 64
    aput-object v0, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 65
    throw v2
.end method

.method public static getWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 3
    .param p0, "reader"    # Ljava/io/Reader;
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;IZ)V

    invoke-static {p0, v0}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getWordSet(Ljava/io/Reader;Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method
