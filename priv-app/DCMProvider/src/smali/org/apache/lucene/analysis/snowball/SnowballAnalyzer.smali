.class public final Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "SnowballAnalyzer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final matchVersion:Lorg/apache/lucene/util/Version;

.field private name:Ljava/lang/String;

.field private stopSet:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;)V
    .locals 0
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 55
    iput-object p2, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->name:Ljava/lang/String;

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "stopWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/lang/String;)V

    .line 62
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->stopSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 64
    return-void
.end method


# virtual methods
.method public createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 71
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 72
    .local v2, "tokenizer":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 74
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 75
    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->name:Ljava/lang/String;

    const-string v4, "English"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->name:Ljava/lang/String;

    const-string v4, "Porter"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->name:Ljava/lang/String;

    const-string v4, "Lovins"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 76
    :cond_0
    new-instance v1, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/en/EnglishPossessiveFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 78
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->name:Ljava/lang/String;

    const-string v4, "Turkish"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 79
    new-instance v1, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 82
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->stopSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v3, :cond_2

    .line 83
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 84
    iget-object v4, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->stopSet:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 83
    invoke-direct {v1, v3, v0, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 85
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_2
    new-instance v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->name:Ljava/lang/String;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Ljava/lang/String;)V

    .line 86
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v3

    .line 81
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_3
    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    goto :goto_0
.end method
