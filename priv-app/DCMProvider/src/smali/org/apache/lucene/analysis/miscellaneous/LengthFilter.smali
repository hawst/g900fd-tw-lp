.class public final Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;
.super Lorg/apache/lucene/analysis/util/FilteringTokenFilter;
.source "LengthFilter.java"


# instance fields
.field private final max:I

.field private final min:I

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;II)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "in"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "min"    # I
    .param p4, "max"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;-><init>(ZLorg/apache/lucene/analysis/TokenStream;)V

    .line 35
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 43
    iput p3, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->min:I

    .line 44
    iput p4, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->max:I

    .line 45
    return-void
.end method


# virtual methods
.method public accept()Z
    .locals 2

    .prologue
    .line 49
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v0

    .line 50
    .local v0, "len":I
    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->min:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/analysis/miscellaneous/LengthFilter;->max:I

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
