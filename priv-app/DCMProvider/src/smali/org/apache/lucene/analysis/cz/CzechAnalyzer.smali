.class public final Lorg/apache/lucene/analysis/cz/CzechAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "CzechAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/cz/CzechAnalyzer$DefaultSetHolder;
    }
.end annotation


# static fields
.field public static final DEFAULT_STOPWORD_FILE:Ljava/lang/String; = "stopwords.txt"


# instance fields
.field private final stemExclusionTable:Lorg/apache/lucene/analysis/util/CharArraySet;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 93
    # getter for: Lorg/apache/lucene/analysis/cz/CzechAnalyzer$DefaultSetHolder;->DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {}, Lorg/apache/lucene/analysis/cz/CzechAnalyzer$DefaultSetHolder;->access$0()Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 104
    sget-object v0, Lorg/apache/lucene/analysis/util/CharArraySet;->EMPTY_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Lorg/apache/lucene/analysis/util/CharArraySet;
    .param p3, "stemExclusionTable"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 118
    invoke-static {p1, p3}, Lorg/apache/lucene/analysis/util/CharArraySet;->copy(Lorg/apache/lucene/util/Version;Ljava/util/Set;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->stemExclusionTable:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 119
    return-void
.end method

.method public static final getDefaultStopSet()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 65
    # getter for: Lorg/apache/lucene/analysis/cz/CzechAnalyzer$DefaultSetHolder;->DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    invoke-static {}, Lorg/apache/lucene/analysis/cz/CzechAnalyzer$DefaultSetHolder;->access$0()Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 138
    new-instance v2, Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, p2}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 139
    .local v2, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v0, Lorg/apache/lucene/analysis/standard/StandardFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, v2}, Lorg/apache/lucene/analysis/standard/StandardFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 140
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v1, v3, v0}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 141
    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "result":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v3, v1, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 142
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    sget-object v4, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 143
    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->stemExclusionTable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/util/CharArraySet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 144
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/cz/CzechAnalyzer;->stemExclusionTable:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v0, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 145
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v1, Lorg/apache/lucene/analysis/cz/CzechStemFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/cz/CzechStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    move-object v0, v1

    .line 147
    .end local v1    # "result":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    new-instance v3, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v3, v2, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v3
.end method
