.class public Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "SnowballPorterFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# static fields
.field public static final PROTECTED_TOKENS:Ljava/lang/String; = "protected"


# instance fields
.field private final language:Ljava/lang/String;

.field private protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private stemClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lorg/tartarus/snowball/SnowballProgram;",
            ">;"
        }
    .end annotation
.end field

.field private final wordFiles:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 56
    const-string v0, "language"

    const-string v1, "English"

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->language:Ljava/lang/String;

    .line 57
    const-string v0, "protected"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->wordFiles:Ljava/lang/String;

    .line 58
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenFilter;
    .locals 6
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 77
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->stemClass:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/tartarus/snowball/SnowballProgram;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .local v2, "program":Lorg/tartarus/snowball/SnowballProgram;
    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v3, :cond_0

    .line 83
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, p1, v3}, Lorg/apache/lucene/analysis/miscellaneous/SetKeywordMarkerFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .end local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    .local v1, "input":Lorg/apache/lucene/analysis/TokenStream;
    move-object p1, v1

    .line 84
    .end local v1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    .restart local p1    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v3, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    invoke-direct {v3, p1, v2}, Lorg/apache/lucene/analysis/snowball/SnowballFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/tartarus/snowball/SnowballProgram;)V

    return-object v3

    .line 78
    .end local v2    # "program":Lorg/tartarus/snowball/SnowballProgram;
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error instantiating stemmer for language "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->language:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "from class "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->stemClass:Ljava/lang/Class;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenFilter;

    move-result-object v0

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 3
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "org.tartarus.snowball.ext."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->language:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Stemmer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "className":Ljava/lang/String;
    const-class v1, Lorg/tartarus/snowball/SnowballProgram;

    invoke-interface {p1, v0, v1}, Lorg/apache/lucene/analysis/util/ResourceLoader;->newInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/tartarus/snowball/SnowballProgram;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->stemClass:Ljava/lang/Class;

    .line 68
    iget-object v1, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->wordFiles:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->wordFiles:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->getWordSet(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;Z)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/snowball/SnowballPorterFilterFactory;->protectedWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 71
    :cond_0
    return-void
.end method
