.class public Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;
.super Lorg/apache/lucene/analysis/util/TokenizerFactory;
.source "PathHierarchyTokenizerFactory.java"


# instance fields
.field private final delimiter:C

.field private final replacement:C

.field private final reverse:Z

.field private final skip:I


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 79
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenizerFactory;-><init>(Ljava/util/Map;)V

    .line 80
    const-string v0, "delimiter"

    const/16 v1, 0x2f

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->getChar(Ljava/util/Map;Ljava/lang/String;C)C

    move-result v0

    iput-char v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->delimiter:C

    .line 81
    const-string v0, "replace"

    iget-char v1, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->delimiter:C

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->getChar(Ljava/util/Map;Ljava/lang/String;C)C

    move-result v0

    iput-char v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->replacement:C

    .line 82
    const-string v0, "reverse"

    invoke-virtual {p0, p1, v0, v2}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->reverse:Z

    .line 83
    const-string v0, "skip"

    invoke-virtual {p0, p1, v0, v2}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->getInt(Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->skip:I

    .line 84
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Tokenizer;
    .locals 6
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 91
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->reverse:Z

    if-eqz v0, :cond_0

    .line 92
    new-instance v0, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;

    iget-char v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->delimiter:C

    iget-char v4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->replacement:C

    iget v5, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->skip:I

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/ReversePathHierarchyTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;CCI)V

    .line 94
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;

    iget-char v3, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->delimiter:C

    iget-char v4, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->replacement:C

    iget v5, p0, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizerFactory;->skip:I

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/path/PathHierarchyTokenizer;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;CCI)V

    goto :goto_0
.end method
