.class public abstract Lorg/apache/lucene/analysis/util/FilteringTokenFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "FilteringTokenFilter.java"


# instance fields
.field private enablePositionIncrements:Z

.field private first:Z

.field private final posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;


# direct methods
.method public constructor <init>(ZLorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "enablePositionIncrements"    # Z
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 39
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 34
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->first:Z

    .line 40
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->enablePositionIncrements:Z

    .line 41
    return-void
.end method


# virtual methods
.method protected abstract accept()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getEnablePositionIncrements()Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->enablePositionIncrements:Z

    return v0
.end method

.method public final incrementToken()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 48
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->enablePositionIncrements:Z

    if-eqz v3, :cond_5

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "skippedPositions":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-nez v3, :cond_1

    .end local v0    # "skippedPositions":I
    :goto_1
    move v1, v2

    .line 74
    :cond_0
    :goto_2
    return v1

    .line 51
    .restart local v0    # "skippedPositions":I
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->accept()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 52
    if-eqz v0, :cond_0

    .line 53
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iget-object v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    add-int/2addr v3, v0

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    goto :goto_2

    .line 57
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0

    .line 61
    .end local v0    # "skippedPositions":I
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->accept()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 62
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->first:Z

    if-eqz v3, :cond_0

    .line 64
    iget-object v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v3

    if-nez v3, :cond_4

    .line 65
    iget-object v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->posIncrAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v3, v1}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 67
    :cond_4
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->first:Z

    goto :goto_2

    .line 60
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v3}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->first:Z

    .line 81
    return-void
.end method

.method public setEnablePositionIncrements(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/util/FilteringTokenFilter;->enablePositionIncrements:Z

    .line 108
    return-void
.end method
