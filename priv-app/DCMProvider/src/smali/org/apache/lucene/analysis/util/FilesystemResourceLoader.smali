.class public final Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;
.super Ljava/lang/Object;
.source "FilesystemResourceLoader.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoader;


# instance fields
.field private final baseDirectory:Ljava/io/File;

.field private final delegate:Lorg/apache/lucene/analysis/util/ResourceLoader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;-><init>(Ljava/io/File;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "baseDirectory"    # Ljava/io/File;

    .prologue
    .line 59
    new-instance v0, Lorg/apache/lucene/analysis/util/ClasspathResourceLoader;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/util/ClasspathResourceLoader;-><init>()V

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;-><init>(Ljava/io/File;Lorg/apache/lucene/analysis/util/ResourceLoader;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "baseDirectory"    # Ljava/io/File;
    .param p2, "delegate"    # Lorg/apache/lucene/analysis/util/ResourceLoader;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "baseDirectory is not a directory or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    if-nez p2, :cond_1

    .line 72
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "delegate ResourceLoader may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->baseDirectory:Ljava/io/File;

    .line 74
    iput-object p2, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->delegate:Lorg/apache/lucene/analysis/util/ResourceLoader;

    .line 75
    return-void
.end method


# virtual methods
.method public findClass(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;
    .locals 1
    .param p1, "cname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/lang/Class",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "expectedType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->delegate:Lorg/apache/lucene/analysis/util/ResourceLoader;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/analysis/util/ResourceLoader;->findClass(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public newInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "cname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "expectedType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->delegate:Lorg/apache/lucene/analysis/util/ResourceLoader;

    invoke-interface {v0, p1, p2}, Lorg/apache/lucene/analysis/util/ResourceLoader;->newInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public openResource(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3
    .param p1, "resource"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "file":Ljava/io/File;
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->baseDirectory:Ljava/io/File;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isAbsolute()Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->baseDirectory:Ljava/io/File;

    invoke-direct {v0, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 84
    .restart local v0    # "file":Ljava/io/File;
    :cond_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return-object v2

    .line 85
    :catch_0
    move-exception v1

    .line 86
    .local v1, "fnfe":Ljava/io/FileNotFoundException;
    iget-object v2, p0, Lorg/apache/lucene/analysis/util/FilesystemResourceLoader;->delegate:Lorg/apache/lucene/analysis/util/ResourceLoader;

    invoke-interface {v2, p1}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    goto :goto_0
.end method
