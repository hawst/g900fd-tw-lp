.class public final Lorg/apache/lucene/analysis/th/ThaiWordFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "ThaiWordFilter.java"


# static fields
.field public static final DBBI_AVAILABLE:Z

.field private static final proto:Ljava/text/BreakIterator;


# instance fields
.field private final breaker:Ljava/text/BreakIterator;

.field private final charIterator:Lorg/apache/lucene/analysis/util/CharArrayIterator;

.field private clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private clonedToken:Lorg/apache/lucene/util/AttributeSource;

.field private final handlePosIncr:Z

.field private hasIllegalOffsets:Z

.field private hasMoreTokensInClone:Z

.field private final offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/util/Locale;

    const-string v1, "th"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/text/BreakIterator;->getWordInstance(Ljava/util/Locale;)Ljava/text/BreakIterator;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->proto:Ljava/text/BreakIterator;

    .line 55
    sget-object v0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->proto:Ljava/text/BreakIterator;

    const-string/jumbo v1, "\u0e20\u0e32\u0e29\u0e32\u0e44\u0e17\u0e22"

    invoke-virtual {v0, v1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 56
    sget-object v0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->proto:Ljava/text/BreakIterator;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/text/BreakIterator;->isBoundary(I)Z

    move-result v0

    sput-boolean v0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->DBBI_AVAILABLE:Z

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 3
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    .end local p2    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :goto_0
    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 58
    sget-object v0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->proto:Ljava/text/BreakIterator;

    invoke-virtual {v0}, Ljava/text/BreakIterator;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/BreakIterator;

    iput-object v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->breaker:Ljava/text/BreakIterator;

    .line 59
    invoke-static {}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->newWordInstance()Lorg/apache/lucene/analysis/util/CharArrayIterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->charIterator:Lorg/apache/lucene/analysis/util/CharArrayIterator;

    .line 63
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 64
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 65
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 67
    iput-object v1, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    .line 68
    iput-object v1, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 69
    iput-object v1, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 70
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasMoreTokensInClone:Z

    .line 71
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasIllegalOffsets:Z

    .line 77
    sget-boolean v0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->DBBI_AVAILABLE:Z

    if-nez v0, :cond_1

    .line 78
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This JRE does not have support for Thai segmentation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    .restart local p2    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    move-object p2, v0

    goto :goto_0

    .line 79
    .end local p2    # "input":Lorg/apache/lucene/analysis/TokenStream;
    :cond_1
    sget-object v0, Lorg/apache/lucene/util/Version;->LUCENE_31:Lorg/apache/lucene/util/Version;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->handlePosIncr:Z

    .line 80
    return-void
.end method


# virtual methods
.method public incrementToken()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 84
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasMoreTokensInClone:Z

    if-eqz v2, :cond_3

    .line 85
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->breaker:Ljava/text/BreakIterator;

    invoke-virtual {v2}, Ljava/text/BreakIterator;->current()I

    move-result v1

    .line 86
    .local v1, "start":I
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->breaker:Ljava/text/BreakIterator;

    invoke-virtual {v2}, Ljava/text/BreakIterator;->next()I

    move-result v0

    .line 87
    .local v0, "end":I
    if-eq v0, v7, :cond_2

    .line 88
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {v2, p0}, Lorg/apache/lucene/util/AttributeSource;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    .line 89
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v4

    sub-int v5, v0, v1

    invoke-interface {v2, v4, v1, v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->copyBuffer([CII)V

    .line 90
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasIllegalOffsets:Z

    if-eqz v2, :cond_1

    .line 91
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v5

    invoke-interface {v2, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 95
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->handlePosIncr:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->posAtt:Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->setPositionIncrement(I)V

    .line 138
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_0
    :goto_1
    return v3

    .line 93
    .restart local v0    # "end":I
    .restart local v1    # "start":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v5

    add-int/2addr v5, v0

    invoke-interface {v2, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto :goto_0

    .line 98
    :cond_2
    iput-boolean v4, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasMoreTokensInClone:Z

    .line 101
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v2}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v2

    if-nez v2, :cond_4

    move v3, v4

    .line 102
    goto :goto_1

    .line 105
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    sget-object v5, Ljava/lang/Character$UnicodeBlock;->THAI:Ljava/lang/Character$UnicodeBlock;

    if-ne v2, v5, :cond_0

    .line 109
    iput-boolean v3, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasMoreTokensInClone:Z

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v2}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v2

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v5

    sub-int/2addr v2, v5

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v5

    if-eq v2, v5, :cond_5

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasIllegalOffsets:Z

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    if-nez v2, :cond_6

    .line 117
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->cloneAttributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    .line 118
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    const-class v5, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 119
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    const-class v5, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {v2, v5}, Lorg/apache/lucene/util/AttributeSource;->getAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 125
    :goto_3
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->charIterator:Lorg/apache/lucene/analysis/util/CharArrayIterator;

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v6

    invoke-virtual {v2, v5, v4, v6}, Lorg/apache/lucene/analysis/util/CharArrayIterator;->setText([CII)V

    .line 126
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->breaker:Ljava/text/BreakIterator;

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->charIterator:Lorg/apache/lucene/analysis/util/CharArrayIterator;

    invoke-virtual {v2, v5}, Ljava/text/BreakIterator;->setText(Ljava/text/CharacterIterator;)V

    .line 127
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->breaker:Ljava/text/BreakIterator;

    invoke-virtual {v2}, Ljava/text/BreakIterator;->next()I

    move-result v0

    .line 128
    .restart local v0    # "end":I
    if-eq v0, v7, :cond_8

    .line 129
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v2, v0}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 130
    iget-boolean v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasIllegalOffsets:Z

    if-eqz v2, :cond_7

    .line 131
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v5

    invoke-interface {v2, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto/16 :goto_1

    .end local v0    # "end":I
    :cond_5
    move v2, v4

    .line 113
    goto :goto_2

    .line 121
    :cond_6
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->copyTo(Lorg/apache/lucene/util/AttributeSource;)V

    goto :goto_3

    .line 133
    .restart local v0    # "end":I
    :cond_7
    iget-object v2, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->offsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v4, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v5

    add-int/2addr v5, v0

    invoke-interface {v2, v4, v5}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    goto/16 :goto_1

    :cond_8
    move v3, v4

    .line 138
    goto/16 :goto_1
.end method

.method public reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-super {p0}, Lorg/apache/lucene/analysis/TokenFilter;->reset()V

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->hasMoreTokensInClone:Z

    .line 145
    iput-object v1, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedToken:Lorg/apache/lucene/util/AttributeSource;

    .line 146
    iput-object v1, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedTermAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 147
    iput-object v1, p0, Lorg/apache/lucene/analysis/th/ThaiWordFilter;->clonedOffsetAtt:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 148
    return-void
.end method
