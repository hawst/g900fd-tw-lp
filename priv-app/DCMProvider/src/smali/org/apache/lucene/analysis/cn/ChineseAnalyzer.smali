.class public final Lorg/apache/lucene/analysis/cn/ChineseAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "ChineseAnalyzer.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    return-void
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 3
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 47
    new-instance v0, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;

    invoke-direct {v0, p2}, Lorg/apache/lucene/analysis/cn/ChineseTokenizer;-><init>(Ljava/io/Reader;)V

    .line 48
    .local v0, "source":Lorg/apache/lucene/analysis/Tokenizer;
    new-instance v1, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v2, Lorg/apache/lucene/analysis/cn/ChineseFilter;

    invoke-direct {v2, v0}, Lorg/apache/lucene/analysis/cn/ChineseFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    invoke-direct {v1, v0, v2}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-object v1
.end method
