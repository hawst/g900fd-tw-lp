.class public Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "HunspellStemFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# static fields
.field private static final PARAM_AFFIX:Ljava/lang/String; = "affix"

.field private static final PARAM_DICTIONARY:Ljava/lang/String; = "dictionary"

.field private static final PARAM_IGNORE_CASE:Ljava/lang/String; = "ignoreCase"

.field private static final PARAM_STRICT_AFFIX_PARSING:Ljava/lang/String; = "strictAffixParsing"


# instance fields
.field private final affixFile:Ljava/lang/String;

.field private dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

.field private final dictionaryArg:Ljava/lang/String;

.field private final ignoreCase:Z

.field private final strictAffixParsing:Z


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->assureMatchVersion()V

    .line 68
    const-string v0, "dictionary"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->dictionaryArg:Ljava/lang/String;

    .line 69
    const-string v0, "affix"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->affixFile:Ljava/lang/String;

    .line 70
    const-string v0, "ignoreCase"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->ignoreCase:Z

    .line 71
    const-string v0, "strictAffixParsing"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->getBoolean(Ljava/util/Map;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->strictAffixParsing:Z

    .line 72
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 2
    .param p1, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 114
    new-instance v0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;)V

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 12
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->dictionaryArg:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 86
    .local v7, "dictionaryFiles":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 87
    .local v1, "affix":Ljava/io/InputStream;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v6, "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 91
    .end local v6    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    .local v2, "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    :try_start_1
    array-length v3, v7

    move v0, v10

    :goto_0
    if-lt v0, v3, :cond_0

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->affixFile:Ljava/lang/String;

    invoke-interface {p1, v0}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 96
    new-instance v0, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;

    iget-object v3, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    iget-boolean v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->ignoreCase:Z

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->strictAffixParsing:Z

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;-><init>(Ljava/io/InputStream;Ljava/util/List;Lorg/apache/lucene/util/Version;ZZ)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->dictionary:Lorg/apache/lucene/analysis/hunspell/HunspellDictionary;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    new-array v0, v11, [Ljava/io/Closeable;

    .line 100
    aput-object v1, v0, v10

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 101
    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 103
    return-void

    .line 91
    :cond_0
    :try_start_2
    aget-object v9, v7, v0

    .line 92
    .local v9, "file":Ljava/lang/String;
    invoke-interface {p1, v9}, Lorg/apache/lucene/analysis/util/ResourceLoader;->openResource(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    .end local v2    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    .end local v9    # "file":Ljava/lang/String;
    .restart local v6    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    :catch_0
    move-exception v8

    move-object v2, v6

    .line 98
    .end local v6    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    .restart local v2    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    .local v8, "e":Ljava/text/ParseException;
    :goto_1
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to load hunspell data! [dictionary="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->dictionaryArg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",affix="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/analysis/hunspell/HunspellStemFilterFactory;->affixFile:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 99
    .end local v8    # "e":Ljava/text/ParseException;
    :catchall_0
    move-exception v0

    :goto_2
    new-array v3, v11, [Ljava/io/Closeable;

    .line 100
    aput-object v1, v3, v10

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 101
    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 102
    throw v0

    .line 99
    .end local v2    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    .restart local v6    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    :catchall_1
    move-exception v0

    move-object v2, v6

    .end local v6    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    .restart local v2    # "dictionaries":Ljava/util/List;, "Ljava/util/List<Ljava/io/InputStream;>;"
    goto :goto_2

    .line 97
    :catch_1
    move-exception v8

    goto :goto_1
.end method
