.class Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;
.super Ljava/lang/Object;
.source "GermanAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/de/GermanAnalyzer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultSetHolder"
.end annotation


# static fields
.field private static final DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private static final DEFAULT_SET_30:Lorg/apache/lucene/analysis/util/CharArraySet;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 99
    new-instance v1, Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 100
    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    # getter for: Lorg/apache/lucene/analysis/de/GermanAnalyzer;->GERMAN_STOP_WORDS:[Ljava/lang/String;
    invoke-static {}, Lorg/apache/lucene/analysis/de/GermanAnalyzer;->access$0()[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    .line 99
    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    invoke-static {v1}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET_30:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 104
    :try_start_0
    const-class v1, Lorg/apache/lucene/analysis/snowball/SnowballFilter;

    .line 105
    const-string v2, "german_stop.txt"

    sget-object v3, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    .line 104
    invoke-static {v1, v2, v3}, Lorg/apache/lucene/util/IOUtils;->getDecodingReader(Ljava/lang/Class;Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/io/Reader;

    move-result-object v1

    .line 105
    sget-object v2, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    .line 104
    invoke-static {v1, v2}, Lorg/apache/lucene/analysis/util/WordlistLoader;->getSnowballWordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v1

    sput-object v1, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 109
    .local v0, "ex":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to load default stopword set"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method

.method static synthetic access$1()Lorg/apache/lucene/analysis/util/CharArraySet;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lorg/apache/lucene/analysis/de/GermanAnalyzer$DefaultSetHolder;->DEFAULT_SET_30:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-object v0
.end method
