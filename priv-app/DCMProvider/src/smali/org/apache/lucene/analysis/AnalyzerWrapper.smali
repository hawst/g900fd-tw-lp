.class public abstract Lorg/apache/lucene/analysis/AnalyzerWrapper;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "AnalyzerWrapper.java"


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/Analyzer$PerFieldReuseStrategy;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/Analyzer;-><init>(Lorg/apache/lucene/analysis/Analyzer$ReuseStrategy;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected final createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "aReader"    # Ljava/io/Reader;

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/AnalyzerWrapper;->getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/AnalyzerWrapper;->wrapComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    move-result-object v0

    return-object v0
.end method

.method public final getOffsetGap(Ljava/lang/String;)I
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/AnalyzerWrapper;->getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Analyzer;->getOffsetGap(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getPositionIncrementGap(Ljava/lang/String;)I
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/AnalyzerWrapper;->getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/Analyzer;->getPositionIncrementGap(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected abstract getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;
.end method

.method public final initReader(Ljava/lang/String;Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/AnalyzerWrapper;->getWrappedAnalyzer(Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/analysis/Analyzer;->initReader(Ljava/lang/String;Ljava/io/Reader;)Ljava/io/Reader;

    move-result-object v0

    return-object v0
.end method

.method protected abstract wrapComponents(Ljava/lang/String;Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.end method
