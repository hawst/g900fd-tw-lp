.class public Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;
.super Ljava/lang/Object;
.source "SynonymMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/analysis/synonym/SynonymMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final dedup:Z

.field private maxHorizontalContext:I

.field private final utf8Scratch:Lorg/apache/lucene/util/BytesRef;

.field private final words:Lorg/apache/lucene/util/BytesRefHash;

.field private final workingSet:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lorg/apache/lucene/util/CharsRef;",
            "Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lorg/apache/lucene/analysis/synonym/SynonymMap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "dedup"    # Z

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->workingSet:Ljava/util/HashMap;

    .line 70
    new-instance v0, Lorg/apache/lucene/util/BytesRefHash;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRefHash;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->words:Lorg/apache/lucene/util/BytesRefHash;

    .line 71
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->utf8Scratch:Lorg/apache/lucene/util/BytesRef;

    .line 78
    iput-boolean p1, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->dedup:Z

    .line 79
    return-void
.end method

.method private add(Lorg/apache/lucene/util/CharsRef;ILorg/apache/lucene/util/CharsRef;IZ)V
    .locals 7
    .param p1, "input"    # Lorg/apache/lucene/util/CharsRef;
    .param p2, "numInputWords"    # I
    .param p3, "output"    # Lorg/apache/lucene/util/CharsRef;
    .param p4, "numOutputWords"    # I
    .param p5, "includeOrig"    # Z

    .prologue
    .line 170
    if-gtz p2, :cond_0

    .line 171
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "numInputWords must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 173
    :cond_0
    iget v3, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    if-gtz v3, :cond_1

    .line 174
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "input.length must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 176
    :cond_1
    if-gtz p4, :cond_2

    .line 177
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "numOutputWords must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 179
    :cond_2
    iget v3, p3, Lorg/apache/lucene/util/CharsRef;->length:I

    if-gtz v3, :cond_3

    .line 180
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "output.length must be > 0 (got "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p3, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 183
    :cond_3
    sget-boolean v3, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->hasHoles(Lorg/apache/lucene/util/CharsRef;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "input has holes: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 184
    :cond_4
    sget-boolean v3, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    invoke-direct {p0, p3}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->hasHoles(Lorg/apache/lucene/util/CharsRef;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "output has holes: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 187
    :cond_5
    iget-object v3, p3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v4, p3, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v5, p3, Lorg/apache/lucene/util/CharsRef;->length:I

    iget-object v6, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->utf8Scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v3, v4, v5, v6}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8WithHash([CIILorg/apache/lucene/util/BytesRef;)I

    move-result v1

    .line 189
    .local v1, "hashCode":I
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->words:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v4, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->utf8Scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4, v1}, Lorg/apache/lucene/util/BytesRefHash;->add(Lorg/apache/lucene/util/BytesRef;I)I

    move-result v2

    .line 190
    .local v2, "ord":I
    if-gez v2, :cond_6

    .line 192
    neg-int v3, v2

    add-int/lit8 v2, v3, -0x1

    .line 198
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->workingSet:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;

    .line 199
    .local v0, "e":Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;
    if-nez v0, :cond_7

    .line 200
    new-instance v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;

    .end local v0    # "e":Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;
    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;-><init>(Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;)V

    .line 201
    .restart local v0    # "e":Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;
    iget-object v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->workingSet:Ljava/util/HashMap;

    invoke-static {p1}, Lorg/apache/lucene/util/CharsRef;->deepCopyOf(Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_7
    iget-object v3, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->ords:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    iget-boolean v3, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->includeOrig:Z

    or-int/2addr v3, p5

    iput-boolean v3, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->includeOrig:Z

    .line 206
    iget v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->maxHorizontalContext:I

    invoke-static {v3, p2}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->maxHorizontalContext:I

    .line 207
    iget v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->maxHorizontalContext:I

    invoke-static {v3, p4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->maxHorizontalContext:I

    .line 208
    return-void
.end method

.method public static analyze(Lorg/apache/lucene/analysis/Analyzer;Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;
    .locals 9
    .param p0, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "reuse"    # Lorg/apache/lucene/util/CharsRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 115
    const-string v6, ""

    new-instance v7, Ljava/io/StringReader;

    invoke-direct {v7, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6, v7}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v5

    .line 116
    .local v5, "ts":Lorg/apache/lucene/analysis/TokenStream;
    const-class v6, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 117
    .local v4, "termAtt":Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;
    const-class v6, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/analysis/TokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 118
    .local v3, "posIncAtt":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 119
    iput v8, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 120
    :goto_0
    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v6

    if-nez v6, :cond_0

    .line 137
    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 138
    invoke-virtual {v5}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    .line 139
    iget v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    if-nez v6, :cond_4

    .line 140
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "term: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " was completely eliminated by analyzer"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 121
    :cond_0
    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v2

    .line 122
    .local v2, "length":I
    if-nez v2, :cond_1

    .line 123
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "term: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " analyzed to a zero-length token"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 125
    :cond_1
    invoke-interface {v3}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    .line 126
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "term: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " analyzed to a token with posinc != 1"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 128
    :cond_2
    iget v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v6, v2

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p2, v6}, Lorg/apache/lucene/util/CharsRef;->grow(I)V

    .line 129
    iget v6, p2, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v7, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v6, v7

    .line 130
    .local v0, "end":I
    iget v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    if-lez v6, :cond_3

    .line 131
    iget-object v6, p2, Lorg/apache/lucene/util/CharsRef;->chars:[C

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "end":I
    .local v1, "end":I
    aput-char v8, v6, v0

    .line 132
    iget v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    move v0, v1

    .line 134
    .end local v1    # "end":I
    .restart local v0    # "end":I
    :cond_3
    invoke-interface {v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v6

    iget-object v7, p2, Lorg/apache/lucene/util/CharsRef;->chars:[C

    invoke-static {v6, v8, v7, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    iget v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v6, v2

    iput v6, p2, Lorg/apache/lucene/util/CharsRef;->length:I

    goto/16 :goto_0

    .line 142
    .end local v0    # "end":I
    .end local v2    # "length":I
    :cond_4
    return-object p2
.end method

.method private countWords(Lorg/apache/lucene/util/CharsRef;)I
    .locals 6
    .param p1, "chars"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 211
    const/4 v3, 0x1

    .line 212
    .local v3, "wordCount":I
    iget v1, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    .line 213
    .local v1, "upto":I
    iget v4, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v4, v5

    .local v0, "limit":I
    move v2, v1

    .line 214
    .end local v1    # "upto":I
    .local v2, "upto":I
    :goto_0
    if-lt v2, v0, :cond_0

    .line 219
    return v3

    .line 215
    :cond_0
    iget-object v4, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "upto":I
    .restart local v1    # "upto":I
    aget-char v4, v4, v2

    if-nez v4, :cond_1

    .line 216
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "upto":I
    .restart local v2    # "upto":I
    goto :goto_0

    .end local v2    # "upto":I
    .restart local v1    # "upto":I
    :cond_1
    move v2, v1

    .end local v1    # "upto":I
    .restart local v2    # "upto":I
    goto :goto_0
.end method

.method private hasHoles(Lorg/apache/lucene/util/CharsRef;)Z
    .locals 6
    .param p1, "chars"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    const/4 v2, 0x1

    .line 147
    iget v3, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v4, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int v0, v3, v4

    .line 148
    .local v0, "end":I
    iget v3, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    add-int/lit8 v1, v3, 0x1

    .local v1, "idx":I
    :goto_0
    if-lt v1, v0, :cond_1

    .line 153
    iget-object v3, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v4, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    aget-char v3, v3, v4

    if-nez v3, :cond_3

    .line 160
    :cond_0
    :goto_1
    return v2

    .line 149
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    aget-char v3, v3, v1

    if-nez v3, :cond_2

    iget-object v3, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    add-int/lit8 v4, v1, -0x1

    aget-char v3, v3, v4

    if-eqz v3, :cond_0

    .line 148
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    :cond_3
    iget-object v3, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget v4, p1, Lorg/apache/lucene/util/CharsRef;->offset:I

    iget v5, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    aget-char v3, v3, v4

    if-eqz v3, :cond_0

    .line 160
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static join([Ljava/lang/String;Lorg/apache/lucene/util/CharsRef;)Lorg/apache/lucene/util/CharsRef;
    .locals 10
    .param p0, "words"    # [Ljava/lang/String;
    .param p1, "reuse"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    const/4 v7, 0x0

    .line 91
    const/4 v2, 0x0

    .line 92
    .local v2, "upto":I
    iget-object v0, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 93
    .local v0, "buffer":[C
    array-length v8, p0

    move v6, v7

    move v3, v2

    .end local v2    # "upto":I
    .local v3, "upto":I
    :goto_0
    if-lt v6, v8, :cond_0

    .line 107
    iput v3, p1, Lorg/apache/lucene/util/CharsRef;->length:I

    .line 108
    return-object p1

    .line 93
    :cond_0
    aget-object v4, p0, v6

    .line 94
    .local v4, "word":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 95
    .local v5, "wordLen":I
    if-nez v3, :cond_2

    move v1, v5

    .line 96
    .local v1, "needed":I
    :goto_1
    array-length v9, v0

    if-le v1, v9, :cond_1

    .line 97
    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/CharsRef;->grow(I)V

    .line 98
    iget-object v0, p1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    .line 100
    :cond_1
    if-lez v3, :cond_3

    .line 101
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "upto":I
    .restart local v2    # "upto":I
    aput-char v7, v0, v3

    .line 104
    :goto_2
    invoke-virtual {v4, v7, v5, v0, v2}, Ljava/lang/String;->getChars(II[CI)V

    .line 105
    add-int/2addr v2, v5

    .line 93
    add-int/lit8 v6, v6, 0x1

    move v3, v2

    .end local v2    # "upto":I
    .restart local v3    # "upto":I
    goto :goto_0

    .line 95
    .end local v1    # "needed":I
    :cond_2
    add-int/lit8 v9, v3, 0x1

    add-int v1, v9, v5

    goto :goto_1

    .restart local v1    # "needed":I
    :cond_3
    move v2, v3

    .end local v3    # "upto":I
    .restart local v2    # "upto":I
    goto :goto_2
.end method


# virtual methods
.method public add(Lorg/apache/lucene/util/CharsRef;Lorg/apache/lucene/util/CharsRef;Z)V
    .locals 6
    .param p1, "input"    # Lorg/apache/lucene/util/CharsRef;
    .param p2, "output"    # Lorg/apache/lucene/util/CharsRef;
    .param p3, "includeOrig"    # Z

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->countWords(Lorg/apache/lucene/util/CharsRef;)I

    move-result v2

    invoke-direct {p0, p2}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->countWords(Lorg/apache/lucene/util/CharsRef;)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->add(Lorg/apache/lucene/util/CharsRef;ILorg/apache/lucene/util/CharsRef;IZ)V

    .line 235
    return-void
.end method

.method public build()Lorg/apache/lucene/analysis/synonym/SynonymMap;
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-static {}, Lorg/apache/lucene/util/fst/ByteSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/ByteSequenceOutputs;

    move-result-object v17

    .line 244
    .local v17, "outputs":Lorg/apache/lucene/util/fst/ByteSequenceOutputs;
    new-instance v5, Lorg/apache/lucene/util/fst/Builder;

    sget-object v26, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE4:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-direct {v5, v0, v1}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 246
    .local v5, "builder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v20, Lorg/apache/lucene/util/BytesRef;

    const/16 v26, 0x40

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 247
    .local v20, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v22, Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-direct/range {v22 .. v22}, Lorg/apache/lucene/store/ByteArrayDataOutput;-><init>()V

    .line 251
    .local v22, "scratchOutput":Lorg/apache/lucene/store/ByteArrayDataOutput;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->dedup:Z

    move/from16 v26, v0

    if-eqz v26, :cond_0

    .line 252
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 257
    .local v7, "dedupSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_0
    const/16 v26, 0x5

    move/from16 v0, v26

    new-array v0, v0, [B

    move-object/from16 v24, v0

    .line 259
    .local v24, "spare":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->workingSet:Ljava/util/HashMap;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 260
    .local v14, "keys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/CharsRef;>;"
    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v26

    move/from16 v0, v26

    new-array v0, v0, [Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-interface {v14, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v23

    check-cast v23, [Lorg/apache/lucene/util/CharsRef;

    .line 261
    .local v23, "sortedKeys":[Lorg/apache/lucene/util/CharsRef;
    invoke-static {}, Lorg/apache/lucene/util/CharsRef;->getUTF16SortedAsUTF8Comparator()Ljava/util/Comparator;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 263
    new-instance v21, Lorg/apache/lucene/util/IntsRef;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 266
    .local v21, "scratchIntsRef":Lorg/apache/lucene/util/IntsRef;
    const/4 v13, 0x0

    .local v13, "keyIdx":I
    :goto_1
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    if-lt v13, v0, :cond_1

    .line 312
    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    move-result-object v10

    .line 313
    .local v10, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v26, Lorg/apache/lucene/analysis/synonym/SynonymMap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->words:Lorg/apache/lucene/util/BytesRefHash;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->maxHorizontalContext:I

    move/from16 v28, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v10, v1, v2}, Lorg/apache/lucene/analysis/synonym/SynonymMap;-><init>(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/BytesRefHash;I)V

    return-object v26

    .line 254
    .end local v7    # "dedupSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v10    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v13    # "keyIdx":I
    .end local v14    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/CharsRef;>;"
    .end local v21    # "scratchIntsRef":Lorg/apache/lucene/util/IntsRef;
    .end local v23    # "sortedKeys":[Lorg/apache/lucene/util/CharsRef;
    .end local v24    # "spare":[B
    :cond_0
    const/4 v7, 0x0

    .restart local v7    # "dedupSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    goto :goto_0

    .line 267
    .restart local v13    # "keyIdx":I
    .restart local v14    # "keys":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/util/CharsRef;>;"
    .restart local v21    # "scratchIntsRef":Lorg/apache/lucene/util/IntsRef;
    .restart local v23    # "sortedKeys":[Lorg/apache/lucene/util/CharsRef;
    .restart local v24    # "spare":[B
    :cond_1
    aget-object v12, v23, v13

    .line 268
    .local v12, "input":Lorg/apache/lucene/util/CharsRef;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->workingSet:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;

    .line 270
    .local v16, "output":Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->ords:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 272
    .local v15, "numEntries":I
    mul-int/lit8 v26, v15, 0x5

    add-int/lit8 v9, v26, 0x5

    .line 274
    .local v9, "estimatedSize":I
    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 275
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v26, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v27, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([BII)V

    .line 276
    sget-boolean v26, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder;->$assertionsDisabled:Z

    if-nez v26, :cond_2

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v26, v0

    if-eqz v26, :cond_2

    new-instance v26, Ljava/lang/AssertionError;

    invoke-direct/range {v26 .. v26}, Ljava/lang/AssertionError;-><init>()V

    throw v26

    .line 279
    :cond_2
    const/4 v6, 0x0

    .line 280
    .local v6, "count":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-lt v11, v15, :cond_4

    .line 293
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v18

    .line 294
    .local v18, "pos":I
    shl-int/lit8 v27, v6, 0x1

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->includeOrig:Z

    move/from16 v26, v0

    if-eqz v26, :cond_7

    const/16 v26, 0x0

    :goto_3
    or-int v26, v26, v27

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeVInt(I)V

    .line 295
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v19

    .line 296
    .local v19, "pos2":I
    sub-int v25, v19, v18

    .line 299
    .local v25, "vIntLen":I
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v18

    move-object/from16 v2, v24

    move/from16 v3, v27

    move/from16 v4, v25

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 300
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v28, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v28

    move/from16 v3, v25

    move/from16 v4, v18

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 301
    const/16 v26, 0x0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v27, v0

    const/16 v28, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    move-object/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v25

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 303
    if-eqz v7, :cond_3

    .line 304
    invoke-interface {v7}, Ljava/util/Set;->clear()V

    .line 307
    :cond_3
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v26

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v27, v0

    sub-int v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v20

    iput v0, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 309
    move-object/from16 v0, v21

    invoke-static {v12, v0}, Lorg/apache/lucene/util/fst/Util;->toUTF32(Ljava/lang/CharSequence;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v26

    invoke-static/range {v20 .. v20}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 266
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 281
    .end local v18    # "pos":I
    .end local v19    # "pos2":I
    .end local v25    # "vIntLen":I
    :cond_4
    if-eqz v7, :cond_6

    .line 283
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->ords:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 284
    .local v8, "ent":Ljava/lang/Integer;
    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 280
    .end local v8    # "ent":Ljava/lang/Integer;
    :goto_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 287
    .restart local v8    # "ent":Ljava/lang/Integer;
    :cond_5
    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    .end local v8    # "ent":Ljava/lang/Integer;
    :cond_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/analysis/synonym/SynonymMap$Builder$MapEntry;->ords:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeVInt(I)V

    .line 290
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 294
    .restart local v18    # "pos":I
    :cond_7
    const/16 v26, 0x1

    goto/16 :goto_3
.end method
