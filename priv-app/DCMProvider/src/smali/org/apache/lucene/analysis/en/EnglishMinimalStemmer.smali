.class public Lorg/apache/lucene/analysis/en/EnglishMinimalStemmer;
.super Ljava/lang/Object;
.source "EnglishMinimalStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 6
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v5, 0x69

    const/16 v4, 0x65

    const/16 v3, 0x61

    const/4 v2, 0x3

    .line 30
    if-lt p2, v2, :cond_0

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-eq v0, v1, :cond_1

    .line 43
    .end local p2    # "len":I
    :cond_0
    :goto_0
    :sswitch_0
    return p2

    .line 33
    .restart local p2    # "len":I
    :cond_1
    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    .line 43
    :cond_2
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 37
    :sswitch_1
    if-le p2, v2, :cond_3

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    if-ne v0, v5, :cond_3

    add-int/lit8 v0, p2, -0x4

    aget-char v0, p1, v0

    if-eq v0, v3, :cond_3

    add-int/lit8 v0, p2, -0x4

    aget-char v0, p1, v0

    if-eq v0, v4, :cond_3

    .line 38
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x79

    aput-char v1, p1, v0

    .line 39
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 41
    :cond_3
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    if-eq v0, v5, :cond_0

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    if-eq v0, v3, :cond_0

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_0

    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    if-ne v0, v4, :cond_2

    goto :goto_0

    .line 33
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x73 -> :sswitch_0
        0x75 -> :sswitch_0
    .end sparse-switch
.end method
