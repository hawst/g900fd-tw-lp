.class public Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "DelimitedPayloadTokenFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# static fields
.field public static final DELIMITER_ATTR:Ljava/lang/String; = "delimiter"

.field public static final ENCODER_ATTR:Ljava/lang/String; = "encoder"


# instance fields
.field private final delimiter:C

.field private encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

.field private final encoderClass:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 49
    const-string v0, "encoder"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoderClass:Ljava/lang/String;

    .line 50
    const-string v0, "delimiter"

    const/16 v1, 0x7c

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->getChar(Ljava/util/Map;Ljava/lang/String;C)C

    move-result v0

    iput-char v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->delimiter:C

    .line 51
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;

    iget-char v1, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->delimiter:C

    iget-object v2, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;CLorg/apache/lucene/analysis/payloads/PayloadEncoder;)V

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 2
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoderClass:Ljava/lang/String;

    const-string v1, "float"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Lorg/apache/lucene/analysis/payloads/FloatEncoder;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/payloads/FloatEncoder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    .line 72
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoderClass:Ljava/lang/String;

    const-string v1, "integer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Lorg/apache/lucene/analysis/payloads/IntegerEncoder;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/payloads/IntegerEncoder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    goto :goto_0

    .line 67
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoderClass:Ljava/lang/String;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    new-instance v0, Lorg/apache/lucene/analysis/payloads/IdentityEncoder;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/payloads/IdentityEncoder;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    goto :goto_0

    .line 70
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoderClass:Ljava/lang/String;

    const-class v1, Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    invoke-interface {p1, v0, v1}, Lorg/apache/lucene/analysis/util/ResourceLoader;->newInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/DelimitedPayloadTokenFilterFactory;->encoder:Lorg/apache/lucene/analysis/payloads/PayloadEncoder;

    goto :goto_0
.end method
