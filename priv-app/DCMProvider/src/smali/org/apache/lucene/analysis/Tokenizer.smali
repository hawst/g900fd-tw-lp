.class public abstract Lorg/apache/lucene/analysis/Tokenizer;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "Tokenizer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected input:Ljava/io/Reader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/analysis/Tokenizer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/analysis/Tokenizer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 39
    sget-boolean v0, Lorg/apache/lucene/analysis/Tokenizer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 41
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;Ljava/io/Reader;)V
    .locals 2
    .param p1, "factory"    # Lorg/apache/lucene/util/AttributeSource$AttributeFactory;
    .param p2, "input"    # Ljava/io/Reader;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenStream;-><init>(Lorg/apache/lucene/util/AttributeSource$AttributeFactory;)V

    .line 46
    sget-boolean v0, Lorg/apache/lucene/analysis/Tokenizer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 47
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 48
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 65
    :cond_0
    return-void
.end method

.method protected final correctOffset(I)I
    .locals 2
    .param p1, "currentOff"    # I

    .prologue
    .line 74
    sget-boolean v0, Lorg/apache/lucene/analysis/Tokenizer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "this tokenizer is closed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 75
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    instance-of v0, v0, Lorg/apache/lucene/analysis/CharFilter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    check-cast v0, Lorg/apache/lucene/analysis/CharFilter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/CharFilter;->correctOffset(I)I

    move-result p1

    .end local p1    # "currentOff":I
    :cond_1
    return p1
.end method

.method public final setReader(Ljava/io/Reader;)V
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget-boolean v0, Lorg/apache/lucene/analysis/Tokenizer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 83
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/analysis/Tokenizer;->input:Ljava/io/Reader;

    .line 84
    sget-boolean v0, Lorg/apache/lucene/analysis/Tokenizer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/Tokenizer;->setReaderTestPoint()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_1
    return-void
.end method

.method setReaderTestPoint()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method
