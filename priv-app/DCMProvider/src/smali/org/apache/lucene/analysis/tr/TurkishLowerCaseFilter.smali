.class public final Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;
.super Lorg/apache/lucene/analysis/TokenFilter;
.source "TurkishLowerCaseFilter.java"


# static fields
.field private static final COMBINING_DOT_ABOVE:I = 0x307

.field private static final LATIN_CAPITAL_LETTER_I:I = 0x49

.field private static final LATIN_SMALL_LETTER_DOTLESS_I:I = 0x131

.field private static final LATIN_SMALL_LETTER_I:I = 0x69


# instance fields
.field private final termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/TokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 40
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 50
    return-void
.end method

.method private delete([CII)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "pos"    # I
    .param p3, "len"    # I

    .prologue
    .line 119
    if-ge p2, p3, :cond_0

    .line 120
    add-int/lit8 v0, p2, 0x1

    sub-int v1, p3, p2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v0, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    :cond_0
    add-int/lit8 v0, p3, -0x1

    return v0
.end method

.method private isBeforeDot([CII)Z
    .locals 5
    .param p1, "s"    # [C
    .param p2, "pos"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v2, 0x0

    .line 102
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-lt v1, p3, :cond_1

    .line 111
    :cond_0
    :goto_1
    return v2

    .line 103
    :cond_1
    invoke-static {p1, v1}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v0

    .line 104
    .local v0, "ch":I
    invoke-static {v0}, Ljava/lang/Character;->getType(I)I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    .line 106
    const/16 v3, 0x307

    if-ne v0, v3, :cond_2

    .line 107
    const/4 v2, 0x1

    goto :goto_1

    .line 108
    :cond_2
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v1, v3

    goto :goto_0
.end method


# virtual methods
.method public final incrementToken()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 54
    const/4 v3, 0x0

    .line 56
    .local v3, "iOrAfter":Z
    iget-object v7, p0, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->input:Lorg/apache/lucene/analysis/TokenStream;

    invoke-virtual {v7}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 57
    iget-object v7, p0, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->buffer()[C

    move-result-object v0

    .line 58
    .local v0, "buffer":[C
    iget-object v7, p0, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v7}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->length()I

    move-result v4

    .line 59
    .local v4, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 90
    iget-object v6, p0, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->termAtt:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-interface {v6, v4}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->setLength(I)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 93
    .end local v0    # "buffer":[C
    .end local v2    # "i":I
    .end local v4    # "length":I
    :goto_1
    return v5

    .line 60
    .restart local v0    # "buffer":[C
    .restart local v2    # "i":I
    .restart local v4    # "length":I
    :cond_0
    invoke-static {v0, v2}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v1

    .line 62
    .local v1, "ch":I
    const/16 v7, 0x49

    if-eq v1, v7, :cond_3

    .line 63
    if-eqz v3, :cond_1

    invoke-static {v1}, Ljava/lang/Character;->getType(I)I

    move-result v7

    const/4 v8, 0x6

    if-eq v7, v8, :cond_3

    :cond_1
    move v3, v6

    .line 65
    :goto_2
    if-eqz v3, :cond_2

    .line 66
    sparse-switch v1, :sswitch_data_0

    .line 87
    :cond_2
    invoke-static {v1}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v7

    invoke-static {v7, v0, v2}, Ljava/lang/Character;->toChars(I[CI)I

    move-result v7

    add-int/2addr v2, v7

    goto :goto_0

    :cond_3
    move v3, v5

    .line 62
    goto :goto_2

    .line 69
    :sswitch_0
    invoke-direct {p0, v0, v2, v4}, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->delete([CII)I

    move-result v4

    .line 70
    goto :goto_0

    .line 74
    :sswitch_1
    add-int/lit8 v7, v2, 0x1

    invoke-direct {p0, v0, v7, v4}, Lorg/apache/lucene/analysis/tr/TurkishLowerCaseFilter;->isBeforeDot([CII)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 75
    const/16 v7, 0x69

    aput-char v7, v0, v2

    .line 82
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 83
    goto :goto_0

    .line 77
    :cond_4
    const/16 v7, 0x131

    aput-char v7, v0, v2

    .line 80
    const/4 v3, 0x0

    goto :goto_3

    .end local v0    # "buffer":[C
    .end local v1    # "ch":I
    .end local v2    # "i":I
    .end local v4    # "length":I
    :cond_5
    move v5, v6

    .line 93
    goto :goto_1

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        0x49 -> :sswitch_1
        0x307 -> :sswitch_0
    .end sparse-switch
.end method
