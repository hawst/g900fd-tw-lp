.class public final Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;
.super Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;
.source "ClassicAnalyzer.java"


# static fields
.field public static final DEFAULT_MAX_TOKEN_LENGTH:I = 0xff

.field public static final STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;


# instance fields
.field private maxTokenLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lorg/apache/lucene/analysis/core/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    sput-object v0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 79
    sget-object v0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopwords"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {p2, p1}, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->loadStopwordSet(Ljava/io/Reader;Lorg/apache/lucene/util/Version;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 1
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "stopWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/util/StopwordAnalyzerBase;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 59
    const/16 v0, 0xff

    iput v0, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I

    .line 71
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I

    return v0
.end method


# virtual methods
.method protected createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 5
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 110
    new-instance v0, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v0, v3, p2}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;-><init>(Lorg/apache/lucene/util/Version;Ljava/io/Reader;)V

    .line 111
    .local v0, "src":Lorg/apache/lucene/analysis/standard/ClassicTokenizer;
    iget v3, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I

    invoke-virtual {v0, v3}, Lorg/apache/lucene/analysis/standard/ClassicTokenizer;->setMaxTokenLength(I)V

    .line 112
    new-instance v1, Lorg/apache/lucene/analysis/standard/ClassicFilter;

    invoke-direct {v1, v0}, Lorg/apache/lucene/analysis/standard/ClassicFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;)V

    .line 113
    .local v1, "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v2, Lorg/apache/lucene/analysis/core/LowerCaseFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-direct {v2, v3, v1}, Lorg/apache/lucene/analysis/core/LowerCaseFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;)V

    .line 114
    .end local v1    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    .local v2, "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v1, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v3, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v4, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->stopwords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v1, v3, v2, v4}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 115
    .end local v2    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v1    # "tok":Lorg/apache/lucene/analysis/TokenStream;
    new-instance v3, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;

    invoke-direct {v3, p0, v0, v1, v0}, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer$1;-><init>(Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/ClassicTokenizer;)V

    return-object v3
.end method

.method public getMaxTokenLength()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I

    return v0
.end method

.method public setMaxTokenLength(I)V
    .locals 0
    .param p1, "length"    # I

    .prologue
    .line 98
    iput p1, p0, Lorg/apache/lucene/analysis/standard/ClassicAnalyzer;->maxTokenLength:I

    .line 99
    return-void
.end method
