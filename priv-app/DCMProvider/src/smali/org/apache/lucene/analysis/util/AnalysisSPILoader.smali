.class final Lorg/apache/lucene/analysis/util/AnalysisSPILoader;
.super Ljava/lang/Object;
.source "AnalysisSPILoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TS;>;"
        }
    .end annotation
.end field

.field private volatile services:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+TS;>;>;"
        }
    .end annotation
.end field

.field private final suffixes:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;-><init>(Ljava/lang/Class;[Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/ClassLoader;)V
    .locals 3
    .param p2, "loader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;",
            "Ljava/lang/ClassLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;-><init>(Ljava/lang/Class;[Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;[Ljava/lang/String;)V
    .locals 1
    .param p2, "suffixes"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getContextClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;-><init>(Ljava/lang/Class;[Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;[Ljava/lang/String;Ljava/lang/ClassLoader;)V
    .locals 2
    .param p2, "suffixes"    # [Ljava/lang/String;
    .param p3, "classloader"    # Ljava/lang/ClassLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TS;>;[",
            "Ljava/lang/String;",
            "Ljava/lang/ClassLoader;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TS;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->services:Ljava/util/Map;

    .line 53
    iput-object p1, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->clazz:Ljava/lang/Class;

    .line 54
    iput-object p2, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->suffixes:[Ljava/lang/String;

    .line 56
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 57
    .local v0, "clazzClassloader":Ljava/lang/ClassLoader;
    if-eqz v0, :cond_0

    invoke-static {v0, p3}, Lorg/apache/lucene/util/SPIClassIterator;->isParentClassLoader(Ljava/lang/ClassLoader;Ljava/lang/ClassLoader;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    invoke-virtual {p0, v0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 60
    :cond_0
    invoke-virtual {p0, p3}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 61
    return-void
.end method


# virtual methods
.method public availableServices()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    iget-object v0, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->services:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public lookupClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<+TS;>;"
        }
    .end annotation

    .prologue
    .line 118
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    iget-object v1, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->services:Ljava/util/Map;

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 119
    .local v0, "service":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    if-eqz v0, :cond_0

    .line 120
    return-object v0

    .line 122
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "A SPI class of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->clazz:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with name \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' does not exist. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 123
    const-string v3, "You need to add the corresponding JAR file supporting this SPI to your classpath."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 124
    const-string v3, "The current classpath supports the following names: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->availableServices()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 122
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public newInstance(Ljava/lang/String;Ljava/util/Map;)Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)TS;"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    .local p2, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->lookupClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 110
    .local v1, "service":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/util/Map;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SPI class of type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->clazz:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with name \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' cannot be instantiated. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 113
    const-string v4, "This is likely due to a misconfiguration of the java class \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\': "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 112
    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public declared-synchronized reload(Ljava/lang/ClassLoader;)V
    .locals 11
    .param p1, "classloader"    # Ljava/lang/ClassLoader;

    .prologue
    .local p0, "this":Lorg/apache/lucene/analysis/util/AnalysisSPILoader;, "Lorg/apache/lucene/analysis/util/AnalysisSPILoader<TS;>;"
    const/4 v7, 0x0

    .line 76
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/util/LinkedHashMap;

    iget-object v6, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->services:Ljava/util/Map;

    invoke-direct {v4, v6}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 77
    .local v4, "services":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/Class<+TS;>;>;"
    iget-object v6, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->clazz:Ljava/lang/Class;

    invoke-static {v6, p1}, Lorg/apache/lucene/util/SPIClassIterator;->get(Ljava/lang/Class;Ljava/lang/ClassLoader;)Lorg/apache/lucene/util/SPIClassIterator;

    move-result-object v1

    .line 78
    .local v1, "loader":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/util/SPIClassIterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 104
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->services:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    monitor-exit p0

    return-void

    .line 79
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lorg/apache/lucene/util/SPIClassIterator;->next()Ljava/lang/Class;

    move-result-object v3

    .line 80
    .local v3, "service":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "clazzName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 82
    .local v2, "name":Ljava/lang/String;
    iget-object v8, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->suffixes:[Ljava/lang/String;

    array-length v9, v8

    move v6, v7

    :goto_1
    if-lt v6, v9, :cond_2

    .line 88
    :goto_2
    if-nez v2, :cond_4

    .line 89
    new-instance v6, Ljava/util/ServiceConfigurationError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "The class name "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 90
    const-string v8, " has wrong suffix, allowed are: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/analysis/util/AnalysisSPILoader;->suffixes:[Ljava/lang/String;

    invoke-static {v8}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 89
    invoke-direct {v6, v7}, Ljava/util/ServiceConfigurationError;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    .end local v0    # "clazzName":Ljava/lang/String;
    .end local v1    # "loader":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "service":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    .end local v4    # "services":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/Class<+TS;>;>;"
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 82
    .restart local v0    # "clazzName":Ljava/lang/String;
    .restart local v1    # "loader":Lorg/apache/lucene/util/SPIClassIterator;, "Lorg/apache/lucene/util/SPIClassIterator<TS;>;"
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v3    # "service":Ljava/lang/Class;, "Ljava/lang/Class<+TS;>;"
    .restart local v4    # "services":Ljava/util/LinkedHashMap;, "Ljava/util/LinkedHashMap<Ljava/lang/String;Ljava/lang/Class<+TS;>;>;"
    :cond_2
    :try_start_2
    aget-object v5, v8, v6

    .line 83
    .local v5, "suffix":Ljava/lang/String;
    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 84
    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {v0, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    sget-object v8, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v6, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 85
    goto :goto_2

    .line 82
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 100
    .end local v5    # "suffix":Ljava/lang/String;
    :cond_4
    invoke-virtual {v4, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 101
    invoke-virtual {v4, v2, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
