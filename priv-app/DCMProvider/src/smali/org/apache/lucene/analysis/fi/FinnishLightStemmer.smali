.class public Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;
.super Ljava/lang/Object;
.source "FinnishLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isVowel(C)Z
    .locals 1
    .param p1, "ch"    # C

    .prologue
    .line 249
    sparse-switch p1, :sswitch_data_0

    .line 256
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 255
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 249
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6f -> :sswitch_0
        0x75 -> :sswitch_0
        0x79 -> :sswitch_0
    .end sparse-switch
.end method

.method private norm1([CI)I
    .locals 2
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 197
    const/4 v0, 0x5

    if-le p2, v0, :cond_0

    const-string v0, "hde"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    add-int/lit8 v0, p2, -0x3

    const/16 v1, 0x6b

    aput-char v1, p1, v0

    .line 199
    add-int/lit8 v0, p2, -0x2

    const/16 v1, 0x73

    aput-char v1, p1, v0

    .line 200
    add-int/lit8 v0, p2, -0x1

    const/16 v1, 0x69

    aput-char v1, p1, v0

    .line 203
    :cond_0
    const/4 v0, 0x4

    if-le p2, v0, :cond_3

    .line 204
    const-string v0, "ei"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "at"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    :cond_1
    add-int/lit8 p2, p2, -0x2

    .line 218
    .end local p2    # "len":I
    :cond_2
    :goto_0
    return p2

    .line 208
    .restart local p2    # "len":I
    :cond_3
    const/4 v0, 0x3

    if-le p2, v0, :cond_2

    .line 209
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 215
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 209
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x69 -> :sswitch_0
        0x6a -> :sswitch_0
        0x73 -> :sswitch_0
        0x74 -> :sswitch_0
    .end sparse-switch
.end method

.method private norm2([CI)I
    .locals 6
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v5, 0x4

    .line 222
    const/16 v3, 0x8

    if-le p2, v3, :cond_1

    .line 223
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    const/16 v4, 0x65

    if-eq v3, v4, :cond_0

    .line 224
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    const/16 v4, 0x6f

    if-eq v3, v4, :cond_0

    .line 225
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    const/16 v4, 0x75

    if-ne v3, v4, :cond_1

    .line 226
    :cond_0
    add-int/lit8 p2, p2, -0x1

    .line 229
    :cond_1
    if-le p2, v5, :cond_3

    .line 230
    add-int/lit8 v3, p2, -0x1

    aget-char v3, p1, v3

    const/16 v4, 0x69

    if-ne v3, v4, :cond_2

    .line 231
    add-int/lit8 p2, p2, -0x1

    .line 233
    :cond_2
    if-le p2, v5, :cond_3

    .line 234
    const/4 v3, 0x0

    aget-char v0, p1, v3

    .line 235
    .local v0, "ch":C
    const/4 v1, 0x1

    .local v1, "i":I
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    if-lt v2, p2, :cond_4

    .line 245
    .end local v0    # "ch":C
    .end local v2    # "i":I
    :cond_3
    return p2

    .line 236
    .restart local v0    # "ch":C
    .restart local v2    # "i":I
    :cond_4
    aget-char v3, p1, v2

    if-ne v3, v0, :cond_6

    .line 237
    const/16 v3, 0x6b

    if-eq v0, v3, :cond_5

    const/16 v3, 0x70

    if-eq v0, v3, :cond_5

    const/16 v3, 0x74

    if-ne v0, v3, :cond_6

    .line 238
    :cond_5
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-static {p1, v2, p2}, Lorg/apache/lucene/analysis/util/StemmerUtil;->delete([CII)I

    move-result p2

    .line 235
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v2, v1

    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 240
    :cond_6
    aget-char v0, p1, v2

    move v1, v2

    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_1
.end method

.method private step1([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 86
    const/16 v0, 0x8

    if-le p2, v0, :cond_2

    .line 87
    const-string v0, "kin"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    add-int/lit8 v0, p2, -0x3

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->step1([CI)I

    move-result p2

    .line 99
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 89
    .restart local p2    # "len":I
    :cond_1
    const-string v0, "ko"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    add-int/lit8 v0, p2, -0x2

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->step1([CI)I

    move-result p2

    goto :goto_0

    .line 93
    :cond_2
    const/16 v0, 0xb

    if-le p2, v0, :cond_0

    .line 94
    const-string v0, "dellinen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    add-int/lit8 p2, p2, -0x8

    goto :goto_0

    .line 96
    :cond_3
    const-string v0, "dellisuus"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    add-int/lit8 p2, p2, -0x9

    goto :goto_0
.end method

.method private step2([CI)I
    .locals 1
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 103
    const/4 v0, 0x5

    if-le p2, v0, :cond_1

    .line 104
    const-string v0, "lla"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    const-string v0, "tse"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    const-string v0, "sti"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    :cond_0
    add-int/lit8 p2, p2, -0x3

    .line 116
    .end local p2    # "len":I
    :cond_1
    :goto_0
    return p2

    .line 109
    .restart local p2    # "len":I
    :cond_2
    const-string v0, "ni"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 112
    :cond_3
    const-string v0, "aa"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    add-int/lit8 p2, p2, -0x1

    goto :goto_0
.end method

.method private step3([CI)I
    .locals 4
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/16 v3, 0x6e

    const/16 v2, 0x73

    .line 120
    const/16 v0, 0x8

    if-le p2, v0, :cond_4

    .line 121
    const-string v0, "nnen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    add-int/lit8 v0, p2, -0x4

    aput-char v2, p1, v0

    .line 123
    add-int/lit8 p2, p2, -0x3

    .line 193
    .end local p2    # "len":I
    :cond_0
    :goto_0
    return p2

    .line 126
    .restart local p2    # "len":I
    :cond_1
    const-string v0, "ntena"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    add-int/lit8 v0, p2, -0x5

    aput-char v2, p1, v0

    .line 128
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 131
    :cond_2
    const-string v0, "tten"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 132
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 134
    :cond_3
    const-string v0, "eiden"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 135
    add-int/lit8 p2, p2, -0x5

    goto :goto_0

    .line 138
    :cond_4
    const/4 v0, 0x6

    if-le p2, v0, :cond_b

    .line 139
    const-string v0, "neen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 140
    const-string v0, "niin"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 141
    const-string v0, "seen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 142
    const-string v0, "teen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 143
    const-string v0, "inen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 144
    :cond_5
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 146
    :cond_6
    add-int/lit8 v0, p2, -0x3

    aget-char v0, p1, v0

    const/16 v1, 0x68

    if-ne v0, v1, :cond_7

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_7

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_7

    .line 147
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 149
    :cond_7
    const-string v0, "den"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 150
    add-int/lit8 v0, p2, -0x3

    aput-char v2, p1, v0

    .line 151
    add-int/lit8 p2, p2, -0x2

    goto :goto_0

    .line 154
    :cond_8
    const-string v0, "ksen"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 155
    add-int/lit8 v0, p2, -0x4

    aput-char v2, p1, v0

    .line 156
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 159
    :cond_9
    const-string v0, "ssa"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 160
    const-string v0, "sta"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 161
    const-string v0, "lla"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 162
    const-string v0, "lta"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 163
    const-string v0, "tta"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 164
    const-string v0, "ksi"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 165
    const-string v0, "lle"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 166
    :cond_a
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 169
    :cond_b
    const/4 v0, 0x5

    if-le p2, v0, :cond_e

    .line 170
    const-string v0, "na"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 171
    const-string v0, "ne"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 172
    :cond_c
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 174
    :cond_d
    const-string v0, "nei"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 175
    add-int/lit8 p2, p2, -0x3

    goto/16 :goto_0

    .line 178
    :cond_e
    const/4 v0, 0x4

    if-le p2, v0, :cond_0

    .line 179
    const-string v0, "ja"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 180
    const-string v0, "ta"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 181
    :cond_f
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 183
    :cond_10
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x61

    if-ne v0, v1, :cond_11

    .line 184
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 186
    :cond_11
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_12

    add-int/lit8 v0, p2, -0x2

    aget-char v0, p1, v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->isVowel(C)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 187
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 189
    :cond_12
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_0

    .line 190
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public stem([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    .line 67
    const/4 v2, 0x4

    if-ge p2, v2, :cond_0

    move v1, p2

    .line 82
    .end local p2    # "len":I
    .local v1, "len":I
    :goto_0
    return v1

    .line 70
    .end local v1    # "len":I
    .restart local p2    # "len":I
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, p2, :cond_1

    .line 77
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->step1([CI)I

    move-result p2

    .line 78
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->step2([CI)I

    move-result p2

    .line 79
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->step3([CI)I

    move-result p2

    .line 80
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->norm1([CI)I

    move-result p2

    .line 81
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/analysis/fi/FinnishLightStemmer;->norm2([CI)I

    move-result p2

    move v1, p2

    .line 82
    .end local p2    # "len":I
    .restart local v1    # "len":I
    goto :goto_0

    .line 71
    .end local v1    # "len":I
    .restart local p2    # "len":I
    :cond_1
    aget-char v2, p1, v0

    sparse-switch v2, :sswitch_data_0

    .line 70
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 73
    :sswitch_0
    const/16 v2, 0x61

    aput-char v2, p1, v0

    goto :goto_2

    .line 74
    :sswitch_1
    const/16 v2, 0x6f

    aput-char v2, p1, v0

    goto :goto_2

    .line 71
    :sswitch_data_0
    .sparse-switch
        0xe4 -> :sswitch_0
        0xe5 -> :sswitch_0
        0xf6 -> :sswitch_1
    .end sparse-switch
.end method
