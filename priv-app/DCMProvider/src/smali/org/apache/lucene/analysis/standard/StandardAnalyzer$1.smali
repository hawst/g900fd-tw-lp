.class Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;
.super Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.source "StandardAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

.field private final synthetic val$src:Lorg/apache/lucene/analysis/standard/StandardTokenizer;


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/standard/StandardAnalyzer;Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/standard/StandardTokenizer;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/analysis/Tokenizer;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

    iput-object p4, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    .line 116
    invoke-direct {p0, p2, p3}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    return-void
.end method


# virtual methods
.method protected setReader(Ljava/io/Reader;)V
    .locals 2
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->val$src:Lorg/apache/lucene/analysis/standard/StandardTokenizer;

    iget-object v1, p0, Lorg/apache/lucene/analysis/standard/StandardAnalyzer$1;->this$0:Lorg/apache/lucene/analysis/standard/StandardAnalyzer;

    # getter for: Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->maxTokenLength:I
    invoke-static {v1}, Lorg/apache/lucene/analysis/standard/StandardAnalyzer;->access$0(Lorg/apache/lucene/analysis/standard/StandardAnalyzer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/standard/StandardTokenizer;->setMaxTokenLength(I)V

    .line 120
    invoke-super {p0, p1}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;->setReader(Ljava/io/Reader;)V

    .line 121
    return-void
.end method
