.class public final Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;
.super Lorg/apache/lucene/analysis/Analyzer;
.source "PatternAnalyzer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringReader;,
        Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;,
        Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final DEFAULT_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

.field public static final EXTENDED_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

.field private static final EXTENDED_ENGLISH_STOP_WORDS:Lorg/apache/lucene/analysis/util/CharArraySet;

.field public static final NON_WORD_PATTERN:Ljava/util/regex/Pattern;

.field public static final WHITESPACE_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final matchVersion:Lorg/apache/lucene/util/Version;

.field private final pattern:Ljava/util/regex/Pattern;

.field private final stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

.field private final toLowerCase:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 71
    const-string v0, "\\W+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->NON_WORD_PATTERN:Ljava/util/regex/Pattern;

    .line 74
    const-string v0, "\\s+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->WHITESPACE_PATTERN:Ljava/util/regex/Pattern;

    .line 77
    new-instance v0, Lorg/apache/lucene/analysis/util/CharArraySet;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    const/16 v2, 0x112

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 79
    const-string v4, "a"

    aput-object v4, v2, v3

    const-string v3, "about"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "above"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "across"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "adj"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "after"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "afterwards"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    .line 80
    const-string v4, "again"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "against"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "albeit"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "almost"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "alone"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "along"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    .line 81
    const-string v4, "already"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "also"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "although"

    aput-object v4, v2, v3

    const/16 v3, 0x11

    const-string v4, "always"

    aput-object v4, v2, v3

    const/16 v3, 0x12

    const-string v4, "among"

    aput-object v4, v2, v3

    const/16 v3, 0x13

    const-string v4, "amongst"

    aput-object v4, v2, v3

    const/16 v3, 0x14

    const-string v4, "an"

    aput-object v4, v2, v3

    const/16 v3, 0x15

    .line 82
    const-string v4, "and"

    aput-object v4, v2, v3

    const/16 v3, 0x16

    const-string v4, "another"

    aput-object v4, v2, v3

    const/16 v3, 0x17

    const-string v4, "any"

    aput-object v4, v2, v3

    const/16 v3, 0x18

    const-string v4, "anyhow"

    aput-object v4, v2, v3

    const/16 v3, 0x19

    const-string v4, "anyone"

    aput-object v4, v2, v3

    const/16 v3, 0x1a

    const-string v4, "anything"

    aput-object v4, v2, v3

    const/16 v3, 0x1b

    .line 83
    const-string v4, "anywhere"

    aput-object v4, v2, v3

    const/16 v3, 0x1c

    const-string v4, "are"

    aput-object v4, v2, v3

    const/16 v3, 0x1d

    const-string v4, "around"

    aput-object v4, v2, v3

    const/16 v3, 0x1e

    const-string v4, "as"

    aput-object v4, v2, v3

    const/16 v3, 0x1f

    const-string v4, "at"

    aput-object v4, v2, v3

    const/16 v3, 0x20

    const-string v4, "be"

    aput-object v4, v2, v3

    const/16 v3, 0x21

    const-string v4, "became"

    aput-object v4, v2, v3

    const/16 v3, 0x22

    const-string v4, "because"

    aput-object v4, v2, v3

    const/16 v3, 0x23

    .line 84
    const-string v4, "become"

    aput-object v4, v2, v3

    const/16 v3, 0x24

    const-string v4, "becomes"

    aput-object v4, v2, v3

    const/16 v3, 0x25

    const-string v4, "becoming"

    aput-object v4, v2, v3

    const/16 v3, 0x26

    const-string v4, "been"

    aput-object v4, v2, v3

    const/16 v3, 0x27

    const-string v4, "before"

    aput-object v4, v2, v3

    const/16 v3, 0x28

    const-string v4, "beforehand"

    aput-object v4, v2, v3

    const/16 v3, 0x29

    .line 85
    const-string v4, "behind"

    aput-object v4, v2, v3

    const/16 v3, 0x2a

    const-string v4, "being"

    aput-object v4, v2, v3

    const/16 v3, 0x2b

    const-string v4, "below"

    aput-object v4, v2, v3

    const/16 v3, 0x2c

    const-string v4, "beside"

    aput-object v4, v2, v3

    const/16 v3, 0x2d

    const-string v4, "besides"

    aput-object v4, v2, v3

    const/16 v3, 0x2e

    const-string v4, "between"

    aput-object v4, v2, v3

    const/16 v3, 0x2f

    .line 86
    const-string v4, "beyond"

    aput-object v4, v2, v3

    const/16 v3, 0x30

    const-string v4, "both"

    aput-object v4, v2, v3

    const/16 v3, 0x31

    const-string v4, "but"

    aput-object v4, v2, v3

    const/16 v3, 0x32

    const-string v4, "by"

    aput-object v4, v2, v3

    const/16 v3, 0x33

    const-string v4, "can"

    aput-object v4, v2, v3

    const/16 v3, 0x34

    const-string v4, "cannot"

    aput-object v4, v2, v3

    const/16 v3, 0x35

    const-string v4, "co"

    aput-object v4, v2, v3

    const/16 v3, 0x36

    const-string v4, "could"

    aput-object v4, v2, v3

    const/16 v3, 0x37

    .line 87
    const-string v4, "down"

    aput-object v4, v2, v3

    const/16 v3, 0x38

    const-string v4, "during"

    aput-object v4, v2, v3

    const/16 v3, 0x39

    const-string v4, "each"

    aput-object v4, v2, v3

    const/16 v3, 0x3a

    const-string v4, "eg"

    aput-object v4, v2, v3

    const/16 v3, 0x3b

    const-string v4, "either"

    aput-object v4, v2, v3

    const/16 v3, 0x3c

    const-string v4, "else"

    aput-object v4, v2, v3

    const/16 v3, 0x3d

    const-string v4, "elsewhere"

    aput-object v4, v2, v3

    const/16 v3, 0x3e

    .line 88
    const-string v4, "enough"

    aput-object v4, v2, v3

    const/16 v3, 0x3f

    const-string v4, "etc"

    aput-object v4, v2, v3

    const/16 v3, 0x40

    const-string v4, "even"

    aput-object v4, v2, v3

    const/16 v3, 0x41

    const-string v4, "ever"

    aput-object v4, v2, v3

    const/16 v3, 0x42

    const-string v4, "every"

    aput-object v4, v2, v3

    const/16 v3, 0x43

    const-string v4, "everyone"

    aput-object v4, v2, v3

    const/16 v3, 0x44

    const-string v4, "everything"

    aput-object v4, v2, v3

    const/16 v3, 0x45

    .line 89
    const-string v4, "everywhere"

    aput-object v4, v2, v3

    const/16 v3, 0x46

    const-string v4, "except"

    aput-object v4, v2, v3

    const/16 v3, 0x47

    const-string v4, "few"

    aput-object v4, v2, v3

    const/16 v3, 0x48

    const-string v4, "first"

    aput-object v4, v2, v3

    const/16 v3, 0x49

    const-string v4, "for"

    aput-object v4, v2, v3

    const/16 v3, 0x4a

    const-string v4, "former"

    aput-object v4, v2, v3

    const/16 v3, 0x4b

    .line 90
    const-string v4, "formerly"

    aput-object v4, v2, v3

    const/16 v3, 0x4c

    const-string v4, "from"

    aput-object v4, v2, v3

    const/16 v3, 0x4d

    const-string v4, "further"

    aput-object v4, v2, v3

    const/16 v3, 0x4e

    const-string v4, "had"

    aput-object v4, v2, v3

    const/16 v3, 0x4f

    const-string v4, "has"

    aput-object v4, v2, v3

    const/16 v3, 0x50

    const-string v4, "have"

    aput-object v4, v2, v3

    const/16 v3, 0x51

    const-string v4, "he"

    aput-object v4, v2, v3

    const/16 v3, 0x52

    const-string v4, "hence"

    aput-object v4, v2, v3

    const/16 v3, 0x53

    .line 91
    const-string v4, "her"

    aput-object v4, v2, v3

    const/16 v3, 0x54

    const-string v4, "here"

    aput-object v4, v2, v3

    const/16 v3, 0x55

    const-string v4, "hereafter"

    aput-object v4, v2, v3

    const/16 v3, 0x56

    const-string v4, "hereby"

    aput-object v4, v2, v3

    const/16 v3, 0x57

    const-string v4, "herein"

    aput-object v4, v2, v3

    const/16 v3, 0x58

    const-string v4, "hereupon"

    aput-object v4, v2, v3

    const/16 v3, 0x59

    const-string v4, "hers"

    aput-object v4, v2, v3

    const/16 v3, 0x5a

    .line 92
    const-string v4, "herself"

    aput-object v4, v2, v3

    const/16 v3, 0x5b

    const-string v4, "him"

    aput-object v4, v2, v3

    const/16 v3, 0x5c

    const-string v4, "himself"

    aput-object v4, v2, v3

    const/16 v3, 0x5d

    const-string v4, "his"

    aput-object v4, v2, v3

    const/16 v3, 0x5e

    const-string v4, "how"

    aput-object v4, v2, v3

    const/16 v3, 0x5f

    const-string v4, "however"

    aput-object v4, v2, v3

    const/16 v3, 0x60

    const-string v4, "i"

    aput-object v4, v2, v3

    const/16 v3, 0x61

    const-string v4, "ie"

    aput-object v4, v2, v3

    const/16 v3, 0x62

    const-string v4, "if"

    aput-object v4, v2, v3

    const/16 v3, 0x63

    .line 93
    const-string v4, "in"

    aput-object v4, v2, v3

    const/16 v3, 0x64

    const-string v4, "inc"

    aput-object v4, v2, v3

    const/16 v3, 0x65

    const-string v4, "indeed"

    aput-object v4, v2, v3

    const/16 v3, 0x66

    const-string v4, "into"

    aput-object v4, v2, v3

    const/16 v3, 0x67

    const-string v4, "is"

    aput-object v4, v2, v3

    const/16 v3, 0x68

    const-string v4, "it"

    aput-object v4, v2, v3

    const/16 v3, 0x69

    const-string v4, "its"

    aput-object v4, v2, v3

    const/16 v3, 0x6a

    const-string v4, "itself"

    aput-object v4, v2, v3

    const/16 v3, 0x6b

    const-string v4, "last"

    aput-object v4, v2, v3

    const/16 v3, 0x6c

    .line 94
    const-string v4, "latter"

    aput-object v4, v2, v3

    const/16 v3, 0x6d

    const-string v4, "latterly"

    aput-object v4, v2, v3

    const/16 v3, 0x6e

    const-string v4, "least"

    aput-object v4, v2, v3

    const/16 v3, 0x6f

    const-string v4, "less"

    aput-object v4, v2, v3

    const/16 v3, 0x70

    const-string v4, "ltd"

    aput-object v4, v2, v3

    const/16 v3, 0x71

    const-string v4, "many"

    aput-object v4, v2, v3

    const/16 v3, 0x72

    const-string v4, "may"

    aput-object v4, v2, v3

    const/16 v3, 0x73

    const-string v4, "me"

    aput-object v4, v2, v3

    const/16 v3, 0x74

    .line 95
    const-string v4, "meanwhile"

    aput-object v4, v2, v3

    const/16 v3, 0x75

    const-string v4, "might"

    aput-object v4, v2, v3

    const/16 v3, 0x76

    const-string v4, "more"

    aput-object v4, v2, v3

    const/16 v3, 0x77

    const-string v4, "moreover"

    aput-object v4, v2, v3

    const/16 v3, 0x78

    const-string v4, "most"

    aput-object v4, v2, v3

    const/16 v3, 0x79

    const-string v4, "mostly"

    aput-object v4, v2, v3

    const/16 v3, 0x7a

    const-string v4, "much"

    aput-object v4, v2, v3

    const/16 v3, 0x7b

    .line 96
    const-string v4, "must"

    aput-object v4, v2, v3

    const/16 v3, 0x7c

    const-string v4, "my"

    aput-object v4, v2, v3

    const/16 v3, 0x7d

    const-string v4, "myself"

    aput-object v4, v2, v3

    const/16 v3, 0x7e

    const-string v4, "namely"

    aput-object v4, v2, v3

    const/16 v3, 0x7f

    const-string v4, "neither"

    aput-object v4, v2, v3

    const/16 v3, 0x80

    const-string v4, "never"

    aput-object v4, v2, v3

    const/16 v3, 0x81

    .line 97
    const-string v4, "nevertheless"

    aput-object v4, v2, v3

    const/16 v3, 0x82

    const-string v4, "next"

    aput-object v4, v2, v3

    const/16 v3, 0x83

    const-string v4, "no"

    aput-object v4, v2, v3

    const/16 v3, 0x84

    const-string v4, "nobody"

    aput-object v4, v2, v3

    const/16 v3, 0x85

    const-string v4, "none"

    aput-object v4, v2, v3

    const/16 v3, 0x86

    const-string v4, "noone"

    aput-object v4, v2, v3

    const/16 v3, 0x87

    const-string v4, "nor"

    aput-object v4, v2, v3

    const/16 v3, 0x88

    .line 98
    const-string v4, "not"

    aput-object v4, v2, v3

    const/16 v3, 0x89

    const-string v4, "nothing"

    aput-object v4, v2, v3

    const/16 v3, 0x8a

    const-string v4, "now"

    aput-object v4, v2, v3

    const/16 v3, 0x8b

    const-string v4, "nowhere"

    aput-object v4, v2, v3

    const/16 v3, 0x8c

    const-string v4, "of"

    aput-object v4, v2, v3

    const/16 v3, 0x8d

    const-string v4, "off"

    aput-object v4, v2, v3

    const/16 v3, 0x8e

    const-string v4, "often"

    aput-object v4, v2, v3

    const/16 v3, 0x8f

    const-string v4, "on"

    aput-object v4, v2, v3

    const/16 v3, 0x90

    .line 99
    const-string v4, "once one"

    aput-object v4, v2, v3

    const/16 v3, 0x91

    const-string v4, "only"

    aput-object v4, v2, v3

    const/16 v3, 0x92

    const-string v4, "onto"

    aput-object v4, v2, v3

    const/16 v3, 0x93

    const-string v4, "or"

    aput-object v4, v2, v3

    const/16 v3, 0x94

    const-string v4, "other"

    aput-object v4, v2, v3

    const/16 v3, 0x95

    const-string v4, "others"

    aput-object v4, v2, v3

    const/16 v3, 0x96

    const-string v4, "otherwise"

    aput-object v4, v2, v3

    const/16 v3, 0x97

    .line 100
    const-string v4, "our"

    aput-object v4, v2, v3

    const/16 v3, 0x98

    const-string v4, "ours"

    aput-object v4, v2, v3

    const/16 v3, 0x99

    const-string v4, "ourselves"

    aput-object v4, v2, v3

    const/16 v3, 0x9a

    const-string v4, "out"

    aput-object v4, v2, v3

    const/16 v3, 0x9b

    const-string v4, "over"

    aput-object v4, v2, v3

    const/16 v3, 0x9c

    const-string v4, "own"

    aput-object v4, v2, v3

    const/16 v3, 0x9d

    const-string v4, "per"

    aput-object v4, v2, v3

    const/16 v3, 0x9e

    const-string v4, "perhaps"

    aput-object v4, v2, v3

    const/16 v3, 0x9f

    .line 101
    const-string v4, "rather"

    aput-object v4, v2, v3

    const/16 v3, 0xa0

    const-string v4, "s"

    aput-object v4, v2, v3

    const/16 v3, 0xa1

    const-string v4, "same"

    aput-object v4, v2, v3

    const/16 v3, 0xa2

    const-string v4, "seem"

    aput-object v4, v2, v3

    const/16 v3, 0xa3

    const-string v4, "seemed"

    aput-object v4, v2, v3

    const/16 v3, 0xa4

    const-string v4, "seeming"

    aput-object v4, v2, v3

    const/16 v3, 0xa5

    const-string v4, "seems"

    aput-object v4, v2, v3

    const/16 v3, 0xa6

    .line 102
    const-string v4, "several"

    aput-object v4, v2, v3

    const/16 v3, 0xa7

    const-string v4, "she"

    aput-object v4, v2, v3

    const/16 v3, 0xa8

    const-string v4, "should"

    aput-object v4, v2, v3

    const/16 v3, 0xa9

    const-string v4, "since"

    aput-object v4, v2, v3

    const/16 v3, 0xaa

    const-string v4, "so"

    aput-object v4, v2, v3

    const/16 v3, 0xab

    const-string v4, "some"

    aput-object v4, v2, v3

    const/16 v3, 0xac

    const-string v4, "somehow"

    aput-object v4, v2, v3

    const/16 v3, 0xad

    .line 103
    const-string v4, "someone"

    aput-object v4, v2, v3

    const/16 v3, 0xae

    const-string v4, "something"

    aput-object v4, v2, v3

    const/16 v3, 0xaf

    const-string v4, "sometime"

    aput-object v4, v2, v3

    const/16 v3, 0xb0

    const-string v4, "sometimes"

    aput-object v4, v2, v3

    const/16 v3, 0xb1

    const-string v4, "somewhere"

    aput-object v4, v2, v3

    const/16 v3, 0xb2

    .line 104
    const-string v4, "still"

    aput-object v4, v2, v3

    const/16 v3, 0xb3

    const-string v4, "such"

    aput-object v4, v2, v3

    const/16 v3, 0xb4

    const-string v4, "t"

    aput-object v4, v2, v3

    const/16 v3, 0xb5

    const-string v4, "than"

    aput-object v4, v2, v3

    const/16 v3, 0xb6

    const-string v4, "that"

    aput-object v4, v2, v3

    const/16 v3, 0xb7

    const-string v4, "the"

    aput-object v4, v2, v3

    const/16 v3, 0xb8

    const-string v4, "their"

    aput-object v4, v2, v3

    const/16 v3, 0xb9

    const-string v4, "them"

    aput-object v4, v2, v3

    const/16 v3, 0xba

    .line 105
    const-string v4, "themselves"

    aput-object v4, v2, v3

    const/16 v3, 0xbb

    const-string v4, "then"

    aput-object v4, v2, v3

    const/16 v3, 0xbc

    const-string v4, "thence"

    aput-object v4, v2, v3

    const/16 v3, 0xbd

    const-string v4, "there"

    aput-object v4, v2, v3

    const/16 v3, 0xbe

    const-string v4, "thereafter"

    aput-object v4, v2, v3

    const/16 v3, 0xbf

    const-string v4, "thereby"

    aput-object v4, v2, v3

    const/16 v3, 0xc0

    .line 106
    const-string v4, "therefor"

    aput-object v4, v2, v3

    const/16 v3, 0xc1

    const-string v4, "therein"

    aput-object v4, v2, v3

    const/16 v3, 0xc2

    const-string v4, "thereupon"

    aput-object v4, v2, v3

    const/16 v3, 0xc3

    const-string v4, "these"

    aput-object v4, v2, v3

    const/16 v3, 0xc4

    const-string v4, "they"

    aput-object v4, v2, v3

    const/16 v3, 0xc5

    const-string v4, "this"

    aput-object v4, v2, v3

    const/16 v3, 0xc6

    .line 107
    const-string v4, "those"

    aput-object v4, v2, v3

    const/16 v3, 0xc7

    const-string v4, "though"

    aput-object v4, v2, v3

    const/16 v3, 0xc8

    const-string v4, "through"

    aput-object v4, v2, v3

    const/16 v3, 0xc9

    const-string v4, "throughout"

    aput-object v4, v2, v3

    const/16 v3, 0xca

    const-string v4, "thru"

    aput-object v4, v2, v3

    const/16 v3, 0xcb

    const-string v4, "thus"

    aput-object v4, v2, v3

    const/16 v3, 0xcc

    const-string v4, "to"

    aput-object v4, v2, v3

    const/16 v3, 0xcd

    .line 108
    const-string v4, "together"

    aput-object v4, v2, v3

    const/16 v3, 0xce

    const-string v4, "too"

    aput-object v4, v2, v3

    const/16 v3, 0xcf

    const-string v4, "toward"

    aput-object v4, v2, v3

    const/16 v3, 0xd0

    const-string v4, "towards"

    aput-object v4, v2, v3

    const/16 v3, 0xd1

    const-string v4, "under"

    aput-object v4, v2, v3

    const/16 v3, 0xd2

    const-string v4, "until"

    aput-object v4, v2, v3

    const/16 v3, 0xd3

    const-string v4, "up"

    aput-object v4, v2, v3

    const/16 v3, 0xd4

    .line 109
    const-string v4, "upon"

    aput-object v4, v2, v3

    const/16 v3, 0xd5

    const-string v4, "us"

    aput-object v4, v2, v3

    const/16 v3, 0xd6

    const-string/jumbo v4, "very"

    aput-object v4, v2, v3

    const/16 v3, 0xd7

    const-string/jumbo v4, "via"

    aput-object v4, v2, v3

    const/16 v3, 0xd8

    const-string/jumbo v4, "was"

    aput-object v4, v2, v3

    const/16 v3, 0xd9

    const-string/jumbo v4, "we"

    aput-object v4, v2, v3

    const/16 v3, 0xda

    const-string/jumbo v4, "well"

    aput-object v4, v2, v3

    const/16 v3, 0xdb

    const-string/jumbo v4, "were"

    aput-object v4, v2, v3

    const/16 v3, 0xdc

    const-string/jumbo v4, "what"

    aput-object v4, v2, v3

    const/16 v3, 0xdd

    .line 110
    const-string/jumbo v4, "whatever"

    aput-object v4, v2, v3

    const/16 v3, 0xde

    const-string/jumbo v4, "whatsoever"

    aput-object v4, v2, v3

    const/16 v3, 0xdf

    const-string/jumbo v4, "when"

    aput-object v4, v2, v3

    const/16 v3, 0xe0

    const-string/jumbo v4, "whence"

    aput-object v4, v2, v3

    const/16 v3, 0xe1

    const-string/jumbo v4, "whenever"

    aput-object v4, v2, v3

    const/16 v3, 0xe2

    .line 111
    const-string/jumbo v4, "whensoever"

    aput-object v4, v2, v3

    const/16 v3, 0xe3

    const-string/jumbo v4, "where"

    aput-object v4, v2, v3

    const/16 v3, 0xe4

    const-string/jumbo v4, "whereafter"

    aput-object v4, v2, v3

    const/16 v3, 0xe5

    const-string/jumbo v4, "whereas"

    aput-object v4, v2, v3

    const/16 v3, 0xe6

    const-string/jumbo v4, "whereat"

    aput-object v4, v2, v3

    const/16 v3, 0xe7

    .line 112
    const-string/jumbo v4, "whereby"

    aput-object v4, v2, v3

    const/16 v3, 0xe8

    const-string/jumbo v4, "wherefrom"

    aput-object v4, v2, v3

    const/16 v3, 0xe9

    const-string/jumbo v4, "wherein"

    aput-object v4, v2, v3

    const/16 v3, 0xea

    const-string/jumbo v4, "whereinto"

    aput-object v4, v2, v3

    const/16 v3, 0xeb

    const-string/jumbo v4, "whereof"

    aput-object v4, v2, v3

    const/16 v3, 0xec

    .line 113
    const-string/jumbo v4, "whereon"

    aput-object v4, v2, v3

    const/16 v3, 0xed

    const-string/jumbo v4, "whereto"

    aput-object v4, v2, v3

    const/16 v3, 0xee

    const-string/jumbo v4, "whereunto"

    aput-object v4, v2, v3

    const/16 v3, 0xef

    const-string/jumbo v4, "whereupon"

    aput-object v4, v2, v3

    const/16 v3, 0xf0

    const-string/jumbo v4, "wherever"

    aput-object v4, v2, v3

    const/16 v3, 0xf1

    .line 114
    const-string/jumbo v4, "wherewith"

    aput-object v4, v2, v3

    const/16 v3, 0xf2

    const-string/jumbo v4, "whether"

    aput-object v4, v2, v3

    const/16 v3, 0xf3

    const-string/jumbo v4, "which"

    aput-object v4, v2, v3

    const/16 v3, 0xf4

    const-string/jumbo v4, "whichever"

    aput-object v4, v2, v3

    const/16 v3, 0xf5

    const-string/jumbo v4, "whichsoever"

    aput-object v4, v2, v3

    const/16 v3, 0xf6

    .line 115
    const-string/jumbo v4, "while"

    aput-object v4, v2, v3

    const/16 v3, 0xf7

    const-string/jumbo v4, "whilst"

    aput-object v4, v2, v3

    const/16 v3, 0xf8

    const-string/jumbo v4, "whither"

    aput-object v4, v2, v3

    const/16 v3, 0xf9

    const-string/jumbo v4, "who"

    aput-object v4, v2, v3

    const/16 v3, 0xfa

    const-string/jumbo v4, "whoever"

    aput-object v4, v2, v3

    const/16 v3, 0xfb

    const-string/jumbo v4, "whole"

    aput-object v4, v2, v3

    const/16 v3, 0xfc

    const-string/jumbo v4, "whom"

    aput-object v4, v2, v3

    const/16 v3, 0xfd

    .line 116
    const-string/jumbo v4, "whomever"

    aput-object v4, v2, v3

    const/16 v3, 0xfe

    const-string/jumbo v4, "whomsoever"

    aput-object v4, v2, v3

    const/16 v3, 0xff

    const-string/jumbo v4, "whose"

    aput-object v4, v2, v3

    const/16 v3, 0x100

    const-string/jumbo v4, "whosoever"

    aput-object v4, v2, v3

    const/16 v3, 0x101

    const-string/jumbo v4, "why"

    aput-object v4, v2, v3

    const/16 v3, 0x102

    const-string/jumbo v4, "will"

    aput-object v4, v2, v3

    const/16 v3, 0x103

    .line 117
    const-string/jumbo v4, "with"

    aput-object v4, v2, v3

    const/16 v3, 0x104

    const-string/jumbo v4, "within"

    aput-object v4, v2, v3

    const/16 v3, 0x105

    const-string/jumbo v4, "without"

    aput-object v4, v2, v3

    const/16 v3, 0x106

    const-string/jumbo v4, "would"

    aput-object v4, v2, v3

    const/16 v3, 0x107

    const-string/jumbo v4, "xsubj"

    aput-object v4, v2, v3

    const/16 v3, 0x108

    const-string/jumbo v4, "xcal"

    aput-object v4, v2, v3

    const/16 v3, 0x109

    const-string/jumbo v4, "xauthor"

    aput-object v4, v2, v3

    const/16 v3, 0x10a

    .line 118
    const-string/jumbo v4, "xother "

    aput-object v4, v2, v3

    const/16 v3, 0x10b

    const-string/jumbo v4, "xnote"

    aput-object v4, v2, v3

    const/16 v3, 0x10c

    const-string/jumbo v4, "yet"

    aput-object v4, v2, v3

    const/16 v3, 0x10d

    const-string/jumbo v4, "you"

    aput-object v4, v2, v3

    const/16 v3, 0x10e

    const-string/jumbo v4, "your"

    aput-object v4, v2, v3

    const/16 v3, 0x10f

    const-string/jumbo v4, "yours"

    aput-object v4, v2, v3

    const/16 v3, 0x110

    const-string/jumbo v4, "yourself"

    aput-object v4, v2, v3

    const/16 v3, 0x111

    .line 119
    const-string/jumbo v4, "yourselves"

    aput-object v4, v2, v3

    .line 78
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 77
    invoke-direct {v0, v1, v2, v5}, Lorg/apache/lucene/analysis/util/CharArraySet;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/Collection;Z)V

    invoke-static {v0}, Lorg/apache/lucene/analysis/util/CharArraySet;->unmodifiableSet(Lorg/apache/lucene/analysis/util/CharArraySet;)Lorg/apache/lucene/analysis/util/CharArraySet;

    move-result-object v0

    .line 76
    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->EXTENDED_ENGLISH_STOP_WORDS:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 126
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    .line 127
    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    sget-object v2, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->NON_WORD_PATTERN:Ljava/util/regex/Pattern;

    sget-object v3, Lorg/apache/lucene/analysis/core/StopAnalyzer;->ENGLISH_STOP_WORDS_SET:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 126
    invoke-direct {v0, v1, v2, v5, v3}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/regex/Pattern;ZLorg/apache/lucene/analysis/util/CharArraySet;)V

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->DEFAULT_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    .line 136
    new-instance v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    .line 137
    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    sget-object v2, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->NON_WORD_PATTERN:Ljava/util/regex/Pattern;

    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->EXTENDED_ENGLISH_STOP_WORDS:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 136
    invoke-direct {v0, v1, v2, v5, v3}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;-><init>(Lorg/apache/lucene/util/Version;Ljava/util/regex/Pattern;ZLorg/apache/lucene/analysis/util/CharArraySet;)V

    sput-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->EXTENDED_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    .line 137
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/Version;Ljava/util/regex/Pattern;ZLorg/apache/lucene/analysis/util/CharArraySet;)V
    .locals 2
    .param p1, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p2, "pattern"    # Ljava/util/regex/Pattern;
    .param p3, "toLowerCase"    # Z
    .param p4, "stopWords"    # Lorg/apache/lucene/analysis/util/CharArraySet;

    .prologue
    .line 164
    invoke-direct {p0}, Lorg/apache/lucene/analysis/Analyzer;-><init>()V

    .line 165
    if-nez p2, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pattern must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    sget-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->NON_WORD_PATTERN:Ljava/util/regex/Pattern;

    invoke-static {v0, p2}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->eqPattern(Ljava/util/regex/Pattern;Ljava/util/regex/Pattern;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p2, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->NON_WORD_PATTERN:Ljava/util/regex/Pattern;

    .line 171
    :cond_1
    :goto_0
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Lorg/apache/lucene/analysis/util/CharArraySet;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 p4, 0x0

    .line 173
    :cond_2
    iput-object p2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    .line 174
    iput-boolean p3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    .line 175
    iput-object p4, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    .line 176
    iput-object p1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 177
    return-void

    .line 169
    :cond_3
    sget-object v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->WHITESPACE_PATTERN:Ljava/util/regex/Pattern;

    invoke-static {v0, p2}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->eqPattern(Ljava/util/regex/Pattern;Ljava/util/regex/Pattern;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p2, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->WHITESPACE_PATTERN:Ljava/util/regex/Pattern;

    goto :goto_0
.end method

.method static synthetic access$0(Ljava/io/Reader;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    invoke-static {p0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toString(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static eq(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "o1"    # Ljava/lang/Object;
    .param p1, "o2"    # Ljava/lang/Object;

    .prologue
    .line 267
    if-eq p0, p1, :cond_1

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static eqPattern(Ljava/util/regex/Pattern;Ljava/util/regex/Pattern;)Z
    .locals 2
    .param p0, "p1"    # Ljava/util/regex/Pattern;
    .param p1, "p2"    # Ljava/util/regex/Pattern;

    .prologue
    .line 272
    if-eq p0, p1, :cond_1

    invoke-virtual {p0}, Ljava/util/regex/Pattern;->flags()I

    move-result v0

    invoke-virtual {p1}, Ljava/util/regex/Pattern;->flags()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static toString(Ljava/io/Reader;)Ljava/lang/String;
    .locals 7
    .param p0, "input"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    instance-of v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringReader;

    if-eqz v5, :cond_0

    .line 283
    check-cast p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringReader;

    .end local p0    # "input":Ljava/io/Reader;
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringReader;->getString()Ljava/lang/String;

    move-result-object v5

    .line 306
    .local v0, "buffer":[C
    .local v1, "len":I
    .local v2, "n":I
    .local v3, "output":[C
    .restart local p0    # "input":Ljava/io/Reader;
    :goto_0
    return-object v5

    .line 287
    .end local v0    # "buffer":[C
    .end local v1    # "len":I
    .end local v2    # "n":I
    .end local v3    # "output":[C
    :cond_0
    const/16 v1, 0x100

    .line 288
    .restart local v1    # "len":I
    :try_start_0
    new-array v0, v1, [C

    .line 289
    .restart local v0    # "buffer":[C
    new-array v3, v1, [C

    .line 291
    .restart local v3    # "output":[C
    const/4 v1, 0x0

    .line 293
    :goto_1
    invoke-virtual {p0, v0}, Ljava/io/Reader;->read([C)I

    move-result v2

    .restart local v2    # "n":I
    if-gez v2, :cond_1

    .line 306
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v6, v1}, Ljava/lang/String;-><init>([CII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    goto :goto_0

    .line 294
    :cond_1
    add-int v5, v1, v2

    :try_start_1
    array-length v6, v3

    if-le v5, v6, :cond_2

    .line 295
    array-length v5, v3

    shl-int/lit8 v5, v5, 0x1

    add-int v6, v1, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    new-array v4, v5, [C

    .line 296
    .local v4, "tmp":[C
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v5, v4, v6, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 297
    const/4 v5, 0x0

    invoke-static {v0, v5, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    move-object v0, v3

    .line 299
    move-object v3, v4

    .line 303
    .end local v4    # "tmp":[C
    :goto_2
    add-int/2addr v1, v2

    goto :goto_1

    .line 301
    :cond_2
    const/4 v5, 0x0

    invoke-static {v0, v5, v3, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 307
    .end local v0    # "buffer":[C
    .end local v2    # "n":I
    .end local v3    # "output":[C
    :catchall_0
    move-exception v5

    .line 308
    invoke-virtual {p0}, Ljava/io/Reader;->close()V

    .line 309
    throw v5
.end method


# virtual methods
.method public createComponents(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 221
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->createComponents(Ljava/lang/String;Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    move-result-object v0

    return-object v0
.end method

.method public createComponents(Ljava/lang/String;Ljava/io/Reader;Ljava/lang/String;)Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;
    .locals 7
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 194
    if-nez p2, :cond_0

    .line 195
    new-instance p2, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringReader;

    .end local p2    # "reader":Ljava/io/Reader;
    invoke-direct {p2, p3}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringReader;-><init>(Ljava/lang/String;)V

    .line 197
    .restart local p2    # "reader":Ljava/io/Reader;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->NON_WORD_PATTERN:Ljava/util/regex/Pattern;

    if-ne v2, v3, :cond_1

    .line 198
    new-instance v2, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;

    const/4 v4, 0x1

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v3, p2, v4, v5, v6}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;-><init>(Ljava/io/Reader;ZZLorg/apache/lucene/analysis/util/CharArraySet;)V

    invoke-direct {v2, v3}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;)V

    .line 205
    :goto_0
    return-object v2

    .line 199
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->WHITESPACE_PATTERN:Ljava/util/regex/Pattern;

    if-ne v2, v3, :cond_2

    .line 200
    new-instance v2, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    new-instance v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    iget-object v6, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v3, p2, v4, v5, v6}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$FastStringTokenizer;-><init>(Ljava/io/Reader;ZZLorg/apache/lucene/analysis/util/CharArraySet;)V

    invoke-direct {v2, v3}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;)V

    goto :goto_0

    .line 203
    :cond_2
    new-instance v1, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    invoke-direct {v1, p2, v2, v3}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer$PatternTokenizer;-><init>(Ljava/io/Reader;Ljava/util/regex/Pattern;Z)V

    .line 204
    .local v1, "tokenizer":Lorg/apache/lucene/analysis/Tokenizer;
    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v2, :cond_3

    new-instance v0, Lorg/apache/lucene/analysis/core/StopFilter;

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->matchVersion:Lorg/apache/lucene/util/Version;

    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-direct {v0, v2, v1, v3}, Lorg/apache/lucene/analysis/core/StopFilter;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/analysis/util/CharArraySet;)V

    .line 205
    .local v0, "result":Lorg/apache/lucene/analysis/TokenStream;
    :goto_1
    new-instance v2, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;

    invoke-direct {v2, v1, v0}, Lorg/apache/lucene/analysis/Analyzer$TokenStreamComponents;-><init>(Lorg/apache/lucene/analysis/Tokenizer;Lorg/apache/lucene/analysis/TokenStream;)V

    goto :goto_0

    .end local v0    # "result":Lorg/apache/lucene/analysis/TokenStream;
    :cond_3
    move-object v0, v1

    .line 204
    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233
    if-ne p0, p1, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v1

    .line 234
    :cond_1
    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->DEFAULT_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-ne p0, v3, :cond_2

    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->EXTENDED_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-ne p1, v3, :cond_2

    move v1, v2

    goto :goto_0

    .line 235
    :cond_2
    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->DEFAULT_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-ne p1, v3, :cond_3

    sget-object v3, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->EXTENDED_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-ne p0, v3, :cond_3

    move v1, v2

    goto :goto_0

    .line 237
    :cond_3
    instance-of v3, p1, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 238
    check-cast v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    .line 240
    .local v0, "p2":Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;
    iget-boolean v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    iget-boolean v4, v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    if-ne v3, v4, :cond_4

    .line 241
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    iget-object v4, v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    invoke-static {v3, v4}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->eqPattern(Ljava/util/regex/Pattern;Ljava/util/regex/Pattern;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 242
    iget-object v3, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    iget-object v4, v0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-static {v3, v4}, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->eq(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    .line 239
    goto :goto_0

    .end local v0    # "p2":Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;
    :cond_5
    move v1, v2

    .line 244
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 254
    sget-object v1, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->DEFAULT_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-ne p0, v1, :cond_0

    const v0, -0x489f96f2

    .line 262
    :goto_0
    return v0

    .line 255
    :cond_0
    sget-object v1, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->EXTENDED_ANALYZER:Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;

    if-ne p0, v1, :cond_1

    const v0, 0x4db1f077    # 3.73165792E8f

    goto :goto_0

    .line 257
    :cond_1
    const/4 v0, 0x1

    .line 258
    .local v0, "h":I
    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit8 v0, v1, 0x1f

    .line 259
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2}, Ljava/util/regex/Pattern;->flags()I

    move-result v2

    add-int v0, v1, v2

    .line 260
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->toLowerCase:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x4cf

    :goto_1
    add-int v0, v2, v1

    .line 261
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/analysis/miscellaneous/PatternAnalyzer;->stopWords:Lorg/apache/lucene/analysis/util/CharArraySet;

    invoke-virtual {v1}, Lorg/apache/lucene/analysis/util/CharArraySet;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v2, v1

    .line 262
    goto :goto_0

    .line 260
    :cond_2
    const/16 v1, 0x4d5

    goto :goto_1

    .line 261
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method
