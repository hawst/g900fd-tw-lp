.class public Lorg/apache/lucene/analysis/sv/SwedishLightStemmer;
.super Ljava/lang/Object;
.source "SwedishLightStemmer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public stem([CI)I
    .locals 3
    .param p1, "s"    # [C
    .param p2, "len"    # I

    .prologue
    const/4 v2, 0x4

    .line 67
    if-le p2, v2, :cond_0

    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_0

    .line 68
    add-int/lit8 p2, p2, -0x1

    .line 70
    :cond_0
    const/4 v0, 0x7

    if-le p2, v0, :cond_3

    .line 71
    const-string v0, "elser"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    const-string v0, "heten"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 73
    :cond_1
    add-int/lit8 p2, p2, -0x5

    .line 109
    .end local p2    # "len":I
    :cond_2
    :goto_0
    return p2

    .line 75
    .restart local p2    # "len":I
    :cond_3
    const/4 v0, 0x6

    if-le p2, v0, :cond_5

    .line 76
    const-string v0, "arne"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 77
    const-string v0, "erna"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 78
    const-string v0, "ande"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 79
    const-string v0, "else"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 80
    const-string v0, "aste"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 81
    const-string v0, "orna"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 82
    const-string v0, "aren"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 83
    :cond_4
    add-int/lit8 p2, p2, -0x4

    goto :goto_0

    .line 85
    :cond_5
    const/4 v0, 0x5

    if-le p2, v0, :cond_7

    .line 86
    const-string v0, "are"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 87
    const-string v0, "ast"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 88
    const-string v0, "het"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89
    :cond_6
    add-int/lit8 p2, p2, -0x3

    goto :goto_0

    .line 91
    :cond_7
    if-le p2, v2, :cond_9

    .line 92
    const-string v0, "ar"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 93
    const-string v0, "er"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 94
    const-string v0, "or"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 95
    const-string v0, "en"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 96
    const-string v0, "at"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 97
    const-string v0, "te"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 98
    const-string v0, "et"

    invoke-static {p1, p2, v0}, Lorg/apache/lucene/analysis/util/StemmerUtil;->endsWith([CILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 99
    :cond_8
    add-int/lit8 p2, p2, -0x2

    goto/16 :goto_0

    .line 101
    :cond_9
    const/4 v0, 0x3

    if-le p2, v0, :cond_2

    .line 102
    add-int/lit8 v0, p2, -0x1

    aget-char v0, p1, v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 106
    :sswitch_0
    add-int/lit8 p2, p2, -0x1

    goto/16 :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x61 -> :sswitch_0
        0x65 -> :sswitch_0
        0x6e -> :sswitch_0
        0x74 -> :sswitch_0
    .end sparse-switch
.end method
