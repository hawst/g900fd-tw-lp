.class public Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "NumericPayloadTokenFilterFactory.java"


# instance fields
.field private final payload:F

.field private final typeMatch:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 41
    const-string v0, "payload"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->requireFloat(Ljava/util/Map;Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->payload:F

    .line 42
    const-string v0, "typeMatch"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->require(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->typeMatch:Ljava/lang/String;

    .line 43
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;

    move-result-object v0

    return-object v0
.end method

.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;

    iget v1, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->payload:F

    iget-object v2, p0, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilterFactory;->typeMatch:Ljava/lang/String;

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/lucene/analysis/payloads/NumericPayloadTokenFilter;-><init>(Lorg/apache/lucene/analysis/TokenStream;FLjava/lang/String;)V

    return-object v0
.end method
