.class public Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;
.super Lorg/apache/lucene/analysis/util/TokenFilterFactory;
.source "SynonymFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# instance fields
.field private final delegator:Lorg/apache/lucene/analysis/util/TokenFilterFactory;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;-><init>(Ljava/util/Map;)V

    .line 48
    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->assureMatchVersion()V

    .line 49
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->luceneMatchVersion:Lorg/apache/lucene/util/Version;

    sget-object v1, Lorg/apache/lucene/util/Version;->LUCENE_34:Lorg/apache/lucene/util/Version;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/Version;->onOrAfter(Lorg/apache/lucene/util/Version;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;

    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->getOriginalArgs()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/synonym/FSTSynonymFilterFactory;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->delegator:Lorg/apache/lucene/analysis/util/TokenFilterFactory;

    .line 59
    :goto_0
    return-void

    .line 54
    :cond_0
    const-string v0, "format"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "format"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "solr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must specify luceneMatchVersion >= 3.4 to use alternate synonyms formats"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_1
    new-instance v0, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;

    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p0}, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->getOriginalArgs()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/analysis/synonym/SlowSynonymFilterFactory;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->delegator:Lorg/apache/lucene/analysis/util/TokenFilterFactory;

    goto :goto_0
.end method


# virtual methods
.method public create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 1
    .param p1, "input"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->delegator:Lorg/apache/lucene/analysis/util/TokenFilterFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/analysis/util/TokenFilterFactory;->create(Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v0

    return-object v0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 1
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/analysis/synonym/SynonymFilterFactory;->delegator:Lorg/apache/lucene/analysis/util/TokenFilterFactory;

    check-cast v0, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;

    invoke-interface {v0, p1}, Lorg/apache/lucene/analysis/util/ResourceLoaderAware;->inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V

    .line 69
    return-void
.end method
