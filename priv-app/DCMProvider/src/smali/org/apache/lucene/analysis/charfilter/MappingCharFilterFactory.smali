.class public Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;
.super Lorg/apache/lucene/analysis/util/CharFilterFactory;
.source "MappingCharFilterFactory.java"

# interfaces
.implements Lorg/apache/lucene/analysis/util/MultiTermAwareComponent;
.implements Lorg/apache/lucene/analysis/util/ResourceLoaderAware;


# static fields
.field static p:Ljava/util/regex/Pattern;


# instance fields
.field private final mapping:Ljava/lang/String;

.field protected normMap:Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

.field out:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-string v0, "\"(.*)\"\\s*=>\\s*\"(.*)\"\\s*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->p:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "args":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/analysis/util/CharFilterFactory;-><init>(Ljava/util/Map;)V

    .line 108
    const/16 v0, 0x100

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->out:[C

    .line 56
    const-string v0, "mapping"

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->get(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->mapping:Ljava/lang/String;

    .line 57
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown parameters: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public create(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1, "input"    # Ljava/io/Reader;

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->normMap:Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    if-nez v0, :cond_0

    .end local p1    # "input":Ljava/io/Reader;
    :goto_0
    return-object p1

    .restart local p1    # "input":Ljava/io/Reader;
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;

    iget-object v1, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->normMap:Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilter;-><init>(Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;Ljava/io/Reader;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public getMultiTermComponent()Lorg/apache/lucene/analysis/util/AbstractAnalysisFactory;
    .locals 0

    .prologue
    .line 143
    return-object p0
.end method

.method public inform(Lorg/apache/lucene/analysis/util/ResourceLoader;)V
    .locals 8
    .param p1, "loader"    # Lorg/apache/lucene/analysis/util/ResourceLoader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->mapping:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 66
    const/4 v5, 0x0

    .line 67
    .local v5, "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/io/File;

    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->mapping:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .local v4, "mappingFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 69
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->mapping:Ljava/lang/String;

    invoke-virtual {p0, p1, v6}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 78
    :cond_0
    new-instance v0, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;

    invoke-direct {v0}, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;-><init>()V

    .line 79
    .local v0, "builder":Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;
    invoke-virtual {p0, v5, v0}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->parseRules(Ljava/util/List;Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;)V

    .line 80
    invoke-virtual {v0}, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->build()Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->normMap:Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    .line 81
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->normMap:Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    iget-object v6, v6, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;->map:Lorg/apache/lucene/util/fst/FST;

    if-nez v6, :cond_1

    .line 84
    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->normMap:Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap;

    .line 87
    .end local v0    # "builder":Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;
    .end local v4    # "mappingFile":Ljava/io/File;
    .end local v5    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-void

    .line 71
    .restart local v4    # "mappingFile":Ljava/io/File;
    .restart local v5    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->mapping:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->splitFileNames(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 72
    .local v2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .restart local v5    # "wlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 74
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, p1, v7}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->getLines(Lorg/apache/lucene/analysis/util/ResourceLoader;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 75
    .local v3, "lines":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method protected parseRules(Ljava/util/List;Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;)V
    .locals 5
    .param p2, "builder"    # Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "rules":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 106
    return-void

    .line 100
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 101
    .local v1, "rule":Ljava/lang/String;
    sget-object v3, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->p:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 102
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_1

    .line 103
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid Mapping Rule : ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "], file = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->mapping:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 104
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->parseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->parseString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lorg/apache/lucene/analysis/charfilter/NormalizeCharMap$Builder;->add(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected parseString(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 111
    const/4 v2, 0x0

    .line 112
    .local v2, "readPos":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 113
    .local v1, "len":I
    const/4 v4, 0x0

    .local v4, "writePos":I
    move v5, v4

    .end local v4    # "writePos":I
    .local v5, "writePos":I
    move v3, v2

    .line 114
    .end local v2    # "readPos":I
    .local v3, "readPos":I
    :goto_0
    if-lt v3, v1, :cond_0

    .line 138
    new-instance v6, Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->out:[C

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8, v5}, Ljava/lang/String;-><init>([CII)V

    return-object v6

    .line 115
    :cond_0
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 116
    .local v0, "c":C
    const/16 v6, 0x5c

    if-ne v0, v6, :cond_2

    .line 117
    if-lt v2, v1, :cond_1

    .line 118
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid escaped char in ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 119
    :cond_1
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 120
    sparse-switch v0, :sswitch_data_0

    move v2, v3

    .line 136
    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    :cond_2
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/analysis/charfilter/MappingCharFilterFactory;->out:[C

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "writePos":I
    .restart local v4    # "writePos":I
    aput-char v0, v6, v5

    move v5, v4

    .end local v4    # "writePos":I
    .restart local v5    # "writePos":I
    move v3, v2

    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    goto :goto_0

    .line 121
    :sswitch_0
    const/16 v0, 0x5c

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 122
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_1
    const/16 v0, 0x22

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 123
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_2
    const/16 v0, 0xa

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 124
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_3
    const/16 v0, 0x9

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 125
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_4
    const/16 v0, 0xd

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 126
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_5
    const/16 v0, 0x8

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 127
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_6
    const/16 v0, 0xc

    move v2, v3

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 129
    .end local v2    # "readPos":I
    .restart local v3    # "readPos":I
    :sswitch_7
    add-int/lit8 v6, v3, 0x3

    if-lt v6, v1, :cond_3

    .line 130
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid escaped char in ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 131
    :cond_3
    add-int/lit8 v6, v3, 0x4

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    int-to-char v0, v6

    .line 132
    add-int/lit8 v2, v3, 0x4

    .end local v3    # "readPos":I
    .restart local v2    # "readPos":I
    goto :goto_1

    .line 120
    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_1
        0x5c -> :sswitch_0
        0x62 -> :sswitch_5
        0x66 -> :sswitch_6
        0x6e -> :sswitch_2
        0x72 -> :sswitch_4
        0x74 -> :sswitch_3
        0x75 -> :sswitch_7
    .end sparse-switch
.end method
