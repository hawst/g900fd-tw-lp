.class public Lorg/apache/lucene/analysis/sinks/TokenTypeSinkFilter;
.super Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;
.source "TokenTypeSinkFilter.java"


# instance fields
.field private typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

.field private typeToMatch:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "typeToMatch"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/analysis/sinks/TeeSinkTokenFilter$SinkFilter;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/apache/lucene/analysis/sinks/TokenTypeSinkFilter;->typeToMatch:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/lucene/util/AttributeSource;)Z
    .locals 2
    .param p1, "source"    # Lorg/apache/lucene/util/AttributeSource;

    .prologue
    .line 36
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TokenTypeSinkFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    if-nez v0, :cond_0

    .line 37
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    iput-object v0, p0, Lorg/apache/lucene/analysis/sinks/TokenTypeSinkFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    .line 41
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/analysis/sinks/TokenTypeSinkFilter;->typeToMatch:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/analysis/sinks/TokenTypeSinkFilter;->typeAtt:Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;

    invoke-interface {v1}, Lorg/apache/lucene/analysis/tokenattributes/TypeAttribute;->type()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
