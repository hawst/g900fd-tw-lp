.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;
.super Lorg/apache/lucene/codecs/FieldInfosWriter;
.source "SimpleTextFieldInfosWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final ATT_KEY:Lorg/apache/lucene/util/BytesRef;

.field static final ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

.field static final DOCVALUES:Lorg/apache/lucene/util/BytesRef;

.field static final FIELD_INFOS_EXTENSION:Ljava/lang/String; = "inf"

.field static final INDEXOPTIONS:Lorg/apache/lucene/util/BytesRef;

.field static final ISINDEXED:Lorg/apache/lucene/util/BytesRef;

.field static final NAME:Lorg/apache/lucene/util/BytesRef;

.field static final NORMS:Lorg/apache/lucene/util/BytesRef;

.field static final NORMS_TYPE:Lorg/apache/lucene/util/BytesRef;

.field static final NUMBER:Lorg/apache/lucene/util/BytesRef;

.field static final NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

.field static final NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

.field static final PAYLOADS:Lorg/apache/lucene/util/BytesRef;

.field static final STORETV:Lorg/apache/lucene/util/BytesRef;

.field static final STORETVOFF:Lorg/apache/lucene/util/BytesRef;

.field static final STORETVPOS:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->$assertionsDisabled:Z

    .line 45
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "number of fields "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  name "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    .line 47
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  number "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMBER:Lorg/apache/lucene/util/BytesRef;

    .line 48
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  indexed "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ISINDEXED:Lorg/apache/lucene/util/BytesRef;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  term vectors "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->STORETV:Lorg/apache/lucene/util/BytesRef;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  term vector positions "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->STORETVPOS:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  term vector offsets "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->STORETVOFF:Lorg/apache/lucene/util/BytesRef;

    .line 52
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  payloads "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->PAYLOADS:Lorg/apache/lucene/util/BytesRef;

    .line 53
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  norms "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS:Lorg/apache/lucene/util/BytesRef;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  norms type "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS_TYPE:Lorg/apache/lucene/util/BytesRef;

    .line 55
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  doc values "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->DOCVALUES:Lorg/apache/lucene/util/BytesRef;

    .line 56
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  index options "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->INDEXOPTIONS:Lorg/apache/lucene/util/BytesRef;

    .line 57
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  attributes "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    .line 58
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    key "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    .line 59
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    value "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosWriter;-><init>()V

    return-void
.end method

.method private static getDocValuesType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 141
    if-nez p0, :cond_0

    const-string v0, "false"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 11
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "infos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    const-string v8, ""

    const-string v9, "inf"

    invoke-static {p2, v8, v9}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "fileName":Ljava/lang/String;
    invoke-virtual {p1, v3, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v5

    .line 65
    .local v5, "out":Lorg/apache/lucene/store/IndexOutput;
    new-instance v6, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v6}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 66
    .local v6, "scratch":Lorg/apache/lucene/util/BytesRef;
    const/4 v7, 0x0

    .line 68
    .local v7, "success":Z
    :try_start_0
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 69
    invoke-virtual {p3}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 70
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 72
    invoke-virtual {p3}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-nez v8, :cond_1

    .line 130
    const/4 v7, 0x1

    .line 132
    if-eqz v7, :cond_7

    .line 133
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 138
    :goto_0
    return-void

    .line 72
    :cond_1
    :try_start_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/FieldInfo;

    .line 73
    .local v2, "fi":Lorg/apache/lucene/index/FieldInfo;
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 74
    iget-object v8, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 75
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 77
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMBER:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 78
    iget v8, v2, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 79
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 81
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ISINDEXED:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 82
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 83
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 85
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 86
    sget-boolean v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->$assertionsDisabled:Z

    if-nez v8, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    sget-object v10, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v8, v10}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-gez v8, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    .end local v2    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :catchall_0
    move-exception v8

    .line 132
    if-eqz v7, :cond_6

    .line 133
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 137
    :goto_1
    throw v8

    .line 87
    .restart local v2    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_2
    :try_start_2
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->INDEXOPTIONS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 88
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 89
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 92
    :cond_3
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->STORETV:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 93
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 94
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 96
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->PAYLOADS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 97
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 98
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 100
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 101
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v8, 0x0

    :goto_2
    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 102
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 104
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS_TYPE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 105
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->getDocValuesType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 106
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 108
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->DOCVALUES:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 109
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->getDocValuesType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 110
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 112
    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfo;->attributes()Ljava/util/Map;

    move-result-object v0

    .line 113
    .local v0, "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v0, :cond_5

    const/4 v4, 0x0

    .line 114
    .local v4, "numAtts":I
    :goto_3
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 115
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 116
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 118
    if-lez v4, :cond_0

    .line 119
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 120
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 121
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 122
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 124
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 125
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v5, v8, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 126
    invoke-static {v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    goto :goto_4

    .line 101
    .end local v0    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "numAtts":I
    :cond_4
    const/4 v8, 0x1

    goto :goto_2

    .line 113
    .restart local v0    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    goto :goto_3

    .line 134
    .end local v0    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_6
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 135
    aput-object v5, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 134
    :cond_7
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/io/Closeable;

    const/4 v9, 0x0

    .line 135
    aput-object v5, v8, v9

    invoke-static {v8}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_0
.end method
