.class public abstract Lorg/apache/lucene/codecs/LiveDocsFormat;
.super Ljava/lang/Object;
.source "LiveDocsFormat.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public abstract files(Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract newLiveDocs(I)Lorg/apache/lucene/util/MutableBits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract newLiveDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/MutableBits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract readLiveDocs(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract writeLiveDocs(Lorg/apache/lucene/util/MutableBits;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
