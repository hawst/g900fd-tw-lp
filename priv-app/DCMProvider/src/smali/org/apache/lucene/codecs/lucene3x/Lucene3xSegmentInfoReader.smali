.class public Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;
.super Lorg/apache/lucene/codecs/SegmentInfoReader;
.source "Lucene3xSegmentInfoReader.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoReader;-><init>()V

    return-void
.end method

.method private static addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V
    .locals 1
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    .local p1, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0, p2}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    return-void
.end method

.method public static readLegacyInfos(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IndexInput;I)V
    .locals 10
    .param p0, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "format"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 50
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 51
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v6

    iput v6, p0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    .line 52
    new-instance v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;

    invoke-direct {v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;-><init>()V

    .line 53
    .local v3, "reader":Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .local v2, "i":I
    :goto_0
    if-gtz v2, :cond_0

    .line 93
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 94
    return-void

    .line 54
    :cond_0
    invoke-direct {v3, p1, p3, p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->readLegacySegmentInfo(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v5

    .line 55
    .local v5, "siPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v4, v5, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    .line 57
    .local v4, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_6

    .line 61
    move-object v0, p1

    .line 62
    .local v0, "dir":Lorg/apache/lucene/store/Directory;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    .line 63
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreIsCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 64
    new-instance v1, Lorg/apache/lucene/store/CompoundFileDirectory;

    .line 65
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    .line 66
    const-string v8, "cfx"

    .line 64
    invoke-static {v6, v7, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 66
    sget-object v7, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    .line 64
    invoke-direct {v1, v0, v6, v7, v9}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .end local v0    # "dir":Lorg/apache/lucene/store/Directory;
    .local v1, "dir":Lorg/apache/lucene/store/Directory;
    move-object v0, v1

    .line 74
    .end local v1    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v0    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_1
    :goto_1
    :try_start_0
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->checkCodeVersion(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    if-eq v0, p1, :cond_2

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 83
    :cond_2
    const-string v6, "3.0"

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/SegmentInfo;->setVersion(Ljava/lang/String;)V

    .line 90
    .end local v0    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_3
    invoke-virtual {p0, v5}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 53
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 68
    .restart local v0    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_4
    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 69
    new-instance v1, Lorg/apache/lucene/store/CompoundFileDirectory;

    .line 70
    iget-object v6, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v7, ""

    const-string v8, "cfs"

    .line 69
    invoke-static {v6, v7, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 70
    sget-object v7, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    .line 69
    invoke-direct {v1, v0, v6, v7, v9}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .end local v0    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v1    # "dir":Lorg/apache/lucene/store/Directory;
    move-object v0, v1

    .end local v1    # "dir":Lorg/apache/lucene/store/Directory;
    .restart local v0    # "dir":Lorg/apache/lucene/store/Directory;
    goto :goto_1

    .line 75
    :catchall_0
    move-exception v6

    .line 77
    if-eq v0, p1, :cond_5

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 78
    :cond_5
    throw v6

    .line 84
    .end local v0    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_6
    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v6

    const-string v7, "2.x"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 88
    new-instance v6, Lorg/apache/lucene/index/IndexFormatTooOldException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "segment "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " in resource "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v6
.end method

.method private readLegacySegmentInfo(Lorg/apache/lucene/store/Directory;ILorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .locals 30
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "format"    # I
    .param p3, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    const/16 v3, -0x9

    move/from16 v0, p2

    if-le v0, v3, :cond_0

    .line 128
    new-instance v3, Lorg/apache/lucene/index/IndexFormatTooOldException;

    .line 129
    const/16 v8, -0x9

    const/16 v10, -0xb

    .line 128
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-direct {v3, v0, v1, v8, v10}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v3

    .line 131
    :cond_0
    const/16 v3, -0xb

    move/from16 v0, p2

    if-ge v0, v3, :cond_1

    .line 132
    new-instance v3, Lorg/apache/lucene/index/IndexFormatTooNewException;

    .line 133
    const/16 v8, -0x9

    const/16 v10, -0xb

    .line 132
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-direct {v3, v0, v1, v8, v10}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v3

    .line 136
    :cond_1
    const/16 v3, -0xb

    move/from16 v0, p2

    if-gt v0, v3, :cond_2

    .line 137
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, "version":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v5

    .line 144
    .local v5, "name":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v6

    .line 145
    .local v6, "docCount":I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v14

    .line 147
    .local v14, "delGen":J
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v17

    .line 148
    .local v17, "docStoreOffset":I
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 153
    .local v11, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_4

    .line 154
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v18

    .line 155
    .local v18, "docStoreSegment":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_3

    const/16 v16, 0x1

    .line 156
    .local v16, "docStoreIsCompoundFile":Z
    :goto_1
    sget-object v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_OFFSET_KEY:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v11, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_NAME_KEY:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v11, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_COMPOUND_KEY:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v11, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    :goto_2
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v12

    .line 169
    .local v12, "b":B
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    if-eq v3, v12, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "expected 1 but was: "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " format: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 139
    .end local v4    # "version":Ljava/lang/String;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "docCount":I
    .end local v11    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "b":B
    .end local v14    # "delGen":J
    .end local v16    # "docStoreIsCompoundFile":Z
    .end local v17    # "docStoreOffset":I
    .end local v18    # "docStoreSegment":Ljava/lang/String;
    :cond_2
    const/4 v4, 0x0

    .restart local v4    # "version":Ljava/lang/String;
    goto :goto_0

    .line 155
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v6    # "docCount":I
    .restart local v11    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v14    # "delGen":J
    .restart local v17    # "docStoreOffset":I
    .restart local v18    # "docStoreSegment":Ljava/lang/String;
    :cond_3
    const/16 v16, 0x0

    goto :goto_1

    .line 160
    .end local v18    # "docStoreSegment":Ljava/lang/String;
    :cond_4
    move-object/from16 v18, v5

    .line 161
    .restart local v18    # "docStoreSegment":Ljava/lang/String;
    const/16 v16, 0x0

    .restart local v16    # "docStoreIsCompoundFile":Z
    goto :goto_2

    .line 170
    .restart local v12    # "b":B
    :cond_5
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v27

    .line 172
    .local v27, "numNormGen":I
    const/4 v3, -0x1

    move/from16 v0, v27

    if-ne v0, v3, :cond_7

    .line 173
    const/16 v26, 0x0

    .line 180
    .local v26, "normGen":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :cond_6
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_8

    const/4 v7, 0x1

    .line 182
    .local v7, "isCompoundFile":Z
    :goto_3
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v13

    .line 183
    .local v13, "delCount":I
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    if-le v13, v6, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 175
    .end local v7    # "isCompoundFile":Z
    .end local v13    # "delCount":I
    .end local v26    # "normGen":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Long;>;"
    :cond_7
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 176
    .restart local v26    # "normGen":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Long;>;"
    const/16 v25, 0x0

    .local v25, "j":I
    :goto_4
    move/from16 v0, v25

    move/from16 v1, v27

    if-ge v0, v1, :cond_6

    .line 177
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-interface {v0, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    add-int/lit8 v25, v25, 0x1

    goto :goto_4

    .line 180
    .end local v25    # "j":I
    :cond_8
    const/4 v7, 0x0

    goto :goto_3

    .line 185
    .restart local v7    # "isCompoundFile":Z
    .restart local v13    # "delCount":I
    :cond_9
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_e

    const/16 v21, 0x1

    .line 187
    .local v21, "hasProx":Z
    :goto_5
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v9

    .line 189
    .local v9, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v3, -0xa

    move/from16 v0, p2

    if-gt v0, v3, :cond_a

    .line 191
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    .line 195
    :cond_a
    new-instance v20, Ljava/util/HashSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    .line 196
    .local v20, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v7, :cond_f

    .line 197
    const-string v3, ""

    const-string v8, "cfs"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 207
    :goto_6
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_11

    .line 208
    if-eqz v16, :cond_10

    .line 209
    const-string v3, ""

    const-string v8, "cfx"

    move-object/from16 v0, v18

    invoke-static {v0, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_b
    :goto_7
    if-eqz v26, :cond_d

    .line 227
    sget-object v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_KEY:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v11, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    invoke-interface/range {v26 .. v26}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_c
    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_12

    .line 243
    :cond_d
    new-instance v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 244
    const/4 v8, 0x0

    invoke-static {v11}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v10

    move-object/from16 v3, p1

    .line 243
    invoke-direct/range {v2 .. v10}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 245
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfo;
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 247
    new-instance v24, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v0, v24

    invoke-direct {v0, v2, v13, v14, v15}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    .line 248
    .local v24, "infoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    return-object v24

    .line 185
    .end local v2    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v9    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v20    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v21    # "hasProx":Z
    .end local v24    # "infoPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_e
    const/16 v21, 0x0

    goto :goto_5

    .line 199
    .restart local v9    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v20    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v21    # "hasProx":Z
    :cond_f
    const-string v3, ""

    const-string v8, "fnm"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 200
    const-string v3, ""

    const-string v8, "frq"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 201
    const-string v3, ""

    const-string v8, "prx"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 202
    const-string v3, ""

    const-string v8, "tis"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 203
    const-string v3, ""

    const-string v8, "tii"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 204
    const-string v3, ""

    const-string v8, "nrm"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 211
    :cond_10
    const-string v3, ""

    const-string v8, "fdx"

    move-object/from16 v0, v18

    invoke-static {v0, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 212
    const-string v3, ""

    const-string v8, "fdt"

    move-object/from16 v0, v18

    invoke-static {v0, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 213
    const-string v3, ""

    const-string v8, "tvx"

    move-object/from16 v0, v18

    invoke-static {v0, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 214
    const-string v3, ""

    const-string v8, "tvf"

    move-object/from16 v0, v18

    invoke-static {v0, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 215
    const-string v3, ""

    const-string v8, "tvd"

    move-object/from16 v0, v18

    invoke-static {v0, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 217
    :cond_11
    if-nez v7, :cond_b

    .line 218
    const-string v3, ""

    const-string v8, "fdx"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v3, ""

    const-string v8, "fdt"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 220
    const-string v3, ""

    const-string v8, "tvx"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 221
    const-string v3, ""

    const-string v8, "tvf"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    .line 222
    const-string v3, ""

    const-string v8, "tvd"

    invoke-static {v5, v3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->addIfExists(Lorg/apache/lucene/store/Directory;Ljava/util/Set;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 228
    :cond_12
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    .line 229
    .local v19, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Long;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    .line 230
    .local v22, "gen":J
    const-wide/16 v28, 0x1

    cmp-long v3, v22, v28

    if-ltz v3, :cond_13

    .line 232
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, "s"

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-wide/from16 v0, v22

    invoke-static {v5, v3, v0, v1}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 233
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v10, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_PREFIX:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v11, v3, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 234
    :cond_13
    const-wide/16 v28, -0x1

    cmp-long v3, v22, v28

    if-eqz v3, :cond_c

    .line 238
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v3, :cond_c

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method private readUpgradedSegmentInfo(Ljava/lang/String;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SegmentInfo;
    .locals 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 252
    const-string v3, "Lucene3xSegmentInfo"

    invoke-static {p3, v3, v1, v1}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 255
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v2

    .line 257
    .local v2, "version":Ljava/lang/String;
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v4

    .line 259
    .local v4, "docCount":I
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v9

    .line 261
    .local v9, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    if-ne v3, v5, :cond_0

    .line 263
    .local v5, "isCompoundFile":Z
    :goto_0
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v7

    .line 265
    .local v7, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readStringSet()Ljava/util/Set;

    move-result-object v10

    .line 267
    .local v10, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/lucene/index/SegmentInfo;

    .line 268
    const/4 v6, 0x0

    invoke-static {v9}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v8

    move-object v1, p2

    move-object v3, p1

    .line 267
    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 269
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v0, v10}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V

    .line 270
    return-object v0

    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v5    # "isCompoundFile":Z
    .end local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v10    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    move v5, v1

    .line 261
    goto :goto_0
.end method


# virtual methods
.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentInfo;
    .locals 8
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 99
    const-string v4, ""

    const-string v5, "si"

    invoke-static {p2, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "fileName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 103
    .local v3, "success":Z
    invoke-virtual {p1, v0, p3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 106
    .local v1, "input":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-direct {p0, p2, p1, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->readUpgradedSegmentInfo(Ljava/lang/String;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SegmentInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 107
    .local v2, "si":Lorg/apache/lucene/index/SegmentInfo;
    const/4 v3, 0x1

    .line 110
    if-nez v3, :cond_0

    new-array v4, v7, [Ljava/io/Closeable;

    .line 111
    aput-object v1, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 108
    :goto_0
    return-object v2

    .line 113
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto :goto_0

    .line 109
    .end local v2    # "si":Lorg/apache/lucene/index/SegmentInfo;
    :catchall_0
    move-exception v4

    .line 110
    if-nez v3, :cond_1

    new-array v5, v7, [Ljava/io/Closeable;

    .line 111
    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 115
    :goto_1
    throw v4

    .line 113
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto :goto_1
.end method
