.class final Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
.super Ljava/lang/Object;
.source "SegmentTermEnum.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FORMAT_CURRENT:I = -0x4

.field public static final FORMAT_MINIMUM:I = -0x4

.field public static final FORMAT_VERSION_UTF8_LENGTH_IN_BYTES:I = -0x4


# instance fields
.field fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private first:Z

.field private format:I

.field indexInterval:I

.field indexPointer:J

.field private input:Lorg/apache/lucene/store/IndexInput;

.field private isIndex:Z

.field maxSkipLevels:I

.field newSuffixStart:I

.field position:J

.field private prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

.field private scanBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

.field size:J

.field skipInterval:I

.field private termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

.field termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->$assertionsDisabled:Z

    .line 50
    return-void

    .line 35
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;Z)V
    .locals 7
    .param p1, "i"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "isi"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x4

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    .line 52
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .line 53
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .line 54
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-direct {v1}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .line 56
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    invoke-direct {v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfo;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 59
    iput-boolean v5, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->isIndex:Z

    .line 60
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexPointer:J

    .line 65
    iput-boolean v6, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->first:Z

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    .line 70
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 71
    iput-boolean p3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->isIndex:Z

    .line 72
    iput v6, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->maxSkipLevels:I

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 75
    .local v0, "firstInt":I
    if-ltz v0, :cond_1

    .line 77
    iput v5, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->format:I

    .line 78
    int-to-long v2, v0

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->size:J

    .line 81
    const/16 v1, 0x80

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexInterval:I

    .line 82
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    .line 101
    :cond_0
    return-void

    .line 85
    :cond_1
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->format:I

    .line 88
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->format:I

    if-le v1, v4, :cond_2

    .line 89
    new-instance v1, Lorg/apache/lucene/index/IndexFormatTooOldException;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->format:I

    invoke-direct {v1, v2, v3, v4, v4}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v1

    .line 90
    :cond_2
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->format:I

    if-ge v1, v4, :cond_3

    .line 91
    new-instance v1, Lorg/apache/lucene/index/IndexFormatTooNewException;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->format:I

    invoke-direct {v1, v2, v3, v4, v4}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v1

    .line 93
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->size:J

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexInterval:I

    .line 96
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    .line 97
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->maxSkipLevels:I

    .line 98
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexInterval:I

    if-gtz v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "indexInterval="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexInterval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is negative; must be > 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 99
    :cond_4
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "skipInterval="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is negative; must be > 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method


# virtual methods
.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->clone()Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-result-object v0

    return-object v0
.end method

.method protected clone()Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .locals 4

    .prologue
    .line 105
    const/4 v1, 0x0

    .line 107
    .local v1, "clone":Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    .line 111
    new-instance v2, Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    invoke-direct {v2, v3}, Lorg/apache/lucene/codecs/lucene3x/TermInfo;-><init>(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->clone()Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .line 114
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->clone()Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .line 115
    new-instance v2, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-direct {v2}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .line 117
    return-object v1

    .line 108
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 225
    return-void
.end method

.method public final docFreq()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    return v0
.end method

.method final freqPointer()J
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    return-wide v0
.end method

.method public final next()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x1

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->set(Lorg/apache/lucene/codecs/lucene3x/TermBuffer;)V

    .line 136
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    add-long v2, v0, v4

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->size:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 137
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->reset()V

    .line 139
    const/4 v0, 0x0

    .line 156
    :goto_0
    return v0

    .line 142
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->read(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V

    .line 143
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->newSuffixStart:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->newSuffixStart:I

    .line 145
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 146
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 147
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 149
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    if-lt v0, v1, :cond_1

    .line 150
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    .line 152
    :cond_1
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->isIndex:Z

    if-eqz v0, :cond_2

    .line 153
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexPointer:J

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexPointer:J

    .line 156
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final prev()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->toTerm()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method final proxPointer()J
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    return-wide v0
.end method

.method final scanTo(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->set(Lorg/apache/lucene/index/Term;)V

    .line 167
    const/4 v0, 0x0

    .line 168
    .local v0, "count":I
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->first:Z

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->next()Z

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->first:Z

    .line 173
    add-int/lit8 v0, v0, 0x1

    .line 175
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->scanBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->compareTo(Lorg/apache/lucene/codecs/lucene3x/TermBuffer;)I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->next()Z

    move-result v1

    if-nez v1, :cond_2

    .line 178
    :cond_1
    return v0

    .line 176
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method final seek(JJLorg/apache/lucene/index/Term;Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V
    .locals 3
    .param p1, "pointer"    # J
    .param p3, "p"    # J
    .param p5, "t"    # Lorg/apache/lucene/index/Term;
    .param p6, "ti"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->input:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 123
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0, p5}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->set(Lorg/apache/lucene/index/Term;)V

    .line 125
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prevBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->reset()V

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    invoke-virtual {v0, p6}, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->set(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V

    .line 128
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->first:Z

    .line 129
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final term()Lorg/apache/lucene/index/Term;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termBuffer:Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->toTerm()Lorg/apache/lucene/index/Term;

    move-result-object v0

    return-object v0
.end method

.method final termInfo()Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .locals 2

    .prologue
    .line 195
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfo;-><init>(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V

    return-object v0
.end method

.method final termInfo(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V
    .locals 1
    .param p1, "ti"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->set(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V

    .line 202
    return-void
.end method
