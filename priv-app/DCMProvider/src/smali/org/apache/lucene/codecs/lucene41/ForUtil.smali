.class final Lorg/apache/lucene/codecs/lucene41/ForUtil;
.super Ljava/lang/Object;
.source "ForUtil.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final ALL_VALUES_EQUAL:I = 0x0

.field static final MAX_DATA_SIZE:I

.field static final MAX_ENCODED_SIZE:I = 0x200


# instance fields
.field private final decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

.field private final encodedSizes:[I

.field private final encoders:[Lorg/apache/lucene/util/packed/PackedInts$Encoder;

.field private final iterations:[I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 36
    const-class v6, Lorg/apache/lucene/codecs/lucene41/ForUtil;

    invoke-virtual {v6}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v6

    if-nez v6, :cond_0

    move v6, v7

    :goto_0
    sput-boolean v6, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    .line 57
    const/4 v4, 0x0

    .line 58
    .local v4, "maxDataSize":I
    const/4 v5, 0x0

    .local v5, "version":I
    :goto_1
    if-le v5, v7, :cond_1

    .line 70
    sput v4, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    .line 71
    return-void

    .end local v4    # "maxDataSize":I
    .end local v5    # "version":I
    :cond_0
    move v6, v8

    .line 36
    goto :goto_0

    .line 59
    .restart local v4    # "maxDataSize":I
    .restart local v5    # "version":I
    :cond_1
    invoke-static {}, Lorg/apache/lucene/util/packed/PackedInts$Format;->values()[Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v9

    array-length v10, v9

    move v6, v8

    :goto_2
    if-lt v6, v10, :cond_2

    .line 58
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 59
    :cond_2
    aget-object v2, v9, v6

    .line 60
    .local v2, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    const/4 v0, 0x1

    .local v0, "bpv":I
    :goto_3
    const/16 v11, 0x20

    if-le v0, v11, :cond_3

    .line 59
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 61
    :cond_3
    invoke-virtual {v2, v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->isSupported(I)Z

    move-result v11

    if-nez v11, :cond_4

    .line 60
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 64
    :cond_4
    invoke-static {v2, v5, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getDecoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    move-result-object v1

    .line 65
    .local v1, "decoder":Lorg/apache/lucene/util/packed/PackedInts$Decoder;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->computeIterations(Lorg/apache/lucene/util/packed/PackedInts$Decoder;)I

    move-result v3

    .line 66
    .local v3, "iterations":I
    invoke-interface {v1}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->byteValueCount()I

    move-result v11

    mul-int/2addr v11, v3

    invoke-static {v4, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto :goto_4
.end method

.method constructor <init>(FLorg/apache/lucene/store/DataOutput;)V
    .locals 7
    .param p1, "acceptableOverheadRatio"    # F
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x20

    const/16 v3, 0x21

    const/4 v5, 0x1

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    invoke-virtual {p2, v5}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 101
    new-array v2, v3, [I

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    .line 102
    new-array v2, v3, [Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encoders:[Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    .line 103
    new-array v2, v3, [Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    .line 104
    new-array v2, v3, [I

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->iterations:[I

    .line 106
    const/4 v0, 0x1

    .local v0, "bpv":I
    :goto_0
    if-le v0, v6, :cond_0

    .line 120
    return-void

    .line 108
    :cond_0
    const/16 v2, 0x80

    .line 107
    invoke-static {v2, v0, p1}, Lorg/apache/lucene/util/packed/PackedInts;->fastestFormatAndBits(IIF)Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;

    move-result-object v1

    .line 109
    .local v1, "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v3, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts$Format;->isSupported(I)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 110
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget v2, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    if-le v2, v6, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 111
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    iget-object v3, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v4, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    invoke-static {v3, v5, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSize(Lorg/apache/lucene/util/packed/PackedInts$Format;II)I

    move-result v3

    aput v3, v2, v0

    .line 112
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encoders:[Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    .line 113
    iget-object v3, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v4, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    .line 112
    invoke-static {v3, v5, v4}, Lorg/apache/lucene/util/packed/PackedInts;->getEncoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    move-result-object v3

    aput-object v3, v2, v0

    .line 114
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    .line 115
    iget-object v3, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    iget v4, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    .line 114
    invoke-static {v3, v5, v4}, Lorg/apache/lucene/util/packed/PackedInts;->getDecoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    move-result-object v3

    aput-object v3, v2, v0

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->iterations:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    aget-object v3, v3, v0

    invoke-static {v3}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->computeIterations(Lorg/apache/lucene/util/packed/PackedInts$Decoder;)I

    move-result v3

    aput v3, v2, v0

    .line 118
    iget-object v2, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/PackedInts$Format;->getId()I

    move-result v2

    shl-int/lit8 v2, v2, 0x5

    iget v3, v1, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    add-int/lit8 v3, v3, -0x1

    or-int/2addr v2, v3

    invoke-virtual {p2, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/DataInput;)V
    .locals 8
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x21

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v5

    .line 127
    .local v5, "packedIntsVersion":I
    invoke-static {v5}, Lorg/apache/lucene/util/packed/PackedInts;->checkVersion(I)V

    .line 128
    new-array v6, v7, [I

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    .line 129
    new-array v6, v7, [Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encoders:[Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    .line 130
    new-array v6, v7, [Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    .line 131
    new-array v6, v7, [I

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->iterations:[I

    .line 133
    const/4 v1, 0x1

    .local v1, "bpv":I
    :goto_0
    const/16 v6, 0x20

    if-le v1, v6, :cond_0

    .line 147
    return-void

    .line 134
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 135
    .local v2, "code":I
    ushr-int/lit8 v4, v2, 0x5

    .line 136
    .local v4, "formatId":I
    and-int/lit8 v6, v2, 0x1f

    add-int/lit8 v0, v6, 0x1

    .line 138
    .local v0, "bitsPerValue":I
    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v3

    .line 139
    .local v3, "format":Lorg/apache/lucene/util/packed/PackedInts$Format;
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    invoke-virtual {v3, v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->isSupported(I)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 140
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    invoke-static {v3, v5, v0}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSize(Lorg/apache/lucene/util/packed/PackedInts$Format;II)I

    move-result v7

    aput v7, v6, v1

    .line 141
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encoders:[Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    invoke-static {v3, v5, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getEncoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    move-result-object v7

    aput-object v7, v6, v1

    .line 143
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    invoke-static {v3, v5, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getDecoder(Lorg/apache/lucene/util/packed/PackedInts$Format;II)Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    move-result-object v7

    aput-object v7, v6, v1

    .line 145
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->iterations:[I

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    aget-object v7, v7, v1

    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->computeIterations(Lorg/apache/lucene/util/packed/PackedInts$Decoder;)I

    move-result v7

    aput v7, v6, v1

    .line 133
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static bitsRequired([I)I
    .locals 6
    .param p0, "data"    # [I

    .prologue
    .line 238
    const-wide/16 v2, 0x0

    .line 239
    .local v2, "or":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x80

    if-lt v0, v1, :cond_0

    .line 243
    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v1

    return v1

    .line 240
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    aget v1, p0, v0

    if-gez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 241
    :cond_1
    aget v1, p0, v0

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static computeIterations(Lorg/apache/lucene/util/packed/PackedInts$Decoder;)I
    .locals 2
    .param p0, "decoder"    # Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    .prologue
    .line 78
    const/high16 v0, 0x43000000    # 128.0f

    invoke-interface {p0}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->byteValueCount()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method private static encodedSize(Lorg/apache/lucene/util/packed/PackedInts$Format;II)I
    .locals 4
    .param p0, "format"    # Lorg/apache/lucene/util/packed/PackedInts$Format;
    .param p1, "packedIntsVersion"    # I
    .param p2, "bitsPerValue"    # I

    .prologue
    .line 86
    const/16 v2, 0x80

    invoke-virtual {p0, p1, v2, p2}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v0

    .line 87
    .local v0, "byteCount":J
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v0, v1}, Ljava/lang/AssertionError;-><init>(J)V

    throw v2

    .line 88
    :cond_1
    long-to-int v2, v0

    return v2
.end method

.method private static isAllEqual([I)Z
    .locals 4
    .param p0, "data"    # [I

    .prologue
    const/4 v2, 0x0

    .line 224
    aget v1, p0, v2

    .line 225
    .local v1, "v":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x80

    if-lt v0, v3, :cond_1

    .line 230
    const/4 v2, 0x1

    :cond_0
    return v2

    .line 226
    :cond_1
    aget v3, p0, v0

    if-ne v3, v1, :cond_0

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V
    .locals 9
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "encoded"    # [B
    .param p3, "decoded"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x80

    const/4 v2, 0x0

    .line 187
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v7

    .line 188
    .local v7, "numBits":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/16 v1, 0x20

    if-le v7, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v7}, Ljava/lang/AssertionError;-><init>(I)V

    throw v1

    .line 190
    :cond_0
    if-nez v7, :cond_1

    .line 191
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v8

    .line 192
    .local v8, "value":I
    invoke-static {p3, v2, v3, v8}, Ljava/util/Arrays;->fill([IIII)V

    .line 204
    .end local v8    # "value":I
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    aget v6, v1, v7

    .line 197
    .local v6, "encodedSize":I
    invoke-virtual {p1, p2, v2, v6}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 199
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->decoders:[Lorg/apache/lucene/util/packed/PackedInts$Decoder;

    aget-object v0, v1, v7

    .line 200
    .local v0, "decoder":Lorg/apache/lucene/util/packed/PackedInts$Decoder;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->iterations:[I

    aget v5, v1, v7

    .line 201
    .local v5, "iters":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->byteValueCount()I

    move-result v1

    mul-int/2addr v1, v5

    if-ge v1, v3, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_2
    move-object v1, p2

    move-object v3, p3

    move v4, v2

    .line 203
    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedInts$Decoder;->decode([BI[III)V

    goto :goto_0
.end method

.method skipBlock(Lorg/apache/lucene/store/IndexInput;)V
    .locals 6
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v1

    .line 214
    .local v1, "numBits":I
    if-nez v1, :cond_0

    .line 215
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    .line 221
    :goto_0
    return-void

    .line 218
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-lez v1, :cond_1

    const/16 v2, 0x20

    if-le v1, v2, :cond_2

    :cond_1
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v2

    .line 219
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    aget v0, v2, v1

    .line 220
    .local v0, "encodedSize":I
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    int-to-long v4, v0

    add-long/2addr v2, v4

    invoke-virtual {p1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_0
.end method

.method writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V
    .locals 8
    .param p1, "data"    # [I
    .param p2, "encoded"    # [B
    .param p3, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->isAllEqual([I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    invoke-virtual {p3, v2}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 160
    aget v1, p1, v2

    invoke-virtual {p3, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 176
    :goto_0
    return-void

    .line 164
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->bitsRequired([I)I

    move-result v7

    .line 165
    .local v7, "numBits":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    if-lez v7, :cond_1

    const/16 v1, 0x20

    if-le v7, v1, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v7}, Ljava/lang/AssertionError;-><init>(I)V

    throw v1

    .line 166
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encoders:[Lorg/apache/lucene/util/packed/PackedInts$Encoder;

    aget-object v0, v1, v7

    .line 167
    .local v0, "encoder":Lorg/apache/lucene/util/packed/PackedInts$Encoder;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->iterations:[I

    aget v5, v1, v7

    .line 168
    .local v5, "iters":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->byteValueCount()I

    move-result v1

    mul-int/2addr v1, v5

    const/16 v3, 0x80

    if-ge v1, v3, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 169
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->encodedSizes:[I

    aget v6, v1, v7

    .line 170
    .local v6, "encodedSize":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/ForUtil;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    invoke-interface {v0}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->byteBlockCount()I

    move-result v1

    mul-int/2addr v1, v5

    if-ge v1, v6, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 172
    :cond_4
    int-to-byte v1, v7

    invoke-virtual {p3, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    move-object v1, p1

    move-object v3, p2

    move v4, v2

    .line 174
    invoke-interface/range {v0 .. v5}, Lorg/apache/lucene/util/packed/PackedInts$Encoder;->encode([II[BII)V

    .line 175
    invoke-virtual {p3, p2, v6}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    goto :goto_0
.end method
