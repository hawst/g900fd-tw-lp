.class public Lorg/apache/lucene/codecs/lucene40/Lucene40NormsFormat;
.super Lorg/apache/lucene/codecs/NormsFormat;
.source "Lucene40NormsFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/apache/lucene/codecs/NormsFormat;-><init>()V

    return-void
.end method


# virtual methods
.method public normsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public normsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 59
    const-string v2, "nrm"

    .line 60
    const-string v3, "cfs"

    .line 58
    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->LEGACY_NORM_TYPE_KEY:Ljava/lang/String;

    invoke-direct {v1, p1, v0, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
