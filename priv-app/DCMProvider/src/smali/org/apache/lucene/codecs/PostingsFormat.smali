.class public abstract Lorg/apache/lucene/codecs/PostingsFormat;
.super Ljava/lang/Object;
.source "PostingsFormat.java"

# interfaces
.implements Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;"
    }
.end annotation


# static fields
.field public static final EMPTY:[Lorg/apache/lucene/codecs/PostingsFormat;

.field private static final loader:Lorg/apache/lucene/util/NamedSPILoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/NamedSPILoader",
            "<",
            "Lorg/apache/lucene/codecs/PostingsFormat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/util/NamedSPILoader;

    const-class v1, Lorg/apache/lucene/codecs/PostingsFormat;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/NamedSPILoader;-><init>(Ljava/lang/Class;)V

    .line 44
    sput-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    .line 48
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/codecs/PostingsFormat;

    sput-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->EMPTY:[Lorg/apache/lucene/codecs/PostingsFormat;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p1}, Lorg/apache/lucene/util/NamedSPILoader;->checkServiceName(Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lorg/apache/lucene/codecs/PostingsFormat;->name:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public static availablePostingsFormats()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    sget-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You called PostingsFormat.availablePostingsFormats() before all formats could be initialized. This likely happens if you call it from a PostingsFormat\'s ctor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0}, Lorg/apache/lucene/util/NamedSPILoader;->availableServices()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    sget-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You called PostingsFormat.forName() before all formats could be initialized. This likely happens if you call it from a PostingsFormat\'s ctor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/NamedSPILoader;->lookup(Ljava/lang/String;)Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public static reloadPostingsFormats(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p0, "classloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 124
    sget-object v0, Lorg/apache/lucene/codecs/PostingsFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/NamedSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 125
    return-void
.end method


# virtual methods
.method public abstract fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/codecs/PostingsFormat;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PostingsFormat(name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/codecs/PostingsFormat;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
