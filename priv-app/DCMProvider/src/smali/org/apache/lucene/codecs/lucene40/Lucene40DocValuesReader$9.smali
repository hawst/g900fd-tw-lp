.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesFixedStraight(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final synthetic val$fixedLength:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/PagedBytes$Reader;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    iput p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;->val$fixedLength:I

    .line 320
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 323
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;->val$fixedLength:I

    int-to-long v2, v1

    int-to-long v4, p1

    mul-long/2addr v2, v4

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;->val$fixedLength:I

    invoke-virtual {v0, p2, v2, v3, v1}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 324
    return-void
.end method
