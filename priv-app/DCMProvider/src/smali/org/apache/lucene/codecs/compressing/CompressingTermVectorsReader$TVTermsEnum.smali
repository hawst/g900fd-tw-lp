.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "CompressingTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TVTermsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private in:Lorg/apache/lucene/store/ByteArrayDataInput;

.field private lengths:[I

.field private numTerms:I

.field private ord:I

.field private payloadIndex:[I

.field private payloads:Lorg/apache/lucene/util/BytesRef;

.field private positionIndex:[I

.field private positions:[I

.field private prefixLengths:[I

.field private startOffsets:[I

.field private startPos:I

.field private suffixLengths:[I

.field private final term:Lorg/apache/lucene/util/BytesRef;

.field private termFreqs:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 763
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 771
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 772
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 773
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;)V
    .locals 0

    .prologue
    .line 771
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;-><init>()V

    return-void
.end method


# virtual methods
.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 875
    const/4 v0, 0x1

    return v0
.end method

.method public final docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 9
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 886
    if-eqz p2, :cond_0

    instance-of v1, p2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 887
    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;

    .line 892
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->termFreqs:[I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v2, v1, v2

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->positionIndex:[I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v3, v1, v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->positions:[I

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->startOffsets:[I

    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->lengths:[I

    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->payloads:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->payloadIndex:[I

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->reset(Lorg/apache/lucene/util/Bits;II[I[I[ILorg/apache/lucene/util/BytesRef;[I)V

    .line 893
    return-object v0

    .line 889
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;-><init>()V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;
    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 898
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->positions:[I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->startOffsets:[I

    if-nez v0, :cond_0

    .line 899
    const/4 v0, 0x0

    .line 902
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 820
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 800
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->numTerms:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 801
    const/4 v0, 0x0

    .line 815
    :goto_0
    return-object v0

    .line 803
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->numTerms:I

    if-lt v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 804
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    .line 808
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 809
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->prefixLengths:[I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->suffixLengths:[I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v2, v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 810
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_2

    .line 811
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 813
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->in:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->prefixLengths:[I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v2, v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->suffixLengths:[I

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v3, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readBytes([BII)V

    .line 815
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 870
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    int-to-long v0, v0

    return-wide v0
.end method

.method reset()V
    .locals 2

    .prologue
    .line 793
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 794
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->in:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->startPos:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->setPosition(I)V

    .line 795
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    .line 796
    return-void
.end method

.method reset(II[I[I[I[I[I[I[I[ILorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/store/ByteArrayDataInput;)V
    .locals 1
    .param p1, "numTerms"    # I
    .param p2, "flags"    # I
    .param p3, "prefixLengths"    # [I
    .param p4, "suffixLengths"    # [I
    .param p5, "termFreqs"    # [I
    .param p6, "positionIndex"    # [I
    .param p7, "positions"    # [I
    .param p8, "startOffsets"    # [I
    .param p9, "lengths"    # [I
    .param p10, "payloadIndex"    # [I
    .param p11, "payloads"    # Lorg/apache/lucene/util/BytesRef;
    .param p12, "in"    # Lorg/apache/lucene/store/ByteArrayDataInput;

    .prologue
    .line 777
    iput p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->numTerms:I

    .line 778
    iput-object p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->prefixLengths:[I

    .line 779
    iput-object p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->suffixLengths:[I

    .line 780
    iput-object p5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->termFreqs:[I

    .line 781
    iput-object p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->positionIndex:[I

    .line 782
    iput-object p7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->positions:[I

    .line 783
    iput-object p8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->startOffsets:[I

    .line 784
    iput-object p9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->lengths:[I

    .line 785
    iput-object p10, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->payloadIndex:[I

    .line 786
    iput-object p11, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->payloads:Lorg/apache/lucene/util/BytesRef;

    .line 787
    iput-object p12, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->in:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 788
    invoke-virtual {p12}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->startPos:I

    .line 789
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->reset()V

    .line 790
    return-void
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 4
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 826
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->numTerms:I

    if-ge v2, v3, :cond_1

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    if-ltz v2, :cond_1

    .line 827
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 828
    .local v0, "cmp":I
    if-nez v0, :cond_0

    .line 829
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 844
    .end local v0    # "cmp":I
    :goto_0
    return-object v2

    .line 830
    .restart local v0    # "cmp":I
    :cond_0
    if-lez v0, :cond_1

    .line 831
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->reset()V

    .line 836
    .end local v0    # "cmp":I
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 837
    .local v1, "term":Lorg/apache/lucene/util/BytesRef;
    if-nez v1, :cond_2

    .line 838
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 840
    :cond_2
    invoke-virtual {v1, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 841
    .restart local v0    # "cmp":I
    if-lez v0, :cond_3

    .line 842
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 843
    :cond_3
    if-nez v0, :cond_1

    .line 844
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 5
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 851
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->numTerms:I

    int-to-long v2, v1

    cmp-long v1, p1, v2

    if-ltz v1, :cond_1

    .line 852
    :cond_0
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ord is out of range: ord="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", numTerms="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->numTerms:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 854
    :cond_1
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    int-to-long v2, v1

    cmp-long v1, p1, v2

    if-gez v1, :cond_2

    .line 855
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->reset()V

    .line 857
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    .local v0, "i":I
    :goto_0
    int-to-long v2, v0

    cmp-long v1, v2, p1

    if-ltz v1, :cond_3

    .line 860
    sget-boolean v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 858
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    .line 857
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 861
    :cond_4
    return-void
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 865
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 880
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->termFreqs:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->ord:I

    aget v0, v0, v1

    int-to-long v0, v0

    return-wide v0
.end method
