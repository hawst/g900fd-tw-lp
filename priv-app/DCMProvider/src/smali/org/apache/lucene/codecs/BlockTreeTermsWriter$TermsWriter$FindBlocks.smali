.class Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;
.super Lorg/apache/lucene/util/fst/Builder$FreezeTail;
.source "BlockTreeTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FindBlocks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/fst/Builder$FreezeTail",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/util/fst/Builder$FreezeTail;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;)V
    .locals 0

    .prologue
    .line 510
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;)V

    return-void
.end method


# virtual methods
.method public freeze([Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;ILorg/apache/lucene/util/IntsRef;)V
    .locals 10
    .param p1, "frontier"    # [Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;
    .param p2, "prefixLenPlus1"    # I
    .param p3, "lastInput"    # Lorg/apache/lucene/util/IntsRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode",
            "<",
            "Ljava/lang/Object;",
            ">;I",
            "Lorg/apache/lucene/util/IntsRef;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x1

    .line 517
    iget v1, p3, Lorg/apache/lucene/util/IntsRef;->length:I

    .local v1, "idx":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 552
    return-void

    .line 518
    :cond_0
    aget-object v2, p1, v1

    .line 520
    .local v2, "node":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<Ljava/lang/Object;>;"
    const-wide/16 v4, 0x0

    .line 522
    .local v4, "totCount":J
    iget-boolean v6, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->isFinal:Z

    if-eqz v6, :cond_1

    .line 523
    add-long/2addr v4, v8

    .line 526
    :cond_1
    const/4 v0, 0x0

    .local v0, "arcIdx":I
    :goto_1
    iget v6, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    if-lt v0, v6, :cond_3

    .line 532
    const/4 v6, 0x0

    iput v6, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->numArcs:I

    .line 534
    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
    invoke-static {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;)Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    move-result-object v6

    iget v6, v6, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->minItemsInBlock:I

    int-to-long v6, v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_2

    if-nez v1, :cond_4

    .line 544
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;

    long-to-int v7, v4

    invoke-virtual {v6, p3, v1, v7}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->writeBlocks(Lorg/apache/lucene/util/IntsRef;II)V

    .line 545
    iput-wide v8, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    .line 550
    :goto_2
    new-instance v6, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->blockBuilder:Lorg/apache/lucene/util/fst/Builder;
    invoke-static {v7}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;)Lorg/apache/lucene/util/fst/Builder;

    move-result-object v7

    invoke-direct {v6, v7, v1}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;-><init>(Lorg/apache/lucene/util/fst/Builder;I)V

    aput-object v6, p1, v1

    .line 517
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 527
    :cond_3
    iget-object v6, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v6, v6, v0

    iget-object v3, v6, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    check-cast v3, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;

    .line 528
    .local v3, "target":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<Ljava/lang/Object;>;"
    iget-wide v6, v3, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    add-long/2addr v4, v6

    .line 529
    invoke-virtual {v3}, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->clear()V

    .line 530
    iget-object v6, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->arcs:[Lorg/apache/lucene/util/fst/Builder$Arc;

    aget-object v6, v6, v0

    const/4 v7, 0x0

    iput-object v7, v6, Lorg/apache/lucene/util/fst/Builder$Arc;->target:Lorg/apache/lucene/util/fst/Builder$Node;

    .line 526
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 548
    .end local v3    # "target":Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;, "Lorg/apache/lucene/util/fst/Builder$UnCompiledNode<Ljava/lang/Object;>;"
    :cond_4
    iput-wide v4, v2, Lorg/apache/lucene/util/fst/Builder$UnCompiledNode;->inputCount:J

    goto :goto_2
.end method
