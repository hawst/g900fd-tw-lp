.class Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;
.super Ljava/lang/Object;
.source "TermInfosReaderIndex.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final MAX_PAGE_BITS:I = 0x12


# instance fields
.field private comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

.field private fields:[Lorg/apache/lucene/index/Term;

.field private final indexSize:I

.field private final indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final skipInterval:I

.field private totalIndexInterval:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;IJI)V
    .locals 21
    .param p1, "indexEnum"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "indexDivisor"    # I
    .param p3, "tiiFileLength"    # J
    .param p5, "totalIndexInterval"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->comparator:Ljava/util/Comparator;

    .line 67
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->totalIndexInterval:I

    .line 68
    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->size:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-int v15, v0

    add-int/lit8 v15, v15, -0x1

    div-int v15, v15, p2

    add-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexSize:I

    .line 69
    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->skipInterval:I

    .line 71
    move-wide/from16 v0, p3

    long-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x3ff8000000000000L    # 1.5

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-long v0, v0

    move-wide/from16 v16, v0

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v18, v0

    div-long v10, v16, v18

    .line 72
    .local v10, "initialSize":J
    new-instance v5, Lorg/apache/lucene/util/PagedBytes;

    invoke-static {v10, v11}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->estimatePageBits(J)I

    move-result v15

    invoke-direct {v5, v15}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 73
    .local v5, "dataPagedBytes":Lorg/apache/lucene/util/PagedBytes;
    invoke-virtual {v5}, Lorg/apache/lucene/util/PagedBytes;->getDataOutput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;

    move-result-object v4

    .line 75
    .local v4, "dataOutput":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;
    const/4 v15, 0x2

    move-wide/from16 v0, p3

    invoke-static {v0, v1, v15}, Lorg/apache/lucene/util/MathUtil;->log(JI)I

    move-result v15

    add-int/lit8 v2, v15, 0x1

    .line 76
    .local v2, "bitEstimate":I
    new-instance v9, Lorg/apache/lucene/util/packed/GrowableWriter;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexSize:I

    const v16, 0x3e4ccccd    # 0.2f

    move/from16 v0, v16

    invoke-direct {v9, v2, v15, v0}, Lorg/apache/lucene/util/packed/GrowableWriter;-><init>(IIF)V

    .line 78
    .local v9, "indexToTerms":Lorg/apache/lucene/util/packed/GrowableWriter;
    const/4 v3, 0x0

    .line 79
    .local v3, "currentField":Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v7, "fieldStrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, -0x1

    .line 81
    .local v6, "fieldCounter":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->next()Z

    move-result v15

    if-nez v15, :cond_0

    .line 106
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v15

    new-array v15, v15, [Lorg/apache/lucene/index/Term;

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    .line 107
    const/4 v8, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    array-length v15, v15

    if-lt v8, v15, :cond_6

    .line 111
    const/4 v15, 0x1

    invoke-virtual {v5, v15}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 112
    invoke-virtual {v5}, Lorg/apache/lucene/util/PagedBytes;->getDataInput()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    .line 113
    invoke-virtual {v9}, Lorg/apache/lucene/util/packed/GrowableWriter;->getMutable()Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 114
    return-void

    .line 82
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v13

    .line 83
    .local v13, "term":Lorg/apache/lucene/index/Term;
    if-eqz v3, :cond_1

    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 84
    :cond_1
    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    .line 85
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v6, v6, 0x1

    .line 88
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo()Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v14

    .line 89
    .local v14, "termInfo":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    invoke-virtual {v4}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->getPosition()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v9, v8, v0, v1}, Lorg/apache/lucene/util/packed/GrowableWriter;->set(IJ)V

    .line 90
    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVInt(I)V

    .line 91
    invoke-virtual {v13}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeString(Ljava/lang/String;)V

    .line 92
    iget v15, v14, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    invoke-virtual {v4, v15}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVInt(I)V

    .line 93
    iget v15, v14, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->skipInterval:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-lt v15, v0, :cond_3

    .line 94
    iget v15, v14, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    invoke-virtual {v4, v15}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVInt(I)V

    .line 96
    :cond_3
    iget-wide v0, v14, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVLong(J)V

    .line 97
    iget-wide v0, v14, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVLong(J)V

    .line 98
    move-object/from16 v0, p1

    iget-wide v0, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexPointer:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataOutput;->writeVLong(J)V

    .line 99
    const/4 v12, 0x1

    .local v12, "j":I
    :goto_2
    move/from16 v0, p2

    if-lt v12, v0, :cond_5

    .line 81
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 100
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->next()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 99
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 108
    .end local v12    # "j":I
    .end local v13    # "term":Lorg/apache/lucene/index/Term;
    .end local v14    # "termInfo":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    move-object/from16 v16, v0

    new-instance v17, Lorg/apache/lucene/index/Term;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v15}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    aput-object v17, v16, v8

    .line 107
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1
.end method

.method private compareField(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termIndex"    # I
    .param p3, "input"    # Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v0, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    invoke-virtual {p3, v0, v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 254
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    invoke-virtual {p3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private compareTo(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;Lorg/apache/lucene/util/BytesRef;)I
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termIndex"    # I
    .param p3, "input"    # Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    .param p4, "reuse"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->compareField(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;)I

    move-result v0

    .line 231
    .local v0, "c":I
    if-nez v0, :cond_0

    .line 232
    invoke-virtual {p3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    iput v1, p4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 233
    iget v1, p4, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p4, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 234
    iget-object v1, p4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v2, 0x0

    iget v3, p4, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p3, v1, v2, v3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readBytes([BII)V

    .line 235
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->comparator:Ljava/util/Comparator;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-interface {v1, v2, p4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 237
    .end local v0    # "c":I
    :cond_0
    return v0
.end method

.method private static estimatePageBits(J)I
    .locals 2
    .param p0, "estSize"    # J

    .prologue
    .line 117
    invoke-static {p0, p1}, Ljava/lang/Long;->numberOfLeadingZeros(J)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x40

    const/16 v1, 0x12

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method compareTo(Lorg/apache/lucene/index/Term;I)I
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "termIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v0

    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->compareTo(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    return v0
.end method

.method getIndexOffset(Lorg/apache/lucene/index/Term;)I
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    const/4 v3, 0x0

    .line 156
    .local v3, "lo":I
    iget v6, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexSize:I

    add-int/lit8 v1, v6, -0x1

    .line 157
    .local v1, "hi":I
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v6}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v2

    .line 158
    .local v2, "input":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 159
    .local v5, "scratch":Lorg/apache/lucene/util/BytesRef;
    :goto_0
    if-ge v1, v3, :cond_1

    move v4, v1

    .line 169
    :cond_0
    return v4

    .line 160
    :cond_1
    add-int v6, v3, v1

    ushr-int/lit8 v4, v6, 0x1

    .line 161
    .local v4, "mid":I
    invoke-direct {p0, p1, v4, v2, v5}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->compareTo(Lorg/apache/lucene/index/Term;ILorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 162
    .local v0, "delta":I
    if-gez v0, :cond_2

    .line 163
    add-int/lit8 v1, v4, -0x1

    goto :goto_0

    .line 164
    :cond_2
    if-lez v0, :cond_0

    .line 165
    add-int/lit8 v3, v4, 0x1

    goto :goto_0
.end method

.method getTerm(I)Lorg/apache/lucene/index/Term;
    .locals 6
    .param p1, "termIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v2

    .line 182
    .local v2, "input":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v3, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 185
    invoke-virtual {v2}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    .line 186
    .local v1, "fieldId":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    aget-object v0, v3, v1

    .line 187
    .local v0, "field":Lorg/apache/lucene/index/Term;
    new-instance v3, Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method length()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexSize:I

    return v0
.end method

.method seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;I)V
    .locals 12
    .param p1, "enumerator"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "indexOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->dataInput:Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->clone()Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;

    move-result-object v9

    .line 123
    .local v9, "input":Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->indexToDataOffset:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v1, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-virtual {v9, v4, v5}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->setPosition(J)V

    .line 126
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v8

    .line 127
    .local v8, "fieldId":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->fields:[Lorg/apache/lucene/index/Term;

    aget-object v0, v1, v8

    .line 128
    .local v0, "field":Lorg/apache/lucene/index/Term;
    new-instance v6, Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v1, v4}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .local v6, "term":Lorg/apache/lucene/index/Term;
    new-instance v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    invoke-direct {v7}, Lorg/apache/lucene/codecs/lucene3x/TermInfo;-><init>()V

    .line 132
    .local v7, "termInfo":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    iput v1, v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 133
    iget v1, v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->skipInterval:I

    if-lt v1, v4, :cond_0

    .line 134
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVInt()I

    move-result v1

    iput v1, v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    .line 138
    :goto_0
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVLong()J

    move-result-wide v4

    iput-wide v4, v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 139
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVLong()J

    move-result-wide v4

    iput-wide v4, v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 141
    invoke-virtual {v9}, Lorg/apache/lucene/util/PagedBytes$PagedBytesDataInput;->readVLong()J

    move-result-wide v2

    .line 144
    .local v2, "pointer":J
    int-to-long v4, p2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->totalIndexInterval:I

    int-to-long v10, v1

    mul-long/2addr v4, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v4, v10

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->seek(JJLorg/apache/lucene/index/Term;Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V

    .line 145
    return-void

    .line 136
    .end local v2    # "pointer":J
    :cond_0
    const/4 v1, 0x0

    iput v1, v7, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    goto :goto_0
.end method
