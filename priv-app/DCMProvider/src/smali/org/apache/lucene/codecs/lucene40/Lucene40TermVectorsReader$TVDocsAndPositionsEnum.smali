.class Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "Lucene40TermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TVDocsAndPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private didNext:Z

.field private doc:I

.field private endOffsets:[I

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private nextPos:I

.field private payload:Lorg/apache/lucene/util/BytesRef;

.field private payloadBytes:[B

.field private payloadOffsets:[I

.field private positions:[I

.field private startOffsets:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 625
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 625
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 627
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->doc:I

    .line 634
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 625
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;)V
    .locals 0

    .prologue
    .line 625
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;-><init>()V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 664
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 729
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 649
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 2

    .prologue
    .line 720
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->endOffsets:[I

    if-nez v0, :cond_0

    .line 721
    const/4 v0, -0x1

    .line 723
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->endOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 639
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    array-length v0, v0

    .line 643
    :goto_0
    return v0

    .line 642
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 643
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    array-length v0, v0

    goto :goto_0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 681
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadOffsets:[I

    if-nez v3, :cond_1

    .line 692
    :cond_0
    :goto_0
    return-object v2

    .line 684
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadOffsets:[I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v4, v4, -0x1

    aget v1, v3, v4

    .line 685
    .local v1, "off":I
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadOffsets:[I

    array-length v4, v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadBytes:[B

    array-length v0, v3

    .line 686
    .local v0, "end":I
    :goto_1
    sub-int v3, v0, v1

    if-eqz v3, :cond_0

    .line 689
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadBytes:[B

    iput-object v3, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 690
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iput v1, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 691
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    sub-int v3, v0, v1

    iput v3, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 692
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 685
    .end local v0    # "end":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadOffsets:[I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    aget v0, v3, v4

    goto :goto_1
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 654
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->didNext:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 655
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->didNext:Z

    .line 656
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->doc:I

    .line 658
    :goto_0
    return v0

    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->doc:I

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 3

    .prologue
    .line 698
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 699
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 701
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    if-eqz v0, :cond_3

    .line 702
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    aget v0, v0, v1

    .line 705
    :goto_0
    return v0

    .line 704
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    .line 705
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;[I[I[I[I[B)V
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "positions"    # [I
    .param p3, "startOffsets"    # [I
    .param p4, "endOffsets"    # [I
    .param p5, "payloadLengths"    # [I
    .param p6, "payloadBytes"    # [B

    .prologue
    const/4 v1, 0x0

    .line 668
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 669
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->positions:[I

    .line 670
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    .line 671
    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->endOffsets:[I

    .line 672
    iput-object p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadOffsets:[I

    .line 673
    iput-object p6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->payloadBytes:[B

    .line 674
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->doc:I

    .line 675
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->didNext:Z

    .line 676
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    .line 677
    return-void
.end method

.method public startOffset()I
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    if-nez v0, :cond_0

    .line 712
    const/4 v0, -0x1

    .line 714
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->startOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method
