.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;
.super Lorg/apache/lucene/codecs/SegmentInfoFormat;
.source "SimpleTextSegmentInfoFormat.java"


# static fields
.field public static final SI_EXTENSION:Ljava/lang/String; = "si"


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

.field private final writer:Lorg/apache/lucene/codecs/SegmentInfoWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoFormat;-><init>()V

    .line 31
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;->reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

    .line 32
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;->writer:Lorg/apache/lucene/codecs/SegmentInfoWriter;

    .line 30
    return-void
.end method


# virtual methods
.method public getSegmentInfoReader()Lorg/apache/lucene/codecs/SegmentInfoReader;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;->reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

    return-object v0
.end method

.method public getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;->writer:Lorg/apache/lucene/codecs/SegmentInfoWriter;

    return-object v0
.end method
