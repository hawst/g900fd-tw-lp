.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;
.super Lorg/apache/lucene/index/Fields;
.source "Lucene3xTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVFields"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final fieldFPs:[J

.field private final fieldNumberToIndex:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldNumbers:[I

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 200
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;I)V
    .locals 9
    .param p2, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 205
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Fields;-><init>()V

    .line 203
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumberToIndex:Ljava/util/Map;

    .line 206
    invoke-virtual {p1, p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->seekTvx(I)V

    .line 207
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 209
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 210
    .local v0, "fieldCount":I
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-gez v0, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 211
    :cond_0
    if-eqz v0, :cond_3

    .line 212
    new-array v3, v0, [I

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I

    .line 213
    new-array v3, v0, [J

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldFPs:[J

    .line 214
    const/4 v2, 0x0

    .local v2, "fieldUpto":I
    :goto_0
    if-lt v2, v0, :cond_1

    .line 220
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    .line 221
    .local v4, "position":J
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldFPs:[J

    const/4 v6, 0x0

    aput-wide v4, v3, v6

    .line 222
    const/4 v2, 0x1

    :goto_1
    if-lt v2, v0, :cond_2

    .line 233
    .end local v2    # "fieldUpto":I
    .end local v4    # "position":J
    :goto_2
    return-void

    .line 215
    .restart local v2    # "fieldUpto":I
    :cond_1
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 216
    .local v1, "fieldNumber":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I

    aput v1, v3, v2

    .line 217
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumberToIndex:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 223
    .end local v1    # "fieldNumber":I
    .restart local v4    # "position":J
    :cond_2
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 224
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldFPs:[J

    aput-wide v4, v3, v2

    .line 222
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 230
    .end local v2    # "fieldUpto":I
    .end local v4    # "position":J
    :cond_3
    iput-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I

    .line 231
    iput-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldFPs:[J

    goto :goto_2
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)[I
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    return-object v0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 283
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I

    array-length v0, v0

    goto :goto_0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 6
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 263
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$2(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 264
    .local v1, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    if-nez v1, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-object v2

    .line 269
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumberToIndex:Ljava/util/Map;

    iget v4, v1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 270
    .local v0, "fieldIndex":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 275
    new-instance v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldFPs:[J

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aget-wide v4, v4, v5

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;J)V

    goto :goto_0
.end method
