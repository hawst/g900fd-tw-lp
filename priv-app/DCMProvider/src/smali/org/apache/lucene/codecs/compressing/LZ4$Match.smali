.class Lorg/apache/lucene/codecs/compressing/LZ4$Match;
.super Ljava/lang/Object;
.source "LZ4.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/LZ4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Match"
.end annotation


# instance fields
.field len:I

.field ref:I

.field start:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;-><init>()V

    return-void
.end method


# virtual methods
.method end()I
    .locals 2

    .prologue
    .line 265
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v0, v1

    return v0
.end method

.method fix(I)V
    .locals 1
    .param p1, "correction"    # I

    .prologue
    .line 259
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    .line 260
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    .line 261
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 262
    return-void
.end method
