.class final enum Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
.super Ljava/lang/Enum;
.source "Lucene40FieldInfosReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "LegacyDocValuesType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BYTES_FIXED_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum BYTES_FIXED_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum BYTES_FIXED_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum BYTES_VAR_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum BYTES_VAR_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum BYTES_VAR_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum FIXED_INTS_16:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum FIXED_INTS_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum FIXED_INTS_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum FIXED_INTS_8:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum FLOAT_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum FLOAT_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum NONE:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

.field public static final enum VAR_INTS:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;


# instance fields
.field final mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 130
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->NONE:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 131
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "VAR_INTS"

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->VAR_INTS:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 132
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "FLOAT_32"

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v6, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FLOAT_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 133
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "FLOAT_64"

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v7, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FLOAT_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 134
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "BYTES_FIXED_STRAIGHT"

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 135
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "BYTES_FIXED_DEREF"

    const/4 v2, 0x5

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 136
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "BYTES_VAR_STRAIGHT"

    const/4 v2, 0x6

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 137
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "BYTES_VAR_DEREF"

    const/4 v2, 0x7

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 138
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "FIXED_INTS_16"

    const/16 v2, 0x8

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_16:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 139
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "FIXED_INTS_32"

    const/16 v2, 0x9

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 140
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "FIXED_INTS_64"

    const/16 v2, 0xa

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 141
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "FIXED_INTS_8"

    const/16 v2, 0xb

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_8:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 142
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "BYTES_FIXED_SORTED"

    const/16 v2, 0xc

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 143
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    const-string v1, "BYTES_VAR_SORTED"

    const/16 v2, 0xd

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;-><init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    .line 129
    const/16 v0, 0xe

    new-array v0, v0, [Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->NONE:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->VAR_INTS:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FLOAT_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FLOAT_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v1, v0, v7

    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_16:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_8:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ENUM$VALUES:[Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)V
    .locals 0
    .param p3, "mapping"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 147
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 148
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ENUM$VALUES:[Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
