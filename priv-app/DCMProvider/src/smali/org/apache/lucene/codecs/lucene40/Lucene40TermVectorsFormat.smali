.class public Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsFormat;
.super Lorg/apache/lucene/codecs/TermVectorsFormat;
.source "Lucene40TermVectorsFormat.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsFormat;-><init>()V

    .line 120
    return-void
.end method


# virtual methods
.method public vectorsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 1
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-direct {v0, p1, p2, p3, p4}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method

.method public vectorsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsWriter;
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;

    iget-object v1, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method
