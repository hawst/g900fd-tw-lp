.class public abstract Lorg/apache/lucene/codecs/TermVectorsReader;
.super Ljava/lang/Object;
.source "TermVectorsReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;->clone()Lorg/apache/lucene/codecs/TermVectorsReader;

    move-result-object v0

    return-object v0
.end method

.method public abstract clone()Lorg/apache/lucene/codecs/TermVectorsReader;
.end method

.method public abstract get(I)Lorg/apache/lucene/index/Fields;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
