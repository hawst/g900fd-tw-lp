.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;
.super Lorg/apache/lucene/index/Terms;
.source "SimpleTextFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTextTerms"
.end annotation


# instance fields
.field private docCount:I

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field private fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<",
            "Ljava/lang/Long;",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final scratch:Lorg/apache/lucene/util/BytesRef;

.field private final scratchUTF16:Lorg/apache/lucene/util/CharsRef;

.field private sumDocFreq:J

.field private sumTotalTermFreq:J

.field private termCount:I

.field private final termsStart:J

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;Ljava/lang/String;J)V
    .locals 3
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "termsStart"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0xa

    .line 509
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 506
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 507
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 510
    iput-wide p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->termsStart:J

    .line 511
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 512
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->loadTerms()V

    .line 513
    return-void
.end method

.method private loadTerms()V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 516
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->getSingleton(Z)Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    move-result-object v14

    .line 518
    .local v14, "posIntOutputs":Lorg/apache/lucene/util/fst/PositiveIntOutputs;
    new-instance v13, Lorg/apache/lucene/util/fst/PairOutputs;

    invoke-direct {v13, v14, v14}, Lorg/apache/lucene/util/fst/PairOutputs;-><init>(Lorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 519
    .local v13, "outputsInner":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<Ljava/lang/Long;Ljava/lang/Long;>;"
    new-instance v12, Lorg/apache/lucene/util/fst/PairOutputs;

    invoke-direct {v12, v14, v13}, Lorg/apache/lucene/util/fst/PairOutputs;-><init>(Lorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 521
    .local v12, "outputs":Lorg/apache/lucene/util/fst/PairOutputs;, "Lorg/apache/lucene/util/fst/PairOutputs<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    new-instance v4, Lorg/apache/lucene/util/fst/Builder;

    sget-object v19, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    move-object/from16 v0, v19

    invoke-direct {v4, v0, v12}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 522
    .local v4, "b":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    move-object/from16 v19, v0

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    .line 523
    .local v7, "in":Lorg/apache/lucene/store/IndexInput;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->termsStart:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v7, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 524
    new-instance v10, Lorg/apache/lucene/util/BytesRef;

    const/16 v19, 0xa

    move/from16 v0, v19

    invoke-direct {v10, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 525
    .local v10, "lastTerm":Lorg/apache/lucene/util/BytesRef;
    const-wide/16 v8, -0x1

    .line 526
    .local v8, "lastDocsStart":J
    const/4 v5, 0x0

    .line 527
    .local v5, "docFreq":I
    const-wide/16 v16, 0x0

    .line 528
    .local v16, "totalTermFreq":J
    new-instance v18, Lorg/apache/lucene/util/OpenBitSet;

    invoke-direct/range {v18 .. v18}, Lorg/apache/lucene/util/OpenBitSet;-><init>()V

    .line 529
    .local v18, "visitedDocs":Lorg/apache/lucene/util/OpenBitSet;
    new-instance v15, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v15}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 531
    .local v15, "scratchIntsRef":Lorg/apache/lucene/util/IntsRef;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v7, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual/range {v19 .. v20}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 533
    :cond_1
    const-wide/16 v20, -0x1

    cmp-long v19, v8, v20

    if-eqz v19, :cond_2

    .line 534
    invoke-static {v10, v15}, Lorg/apache/lucene/util/fst/Util;->toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v19

    .line 535
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 536
    int-to-long v0, v5

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v21

    .line 535
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v20

    .line 534
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 537
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumTotalTermFreq:J

    move-wide/from16 v20, v0

    add-long v20, v20, v16

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumTotalTermFreq:J

    .line 567
    :cond_2
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->docCount:I

    .line 568
    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 576
    return-void

    .line 540
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 541
    add-int/lit8 v5, v5, 0x1

    .line 542
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumDocFreq:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumDocFreq:J

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v20, v0

    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v22, v0

    invoke-static/range {v19 .. v22}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 544
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/CharsRef;->length:I

    move/from16 v21, v0

    invoke-static/range {v19 .. v21}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v6

    .line 545
    .local v6, "docID":I
    int-to-long v0, v6

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    goto/16 :goto_0

    .line 546
    .end local v6    # "docID":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 547
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v20, v0

    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v22, v0

    invoke-static/range {v19 .. v22}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 548
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/CharsRef;->length:I

    move/from16 v21, v0

    invoke-static/range {v19 .. v21}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v16, v16, v20

    .line 549
    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->TERM:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v19 .. v20}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 550
    const-wide/16 v20, -0x1

    cmp-long v19, v8, v20

    if-eqz v19, :cond_6

    .line 551
    invoke-static {v10, v15}, Lorg/apache/lucene/util/fst/Util;->toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v19

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 552
    int-to-long v0, v5

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v21

    .line 551
    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Lorg/apache/lucene/util/fst/PairOutputs;->newPair(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 554
    :cond_6
    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->TERM:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v20, v0

    sub-int v11, v19, v20

    .line 556
    .local v11, "len":I
    iget v0, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-le v11, v0, :cond_7

    .line 557
    invoke-virtual {v10, v11}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 559
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v19, v0

    sget-object v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->TERM:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v20, v0

    iget-object v0, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    move/from16 v3, v22

    invoke-static {v0, v1, v2, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 560
    iput v11, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 561
    const/4 v5, 0x0

    .line 562
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumTotalTermFreq:J

    move-wide/from16 v20, v0

    add-long v20, v20, v16

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumTotalTermFreq:J

    .line 563
    const-wide/16 v16, 0x0

    .line 564
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->termCount:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->termCount:I

    goto/16 :goto_0
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 589
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->docCount:I

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 604
    iget-wide v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumDocFreq:J

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2

    .prologue
    .line 599
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v1, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->sumTotalTermFreq:J

    goto :goto_0
.end method

.method public hasOffsets()Z
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    return v0
.end method

.method public hasPositions()Z
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 4
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 580
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fst:Lorg/apache/lucene/util/fst/FST;

    if-eqz v0, :cond_0

    .line 581
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 583
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    goto :goto_0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 594
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;->termCount:I

    int-to-long v0, v0

    return-wide v0
.end method
