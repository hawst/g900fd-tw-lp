.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;
.super Lorg/apache/lucene/codecs/FieldsProducer;
.source "SimpleTextFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$TermData;
    }
.end annotation


# static fields
.field static final DOC:Lorg/apache/lucene/util/BytesRef;

.field static final END:Lorg/apache/lucene/util/BytesRef;

.field static final END_OFFSET:Lorg/apache/lucene/util/BytesRef;

.field static final FIELD:Lorg/apache/lucene/util/BytesRef;

.field static final FREQ:Lorg/apache/lucene/util/BytesRef;

.field static final PAYLOAD:Lorg/apache/lucene/util/BytesRef;

.field static final POS:Lorg/apache/lucene/util/BytesRef;

.field static final START_OFFSET:Lorg/apache/lucene/util/BytesRef;

.field static final TERM:Lorg/apache/lucene/util/BytesRef;


# instance fields
.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fields:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private final termsCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Terms;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END:Lorg/apache/lucene/util/BytesRef;

    .line 60
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    .line 61
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->TERM:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->TERM:Lorg/apache/lucene/util/BytesRef;

    .line 62
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    .line 63
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->FREQ:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    .line 64
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->POS:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->POS:Lorg/apache/lucene/util/BytesRef;

    .line 65
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    .line 66
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    .line 67
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentReadState;)V
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 69
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldsProducer;-><init>()V

    .line 633
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->termsCache:Ljava/util/Map;

    .line 70
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 71
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v2, v3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextPostingsFormat;->getPostingsFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 72
    const/4 v0, 0x0

    .line 74
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->readFields(Lorg/apache/lucene/store/IndexInput;)Ljava/util/TreeMap;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->fields:Ljava/util/TreeMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    const/4 v0, 0x1

    .line 77
    if-nez v0, :cond_0

    new-array v1, v5, [Ljava/io/Closeable;

    .line 78
    aput-object p0, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 81
    :cond_0
    return-void

    .line 76
    :catchall_0
    move-exception v1

    .line 77
    if-nez v0, :cond_1

    new-array v2, v5, [Ljava/io/Closeable;

    .line 78
    aput-object p0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 80
    :cond_1
    throw v1
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method private readFields(Lorg/apache/lucene/store/IndexInput;)Ljava/util/TreeMap;
    .locals 7
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/IndexInput;",
            ")",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 85
    .local v2, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 88
    .local v1, "fields":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_0
    :goto_0
    invoke-static {p1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 89
    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 90
    return-object v1

    .line 91
    :cond_1
    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2, v3}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    new-instance v0, Ljava/lang/String;

    iget-object v3, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v4, v5

    iget v5, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v5, v6

    const-string v6, "UTF-8"

    invoke-direct {v0, v3, v4, v5, v6}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 93
    .local v0, "fieldName":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 657
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 658
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 630
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 652
    const/4 v0, -0x1

    return v0
.end method

.method public declared-synchronized terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 4
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 637
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->termsCache:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Terms;

    .line 638
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-nez v1, :cond_1

    .line 639
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v2, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 640
    .local v0, "fp":Ljava/lang/Long;
    if-nez v0, :cond_0

    .line 641
    const/4 v2, 0x0

    .line 647
    .end local v0    # "fp":Ljava/lang/Long;
    :goto_0
    monitor-exit p0

    return-object v2

    .line 643
    .restart local v0    # "fp":Ljava/lang/Long;
    :cond_0
    :try_start_1
    new-instance v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;

    .end local v1    # "terms":Lorg/apache/lucene/index/Terms;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, p0, p1, v2, v3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTerms;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;Ljava/lang/String;J)V

    .line 644
    .restart local v1    # "terms":Lorg/apache/lucene/index/Terms;
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->termsCache:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "fp":Ljava/lang/Long;
    :cond_1
    move-object v2, v1

    .line 647
    goto :goto_0

    .line 637
    .end local v1    # "terms":Lorg/apache/lucene/index/Terms;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
