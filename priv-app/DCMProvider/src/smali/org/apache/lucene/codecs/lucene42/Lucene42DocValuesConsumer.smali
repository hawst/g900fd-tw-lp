.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;
.super Lorg/apache/lucene/codecs/DocValuesConsumer;
.source "Lucene42DocValuesConsumer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;
    }
.end annotation


# static fields
.field static final BLOCK_SIZE:I = 0x1000

.field static final BYTES:B = 0x1t

.field static final DELTA_COMPRESSED:B = 0x0t

.field static final FST:B = 0x2t

.field static final NUMBER:B = 0x0t

.field static final TABLE_COMPRESSED:B = 0x1t

.field static final UNCOMPRESSED:B = 0x2t

.field static final VERSION_CURRENT:I

.field static final VERSION_START:I


# instance fields
.field final acceptableOverheadRatio:F

.field final data:Lorg/apache/lucene/store/IndexOutput;

.field final maxDoc:I

.field final meta:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 7
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "dataCodec"    # Ljava/lang/String;
    .param p3, "dataExtension"    # Ljava/lang/String;
    .param p4, "metaCodec"    # Ljava/lang/String;
    .param p5, "metaExtension"    # Ljava/lang/String;
    .param p6, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 68
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer;-><init>()V

    .line 69
    iput p6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->acceptableOverheadRatio:F

    .line 70
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->maxDoc:I

    .line 71
    const/4 v2, 0x0

    .line 73
    .local v2, "success":Z
    :try_start_0
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v3, v4, p3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "dataName":Ljava/lang/String;
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v3, v0, v4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    .line 75
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    const/4 v4, 0x0

    invoke-static {v3, p2, v4}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 76
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v3, v4, p5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "metaName":Ljava/lang/String;
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v3, v1, v4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    .line 78
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    const/4 v4, 0x0

    invoke-static {v3, p4, v4}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    const/4 v2, 0x1

    .line 81
    if-nez v2, :cond_0

    new-array v3, v6, [Ljava/io/Closeable;

    .line 82
    aput-object p0, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 85
    :cond_0
    return-void

    .line 80
    .end local v0    # "dataName":Ljava/lang/String;
    .end local v1    # "metaName":Ljava/lang/String;
    :catchall_0
    move-exception v3

    .line 81
    if-nez v2, :cond_1

    new-array v4, v6, [Ljava/io/Closeable;

    .line 82
    aput-object p0, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 84
    :cond_1
    throw v3
.end method

.method private writeFST(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 10
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    iget v8, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 205
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 206
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 207
    const/4 v7, 0x1

    invoke-static {v7}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->getSingleton(Z)Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    move-result-object v4

    .line 208
    .local v4, "outputs":Lorg/apache/lucene/util/fst/PositiveIntOutputs;
    new-instance v0, Lorg/apache/lucene/util/fst/Builder;

    sget-object v7, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    invoke-direct {v0, v7, v4}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 209
    .local v0, "builder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Ljava/lang/Long;>;"
    new-instance v5, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 210
    .local v5, "scratch":Lorg/apache/lucene/util/IntsRef;
    const-wide/16 v2, 0x0

    .line 211
    .local v2, "ord":J
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 215
    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    move-result-object v1

    .line 216
    .local v1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    if-eqz v1, :cond_0

    .line 217
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v7}, Lorg/apache/lucene/util/fst/FST;->save(Lorg/apache/lucene/store/DataOutput;)V

    .line 219
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v7, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 220
    return-void

    .line 211
    .end local v1    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/BytesRef;

    .line 212
    .local v6, "v":Lorg/apache/lucene/util/BytesRef;
    invoke-static {v6, v5}, Lorg/apache/lucene/util/fst/Util;->toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 213
    const-wide/16 v8, 0x1

    add-long/2addr v2, v8

    goto :goto_0
.end method


# virtual methods
.method public addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 13
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    iget v9, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 173
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 174
    const v3, 0x7fffffff

    .line 175
    .local v3, "minLength":I
    const/high16 v2, -0x80000000

    .line 176
    .local v2, "maxLength":I
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 177
    .local v4, "startFP":J
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 182
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 183
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v10

    sub-long/2addr v10, v4

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 184
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 185
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 189
    if-eq v3, v2, :cond_0

    .line 190
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 191
    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    const/16 v9, 0x1000

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 193
    new-instance v7, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    const/16 v9, 0x1000

    invoke-direct {v7, v8, v9}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;-><init>(Lorg/apache/lucene/store/DataOutput;I)V

    .line 194
    .local v7, "writer":Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;
    const-wide/16 v0, 0x0

    .line 195
    .local v0, "addr":J
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    .line 199
    invoke-virtual {v7}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->finish()V

    .line 201
    .end local v0    # "addr":J
    .end local v7    # "writer":Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;
    :cond_0
    return-void

    .line 177
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/BytesRef;

    .line 178
    .local v6, "v":Lorg/apache/lucene/util/BytesRef;
    iget v9, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v3, v9}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 179
    iget v9, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v9}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 180
    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    iget-object v10, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v11, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v12, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v9, v10, v11, v12}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    goto :goto_0

    .line 195
    .end local v6    # "v":Lorg/apache/lucene/util/BytesRef;
    .restart local v0    # "addr":J
    .restart local v7    # "writer":Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;
    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/BytesRef;

    .line 196
    .restart local v6    # "v":Lorg/apache/lucene/util/BytesRef;
    iget v9, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v10, v9

    add-long/2addr v0, v10

    .line 197
    invoke-virtual {v7, v0, v1}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedWriter;->add(J)V

    goto :goto_1
.end method

.method public addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 23
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 92
    const-wide v12, 0x7fffffffffffffffL

    .line 93
    .local v12, "minValue":J
    const-wide/high16 v10, -0x8000000000000000L

    .line 95
    .local v10, "maxValue":J
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 96
    .local v14, "uniqueValues":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_1

    .line 109
    if-eqz v14, :cond_6

    .line 111
    invoke-virtual {v14}, Ljava/util/HashSet;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v4

    .line 112
    .local v4, "bitsPerValue":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->maxDoc:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->acceptableOverheadRatio:F

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v4, v1}, Lorg/apache/lucene/util/packed/PackedInts;->fastestFormatAndBits(IIF)Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;

    move-result-object v7

    .line 113
    .local v7, "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    iget v0, v7, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    move/from16 v18, v0

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    const-wide/16 v18, -0x80

    cmp-long v18, v12, v18

    if-ltz v18, :cond_3

    const-wide/16 v18, 0x7f

    cmp-long v18, v10, v18

    if-gtz v18, :cond_3

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 115
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_2

    .line 150
    .end local v4    # "bitsPerValue":I
    .end local v7    # "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    :goto_2
    return-void

    .line 96
    :cond_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    .line 97
    .local v9, "nv":Ljava/lang/Number;
    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v16

    .line 98
    .local v16, "v":J
    move-wide/from16 v0, v16

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    .line 99
    move-wide/from16 v0, v16

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    .line 100
    if-eqz v14, :cond_0

    .line 101
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 102
    invoke-virtual {v14}, Ljava/util/HashSet;->size()I

    move-result v19

    const/16 v20, 0x100

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_0

    .line 103
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 115
    .end local v9    # "nv":Ljava/lang/Number;
    .end local v16    # "v":J
    .restart local v4    # "bitsPerValue":I
    .restart local v7    # "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    :cond_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    .line 116
    .restart local v9    # "nv":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v19, v0

    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-byte v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    goto :goto_1

    .line 119
    .end local v9    # "nv":Ljava/lang/Number;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 120
    invoke-virtual {v14}, Ljava/util/HashSet;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Long;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Long;

    .line 121
    .local v5, "decode":[Ljava/lang/Long;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 122
    .local v6, "encode":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    array-length v0, v5

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 123
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    array-length v0, v5

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v8, v0, :cond_4

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    iget-object v0, v7, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/util/packed/PackedInts$Format;->getId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    iget v0, v7, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    iget-object v0, v7, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->format:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->maxDoc:I

    move/from16 v20, v0

    iget v0, v7, Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;->bitsPerValue:I

    move/from16 v21, v0

    const/16 v22, 0x400

    invoke-static/range {v18 .. v22}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v15

    .line 133
    .local v15, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_5

    .line 136
    invoke-virtual {v15}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    goto/16 :goto_2

    .line 124
    .end local v15    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    aget-object v19, v5, v8

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 125
    aget-object v18, v5, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 133
    .restart local v15    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    .line 134
    .restart local v9    # "nv":Ljava/lang/Number;
    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v15, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    goto :goto_4

    .line 139
    .end local v4    # "bitsPerValue":I
    .end local v5    # "decode":[Ljava/lang/Long;
    .end local v6    # "encode":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .end local v7    # "formatAndBits":Lorg/apache/lucene/util/packed/PackedInts$FormatAndBits;
    .end local v8    # "i":I
    .end local v9    # "nv":Ljava/lang/Number;
    .end local v15    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x1000

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 144
    new-instance v15, Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    const/16 v19, 0x1000

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v15, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedWriter;-><init>(Lorg/apache/lucene/store/DataOutput;I)V

    .line 145
    .local v15, "writer":Lorg/apache/lucene/util/packed/BlockPackedWriter;
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-nez v19, :cond_7

    .line 148
    invoke-virtual {v15}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    goto/16 :goto_2

    .line 145
    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Number;

    .line 146
    .restart local v9    # "nv":Ljava/lang/Number;
    invoke-virtual {v9}, Ljava/lang/Number;->longValue()J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v15, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    goto :goto_5
.end method

.method public addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 0
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrd":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    invoke-virtual {p0, p1, p3}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 228
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->writeFST(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 229
    return-void
.end method

.method public addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrdCount":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    .local p4, "ords":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$1;

    invoke-direct {v0, p0, p3, p4}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$1;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 243
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->writeFST(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 244
    return-void
.end method

.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    const/4 v0, 0x0

    .line 156
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_0
    const/4 v0, 0x1

    .line 161
    if-eqz v0, :cond_2

    new-array v1, v3, [Ljava/io/Closeable;

    .line 162
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v5

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 167
    :goto_0
    return-void

    .line 160
    :catchall_0
    move-exception v1

    .line 161
    if-eqz v0, :cond_1

    new-array v2, v3, [Ljava/io/Closeable;

    .line 162
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v5

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 166
    :goto_1
    throw v1

    .line 163
    :cond_1
    new-array v2, v3, [Ljava/io/Closeable;

    .line 164
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v5

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 163
    :cond_2
    new-array v1, v3, [Ljava/io/Closeable;

    .line 164
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;->meta:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v5

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method
