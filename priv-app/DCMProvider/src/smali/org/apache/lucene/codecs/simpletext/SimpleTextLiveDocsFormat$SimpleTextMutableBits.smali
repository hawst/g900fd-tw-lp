.class Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;
.super Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;
.source "SimpleTextLiveDocsFormat.java"

# interfaces
.implements Lorg/apache/lucene/util/MutableBits;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SimpleTextMutableBits"
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 172
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0, p1}, Ljava/util/BitSet;-><init>(I)V

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;-><init>(Ljava/util/BitSet;I)V

    .line 173
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;->bits:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/BitSet;->set(II)V

    .line 174
    return-void
.end method

.method constructor <init>(Ljava/util/BitSet;I)V
    .locals 0
    .param p1, "bits"    # Ljava/util/BitSet;
    .param p2, "size"    # I

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;-><init>(Ljava/util/BitSet;I)V

    .line 178
    return-void
.end method


# virtual methods
.method public clear(I)V
    .locals 1
    .param p1, "bit"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;->bits:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->clear(I)V

    .line 183
    return-void
.end method
