.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesVarSorted(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SortedDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$addressReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final synthetic val$ordsReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final synthetic val$valueCount:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;I)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$ordsReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$addressReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    iput p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$valueCount:I

    .line 570
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 573
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$ordsReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getValueCount()I
    .locals 1

    .prologue
    .line 585
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$valueCount:I

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 8
    .param p1, "ord"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 578
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$addressReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v4, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    .line 579
    .local v2, "startAddress":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$addressReader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    add-int/lit8 v5, p1, 0x1

    invoke-interface {v4, v5}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    .line 580
    .local v0, "endAddress":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    sub-long v6, v0, v2

    long-to-int v5, v6

    invoke-virtual {v4, p2, v2, v3, v5}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 581
    return-void
.end method
