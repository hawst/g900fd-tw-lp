.class Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;
.super Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;
.source "TermInfosReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CloneableTerm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;"
    }
.end annotation


# instance fields
.field term:Lorg/apache/lucene/index/Term;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "t"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;-><init>()V

    .line 72
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    .line 73
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;->clone()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 77
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    .line 78
    .local v0, "t":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;->term:Lorg/apache/lucene/index/Term;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->hashCode()I

    move-result v0

    return v0
.end method
