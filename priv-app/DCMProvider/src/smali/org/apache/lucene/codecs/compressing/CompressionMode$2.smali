.class Lorg/apache/lucene/codecs/compressing/CompressionMode$2;
.super Lorg/apache/lucene/codecs/compressing/CompressionMode;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressionMode;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public newCompressor()Lorg/apache/lucene/codecs/compressing/Compressor;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;-><init>(I)V

    return-object v0
.end method

.method public newDecompressor()Lorg/apache/lucene/codecs/compressing/Decompressor;
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;-><init>()V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "HIGH_COMPRESSION"

    return-object v0
.end method
