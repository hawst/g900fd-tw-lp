.class public final Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
.super Ljava/lang/Object;
.source "CompressingStoredFieldsIndexReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# instance fields
.field final avgChunkDocs:[I

.field final avgChunkSizes:[J

.field final docBases:[I

.field final docBasesDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field final fieldsIndexIn:Lorg/apache/lucene/store/IndexInput;

.field final maxDoc:I

.field final startPointers:[J

.field final startPointersDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;)V
    .locals 1
    .param p1, "other"    # Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->fieldsIndexIn:Lorg/apache/lucene/store/IndexInput;

    .line 111
    iget v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->maxDoc:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->maxDoc:I

    .line 112
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBases:[I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBases:[I

    .line 113
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointers:[J

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointers:[J

    .line 114
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkDocs:[I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkDocs:[I

    .line 115
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkSizes:[J

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkSizes:[J

    .line 116
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBasesDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBasesDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 117
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointersDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointersDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 118
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/SegmentInfo;)V
    .locals 14
    .param p1, "fieldsIndexIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->fieldsIndexIn:Lorg/apache/lucene/store/IndexInput;

    .line 53
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v12

    iput v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->maxDoc:I

    .line 54
    const/16 v12, 0x10

    new-array v5, v12, [I

    .line 55
    .local v5, "docBases":[I
    const/16 v12, 0x10

    new-array v10, v12, [J

    .line 56
    .local v10, "startPointers":[J
    const/16 v12, 0x10

    new-array v0, v12, [I

    .line 57
    .local v0, "avgChunkDocs":[I
    const/16 v12, 0x10

    new-array v1, v12, [J

    .line 58
    .local v1, "avgChunkSizes":[J
    const/16 v12, 0x10

    new-array v6, v12, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 59
    .local v6, "docBasesDeltas":[Lorg/apache/lucene/util/packed/PackedInts$Reader;
    const/16 v12, 0x10

    new-array v11, v12, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 61
    .local v11, "startPointersDeltas":[Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    .line 63
    .local v9, "packedIntsVersion":I
    const/4 v4, 0x0

    .line 66
    .local v4, "blockCount":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v8

    .line 67
    .local v8, "numChunks":I
    if-nez v8, :cond_0

    .line 101
    invoke-static {v5, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v12

    iput-object v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBases:[I

    .line 102
    invoke-static {v10, v4}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v12

    iput-object v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointers:[J

    .line 103
    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v12

    iput-object v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkDocs:[I

    .line 104
    invoke-static {v1, v4}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v12

    iput-object v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkSizes:[J

    .line 105
    invoke-static {v6, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBasesDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 106
    invoke-static {v11, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object v12, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointersDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 107
    return-void

    .line 70
    :cond_0
    array-length v12, v5

    if-ne v4, v12, :cond_1

    .line 71
    add-int/lit8 v12, v4, 0x1

    const/16 v13, 0x8

    invoke-static {v12, v13}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v7

    .line 72
    .local v7, "newSize":I
    invoke-static {v5, v7}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v5

    .line 73
    invoke-static {v10, v7}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v10

    .line 74
    invoke-static {v0, v7}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    .line 75
    invoke-static {v1, v7}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    .line 76
    invoke-static {v6, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "docBasesDeltas":[Lorg/apache/lucene/util/packed/PackedInts$Reader;
    check-cast v6, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 77
    .restart local v6    # "docBasesDeltas":[Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-static {v11, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "startPointersDeltas":[Lorg/apache/lucene/util/packed/PackedInts$Reader;
    check-cast v11, [Lorg/apache/lucene/util/packed/PackedInts$Reader;

    .line 81
    .end local v7    # "newSize":I
    .restart local v11    # "startPointersDeltas":[Lorg/apache/lucene/util/packed/PackedInts$Reader;
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v12

    aput v12, v5, v4

    .line 82
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v12

    aput v12, v0, v4

    .line 83
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 84
    .local v2, "bitsPerDocBase":I
    const/16 v12, 0x20

    if-le v2, v12, :cond_2

    .line 85
    new-instance v12, Lorg/apache/lucene/index/CorruptIndexException;

    const-string v13, "Corrupted"

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 87
    :cond_2
    sget-object v12, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-static {p1, v12, v9, v8, v2}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v12

    aput-object v12, v6, v4

    .line 90
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v12

    aput-wide v12, v10, v4

    .line 91
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v12

    aput-wide v12, v1, v4

    .line 92
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    .line 93
    .local v3, "bitsPerStartPointer":I
    const/16 v12, 0x40

    if-le v3, v12, :cond_3

    .line 94
    new-instance v12, Lorg/apache/lucene/index/CorruptIndexException;

    const-string v13, "Corrupted"

    invoke-direct {v12, v13}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 96
    :cond_3
    sget-object v12, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-static {p1, v12, v9, v8, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v12

    aput-object v12, v11, v4

    .line 98
    add-int/lit8 v4, v4, 0x1

    .line 65
    goto/16 :goto_0
.end method

.method private block(I)I
    .locals 5
    .param p1, "docID"    # I

    .prologue
    .line 121
    const/4 v1, 0x0

    .local v1, "lo":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBases:[I

    array-length v4, v4

    add-int/lit8 v0, v4, -0x1

    .line 122
    .local v0, "hi":I
    :goto_0
    if-le v1, v0, :cond_1

    move v2, v0

    .line 133
    :cond_0
    return v2

    .line 123
    :cond_1
    add-int v4, v1, v0

    ushr-int/lit8 v2, v4, 0x1

    .line 124
    .local v2, "mid":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBases:[I

    aget v3, v4, v2

    .line 125
    .local v3, "midValue":I
    if-eq v3, p1, :cond_0

    .line 127
    if-ge v3, p1, :cond_2

    .line 128
    add-int/lit8 v1, v2, 0x1

    .line 129
    goto :goto_0

    .line 130
    :cond_2
    add-int/lit8 v0, v2, -0x1

    goto :goto_0
.end method

.method static moveLowOrderBitToSign(J)J
    .locals 4
    .param p0, "n"    # J

    .prologue
    .line 40
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method private relativeChunk(II)I
    .locals 5
    .param p1, "block"    # I
    .param p2, "relativeDoc"    # I

    .prologue
    .line 149
    const/4 v1, 0x0

    .local v1, "lo":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBasesDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v4, v4, p1

    invoke-interface {v4}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 150
    .local v0, "hi":I
    :goto_0
    if-le v1, v0, :cond_1

    move v2, v0

    .line 161
    :cond_0
    return v2

    .line 151
    :cond_1
    add-int v4, v1, v0

    ushr-int/lit8 v2, v4, 0x1

    .line 152
    .local v2, "mid":I
    invoke-direct {p0, p1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->relativeDocBase(II)I

    move-result v3

    .line 153
    .local v3, "midValue":I
    if-eq v3, p2, :cond_0

    .line 155
    if-ge v3, p2, :cond_2

    .line 156
    add-int/lit8 v1, v2, 0x1

    .line 157
    goto :goto_0

    .line 158
    :cond_2
    add-int/lit8 v0, v2, -0x1

    goto :goto_0
.end method

.method private relativeDocBase(II)I
    .locals 6
    .param p1, "block"    # I
    .param p2, "relativeChunk"    # I

    .prologue
    .line 137
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkDocs:[I

    aget v3, v3, p1

    mul-int v2, v3, p2

    .line 138
    .local v2, "expected":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBasesDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v3, v3, p1

    invoke-interface {v3, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->moveLowOrderBitToSign(J)J

    move-result-wide v0

    .line 139
    .local v0, "delta":J
    long-to-int v3, v0

    add-int/2addr v3, v2

    return v3
.end method

.method private relativeStartPointer(II)J
    .locals 8
    .param p1, "block"    # I
    .param p2, "relativeChunk"    # I

    .prologue
    .line 143
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->avgChunkSizes:[J

    aget-wide v4, v4, p1

    int-to-long v6, p2

    mul-long v2, v4, v6

    .line 144
    .local v2, "expected":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointersDeltas:[Lorg/apache/lucene/util/packed/PackedInts$Reader;

    aget-object v4, v4, p1

    invoke-interface {v4, p2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->moveLowOrderBitToSign(J)J

    move-result-wide v0

    .line 145
    .local v0, "delta":J
    add-long v4, v2, v0

    return-wide v4
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->clone()Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->fieldsIndexIn:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 178
    .end local p0    # "this":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 184
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->fieldsIndexIn:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method getStartPointer(I)J
    .locals 6
    .param p1, "docID"    # I

    .prologue
    .line 165
    if-ltz p1, :cond_0

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->maxDoc:I

    if-lt p1, v2, :cond_1

    .line 166
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "docID out of range [0-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->maxDoc:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 168
    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->block(I)I

    move-result v0

    .line 169
    .local v0, "block":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->docBases:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-direct {p0, v0, v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->relativeChunk(II)I

    move-result v1

    .line 170
    .local v1, "relativeChunk":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->startPointers:[J

    aget-wide v2, v2, v0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->relativeStartPointer(II)J

    move-result-wide v4

    add-long/2addr v2, v4

    return-wide v2
.end method
