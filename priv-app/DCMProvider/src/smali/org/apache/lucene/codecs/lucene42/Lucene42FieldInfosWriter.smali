.class final Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;
.super Lorg/apache/lucene/codecs/FieldInfosWriter;
.source "Lucene42FieldInfosWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosWriter;-><init>()V

    .line 44
    return-void
.end method

.method private static docValuesByte(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)B
    .locals 1
    .param p0, "type"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 94
    if-nez p0, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    .line 96
    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne p0, v0, :cond_1

    .line 97
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    :cond_1
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne p0, v0, :cond_2

    .line 99
    const/4 v0, 0x2

    goto :goto_0

    .line 100
    :cond_2
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne p0, v0, :cond_3

    .line 101
    const/4 v0, 0x3

    goto :goto_0

    .line 102
    :cond_3
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne p0, v0, :cond_4

    .line 103
    const/4 v0, 0x4

    goto :goto_0

    .line 105
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method


# virtual methods
.method public write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 13
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "infos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const-string v10, ""

    const-string v11, "fnm"

    invoke-static {p2, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "fileName":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {p1, v4, v0}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v7

    .line 50
    .local v7, "output":Lorg/apache/lucene/store/IndexOutput;
    const/4 v8, 0x0

    .line 52
    .local v8, "success":Z
    :try_start_0
    const-string v10, "Lucene42FieldInfos"

    const/4 v11, 0x0

    invoke-static {v7, v10, v11}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 53
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v10

    invoke-virtual {v7, v10}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 54
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    if-nez v11, :cond_0

    .line 83
    const/4 v8, 0x1

    .line 85
    if-eqz v8, :cond_b

    .line 86
    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 91
    :goto_1
    return-void

    .line 54
    :cond_0
    :try_start_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/FieldInfo;

    .line 55
    .local v3, "fi":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v5

    .line 56
    .local v5, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    const/4 v1, 0x0

    .line 57
    .local v1, "bits":B
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x2

    int-to-byte v1, v11

    .line 58
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v11

    if-eqz v11, :cond_2

    or-int/lit8 v11, v1, 0x10

    int-to-byte v1, v11

    .line 59
    :cond_2
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v11

    if-eqz v11, :cond_3

    or-int/lit8 v11, v1, 0x20

    int-to-byte v1, v11

    .line 60
    :cond_3
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 61
    or-int/lit8 v11, v1, 0x1

    int-to-byte v1, v11

    .line 62
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;->$assertionsDisabled:Z

    if-nez v11, :cond_4

    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v5, v11}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v11

    if-gez v11, :cond_4

    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v11

    if-eqz v11, :cond_4

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    .end local v1    # "bits":B
    .end local v3    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :catchall_0
    move-exception v10

    .line 85
    if-eqz v8, :cond_a

    .line 86
    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 90
    :goto_2
    throw v10

    .line 63
    .restart local v1    # "bits":B
    .restart local v3    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .restart local v5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_4
    :try_start_2
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v5, v11, :cond_7

    .line 64
    or-int/lit8 v11, v1, 0x40

    int-to-byte v1, v11

    .line 71
    :cond_5
    :goto_3
    iget-object v11, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v11}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 72
    iget v11, v3, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v7, v11}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 73
    invoke-virtual {v7, v1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 76
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v11

    invoke-static {v11}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;->docValuesByte(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)B

    move-result v2

    .line 77
    .local v2, "dv":B
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v11

    invoke-static {v11}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;->docValuesByte(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)B

    move-result v6

    .line 78
    .local v6, "nrm":B
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;->$assertionsDisabled:Z

    if-nez v11, :cond_9

    and-int/lit8 v11, v2, -0x10

    if-nez v11, :cond_6

    and-int/lit8 v11, v6, -0x10

    if-eqz v11, :cond_9

    :cond_6
    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 65
    .end local v2    # "dv":B
    .end local v6    # "nrm":B
    :cond_7
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v5, v11, :cond_8

    .line 66
    or-int/lit8 v11, v1, 0x4

    int-to-byte v1, v11

    .line 67
    goto :goto_3

    :cond_8
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v5, v11, :cond_5

    .line 68
    or-int/lit8 v11, v1, -0x80

    int-to-byte v1, v11

    goto :goto_3

    .line 79
    .restart local v2    # "dv":B
    .restart local v6    # "nrm":B
    :cond_9
    shl-int/lit8 v11, v6, 0x4

    or-int/2addr v11, v2

    and-int/lit16 v11, v11, 0xff

    int-to-byte v9, v11

    .line 80
    .local v9, "val":B
    invoke-virtual {v7, v9}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 81
    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfo;->attributes()Ljava/util/Map;

    move-result-object v11

    invoke-virtual {v7, v11}, Lorg/apache/lucene/store/IndexOutput;->writeStringStringMap(Ljava/util/Map;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 87
    .end local v1    # "bits":B
    .end local v2    # "dv":B
    .end local v3    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v5    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v6    # "nrm":B
    .end local v9    # "val":B
    :cond_a
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 88
    aput-object v7, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2

    .line 87
    :cond_b
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 88
    aput-object v7, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_1
.end method
