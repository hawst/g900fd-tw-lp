.class final Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
.super Lorg/apache/lucene/codecs/BlockTermState;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StandardTermState"
.end annotation


# instance fields
.field bytes:[B

.field bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field freqOffset:J

.field proxOffset:J

.field skipOffset:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTermState;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;-><init>()V

    .line 132
    .local v0, "other":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->copyFrom(Lorg/apache/lucene/index/TermState;)V

    .line 133
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/index/TermState;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->clone()Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    move-result-object v0

    return-object v0
.end method

.method public copyFrom(Lorg/apache/lucene/index/TermState;)V
    .locals 4
    .param p1, "_other"    # Lorg/apache/lucene/index/TermState;

    .prologue
    .line 138
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/BlockTermState;->copyFrom(Lorg/apache/lucene/index/TermState;)V

    move-object v0, p1

    .line 139
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .line 140
    .local v0, "other":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    .line 141
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    .line 142
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    .line 148
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lorg/apache/lucene/codecs/BlockTermState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " freqFP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " proxFP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " skipOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
