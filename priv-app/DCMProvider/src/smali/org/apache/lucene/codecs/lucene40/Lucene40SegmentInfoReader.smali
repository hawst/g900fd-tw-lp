.class public Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoReader;
.super Lorg/apache/lucene/codecs/SegmentInfoReader;
.source "Lucene40SegmentInfoReader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoReader;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentInfo;
    .locals 20
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    const-string v3, ""

    const-string v5, "si"

    move-object/from16 v0, p2

    invoke-static {v0, v3, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 50
    .local v12, "fileName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v12, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v14

    .line 51
    .local v14, "input":Lorg/apache/lucene/store/IndexInput;
    const/4 v15, 0x0

    .line 53
    .local v15, "success":Z
    :try_start_0
    const-string v3, "Lucene40SegmentInfo"

    .line 54
    const/4 v5, 0x0

    .line 55
    const/4 v8, 0x0

    .line 53
    invoke-static {v14, v3, v5, v8}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 56
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "version":Ljava/lang/String;
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v6

    .line 58
    .local v6, "docCount":I
    if-gez v6, :cond_0

    .line 59
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "invalid docCount: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " (resource="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ")"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    .end local v4    # "version":Ljava/lang/String;
    .end local v6    # "docCount":I
    :catchall_0
    move-exception v3

    .line 79
    if-nez v15, :cond_4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v8, 0x0

    .line 80
    aput-object v14, v5, v8

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 84
    :goto_0
    throw v3

    .line 61
    .restart local v4    # "version":Ljava/lang/String;
    .restart local v6    # "docCount":I
    :cond_0
    :try_start_1
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_1

    const/4 v7, 0x1

    .line 62
    .local v7, "isCompoundFile":Z
    :goto_1
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v9

    .line 63
    .local v9, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v11

    .line 64
    .local v11, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->readStringSet()Ljava/util/Set;

    move-result-object v13

    .line 66
    .local v13, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v16

    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v18

    cmp-long v3, v16, v18

    if-eqz v3, :cond_2

    .line 67
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "did not read all bytes from file \""

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\": read "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " vs size "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " (resource: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ")"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 61
    .end local v7    # "isCompoundFile":Z
    .end local v9    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 70
    .restart local v7    # "isCompoundFile":Z
    .restart local v9    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v11    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v13    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    new-instance v2, Lorg/apache/lucene/index/SegmentInfo;

    .line 71
    const/4 v8, 0x0

    invoke-static {v11}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v10

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    .line 70
    invoke-direct/range {v2 .. v10}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 72
    .local v2, "si":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v2, v13}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    const/4 v15, 0x1

    .line 79
    if-nez v15, :cond_3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 80
    aput-object v14, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 76
    :goto_2
    return-object v2

    .line 82
    :cond_3
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto :goto_2

    .end local v2    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v4    # "version":Ljava/lang/String;
    .end local v6    # "docCount":I
    .end local v7    # "isCompoundFile":Z
    .end local v9    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_4
    invoke-virtual {v14}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto/16 :goto_0
.end method
