.class final Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
.super Ljava/lang/Object;
.source "TermInfosReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;,
        Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;,
        Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DEFAULT_CACHE_SIZE:I = 0x400

.field private static final legacyComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final directory:Lorg/apache/lucene/store/Directory;

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

.field private final indexLength:I

.field private final origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

.field private final segment:Ljava/lang/String;

.field private final size:J

.field private final termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/DoubleBarrelLRUCache",
            "<",
            "Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;",
            "Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;",
            ">;"
        }
    .end annotation
.end field

.field private final threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/CloseableThreadLocal",
            "<",
            "Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;",
            ">;"
        }
    .end annotation
.end field

.field private final totalIndexInterval:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->$assertionsDisabled:Z

    .line 180
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 179
    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->legacyComparator:Ljava/util/Comparator;

    .line 180
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;I)V
    .locals 9
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "seg"    # Ljava/lang/String;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "indexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v8, -0x1

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v1, Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-direct {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    .line 92
    new-instance v1, Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    const/16 v3, 0x400

    invoke-direct {v1, v3}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    .line 103
    const/4 v7, 0x0

    .line 105
    .local v7, "success":Z
    if-ge p5, v4, :cond_0

    if-eq p5, v8, :cond_0

    .line 106
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "indexDivisor must be -1 (don\'t load terms index) or greater than 0: got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 110
    :cond_0
    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 111
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->segment:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 114
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->segment:Ljava/lang/String;

    const-string v5, ""

    const-string v6, "tis"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    .line 115
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    const/4 v5, 0x0

    invoke-direct {v1, v3, v4, v5}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;Z)V

    .line 114
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-wide v4, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->size:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->size:J

    .line 119
    if-eq p5, v8, :cond_3

    .line 121
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget v1, v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->indexInterval:I

    mul-int/2addr v1, p5

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->totalIndexInterval:I

    .line 123
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->segment:Ljava/lang/String;

    const-string v3, ""

    const-string v4, "tii"

    invoke-static {v1, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "indexFileName":Ljava/lang/String;
    new-instance v2, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, v0, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 125
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    const/4 v4, 0x1

    .line 124
    invoke-direct {v2, v1, v3, v4}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 128
    .local v2, "indexEnum":Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    :try_start_1
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v4

    iget v6, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->totalIndexInterval:I

    move v3, p5

    invoke-direct/range {v1 .. v6}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;-><init>(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;IJI)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->indexLength:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    :try_start_2
    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 139
    .end local v0    # "indexFileName":Ljava/lang/String;
    .end local v2    # "indexEnum":Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    :goto_0
    const/4 v7, 0x1

    .line 146
    if-nez v7, :cond_1

    .line 147
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->close()V

    .line 150
    :cond_1
    return-void

    .line 130
    .restart local v0    # "indexFileName":Ljava/lang/String;
    .restart local v2    # "indexEnum":Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    :catchall_0
    move-exception v1

    .line 131
    :try_start_3
    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->close()V

    .line 132
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 140
    .end local v0    # "indexFileName":Ljava/lang/String;
    .end local v2    # "indexEnum":Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    :catchall_1
    move-exception v1

    .line 146
    if-nez v7, :cond_2

    .line 147
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->close()V

    .line 149
    :cond_2
    throw v1

    .line 135
    :cond_3
    const/4 v1, -0x1

    :try_start_4
    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->totalIndexInterval:I

    .line 136
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    .line 137
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->indexLength:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method private final compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term1"    # Lorg/apache/lucene/index/Term;
    .param p2, "term2"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 183
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->legacyComparator:Ljava/util/Comparator;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 186
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method static deepCopyOf(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/Term;
    .locals 3
    .param p0, "other"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 217
    new-instance v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    return-object v0
.end method

.method private ensureIndexIsRead()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    if-nez v0, :cond_0

    .line 323
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "terms index was not loaded when this reader was created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_0
    return-void
.end method

.method private get(Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "mustSeekEnum"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->size:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    const/4 v1, 0x0

    .line 207
    :cond_0
    :goto_0
    return-object v1

    .line 199
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->ensureIndexIsRead()V

    .line 200
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v3, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    invoke-direct {v3, p1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->get(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;

    .line 201
    .local v1, "tiOrd":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->getThreadResources()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;

    move-result-object v0

    .line 203
    .local v0, "resources":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;
    if-nez p2, :cond_2

    if-nez v1, :cond_0

    .line 207
    :cond_2
    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    const/4 v3, 0x1

    invoke-virtual {p0, v2, p1, v1, v3}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v1

    goto :goto_0
.end method

.method private getThreadResources()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CloseableThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;

    .line 171
    .local v0, "resources":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;
    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;

    .end local v0    # "resources":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;-><init>(Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;)V

    .line 173
    .restart local v0    # "resources":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->terms()Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .line 174
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/CloseableThreadLocal;->set(Ljava/lang/Object;)V

    .line 176
    :cond_0
    return-object v0
.end method

.method private sameTermInfo(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)Z
    .locals 6
    .param p1, "ti1"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .param p2, "ti2"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .param p3, "enumerator"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .prologue
    const/4 v0, 0x0

    .line 304
    iget v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iget v2, p2, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    if-eq v1, v2, :cond_1

    .line 318
    :cond_0
    :goto_0
    return v0

    .line 307
    :cond_1
    iget-wide v2, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 310
    iget-wide v2, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 314
    iget v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iget v2, p3, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    if-lt v1, v2, :cond_2

    .line 315
    iget v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    iget v2, p2, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    if-ne v1, v2, :cond_0

    .line 318
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cacheCurrentTerm(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)V
    .locals 6
    .param p1, "enumerator"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .prologue
    .line 211
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 212
    new-instance v2, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;

    iget-object v3, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 213
    iget-wide v4, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    .line 212
    invoke-direct {v2, v3, v4, v5}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;-><init>(Lorg/apache/lucene/codecs/lucene3x/TermInfo;J)V

    .line 211
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V

    .line 214
    return-void
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 161
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->threadResources:Lorg/apache/lucene/util/CloseableThreadLocal;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->get(Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v0

    return-object v0
.end method

.method public getMaxSkipLevels()I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->maxSkipLevels:I

    return v0
.end method

.method getPosition(Lorg/apache/lucene/index/Term;)J
    .locals 8
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 329
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->size:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-wide v2

    .line 331
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->ensureIndexIsRead()V

    .line 332
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {v4, p1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->getIndexOffset(Lorg/apache/lucene/index/Term;)I

    move-result v1

    .line 334
    .local v1, "indexOffset":I
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->getThreadResources()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;

    move-result-object v4

    iget-object v0, v4, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .line 335
    .local v0, "enumerator":Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;I)V

    .line 337
    :cond_2
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->next()Z

    move-result v4

    if-nez v4, :cond_2

    .line 339
    :cond_3
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I

    move-result v4

    if-nez v4, :cond_0

    .line 340
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    goto :goto_0
.end method

.method public getSkipInterval()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->skipInterval:I

    return v0
.end method

.method seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .locals 10
    .param p1, "enumerator"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "tiOrd"    # Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;
    .param p4, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->size:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 232
    const/4 v3, 0x0

    .line 299
    :cond_0
    :goto_0
    return-object v3

    .line 236
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 237
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prev()Lorg/apache/lucene/index/Term;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->prev()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I

    move-result v4

    if-gtz v4, :cond_3

    .line 238
    :cond_2
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I

    move-result v4

    if-ltz v4, :cond_8

    .line 239
    :cond_3
    iget-wide v4, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    iget v6, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->totalIndexInterval:I

    int-to-long v6, v6

    div-long/2addr v4, v6

    long-to-int v4, v4

    add-int/lit8 v0, v4, 0x1

    .line 240
    .local v0, "enumOffset":I
    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->indexLength:I

    if-eq v4, v0, :cond_4

    .line 241
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {v4, p2, v0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->compareTo(Lorg/apache/lucene/index/Term;I)I

    move-result v4

    if-gez v4, :cond_8

    .line 245
    :cond_4
    invoke-virtual {p1, p2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->scanTo(Lorg/apache/lucene/index/Term;)I

    move-result v2

    .line 246
    .local v2, "numScans":I
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I

    move-result v4

    if-nez v4, :cond_7

    .line 247
    iget-object v3, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 248
    .local v3, "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    const/4 v4, 0x1

    if-le v2, v4, :cond_0

    .line 254
    if-nez p3, :cond_5

    .line 255
    if-eqz p4, :cond_0

    .line 256
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v5, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->deepCopyOf(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/Term;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 257
    new-instance v6, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;

    iget-wide v8, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    invoke-direct {v6, v3, v8, v9}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;-><init>(Lorg/apache/lucene/codecs/lucene3x/TermInfo;J)V

    .line 256
    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V

    goto :goto_0

    .line 260
    :cond_5
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    invoke-direct {p0, v3, p3, p1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->sameTermInfo(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 261
    :cond_6
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    long-to-int v4, v4

    int-to-long v4, v4

    iget-wide v6, p3, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;->termOrd:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 265
    .end local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_7
    const/4 v3, 0x0

    .restart local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    goto/16 :goto_0

    .line 274
    .end local v0    # "enumOffset":I
    .end local v2    # "numScans":I
    .end local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_8
    if-eqz p3, :cond_9

    .line 275
    iget-wide v4, p3, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;->termOrd:J

    iget v6, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->totalIndexInterval:I

    int-to-long v6, v6

    div-long/2addr v4, v6

    long-to-int v1, v4

    .line 281
    .local v1, "indexPos":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {v4, p1, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;I)V

    .line 282
    invoke-virtual {p1, p2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->scanTo(Lorg/apache/lucene/index/Term;)I

    .line 285
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    if-eqz v4, :cond_c

    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-direct {p0, p2, v4}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->compareAsUTF16(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/Term;)I

    move-result v4

    if-nez v4, :cond_c

    .line 286
    iget-object v3, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo:Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 287
    .restart local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    if-nez p3, :cond_a

    .line 288
    if-eqz p4, :cond_0

    .line 289
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v5, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->deepCopyOf(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/Term;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 290
    new-instance v6, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;

    iget-wide v8, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    invoke-direct {v6, v3, v8, v9}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;-><init>(Lorg/apache/lucene/codecs/lucene3x/TermInfo;J)V

    .line 289
    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->put(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 278
    .end local v1    # "indexPos":I
    .end local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_9
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->index:Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;

    invoke-virtual {v4, p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReaderIndex;->getIndexOffset(Lorg/apache/lucene/index/Term;)I

    move-result v1

    .restart local v1    # "indexPos":I
    goto :goto_1

    .line 293
    .restart local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_a
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    invoke-direct {p0, v3, p3, p1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->sameTermInfo(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 294
    :cond_b
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-wide v4, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->position:J

    iget-wide v6, p3, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;->termOrd:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 297
    .end local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_c
    const/4 v3, 0x0

    .restart local v3    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    goto/16 :goto_0
.end method

.method seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .locals 3
    .param p1, "enumerator"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .param p3, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    if-eqz p3, :cond_0

    .line 223
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->termsCache:Lorg/apache/lucene/util/DoubleBarrelLRUCache;

    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;

    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->deepCopyOf(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$CloneableTerm;-><init>(Lorg/apache/lucene/index/Term;)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/DoubleBarrelLRUCache;->get(Lorg/apache/lucene/util/DoubleBarrelLRUCache$CloneableKey;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;

    .line 222
    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v0

    .line 226
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$TermInfoAndOrd;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v0

    goto :goto_0
.end method

.method size()J
    .locals 2

    .prologue
    .line 166
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->size:J

    return-wide v0
.end method

.method public terms()Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->origEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->clone()Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-result-object v0

    return-object v0
.end method

.method public terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 352
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->get(Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 353
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->getThreadResources()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader$ThreadResources;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->clone()Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-result-object v0

    return-object v0
.end method
