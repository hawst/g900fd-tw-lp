.class Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;
.super Ljava/lang/Object;
.source "BlockTreeTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FieldMetaData"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final docCount:I

.field public final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field public final indexStartFP:J

.field public final numTerms:J

.field public final rootCode:Lorg/apache/lucene/util/BytesRef;

.field public final sumDocFreq:J

.field public final sumTotalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/BytesRef;JJJJI)V
    .locals 3
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "rootCode"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "numTerms"    # J
    .param p5, "indexStartFP"    # J
    .param p7, "sumTotalTermFreq"    # J
    .param p9, "sumDocFreq"    # J
    .param p11, "docCount"    # I

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 243
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 244
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "field="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " numTerms="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 245
    :cond_1
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->rootCode:Lorg/apache/lucene/util/BytesRef;

    .line 246
    iput-wide p5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->indexStartFP:J

    .line 247
    iput-wide p3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->numTerms:J

    .line 248
    iput-wide p7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->sumTotalTermFreq:J

    .line 249
    iput-wide p9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->sumDocFreq:J

    .line 250
    iput p11, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->docCount:I

    .line 251
    return-void
.end method
