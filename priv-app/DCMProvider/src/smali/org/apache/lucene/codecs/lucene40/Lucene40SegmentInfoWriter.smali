.class public Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoWriter;
.super Lorg/apache/lucene/codecs/SegmentInfoWriter;
.source "Lucene40SegmentInfoWriter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoWriter;-><init>()V

    .line 42
    return-void
.end method


# virtual methods
.method public write(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 8
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "ioContext"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 47
    iget-object v3, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v5, ""

    const-string v6, "si"

    invoke-static {v3, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/SegmentInfo;->addFile(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1, v0, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    .line 52
    .local v1, "output":Lorg/apache/lucene/store/IndexOutput;
    const/4 v2, 0x0

    .line 54
    .local v2, "success":Z
    :try_start_0
    const-string v3, "Lucene40SegmentInfo"

    const/4 v5, 0x0

    invoke-static {v1, v3, v5}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 56
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 59
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    int-to-byte v3, v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 60
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDiagnostics()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 61
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->attributes()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 62
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeStringSet(Ljava/util/Set;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    const/4 v2, 0x1

    .line 66
    if-nez v2, :cond_2

    new-array v3, v4, [Ljava/io/Closeable;

    .line 67
    aput-object v1, v3, v7

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 68
    iget-object v3, p2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 73
    :goto_1
    return-void

    .line 59
    :cond_0
    const/4 v3, -0x1

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v3

    .line 66
    if-nez v2, :cond_1

    new-array v4, v4, [Ljava/io/Closeable;

    .line 67
    aput-object v1, v4, v7

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 68
    iget-object v4, p2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 72
    :goto_2
    throw v3

    .line 70
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    goto :goto_2

    :cond_2
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    goto :goto_1
.end method
