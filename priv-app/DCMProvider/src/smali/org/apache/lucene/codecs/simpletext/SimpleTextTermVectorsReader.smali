.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.super Lorg/apache/lucene/codecs/TermVectorsReader;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private in:Lorg/apache/lucene/store/IndexInput;

.field private offsets:[J

.field private scratch:Lorg/apache/lucene/util/BytesRef;

.field private scratchUTF16:Lorg/apache/lucene/util/CharsRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)V
    .locals 4
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 58
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 59
    new-instance v1, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 62
    const/4 v0, 0x0

    .line 64
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v2, ""

    const-string v3, "vec"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    const/4 v0, 0x1

    .line 67
    if-nez v0, :cond_0

    .line 69
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 73
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readIndex(I)V

    .line 74
    return-void

    .line 66
    :catchall_0
    move-exception v1

    .line 67
    if-nez v0, :cond_1

    .line 69
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 72
    :cond_1
    :goto_1
    throw v1

    .line 70
    :catch_0
    move-exception v2

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method constructor <init>([JLorg/apache/lucene/store/IndexInput;)V
    .locals 1
    .param p1, "offsets"    # [J
    .param p2, "in"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 58
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 59
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 78
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    .line 79
    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 80
    return-void
.end method

.method private parseIntAt(I)I
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v2, p1

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 224
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget-object v0, v0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget v2, v2, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v0

    return v0
.end method

.method private readIndex(I)V
    .locals 4
    .param p1, "maxDoc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    new-array v1, p1, [J

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "upto":I
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    sget-boolean v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    array-length v1, v1

    if-eq v0, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 89
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 90
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_2
    return-void
.end method

.method private readLine()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 220
    return-void
.end method

.method private readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 4
    .param p1, "offset"    # I
    .param p2, "scratch"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 228
    iget-object v0, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v1, p1

    iget v2, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v2, p1

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 229
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v0}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this TermVectorsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;-><init>([JLorg/apache/lucene/store/IndexInput;)V

    return-object v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 210
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 211
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 214
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    .line 216
    return-void

    .line 212
    :catchall_0
    move-exception v0

    .line 213
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 214
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    .line 215
    throw v0
.end method

.method public get(I)Lorg/apache/lucene/index/Fields;
    .locals 25
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    .line 101
    .local v7, "fields":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/String;Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->offsets:[J

    move-object/from16 v22, v0

    aget-wide v22, v22, p1

    invoke-virtual/range {v21 .. v23}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 102
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 103
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_0

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 104
    :cond_0
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    move-result v11

    .line 105
    .local v11, "numFields":I
    if-nez v11, :cond_1

    .line 106
    const/16 v21, 0x0

    .line 197
    :goto_0
    return-object v21

    .line 108
    :cond_1
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-lt v8, v11, :cond_2

    .line 197
    new-instance v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;Ljava/util/SortedMap;)V

    goto :goto_0

    .line 109
    :cond_2
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 110
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_3

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 112
    :cond_3
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    .line 114
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 115
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDNAME:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_4

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 116
    :cond_4
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDNAME:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v6

    .line 118
    .local v6, "fieldName":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 119
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPOSITIONS:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_5

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 120
    :cond_5
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPOSITIONS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v15

    .line 122
    .local v15, "positions":Z
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 123
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDOFFSETS:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_6

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 124
    :cond_6
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDOFFSETS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v12

    .line 126
    .local v12, "offsets":Z
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 127
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPAYLOADS:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_7

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 128
    :cond_7
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPAYLOADS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v14

    .line 130
    .local v14, "payloads":Z
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 131
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDTERMCOUNT:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_8

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 132
    :cond_8
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDTERMCOUNT:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    move-result v18

    .line 134
    .local v18, "termCount":I
    new-instance v20, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;

    move-object/from16 v0, v20

    invoke-direct {v0, v12, v15, v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;-><init>(ZZZ)V

    .line 135
    .local v20, "terms":Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;
    move-object/from16 v0, v20

    invoke-interface {v7, v6, v0}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    move/from16 v0, v18

    if-lt v9, v0, :cond_9

    .line 108
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 138
    :cond_9
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 139
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMTEXT:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_a

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 140
    :cond_a
    new-instance v17, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v17 .. v17}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 141
    .local v17, "term":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMTEXT:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    sub-int v19, v21, v22

    .line 142
    .local v19, "termLength":I
    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 143
    move/from16 v0, v19

    move-object/from16 v1, v17

    iput v0, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v22, v0

    sget-object v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMTEXT:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v24, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v19

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    new-instance v16, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;

    const/16 v21, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)V

    .line 147
    .local v16, "postings":Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;
    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->terms:Ljava/util/SortedMap;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    move-object/from16 v2, v16

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 150
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMFREQ:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_b

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 151
    :cond_b
    sget-object v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMFREQ:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$6(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;I)V

    .line 153
    if-nez v15, :cond_c

    if-eqz v12, :cond_f

    .line 154
    :cond_c
    if-eqz v15, :cond_d

    .line 155
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$7(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[I)V

    .line 156
    if-eqz v14, :cond_d

    .line 157
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$8(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[Lorg/apache/lucene/util/BytesRef;)V

    .line 161
    :cond_d
    if-eqz v12, :cond_e

    .line 162
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$9(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[I)V

    .line 163
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$10(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[I)V

    .line 166
    :cond_e
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_3
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v21

    move/from16 v0, v21

    if-lt v10, v0, :cond_10

    .line 137
    .end local v10    # "k":I
    :cond_f
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 167
    .restart local v10    # "k":I
    :cond_10
    if-eqz v15, :cond_13

    .line 168
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 169
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->POSITION:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_11

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 170
    :cond_11
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->positions:[I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->POSITION:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    move-result v22

    aput v22, v21, v10

    .line 171
    if-eqz v14, :cond_13

    .line 172
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 173
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_12

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 174
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    if-nez v21, :cond_14

    .line 175
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->payloads:[Lorg/apache/lucene/util/BytesRef;
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$4(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[Lorg/apache/lucene/util/BytesRef;

    move-result-object v21

    const/16 v22, 0x0

    aput-object v22, v21, v10

    .line 184
    :cond_13
    :goto_4
    if-eqz v12, :cond_17

    .line 185
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 186
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->STARTOFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_15

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 177
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    move/from16 v0, v21

    new-array v13, v0, [B

    .line 178
    .local v13, "payloadBytes":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v22, v0

    sget-object v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v23, v0

    add-int v22, v22, v23

    const/16 v23, 0x0

    array-length v0, v13

    move/from16 v24, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    invoke-static {v0, v1, v13, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 179
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->payloads:[Lorg/apache/lucene/util/BytesRef;
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$4(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[Lorg/apache/lucene/util/BytesRef;

    move-result-object v21

    new-instance v22, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    aput-object v22, v21, v10

    goto :goto_4

    .line 187
    .end local v13    # "payloadBytes":[B
    :cond_15
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->startOffsets:[I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->STARTOFFSET:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    move-result v22

    aput v22, v21, v10

    .line 189
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->readLine()V

    .line 190
    sget-boolean v21, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->$assertionsDisabled:Z

    if-nez v21, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->ENDOFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v21

    if-nez v21, :cond_16

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 191
    :cond_16
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->endOffsets:[I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$3(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v21

    sget-object v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->ENDOFFSET:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;->parseIntAt(I)I

    move-result v22

    aput v22, v21, v10

    .line 166
    :cond_17
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3
.end method
