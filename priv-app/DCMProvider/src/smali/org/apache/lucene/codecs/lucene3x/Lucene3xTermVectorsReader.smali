.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
.super Lorg/apache/lucene/codecs/TermVectorsReader;
.source "Lucene3xTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FORMAT_CURRENT:I = 0x4

.field public static final FORMAT_MINIMUM:I = 0x4

.field static final FORMAT_SIZE:I = 0x4

.field static final FORMAT_UTF8_LENGTH_IN_BYTES:I = 0x4

.field public static final STORE_OFFSET_WITH_TERMVECTOR:B = 0x2t

.field public static final STORE_POSITIONS_WITH_TERMVECTOR:B = 0x1t

.field public static final VECTORS_DOCUMENTS_EXTENSION:Ljava/lang/String; = "tvd"

.field public static final VECTORS_FIELDS_EXTENSION:Ljava/lang/String; = "tvf"

.field public static final VECTORS_INDEX_EXTENSION:Ljava/lang/String; = "tvx"


# instance fields
.field private docStoreOffset:I

.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final format:I

.field private numTotalDocs:I

.field private size:I

.field private final storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

.field private tvd:Lorg/apache/lucene/store/IndexInput;

.field private tvf:Lorg/apache/lucene/store/IndexInput;

.field private tvx:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->$assertionsDisabled:Z

    .line 80
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;IIII)V
    .locals 1
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "tvx"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "tvd"    # Lorg/apache/lucene/store/IndexInput;
    .param p4, "tvf"    # Lorg/apache/lucene/store/IndexInput;
    .param p5, "size"    # I
    .param p6, "numTotalDocs"    # I
    .param p7, "docStoreOffset"    # I
    .param p8, "format"    # I

    .prologue
    .line 103
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 104
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 105
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    .line 106
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    .line 107
    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 108
    iput p5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->size:I

    .line 109
    iput p6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    .line 110
    iput p7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->docStoreOffset:I

    .line 111
    iput p8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->format:I

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    .line 113
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 14
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 117
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v6

    .line 118
    .local v6, "segment":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v3

    .line 119
    .local v3, "docStoreOffset":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v7

    .line 121
    .local v7, "size":I
    const/4 v8, 0x0

    .line 124
    .local v8, "success":Z
    const/4 v11, -0x1

    if-eq v3, v11, :cond_1

    :try_start_0
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreIsCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 125
    new-instance v2, Lorg/apache/lucene/store/CompoundFileDirectory;

    move-object/from16 v0, p2

    iget-object v11, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 126
    const-string v12, ""

    const-string v13, "cfx"

    invoke-static {v6, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p4

    invoke-direct {v2, v11, v12, v0, v13}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .line 125
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    .end local p1    # "d":Lorg/apache/lucene/store/Directory;
    .local v2, "d":Lorg/apache/lucene/store/Directory;
    move-object p1, v2

    .line 130
    .end local v2    # "d":Lorg/apache/lucene/store/Directory;
    .restart local p1    # "d":Lorg/apache/lucene/store/Directory;
    :goto_0
    const-string v11, ""

    const-string v12, "tvx"

    invoke-static {v6, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 131
    .local v5, "idxName":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {p1, v5, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    .line 132
    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {p0, v11}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->checkValidFormat(Lorg/apache/lucene/store/IndexInput;)I

    move-result v11

    iput v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->format:I

    .line 133
    const-string v11, ""

    const-string v12, "tvd"

    invoke-static {v6, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 134
    .local v4, "fn":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {p1, v4, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    .line 135
    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {p0, v11}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->checkValidFormat(Lorg/apache/lucene/store/IndexInput;)I

    move-result v9

    .line 136
    .local v9, "tvdFormat":I
    const-string v11, ""

    const-string v12, "tvf"

    invoke-static {v6, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 137
    move-object/from16 v0, p4

    invoke-virtual {p1, v4, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v11

    iput-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 138
    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {p0, v11}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->checkValidFormat(Lorg/apache/lucene/store/IndexInput;)I

    move-result v10

    .line 140
    .local v10, "tvfFormat":I
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->$assertionsDisabled:Z

    if-nez v11, :cond_2

    iget v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->format:I

    if-eq v11, v9, :cond_2

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    .end local v4    # "fn":Ljava/lang/String;
    .end local v5    # "idxName":Ljava/lang/String;
    .end local v9    # "tvdFormat":I
    .end local v10    # "tvfFormat":I
    :catchall_0
    move-exception v11

    .line 165
    if-nez v8, :cond_0

    .line 167
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 170
    :cond_0
    :goto_1
    throw v11

    .line 128
    :cond_1
    const/4 v11, 0x0

    :try_start_2
    iput-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    goto :goto_0

    .line 141
    .restart local v4    # "fn":Ljava/lang/String;
    .restart local v5    # "idxName":Ljava/lang/String;
    .restart local v9    # "tvdFormat":I
    .restart local v10    # "tvfFormat":I
    :cond_2
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->$assertionsDisabled:Z

    if-nez v11, :cond_3

    iget v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->format:I

    if-eq v11, v10, :cond_3

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 143
    :cond_3
    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    const/4 v11, 0x4

    shr-long/2addr v12, v11

    long-to-int v11, v12

    iput v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    .line 145
    const/4 v11, -0x1

    if-ne v11, v3, :cond_4

    .line 146
    const/4 v11, 0x0

    iput v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->docStoreOffset:I

    .line 147
    iget v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    iput v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->size:I

    .line 148
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->$assertionsDisabled:Z

    if-nez v11, :cond_5

    if-eqz v7, :cond_5

    iget v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    if-eq v11, v7, :cond_5

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 150
    :cond_4
    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->docStoreOffset:I

    .line 151
    iput v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->size:I

    .line 154
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->$assertionsDisabled:Z

    if-nez v11, :cond_5

    iget v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    add-int v12, v7, v3

    if-ge v11, v12, :cond_5

    new-instance v11, Ljava/lang/AssertionError;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "numTotalDocs="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v13, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " size="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " docStoreOffset="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v11

    .line 157
    :cond_5
    move-object/from16 v0, p3

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 158
    const/4 v8, 0x1

    .line 165
    if-nez v8, :cond_6

    .line 167
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 171
    :cond_6
    :goto_2
    return-void

    .line 168
    .end local v4    # "fn":Ljava/lang/String;
    .end local v5    # "idxName":Ljava/lang/String;
    .end local v9    # "tvdFormat":I
    .end local v10    # "tvfFormat":I
    :catch_0
    move-exception v12

    goto :goto_1

    .restart local v4    # "fn":Ljava/lang/String;
    .restart local v5    # "idxName":Ljava/lang/String;
    .restart local v9    # "tvdFormat":I
    .restart local v10    # "tvfFormat":I
    :catch_1
    move-exception v11

    goto :goto_2
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method private checkValidFormat(Lorg/apache/lucene/store/IndexInput;)I
    .locals 3
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 180
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 181
    .local v0, "format":I
    if-ge v0, v2, :cond_0

    .line 182
    new-instance v1, Lorg/apache/lucene/index/IndexFormatTooOldException;

    invoke-direct {v1, p1, v0, v2, v2}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v1

    .line 183
    :cond_0
    if-le v0, v2, :cond_1

    .line 184
    new-instance v1, Lorg/apache/lucene/index/IndexFormatTooNewException;

    invoke-direct {v1, p1, v0, v2, v2}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v1

    .line 185
    :cond_1
    return v0
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 9

    .prologue
    .line 711
    const/4 v2, 0x0

    .line 712
    .local v2, "cloneTvx":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 713
    .local v3, "cloneTvd":Lorg/apache/lucene/store/IndexInput;
    const/4 v4, 0x0

    .line 717
    .local v4, "cloneTvf":Lorg/apache/lucene/store/IndexInput;
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 719
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    .line 720
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 723
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->size:I

    iget v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->numTotalDocs:I

    iget v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->docStoreOffset:I

    iget v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->format:I

    invoke-direct/range {v0 .. v8}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;-><init>(Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;IIII)V

    return-object v0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 189
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method public get(I)Lorg/apache/lucene/index/Fields;
    .locals 3
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 694
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v2, :cond_1

    .line 695
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;I)V

    .line 696
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    invoke-virtual {v0}, Lorg/apache/lucene/index/Fields;->size()I

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 705
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method seekTvx(I)V
    .locals 6
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->docStoreOffset:I

    add-int/2addr v1, p1

    int-to-long v2, v1

    const-wide/16 v4, 0x10

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x4

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 176
    return-void
.end method

.method size()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->size:I

    return v0
.end method

.method protected sortTermsByUnicode()Z
    .locals 1

    .prologue
    .line 732
    const/4 v0, 0x1

    return v0
.end method
