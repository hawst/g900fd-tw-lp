.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesFixedDeref(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final synthetic val$fixedLength:I

.field private final synthetic val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;ILorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$fixedLength:I

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 408
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 411
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$fixedLength:I

    int-to-long v2, v2

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v4, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    mul-long v0, v2, v4

    .line 412
    .local v0, "offset":J
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;->val$fixedLength:I

    invoke-virtual {v2, p2, v0, v1, v3}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 413
    return-void
.end method
