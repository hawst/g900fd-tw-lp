.class final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "Lucene41PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "BlockDocsAndPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private accum:I

.field private doc:I

.field private docBufferUpto:I

.field private final docDeltaBuffer:[I

.field private docFreq:I

.field docIn:Lorg/apache/lucene/store/IndexInput;

.field private docTermStartFP:J

.field private docUpto:I

.field private final encoded:[B

.field private freq:I

.field private final freqBuffer:[I

.field final indexHasOffsets:Z

.field final indexHasPayloads:Z

.field private lastPosBlockFP:J

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private nextSkipDoc:I

.field private payTermStartFP:J

.field private posBufferUpto:I

.field private final posDeltaBuffer:[I

.field final posIn:Lorg/apache/lucene/store/IndexInput;

.field private posPendingCount:I

.field private posPendingFP:J

.field private posTermStartFP:J

.field private position:I

.field private singletonDocID:I

.field private skipOffset:J

.field private skipped:Z

.field private skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

.field final startDocIn:Lorg/apache/lucene/store/IndexInput;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

.field private totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610
    const-class v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 2
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 673
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 614
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docDeltaBuffer:[I

    .line 615
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freqBuffer:[I

    .line 616
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posDeltaBuffer:[I

    .line 674
    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$1(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    .line 675
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 676
    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->posIn:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$3(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    .line 677
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->encoded:[B

    .line 678
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasOffsets:Z

    .line 679
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasPayloads:Z

    .line 680
    return-void

    .line 678
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refillDocs()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 737
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    sub-int v0, v1, v2

    .line 738
    .local v0, "left":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 740
    :cond_0
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    .line 744
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docDeltaBuffer:[I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 748
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freqBuffer:[I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 759
    :goto_0
    iput v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    .line 760
    return-void

    .line 749
    :cond_1
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    if-ne v1, v4, :cond_2

    .line 750
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->singletonDocID:I

    aput v2, v1, v5

    .line 751
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freqBuffer:[I

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->totalTermFreq:J

    long-to-int v2, v2

    aput v2, v1, v5

    goto :goto_0

    .line 757
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docDeltaBuffer:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freqBuffer:[I

    invoke-static {v1, v2, v3, v0, v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->readVIntBlock(Lorg/apache/lucene/store/IndexInput;[I[IIZ)V

    goto :goto_0
.end method

.method private refillPositions()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 766
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->lastPosBlockFP:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    .line 770
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->totalTermFreq:J

    const-wide/16 v6, 0x80

    rem-long/2addr v4, v6

    long-to-int v1, v4

    .line 771
    .local v1, "count":I
    const/4 v3, 0x0

    .line 772
    .local v3, "payloadLength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 798
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "payloadLength":I
    :goto_1
    return-void

    .line 773
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "payloadLength":I
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 774
    .local v0, "code":I
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasPayloads:Z

    if-eqz v4, :cond_4

    .line 775
    and-int/lit8 v4, v0, 0x1

    if-eqz v4, :cond_1

    .line 776
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    .line 778
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posDeltaBuffer:[I

    ushr-int/lit8 v5, v0, 0x1

    aput v5, v4, v2

    .line 779
    if-eqz v3, :cond_2

    .line 780
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    int-to-long v8, v3

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 785
    :cond_2
    :goto_2
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasOffsets:Z

    if-eqz v4, :cond_3

    .line 786
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    .line 788
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    .line 772
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 783
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posDeltaBuffer:[I

    aput v0, v4, v2

    goto :goto_2

    .line 796
    .end local v0    # "code":I
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "payloadLength":I
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->encoded:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posDeltaBuffer:[I

    invoke-virtual {v4, v5, v6, v7}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    goto :goto_1
.end method

.method private skipPositions()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 940
    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    sub-int v1, v2, v3

    .line 945
    .local v1, "toSkip":I
    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    rsub-int v0, v2, 0x80

    .line 946
    .local v0, "leftInBlock":I
    if-ge v1, v0, :cond_0

    .line 947
    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    add-int/2addr v2, v1

    iput v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    .line 968
    :goto_0
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->position:I

    .line 969
    return-void

    .line 952
    :cond_0
    sub-int/2addr v1, v0

    .line 953
    :goto_1
    const/16 v2, 0x80

    if-ge v1, v2, :cond_1

    .line 961
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->refillPositions()V

    .line 962
    iput v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    goto :goto_0

    .line 957
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->lastPosBlockFP:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 958
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v2}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 959
    add-int/lit8 v1, v1, -0x80

    goto :goto_1
.end method


# virtual methods
.method public advance(I)I
    .locals 12
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 845
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    const/16 v1, 0x80

    if-le v0, v1, :cond_5

    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->nextSkipDoc:I

    if-le p1, v0, :cond_5

    .line 849
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    if-nez v0, :cond_0

    .line 854
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 855
    const/16 v2, 0xa

    .line 856
    const/16 v3, 0x80

    .line 857
    const/4 v4, 0x1

    .line 858
    iget-boolean v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasOffsets:Z

    .line 859
    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasPayloads:Z

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;-><init>(Lorg/apache/lucene/store/IndexInput;IIZZZ)V

    .line 854
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    .line 862
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipped:Z

    if-nez v0, :cond_2

    .line 863
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipOffset:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 869
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docTermStartFP:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipOffset:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docTermStartFP:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posTermStartFP:J

    iget-wide v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->payTermStartFP:J

    iget v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    invoke-virtual/range {v1 .. v10}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->init(JJJJI)V

    .line 870
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipped:Z

    .line 873
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->skipTo(I)I

    move-result v0

    add-int/lit8 v11, v0, 0x1

    .line 875
    .local v11, "newDocUpto":I
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    if-le v11, v0, :cond_4

    .line 881
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    rem-int/lit16 v0, v11, 0x80

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 882
    :cond_3
    iput v11, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    .line 885
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    .line 886
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    .line 887
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getDocPointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 888
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getPosPointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingFP:J

    .line 889
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getPosBufferUpto()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    .line 891
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getNextSkipDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->nextSkipDoc:I

    .line 893
    .end local v11    # "newDocUpto":I
    :cond_5
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    if-ne v0, v1, :cond_6

    .line 894
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    .line 930
    :goto_0
    return v0

    .line 896
    :cond_6
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 897
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->refillDocs()V

    .line 906
    :cond_7
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    .line 907
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freqBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    .line 908
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    .line 909
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    .line 910
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    .line 912
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    if-lt v0, p1, :cond_9

    .line 920
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 924
    :cond_8
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->position:I

    .line 925
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    goto :goto_0

    .line 915
    :cond_9
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    if-ne v0, v1, :cond_7

    .line 916
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    goto :goto_0

    .line 930
    :cond_a
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->nextDoc()I

    move-result v0

    goto :goto_0
.end method

.method public canReuse(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;)Z
    .locals 5
    .param p1, "docIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 683
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v2, :cond_1

    .line 684
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasOffsets:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    if-ne v3, v2, :cond_1

    .line 685
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->indexHasPayloads:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v3

    if-ne v2, v3, :cond_1

    .line 683
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 684
    goto :goto_0

    :cond_1
    move v0, v1

    .line 683
    goto :goto_1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 1021
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 733
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 1

    .prologue
    .line 1011
    const/4 v0, -0x1

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 728
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 1016
    const/4 v0, 0x0

    return-object v0
.end method

.method public nextDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 809
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    if-ne v0, v1, :cond_1

    .line 810
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    .line 830
    :goto_0
    return v0

    .line 812
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2

    .line 813
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->refillDocs()V

    .line 818
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    .line 819
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freqBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    .line 820
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    .line 821
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    .line 822
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    .line 824
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    .line 826
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->position:I

    .line 830
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    const/16 v4, 0x80

    .line 976
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingFP:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 980
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingFP:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 981
    iput-wide v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingFP:J

    .line 984
    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    .line 987
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    if-le v0, v1, :cond_1

    .line 988
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipPositions()V

    .line 989
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->freq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    .line 992
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    if-ne v0, v4, :cond_2

    .line 993
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->refillPositions()V

    .line 994
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    .line 996
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->position:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->position:I

    .line 997
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    .line 1001
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->position:I

    return v0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 8
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x80

    const/4 v4, 0x0

    .line 689
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 693
    iget v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    .line 694
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docTermStartFP:J

    .line 695
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posTermStartFP:J

    .line 696
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->payTermStartFP:J

    .line 697
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipOffset:J

    .line 698
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->totalTermFreq:J

    .line 699
    iget v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->singletonDocID:I

    .line 700
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docFreq:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 701
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 703
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 705
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docTermStartFP:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 707
    :cond_1
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posTermStartFP:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingFP:J

    .line 708
    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posPendingCount:I

    .line 709
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_2

    .line 710
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posTermStartFP:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->lastPosBlockFP:J

    .line 717
    :goto_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->doc:I

    .line 718
    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->accum:I

    .line 719
    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docUpto:I

    .line 720
    const/16 v0, 0x7f

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->nextSkipDoc:I

    .line 721
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->docBufferUpto:I

    .line 722
    iput-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->skipped:Z

    .line 723
    return-object p0

    .line 711
    :cond_2
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_3

    .line 712
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->lastPosBlockFP:J

    goto :goto_0

    .line 714
    :cond_3
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->posTermStartFP:J

    iget-wide v2, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->lastPosBlockFP:J

    goto :goto_0
.end method

.method public startOffset()I
    .locals 1

    .prologue
    .line 1006
    const/4 v0, -0x1

    return v0
.end method
