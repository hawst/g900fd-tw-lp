.class final Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;
.super Lorg/apache/lucene/store/IndexInput;
.source "MultiLevelSkipListReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/MultiLevelSkipListReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SkipBuffer"
.end annotation


# instance fields
.field private data:[B

.field private pointer:J

.field private pos:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/IndexInput;I)V
    .locals 2
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 278
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SkipBuffer on "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 279
    new-array v0, p2, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->data:[B

    .line 280
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pointer:J

    .line 281
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->data:[B

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 282
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->data:[B

    .line 287
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 291
    iget-wide v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pointer:J

    iget v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->data:[B

    array-length v0, v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 301
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->data:[B

    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 306
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->data:[B

    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 307
    iget v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    .line 308
    return-void
.end method

.method public seek(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 312
    iget-wide v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pointer:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;->pos:I

    .line 313
    return-void
.end method
