.class public final Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;
.super Lorg/apache/lucene/codecs/StoredFieldsReader;
.source "Lucene40StoredFieldsReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private closed:Z

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fieldsStream:Lorg/apache/lucene/store/IndexInput;

.field private final indexStream:Lorg/apache/lucene/store/IndexInput;

.field private numTotalDocs:I

.field private size:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->values()[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/index/FieldInfos;IILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V
    .locals 0
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numTotalDocs"    # I
    .param p3, "size"    # I
    .param p4, "fieldsStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p5, "indexStream"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 68
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 70
    iput p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->numTotalDocs:I

    .line 71
    iput p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->size:I

    .line 72
    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 73
    iput-object p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    .line 74
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 11
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 78
    iget-object v3, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 79
    .local v3, "segment":Ljava/lang/String;
    const/4 v4, 0x0

    .line 80
    .local v4, "success":Z
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 82
    :try_start_0
    const-string v5, ""

    const-string v6, "fdt"

    invoke-static {v3, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 83
    const-string v5, ""

    const-string v6, "fdx"

    invoke-static {v3, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "indexStreamFN":Ljava/lang/String;
    invoke-virtual {p1, v2, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    .line 86
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    const-string v6, "Lucene40StoredFieldsIndex"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 87
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    const-string v6, "Lucene40StoredFieldsData"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 88
    sget-boolean v5, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    sget-wide v6, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_DAT:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    .end local v2    # "indexStreamFN":Ljava/lang/String;
    :catchall_0
    move-exception v5

    .line 104
    if-nez v4, :cond_0

    .line 106
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 109
    :cond_0
    :goto_0
    throw v5

    .line 89
    .restart local v2    # "indexStreamFN":Ljava/lang/String;
    :cond_1
    :try_start_2
    sget-boolean v5, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    sget-wide v6, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_IDX:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 90
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    sget-wide v8, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_IDX:J

    sub-long v0, v6, v8

    .line 91
    .local v0, "indexSize":J
    shr-long v6, v0, v10

    long-to-int v5, v6

    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->size:I

    .line 93
    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->size:I

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v6

    if-eq v5, v6, :cond_3

    .line 94
    new-instance v5, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "doc counts differ for segment "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": fieldsReader shows "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->size:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " but segmentInfo shows "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 96
    :cond_3
    shr-long v6, v0, v10

    long-to-int v5, v6

    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->numTotalDocs:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    const/4 v4, 0x1

    .line 104
    if-nez v4, :cond_4

    .line 106
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 110
    :cond_4
    :goto_1
    return-void

    .line 107
    .end local v0    # "indexSize":J
    .end local v2    # "indexStreamFN":Ljava/lang/String;
    :catch_0
    move-exception v6

    goto :goto_0

    .restart local v0    # "indexSize":J
    .restart local v2    # "indexStreamFN":Ljava/lang/String;
    :catch_1
    move-exception v5

    goto :goto_1
.end method

.method private ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 116
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->closed:Z

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this FieldsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    return-void
.end method

.method private readField(Lorg/apache/lucene/index/StoredFieldVisitor;Lorg/apache/lucene/index/FieldInfo;I)V
    .locals 7
    .param p1, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .param p2, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "bits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 171
    and-int/lit8 v2, p3, 0x38

    .line 172
    .local v2, "numeric":I
    if-eqz v2, :cond_0

    .line 173
    sparse-switch v2, :sswitch_data_0

    .line 187
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid numeric type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 175
    :sswitch_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v3

    invoke-virtual {p1, p2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->intField(Lorg/apache/lucene/index/FieldInfo;I)V

    .line 199
    :goto_0
    return-void

    .line 178
    :sswitch_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    invoke-virtual {p1, p2, v4, v5}, Lorg/apache/lucene/index/StoredFieldVisitor;->longField(Lorg/apache/lucene/index/FieldInfo;J)V

    goto :goto_0

    .line 181
    :sswitch_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    invoke-virtual {p1, p2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->floatField(Lorg/apache/lucene/index/FieldInfo;F)V

    goto :goto_0

    .line 184
    :sswitch_3
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    invoke-virtual {p1, p2, v4, v5}, Lorg/apache/lucene/index/StoredFieldVisitor;->doubleField(Lorg/apache/lucene/index/FieldInfo;D)V

    goto :goto_0

    .line 190
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 191
    .local v1, "length":I
    new-array v0, v1, [B

    .line 192
    .local v0, "bytes":[B
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3, v0, v6, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 193
    and-int/lit8 v3, p3, 0x2

    if-eqz v3, :cond_1

    .line 194
    invoke-virtual {p1, p2, v0}, Lorg/apache/lucene/index/StoredFieldVisitor;->binaryField(Lorg/apache/lucene/index/FieldInfo;[B)V

    goto :goto_0

    .line 196
    :cond_1
    new-instance v3, Ljava/lang/String;

    array-length v4, v0

    sget-object v5, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v0, v6, v4, v5}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-virtual {p1, p2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->stringField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 173
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method private seekIndex(I)V
    .locals 8
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    sget-wide v2, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_IDX:J

    int-to-long v4, p1

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 142
    return-void
.end method

.method private skipField(I)V
    .locals 8
    .param p1, "bits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    and-int/lit8 v1, p1, 0x38

    .line 203
    .local v1, "numeric":I
    if-eqz v1, :cond_0

    .line 204
    sparse-switch v1, :sswitch_data_0

    .line 214
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid numeric type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 207
    :sswitch_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    .line 220
    :goto_0
    return-void

    .line 211
    :sswitch_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    goto :goto_0

    .line 217
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 218
    .local v0, "length":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_0

    .line 204
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/codecs/StoredFieldsReader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->clone()Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;
    .locals 6

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->ensureOpen()V

    .line 64
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->numTotalDocs:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->size:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;-><init>(Lorg/apache/lucene/index/FieldInfos;IILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    return-object v0
.end method

.method public final close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 129
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->closed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 130
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v3

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 131
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->closed:Z

    .line 133
    :cond_0
    return-void
.end method

.method public final rawDocs([III)Lorg/apache/lucene/store/IndexInput;
    .locals 12
    .param p1, "lengths"    # [I
    .param p2, "startDocID"    # I
    .param p3, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    invoke-direct {p0, p2}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->seekIndex(I)V

    .line 228
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 229
    .local v8, "startOffset":J
    move-wide v4, v8

    .line 230
    .local v4, "lastOffset":J
    const/4 v0, 0x0

    .local v0, "count":I
    move v1, v0

    .line 231
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_0
    if-lt v1, p3, :cond_0

    .line 243
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 245
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    return-object v3

    .line 233
    :cond_0
    add-int v3, p2, v1

    add-int/lit8 v2, v3, 0x1

    .line 234
    .local v2, "docID":I
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->numTotalDocs:I

    if-le v2, v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 235
    :cond_1
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->numTotalDocs:I

    if-ge v2, v3, :cond_2

    .line 236
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    .line 239
    .local v6, "offset":J
    :goto_1
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    sub-long v10, v6, v4

    long-to-int v3, v10

    aput v3, p1, v1

    .line 240
    move-wide v4, v6

    move v1, v0

    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_0

    .line 238
    .end local v6    # "offset":J
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    .restart local v6    # "offset":J
    goto :goto_1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->size:I

    return v0
.end method

.method public final visitDocument(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 8
    .param p1, "n"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->seekIndex(I)V

    .line 147
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 149
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 150
    .local v4, "numFields":I
    const/4 v1, 0x0

    .local v1, "fieldIDX":I
    :goto_0
    if-lt v1, v4, :cond_0

    .line 168
    :pswitch_0
    return-void

    .line 151
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    .line 152
    .local v3, "fieldNumber":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 154
    .local v2, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v5

    and-int/lit16 v0, v5, 0xff

    .line 155
    .local v0, "bits":I
    sget-boolean v5, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    const/16 v5, 0x3a

    if-le v0, v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "bits="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 157
    :cond_1
    invoke-static {}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I

    move-result-object v5

    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/StoredFieldVisitor;->needsField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 150
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 159
    :pswitch_1
    invoke-direct {p0, p2, v2, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->readField(Lorg/apache/lucene/index/StoredFieldVisitor;Lorg/apache/lucene/index/FieldInfo;I)V

    goto :goto_1

    .line 162
    :pswitch_2
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->skipField(I)V

    goto :goto_1

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
