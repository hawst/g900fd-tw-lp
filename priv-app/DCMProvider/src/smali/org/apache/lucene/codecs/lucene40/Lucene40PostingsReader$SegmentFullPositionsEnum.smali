.class Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SegmentFullPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field accum:I

.field doc:I

.field freq:I

.field private final freqIn:Lorg/apache/lucene/store/IndexInput;

.field freqOffset:J

.field private lazyProxPointer:J

.field limit:I

.field liveDocs:Lorg/apache/lucene/util/Bits;

.field offsetLength:I

.field ord:I

.field private payload:Lorg/apache/lucene/util/BytesRef;

.field payloadLength:I

.field payloadPending:Z

.field posPendingCount:I

.field position:I

.field private final proxIn:Lorg/apache/lucene/store/IndexInput;

.field proxOffset:J

.field skipOffset:J

.field skipped:Z

.field skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

.field final startFreqIn:Lorg/apache/lucene/store/IndexInput;

.field startOffset:I

.field storeOffsets:Z

.field storePayloads:Z

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 902
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V
    .locals 1
    .param p2, "freqIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "proxIn"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 935
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 909
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    .line 936
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startFreqIn:Lorg/apache/lucene/store/IndexInput;

    .line 937
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 938
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    .line 939
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 12
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 1026
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v1, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    sub-int v1, p1, v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->limit:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v2, v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipMinimum:I

    if-lt v1, v2, :cond_2

    .line 1031
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    if-nez v1, :cond_0

    .line 1033
    new-instance v1, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v3, v3, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->maxSkipLevels:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v4, v4, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    .line 1036
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipped:Z

    if-nez v1, :cond_1

    .line 1042
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqOffset:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipOffset:J

    add-long/2addr v2, v4

    .line 1043
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqOffset:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxOffset:J

    .line 1044
    iget v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->limit:I

    iget-boolean v9, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    iget-boolean v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    .line 1042
    invoke-virtual/range {v1 .. v10}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->init(JJJIZZ)V

    .line 1046
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipped:Z

    .line 1049
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->skipTo(I)I

    move-result v0

    .line 1051
    .local v0, "newOrd":I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->ord:I

    if-le v0, v1, :cond_2

    .line 1053
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->ord:I

    .line 1054
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->accum:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    .line 1055
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getFreqPointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1056
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getProxPointer()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->lazyProxPointer:J

    .line 1057
    iput v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    .line 1058
    iput v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    .line 1059
    iput v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    .line 1060
    iput-boolean v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    .line 1061
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getPayloadLength()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    .line 1062
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getOffsetLength()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->offsetLength:I

    .line 1068
    .end local v0    # "newOrd":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->nextDoc()I

    .line 1069
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    .line 1067
    if-gt p1, v1, :cond_2

    .line 1071
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    return v1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 1195
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->limit:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 1013
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1163
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->offsetLength:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1018
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freq:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 1170
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    if-eqz v1, :cond_0

    .line 1171
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-gtz v1, :cond_1

    .line 1189
    :cond_0
    :goto_0
    return-object v0

    .line 1174
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->lazyProxPointer:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1175
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freq:I

    if-lt v0, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1177
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    if-eqz v0, :cond_5

    .line 1178
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_4

    .line 1179
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 1182
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    invoke-virtual {v0, v1, v4, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 1183
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1184
    iput-boolean v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    .line 1187
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 981
    :cond_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->ord:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->limit:I

    if-ne v1, v2, :cond_1

    .line 983
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    .line 1008
    :goto_0
    return v1

    .line 986
    :cond_1
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->ord:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->ord:I

    .line 989
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 991
    .local v0, "code":I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->accum:I

    ushr-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->accum:I

    .line 992
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_3

    .line 993
    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freq:I

    .line 997
    :goto_1
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freq:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    .line 999
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->accum:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1004
    :cond_2
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    .line 1005
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    .line 1008
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->accum:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    goto :goto_0

    .line 995
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freq:I

    goto :goto_1
.end method

.method public nextPosition()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 1077
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->lazyProxPointer:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 1078
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->lazyProxPointer:J

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1079
    iput-wide v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->lazyProxPointer:J

    .line 1082
    :cond_0
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-lez v2, :cond_1

    .line 1084
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1085
    iput-boolean v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    .line 1089
    :cond_1
    :goto_0
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freq:I

    if-gt v2, v3, :cond_3

    .line 1120
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-lez v2, :cond_2

    .line 1122
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1125
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 1126
    .local v0, "code":I
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    if-eqz v2, :cond_a

    .line 1127
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_8

    .line 1129
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    .line 1130
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_8

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-gez v2, :cond_8

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1090
    .end local v0    # "code":I
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 1092
    .restart local v0    # "code":I
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    if-eqz v2, :cond_5

    .line 1093
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_4

    .line 1095
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    .line 1096
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-gez v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1098
    :cond_4
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-ne v2, v9, :cond_5

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1101
    :cond_5
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    if-eqz v2, :cond_6

    .line 1102
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_6

    .line 1104
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->offsetLength:I

    .line 1108
    :cond_6
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    if-eqz v2, :cond_7

    .line 1109
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1112
    :cond_7
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    .line 1113
    iput v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    .line 1114
    iput v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    .line 1115
    iput-boolean v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    goto/16 :goto_0

    .line 1132
    :cond_8
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_9

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadLength:I

    if-ne v2, v9, :cond_9

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1134
    :cond_9
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    .line 1135
    ushr-int/lit8 v0, v0, 0x1

    .line 1137
    :cond_a
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    .line 1139
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    if-eqz v2, :cond_c

    .line 1140
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 1141
    .local v1, "offsetCode":I
    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_b

    .line 1143
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->offsetLength:I

    .line 1145
    :cond_b
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    ushr-int/lit8 v3, v1, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    .line 1148
    .end local v1    # "offsetCode":I
    :cond_c
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    .line 1150
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_d

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    if-gez v2, :cond_d

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "nextPosition() was called too many times (more than freq() times) posPendingCount="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 1153
    :cond_d
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    return v2
.end method

.method public reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    .locals 6
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 942
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    .line 943
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    .line 944
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 942
    goto :goto_0

    .line 945
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storePayloads:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 946
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_3

    .line 947
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 948
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    new-array v1, v1, [B

    iput-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 951
    :cond_3
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 956
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 957
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->lazyProxPointer:J

    .line 959
    iget v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->limit:I

    .line 960
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->ord:I

    .line 961
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->doc:I

    .line 962
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->accum:I

    .line 963
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->position:I

    .line 964
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    .line 966
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipped:Z

    .line 967
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->posPendingCount:I

    .line 968
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->payloadPending:Z

    .line 970
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->freqOffset:J

    .line 971
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->proxOffset:J

    .line 972
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->skipOffset:J

    .line 975
    return-object p0
.end method

.method public startOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1158
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->storeOffsets:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startOffset:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
