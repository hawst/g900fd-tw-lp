.class final Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;
.super Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AllDocsSegmentDocsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 523
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;)V
    .locals 1
    .param p2, "startFreqIn"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 525
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    .line 526
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/Bits;)V

    .line 527
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 528
    :cond_0
    return-void
.end method


# virtual methods
.method protected final linearScan(I)I
    .locals 5
    .param p1, "scanTo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 542
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->docs:[I

    .line 543
    .local v1, "docs":[I
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->count:I

    .line 544
    .local v3, "upTo":I
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->start:I

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 552
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->refill()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->doc:I

    :goto_1
    return v4

    .line 545
    :cond_0
    aget v0, v1, v2

    .line 546
    .local v0, "d":I
    if-gt p1, v0, :cond_1

    .line 547
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->start:I

    .line 548
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freqs:[I

    aget v4, v4, v2

    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freq:I

    .line 549
    aget v4, v1, v2

    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->doc:I

    goto :goto_1

    .line 544
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final nextDoc()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 532
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->start:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->start:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->count:I

    if-ge v0, v1, :cond_0

    .line 533
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freqs:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->start:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freq:I

    .line 534
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->docs:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->start:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->doc:I

    .line 536
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->refill()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->doc:I

    goto :goto_0
.end method

.method protected final nextUnreadDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 584
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->ord:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->ord:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->limit:I

    if-ge v1, v2, :cond_1

    .line 585
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 586
    .local v0, "code":I
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->indexOmitsTF:Z

    if-eqz v1, :cond_0

    .line 587
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    .line 592
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    .line 594
    .end local v0    # "code":I
    :goto_1
    return v1

    .line 589
    .restart local v0    # "code":I
    :cond_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    ushr-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    .line 590
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->readFreq(Lorg/apache/lucene/store/IndexInput;I)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freq:I

    goto :goto_0

    .line 594
    .end local v0    # "code":I
    :cond_1
    const v1, 0x7fffffff

    goto :goto_1
.end method

.method protected scanTo(I)I
    .locals 8
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 557
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    .line 558
    .local v1, "docAcc":I
    const/4 v3, 0x1

    .line 559
    .local v3, "frq":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 560
    .local v2, "freqIn":Lorg/apache/lucene/store/IndexInput;
    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->indexOmitsTF:Z

    .line 561
    .local v6, "omitTF":Z
    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->limit:I

    .line 562
    .local v5, "loopLimit":I
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->ord:I

    .local v4, "i":I
    :goto_0
    if-lt v4, v5, :cond_0

    .line 576
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->limit:I

    iput v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->ord:I

    .line 577
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freq:I

    .line 578
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    .line 579
    const v7, 0x7fffffff

    :goto_1
    return v7

    .line 563
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 564
    .local v0, "code":I
    if-eqz v6, :cond_1

    .line 565
    add-int/2addr v1, v0

    .line 570
    :goto_2
    if-lt v1, p1, :cond_2

    .line 571
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->freq:I

    .line 572
    add-int/lit8 v7, v4, 0x1

    iput v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->ord:I

    .line 573
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->accum:I

    move v7, v1

    goto :goto_1

    .line 567
    :cond_1
    ushr-int/lit8 v7, v0, 0x1

    add-int/2addr v1, v7

    .line 568
    invoke-virtual {p0, v2, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->readFreq(Lorg/apache/lucene/store/IndexInput;I)I

    move-result v3

    goto :goto_2

    .line 562
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
