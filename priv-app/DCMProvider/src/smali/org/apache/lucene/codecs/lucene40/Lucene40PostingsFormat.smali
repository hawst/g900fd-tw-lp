.class public Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;
.super Lorg/apache/lucene/codecs/PostingsFormat;
.source "Lucene40PostingsFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final FREQ_EXTENSION:Ljava/lang/String; = "frq"

.field static final PROX_EXTENSION:Ljava/lang/String; = "prx"


# instance fields
.field protected final maxBlockSize:I

.field protected final minBlockSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 220
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->$assertionsDisabled:Z

    .line 276
    return-void

    .line 220
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 230
    const/16 v0, 0x19

    const/16 v1, 0x30

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;-><init>(II)V

    .line 231
    return-void
.end method

.method private constructor <init>(II)V
    .locals 1
    .param p1, "minBlockSize"    # I
    .param p2, "maxBlockSize"    # I

    .prologue
    .line 238
    const-string v0, "Lucene40"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/PostingsFormat;-><init>(Ljava/lang/String;)V

    .line 239
    iput p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->minBlockSize:I

    .line 240
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 241
    :cond_0
    iput p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->maxBlockSize:I

    .line 242
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;
    .locals 10
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 251
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;)V

    .line 253
    .local v0, "postings":Lorg/apache/lucene/codecs/PostingsReaderBase;
    const/4 v9, 0x0

    .line 255
    .local v9, "success":Z
    :try_start_0
    new-instance v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    .line 256
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    .line 257
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 258
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 260
    iget-object v6, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    .line 261
    iget-object v7, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    .line 262
    iget v8, p1, Lorg/apache/lucene/index/SegmentReadState;->termsIndexDivisor:I

    move-object v5, v0

    .line 255
    invoke-direct/range {v1 .. v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/codecs/PostingsReaderBase;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    .local v1, "ret":Lorg/apache/lucene/codecs/FieldsProducer;
    const/4 v9, 0x1

    .line 266
    if-nez v9, :cond_0

    .line 267
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/PostingsReaderBase;->close()V

    .line 264
    :cond_0
    return-object v1

    .line 265
    .end local v1    # "ret":Lorg/apache/lucene/codecs/FieldsProducer;
    :catchall_0
    move-exception v2

    .line 266
    if-nez v9, :cond_1

    .line 267
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/PostingsReaderBase;->close()V

    .line 269
    :cond_1
    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(minBlockSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->minBlockSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " maxBlockSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsFormat;->maxBlockSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
