.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;
.super Lorg/apache/lucene/index/Terms;
.source "CompressingTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVTerms"
.end annotation


# instance fields
.field private final flags:I

.field private final lengths:[I

.field private final numTerms:I

.field private final payloadBytes:Lorg/apache/lucene/util/BytesRef;

.field private final payloadIndex:[I

.field private final positionIndex:[I

.field private final positions:[I

.field private final prefixLengths:[I

.field private final startOffsets:[I

.field private final suffixLengths:[I

.field private final termBytes:Lorg/apache/lucene/util/BytesRef;

.field private final termFreqs:[I

.field final synthetic this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;II[I[I[I[I[I[I[I[ILorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p2, "numTerms"    # I
    .param p3, "flags"    # I
    .param p4, "prefixLengths"    # [I
    .param p5, "suffixLengths"    # [I
    .param p6, "termFreqs"    # [I
    .param p7, "positionIndex"    # [I
    .param p8, "positions"    # [I
    .param p9, "startOffsets"    # [I
    .param p10, "lengths"    # [I
    .param p11, "payloadIndex"    # [I
    .param p12, "payloadBytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p13, "termBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 692
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    .line 689
    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 693
    iput p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->numTerms:I

    .line 694
    iput p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->flags:I

    .line 695
    iput-object p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->prefixLengths:[I

    .line 696
    iput-object p5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->suffixLengths:[I

    .line 697
    iput-object p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->termFreqs:[I

    .line 698
    iput-object p7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->positionIndex:[I

    .line 699
    iput-object p8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->positions:[I

    .line 700
    iput-object p9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->startOffsets:[I

    .line 701
    iput-object p10, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->lengths:[I

    .line 702
    iput-object p11, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->payloadIndex:[I

    .line 703
    iput-object p12, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->payloadBytes:Lorg/apache/lucene/util/BytesRef;

    .line 704
    iput-object p13, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->termBytes:Lorg/apache/lucene/util/BytesRef;

    .line 705
    return-void
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 723
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 743
    const/4 v0, 0x1

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 738
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->numTerms:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 733
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 748
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 758
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 753
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->flags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 17
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 710
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    instance-of v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;

    if-eqz v2, :cond_0

    move-object/from16 v1, p1

    .line 711
    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;

    .line 715
    .local v1, "termsEnum":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->numTerms:I

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->flags:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->prefixLengths:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->suffixLengths:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->termFreqs:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->positionIndex:[I

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->positions:[I

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->startOffsets:[I

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->lengths:[I

    .line 716
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->payloadIndex:[I

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->payloadBytes:Lorg/apache/lucene/util/BytesRef;

    .line 717
    new-instance v13, Lorg/apache/lucene/store/ByteArrayDataInput;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->termBytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v14, v14, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->termBytes:Lorg/apache/lucene/util/BytesRef;

    iget v15, v15, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->termBytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v16, v0

    invoke-direct/range {v13 .. v16}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>([BII)V

    .line 715
    invoke-virtual/range {v1 .. v13}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;->reset(II[I[I[I[I[I[I[I[ILorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/store/ByteArrayDataInput;)V

    .line 718
    return-object v1

    .line 713
    .end local v1    # "termsEnum":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;
    :cond_0
    new-instance v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;)V

    .restart local v1    # "termsEnum":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;
    goto :goto_0
.end method

.method public size()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 728
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;->numTerms:I

    int-to-long v0, v0

    return-wide v0
.end method
