.class public abstract Lorg/apache/lucene/codecs/TermsConsumer;
.super Ljava/lang/Object;
.source "TermsConsumer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private docsAndFreqsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

.field private docsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

.field private postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lorg/apache/lucene/codecs/TermsConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/TermsConsumer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public abstract finish(JJI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/TermsEnum;)V
    .locals 24
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .param p3, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    sget-boolean v5, Lorg/apache/lucene/codecs/TermsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez p3, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 90
    :cond_0
    const-wide/16 v18, 0x0

    .line 91
    .local v18, "sumTotalTermFreq":J
    const-wide/16 v8, 0x0

    .line 92
    .local v8, "sumDocFreq":J
    const-wide/16 v16, 0x0

    .line 93
    .local v16, "sumDFsinceLastAbortCheck":J
    new-instance v20, Lorg/apache/lucene/util/FixedBitSet;

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v5

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 95
    .local v20, "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v5, :cond_5

    .line 96
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    if-nez v5, :cond_1

    .line 97
    new-instance v5, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    invoke-direct {v5}, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    .line 99
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->setMergeState(Lorg/apache/lucene/index/MergeState;)V

    .line 101
    const/4 v11, 0x0

    .line 103
    .local v11, "docsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    :cond_2
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v15

    .local v15, "term":Lorg/apache/lucene/util/BytesRef;
    if-nez v15, :cond_4

    .line 204
    .end local v11    # "docsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    :cond_3
    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v5, :cond_12

    const-wide/16 v6, -0x1

    :goto_1
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v10

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v10}, Lorg/apache/lucene/codecs/TermsConsumer;->finish(JJI)V

    .line 205
    return-void

    .line 106
    .restart local v11    # "docsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    :cond_4
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v11, v6}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v11

    .end local v11    # "docsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    check-cast v11, Lorg/apache/lucene/index/MultiDocsEnum;

    .line 107
    .restart local v11    # "docsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    if-eqz v11, :cond_2

    .line 108
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    invoke-virtual {v5, v11}, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->reset(Lorg/apache/lucene/index/MultiDocsEnum;)Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    .line 109
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/codecs/TermsConsumer;->startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;

    move-result-object v12

    .line 110
    .local v12, "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-virtual {v12, v0, v1, v5, v2}, Lorg/apache/lucene/codecs/PostingsConsumer;->merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/FixedBitSet;)Lorg/apache/lucene/codecs/TermStats;

    move-result-object v14

    .line 111
    .local v14, "stats":Lorg/apache/lucene/codecs/TermStats;
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    if-lez v5, :cond_2

    .line 112
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v14}, Lorg/apache/lucene/codecs/TermsConsumer;->finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V

    .line 113
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long v18, v18, v6

    .line 114
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long v16, v16, v6

    .line 115
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long/2addr v8, v6

    .line 116
    const-wide/32 v6, 0xea60

    cmp-long v5, v16, v6

    if-lez v5, :cond_2

    .line 117
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-wide/from16 v0, v16

    long-to-double v6, v0

    const-wide/high16 v22, 0x4014000000000000L    # 5.0

    div-double v6, v6, v22

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 118
    const-wide/16 v16, 0x0

    goto :goto_0

    .line 123
    .end local v11    # "docsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    .end local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    .end local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    .end local v15    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_5
    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v5, :cond_9

    .line 124
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsAndFreqsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    if-nez v5, :cond_6

    .line 125
    new-instance v5, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    invoke-direct {v5}, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsAndFreqsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    .line 127
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsAndFreqsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->setMergeState(Lorg/apache/lucene/index/MergeState;)V

    .line 129
    const/4 v4, 0x0

    .line 131
    .local v4, "docsAndFreqsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    :cond_7
    :goto_2
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v15

    .restart local v15    # "term":Lorg/apache/lucene/util/BytesRef;
    if-eqz v15, :cond_3

    .line 134
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v4}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v4

    .end local v4    # "docsAndFreqsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    check-cast v4, Lorg/apache/lucene/index/MultiDocsEnum;

    .line 135
    .restart local v4    # "docsAndFreqsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    sget-boolean v5, Lorg/apache/lucene/codecs/TermsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_8

    if-nez v4, :cond_8

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 136
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsAndFreqsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->reset(Lorg/apache/lucene/index/MultiDocsEnum;)Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    .line 137
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/codecs/TermsConsumer;->startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;

    move-result-object v12

    .line 138
    .restart local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->docsAndFreqsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-virtual {v12, v0, v1, v5, v2}, Lorg/apache/lucene/codecs/PostingsConsumer;->merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/FixedBitSet;)Lorg/apache/lucene/codecs/TermStats;

    move-result-object v14

    .line 139
    .restart local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    if-lez v5, :cond_7

    .line 140
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v14}, Lorg/apache/lucene/codecs/TermsConsumer;->finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V

    .line 141
    iget-wide v6, v14, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    add-long v18, v18, v6

    .line 142
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long v16, v16, v6

    .line 143
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long/2addr v8, v6

    .line 144
    const-wide/32 v6, 0xea60

    cmp-long v5, v16, v6

    if-lez v5, :cond_7

    .line 145
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-wide/from16 v0, v16

    long-to-double v6, v0

    const-wide/high16 v22, 0x4014000000000000L    # 5.0

    div-double v6, v6, v22

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 146
    const-wide/16 v16, 0x0

    goto :goto_2

    .line 150
    .end local v4    # "docsAndFreqsEnumIn":Lorg/apache/lucene/index/MultiDocsEnum;
    .end local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    .end local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    .end local v15    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_9
    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v5, :cond_d

    .line 151
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    if-nez v5, :cond_a

    .line 152
    new-instance v5, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    invoke-direct {v5}, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    .line 154
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->setMergeState(Lorg/apache/lucene/index/MergeState;)V

    .line 155
    const/4 v13, 0x0

    .line 156
    .local v13, "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    :cond_b
    :goto_3
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v15

    .restart local v15    # "term":Lorg/apache/lucene/util/BytesRef;
    if-eqz v15, :cond_3

    .line 159
    const/4 v5, 0x0

    const/4 v6, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v13, v6}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v13

    .end local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    check-cast v13, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    .line 160
    .restart local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    sget-boolean v5, Lorg/apache/lucene/codecs/TermsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_c

    if-nez v13, :cond_c

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 161
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    invoke-virtual {v5, v13}, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->reset(Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;)Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    .line 163
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/codecs/TermsConsumer;->startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;

    move-result-object v12

    .line 164
    .restart local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-virtual {v12, v0, v1, v5, v2}, Lorg/apache/lucene/codecs/PostingsConsumer;->merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/FixedBitSet;)Lorg/apache/lucene/codecs/TermStats;

    move-result-object v14

    .line 165
    .restart local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    if-lez v5, :cond_b

    .line 166
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v14}, Lorg/apache/lucene/codecs/TermsConsumer;->finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V

    .line 167
    iget-wide v6, v14, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    add-long v18, v18, v6

    .line 168
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long v16, v16, v6

    .line 169
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long/2addr v8, v6

    .line 170
    const-wide/32 v6, 0xea60

    cmp-long v5, v16, v6

    if-lez v5, :cond_b

    .line 171
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-wide/from16 v0, v16

    long-to-double v6, v0

    const-wide/high16 v22, 0x4014000000000000L    # 5.0

    div-double v6, v6, v22

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 172
    const-wide/16 v16, 0x0

    goto :goto_3

    .line 177
    .end local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    .end local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    .end local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    .end local v15    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_d
    sget-boolean v5, Lorg/apache/lucene/codecs/TermsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_e

    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-eq v0, v5, :cond_e

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 178
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    if-nez v5, :cond_f

    .line 179
    new-instance v5, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    invoke-direct {v5}, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    .line 181
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->setMergeState(Lorg/apache/lucene/index/MergeState;)V

    .line 182
    const/4 v13, 0x0

    .line 183
    .restart local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    :cond_10
    :goto_4
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v15

    .restart local v15    # "term":Lorg/apache/lucene/util/BytesRef;
    if-eqz v15, :cond_3

    .line 186
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v13}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v13

    .end local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    check-cast v13, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    .line 187
    .restart local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    sget-boolean v5, Lorg/apache/lucene/codecs/TermsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_11

    if-nez v13, :cond_11

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 188
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    invoke-virtual {v5, v13}, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->reset(Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;)Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    .line 190
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/apache/lucene/codecs/TermsConsumer;->startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;

    move-result-object v12

    .line 191
    .restart local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/TermsConsumer;->postingsEnum:Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-virtual {v12, v0, v1, v5, v2}, Lorg/apache/lucene/codecs/PostingsConsumer;->merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/FixedBitSet;)Lorg/apache/lucene/codecs/TermStats;

    move-result-object v14

    .line 192
    .restart local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    if-lez v5, :cond_10

    .line 193
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v14}, Lorg/apache/lucene/codecs/TermsConsumer;->finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V

    .line 194
    iget-wide v6, v14, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    add-long v18, v18, v6

    .line 195
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long v16, v16, v6

    .line 196
    iget v5, v14, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v6, v5

    add-long/2addr v8, v6

    .line 197
    const-wide/32 v6, 0xea60

    cmp-long v5, v16, v6

    if-lez v5, :cond_10

    .line 198
    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-wide/from16 v0, v16

    long-to-double v6, v0

    const-wide/high16 v22, 0x4014000000000000L    # 5.0

    div-double v6, v6, v22

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 199
    const-wide/16 v16, 0x0

    goto :goto_4

    .end local v12    # "postingsConsumer":Lorg/apache/lucene/codecs/PostingsConsumer;
    .end local v13    # "postingsEnumIn":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    .end local v14    # "stats":Lorg/apache/lucene/codecs/TermStats;
    :cond_12
    move-wide/from16 v6, v18

    .line 204
    goto/16 :goto_1
.end method

.method public abstract startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
