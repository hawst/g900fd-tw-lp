.class final Lorg/apache/lucene/codecs/compressing/LZ4;
.super Ljava/lang/Object;
.source "LZ4.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;,
        Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;,
        Lorg/apache/lucene/codecs/compressing/LZ4$Match;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final HASH_LOG_HC:I = 0xf

.field static final HASH_TABLE_SIZE_HC:I = 0x8000

.field static final LAST_LITERALS:I = 0x5

.field static final MAX_DISTANCE:I = 0x10000

.field static final MEMORY_USAGE:I = 0xe

.field static final MIN_MATCH:I = 0x4

.field static final OPTIMAL_ML:I = 0x12


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/codecs/compressing/LZ4;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    .line 43
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0([BI)I
    .locals 1

    .prologue
    .line 54
    invoke-static {p0, p1}, Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I

    move-result v0

    return v0
.end method

.method static synthetic access$1(I)I
    .locals 1

    .prologue
    .line 50
    invoke-static {p0}, Lorg/apache/lucene/codecs/compressing/LZ4;->hashHC(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2([BII)Z
    .locals 1

    .prologue
    .line 58
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/codecs/compressing/LZ4;->readIntEquals([BII)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3([BIII)I
    .locals 1

    .prologue
    .line 62
    invoke-static {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/compressing/LZ4;->commonBytes([BIII)I

    move-result v0

    return v0
.end method

.method static synthetic access$4([BIIII)I
    .locals 1

    .prologue
    .line 71
    invoke-static {p0, p1, p2, p3, p4}, Lorg/apache/lucene/codecs/compressing/LZ4;->commonBytesBackward([BIIII)I

    move-result v0

    return v0
.end method

.method private static commonBytes([BIII)I
    .locals 5
    .param p0, "b"    # [B
    .param p1, "o1"    # I
    .param p2, "o2"    # I
    .param p3, "limit"    # I

    .prologue
    .line 63
    sget-boolean v3, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-lt p1, p2, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 64
    :cond_0
    const/4 v0, 0x0

    .local v0, "count":I
    move v2, p2

    .end local p2    # "o2":I
    .local v2, "o2":I
    move v1, p1

    .line 65
    .end local p1    # "o1":I
    .local v1, "o1":I
    :goto_0
    if-ge v2, p3, :cond_2

    add-int/lit8 p1, v1, 0x1

    .end local v1    # "o1":I
    .restart local p1    # "o1":I
    aget-byte v3, p0, v1

    add-int/lit8 p2, v2, 0x1

    .end local v2    # "o2":I
    .restart local p2    # "o2":I
    aget-byte v4, p0, v2

    if-eq v3, v4, :cond_1

    .line 68
    :goto_1
    return v0

    .line 66
    :cond_1
    add-int/lit8 v0, v0, 0x1

    move v2, p2

    .end local p2    # "o2":I
    .restart local v2    # "o2":I
    move v1, p1

    .end local p1    # "o1":I
    .restart local v1    # "o1":I
    goto :goto_0

    :cond_2
    move p2, v2

    .end local v2    # "o2":I
    .restart local p2    # "o2":I
    move p1, v1

    .end local v1    # "o1":I
    .restart local p1    # "o1":I
    goto :goto_1
.end method

.method private static commonBytesBackward([BIIII)I
    .locals 3
    .param p0, "b"    # [B
    .param p1, "o1"    # I
    .param p2, "o2"    # I
    .param p3, "l1"    # I
    .param p4, "l2"    # I

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "count":I
    :goto_0
    if-le p1, p3, :cond_0

    if-le p2, p4, :cond_0

    add-int/lit8 p1, p1, -0x1

    aget-byte v1, p0, p1

    add-int/lit8 p2, p2, -0x1

    aget-byte v2, p0, p2

    if-eq v1, v2, :cond_1

    .line 76
    :cond_0
    return v0

    .line 74
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static compress([BIILorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;)V
    .locals 20
    .param p0, "bytes"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .param p3, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p4, "ht"    # Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    move/from16 v8, p1

    .line 209
    .local v8, "base":I
    add-int v9, p1, p2

    .line 211
    .local v9, "end":I
    add-int/lit8 v16, p1, 0x1

    .end local p1    # "off":I
    .local v16, "off":I
    move/from16 v3, p1

    .line 213
    .local v3, "anchor":I
    const/16 v2, 0x9

    move/from16 v0, p2

    if-le v0, v2, :cond_5

    .line 215
    add-int/lit8 v13, v9, -0x5

    .line 216
    .local v13, "limit":I
    add-int/lit8 v15, v13, -0x4

    .line 217
    .local v15, "matchLimit":I
    move-object/from16 v0, p4

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->reset(I)V

    .line 218
    # getter for: Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashLog:I
    invoke-static/range {p4 .. p4}, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->access$0(Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;)I

    move-result v11

    .line 219
    .local v11, "hashLog":I
    # getter for: Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    invoke-static/range {p4 .. p4}, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->access$1(Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v12

    .local v12, "hashTable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    move/from16 p1, v16

    .line 222
    .end local v16    # "off":I
    .restart local p1    # "off":I
    :goto_0
    move/from16 v0, p1

    if-lt v0, v13, :cond_2

    .line 250
    .end local v11    # "hashLog":I
    .end local v12    # "hashTable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .end local v13    # "limit":I
    .end local v15    # "matchLimit":I
    :cond_0
    :goto_1
    sub-int v14, v9, v3

    .line 251
    .local v14, "literalLen":I
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    const/4 v2, 0x5

    if-ge v14, v2, :cond_4

    move/from16 v0, p2

    if-eq v14, v0, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 237
    .end local v14    # "literalLen":I
    .local v4, "ref":I
    .local v10, "h":I
    .restart local v11    # "hashLog":I
    .restart local v12    # "hashTable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .restart local v13    # "limit":I
    .restart local v15    # "matchLimit":I
    .local v17, "v":I
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 226
    .end local v4    # "ref":I
    .end local v10    # "h":I
    .end local v17    # "v":I
    :cond_2
    move/from16 v0, p1

    if-ge v0, v15, :cond_0

    .line 229
    invoke-static/range {p0 .. p1}, Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I

    move-result v17

    .line 230
    .restart local v17    # "v":I
    move/from16 v0, v17

    invoke-static {v0, v11}, Lorg/apache/lucene/codecs/compressing/LZ4;->hash(II)I

    move-result v10

    .line 231
    .restart local v10    # "h":I
    invoke-interface {v12, v10}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->get(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v2, v0

    add-int v4, v8, v2

    .line 232
    .restart local v4    # "ref":I
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    sub-int v2, p1, v8

    int-to-long v0, v2

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v2

    invoke-interface {v12}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v5

    if-le v2, v5, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 233
    :cond_3
    sub-int v2, p1, v8

    int-to-long v0, v2

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-interface {v12, v10, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    .line 234
    sub-int v2, p1, v4

    const/high16 v5, 0x10000

    if-ge v2, v5, :cond_1

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I

    move-result v2

    move/from16 v0, v17

    if-ne v2, v0, :cond_1

    .line 241
    add-int/lit8 v2, v4, 0x4

    add-int/lit8 v5, p1, 0x4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5, v13}, Lorg/apache/lucene/codecs/compressing/LZ4;->commonBytes([BIII)I

    move-result v2

    add-int/lit8 v6, v2, 0x4

    .local v6, "matchLen":I
    move-object/from16 v2, p0

    move/from16 v5, p1

    move-object/from16 v7, p3

    .line 243
    invoke-static/range {v2 .. v7}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V

    .line 244
    add-int p1, p1, v6

    .line 245
    move/from16 v3, p1

    goto :goto_0

    .line 252
    .end local v4    # "ref":I
    .end local v6    # "matchLen":I
    .end local v10    # "h":I
    .end local v11    # "hashLog":I
    .end local v12    # "hashTable":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .end local v13    # "limit":I
    .end local v15    # "matchLimit":I
    .end local v17    # "v":I
    .restart local v14    # "literalLen":I
    :cond_4
    sub-int v2, v9, v3

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v3, v2, v1}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeLastLiterals([BIILorg/apache/lucene/store/DataOutput;)V

    .line 253
    return-void

    .end local v14    # "literalLen":I
    .end local p1    # "off":I
    .restart local v16    # "off":I
    :cond_5
    move/from16 p1, v16

    .end local v16    # "off":I
    .restart local p1    # "off":I
    goto/16 :goto_1
.end method

.method public static compressHC([BIILorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;)V
    .locals 27
    .param p0, "src"    # [B
    .param p1, "srcOff"    # I
    .param p2, "srcLen"    # I
    .param p3, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p4, "ht"    # Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    add-int v26, p1, p2

    .line 388
    .local v26, "srcEnd":I
    add-int/lit8 v8, v26, -0x5

    .line 390
    .local v8, "matchLimit":I
    move/from16 v24, p1

    .line 391
    .local v24, "sOff":I
    add-int/lit8 v25, v24, 0x1

    .end local v24    # "sOff":I
    .local v25, "sOff":I
    move/from16 v12, v24

    .line 393
    .local v12, "anchor":I
    move-object/from16 v0, p4

    move/from16 v1, p1

    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->reset(I)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->access$0(Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;I)V

    .line 394
    new-instance v21, Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;-><init>(Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 395
    .local v21, "match0":Lorg/apache/lucene/codecs/compressing/LZ4$Match;
    new-instance v22, Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;-><init>(Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 396
    .local v22, "match1":Lorg/apache/lucene/codecs/compressing/LZ4$Match;
    new-instance v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    const/4 v4, 0x0

    invoke-direct {v10, v4}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;-><init>(Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 397
    .local v10, "match2":Lorg/apache/lucene/codecs/compressing/LZ4$Match;
    new-instance v19, Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v4}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;-><init>(Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .local v19, "match3":Lorg/apache/lucene/codecs/compressing/LZ4$Match;
    move/from16 v24, v25

    .line 400
    .end local v25    # "sOff":I
    .restart local v24    # "sOff":I
    :goto_0
    move/from16 v0, v24

    if-lt v0, v8, :cond_0

    .line 528
    sub-int v4, v26, v12

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v12, v4, v1}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeLastLiterals([BIILorg/apache/lucene/store/DataOutput;)V

    .line 529
    return-void

    .line 401
    :cond_0
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    move/from16 v2, v24

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v8, v3}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->insertAndFindBestMatch([BIILorg/apache/lucene/codecs/compressing/LZ4$Match;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 402
    add-int/lit8 v24, v24, 0x1

    .line 403
    goto :goto_0

    .line 407
    :cond_1
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 411
    :goto_1
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    move-object/from16 v0, v22

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    if-ge v4, v12, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 412
    :cond_2
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    if-ge v4, v8, :cond_3

    .line 413
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    add-int/lit8 v6, v4, -0x2

    move-object/from16 v0, v22

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    add-int/lit8 v7, v4, 0x1

    move-object/from16 v0, v22

    iget v9, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move-object/from16 v4, p4

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v10}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->insertAndFindWiderMatch([BIIIILorg/apache/lucene/codecs/compressing/LZ4$Match;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 415
    :cond_3
    move-object/from16 v0, v22

    iget v13, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    move-object/from16 v0, v22

    iget v14, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v15, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    invoke-static/range {v11 .. v16}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V

    .line 416
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v24

    move/from16 v12, v24

    .line 417
    goto :goto_0

    .line 420
    :cond_4
    move-object/from16 v0, v21

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    if-ge v4, v5, :cond_5

    .line 421
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v21

    iget v6, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v5, v6

    if-ge v4, v5, :cond_5

    .line 422
    invoke-static/range {v21 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 425
    :cond_5
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    if-gt v4, v5, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 427
    :cond_6
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    const/4 v5, 0x3

    if-ge v4, v5, :cond_8

    .line 428
    move-object/from16 v0, v22

    invoke-static {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    goto/16 :goto_1

    .line 495
    :cond_7
    move-object/from16 v0, v19

    invoke-static {v0, v10}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 434
    :cond_8
    :goto_2
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    const/16 v5, 0x12

    if-ge v4, v5, :cond_b

    .line 435
    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move/from16 v23, v0

    .line 436
    .local v23, "newMatchLen":I
    const/16 v4, 0x12

    move/from16 v0, v23

    if-le v0, v4, :cond_9

    .line 437
    const/16 v23, 0x12

    .line 439
    :cond_9
    move-object/from16 v0, v22

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    add-int v4, v4, v23

    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    if-le v4, v5, :cond_a

    .line 440
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    iget v5, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v4, v5

    add-int/lit8 v23, v4, -0x4

    .line 442
    :cond_a
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    sub-int v20, v23, v4

    .line 443
    .local v20, "correction":I
    if-lez v20, :cond_b

    .line 444
    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->fix(I)V

    .line 448
    .end local v20    # "correction":I
    .end local v23    # "newMatchLen":I
    :cond_b
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    iget v5, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v4, v5

    if-ge v4, v8, :cond_c

    .line 449
    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    add-int/lit8 v15, v4, -0x3

    iget v0, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move/from16 v16, v0

    iget v0, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move/from16 v18, v0

    move-object/from16 v13, p4

    move-object/from16 v14, p0

    move/from16 v17, v8

    invoke-virtual/range {v13 .. v19}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->insertAndFindWiderMatch([BIIIILorg/apache/lucene/codecs/compressing/LZ4$Match;)Z

    move-result v4

    if-nez v4, :cond_11

    .line 451
    :cond_c
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    if-ge v4, v5, :cond_f

    .line 452
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    const/16 v5, 0x12

    if-ge v4, v5, :cond_10

    .line 453
    move-object/from16 v0, v22

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    const/16 v5, 0x12

    if-le v4, v5, :cond_d

    .line 454
    const/16 v4, 0x12

    move-object/from16 v0, v22

    iput v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 456
    :cond_d
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    if-le v4, v5, :cond_e

    .line 457
    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x4

    move-object/from16 v0, v22

    iput v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 459
    :cond_e
    move-object/from16 v0, v22

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    iget v5, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v6, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v5, v6

    sub-int v20, v4, v5

    .line 460
    .restart local v20    # "correction":I
    if-lez v20, :cond_f

    .line 461
    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->fix(I)V

    .line 468
    .end local v20    # "correction":I
    :cond_f
    :goto_3
    move-object/from16 v0, v22

    iget v13, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    move-object/from16 v0, v22

    iget v14, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v15, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    invoke-static/range {v11 .. v16}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V

    .line 469
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v24

    move/from16 v12, v24

    .line 471
    iget v13, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    iget v14, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    iget v15, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    invoke-static/range {v11 .. v16}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V

    .line 472
    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v24

    move/from16 v12, v24

    .line 473
    goto/16 :goto_0

    .line 464
    :cond_10
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    move-object/from16 v0, v22

    iput v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    goto :goto_3

    .line 476
    :cond_11
    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    add-int/lit8 v5, v5, 0x3

    if-ge v4, v5, :cond_13

    .line 477
    move-object/from16 v0, v19

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    if-lt v4, v5, :cond_7

    .line 478
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    if-ge v4, v5, :cond_12

    .line 479
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    iget v5, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int v20, v4, v5

    .line 480
    .restart local v20    # "correction":I
    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->fix(I)V

    .line 481
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    const/4 v5, 0x4

    if-ge v4, v5, :cond_12

    .line 482
    move-object/from16 v0, v19

    invoke-static {v0, v10}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 486
    .end local v20    # "correction":I
    :cond_12
    move-object/from16 v0, v22

    iget v13, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    move-object/from16 v0, v22

    iget v14, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v15, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    invoke-static/range {v11 .. v16}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V

    .line 487
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v24

    move/from16 v12, v24

    .line 489
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 490
    move-object/from16 v0, v21

    invoke-static {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    goto/16 :goto_1

    .line 500
    :cond_13
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    if-ge v4, v5, :cond_16

    .line 501
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    const/16 v5, 0xf

    if-ge v4, v5, :cond_17

    .line 502
    move-object/from16 v0, v22

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    const/16 v5, 0x12

    if-le v4, v5, :cond_14

    .line 503
    const/16 v4, 0x12

    move-object/from16 v0, v22

    iput v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 505
    :cond_14
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    if-le v4, v5, :cond_15

    .line 506
    invoke-virtual {v10}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x4

    move-object/from16 v0, v22

    iput v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 508
    :cond_15
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v4

    iget v5, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int v20, v4, v5

    .line 509
    .restart local v20    # "correction":I
    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->fix(I)V

    .line 515
    .end local v20    # "correction":I
    :cond_16
    :goto_4
    move-object/from16 v0, v22

    iget v13, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    move-object/from16 v0, v22

    iget v14, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v15, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    move-object/from16 v11, p0

    move-object/from16 v16, p3

    invoke-static/range {v11 .. v16}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V

    .line 516
    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->end()I

    move-result v24

    move/from16 v12, v24

    .line 518
    move-object/from16 v0, v22

    invoke-static {v10, v0}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    .line 519
    move-object/from16 v0, v19

    invoke-static {v0, v10}, Lorg/apache/lucene/codecs/compressing/LZ4;->copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V

    goto/16 :goto_2

    .line 511
    :cond_17
    iget v4, v10, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    move-object/from16 v0, v22

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    sub-int/2addr v4, v5

    move-object/from16 v0, v22

    iput v4, v0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    goto :goto_4
.end method

.method private static copyTo(Lorg/apache/lucene/codecs/compressing/LZ4$Match;Lorg/apache/lucene/codecs/compressing/LZ4$Match;)V
    .locals 1
    .param p0, "m1"    # Lorg/apache/lucene/codecs/compressing/LZ4$Match;
    .param p1, "m2"    # Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    .prologue
    .line 270
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    iput v0, p1, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 271
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    iput v0, p1, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    .line 272
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    iput v0, p1, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    .line 273
    return-void
.end method

.method public static decompress(Lorg/apache/lucene/store/DataInput;I[BI)I
    .locals 11
    .param p0, "compressed"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "decompressedLen"    # I
    .param p2, "dest"    # [B
    .param p3, "dOff"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    array-length v0, p2

    .line 90
    .local v0, "destEnd":I
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v9

    and-int/lit16 v8, v9, 0xff

    .line 91
    .local v8, "token":I
    ushr-int/lit8 v4, v8, 0x4

    .line 93
    .local v4, "literalLen":I
    if-eqz v4, :cond_2

    .line 94
    const/16 v9, 0xf

    if-ne v4, v9, :cond_1

    .line 96
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v3

    .local v3, "len":B
    const/4 v9, -0x1

    if-eq v3, v9, :cond_3

    .line 99
    and-int/lit16 v9, v3, 0xff

    add-int/2addr v4, v9

    .line 101
    .end local v3    # "len":B
    :cond_1
    invoke-virtual {p0, p2, p3, v4}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 102
    add-int/2addr p3, v4

    .line 105
    :cond_2
    if-lt p3, p1, :cond_4

    .line 137
    :goto_1
    return p3

    .line 97
    .restart local v3    # "len":B
    :cond_3
    add-int/lit16 v4, v4, 0xff

    goto :goto_0

    .line 110
    .end local v3    # "len":B
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v9

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v10

    and-int/lit16 v10, v10, 0xff

    shl-int/lit8 v10, v10, 0x8

    or-int v5, v9, v10

    .line 111
    .local v5, "matchDec":I
    sget-boolean v9, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v9, :cond_5

    if-gtz v5, :cond_5

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    .line 113
    :cond_5
    and-int/lit8 v6, v8, 0xf

    .line 114
    .local v6, "matchLen":I
    const/16 v9, 0xf

    if-ne v6, v9, :cond_6

    .line 116
    :goto_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v3

    .local v3, "len":I
    const/4 v9, -0x1

    if-eq v3, v9, :cond_8

    .line 119
    and-int/lit16 v9, v3, 0xff

    add-int/2addr v6, v9

    .line 121
    .end local v3    # "len":I
    :cond_6
    add-int/lit8 v6, v6, 0x4

    .line 124
    add-int/lit8 v9, v6, 0x7

    and-int/lit8 v2, v9, -0x8

    .line 125
    .local v2, "fastLen":I
    if-lt v5, v6, :cond_7

    add-int v9, p3, v2

    if-le v9, v0, :cond_a

    .line 127
    :cond_7
    sub-int v7, p3, v5

    .local v7, "ref":I
    add-int v1, p3, v6

    .local v1, "end":I
    :goto_3
    if-lt p3, v1, :cond_9

    .line 88
    .end local v1    # "end":I
    .end local v7    # "ref":I
    :goto_4
    if-lt p3, p1, :cond_0

    goto :goto_1

    .line 117
    .end local v2    # "fastLen":I
    .restart local v3    # "len":I
    :cond_8
    add-int/lit16 v6, v6, 0xff

    goto :goto_2

    .line 128
    .end local v3    # "len":I
    .restart local v1    # "end":I
    .restart local v2    # "fastLen":I
    .restart local v7    # "ref":I
    :cond_9
    aget-byte v9, p2, v7

    aput-byte v9, p2, p3

    .line 127
    add-int/lit8 v7, v7, 0x1

    add-int/lit8 p3, p3, 0x1

    goto :goto_3

    .line 132
    .end local v1    # "end":I
    .end local v7    # "ref":I
    :cond_a
    sub-int v9, p3, v5

    invoke-static {p2, v9, p2, p3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 133
    add-int/2addr p3, v6

    goto :goto_4
.end method

.method private static encodeLastLiterals([BIILorg/apache/lucene/store/DataOutput;)V
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "anchor"    # I
    .param p2, "literalLen"    # I
    .param p3, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    const/16 v1, 0xf

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    shl-int/lit8 v0, v1, 0x4

    .line 162
    .local v0, "token":I
    invoke-static {p0, v0, p1, p2, p3}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeLiterals([BIIILorg/apache/lucene/store/DataOutput;)V

    .line 163
    return-void
.end method

.method private static encodeLen(ILorg/apache/lucene/store/DataOutput;)V
    .locals 1
    .param p0, "l"    # I
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    :goto_0
    const/16 v0, 0xff

    if-ge p0, v0, :cond_0

    .line 145
    int-to-byte v0, p0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 146
    return-void

    .line 142
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 143
    add-int/lit16 p0, p0, -0xff

    goto :goto_0
.end method

.method private static encodeLiterals([BIIILorg/apache/lucene/store/DataOutput;)V
    .locals 1
    .param p0, "bytes"    # [B
    .param p1, "token"    # I
    .param p2, "anchor"    # I
    .param p3, "literalLen"    # I
    .param p4, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    int-to-byte v0, p1

    invoke-virtual {p4, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 152
    const/16 v0, 0xf

    if-lt p3, v0, :cond_0

    .line 153
    add-int/lit8 v0, p3, -0xf

    invoke-static {v0, p4}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeLen(ILorg/apache/lucene/store/DataOutput;)V

    .line 157
    :cond_0
    invoke-virtual {p4, p0, p2, p3}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 158
    return-void
.end method

.method private static encodeSequence([BIIIILorg/apache/lucene/store/DataOutput;)V
    .locals 6
    .param p0, "bytes"    # [B
    .param p1, "anchor"    # I
    .param p2, "matchRef"    # I
    .param p3, "matchOff"    # I
    .param p4, "matchLen"    # I
    .param p5, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xf

    .line 166
    sub-int v0, p3, p1

    .line 167
    .local v0, "literalLen":I
    sget-boolean v3, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    const/4 v3, 0x4

    if-ge p4, v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 169
    :cond_0
    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    shl-int/lit8 v3, v3, 0x4

    add-int/lit8 v4, p4, -0x4

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    or-int v2, v3, v4

    .line 170
    .local v2, "token":I
    invoke-static {p0, v2, p1, v0, p5}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeLiterals([BIIILorg/apache/lucene/store/DataOutput;)V

    .line 173
    sub-int v1, p3, p2

    .line 174
    .local v1, "matchDec":I
    sget-boolean v3, Lorg/apache/lucene/codecs/compressing/LZ4;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    if-lez v1, :cond_1

    const/high16 v3, 0x10000

    if-lt v1, v3, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 175
    :cond_2
    int-to-byte v3, v1

    invoke-virtual {p5, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 176
    ushr-int/lit8 v3, v1, 0x8

    int-to-byte v3, v3

    invoke-virtual {p5, v3}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 179
    const/16 v3, 0x13

    if-lt p4, v3, :cond_3

    .line 180
    add-int/lit8 v3, p4, -0xf

    add-int/lit8 v3, v3, -0x4

    invoke-static {v3, p5}, Lorg/apache/lucene/codecs/compressing/LZ4;->encodeLen(ILorg/apache/lucene/store/DataOutput;)V

    .line 182
    :cond_3
    return-void
.end method

.method private static hash(II)I
    .locals 2
    .param p0, "i"    # I
    .param p1, "hashBits"    # I

    .prologue
    .line 47
    const v0, -0x61c8864f

    mul-int/2addr v0, p0

    rsub-int/lit8 v1, p1, 0x20

    ushr-int/2addr v0, v1

    return v0
.end method

.method private static hashHC(I)I
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 51
    const/16 v0, 0xf

    invoke-static {p0, v0}, Lorg/apache/lucene/codecs/compressing/LZ4;->hash(II)I

    move-result v0

    return v0
.end method

.method private static readInt([BI)I
    .locals 2
    .param p0, "buf"    # [B
    .param p1, "i"    # I

    .prologue
    .line 55
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method private static readIntEquals([BII)Z
    .locals 2
    .param p0, "buf"    # [B
    .param p1, "i"    # I
    .param p2, "j"    # I

    .prologue
    .line 59
    invoke-static {p0, p1}, Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I

    move-result v0

    invoke-static {p0, p2}, Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
