.class Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;
.super Ljava/lang/Object;
.source "Lucene41PostingsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PendingTerm"
.end annotation


# instance fields
.field public final docStartFP:J

.field public final lastPosBlockOffset:J

.field public final payStartFP:J

.field public final posStartFP:J

.field public final singletonDocID:I

.field public final skipOffset:J


# direct methods
.method public constructor <init>(JJJJJI)V
    .locals 1
    .param p1, "docStartFP"    # J
    .param p3, "posStartFP"    # J
    .param p5, "payStartFP"    # J
    .param p7, "skipOffset"    # J
    .param p9, "lastPosBlockOffset"    # J
    .param p11, "singletonDocID"    # I

    .prologue
    .line 359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 360
    iput-wide p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->docStartFP:J

    .line 361
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->posStartFP:J

    .line 362
    iput-wide p5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->payStartFP:J

    .line 363
    iput-wide p7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->skipOffset:J

    .line 364
    iput-wide p9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->lastPosBlockOffset:J

    .line 365
    iput p11, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->singletonDocID:I

    .line 366
    return-void
.end method
