.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$TermData;
.super Ljava/lang/Object;
.source "SimpleTextFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TermData"
.end annotation


# instance fields
.field public docFreq:I

.field public docsStart:J


# direct methods
.method public constructor <init>(JI)V
    .locals 1
    .param p1, "docsStart"    # J
    .param p3, "docFreq"    # I

    .prologue
    .line 492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 493
    iput-wide p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$TermData;->docsStart:J

    .line 494
    iput p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$TermData;->docFreq:I

    .line 495
    return-void
.end method
