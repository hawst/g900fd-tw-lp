.class public final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;
.super Lorg/apache/lucene/codecs/PostingsFormat;
.source "Lucene41PostingsFormat.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final BLOCK_SIZE:I = 0x80

.field public static final DOC_EXTENSION:Ljava/lang/String; = "doc"

.field public static final PAY_EXTENSION:Ljava/lang/String; = "pay"

.field public static final POS_EXTENSION:Ljava/lang/String; = "pos"


# instance fields
.field private final maxTermBlockSize:I

.field private final minTermBlockSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 357
    const-class v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->$assertionsDisabled:Z

    .line 384
    return-void

    .line 357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 389
    const/16 v0, 0x19

    const/16 v1, 0x30

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;-><init>(II)V

    .line 390
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "minTermBlockSize"    # I
    .param p2, "maxTermBlockSize"    # I

    .prologue
    .line 397
    const-string v0, "Lucene41"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/PostingsFormat;-><init>(Ljava/lang/String;)V

    .line 398
    iput p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->minTermBlockSize:I

    .line 399
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 400
    :cond_0
    iput p2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->maxTermBlockSize:I

    .line 401
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-le p1, p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 402
    :cond_1
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;
    .locals 7
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 411
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 413
    .local v0, "postingsWriter":Lorg/apache/lucene/codecs/PostingsWriterBase;
    const/4 v2, 0x0

    .line 415
    .local v2, "success":Z
    :try_start_0
    new-instance v1, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    .line 417
    iget v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->minTermBlockSize:I

    .line 418
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->maxTermBlockSize:I

    .line 415
    invoke-direct {v1, p1, v0, v3, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/PostingsWriterBase;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    .local v1, "ret":Lorg/apache/lucene/codecs/FieldsConsumer;
    const/4 v2, 0x1

    .line 422
    if-nez v2, :cond_0

    new-array v3, v6, [Ljava/io/Closeable;

    .line 423
    aput-object v0, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 420
    :cond_0
    return-object v1

    .line 421
    .end local v1    # "ret":Lorg/apache/lucene/codecs/FieldsConsumer;
    :catchall_0
    move-exception v3

    .line 422
    if-nez v2, :cond_1

    new-array v4, v6, [Ljava/io/Closeable;

    .line 423
    aput-object v0, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 425
    :cond_1
    throw v3
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;
    .locals 12
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 430
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    .line 431
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 432
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 433
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    .line 434
    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    .line 430
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;)V

    .line 435
    .local v0, "postingsReader":Lorg/apache/lucene/codecs/PostingsReaderBase;
    const/4 v9, 0x0

    .line 437
    .local v9, "success":Z
    :try_start_0
    new-instance v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    .line 438
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 439
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 441
    iget-object v6, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    .line 442
    iget-object v7, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    .line 443
    iget v8, p1, Lorg/apache/lucene/index/SegmentReadState;->termsIndexDivisor:I

    move-object v5, v0

    .line 437
    invoke-direct/range {v1 .. v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/codecs/PostingsReaderBase;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    .local v1, "ret":Lorg/apache/lucene/codecs/FieldsProducer;
    const/4 v9, 0x1

    .line 447
    if-nez v9, :cond_0

    new-array v2, v11, [Ljava/io/Closeable;

    .line 448
    aput-object v0, v2, v10

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 445
    :cond_0
    return-object v1

    .line 446
    .end local v1    # "ret":Lorg/apache/lucene/codecs/FieldsProducer;
    :catchall_0
    move-exception v2

    .line 447
    if-nez v9, :cond_1

    new-array v3, v11, [Ljava/io/Closeable;

    .line 448
    aput-object v0, v3, v10

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 450
    :cond_1
    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 406
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsFormat;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(blocksize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
