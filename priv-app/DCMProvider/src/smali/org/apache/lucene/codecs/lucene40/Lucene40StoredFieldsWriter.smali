.class public final Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;
.super Lorg/apache/lucene/codecs/StoredFieldsWriter;
.source "Lucene40StoredFieldsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final CODEC_NAME_DAT:Ljava/lang/String; = "Lucene40StoredFieldsData"

.field static final CODEC_NAME_IDX:Ljava/lang/String; = "Lucene40StoredFieldsIndex"

.field public static final FIELDS_EXTENSION:Ljava/lang/String; = "fdt"

.field public static final FIELDS_INDEX_EXTENSION:Ljava/lang/String; = "fdx"

.field static final FIELD_IS_BINARY:I = 0x2

.field static final FIELD_IS_NUMERIC_DOUBLE:I = 0x20

.field static final FIELD_IS_NUMERIC_FLOAT:I = 0x18

.field static final FIELD_IS_NUMERIC_INT:I = 0x8

.field static final FIELD_IS_NUMERIC_LONG:I = 0x10

.field static final FIELD_IS_NUMERIC_MASK:I = 0x38

.field static final HEADER_LENGTH_DAT:J

.field static final HEADER_LENGTH_IDX:J

.field private static final MAX_RAW_MERGE_DOCS:I = 0x1060

.field static final VERSION_CURRENT:I = 0x0

.field static final VERSION_START:I = 0x0

.field private static final _NUMERIC_BIT_SHIFT:I = 0x3


# instance fields
.field private final directory:Lorg/apache/lucene/store/Directory;

.field private fieldsStream:Lorg/apache/lucene/store/IndexOutput;

.field private indexStream:Lorg/apache/lucene/store/IndexOutput;

.field private final segment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->$assertionsDisabled:Z

    .line 71
    const-string v0, "Lucene40StoredFieldsIndex"

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_IDX:J

    .line 72
    const-string v0, "Lucene40StoredFieldsData"

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_DAT:J

    .line 255
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;-><init>()V

    .line 89
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 90
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 91
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->segment:Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    .line 95
    .local v0, "success":Z
    :try_start_0
    const-string v1, ""

    const-string v2, "fdt"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 96
    const-string v1, ""

    const-string v2, "fdx"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    .line 98
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    const-string v2, "Lucene40StoredFieldsData"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 99
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    const-string v2, "Lucene40StoredFieldsIndex"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 100
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    sget-wide v2, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_DAT:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :catchall_0
    move-exception v1

    .line 104
    if-nez v0, :cond_1

    .line 105
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->abort()V

    .line 107
    :cond_1
    throw v1

    .line 101
    :cond_2
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    sget-wide v2, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_IDX:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :cond_3
    const/4 v0, 0x1

    .line 104
    if-nez v0, :cond_4

    .line 105
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->abort()V

    .line 108
    :cond_4
    return-void
.end method

.method private copyFieldsNoDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;[I)I
    .locals 8
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p3, "matchingFieldsReader"    # Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;
    .param p4, "rawDocLengths"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    invoke-virtual {p2}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    .line 315
    .local v3, "maxDoc":I
    const/4 v1, 0x0

    .line 316
    .local v1, "docCount":I
    if-eqz p3, :cond_2

    .line 318
    :goto_0
    if-lt v1, v3, :cond_0

    .line 334
    :goto_1
    return v1

    .line 319
    :cond_0
    const/16 v5, 0x1060

    sub-int v6, v3, v1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 320
    .local v2, "len":I
    invoke-virtual {p3, p4, v1, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->rawDocs([III)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 321
    .local v4, "stream":Lorg/apache/lucene/store/IndexInput;
    invoke-virtual {p0, v4, p4, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->addRawDocuments(Lorg/apache/lucene/store/IndexInput;[II)V

    .line 322
    add-int/2addr v1, v2

    .line 323
    iget-object v5, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    mul-int/lit16 v6, v2, 0x12c

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_0

    .line 329
    .end local v2    # "len":I
    .end local v4    # "stream":Lorg/apache/lucene/store/IndexInput;
    :cond_1
    invoke-virtual {p2, v1}, Lorg/apache/lucene/index/AtomicReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 330
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    iget-object v5, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v0, v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/index/FieldInfos;)V

    .line 331
    iget-object v5, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    const-wide v6, 0x4072c00000000000L    # 300.0

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 326
    add-int/lit8 v1, v1, 0x1

    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_2
    if-lt v1, v3, :cond_1

    goto :goto_1
.end method

.method private copyFieldsWithDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;[I)I
    .locals 14
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p3, "matchingFieldsReader"    # Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;
    .param p4, "rawDocLengths"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    const/4 v3, 0x0

    .line 261
    .local v3, "docCount":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v6

    .line 262
    .local v6, "maxDoc":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v5

    .line 263
    .local v5, "liveDocs":Lorg/apache/lucene/util/Bits;
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v10, :cond_0

    if-nez v5, :cond_0

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 264
    :cond_0
    if-eqz p3, :cond_7

    .line 266
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    if-lt v4, v6, :cond_2

    .line 308
    :cond_1
    return v3

    .line 267
    :cond_2
    invoke-interface {v5, v4}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v10

    if-nez v10, :cond_3

    .line 269
    add-int/lit8 v4, v4, 0x1

    .line 270
    goto :goto_0

    .line 274
    :cond_3
    move v8, v4

    .local v8, "start":I
    const/4 v7, 0x0

    .line 276
    .local v7, "numDocs":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 277
    add-int/lit8 v7, v7, 0x1

    .line 278
    if-lt v4, v6, :cond_5

    .line 285
    :goto_1
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v1, v8, v7}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;->rawDocs([III)Lorg/apache/lucene/store/IndexInput;

    move-result-object v9

    .line 286
    .local v9, "stream":Lorg/apache/lucene/store/IndexInput;
    move-object/from16 v0, p4

    invoke-virtual {p0, v9, v0, v7}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->addRawDocuments(Lorg/apache/lucene/store/IndexInput;[II)V

    .line 287
    add-int/2addr v3, v7

    .line 288
    iget-object v10, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    mul-int/lit16 v11, v7, 0x12c

    int-to-double v12, v11

    invoke-virtual {v10, v12, v13}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_0

    .line 279
    .end local v9    # "stream":Lorg/apache/lucene/store/IndexInput;
    :cond_5
    invoke-interface {v5, v4}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v10

    if-nez v10, :cond_6

    .line 280
    add-int/lit8 v4, v4, 0x1

    .line 281
    goto :goto_1

    .line 283
    :cond_6
    const/16 v10, 0x1060

    .line 275
    if-lt v7, v10, :cond_4

    goto :goto_1

    .line 291
    .end local v4    # "j":I
    .end local v7    # "numDocs":I
    .end local v8    # "start":I
    :cond_7
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_2
    if-ge v4, v6, :cond_1

    .line 292
    invoke-interface {v5, v4}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v10

    if-nez v10, :cond_8

    .line 291
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 302
    :cond_8
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/AtomicReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v2

    .line 303
    .local v2, "doc":Lorg/apache/lucene/document/Document;
    iget-object v10, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v2, v10}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/index/FieldInfos;)V

    .line 304
    add-int/lit8 v3, v3, 0x1

    .line 305
    iget-object v10, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    const-wide v12, 0x4072c00000000000L    # 300.0

    invoke-virtual {v10, v12, v13}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_3
.end method


# virtual methods
.method public abort()V
    .locals 6

    .prologue
    .line 129
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 132
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "fdt"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 133
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "fdx"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 131
    invoke-static {v0, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    .line 134
    return-void

    .line 130
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public addRawDocuments(Lorg/apache/lucene/store/IndexInput;[II)V
    .locals 8
    .param p1, "stream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "lengths"    # [I
    .param p3, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    .line 203
    .local v2, "position":J
    move-wide v4, v2

    .line 204
    .local v4, "start":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 208
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    sub-long v6, v2, v4

    invoke-virtual {v1, p1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 209
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 205
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 206
    aget v1, p2, v0

    int-to-long v6, v1

    add-long/2addr v2, v6

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_1
    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 120
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 121
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 125
    return-void

    .line 122
    :catchall_0
    move-exception v0

    .line 123
    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 124
    throw v0
.end method

.method public finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 6
    .param p1, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numDocs"    # I

    .prologue
    .line 214
    sget-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->HEADER_LENGTH_IDX:J

    int-to-long v2, p2

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 220
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fdx size mismatch: docCount is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but fdx file size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " file="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; now aborting this merge to prevent index corruption"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    return-void
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;)I
    .locals 10
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    const/4 v0, 0x0

    .line 227
    .local v0, "docCount":I
    const/16 v8, 0x1060

    new-array v6, v8, [I

    .line 228
    .local v6, "rawDocLengths":[I
    const/4 v2, 0x0

    .line 230
    .local v2, "idx":I
    iget-object v8, p1, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_0

    .line 249
    iget-object v8, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v8, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V

    .line 250
    return v0

    .line 230
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/AtomicReader;

    .line 231
    .local v7, "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v9, p1, Lorg/apache/lucene/index/MergeState;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idx":I
    .local v3, "idx":I
    aget-object v5, v9, v2

    .line 232
    .local v5, "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v4, 0x0

    .line 233
    .local v4, "matchingFieldsReader":Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;
    if-eqz v5, :cond_1

    .line 234
    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentReader;->getFieldsReader()Lorg/apache/lucene/codecs/StoredFieldsReader;

    move-result-object v1

    .line 236
    .local v1, "fieldsReader":Lorg/apache/lucene/codecs/StoredFieldsReader;
    if-eqz v1, :cond_1

    instance-of v9, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;

    if-eqz v9, :cond_1

    move-object v4, v1

    .line 237
    check-cast v4, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;

    .line 241
    .end local v1    # "fieldsReader":Lorg/apache/lucene/codecs/StoredFieldsReader;
    :cond_1
    invoke-virtual {v7}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 243
    invoke-direct {p0, p1, v7, v4, v6}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->copyFieldsWithDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;[I)I

    move-result v9

    add-int/2addr v0, v9

    move v2, v3

    .line 244
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0

    .line 246
    .end local v2    # "idx":I
    .restart local v3    # "idx":I
    :cond_2
    invoke-direct {p0, p1, v7, v4, v6}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->copyFieldsNoDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;[I)I

    move-result v9

    add-int/2addr v0, v9

    move v2, v3

    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0
.end method

.method public startDocument(I)V
    .locals 4
    .param p1, "numStoredFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->indexStream:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 116
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 117
    return-void
.end method

.method public writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/IndexableField;)V
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "field"    # Lorg/apache/lucene/index/IndexableField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    iget v5, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 138
    const/4 v0, 0x0

    .line 146
    .local v0, "bits":I
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v2

    .line 147
    .local v2, "number":Ljava/lang/Number;
    if-eqz v2, :cond_6

    .line 148
    instance-of v4, v2, Ljava/lang/Byte;

    if-nez v4, :cond_0

    instance-of v4, v2, Ljava/lang/Short;

    if-nez v4, :cond_0

    instance-of v4, v2, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 149
    :cond_0
    or-int/lit8 v0, v0, 0x8

    .line 159
    :goto_0
    const/4 v3, 0x0

    .line 160
    .local v3, "string":Ljava/lang/String;
    const/4 v1, 0x0

    .line 174
    .local v1, "bytes":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    int-to-byte v5, v0

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 176
    if-eqz v1, :cond_8

    .line 177
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    iget v5, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 178
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    iget-object v5, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v7, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v4, v5, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 194
    :goto_2
    return-void

    .line 150
    .end local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v3    # "string":Ljava/lang/String;
    :cond_2
    instance-of v4, v2, Ljava/lang/Long;

    if-eqz v4, :cond_3

    .line 151
    or-int/lit8 v0, v0, 0x10

    .line 152
    goto :goto_0

    :cond_3
    instance-of v4, v2, Ljava/lang/Float;

    if-eqz v4, :cond_4

    .line 153
    or-int/lit8 v0, v0, 0x18

    .line 154
    goto :goto_0

    :cond_4
    instance-of v4, v2, Ljava/lang/Double;

    if-eqz v4, :cond_5

    .line 155
    or-int/lit8 v0, v0, 0x20

    .line 156
    goto :goto_0

    .line 157
    :cond_5
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cannot store numeric type "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 162
    :cond_6
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->binaryValue()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 163
    .restart local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_7

    .line 164
    or-int/lit8 v0, v0, 0x2

    .line 165
    const/4 v3, 0x0

    .line 166
    .restart local v3    # "string":Ljava/lang/String;
    goto :goto_1

    .line 167
    .end local v3    # "string":Ljava/lang/String;
    :cond_7
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v3

    .line 168
    .restart local v3    # "string":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 169
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "field "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is stored but does not have binaryValue, stringValue nor numericValue"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 179
    :cond_8
    if-eqz v3, :cond_9

    .line 180
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 182
    :cond_9
    instance-of v4, v2, Ljava/lang/Byte;

    if-nez v4, :cond_a

    instance-of v4, v2, Ljava/lang/Short;

    if-nez v4, :cond_a

    instance-of v4, v2, Ljava/lang/Integer;

    if-eqz v4, :cond_b

    .line 183
    :cond_a
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    goto :goto_2

    .line 184
    :cond_b
    instance-of v4, v2, Ljava/lang/Long;

    if-eqz v4, :cond_c

    .line 185
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    goto/16 :goto_2

    .line 186
    :cond_c
    instance-of v4, v2, Ljava/lang/Float;

    if-eqz v4, :cond_d

    .line 187
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v5

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    goto/16 :goto_2

    .line 188
    :cond_d
    instance-of v4, v2, Ljava/lang/Double;

    if-eqz v4, :cond_e

    .line 189
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    goto/16 :goto_2

    .line 191
    :cond_e
    new-instance v4, Ljava/lang/AssertionError;

    const-string v5, "Cannot get here"

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
.end method
