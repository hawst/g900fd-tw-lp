.class Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;
.super Lorg/apache/lucene/codecs/DocValuesConsumer;
.source "PerFieldDocValuesFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FieldsWriter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final formats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/codecs/DocValuesFormat;",
            "Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;",
            ">;"
        }
    .end annotation
.end field

.field private final segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

.field private final suffixes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 1
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;

    .prologue
    .line 99
    iput-object p1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer;-><init>()V

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->formats:Ljava/util/Map;

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    .line 100
    iput-object p2, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    .line 101
    return-void
.end method

.method private getInstance(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 9
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    iget-object v7, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->getDocValuesFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v1

    .line 125
    .local v1, "format":Lorg/apache/lucene/codecs/DocValuesFormat;
    if-nez v1, :cond_0

    .line 126
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "invalid null DocValuesFormat for field=\""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 128
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/codecs/DocValuesFormat;->getName()Ljava/lang/String;

    move-result-object v2

    .line 130
    .local v2, "formatName":Ljava/lang/String;
    sget-object v6, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->PER_FIELD_FORMAT_KEY:Ljava/lang/String;

    invoke-virtual {p1, v6, v2}, Lorg/apache/lucene/index/FieldInfo;->putAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 131
    .local v3, "previousValue":Ljava/lang/String;
    sget-boolean v6, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    if-eqz v3, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "formatName="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " prevValue="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 135
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->formats:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;

    .line 136
    .local v0, "consumer":Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;
    if-nez v0, :cond_3

    .line 140
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 141
    .local v5, "suffix":Ljava/lang/Integer;
    if-nez v5, :cond_2

    .line 142
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 146
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    invoke-interface {v6, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v6, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 149
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    .line 150
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->getSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 148
    invoke-static {v6, v7, v8}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->getFullSegmentSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 151
    .local v4, "segmentSuffix":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;

    .end local v0    # "consumer":Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;
    invoke-direct {v0}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;-><init>()V

    .line 152
    .restart local v0    # "consumer":Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;
    new-instance v6, Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    invoke-direct {v6, v7, v4}, Lorg/apache/lucene/index/SegmentWriteState;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lorg/apache/lucene/codecs/DocValuesFormat;->fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v6

    iput-object v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;->consumer:Lorg/apache/lucene/codecs/DocValuesConsumer;

    .line 153
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;->suffix:I

    .line 154
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->formats:Ljava/util/Map;

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    .end local v4    # "segmentSuffix":Ljava/lang/String;
    :goto_1
    sget-object v6, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->PER_FIELD_SUFFIX_KEY:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lorg/apache/lucene/index/FieldInfo;->putAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 162
    sget-boolean v6, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    if-eqz v3, :cond_5

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 144
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 157
    .end local v5    # "suffix":Ljava/lang/Integer;
    :cond_3
    sget-boolean v6, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 158
    :cond_4
    iget v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;->suffix:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .restart local v5    # "suffix":Ljava/lang/Integer;
    goto :goto_1

    .line 166
    :cond_5
    iget-object v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;->consumer:Lorg/apache/lucene/codecs/DocValuesConsumer;

    return-object v6
.end method


# virtual methods
.method public addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->getInstance(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 111
    return-void
.end method

.method public addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->getInstance(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 106
    return-void
.end method

.method public addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrd":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->getInstance(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 116
    return-void
.end method

.method public addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrdCount":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    .local p4, "ords":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->getInstance(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/DocValuesConsumer;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 121
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;->formats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V

    .line 173
    return-void
.end method
