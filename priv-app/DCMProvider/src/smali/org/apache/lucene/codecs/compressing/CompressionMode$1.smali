.class Lorg/apache/lucene/codecs/compressing/CompressionMode$1;
.super Lorg/apache/lucene/codecs/compressing/CompressionMode;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressionMode;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public newCompressor()Lorg/apache/lucene/codecs/compressing/Compressor;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4FastCompressor;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4FastCompressor;-><init>()V

    return-object v0
.end method

.method public newDecompressor()Lorg/apache/lucene/codecs/compressing/Decompressor;
    .locals 1

    .prologue
    .line 53
    # getter for: Lorg/apache/lucene/codecs/compressing/CompressionMode;->LZ4_DECOMPRESSOR:Lorg/apache/lucene/codecs/compressing/Decompressor;
    invoke-static {}, Lorg/apache/lucene/codecs/compressing/CompressionMode;->access$0()Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "FAST"

    return-object v0
.end method
