.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
.super Ljava/lang/Object;
.source "CompressingTermVectorsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FieldData"
.end annotation


# instance fields
.field final fieldNum:I

.field final flags:I

.field final freqs:[I

.field final hasOffsets:Z

.field final hasPayloads:Z

.field final hasPositions:Z

.field final numTerms:I

.field final offStart:I

.field ord:I

.field final payStart:I

.field final posStart:I

.field final prefixLengths:[I

.field final suffixLengths:[I

.field final synthetic this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

.field totalPositions:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;IIZZZIII)V
    .locals 3
    .param p2, "fieldNum"    # I
    .param p3, "numTerms"    # I
    .param p4, "positions"    # Z
    .param p5, "offsets"    # Z
    .param p6, "payloads"    # Z
    .param p7, "posStart"    # I
    .param p8, "offStart"    # I
    .param p9, "payStart"    # I

    .prologue
    const/4 v1, 0x0

    .line 147
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->fieldNum:I

    .line 149
    iput p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    .line 150
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    .line 151
    iput-boolean p5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    .line 152
    iput-boolean p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    .line 153
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    move v2, v0

    :goto_0
    if-eqz p5, :cond_1

    const/4 v0, 0x2

    :goto_1
    or-int/2addr v2, v0

    if-eqz p6, :cond_2

    const/4 v0, 0x4

    :goto_2
    or-int/2addr v0, v2

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    .line 154
    new-array v0, p3, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    .line 155
    new-array v0, p3, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->prefixLengths:[I

    .line 156
    new-array v0, p3, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->suffixLengths:[I

    .line 157
    iput p7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    .line 158
    iput p8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    .line 159
    iput p9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    .line 160
    iput v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    .line 161
    iput v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->ord:I

    .line 162
    return-void

    :cond_0
    move v2, v1

    .line 153
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method addPosition(IIII)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "startOffset"    # I
    .param p3, "length"    # I
    .param p4, "payloadLength"    # I

    .prologue
    .line 170
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    if-eqz v1, :cond_1

    .line 171
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$1(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V

    .line 174
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v2, v3

    aput p1, v1, v2

    .line 176
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    if-eqz v1, :cond_3

    .line 177
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$2(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    array-length v2, v2

    if-ne v1, v2, :cond_2

    .line 178
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v1, v2

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    .line 179
    .local v0, "newLength":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$2(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$3(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V

    .line 180
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$4(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$5(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V

    .line 182
    .end local v0    # "newLength":I
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$2(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v2, v3

    aput p2, v1, v2

    .line 183
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$4(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v2, v3

    aput p3, v1, v2

    .line 185
    :cond_3
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    if-eqz v1, :cond_5

    .line 186
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$6(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    array-length v2, v2

    if-ne v1, v2, :cond_4

    .line 187
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$6(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$7(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V

    .line 189
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->access$6(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/2addr v2, v3

    aput p4, v1, v2

    .line 191
    :cond_5
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    .line 192
    return-void
.end method

.method addTerm(III)V
    .locals 2
    .param p1, "freq"    # I
    .param p2, "prefixLength"    # I
    .param p3, "suffixLength"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->ord:I

    aput p1, v0, v1

    .line 165
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->prefixLengths:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->ord:I

    aput p2, v0, v1

    .line 166
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->suffixLengths:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->ord:I

    aput p3, v0, v1

    .line 167
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->ord:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->ord:I

    .line 168
    return-void
.end method
