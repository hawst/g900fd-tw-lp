.class public final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsBaseFormat;
.super Lorg/apache/lucene/codecs/PostingsBaseFormat;
.source "Lucene41PostingsBaseFormat.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "Lucene41"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/PostingsBaseFormat;-><init>(Ljava/lang/String;)V

    .line 40
    return-void
.end method


# virtual methods
.method public postingsReaderBase(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/PostingsReaderBase;
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;)V

    return-object v0
.end method

.method public postingsWriterBase(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/PostingsWriterBase;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;)V

    return-object v0
.end method
