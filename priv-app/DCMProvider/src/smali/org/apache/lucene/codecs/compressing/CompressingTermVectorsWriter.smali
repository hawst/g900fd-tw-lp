.class public final Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;
.super Lorg/apache/lucene/codecs/TermVectorsWriter;
.source "CompressingTermVectorsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;,
        Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BLOCK_SIZE:I = 0x40

.field static final CODEC_SFX_DAT:Ljava/lang/String; = "Data"

.field static final CODEC_SFX_IDX:Ljava/lang/String; = "Index"

.field static final FLAGS_BITS:I

.field static final MAX_DOCUMENTS_PER_CHUNK:I = 0x80

.field static final OFFSETS:I = 0x2

.field static final PAYLOADS:I = 0x4

.field static final POSITIONS:I = 0x1

.field static final VECTORS_EXTENSION:Ljava/lang/String; = "tvd"

.field static final VECTORS_INDEX_EXTENSION:Ljava/lang/String; = "tvx"

.field static final VERSION_CURRENT:I

.field static final VERSION_START:I


# instance fields
.field private final chunkSize:I

.field private final compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field private final compressor:Lorg/apache/lucene/codecs/compressing/Compressor;

.field private curDoc:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

.field private curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

.field private final lastTerm:Lorg/apache/lucene/util/BytesRef;

.field private lengthsBuf:[I

.field private numDocs:I

.field private final payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

.field private payloadLengthsBuf:[I

.field private final pendingDocs:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;",
            ">;"
        }
    .end annotation
.end field

.field private positionsBuf:[I

.field private final segment:Ljava/lang/String;

.field private final segmentSuffix:Ljava/lang/String;

.field private startOffsetsBuf:[I

.field private final termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

.field private vectorsStream:Lorg/apache/lucene/store/IndexOutput;

.field private final writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    .line 76
    const-wide/16 v0, 0x7

    invoke-static {v0, v1}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    sput v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->FLAGS_BITS:I

    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V
    .locals 8
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "segmentSuffix"    # Ljava/lang/String;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "formatName"    # Ljava/lang/String;
    .param p6, "compressionMode"    # Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .param p7, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;-><init>()V

    .line 208
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 209
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 210
    iget-object v4, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segment:Ljava/lang/String;

    .line 211
    iput-object p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segmentSuffix:Ljava/lang/String;

    .line 212
    iput-object p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 213
    invoke-virtual {p6}, Lorg/apache/lucene/codecs/compressing/CompressionMode;->newCompressor()Lorg/apache/lucene/codecs/compressing/Compressor;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->compressor:Lorg/apache/lucene/codecs/compressing/Compressor;

    .line 214
    iput p7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->chunkSize:I

    .line 216
    const/4 v4, 0x0

    iput v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    .line 217
    new-instance v4, Ljava/util/ArrayDeque;

    invoke-direct {v4}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    .line 218
    new-instance v4, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    const/4 v5, 0x1

    invoke-static {p7, v5}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    .line 219
    new-instance v4, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    .line 220
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    const/16 v5, 0x1e

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    .line 222
    const/4 v3, 0x0

    .line 223
    .local v3, "success":Z
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segment:Ljava/lang/String;

    const-string v5, "tvx"

    invoke-static {v4, p3, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    .line 225
    .local v2, "indexStream":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segment:Ljava/lang/String;

    const-string v5, "tvd"

    invoke-static {v4, p3, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 227
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "Index"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 228
    .local v1, "codecNameIdx":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "Data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    .local v0, "codecNameDat":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v2, v1, v4}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 230
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 231
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    .end local v0    # "codecNameDat":Ljava/lang/String;
    .end local v1    # "codecNameIdx":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 248
    if-nez v3, :cond_1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 249
    aput-object v2, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->abort()V

    .line 252
    :cond_1
    throw v4

    .line 232
    .restart local v0    # "codecNameDat":Ljava/lang/String;
    .restart local v1    # "codecNameIdx":Ljava/lang/String;
    :cond_2
    :try_start_1
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    invoke-static {v1}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 234
    :cond_3
    new-instance v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    invoke-direct {v4, v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;-><init>(Lorg/apache/lucene/store/IndexOutput;)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    .line 235
    const/4 v2, 0x0

    .line 237
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 238
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v4, p7}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 239
    new-instance v4, Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    const/16 v6, 0x40

    invoke-direct {v4, v5, v6}, Lorg/apache/lucene/util/packed/BlockPackedWriter;-><init>(Lorg/apache/lucene/store/DataOutput;I)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    .line 241
    const/16 v4, 0x400

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    .line 242
    const/16 v4, 0x400

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    .line 243
    const/16 v4, 0x400

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    .line 244
    const/16 v4, 0x400

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    const/4 v3, 0x1

    .line 248
    if-nez v3, :cond_4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 249
    aput-object v2, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 250
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->abort()V

    .line 253
    :cond_4
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    return-void
.end method

.method static synthetic access$4(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    return-object v0
.end method

.method static synthetic access$5(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    return-void
.end method

.method static synthetic access$6(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;)[I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    return-object v0
.end method

.method static synthetic access$7(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;[I)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    return-void
.end method

.method private addDocData(I)Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .locals 9
    .param p1, "numVectorFields"    # I

    .prologue
    const/4 v3, 0x0

    .line 117
    const/4 v7, 0x0

    .line 118
    .local v7, "last":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;>;"
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 126
    :goto_0
    if-nez v7, :cond_2

    .line 127
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    move-object v1, p0

    move v2, p1

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;IIII)V

    .line 134
    .local v0, "doc":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 135
    return-object v0

    .line 119
    .end local v0    # "doc":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 120
    .restart local v0    # "doc":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v1, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    iget-object v1, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "last":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    check-cast v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 122
    .restart local v7    # "last":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    goto :goto_0

    .line 129
    .end local v0    # "doc":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    :cond_2
    iget v2, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    iget-boolean v1, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    if-eqz v1, :cond_4

    iget v1, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    :goto_2
    add-int v8, v2, v1

    .line 130
    .local v8, "posStart":I
    iget v2, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    iget-boolean v1, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    if-eqz v1, :cond_5

    iget v1, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    :goto_3
    add-int v4, v2, v1

    .line 131
    .local v4, "offStart":I
    iget v1, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    iget-boolean v2, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    if-eqz v2, :cond_3

    iget v3, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    :cond_3
    add-int v5, v1, v3

    .line 132
    .local v5, "payStart":I
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    move-object v1, p0

    move v2, p1

    move v3, v8

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;IIII)V

    .restart local v0    # "doc":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    goto :goto_1

    .end local v0    # "doc":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v4    # "offStart":I
    .end local v5    # "payStart":I
    .end local v8    # "posStart":I
    :cond_4
    move v1, v3

    .line 129
    goto :goto_2

    .restart local v8    # "posStart":I
    :cond_5
    move v1, v3

    .line 130
    goto :goto_3
.end method

.method private flush()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 333
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->size()I

    move-result v0

    .line 334
    .local v0, "chunkDocs":I
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-gtz v0, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4, v0}, Ljava/lang/AssertionError;-><init>(I)V

    throw v4

    .line 337
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v4, v0, v6, v7}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->writeIndex(IJ)V

    .line 339
    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    sub-int v1, v4, v0

    .line 340
    .local v1, "docBase":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v4, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 341
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 344
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushNumFields(I)I

    move-result v3

    .line 346
    .local v3, "totalFields":I
    if-lez v3, :cond_1

    .line 348
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushFieldNums()[I

    move-result-object v2

    .line 350
    .local v2, "fieldNums":[I
    invoke-direct {p0, v3, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushFields(I[I)V

    .line 352
    invoke-direct {p0, v3, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushFlags(I[I)V

    .line 354
    invoke-direct {p0, v3}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushNumTerms(I)V

    .line 356
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushTermLengths()V

    .line 358
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushTermFreqs()V

    .line 360
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushPositions()V

    .line 362
    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushOffsets([I)V

    .line 364
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flushPayloadLengths()V

    .line 367
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->compressor:Lorg/apache/lucene/codecs/compressing/Compressor;

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v5, v5, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v6, v6, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v4, v5, v8, v6, v7}, Lorg/apache/lucene/codecs/compressing/Compressor;->compress([BIILorg/apache/lucene/store/DataOutput;)V

    .line 371
    .end local v2    # "fieldNums":[I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->clear()V

    .line 372
    iput-object v9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curDoc:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 373
    iput-object v9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 374
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iput v8, v4, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    .line 375
    return-void
.end method

.method private flushFieldNums()[I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    new-instance v6, Ljava/util/TreeSet;

    invoke-direct {v6}, Ljava/util/TreeSet;-><init>()V

    .line 397
    .local v6, "fieldNums":Ljava/util/SortedSet;, "Ljava/util/SortedSet<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v14}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_1

    .line 403
    invoke-interface {v6}, Ljava/util/SortedSet;->size()I

    move-result v11

    .line 404
    .local v11, "numDistinctFields":I
    sget-boolean v14, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v14, :cond_2

    if-gtz v11, :cond_2

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 397
    .end local v11    # "numDistinctFields":I
    :cond_1
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 398
    .local v3, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v15, v3, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v15}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 399
    .local v4, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v0, v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->fieldNum:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v6, v0}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 405
    .end local v3    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v4    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .restart local v11    # "numDistinctFields":I
    :cond_2
    invoke-interface {v6}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    int-to-long v14, v14

    invoke-static {v14, v15}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v2

    .line 406
    .local v2, "bitsRequired":I
    add-int/lit8 v14, v11, -0x1

    const/4 v15, 0x7

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v14

    shl-int/lit8 v14, v14, 0x5

    or-int v12, v14, v2

    .line 407
    .local v12, "token":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    int-to-byte v15, v12

    invoke-virtual {v14, v15}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 408
    add-int/lit8 v14, v11, -0x1

    const/4 v15, 0x7

    if-lt v14, v15, :cond_3

    .line 409
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    add-int/lit8 v15, v11, -0x1

    add-int/lit8 v15, v15, -0x7

    invoke-virtual {v14, v15}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 411
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    sget-object v15, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-interface {v6}, Ljava/util/SortedSet;->size()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v14, v15, v0, v2, v1}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v13

    .line 412
    .local v13, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    invoke-interface {v6}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_4

    .line 415
    invoke-virtual {v13}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 417
    invoke-interface {v6}, Ljava/util/SortedSet;->size()I

    move-result v14

    new-array v7, v14, [I

    .line 418
    .local v7, "fns":[I
    const/4 v8, 0x0

    .line 419
    .local v8, "i":I
    invoke-interface {v6}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_5

    .line 422
    return-object v7

    .line 412
    .end local v7    # "fns":[I
    .end local v8    # "i":I
    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 413
    .local v5, "fieldNum":Ljava/lang/Integer;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v15

    int-to-long v0, v15

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v13, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    goto :goto_1

    .line 419
    .end local v5    # "fieldNum":Ljava/lang/Integer;
    .restart local v7    # "fns":[I
    .restart local v8    # "i":I
    :cond_5
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 420
    .local v10, "key":Ljava/lang/Integer;
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "i":I
    .local v9, "i":I
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v15

    aput v15, v7, v8

    move v8, v9

    .end local v9    # "i":I
    .restart local v8    # "i":I
    goto :goto_2
.end method

.method private flushFields(I[I)V
    .locals 8
    .param p1, "totalFields"    # I
    .param p2, "fieldNums"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    sget-object v5, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    array-length v6, p2

    add-int/lit8 v6, v6, -0x1

    int-to-long v6, v6

    invoke-static {v6, v7}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v6

    const/4 v7, 0x1

    invoke-static {v4, v5, p1, v6, v7}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v3

    .line 427
    .local v3, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 434
    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 435
    return-void

    .line 427
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 428
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v5}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 429
    .local v1, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v6, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->fieldNum:I

    invoke-static {p2, v6}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v2

    .line 430
    .local v2, "fieldNumIndex":I
    sget-boolean v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    if-gez v2, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 431
    :cond_2
    int-to-long v6, v2

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    goto :goto_0
.end method

.method private flushFlags(I[I)V
    .locals 12
    .param p1, "totalFields"    # I
    .param p2, "fieldNums"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 439
    const/4 v5, 0x1

    .line 440
    .local v5, "nonChangingFlags":Z
    array-length v7, p2

    new-array v2, v7, [I

    .line 441
    .local v2, "fieldFlags":[I
    const/4 v7, -0x1

    invoke-static {v2, v7}, Ljava/util/Arrays;->fill([II)V

    .line 443
    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v7}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 456
    :goto_0
    if-eqz v5, :cond_8

    .line 458
    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 459
    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    sget-object v8, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    array-length v9, v2

    sget v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->FLAGS_BITS:I

    const/4 v11, 0x1

    invoke-static {v7, v8, v9, v10, v11}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v6

    .line 460
    .local v6, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    array-length v8, v2

    const/4 v7, 0x0

    :goto_1
    if-lt v7, v8, :cond_5

    .line 464
    sget-boolean v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_7

    invoke-virtual {v6}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->ord()I

    move-result v7

    array-length v8, v2

    add-int/lit8 v8, v8, -0x1

    if-eq v7, v8, :cond_7

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 443
    .end local v6    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 444
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v8, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v8}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 445
    .local v1, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v9, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->fieldNum:I

    invoke-static {p2, v9}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v3

    .line 446
    .local v3, "fieldNumOff":I
    sget-boolean v9, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v9, :cond_3

    if-gez v3, :cond_3

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 447
    :cond_3
    aget v9, v2, v3

    const/4 v10, -0x1

    if-ne v9, v10, :cond_4

    .line 448
    iget v9, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    aput v9, v2, v3

    goto :goto_2

    .line 449
    :cond_4
    aget v9, v2, v3

    iget v10, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    if-eq v9, v10, :cond_2

    .line 450
    const/4 v5, 0x0

    .line 451
    goto :goto_0

    .line 460
    .end local v0    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v1    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .end local v3    # "fieldNumOff":I
    .restart local v6    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_5
    aget v4, v2, v7

    .line 461
    .local v4, "flags":I
    sget-boolean v9, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v9, :cond_6

    if-gez v4, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 462
    :cond_6
    int-to-long v10, v4

    invoke-virtual {v6, v10, v11}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 460
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 465
    .end local v4    # "flags":I
    :cond_7
    invoke-virtual {v6}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 478
    :goto_3
    return-void

    .line 468
    .end local v6    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_8
    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 469
    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    sget-object v8, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    sget v9, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->FLAGS_BITS:I

    const/4 v10, 0x1

    invoke-static {v7, v8, p1, v9, v10}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v6

    .line 470
    .restart local v6    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v7}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_a

    .line 475
    sget-boolean v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_b

    invoke-virtual {v6}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->ord()I

    move-result v7

    add-int/lit8 v8, p1, -0x1

    if-eq v7, v8, :cond_b

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 470
    :cond_a
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 471
    .restart local v0    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v8, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v8}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 472
    .restart local v1    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v9, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    int-to-long v10, v9

    invoke-virtual {v6, v10, v11}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    goto :goto_4

    .line 476
    .end local v0    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v1    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    :cond_b
    invoke-virtual {v6}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    goto :goto_3
.end method

.method private flushNumFields(I)I
    .locals 8
    .param p1, "chunkDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 379
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    iget v1, v3, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->numFields:I

    .line 380
    .local v1, "numFields":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 390
    .end local v1    # "numFields":I
    :goto_0
    return v1

    .line 383
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 384
    const/4 v2, 0x0

    .line 385
    .local v2, "totalFields":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 389
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    move v1, v2

    .line 390
    goto :goto_0

    .line 385
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 386
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->numFields:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 387
    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->numFields:I

    add-int/2addr v2, v4

    goto :goto_1
.end method

.method private flushNumTerms(I)V
    .locals 10
    .param p1, "totalFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 481
    const/4 v3, 0x0

    .line 482
    .local v3, "maxNumTerms":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v5}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 487
    int-to-long v6, v3

    invoke-static {v6, v7}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    .line 488
    .local v0, "bitsRequired":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 490
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    sget-object v6, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    const/4 v7, 0x1

    .line 489
    invoke-static {v5, v6, p1, v0, v7}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v4

    .line 491
    .local v4, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v5}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 496
    sget-boolean v5, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->ord()I

    move-result v5

    add-int/lit8 v6, p1, -0x1

    if-eq v5, v6, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 482
    .end local v0    # "bitsRequired":I
    .end local v4    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 483
    .local v1, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v6, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v6}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 484
    .local v2, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v7, v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    or-int/2addr v3, v7

    goto :goto_0

    .line 491
    .end local v1    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v2    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .restart local v0    # "bitsRequired":I
    .restart local v4    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 492
    .restart local v1    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v6, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v6}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 493
    .restart local v2    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v7, v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    int-to-long v8, v7

    invoke-virtual {v4, v8, v9}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    goto :goto_1

    .line 497
    .end local v1    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v2    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    :cond_4
    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 498
    return-void
.end method

.method private flushOffsets([I)V
    .locals 26
    .param p1, "fieldNums"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 555
    const/4 v9, 0x0

    .line 556
    .local v9, "hasOffsets":Z
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v0, v0, [J

    move-object/from16 v19, v0

    .line 557
    .local v19, "sumPos":[J
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v0, v0, [J

    move-object/from16 v18, v0

    .line 558
    .local v18, "sumOffsets":[J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-nez v21, :cond_1

    .line 582
    if-nez v9, :cond_5

    .line 637
    :goto_0
    return-void

    .line 558
    :cond_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 559
    .local v6, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v0, v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_0

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 560
    .local v7, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget-boolean v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    move/from16 v22, v0

    or-int v9, v9, v22

    .line 561
    iget-boolean v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    move/from16 v22, v0

    if-eqz v22, :cond_2

    iget-boolean v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    move/from16 v22, v0

    if-eqz v22, :cond_2

    .line 562
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->fieldNum:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v8

    .line 563
    .local v8, "fieldNumOff":I
    const/4 v12, 0x0

    .line 564
    .local v12, "pos":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-lt v10, v0, :cond_3

    .line 577
    sget-boolean v22, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v22, :cond_2

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-eq v12, v0, :cond_2

    new-instance v20, Ljava/lang/AssertionError;

    invoke-direct/range {v20 .. v20}, Ljava/lang/AssertionError;-><init>()V

    throw v20

    .line 565
    :cond_3
    const/16 v16, 0x0

    .line 566
    .local v16, "previousPos":I
    const/4 v15, 0x0

    .line 567
    .local v15, "previousOff":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_2
    iget-object v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    move-object/from16 v22, v0

    aget v22, v22, v10

    move/from16 v0, v22

    if-lt v11, v0, :cond_4

    .line 564
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 568
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    move-object/from16 v22, v0

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    move/from16 v23, v0

    add-int v23, v23, v12

    aget v14, v22, v23

    .line 569
    .local v14, "position":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    move-object/from16 v22, v0

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    move/from16 v23, v0

    add-int v23, v23, v12

    aget v17, v22, v23

    .line 570
    .local v17, "startOffset":I
    aget-wide v22, v19, v8

    sub-int v24, v14, v16

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    aput-wide v22, v19, v8

    .line 571
    aget-wide v22, v18, v8

    sub-int v24, v17, v15

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    add-long v22, v22, v24

    aput-wide v22, v18, v8

    .line 572
    move/from16 v16, v14

    .line 573
    move/from16 v15, v17

    .line 574
    add-int/lit8 v12, v12, 0x1

    .line 567
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 587
    .end local v6    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v7    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .end local v8    # "fieldNumOff":I
    .end local v10    # "i":I
    .end local v11    # "j":I
    .end local v12    # "pos":I
    .end local v14    # "position":I
    .end local v15    # "previousOff":I
    .end local v16    # "previousPos":I
    .end local v17    # "startOffset":I
    :cond_5
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v4, v0, [F

    .line 588
    .local v4, "charsPerTerm":[F
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_3
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v10, v0, :cond_8

    .line 593
    const/4 v10, 0x0

    :goto_4
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    if-lt v10, v0, :cond_b

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 598
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_6
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-nez v21, :cond_c

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_7
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-nez v21, :cond_10

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    goto/16 :goto_0

    .line 589
    :cond_8
    aget-wide v20, v19, v10

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_9

    aget-wide v20, v18, v10

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-gtz v20, :cond_a

    :cond_9
    const/16 v20, 0x0

    :goto_5
    aput v20, v4, v10

    .line 588
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 589
    :cond_a
    aget-wide v20, v18, v10

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    aget-wide v22, v19, v10

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v20, v0

    goto :goto_5

    .line 594
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v20, v0

    aget v21, v4, v10

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 593
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_4

    .line 598
    :cond_c
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 599
    .restart local v6    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v0, v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_d
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_6

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 600
    .restart local v7    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x2

    if-eqz v22, :cond_d

    .line 601
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->fieldNum:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-static {v0, v1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v8

    .line 602
    .restart local v8    # "fieldNumOff":I
    aget v5, v4, v8

    .line 603
    .local v5, "cpt":F
    const/4 v12, 0x0

    .line 604
    .restart local v12    # "pos":I
    const/4 v10, 0x0

    :goto_6
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v10, v0, :cond_d

    .line 605
    const/16 v16, 0x0

    .line 606
    .restart local v16    # "previousPos":I
    const/4 v15, 0x0

    .line 607
    .restart local v15    # "previousOff":I
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_7
    iget-object v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    move-object/from16 v22, v0

    aget v22, v22, v10

    move/from16 v0, v22

    if-lt v11, v0, :cond_e

    .line 604
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 608
    :cond_e
    iget-boolean v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    move/from16 v22, v0

    if-eqz v22, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    move-object/from16 v22, v0

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    move/from16 v23, v0

    add-int v23, v23, v12

    aget v14, v22, v23

    .line 609
    .restart local v14    # "position":I
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    move-object/from16 v22, v0

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    move/from16 v23, v0

    add-int v23, v23, v12

    aget v17, v22, v23

    .line 610
    .restart local v17    # "startOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v22, v0

    sub-int v23, v17, v15

    sub-int v24, v14, v16

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v5

    move/from16 v0, v24

    float-to-int v0, v0

    move/from16 v24, v0

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 611
    move/from16 v16, v14

    .line 612
    move/from16 v15, v17

    .line 613
    add-int/lit8 v12, v12, 0x1

    .line 607
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 608
    .end local v14    # "position":I
    .end local v17    # "startOffset":I
    :cond_f
    const/4 v14, 0x0

    goto :goto_8

    .line 623
    .end local v5    # "cpt":F
    .end local v6    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v7    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .end local v8    # "fieldNumOff":I
    .end local v11    # "j":I
    .end local v12    # "pos":I
    .end local v15    # "previousOff":I
    .end local v16    # "previousPos":I
    :cond_10
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 624
    .restart local v6    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v0, v6, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_11
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_7

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 625
    .restart local v7    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x2

    if-eqz v22, :cond_11

    .line 626
    const/4 v12, 0x0

    .line 627
    .restart local v12    # "pos":I
    const/4 v10, 0x0

    :goto_9
    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-lt v10, v0, :cond_12

    .line 632
    sget-boolean v22, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v22, :cond_11

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-eq v12, v0, :cond_11

    new-instance v20, Ljava/lang/AssertionError;

    invoke-direct/range {v20 .. v20}, Ljava/lang/AssertionError;-><init>()V

    throw v20

    .line 628
    :cond_12
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_a
    iget-object v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    move-object/from16 v22, v0

    aget v22, v22, v10

    move/from16 v0, v22

    if-lt v11, v0, :cond_13

    .line 627
    add-int/lit8 v10, v10, 0x1

    goto :goto_9

    .line 629
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    move-object/from16 v23, v0

    iget v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    move/from16 v24, v0

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "pos":I
    .local v13, "pos":I
    add-int v24, v24, v12

    aget v23, v23, v24

    iget-object v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->prefixLengths:[I

    move-object/from16 v24, v0

    aget v24, v24, v10

    sub-int v23, v23, v24

    iget-object v0, v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->suffixLengths:[I

    move-object/from16 v24, v0

    aget v24, v24, v10

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 628
    add-int/lit8 v11, v11, 0x1

    move v12, v13

    .end local v13    # "pos":I
    .restart local v12    # "pos":I
    goto :goto_a
.end method

.method private flushPayloadLengths()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 640
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 641
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 650
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    .line 651
    return-void

    .line 641
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 642
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 643
    .local v1, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget-boolean v5, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    if-eqz v5, :cond_2

    .line 644
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    if-ge v2, v5, :cond_2

    .line 645
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    iget v7, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    add-int/2addr v7, v2

    aget v6, v6, v7

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 644
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private flushPositions()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 534
    iget-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 535
    iget-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v8}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 551
    iget-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v8}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    .line 552
    return-void

    .line 535
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 536
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v9, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v9}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 537
    .local v1, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget-boolean v10, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    if-eqz v10, :cond_2

    .line 538
    const/4 v4, 0x0

    .line 539
    .local v4, "pos":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v10, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    if-lt v2, v10, :cond_3

    .line 547
    sget-boolean v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v10, :cond_2

    iget v10, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    if-eq v4, v10, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 540
    :cond_3
    const/4 v7, 0x0

    .line 541
    .local v7, "previousPosition":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget-object v10, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    aget v10, v10, v2

    if-lt v3, v10, :cond_4

    .line 539
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 542
    :cond_4
    iget-object v10, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    iget v11, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "pos":I
    .local v5, "pos":I
    add-int/2addr v11, v4

    aget v6, v10, v11

    .line 543
    .local v6, "position":I
    iget-object v10, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    sub-int v11, v6, v7

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 544
    move v7, v6

    .line 541
    add-int/lit8 v3, v3, 0x1

    move v4, v5

    .end local v5    # "pos":I
    .restart local v4    # "pos":I
    goto :goto_1
.end method

.method private flushTermFreqs()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 522
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 523
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 530
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    .line 531
    return-void

    .line 523
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 524
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 525
    .local v1, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    if-ge v2, v5, :cond_2

    .line 526
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v6, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->freqs:[I

    aget v6, v6, v2

    add-int/lit8 v6, v6, -0x1

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 525
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private flushTermLengths()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 501
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 502
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 509
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    .line 510
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->reset(Lorg/apache/lucene/store/DataOutput;)V

    .line 511
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 518
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->finish()V

    .line 519
    return-void

    .line 502
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 503
    .local v0, "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 504
    .local v1, "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    if-ge v2, v5, :cond_3

    .line 505
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v6, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->prefixLengths:[I

    aget v6, v6, v2

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 504
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 511
    .end local v0    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    .end local v1    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .end local v2    # "i":I
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 512
    .restart local v0    # "dd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v4}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 513
    .restart local v1    # "fd":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget v5, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->numTerms:I

    if-ge v2, v5, :cond_5

    .line 514
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->writer:Lorg/apache/lucene/util/packed/BlockPackedWriter;

    iget-object v6, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->suffixLengths:[I

    aget v6, v6, v2

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/packed/BlockPackedWriter;->add(J)V

    .line 513
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private static nextDeletedDoc(ILorg/apache/lucene/util/Bits;I)I
    .locals 1
    .param p0, "doc"    # I
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "maxDoc"    # I

    .prologue
    .line 813
    if-nez p1, :cond_1

    .line 819
    .end local p2    # "maxDoc":I
    :goto_0
    return p2

    .line 817
    .restart local p2    # "maxDoc":I
    :cond_0
    add-int/lit8 p0, p0, 0x1

    .line 816
    :cond_1
    if-ge p0, p2, :cond_2

    invoke-interface {p1, p0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    move p2, p0

    .line 819
    goto :goto_0
.end method

.method private static nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I
    .locals 1
    .param p0, "doc"    # I
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "maxDoc"    # I

    .prologue
    .line 803
    if-nez p1, :cond_2

    .line 809
    :cond_0
    :goto_0
    return p0

    .line 807
    :cond_1
    add-int/lit8 p0, p0, 0x1

    .line 806
    :cond_2
    if-ge p0, p2, :cond_0

    invoke-interface {p1, p0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0
.end method

.method private triggerFlush()Z
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->chunkSize:I

    if-ge v0, v1, :cond_0

    .line 329
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    const/16 v1, 0x80

    .line 328
    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public abort()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 271
    new-array v0, v6, [Ljava/io/Closeable;

    .line 267
    aput-object p0, v0, v5

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 268
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    .line 269
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segment:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segmentSuffix:Ljava/lang/String;

    const-string v4, "tvd"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 270
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segment:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->segmentSuffix:Ljava/lang/String;

    const-string v4, "tvx"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 268
    invoke-static {v0, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    return-void
.end method

.method public addPosition(IIILorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "startOffset"    # I
    .param p3, "endOffset"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    sget-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->flags:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 321
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    sub-int v2, p3, p2

    if-nez p4, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p1, p2, v2, v0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->addPosition(IIII)V

    .line 322
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-boolean v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    .line 323
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v1, p4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p4, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p4, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeBytes([BII)V

    .line 325
    :cond_1
    return-void

    .line 321
    :cond_2
    iget v0, p4, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0
.end method

.method public addProx(ILorg/apache/lucene/store/DataInput;Lorg/apache/lucene/store/DataInput;)V
    .locals 16
    .param p1, "numProx"    # I
    .param p2, "positions"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "offsets"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 672
    sget-boolean v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-boolean v14, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    if-eqz p2, :cond_0

    const/4 v13, 0x1

    :goto_0
    if-eq v14, v13, :cond_1

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    :cond_0
    const/4 v13, 0x0

    goto :goto_0

    .line 673
    :cond_1
    sget-boolean v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-boolean v14, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    if-eqz p3, :cond_2

    const/4 v13, 0x1

    :goto_1
    if-eq v14, v13, :cond_3

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    :cond_2
    const/4 v13, 0x0

    goto :goto_1

    .line 675
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-boolean v13, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    if-eqz v13, :cond_6

    .line 676
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v13, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v14, v14, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int v10, v13, v14

    .line 677
    .local v10, "posStart":I
    add-int v13, v10, p1

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    array-length v14, v14

    if-le v13, v14, :cond_4

    .line 678
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    add-int v14, v10, p1

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    .line 680
    :cond_4
    const/4 v11, 0x0

    .line 681
    .local v11, "position":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-boolean v13, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    if-eqz v13, :cond_b

    .line 682
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v13, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v14, v14, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int v8, v13, v14

    .line 683
    .local v8, "payStart":I
    add-int v13, v8, p1

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    array-length v14, v14

    if-le v13, v14, :cond_5

    .line 684
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    add-int v14, v8, p1

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    .line 686
    :cond_5
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    move/from16 v0, p1

    if-lt v4, v0, :cond_9

    .line 707
    .end local v4    # "i":I
    .end local v8    # "payStart":I
    .end local v10    # "posStart":I
    .end local v11    # "position":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-boolean v13, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    if-eqz v13, :cond_8

    .line 708
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v13, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v14, v14, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int v7, v13, v14

    .line 709
    .local v7, "offStart":I
    add-int v13, v7, p1

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    array-length v14, v14

    if-le v13, v14, :cond_7

    .line 710
    add-int v13, v7, p1

    const/4 v14, 0x4

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v6

    .line 711
    .local v6, "newLength":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    invoke-static {v13, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    .line 712
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    invoke-static {v13, v6}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    .line 714
    .end local v6    # "newLength":I
    :cond_7
    const/4 v5, 0x0

    .line 715
    .local v5, "lastOffset":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    move/from16 v0, p1

    if-lt v4, v0, :cond_c

    .line 724
    .end local v4    # "i":I
    .end local v5    # "lastOffset":I
    .end local v7    # "offStart":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v14, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    add-int v14, v14, p1

    iput v14, v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    .line 725
    return-void

    .line 687
    .restart local v4    # "i":I
    .restart local v8    # "payStart":I
    .restart local v10    # "posStart":I
    .restart local v11    # "position":I
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 688
    .local v2, "code":I
    and-int/lit8 v13, v2, 0x1

    if-eqz v13, :cond_a

    .line 690
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v9

    .line 691
    .local v9, "payloadLength":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    add-int v14, v8, v4

    aput v9, v13, v14

    .line 692
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    int-to-long v14, v9

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v15}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 696
    .end local v9    # "payloadLength":I
    :goto_4
    ushr-int/lit8 v13, v2, 0x1

    add-int/2addr v11, v13

    .line 697
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    add-int v14, v10, v4

    aput v11, v13, v14

    .line 686
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 694
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadLengthsBuf:[I

    add-int v14, v8, v4

    const/4 v15, 0x0

    aput v15, v13, v14

    goto :goto_4

    .line 700
    .end local v2    # "code":I
    .end local v4    # "i":I
    .end local v8    # "payStart":I
    :cond_b
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_5
    move/from16 v0, p1

    if-ge v4, v0, :cond_6

    .line 701
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v13

    ushr-int/lit8 v13, v13, 0x1

    add-int/2addr v11, v13

    .line 702
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->positionsBuf:[I

    add-int v14, v10, v4

    aput v11, v13, v14

    .line 700
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 716
    .end local v10    # "posStart":I
    .end local v11    # "position":I
    .restart local v5    # "lastOffset":I
    .restart local v7    # "offStart":I
    :cond_c
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v13

    add-int v12, v5, v13

    .line 717
    .local v12, "startOffset":I
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v13

    add-int v3, v12, v13

    .line 718
    .local v3, "endOffset":I
    move v5, v3

    .line 719
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->startOffsetsBuf:[I

    add-int v14, v7, v4

    aput v12, v13, v14

    .line 720
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lengthsBuf:[I

    add-int v14, v7, v4

    sub-int v15, v3, v12

    aput v15, v13, v14

    .line 715
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 257
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 258
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 261
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    .line 263
    return-void

    .line 259
    :catchall_0
    move-exception v0

    .line 260
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 261
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    .line 262
    throw v0
.end method

.method public finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 3
    .param p1, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 655
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 656
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flush()V

    .line 658
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    if-eq p2, v0, :cond_1

    .line 659
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrote "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " docs, finish called with numDocs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 661
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->finish(I)V

    .line 662
    return-void
.end method

.method public finishDocument()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v1, v1, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v2, v2, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeBytes([BI)V

    .line 282
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->payloadBytes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    .line 283
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    .line 284
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->triggerFlush()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->flush()V

    .line 287
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curDoc:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 288
    return-void
.end method

.method public finishField()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 300
    return-void
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;)I
    .locals 30
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 729
    const/4 v10, 0x0

    .line 730
    .local v10, "docCount":I
    const/4 v12, 0x0

    .line 732
    .local v12, "idx":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_0

    .line 798
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v10}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V

    .line 799
    return v10

    .line 732
    :cond_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/AtomicReader;

    .line 733
    .local v19, "reader":Lorg/apache/lucene/index/AtomicReader;
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v26, v0

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "idx":I
    .local v13, "idx":I
    aget-object v16, v26, v12

    .line 734
    .local v16, "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/16 v17, 0x0

    .line 735
    .local v17, "matchingVectorsReader":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
    if-eqz v16, :cond_1

    .line 736
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/codecs/TermVectorsReader;

    move-result-object v23

    .line 738
    .local v23, "vectorsReader":Lorg/apache/lucene/codecs/TermVectorsReader;
    if-eqz v23, :cond_1

    move-object/from16 v0, v23

    instance-of v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    move/from16 v26, v0

    if-eqz v26, :cond_1

    move-object/from16 v17, v23

    .line 739
    check-cast v17, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    .line 743
    .end local v23    # "vectorsReader":Lorg/apache/lucene/codecs/TermVectorsReader;
    :cond_1
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v18

    .line 744
    .local v18, "maxDoc":I
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v15

    .line 746
    .local v15, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-eqz v17, :cond_2

    .line 747
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->getCompressionMode()Lorg/apache/lucene/codecs/compressing/CompressionMode;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_2

    .line 748
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->getChunkSize()I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->chunkSize:I

    move/from16 v27, v0

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2

    .line 749
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->getPackedIntsVersion()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_4

    .line 751
    :cond_2
    const/16 v26, 0x0

    move/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    .local v11, "i":I
    :goto_1
    move/from16 v0, v18

    if-lt v11, v0, :cond_3

    move v12, v13

    .line 757
    .end local v13    # "idx":I
    .restart local v12    # "idx":I
    goto :goto_0

    .line 752
    .end local v12    # "idx":I
    .restart local v13    # "idx":I
    :cond_3
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v22

    .line 753
    .local v22, "vectors":Lorg/apache/lucene/index/Fields;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V

    .line 754
    add-int/lit8 v10, v10, 0x1

    .line 755
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v26, v0

    const-wide v28, 0x4072c00000000000L    # 300.0

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 751
    add-int/lit8 v26, v11, 0x1

    move/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    goto :goto_1

    .line 758
    .end local v11    # "i":I
    .end local v22    # "vectors":Lorg/apache/lucene/index/Fields;
    :cond_4
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->getIndex()Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-result-object v14

    .line 759
    .local v14, "index":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->getVectorsStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v24

    .line 760
    .local v24, "vectorsStream":Lorg/apache/lucene/store/IndexInput;
    const/16 v26, 0x0

    move/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    .restart local v11    # "i":I
    :goto_2
    move/from16 v0, v18

    if-lt v11, v0, :cond_5

    move v12, v13

    .end local v13    # "idx":I
    .restart local v12    # "idx":I
    goto/16 :goto_0

    .line 761
    .end local v12    # "idx":I
    .restart local v13    # "idx":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->pendingDocs:Ljava/util/Deque;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Ljava/util/Deque;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_a

    .line 762
    if-eqz v11, :cond_6

    add-int/lit8 v26, v11, -0x1

    move/from16 v0, v26

    invoke-virtual {v14, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v26

    invoke-virtual {v14, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v28

    cmp-long v26, v26, v28

    if-gez v26, :cond_a

    .line 763
    :cond_6
    invoke-virtual {v14, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v20

    .line 764
    .local v20, "startPointer":J
    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 765
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v5

    .line 766
    .local v5, "docBase":I
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 767
    .local v4, "chunkDocs":I
    sget-boolean v26, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v26, :cond_7

    add-int v26, v5, v4

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_7

    new-instance v25, Ljava/lang/AssertionError;

    invoke-direct/range {v25 .. v25}, Ljava/lang/AssertionError;-><init>()V

    throw v25

    .line 768
    :cond_7
    add-int v26, v5, v4

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/index/SegmentReader;->maxDoc()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_9

    .line 769
    add-int v26, v5, v4

    move/from16 v0, v26

    invoke-static {v5, v15, v0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextDeletedDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v26

    add-int v27, v5, v4

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_9

    .line 770
    add-int v26, v5, v4

    move/from16 v0, v26

    invoke-virtual {v14, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v6

    .line 771
    .local v6, "chunkEnd":J
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v26

    sub-long v8, v6, v26

    .line 772
    .local v8, "chunkLength":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v28

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->writeIndex(IJ)V

    .line 773
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 774
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 775
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->vectorsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 776
    add-int/2addr v10, v4

    .line 777
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    move/from16 v26, v0

    add-int v26, v26, v4

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->numDocs:I

    .line 778
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v26, v0

    mul-int/lit16 v0, v4, 0x12c

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-double v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 779
    add-int v26, v5, v4

    move/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    .line 780
    goto/16 :goto_2

    .line 782
    .end local v6    # "chunkEnd":J
    .end local v8    # "chunkLength":J
    :cond_8
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v22

    .line 783
    .restart local v22    # "vectors":Lorg/apache/lucene/index/Fields;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V

    .line 784
    add-int/lit8 v10, v10, 0x1

    .line 785
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v26, v0

    const-wide v28, 0x4072c00000000000L    # 300.0

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 781
    add-int/lit8 v26, v11, 0x1

    move/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    .end local v22    # "vectors":Lorg/apache/lucene/index/Fields;
    :cond_9
    add-int v26, v5, v4

    move/from16 v0, v26

    if-lt v11, v0, :cond_8

    goto/16 :goto_2

    .line 789
    .end local v4    # "chunkDocs":I
    .end local v5    # "docBase":I
    .end local v20    # "startPointer":J
    :cond_a
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v22

    .line 790
    .restart local v22    # "vectors":Lorg/apache/lucene/index/Fields;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V

    .line 791
    add-int/lit8 v10, v10, 0x1

    .line 792
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v26, v0

    const-wide v28, 0x4072c00000000000L    # 300.0

    move-object/from16 v0, v26

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 793
    add-int/lit8 v26, v11, 0x1

    move/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    goto/16 :goto_2
.end method

.method public startDocument(I)V
    .locals 1
    .param p1, "numVectorFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->addDocData(I)Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curDoc:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    .line 276
    return-void
.end method

.method public startField(Lorg/apache/lucene/index/FieldInfo;IZZZ)V
    .locals 6
    .param p1, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "numTerms"    # I
    .param p3, "positions"    # Z
    .param p4, "offsets"    # Z
    .param p5, "payloads"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curDoc:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;

    iget v1, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->addField(IIZZZ)Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 294
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 295
    return-void
.end method

.method public startTerm(Lorg/apache/lucene/util/BytesRef;I)V
    .locals 7
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "freq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 304
    sget-boolean v1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ge p2, v6, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 305
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, p1}, Lorg/apache/lucene/util/StringHelper;->bytesDifference(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 306
    .local v0, "prefix":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->curField:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v2, v0

    invoke-virtual {v1, p2, v0, v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->addTerm(III)V

    .line 307
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->termSuffixes:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v3, v0

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeBytes([BII)V

    .line 309
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v1, v2, :cond_1

    .line 310
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v6}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 312
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    iput v5, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 313
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 314
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 315
    return-void
.end method
