.class public Lorg/apache/lucene/codecs/BlockTermState;
.super Lorg/apache/lucene/index/OrdTermState;
.source "BlockTermState.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public blockFilePointer:J

.field public docFreq:I

.field public termBlockOrd:I

.field public totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTermState;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/lucene/index/OrdTermState;-><init>()V

    .line 42
    return-void
.end method


# virtual methods
.method public copyFrom(Lorg/apache/lucene/index/TermState;)V
    .locals 4
    .param p1, "_other"    # Lorg/apache/lucene/index/TermState;

    .prologue
    .line 46
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTermState;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/codecs/BlockTermState;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "can not copy from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_0
    move-object v0, p1

    .line 47
    check-cast v0, Lorg/apache/lucene/codecs/BlockTermState;

    .line 48
    .local v0, "other":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-super {p0, p1}, Lorg/apache/lucene/index/OrdTermState;->copyFrom(Lorg/apache/lucene/index/TermState;)V

    .line 49
    iget v1, v0, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    .line 50
    iget-wide v2, v0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    .line 51
    iget v1, v0, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 52
    iget-wide v2, v0, Lorg/apache/lucene/codecs/BlockTermState;->blockFilePointer:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTermState;->blockFilePointer:J

    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "docFreq="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " totalTermFreq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " termBlockOrd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " blockFP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/BlockTermState;->blockFilePointer:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
