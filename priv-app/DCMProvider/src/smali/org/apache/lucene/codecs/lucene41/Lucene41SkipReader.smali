.class final Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;
.super Lorg/apache/lucene/codecs/MultiLevelSkipListReader;
.source "Lucene41SkipReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final blockSize:I

.field private docPointer:[J

.field private lastDocPointer:J

.field private lastPayPointer:J

.field private lastPayloadByteUpto:I

.field private lastPosBufferUpto:I

.field private lastPosPointer:J

.field private payPointer:[J

.field private payloadByteUpto:[I

.field private posBufferUpto:[I

.field private posPointer:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;IIZZZ)V
    .locals 2
    .param p1, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "maxSkipLevels"    # I
    .param p3, "blockSize"    # I
    .param p4, "hasPos"    # Z
    .param p5, "hasOffsets"    # Z
    .param p6, "hasPayloads"    # Z

    .prologue
    const/4 v1, 0x0

    .line 70
    const/16 v0, 0x8

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;III)V

    .line 71
    iput p3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->blockSize:I

    .line 72
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->docPointer:[J

    .line 73
    if-eqz p4, :cond_3

    .line 74
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    .line 75
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posBufferUpto:[I

    .line 76
    if-eqz p6, :cond_1

    .line 77
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    .line 81
    :goto_0
    if-nez p5, :cond_0

    if-eqz p6, :cond_2

    .line 82
    :cond_0
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    .line 89
    :goto_1
    return-void

    .line 79
    :cond_1
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    goto :goto_0

    .line 84
    :cond_2
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    goto :goto_1

    .line 87
    :cond_3
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    goto :goto_1
.end method


# virtual methods
.method public getDocPointer()J
    .locals 2

    .prologue
    .line 124
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastDocPointer:J

    return-wide v0
.end method

.method public getNextSkipDoc()I
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->skipDoc:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getPayPointer()J
    .locals 2

    .prologue
    .line 136
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayPointer:J

    return-wide v0
.end method

.method public getPayloadByteUpto()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayloadByteUpto:I

    return v0
.end method

.method public getPosBufferUpto()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosBufferUpto:I

    return v0
.end method

.method public getPosPointer()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosPointer:J

    return-wide v0
.end method

.method public init(JJJJI)V
    .locals 3
    .param p1, "skipPointer"    # J
    .param p3, "docBasePointer"    # J
    .param p5, "posBasePointer"    # J
    .param p7, "payBasePointer"    # J
    .param p9, "df"    # I

    .prologue
    .line 105
    invoke-virtual {p0, p9}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->trim(I)I

    move-result v0

    invoke-super {p0, p1, p2, v0}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->init(JI)V

    .line 106
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastDocPointer:J

    .line 107
    iput-wide p5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosPointer:J

    .line 108
    iput-wide p7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayPointer:J

    .line 110
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->docPointer:[J

    invoke-static {v0, p3, p4}, Ljava/util/Arrays;->fill([JJ)V

    .line 111
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    invoke-static {v0, p5, p6}, Ljava/util/Arrays;->fill([JJ)V

    .line 113
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    invoke-static {v0, p7, p8}, Ljava/util/Arrays;->fill([JJ)V

    .line 119
    :cond_0
    return-void

    .line 117
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected readSkipData(ILorg/apache/lucene/store/IndexInput;)I
    .locals 6
    .param p1, "level"    # I
    .param p2, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 198
    .local v0, "delta":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->docPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 203
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    if-eqz v1, :cond_1

    .line 204
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 208
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posBufferUpto:[I

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, v1, p1

    .line 213
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, v1, p1

    .line 217
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    if-eqz v1, :cond_1

    .line 218
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 221
    :cond_1
    return v0
.end method

.method protected seekChild(I)V
    .locals 4
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->seekChild(I)V

    .line 153
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->docPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastDocPointer:J

    aput-wide v2, v0, p1

    .line 154
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosPointer:J

    aput-wide v2, v0, p1

    .line 156
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posBufferUpto:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosBufferUpto:I

    aput v1, v0, p1

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayloadByteUpto:I

    aput v1, v0, p1

    .line 160
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayPointer:J

    aput-wide v2, v0, p1

    .line 164
    :cond_1
    return-void
.end method

.method protected setLastSkipData(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 168
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->setLastSkipData(I)V

    .line 169
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->docPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastDocPointer:J

    .line 174
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosPointer:J

    .line 176
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->posBufferUpto:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPosBufferUpto:I

    .line 180
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayPointer:J

    .line 183
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->payloadByteUpto:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->lastPayloadByteUpto:I

    .line 187
    :cond_1
    return-void
.end method

.method protected trim(I)I
    .locals 1
    .param p1, "df"    # I

    .prologue
    .line 101
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->blockSize:I

    rem-int v0, p1, v0

    if-nez v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    .end local p1    # "df":I
    :cond_0
    return p1
.end method
