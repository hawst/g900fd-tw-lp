.class Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer$6;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field currentLiveDocs:Lorg/apache/lucene/util/Bits;

.field currentReader:Lorg/apache/lucene/index/AtomicReader;

.field docIDUpto:I

.field nextIsSet:Z

.field nextValue:I

.field readerUpto:I

.field final synthetic this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$6;

.field private final synthetic val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

.field private final synthetic val$readers:[Lorg/apache/lucene/index/AtomicReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/DocValuesConsumer$6;[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/SortedSetDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$6;

    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

    .line 459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    return-void
.end method

.method private setNext()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 490
    :goto_0
    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 513
    :goto_1
    return v1

    .line 494
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    if-eqz v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 495
    :cond_1
    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    .line 496
    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    array-length v4, v4

    if-ge v3, v4, :cond_2

    .line 497
    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    iget v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    aget-object v3, v3, v4

    iput-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    .line 498
    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    .line 500
    :cond_2
    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    goto :goto_0

    .line 504
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    iget v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    invoke-interface {v3, v4}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 505
    :cond_4
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextIsSet:Z

    .line 506
    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

    iget v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->readerUpto:I

    aget-object v0, v3, v4

    .line 507
    .local v0, "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 508
    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextValue:I

    .line 509
    :goto_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_5

    .line 512
    iget v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    move v1, v2

    .line 513
    goto :goto_1

    .line 510
    :cond_5
    iget v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextValue:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextValue:I

    goto :goto_2

    .line 516
    .end local v0    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    :cond_6
    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->docIDUpto:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextIsSet:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->setNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 479
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 480
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 482
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextIsSet:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 483
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextIsSet:Z

    .line 485
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->nextValue:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$6$1;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 474
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
