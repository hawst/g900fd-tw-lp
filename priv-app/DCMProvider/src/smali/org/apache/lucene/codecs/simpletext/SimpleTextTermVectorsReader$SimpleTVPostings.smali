.class Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;
.super Ljava/lang/Object;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleTVPostings"
.end annotation


# instance fields
.field private endOffsets:[I

.field private freq:I

.field private payloads:[Lorg/apache/lucene/util/BytesRef;

.field private positions:[I

.field private startOffsets:[I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->positions:[I

    return-object v0
.end method

.method static synthetic access$10(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[I)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->endOffsets:[I

    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->startOffsets:[I

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->endOffsets:[I

    return-object v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->payloads:[Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method static synthetic access$6(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;I)V
    .locals 0

    .prologue
    .line 316
    iput p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I

    return-void
.end method

.method static synthetic access$7(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[I)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->positions:[I

    return-void
.end method

.method static synthetic access$8(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[Lorg/apache/lucene/util/BytesRef;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->payloads:[Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method static synthetic access$9(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;[I)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->startOffsets:[I

    return-void
.end method
