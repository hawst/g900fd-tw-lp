.class final Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;
.super Ljava/lang/Object;
.source "LZ4.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/LZ4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "HashTable"
.end annotation


# instance fields
.field private hashLog:I

.field private hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;)I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashLog:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;)Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    return-object v0
.end method


# virtual methods
.method reset(I)V
    .locals 5
    .param p1, "len"    # I

    .prologue
    const/4 v4, 0x1

    .line 189
    add-int/lit8 v2, p1, -0x5

    int-to-long v2, v2

    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    .line 190
    .local v0, "bitsPerOffset":I
    add-int/lit8 v2, v0, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v2

    rsub-int/lit8 v1, v2, 0x20

    .line 191
    .local v1, "bitsPerOffsetLog":I
    rsub-int/lit8 v2, v1, 0x11

    iput v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashLog:I

    .line 192
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->size()I

    move-result v2

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashLog:I

    shl-int v3, v4, v3

    if-lt v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->getBitsPerValue()I

    move-result v2

    if-ge v2, v0, :cond_1

    .line 193
    :cond_0
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashLog:I

    shl-int v2, v4, v2

    const v3, 0x3e4ccccd    # 0.2f

    invoke-static {v2, v0, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    .line 197
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HashTable;->hashTable:Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    invoke-interface {v2}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->clear()V

    goto :goto_0
.end method
