.class public abstract Lorg/apache/lucene/codecs/compressing/Decompressor;
.super Ljava/lang/Object;
.source "Decompressor.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/Decompressor;->clone()Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v0

    return-object v0
.end method

.method public abstract clone()Lorg/apache/lucene/codecs/compressing/Decompressor;
.end method

.method public abstract decompress(Lorg/apache/lucene/store/DataInput;IIILorg/apache/lucene/util/BytesRef;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
