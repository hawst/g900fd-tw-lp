.class public final Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsBaseFormat;
.super Lorg/apache/lucene/codecs/PostingsBaseFormat;
.source "Lucene40PostingsBaseFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "Lucene40"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/PostingsBaseFormat;-><init>(Ljava/lang/String;)V

    .line 41
    return-void
.end method


# virtual methods
.method public postingsReaderBase(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/PostingsReaderBase;
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;)V

    return-object v0
.end method

.method public postingsWriterBase(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/PostingsWriterBase;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
