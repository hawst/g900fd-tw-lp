.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;
.super Lorg/apache/lucene/index/NumericDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadVarIntsField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$defaultValue:J

.field private final synthetic val$minValue:J

.field private final synthetic val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;JJ)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->val$defaultValue:J

    iput-wide p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->val$minValue:J

    .line 139
    invoke-direct {p0}, Lorg/apache/lucene/index/NumericDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 4
    .param p1, "docID"    # I

    .prologue
    .line 142
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v2, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    .line 143
    .local v0, "value":J
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->val$defaultValue:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 144
    const-wide/16 v2, 0x0

    .line 146
    :goto_0
    return-wide v2

    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;->val$minValue:J

    add-long/2addr v2, v0

    goto :goto_0
.end method
