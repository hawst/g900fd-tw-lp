.class public Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;
.super Lorg/apache/lucene/codecs/Codec;
.source "Lucene40Codec.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

.field private final fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

.field private final infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

.field private final liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

.field private final normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

.field private final postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "Lucene40"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 47
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 48
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    .line 49
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    .line 50
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    .line 51
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    .line 53
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec$1;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 90
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 97
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40NormsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40NormsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    .line 118
    const-string v0, "Lucene40"

    invoke-static {v0}, Lorg/apache/lucene/codecs/PostingsFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 63
    return-void
.end method


# virtual methods
.method public docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    return-object v0
.end method

.method public getPostingsFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    return-object v0
.end method

.method public normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    return-object v0
.end method

.method public final postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    return-object v0
.end method

.method public final storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    return-object v0
.end method

.method public final termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40Codec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    return-object v0
.end method
