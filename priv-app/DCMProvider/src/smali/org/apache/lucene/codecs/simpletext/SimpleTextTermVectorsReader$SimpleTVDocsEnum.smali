.class Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleTVDocsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private didNext:Z

.field private doc:I

.field private freq:I

.field private liveDocs:Lorg/apache/lucene/util/Bits;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 405
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 405
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 407
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->doc:I

    .line 405
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;)V
    .locals 0

    .prologue
    .line 405
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;-><init>()V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 446
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->doc:I

    return v0
.end method

.method public freq()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->freq:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 414
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->freq:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 424
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->didNext:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 425
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->didNext:Z

    .line 426
    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->doc:I

    .line 428
    :goto_0
    return v0

    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->doc:I

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;I)V
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "freq"    # I

    .prologue
    .line 438
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 439
    iput p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->freq:I

    .line 440
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->doc:I

    .line 441
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->didNext:Z

    .line 442
    return-void
.end method
