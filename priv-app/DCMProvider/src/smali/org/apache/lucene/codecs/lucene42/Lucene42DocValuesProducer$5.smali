.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "Lucene42DocValuesProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->loadBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

.field private final synthetic val$addresses:Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;->this$0:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;->val$addresses:Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 219
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 8
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 222
    if-nez p1, :cond_0

    const-wide/16 v2, 0x0

    .line 223
    .local v2, "startAddress":J
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;->val$addresses:Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;

    int-to-long v6, p1

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->get(J)J

    move-result-wide v0

    .line 224
    .local v0, "endAddress":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    sub-long v6, v0, v2

    long-to-int v5, v6

    invoke-virtual {v4, p2, v2, v3, v5}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 225
    return-void

    .line 222
    .end local v0    # "endAddress":J
    .end local v2    # "startAddress":J
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;->val$addresses:Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;

    add-int/lit8 v5, p1, -0x1

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;->get(J)J

    move-result-wide v2

    goto :goto_0
.end method
