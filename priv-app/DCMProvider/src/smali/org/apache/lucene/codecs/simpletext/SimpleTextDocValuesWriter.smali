.class Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;
.super Lorg/apache/lucene/codecs/DocValuesConsumer;
.source "SimpleTextDocValuesWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final END:Lorg/apache/lucene/util/BytesRef;

.field static final FIELD:Lorg/apache/lucene/util/BytesRef;

.field static final LENGTH:Lorg/apache/lucene/util/BytesRef;

.field static final MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

.field static final MINVALUE:Lorg/apache/lucene/util/BytesRef;

.field static final NUMVALUES:Lorg/apache/lucene/util/BytesRef;

.field static final ORDPATTERN:Lorg/apache/lucene/util/BytesRef;

.field static final PATTERN:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE:Lorg/apache/lucene/util/BytesRef;


# instance fields
.field final data:Lorg/apache/lucene/store/IndexOutput;

.field private final fieldsSeen:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final numDocs:I

.field final scratch:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    .line 39
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "END"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->END:Lorg/apache/lucene/util/BytesRef;

    .line 40
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "field "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    .line 41
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  type "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    .line 43
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  minvalue "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MINVALUE:Lorg/apache/lucene/util/BytesRef;

    .line 44
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  pattern "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "length "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    .line 47
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  maxlength "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  numvalues "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->NUMVALUES:Lorg/apache/lucene/util/BytesRef;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  ordpattern "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->ORDPATTERN:Lorg/apache/lucene/util/BytesRef;

    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "ext"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer;-><init>()V

    .line 53
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldsSeen:Ljava/util/Set;

    .line 59
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v1, v2, p2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    .line 60
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->numDocs:I

    .line 61
    return-void
.end method

.method private fieldSeen(Ljava/lang/String;)Z
    .locals 3
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 65
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldsSeen:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "field \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" was added more than once during flush"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 66
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldsSeen:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method private writeFieldEntry(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "type"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 366
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 367
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 369
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 370
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 371
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 372
    return-void
.end method


# virtual methods
.method public addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 12
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_0

    iget-object v7, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldSeen(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 132
    :cond_0
    sget-boolean v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v7

    sget-object v8, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v7, v8, :cond_1

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 133
    :cond_1
    const/4 v3, 0x0

    .line 134
    .local v3, "maxLength":I
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    .line 137
    sget-object v7, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-direct {p0, p1, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->writeFieldEntry(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 140
    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v7, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 141
    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v7, v8, v9}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 142
    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 144
    int-to-long v8, v3

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    .line 145
    .local v2, "maxBytesLength":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_3

    .line 150
    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v7, v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 151
    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v7, v8, v9}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 152
    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 153
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/text/DecimalFormatSymbols;

    sget-object v9, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v8, v9}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 155
    .local v0, "encoder":Ljava/text/DecimalFormat;
    const/4 v4, 0x0

    .line 156
    .local v4, "numDocsWritten":I
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_4

    .line 174
    sget-boolean v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    iget v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->numDocs:I

    if-eq v7, v4, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 134
    .end local v0    # "encoder":Ljava/text/DecimalFormat;
    .end local v1    # "i":I
    .end local v2    # "maxBytesLength":I
    .end local v4    # "numDocsWritten":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/BytesRef;

    .line 135
    .local v6, "value":Lorg/apache/lucene/util/BytesRef;
    iget v8, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_0

    .line 147
    .end local v6    # "value":Lorg/apache/lucene/util/BytesRef;
    .restart local v1    # "i":I
    .restart local v2    # "maxBytesLength":I
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    const/16 v7, 0x30

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 156
    .restart local v0    # "encoder":Ljava/text/DecimalFormat;
    .restart local v4    # "numDocsWritten":I
    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/util/BytesRef;

    .line 158
    .restart local v6    # "value":Lorg/apache/lucene/util/BytesRef;
    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v8, v9}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 159
    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    iget v9, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v10, v9

    invoke-virtual {v0, v10, v11}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v8, v9, v10}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 160
    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 164
    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    iget-object v9, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v10, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v11, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v8, v9, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 167
    iget v1, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    :goto_3
    if-lt v1, v3, :cond_5

    .line 170
    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v8}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 171
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 168
    :cond_5
    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    const/16 v9, 0x20

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 175
    .end local v6    # "value":Lorg/apache/lucene/util/BytesRef;
    :cond_6
    return-void
.end method

.method public addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 27
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    sget-boolean v24, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v24, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldSeen(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_0

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24

    .line 73
    :cond_0
    sget-boolean v24, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v24, :cond_1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v24

    sget-object v25, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 74
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v24

    sget-object v25, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24

    .line 75
    :cond_1
    sget-object v24, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->writeFieldEntry(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 78
    const-wide v14, 0x7fffffffffffffffL

    .line 79
    .local v14, "minValue":J
    const-wide/high16 v10, -0x8000000000000000L

    .line 80
    .local v10, "maxValue":J
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_3

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    sget-object v25, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MINVALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v24 .. v25}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 93
    invoke-static {v10, v11}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v8

    .line 94
    .local v8, "maxBig":Ljava/math/BigInteger;
    invoke-static {v14, v15}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v12

    .line 95
    .local v12, "minBig":Ljava/math/BigInteger;
    invoke-virtual {v8, v12}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 96
    .local v5, "diffBig":Ljava/math/BigInteger;
    invoke-virtual {v5}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    move-result v9

    .line 97
    .local v9, "maxBytesPerValue":I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    .local v19, "sb":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-lt v7, v9, :cond_4

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    sget-object v25, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v24 .. v25}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v26, v0

    invoke-static/range {v24 .. v26}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 107
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 109
    .local v17, "patternString":Ljava/lang/String;
    new-instance v6, Ljava/text/DecimalFormat;

    new-instance v24, Ljava/text/DecimalFormatSymbols;

    sget-object v25, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct/range {v24 .. v25}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-direct {v6, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 111
    .local v6, "encoder":Ljava/text/DecimalFormat;
    const/16 v16, 0x0

    .line 114
    .local v16, "numDocsWritten":I
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :cond_2
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-nez v25, :cond_5

    .line 126
    sget-boolean v24, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v24, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->numDocs:I

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, v16

    if-eq v0, v1, :cond_8

    new-instance v24, Ljava/lang/AssertionError;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "numDocs="

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->numDocs:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " numDocsWritten="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v24

    .line 80
    .end local v5    # "diffBig":Ljava/math/BigInteger;
    .end local v6    # "encoder":Ljava/text/DecimalFormat;
    .end local v7    # "i":I
    .end local v8    # "maxBig":Ljava/math/BigInteger;
    .end local v9    # "maxBytesPerValue":I
    .end local v12    # "minBig":Ljava/math/BigInteger;
    .end local v16    # "numDocsWritten":I
    .end local v17    # "patternString":Ljava/lang/String;
    .end local v19    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Number;

    .line 81
    .local v13, "n":Ljava/lang/Number;
    invoke-virtual {v13}, Ljava/lang/Number;->longValue()J

    move-result-wide v20

    .line 82
    .local v20, "v":J
    move-wide/from16 v0, v20

    invoke-static {v14, v15, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    .line 83
    move-wide/from16 v0, v20

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    goto/16 :goto_0

    .line 99
    .end local v13    # "n":Ljava/lang/Number;
    .end local v20    # "v":J
    .restart local v5    # "diffBig":Ljava/math/BigInteger;
    .restart local v7    # "i":I
    .restart local v8    # "maxBig":Ljava/math/BigInteger;
    .restart local v9    # "maxBytesPerValue":I
    .restart local v12    # "minBig":Ljava/math/BigInteger;
    .restart local v19    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    const/16 v24, 0x30

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 98
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 114
    .restart local v6    # "encoder":Ljava/text/DecimalFormat;
    .restart local v16    # "numDocsWritten":I
    .restart local v17    # "patternString":Ljava/lang/String;
    :cond_5
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Number;

    .line 115
    .restart local v13    # "n":Ljava/lang/Number;
    invoke-virtual {v13}, Ljava/lang/Number;->longValue()J

    move-result-wide v22

    .line 116
    .local v22, "value":J
    sget-boolean v25, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v25, :cond_6

    cmp-long v25, v22, v14

    if-gez v25, :cond_6

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24

    .line 117
    :cond_6
    invoke-static/range {v22 .. v23}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v25

    invoke-static {v14, v15}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 118
    .local v4, "delta":Ljava/lang/Number;
    invoke-virtual {v6, v4}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 119
    .local v18, "s":Ljava/lang/String;
    sget-boolean v25, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v25, :cond_7

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v25

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_7

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24

    .line 120
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 122
    add-int/lit8 v16, v16, 0x1

    .line 123
    sget-boolean v25, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v25, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->numDocs:I

    move/from16 v25, v0

    move/from16 v0, v16

    move/from16 v1, v25

    if-le v0, v1, :cond_2

    new-instance v24, Ljava/lang/AssertionError;

    invoke-direct/range {v24 .. v24}, Ljava/lang/AssertionError;-><init>()V

    throw v24

    .line 127
    .end local v4    # "delta":Ljava/lang/Number;
    .end local v13    # "n":Ljava/lang/Number;
    .end local v18    # "s":Ljava/lang/String;
    .end local v22    # "value":J
    :cond_8
    return-void
.end method

.method public addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 18
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrd":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    sget-boolean v13, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_0

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldSeen(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_0

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 180
    :cond_0
    sget-boolean v13, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v13

    sget-object v14, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v13, v14, :cond_1

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 181
    :cond_1
    sget-object v13, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->writeFieldEntry(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 183
    const/4 v11, 0x0

    .line 184
    .local v11, "valueCount":I
    const/4 v5, -0x1

    .line 185
    .local v5, "maxLength":I
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_3

    .line 191
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v14, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->NUMVALUES:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v13}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v14, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 197
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v13}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 200
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v4

    .line 201
    .local v4, "maxBytesLength":I
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 202
    .local v9, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v4, :cond_4

    .line 207
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v14, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v13}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 210
    new-instance v2, Ljava/text/DecimalFormat;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/text/DecimalFormatSymbols;

    sget-object v15, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v14, v15}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v2, v13, v14}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 212
    .local v2, "encoder":Ljava/text/DecimalFormat;
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v6

    .line 213
    .local v6, "maxOrdBytes":I
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 214
    const/4 v3, 0x0

    :goto_2
    if-lt v3, v6, :cond_5

    .line 219
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v14, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->ORDPATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 220
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v13, v14, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 221
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v13}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 222
    new-instance v8, Ljava/text/DecimalFormat;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Ljava/text/DecimalFormatSymbols;

    sget-object v15, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v14, v15}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v8, v13, v14}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 225
    .local v8, "ordEncoder":Ljava/text/DecimalFormat;
    const/4 v12, 0x0

    .line 227
    .local v12, "valuesSeen":I
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_6

    .line 246
    sget-boolean v13, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v13, :cond_8

    if-eq v12, v11, :cond_8

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 185
    .end local v2    # "encoder":Ljava/text/DecimalFormat;
    .end local v3    # "i":I
    .end local v4    # "maxBytesLength":I
    .end local v6    # "maxOrdBytes":I
    .end local v8    # "ordEncoder":Ljava/text/DecimalFormat;
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local v12    # "valuesSeen":I
    :cond_3
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/util/BytesRef;

    .line 186
    .local v10, "value":Lorg/apache/lucene/util/BytesRef;
    iget v14, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v5, v14}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 187
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 203
    .end local v10    # "value":Lorg/apache/lucene/util/BytesRef;
    .restart local v3    # "i":I
    .restart local v4    # "maxBytesLength":I
    .restart local v9    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    const/16 v13, 0x30

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 215
    .restart local v2    # "encoder":Ljava/text/DecimalFormat;
    .restart local v6    # "maxOrdBytes":I
    :cond_5
    const/16 v13, 0x30

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 214
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 227
    .restart local v8    # "ordEncoder":Ljava/text/DecimalFormat;
    .restart local v12    # "valuesSeen":I
    :cond_6
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/util/BytesRef;

    .line 229
    .restart local v10    # "value":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v14, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    iget v15, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v0, v15

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 231
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 235
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    iget-object v15, v10, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v0, v10, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v16, v0

    iget v0, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v17, v0

    invoke-virtual/range {v14 .. v17}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 238
    iget v3, v10, Lorg/apache/lucene/util/BytesRef;->length:I

    :goto_3
    if-lt v3, v5, :cond_7

    .line 241
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 242
    add-int/lit8 v12, v12, 0x1

    .line 243
    sget-boolean v14, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v14, :cond_2

    if-le v12, v11, :cond_2

    new-instance v13, Ljava/lang/AssertionError;

    invoke-direct {v13}, Ljava/lang/AssertionError;-><init>()V

    throw v13

    .line 239
    :cond_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    const/16 v15, 0x20

    invoke-virtual {v14, v15}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 238
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 248
    .end local v10    # "value":Lorg/apache/lucene/util/BytesRef;
    :cond_8
    invoke-interface/range {p3 .. p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-nez v14, :cond_9

    .line 252
    return-void

    .line 248
    :cond_9
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    .line 249
    .local v7, "ord":Ljava/lang/Number;
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v15

    int-to-long v0, v15

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v16, v0

    invoke-static/range {v14 .. v16}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v14}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    goto :goto_4
.end method

.method public addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 27
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 256
    .local p2, "values":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/util/BytesRef;>;"
    .local p3, "docToOrdCount":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    .local p4, "ords":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Ljava/lang/Number;>;"
    sget-boolean v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v22, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldSeen(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_0

    new-instance v22, Ljava/lang/AssertionError;

    invoke-direct/range {v22 .. v22}, Ljava/lang/AssertionError;-><init>()V

    throw v22

    .line 257
    :cond_0
    sget-boolean v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v22, :cond_1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v22

    sget-object v23, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    new-instance v22, Ljava/lang/AssertionError;

    invoke-direct/range {v22 .. v22}, Ljava/lang/AssertionError;-><init>()V

    throw v22

    .line 258
    :cond_1
    sget-object v22, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->writeFieldEntry(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 260
    const-wide/16 v18, 0x0

    .line 261
    .local v18, "valueCount":J
    const/4 v8, 0x0

    .line 262
    .local v8, "maxLength":I
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-nez v23, :cond_3

    .line 268
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    sget-object v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->NUMVALUES:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    sget-object v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 277
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v7

    .line 278
    .local v7, "maxBytesLength":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 279
    .local v15, "sb":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-lt v6, v7, :cond_4

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    sget-object v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 287
    new-instance v5, Ljava/text/DecimalFormat;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    new-instance v23, Ljava/text/DecimalFormatSymbols;

    sget-object v24, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct/range {v23 .. v24}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v5, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 290
    .local v5, "encoder":Ljava/text/DecimalFormat;
    const/4 v9, 0x0

    .line 291
    .local v9, "maxOrdListLength":I
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 292
    .local v16, "sb2":Ljava/lang/StringBuilder;
    invoke-interface/range {p4 .. p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 293
    .local v14, "ordStream":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Number;>;"
    invoke-interface/range {p3 .. p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_2
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_5

    .line 306
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 307
    const/4 v6, 0x0

    :goto_3
    if-lt v6, v9, :cond_8

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    sget-object v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->ORDPATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v24, v0

    invoke-static/range {v22 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 317
    const-wide/16 v20, 0x0

    .line 319
    .local v20, "valuesSeen":J
    invoke-interface/range {p2 .. p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-nez v23, :cond_9

    .line 338
    sget-boolean v22, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v22, :cond_b

    cmp-long v22, v20, v18

    if-eqz v22, :cond_b

    new-instance v22, Ljava/lang/AssertionError;

    invoke-direct/range {v22 .. v22}, Ljava/lang/AssertionError;-><init>()V

    throw v22

    .line 262
    .end local v5    # "encoder":Ljava/text/DecimalFormat;
    .end local v6    # "i":I
    .end local v7    # "maxBytesLength":I
    .end local v9    # "maxOrdListLength":I
    .end local v14    # "ordStream":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Number;>;"
    .end local v15    # "sb":Ljava/lang/StringBuilder;
    .end local v16    # "sb2":Ljava/lang/StringBuilder;
    .end local v20    # "valuesSeen":J
    :cond_3
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/util/BytesRef;

    .line 263
    .local v17, "value":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 264
    const-wide/16 v24, 0x1

    add-long v18, v18, v24

    goto/16 :goto_0

    .line 280
    .end local v17    # "value":Lorg/apache/lucene/util/BytesRef;
    .restart local v6    # "i":I
    .restart local v7    # "maxBytesLength":I
    .restart local v15    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    const/16 v22, 0x30

    move/from16 v0, v22

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 279
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 293
    .restart local v5    # "encoder":Ljava/text/DecimalFormat;
    .restart local v9    # "maxOrdListLength":I
    .restart local v14    # "ordStream":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Number;>;"
    .restart local v16    # "sb2":Ljava/lang/StringBuilder;
    :cond_5
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    .line 294
    .local v10, "n":Ljava/lang/Number;
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 295
    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 296
    .local v4, "count":I
    const/4 v6, 0x0

    :goto_4
    if-lt v6, v4, :cond_6

    .line 303
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    move/from16 v0, v22

    invoke-static {v9, v0}, Ljava/lang/Math;->max(II)I

    move-result v9

    goto/16 :goto_2

    .line 297
    :cond_6
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Number;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Number;->longValue()J

    move-result-wide v12

    .line 298
    .local v12, "ord":J
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_7

    .line 299
    const-string v22, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    :cond_7
    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 308
    .end local v4    # "count":I
    .end local v10    # "n":Ljava/lang/Number;
    .end local v12    # "ord":J
    :cond_8
    const/16 v22, 0x58

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 307
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    .line 319
    .restart local v20    # "valuesSeen":J
    :cond_9
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/util/BytesRef;

    .line 321
    .restart local v17    # "value":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v23, v0

    sget-object v24, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static/range {v23 .. v24}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 322
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v25, v0

    invoke-static/range {v23 .. v25}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v23, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v24, v0

    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v25, v0

    move-object/from16 v0, v17

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v26, v0

    invoke-virtual/range {v23 .. v26}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 330
    move-object/from16 v0, v17

    iget v6, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    :goto_5
    if-lt v6, v8, :cond_a

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 334
    const-wide/16 v24, 0x1

    add-long v20, v20, v24

    .line 335
    sget-boolean v23, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v23, :cond_2

    cmp-long v23, v20, v18

    if-lez v23, :cond_2

    new-instance v22, Ljava/lang/AssertionError;

    invoke-direct/range {v22 .. v22}, Ljava/lang/AssertionError;-><init>()V

    throw v22

    .line 331
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v23, v0

    const/16 v24, 0x20

    invoke-virtual/range {v23 .. v24}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 330
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 340
    .end local v17    # "value":Lorg/apache/lucene/util/BytesRef;
    :cond_b
    invoke-interface/range {p4 .. p4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 343
    invoke-interface/range {p3 .. p3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :goto_6
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_c

    .line 361
    return-void

    .line 343
    :cond_c
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Number;

    .line 344
    .restart local v10    # "n":Ljava/lang/Number;
    const/16 v22, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 345
    invoke-virtual {v10}, Ljava/lang/Number;->intValue()I

    move-result v4

    .line 346
    .restart local v4    # "count":I
    const/4 v6, 0x0

    :goto_7
    if-lt v6, v4, :cond_d

    .line 354
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    sub-int v11, v9, v22

    .line 355
    .local v11, "numPadding":I
    const/4 v6, 0x0

    :goto_8
    if-lt v6, v11, :cond_f

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    goto :goto_6

    .line 347
    .end local v11    # "numPadding":I
    :cond_d
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Number;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Number;->longValue()J

    move-result-wide v12

    .line 348
    .restart local v12    # "ord":J
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v22

    if-lez v22, :cond_e

    .line 349
    const-string v22, ","

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    :cond_e
    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    .line 356
    .end local v12    # "ord":J
    .restart local v11    # "numPadding":I
    :cond_f
    const/16 v22, 0x20

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 355
    add-int/lit8 v6, v6, 0x1

    goto :goto_8
.end method

.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 376
    const/4 v0, 0x0

    .line 378
    .local v0, "success":Z
    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->fieldsSeen:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    :catchall_0
    move-exception v1

    .line 384
    if-eqz v0, :cond_1

    new-array v2, v3, [Ljava/io/Closeable;

    .line 385
    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 389
    :goto_0
    throw v1

    .line 380
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 381
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 382
    const/4 v0, 0x1

    .line 384
    if-eqz v0, :cond_2

    new-array v1, v3, [Ljava/io/Closeable;

    .line 385
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 390
    :goto_1
    return-void

    .line 386
    :cond_1
    new-array v2, v3, [Ljava/io/Closeable;

    .line 387
    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0

    .line 386
    :cond_2
    new-array v1, v3, [Ljava/io/Closeable;

    .line 387
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->data:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method
