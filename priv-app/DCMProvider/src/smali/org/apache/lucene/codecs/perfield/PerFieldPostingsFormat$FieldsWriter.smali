.class Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;
.super Lorg/apache/lucene/codecs/FieldsConsumer;
.source "PerFieldPostingsFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FieldsWriter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final formats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/codecs/PostingsFormat;",
            "Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;",
            ">;"
        }
    .end annotation
.end field

.field private final segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

.field private final suffixes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 1
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;

    .prologue
    .line 96
    iput-object p1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldsConsumer;-><init>()V

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->formats:Ljava/util/Map;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    .line 97
    iput-object p2, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    .line 98
    return-void
.end method


# virtual methods
.method public addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;
    .locals 9
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;

    iget-object v7, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->getPostingsFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v1

    .line 103
    .local v1, "format":Lorg/apache/lucene/codecs/PostingsFormat;
    if-nez v1, :cond_0

    .line 104
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "invalid null PostingsFormat for field=\""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 106
    :cond_0
    invoke-virtual {v1}, Lorg/apache/lucene/codecs/PostingsFormat;->getName()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "formatName":Ljava/lang/String;
    sget-object v6, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->PER_FIELD_FORMAT_KEY:Ljava/lang/String;

    invoke-virtual {p1, v6, v2}, Lorg/apache/lucene/index/FieldInfo;->putAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "previousValue":Ljava/lang/String;
    sget-boolean v6, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    if-eqz v3, :cond_1

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 113
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->formats:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;

    .line 114
    .local v0, "consumer":Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;
    if-nez v0, :cond_3

    .line 118
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 119
    .local v5, "suffix":Ljava/lang/Integer;
    if-nez v5, :cond_2

    .line 120
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 124
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    invoke-interface {v6, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v6, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 127
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    .line 128
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->getSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 126
    invoke-static {v6, v7, v8}, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->getFullSegmentSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "segmentSuffix":Ljava/lang/String;
    new-instance v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;

    .end local v0    # "consumer":Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;
    invoke-direct {v0}, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;-><init>()V

    .line 130
    .restart local v0    # "consumer":Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;
    new-instance v6, Lorg/apache/lucene/index/SegmentWriteState;

    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->segmentWriteState:Lorg/apache/lucene/index/SegmentWriteState;

    invoke-direct {v6, v7, v4}, Lorg/apache/lucene/index/SegmentWriteState;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lorg/apache/lucene/codecs/PostingsFormat;->fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;

    move-result-object v6

    iput-object v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;->consumer:Lorg/apache/lucene/codecs/FieldsConsumer;

    .line 131
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;->suffix:I

    .line 132
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->formats:Ljava/util/Map;

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    .end local v4    # "segmentSuffix":Ljava/lang/String;
    :goto_1
    sget-object v6, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->PER_FIELD_SUFFIX_KEY:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lorg/apache/lucene/index/FieldInfo;->putAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 140
    sget-boolean v6, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    if-eqz v3, :cond_5

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 122
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_0

    .line 135
    .end local v5    # "suffix":Ljava/lang/Integer;
    :cond_3
    sget-boolean v6, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_4

    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->suffixes:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 136
    :cond_4
    iget v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;->suffix:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .restart local v5    # "suffix":Ljava/lang/Integer;
    goto :goto_1

    .line 147
    :cond_5
    iget-object v6, v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;->consumer:Lorg/apache/lucene/codecs/FieldsConsumer;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/codecs/FieldsConsumer;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;

    move-result-object v6

    return-object v6
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsWriter;->formats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V

    .line 154
    return-void
.end method
