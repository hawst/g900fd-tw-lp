.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosFormat;
.super Lorg/apache/lucene/codecs/FieldInfosFormat;
.source "Lucene3xFieldInfosFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/FieldInfosReader;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosFormat;-><init>()V

    .line 34
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    .line 33
    return-void
.end method


# virtual methods
.method public getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    return-object v0
.end method

.method public getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
