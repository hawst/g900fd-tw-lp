.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsConsumer;
.super Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;
.source "SimpleTextNormsFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SimpleTextNormsConsumer"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    const-string v0, "len"

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;)V

    .line 75
    return-void
.end method


# virtual methods
.method public bridge synthetic addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    return-void
.end method

.method public bridge synthetic addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    return-void
.end method

.method public bridge synthetic addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    return-void
.end method

.method public bridge synthetic addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    return-void
.end method

.method public bridge synthetic close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->close()V

    return-void
.end method
