.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;
.super Lorg/apache/lucene/codecs/DocValuesProducer;
.source "Lucene42DocValuesProducer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;,
        Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;,
        Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;,
        Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;
    }
.end annotation


# instance fields
.field private final binaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final binaryInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/BinaryDocValues;",
            ">;"
        }
    .end annotation
.end field

.field private final data:Lorg/apache/lucene/store/IndexInput;

.field private final fstInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final fsts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final maxDoc:I

.field private final numericInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/NumericDocValues;",
            ">;"
        }
    .end annotation
.end field

.field private final numerics:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .param p2, "dataCodec"    # Ljava/lang/String;
    .param p3, "dataExtension"    # Ljava/lang/String;
    .param p4, "metaCodec"    # Ljava/lang/String;
    .param p5, "metaExtension"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 69
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->numericInstances:Ljava/util/Map;

    .line 71
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->binaryInstances:Ljava/util/Map;

    .line 73
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fstInstances:Ljava/util/Map;

    .line 78
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->maxDoc:I

    .line 79
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v4, v5, p5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "metaName":Ljava/lang/String;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v2, v5}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 82
    .local v1, "in":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 85
    .local v3, "success":Z
    const/4 v4, 0x0

    .line 86
    const/4 v5, 0x0

    .line 84
    :try_start_0
    invoke-static {v1, p4, v4, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 87
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->numerics:Ljava/util/Map;

    .line 88
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->binaries:Ljava/util/Map;

    .line 89
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fsts:Ljava/util/Map;

    .line 90
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {p0, v1, v4}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->readFields(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    const/4 v3, 0x1

    .line 93
    if-eqz v3, :cond_1

    new-array v4, v7, [Ljava/io/Closeable;

    .line 94
    aput-object v1, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 100
    :goto_0
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v4, v5, p3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "dataName":Ljava/lang/String;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v0, v5}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    .line 102
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-static {v4, p2, v6, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 105
    return-void

    .line 92
    .end local v0    # "dataName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 93
    if-eqz v3, :cond_0

    new-array v5, v7, [Ljava/io/Closeable;

    .line 94
    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 98
    :goto_1
    throw v4

    .line 95
    :cond_0
    new-array v5, v7, [Ljava/io/Closeable;

    .line 96
    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 95
    :cond_1
    new-array v4, v7, [Ljava/io/Closeable;

    .line 96
    aput-object v1, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private loadBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 11
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->binaries:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;

    .line 205
    .local v9, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->offset:J

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 206
    new-instance v7, Lorg/apache/lucene/util/PagedBytes;

    const/16 v1, 0x10

    invoke-direct {v7, v1}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 207
    .local v7, "bytes":Lorg/apache/lucene/util/PagedBytes;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->numBytes:J

    invoke-virtual {v7, v1, v2, v3}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 208
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v8

    .line 209
    .local v8, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    iget v1, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->minLength:I

    iget v2, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->maxLength:I

    if-ne v1, v2, :cond_0

    .line 210
    iget v10, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->minLength:I

    .line 211
    .local v10, "fixedLength":I
    new-instance v1, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$4;

    invoke-direct {v1, p0, v8, v10}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$4;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/util/PagedBytes$Reader;I)V

    .line 219
    .end local v10    # "fixedLength":I
    :goto_0
    return-object v1

    .line 218
    :cond_0
    new-instance v0, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget v2, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->packedIntsVersion:I

    iget v3, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->blockSize:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->maxDoc:I

    int-to-long v4, v4

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;-><init>(Lorg/apache/lucene/store/IndexInput;IIJZ)V

    .line 219
    .local v0, "addresses":Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;
    new-instance v1, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;

    invoke-direct {v1, p0, v0, v8}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$5;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/util/packed/MonotonicBlockPackedReader;Lorg/apache/lucene/util/PagedBytes$Reader;)V

    goto :goto_0
.end method

.method private loadNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 14
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 153
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->numerics:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;

    .line 154
    .local v10, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->offset:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 155
    iget-byte v1, v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->format:B

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 156
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v13

    .line 157
    .local v13, "size":I
    new-array v9, v13, [J

    .line 158
    .local v9, "decode":[J
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    array-length v1, v9

    if-lt v12, v1, :cond_0

    .line 161
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v11

    .line 162
    .local v11, "formatID":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    .line 163
    .local v7, "bitsPerValue":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-static {v11}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byId(I)Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-result-object v2

    iget v4, v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->packedIntsVersion:I

    iget v5, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->maxDoc:I

    invoke-static {v1, v2, v4, v5, v7}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v0

    .line 164
    .local v0, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    new-instance v1, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$1;

    invoke-direct {v1, p0, v9, v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$1;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;[JLorg/apache/lucene/util/packed/PackedInts$Reader;)V

    .line 182
    .end local v0    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v7    # "bitsPerValue":I
    .end local v9    # "decode":[J
    .end local v11    # "formatID":I
    .end local v12    # "i":I
    .end local v13    # "size":I
    :goto_1
    return-object v1

    .line 159
    .restart local v9    # "decode":[J
    .restart local v12    # "i":I
    .restart local v13    # "size":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    aput-wide v4, v9, v12

    .line 158
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 170
    .end local v9    # "decode":[J
    .end local v12    # "i":I
    .end local v13    # "size":I
    :cond_1
    iget-byte v1, v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->format:B

    if-nez v1, :cond_2

    .line 171
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    .line 172
    .local v3, "blockSize":I
    new-instance v0, Lorg/apache/lucene/util/packed/BlockPackedReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget v2, v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->packedIntsVersion:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->maxDoc:I

    int-to-long v4, v4

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/util/packed/BlockPackedReader;-><init>(Lorg/apache/lucene/store/IndexInput;IIJZ)V

    .line 173
    .local v0, "reader":Lorg/apache/lucene/util/packed/BlockPackedReader;
    new-instance v1, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$2;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$2;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/util/packed/BlockPackedReader;)V

    goto :goto_1

    .line 179
    .end local v0    # "reader":Lorg/apache/lucene/util/packed/BlockPackedReader;
    .end local v3    # "blockSize":I
    :cond_2
    iget-byte v1, v10, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->format:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 180
    iget v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->maxDoc:I

    new-array v8, v1, [B

    .line 181
    .local v8, "bytes":[B
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    array-length v2, v8

    invoke-virtual {v1, v8, v6, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 182
    new-instance v1, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$3;

    invoke-direct {v1, p0, v8}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$3;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;[B)V

    goto :goto_1

    .line 189
    .end local v8    # "bytes":[B
    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method private readFields(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 7
    .param p1, "meta"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "infos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 108
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 109
    .local v1, "fieldNumber":I
    :goto_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 140
    return-void

    .line 110
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v2

    .line 111
    .local v2, "fieldType":I
    if-nez v2, :cond_2

    .line 112
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;-><init>()V

    .line 113
    .local v0, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    iput-wide v4, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->offset:J

    .line 114
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    iput-byte v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->format:B

    .line 115
    iget-byte v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->format:B

    if-eq v3, v6, :cond_1

    .line 116
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;->packedIntsVersion:I

    .line 118
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->numerics:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    .end local v0    # "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$NumericEntry;
    :goto_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    goto :goto_0

    .line 119
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 120
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;-><init>()V

    .line 121
    .local v0, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    iput-wide v4, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->offset:J

    .line 122
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    iput-wide v4, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->numBytes:J

    .line 123
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->minLength:I

    .line 124
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->maxLength:I

    .line 125
    iget v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->minLength:I

    iget v4, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->maxLength:I

    if-eq v3, v4, :cond_3

    .line 126
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->packedIntsVersion:I

    .line 127
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;->blockSize:I

    .line 129
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->binaries:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 130
    .end local v0    # "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$BinaryEntry;
    :cond_4
    if-ne v2, v6, :cond_5

    .line 131
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;-><init>()V

    .line 132
    .local v0, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    iput-wide v4, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->offset:J

    .line 133
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v4

    iput-wide v4, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->numOrds:J

    .line 134
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fsts:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 136
    .end local v0    # "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;
    :cond_5
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "invalid entry type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", input="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 392
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 393
    return-void
.end method

.method public declared-synchronized getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->binaryInstances:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/BinaryDocValues;

    .line 196
    .local v0, "instance":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v0, :cond_0

    .line 197
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->loadBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->binaryInstances:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_0
    monitor-exit p0

    return-object v0

    .line 195
    .end local v0    # "instance":Lorg/apache/lucene/index/BinaryDocValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->numericInstances:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/NumericDocValues;

    .line 145
    .local v0, "instance":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v0, :cond_0

    .line 146
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->loadNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->numericInstances:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :cond_0
    monitor-exit p0

    return-object v0

    .line 144
    .end local v0    # "instance":Lorg/apache/lucene/index/NumericDocValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 14
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fsts:Ljava/util/Map;

    iget v1, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    .line 234
    .local v9, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;
    monitor-enter p0

    .line 235
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fstInstances:Ljava/util/Map;

    iget v1, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/util/fst/FST;

    .line 236
    .local v10, "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    if-nez v10, :cond_0

    .line 237
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget-wide v12, v9, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->offset:J

    invoke-virtual {v0, v12, v13}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 238
    new-instance v10, Lorg/apache/lucene/util/fst/FST;

    .end local v10    # "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    const/4 v1, 0x1

    invoke-static {v1}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->getSingleton(Z)Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    move-result-object v1

    invoke-direct {v10, v0, v1}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 239
    .restart local v10    # "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fstInstances:Ljava/util/Map;

    iget v1, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 243
    .local v2, "docToOrd":Lorg/apache/lucene/index/NumericDocValues;
    move-object v4, v10

    .line 246
    .local v4, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v3

    .line 247
    .local v3, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 248
    .local v5, "firstArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v6, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v6}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 249
    .local v6, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v7, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v7}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 250
    .local v7, "scratchInts":Lorg/apache/lucene/util/IntsRef;
    new-instance v8, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-direct {v8, v4}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    .line 252
    .local v8, "fstEnum":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<Ljava/lang/Long;>;"
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/index/NumericDocValues;Lorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/fst/BytesRefFSTEnum;Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;)V

    return-object v0

    .line 234
    .end local v2    # "docToOrd":Lorg/apache/lucene/index/NumericDocValues;
    .end local v3    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v4    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    .end local v5    # "firstArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .end local v6    # "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .end local v7    # "scratchInts":Lorg/apache/lucene/util/IntsRef;
    .end local v8    # "fstEnum":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<Ljava/lang/Long;>;"
    .end local v10    # "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 18
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fsts:Ljava/util/Map;

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    .line 304
    .local v13, "entry":Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;
    iget-wide v2, v13, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->numOrds:J

    const-wide/16 v16, 0x0

    cmp-long v2, v2, v16

    if-nez v2, :cond_0

    .line 305
    sget-object v2, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 327
    :goto_0
    return-object v2

    .line 308
    :cond_0
    monitor-enter p0

    .line 309
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fstInstances:Ljava/util/Map;

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/util/fst/FST;

    .line 310
    .local v14, "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    if-nez v14, :cond_1

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    iget-wide v0, v13, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->offset:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 312
    new-instance v14, Lorg/apache/lucene/util/fst/FST;

    .end local v14    # "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->data:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x1

    invoke-static {v3}, Lorg/apache/lucene/util/fst/PositiveIntOutputs;->getSingleton(Z)Lorg/apache/lucene/util/fst/PositiveIntOutputs;

    move-result-object v3

    invoke-direct {v14, v2, v3}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V

    .line 313
    .restart local v14    # "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->fstInstances:Ljava/util/Map;

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    invoke-virtual/range {p0 .. p1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v5

    .line 317
    .local v5, "docToOrds":Lorg/apache/lucene/index/BinaryDocValues;
    move-object v8, v14

    .line 320
    .local v8, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    invoke-virtual {v8}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v7

    .line 321
    .local v7, "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    new-instance v9, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v9}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 322
    .local v9, "firstArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v10, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v10}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    .line 323
    .local v10, "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    new-instance v11, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v11}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    .line 324
    .local v11, "scratchInts":Lorg/apache/lucene/util/IntsRef;
    new-instance v12, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-direct {v12, v8}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    .line 325
    .local v12, "fstEnum":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<Ljava/lang/Long;>;"
    new-instance v6, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v6}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 326
    .local v6, "ref":Lorg/apache/lucene/util/BytesRef;
    new-instance v4, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v4}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    .line 327
    .local v4, "input":Lorg/apache/lucene/store/ByteArrayDataInput;
    new-instance v2, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v13}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/store/ByteArrayDataInput;Lorg/apache/lucene/index/BinaryDocValues;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/fst/BytesRefFSTEnum;Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;)V

    goto :goto_0

    .line 308
    .end local v4    # "input":Lorg/apache/lucene/store/ByteArrayDataInput;
    .end local v5    # "docToOrds":Lorg/apache/lucene/index/BinaryDocValues;
    .end local v6    # "ref":Lorg/apache/lucene/util/BytesRef;
    .end local v7    # "in":Lorg/apache/lucene/util/fst/FST$BytesReader;, "Lorg/apache/lucene/util/fst/FST$BytesReader;"
    .end local v8    # "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    .end local v9    # "firstArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .end local v10    # "scratchArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Ljava/lang/Long;>;"
    .end local v11    # "scratchInts":Lorg/apache/lucene/util/IntsRef;
    .end local v12    # "fstEnum":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<Ljava/lang/Long;>;"
    .end local v14    # "instance":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
