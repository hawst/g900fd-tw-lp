.class public final Lorg/apache/lucene/codecs/lucene42/Lucene42NormsFormat;
.super Lorg/apache/lucene/codecs/NormsFormat;
.source "Lucene42NormsFormat.java"


# static fields
.field private static final DATA_CODEC:Ljava/lang/String; = "Lucene41NormsData"

.field private static final DATA_EXTENSION:Ljava/lang/String; = "nvd"

.field private static final METADATA_CODEC:Ljava/lang/String; = "Lucene41NormsMetadata"

.field private static final METADATA_EXTENSION:Ljava/lang/String; = "nvm"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/codecs/NormsFormat;-><init>()V

    return-void
.end method


# virtual methods
.method public normsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 7
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;

    const-string v2, "Lucene41NormsData"

    const-string v3, "nvd"

    const-string v4, "Lucene41NormsMetadata"

    const-string v5, "nvm"

    const/high16 v6, 0x40e00000    # 7.0f

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V

    return-object v0
.end method

.method public normsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

    const-string v2, "Lucene41NormsData"

    const-string v3, "nvd"

    const-string v4, "Lucene41NormsMetadata"

    const-string v5, "nvm"

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
