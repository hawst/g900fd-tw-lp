.class Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer$5;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field currentOrd:J

.field final scratch:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$5;

.field private final synthetic val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

.field private final synthetic val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/DocValuesConsumer$5;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;[Lorg/apache/lucene/index/SortedSetDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$5;

    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iput-object p3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 428
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->scratch:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 4

    .prologue
    .line 433
    iget-wide v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->currentOrd:J

    iget-object v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getValueCount()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 8

    .prologue
    .line 438
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 439
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 441
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->currentOrd:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentNumber(J)I

    move-result v0

    .line 442
    .local v0, "segmentNumber":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->currentOrd:J

    invoke-virtual {v1, v0, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentOrd(IJ)J

    move-result-wide v2

    .line 443
    .local v2, "segmentOrd":J
    iget-object v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

    aget-object v1, v1, v0

    iget-object v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 444
    iget-wide v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->currentOrd:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->currentOrd:J

    .line 445
    iget-object v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$5$1;->scratch:Lorg/apache/lucene/util/BytesRef;

    return-object v1
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 450
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
