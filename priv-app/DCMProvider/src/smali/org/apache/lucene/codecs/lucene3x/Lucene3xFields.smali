.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;
.super Lorg/apache/lucene/codecs/FieldsProducer;
.source "Lucene3xFields.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;,
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DEBUG_SURROGATES:Z


# instance fields
.field private cfsReader:Lorg/apache/lucene/store/Directory;

.field private final context:Lorg/apache/lucene/store/IOContext;

.field private final dir:Lorg/apache/lucene/store/Directory;

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final fields:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final freqStream:Lorg/apache/lucene/store/IndexInput;

.field final preTerms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Terms;",
            ">;"
        }
    .end annotation
.end field

.field public final proxStream:Lorg/apache/lucene/store/IndexInput;

.field private final si:Lorg/apache/lucene/index/SegmentInfo;

.field public tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

.field public final tisNoIndex:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->$assertionsDisabled:Z

    .line 54
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;I)V
    .locals 9
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "indexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldsProducer;-><init>()V

    .line 63
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fields:Ljava/util/TreeMap;

    .line 64
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->preTerms:Ljava/util/Map;

    .line 72
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->si:Lorg/apache/lucene/index/SegmentInfo;

    .line 78
    if-gez p5, :cond_0

    .line 79
    neg-int p5, p5

    .line 82
    :cond_0
    const/4 v8, 0x0

    .line 84
    .local v8, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;I)V

    .line 85
    .local v0, "r":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    const/4 v1, -0x1

    if-ne p5, v1, :cond_3

    .line 86
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tisNoIndex:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    .line 91
    :goto_0
    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->context:Lorg/apache/lucene/store/IOContext;

    .line 92
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 96
    iget-object v1, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v2, ""

    const-string v3, "frq"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    .line 97
    const/4 v6, 0x0

    .line 98
    .local v6, "anyProx":Z
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 108
    if-eqz v6, :cond_6

    .line 109
    iget-object v1, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v2, ""

    const-string v3, "prx"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->proxStream:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    :goto_2
    const/4 v8, 0x1

    .line 120
    if-nez v8, :cond_2

    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->close()V

    .line 124
    :cond_2
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->dir:Lorg/apache/lucene/store/Directory;

    .line 125
    return-void

    .line 88
    .end local v6    # "anyProx":Z
    :cond_3
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tisNoIndex:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    .line 89
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 114
    .end local v0    # "r":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    :catchall_0
    move-exception v1

    .line 120
    if-nez v8, :cond_4

    .line 121
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->close()V

    .line 123
    :cond_4
    throw v1

    .line 98
    .restart local v0    # "r":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    .restart local v6    # "anyProx":Z
    :cond_5
    :try_start_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/index/FieldInfo;

    .line 99
    .local v7, "fi":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fields:Ljava/util/TreeMap;

    iget-object v3, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v7}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->preTerms:Ljava/util/Map;

    iget-object v3, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    new-instance v4, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;

    invoke-direct {v4, p0, v7}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;Lorg/apache/lucene/index/FieldInfo;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v2, v3, :cond_1

    .line 103
    const/4 v6, 0x1

    goto :goto_1

    .line 111
    .end local v7    # "fi":Lorg/apache/lucene/index/FieldInfo;
    :cond_6
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->proxStream:Lorg/apache/lucene/store/IndexInput;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method private declared-synchronized getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    .locals 1

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tisNoIndex:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 167
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->tisNoIndex:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->cfsReader:Lorg/apache/lucene/store/Directory;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->proxStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method public getUniqueTermCount()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->size()J

    move-result-wide v0

    return-wide v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 148
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->preTerms:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fields:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 149
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    return v0
.end method

.method protected sortTermsByUnicode()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->preTerms:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Terms;

    return-object v0
.end method
