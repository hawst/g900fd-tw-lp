.class final Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;
.super Ljava/lang/Object;
.source "LZ4.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/LZ4;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "HCHashTable"
.end annotation


# static fields
.field static final MASK:I = 0xffff

.field static final MAX_ATTEMPTS:I = 0x100


# instance fields
.field private base:I

.field private final chainTable:[S

.field private final hashTable:[I

.field nextToUpdate:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    const v0, 0x8000

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashTable:[I

    .line 285
    const/high16 v0, 0x10000

    new-array v0, v0, [S

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->chainTable:[S

    .line 286
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;I)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->reset(I)V

    return-void
.end method

.method private addHash([BI)V
    .locals 6
    .param p1, "bytes"    # [B
    .param p2, "off"    # I

    .prologue
    .line 306
    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I
    invoke-static {p1, p2}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$0([BI)I

    move-result v2

    .line 307
    .local v2, "v":I
    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->hashHC(I)I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$1(I)I

    move-result v1

    .line 308
    .local v1, "h":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashTable:[I

    aget v3, v3, v1

    sub-int v0, p2, v3

    .line 309
    .local v0, "delta":I
    const/high16 v3, 0x10000

    if-lt v0, v3, :cond_0

    .line 310
    const v0, 0xffff

    .line 312
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->chainTable:[S

    const v4, 0xffff

    and-int/2addr v4, p2

    int-to-short v5, v0

    aput-short v5, v3, v4

    .line 313
    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashTable:[I

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    sub-int v4, p2, v4

    aput v4, v3, v1

    .line 314
    return-void
.end method

.method private hashPointer([BI)I
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "off"    # I

    .prologue
    .line 296
    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->readInt([BI)I
    invoke-static {p1, p2}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$0([BI)I

    move-result v1

    .line 297
    .local v1, "v":I
    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->hashHC(I)I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$1(I)I

    move-result v0

    .line 298
    .local v0, "h":I
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashTable:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    return v2
.end method

.method private next(I)I
    .locals 4
    .param p1, "off"    # I

    .prologue
    const v3, 0xffff

    .line 302
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->chainTable:[S

    and-int v2, p1, v3

    aget-short v1, v1, v2

    and-int/2addr v1, v3

    sub-int/2addr v0, v1

    return v0
.end method

.method private reset(I)V
    .locals 2
    .param p1, "base"    # I

    .prologue
    .line 289
    iput p1, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    .line 290
    iput p1, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->nextToUpdate:I

    .line 291
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashTable:[I

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 292
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->chainTable:[S

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([SS)V

    .line 293
    return-void
.end method


# virtual methods
.method insert(I[B)V
    .locals 1
    .param p1, "off"    # I
    .param p2, "bytes"    # [B

    .prologue
    .line 317
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->nextToUpdate:I

    if-lt v0, p1, :cond_0

    .line 320
    return-void

    .line 318
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->nextToUpdate:I

    invoke-direct {p0, p2, v0}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->addHash([BI)V

    .line 317
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->nextToUpdate:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->nextToUpdate:I

    goto :goto_0
.end method

.method insertAndFindBestMatch([BIILorg/apache/lucene/codecs/compressing/LZ4$Match;)Z
    .locals 6
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "matchLimit"    # I
    .param p4, "match"    # Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    .prologue
    const/4 v3, 0x0

    .line 323
    iput p2, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    .line 324
    iput v3, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 326
    invoke-virtual {p0, p2, p1}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->insert(I[B)V

    .line 328
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashPointer([BI)I

    move-result v2

    .line 329
    .local v2, "ref":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x100

    if-lt v0, v4, :cond_2

    .line 343
    :cond_0
    iget v4, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    return v3

    .line 330
    :cond_2
    iget v4, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    const/high16 v5, 0x10000

    sub-int v5, p2, v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-lt v2, v4, :cond_0

    .line 333
    iget v4, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v4, v2

    aget-byte v4, p1, v4

    iget v5, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v5, p2

    aget-byte v5, p1, v5

    if-ne v4, v5, :cond_3

    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->readIntEquals([BII)Z
    invoke-static {p1, v2, p2}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$2([BII)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 334
    add-int/lit8 v4, v2, 0x4

    add-int/lit8 v5, p2, 0x4

    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->commonBytes([BIII)I
    invoke-static {p1, v4, v5, p3}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$3([BIII)I

    move-result v4

    add-int/lit8 v1, v4, 0x4

    .line 335
    .local v1, "matchLen":I
    iget v4, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    if-le v1, v4, :cond_3

    .line 336
    iput v2, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    .line 337
    iput v1, p4, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 340
    .end local v1    # "matchLen":I
    :cond_3
    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->next(I)I

    move-result v2

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method insertAndFindWiderMatch([BIIIILorg/apache/lucene/codecs/compressing/LZ4$Match;)Z
    .locals 8
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "startLimit"    # I
    .param p4, "matchLimit"    # I
    .param p5, "minLen"    # I
    .param p6, "match"    # Lorg/apache/lucene/codecs/compressing/LZ4$Match;

    .prologue
    .line 347
    iput p5, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 349
    invoke-virtual {p0, p2, p1}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->insert(I[B)V

    .line 351
    sub-int v0, p2, p3

    .line 352
    .local v0, "delta":I
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->hashPointer([BI)I

    move-result v5

    .line 353
    .local v5, "ref":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v6, 0x100

    if-lt v1, v6, :cond_1

    .line 371
    :cond_0
    iget v6, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    if-le v6, p5, :cond_3

    const/4 v6, 0x1

    :goto_1
    return v6

    .line 354
    :cond_1
    iget v6, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    const/high16 v7, 0x10000

    sub-int v7, p2, v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    if-lt v5, v6, :cond_0

    .line 357
    sub-int v6, v5, v0

    iget v7, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v6, v7

    aget-byte v6, p1, v6

    iget v7, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    add-int/2addr v7, p3

    aget-byte v7, p1, v7

    if-ne v6, v7, :cond_2

    .line 358
    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->readIntEquals([BII)Z
    invoke-static {p1, v5, p2}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$2([BII)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 359
    add-int/lit8 v6, v5, 0x4

    add-int/lit8 v7, p2, 0x4

    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->commonBytes([BIII)I
    invoke-static {p1, v6, v7, p4}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$3([BIII)I

    move-result v6

    add-int/lit8 v4, v6, 0x4

    .line 360
    .local v4, "matchLenForward":I
    iget v6, p0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->base:I

    # invokes: Lorg/apache/lucene/codecs/compressing/LZ4;->commonBytesBackward([BIIII)I
    invoke-static {p1, v5, p2, v6, p3}, Lorg/apache/lucene/codecs/compressing/LZ4;->access$4([BIIII)I

    move-result v3

    .line 361
    .local v3, "matchLenBackward":I
    add-int v2, v3, v4

    .line 362
    .local v2, "matchLen":I
    iget v6, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    if-le v2, v6, :cond_2

    .line 363
    iput v2, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->len:I

    .line 364
    sub-int v6, v5, v3

    iput v6, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->ref:I

    .line 365
    sub-int v6, p2, v3

    iput v6, p6, Lorg/apache/lucene/codecs/compressing/LZ4$Match;->start:I

    .line 368
    .end local v2    # "matchLen":I
    .end local v3    # "matchLenBackward":I
    .end local v4    # "matchLenForward":I
    :cond_2
    invoke-direct {p0, v5}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;->next(I)I

    move-result v5

    .line 353
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 371
    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method
