.class Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;
.super Lorg/apache/lucene/codecs/FieldInfosReader;
.source "Lucene40FieldInfosReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final LEGACY_DV_TYPE_KEY:Ljava/lang/String;

.field static final LEGACY_NORM_TYPE_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".dvtype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->LEGACY_DV_TYPE_KEY:Ljava/lang/String;

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".normtype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->LEGACY_NORM_TYPE_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosReader;-><init>()V

    .line 49
    return-void
.end method

.method private static getDocValuesType(B)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .locals 1
    .param p0, "b"    # B

    .prologue
    .line 153
    invoke-static {}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->values()[Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method


# virtual methods
.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/FieldInfos;
    .locals 30
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "iocontext"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    const-string v4, ""

    const-string v12, "fnm"

    move-object/from16 v0, p2

    invoke-static {v0, v4, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 54
    .local v18, "fileName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v21

    .line 56
    .local v21, "input":Lorg/apache/lucene/store/IndexInput;
    const/16 v25, 0x0

    .line 58
    .local v25, "success":Z
    :try_start_0
    const-string v4, "Lucene40FieldInfos"

    .line 59
    const/4 v12, 0x0

    .line 60
    const/4 v13, 0x0

    .line 58
    move-object/from16 v0, v21

    invoke-static {v0, v4, v12, v13}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 62
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v24

    .line 63
    .local v24, "size":I
    move/from16 v0, v24

    new-array v0, v0, [Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v20, v0

    .line 65
    .local v20, "infos":[Lorg/apache/lucene/index/FieldInfo;
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v24

    if-lt v0, v1, :cond_0

    .line 110
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v28

    cmp-long v4, v12, v28

    if-eqz v4, :cond_d

    .line 111
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "did not read all bytes from file \""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\": read "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v28

    move-wide/from16 v0, v28

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " vs size "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v28

    move-wide/from16 v0, v28

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " (resource: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v4, v12}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    .end local v19    # "i":I
    .end local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .end local v24    # "size":I
    :catchall_0
    move-exception v4

    .line 117
    if-eqz v25, :cond_f

    .line 118
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 122
    :goto_1
    throw v4

    .line 66
    .restart local v19    # "i":I
    .restart local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .restart local v24    # "size":I
    :cond_0
    :try_start_1
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v5

    .line 67
    .local v5, "name":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    .line 68
    .local v7, "fieldNumber":I
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v16

    .line 69
    .local v16, "bits":B
    and-int/lit8 v4, v16, 0x1

    if-eqz v4, :cond_3

    const/4 v6, 0x1

    .line 70
    .local v6, "isIndexed":Z
    :goto_2
    and-int/lit8 v4, v16, 0x2

    if-eqz v4, :cond_4

    const/4 v8, 0x1

    .line 71
    .local v8, "storeTermVector":Z
    :goto_3
    and-int/lit8 v4, v16, 0x10

    if-eqz v4, :cond_5

    const/4 v9, 0x1

    .line 72
    .local v9, "omitNorms":Z
    :goto_4
    and-int/lit8 v4, v16, 0x20

    if-eqz v4, :cond_6

    const/4 v10, 0x1

    .line 74
    .local v10, "storePayloads":Z
    :goto_5
    if-nez v6, :cond_7

    .line 75
    const/4 v11, 0x0

    .line 89
    .local v11, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :goto_6
    if-eqz v6, :cond_1

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v11, v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-gez v4, :cond_1

    .line 90
    const/4 v10, 0x0

    .line 93
    :cond_1
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v26

    .line 94
    .local v26, "val":B
    and-int/lit8 v4, v26, 0xf

    int-to-byte v4, v4

    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->getDocValuesType(B)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v23

    .line 95
    .local v23, "oldValuesType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    ushr-int/lit8 v4, v26, 0x4

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->getDocValuesType(B)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v22

    .line 96
    .local v22, "oldNormsType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v15

    .line 97
    .local v15, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v23

    iget-object v4, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v4, :cond_2

    .line 98
    sget-object v4, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->LEGACY_DV_TYPE_KEY:Ljava/lang/String;

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->name()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v15, v4, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_2
    move-object/from16 v0, v22

    iget-object v4, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v4, :cond_c

    .line 101
    move-object/from16 v0, v22

    iget-object v4, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    sget-object v12, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v4, v12, :cond_b

    .line 102
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "invalid norm type: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v4, v12}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 69
    .end local v6    # "isIndexed":Z
    .end local v8    # "storeTermVector":Z
    .end local v9    # "omitNorms":Z
    .end local v10    # "storePayloads":Z
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v15    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v22    # "oldNormsType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .end local v23    # "oldValuesType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .end local v26    # "val":B
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 70
    .restart local v6    # "isIndexed":Z
    :cond_4
    const/4 v8, 0x0

    goto :goto_3

    .line 71
    .restart local v8    # "storeTermVector":Z
    :cond_5
    const/4 v9, 0x0

    goto :goto_4

    .line 72
    .restart local v9    # "omitNorms":Z
    :cond_6
    const/4 v10, 0x0

    goto :goto_5

    .line 76
    .restart local v10    # "storePayloads":Z
    :cond_7
    and-int/lit8 v4, v16, 0x40

    if-eqz v4, :cond_8

    .line 77
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 78
    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_8
    and-int/lit8 v4, v16, -0x80

    if-eqz v4, :cond_9

    .line 79
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 80
    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_9
    and-int/lit8 v4, v16, 0x4

    if-eqz v4, :cond_a

    .line 81
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 82
    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .line 83
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_a
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .line 104
    .restart local v15    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v22    # "oldNormsType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .restart local v23    # "oldValuesType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .restart local v26    # "val":B
    :cond_b
    sget-object v4, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->LEGACY_NORM_TYPE_KEY:Ljava/lang/String;

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->name()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v15, v4, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_c
    new-instance v4, Lorg/apache/lucene/index/FieldInfo;

    .line 107
    move-object/from16 v0, v23

    iget-object v12, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-object/from16 v0, v22

    iget-object v13, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->mapping:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-static {v15}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v14

    invoke-direct/range {v4 .. v14}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 106
    aput-object v4, v20, v19

    .line 65
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 113
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "isIndexed":Z
    .end local v7    # "fieldNumber":I
    .end local v8    # "storeTermVector":Z
    .end local v9    # "omitNorms":Z
    .end local v10    # "storePayloads":Z
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v15    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "bits":B
    .end local v22    # "oldNormsType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .end local v23    # "oldValuesType":Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;
    .end local v26    # "val":B
    :cond_d
    new-instance v17, Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/FieldInfos;-><init>([Lorg/apache/lucene/index/FieldInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    .local v17, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    const/16 v25, 0x1

    .line 117
    if-eqz v25, :cond_e

    .line 118
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 115
    :goto_7
    return-object v17

    .line 119
    :cond_e
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 120
    aput-object v21, v4, v12

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_7

    .line 119
    .end local v17    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v19    # "i":I
    .end local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .end local v24    # "size":I
    :cond_f
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    .line 120
    aput-object v21, v12, v13

    invoke-static {v12}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_1
.end method
