.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->correctBuggyOrds(Lorg/apache/lucene/index/SortedDocValues;)Lorg/apache/lucene/index/SortedDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$in:Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/index/SortedDocValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;->val$in:Lorg/apache/lucene/index/SortedDocValues;

    .line 600
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 1
    .param p1, "docID"    # I

    .prologue
    .line 603
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;->val$in:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getValueCount()I
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;->val$in:Lorg/apache/lucene/index/SortedDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "ord"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 608
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;->val$in:Lorg/apache/lucene/index/SortedDocValues;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1, p2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 609
    return-void
.end method
