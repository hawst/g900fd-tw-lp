.class public abstract Lorg/apache/lucene/codecs/StoredFieldsWriter;
.super Ljava/lang/Object;
.source "StoredFieldsWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method public abstract abort()V
.end method

.method protected final addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 4
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/index/FieldInfos;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    const/4 v1, 0x0

    .line 113
    .local v1, "storedCount":I
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 119
    invoke-virtual {p0, v1}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->startDocument(I)V

    .line 121
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->finishDocument()V

    .line 128
    return-void

    .line 113
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexableField;

    .line 114
    .local v0, "field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableFieldType;->stored()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 115
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    .end local v0    # "field":Lorg/apache/lucene/index/IndexableField;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexableField;

    .line 122
    .restart local v0    # "field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/lucene/index/IndexableFieldType;->stored()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 123
    invoke-interface {v0}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v3

    invoke-virtual {p0, v3, v0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/IndexableField;)V

    goto :goto_1
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public finishDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    return-void
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;)I
    .locals 10
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    const/4 v1, 0x0

    .line 86
    .local v1, "docCount":I
    iget-object v6, p1, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    .line 106
    iget-object v6, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v6, v1}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V

    .line 107
    return v1

    .line 86
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReader;

    .line 87
    .local v5, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v4

    .line 88
    .local v4, "maxDoc":I
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v3

    .line 89
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 90
    if-eqz v3, :cond_2

    invoke-interface {v3, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v7

    if-nez v7, :cond_2

    .line 89
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    :cond_2
    invoke-virtual {v5, v2}, Lorg/apache/lucene/index/AtomicReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 101
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v0, v7}, Lorg/apache/lucene/codecs/StoredFieldsWriter;->addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/index/FieldInfos;)V

    .line 102
    add-int/lit8 v1, v1, 0x1

    .line 103
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    const-wide v8, 0x4072c00000000000L    # 300.0

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_1
.end method

.method public abstract startDocument(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/IndexableField;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
