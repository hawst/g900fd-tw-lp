.class final Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IntersectEnum"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private arcs:[Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

.field private currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

.field private final fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private final runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

.field private savedStartTerm:Lorg/apache/lucene/util/BytesRef;

.field private stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

.field private final term:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 557
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p2, "compiled"    # Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    .param p3, "startTerm"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x5

    const/4 v6, 0x0

    .line 818
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 562
    new-array v4, v5, [Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 569
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 822
    iget-object v4, p2, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

    .line 823
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    .line 824
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v4

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;

    .line 825
    new-array v4, v5, [Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    .line 826
    const/4 v3, 0x0

    .local v3, "idx":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    array-length v4, v4

    if-lt v3, v4, :cond_0

    .line 829
    const/4 v1, 0x0

    .local v1, "arcIdx":I
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v4, v4

    if-lt v1, v4, :cond_1

    .line 833
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    if-nez v4, :cond_2

    .line 834
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 846
    :goto_2
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 848
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 827
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v1    # "arcIdx":I
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    new-instance v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-direct {v5, p0, v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;I)V

    aput-object v5, v4, v3

    .line 826
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 830
    .restart local v1    # "arcIdx":I
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    new-instance v5, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v5}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    aput-object v5, v4, v1

    .line 829
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 836
    :cond_2
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    goto :goto_2

    .line 851
    .restart local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    aget-object v2, v4, v6

    .line 852
    .local v2, "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    iget-wide v4, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootBlockFP:J

    iput-wide v4, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fpOrig:J

    iput-wide v4, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    .line 853
    iput v6, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    .line 854
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/RunAutomaton;->getInitialState()I

    move-result v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->setState(I)V

    .line 855
    iput-object v0, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 856
    iget-object v4, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v4, Lorg/apache/lucene/util/BytesRef;

    iput-object v4, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->outputPrefix:Lorg/apache/lucene/util/BytesRef;

    .line 857
    iget-object v4, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootCode:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->load(Lorg/apache/lucene/util/BytesRef;)V

    .line 860
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    invoke-direct {p0, p3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->setSavedStartTerm(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 862
    :cond_4
    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    .line 863
    if-eqz p3, :cond_5

    .line 864
    invoke-direct {p0, p3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->seekToStartTerm(Lorg/apache/lucene/util/BytesRef;)V

    .line 866
    :cond_5
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/util/automaton/RunAutomaton;
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    return-object v0
.end method

.method private copyTerm()V
    .locals 6

    .prologue
    .line 1211
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v2

    add-int v0, v1, v2

    .line 1212
    .local v0, "len":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-ge v1, v0, :cond_0

    .line 1213
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-static {v2, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1215
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v5

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1216
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iput v0, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1217
    return-void
.end method

.method private getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 5
    .param p1, "ord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 894
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v2, v2

    if-lt p1, v2, :cond_0

    .line 896
    add-int/lit8 v2, p1, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v1, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    .line 897
    .local v1, "next":[Lorg/apache/lucene/util/fst/FST$Arc;
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 898
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v0, v2

    .local v0, "arcOrd":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    .line 901
    iput-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 903
    .end local v0    # "arcOrd":I
    .end local v1    # "next":[Lorg/apache/lucene/util/fst/FST$Arc;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v2, v2, p1

    return-object v2

    .line 899
    .restart local v0    # "arcOrd":I
    .restart local v1    # "next":[Lorg/apache/lucene/util/fst/FST$Arc;
    :cond_1
    new-instance v2, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v2}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    aput-object v2, v1, v0

    .line 898
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .locals 5
    .param p1, "ord"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 881
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    array-length v2, v2

    if-lt p1, v2, :cond_0

    .line 882
    add-int/lit8 v2, p1, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v0, v2, [Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    .line 883
    .local v0, "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 884
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    array-length v1, v2

    .local v1, "stackOrd":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_1

    .line 887
    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    .line 889
    .end local v0    # "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .end local v1    # "stackOrd":I
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    aget-object v2, v2, p1

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->ord:I

    if-eq v2, p1, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 885
    .restart local v0    # "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .restart local v1    # "stackOrd":I
    :cond_1
    new-instance v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;I)V

    aput-object v2, v0, v1

    .line 884
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 890
    .end local v0    # "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .end local v1    # "stackOrd":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    aget-object v2, v2, p1

    return-object v2
.end method

.method private getState()I
    .locals 5

    .prologue
    .line 977
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v1, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->state:I

    .line 978
    .local v1, "state":I
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 982
    return v1

    .line 979
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v4

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    invoke-virtual {v2, v1, v3}, Lorg/apache/lucene/util/automaton/RunAutomaton;->step(II)I

    move-result v1

    .line 980
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 978
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private pushFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .locals 8
    .param p1, "state"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 907
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    invoke-direct {p0, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->getFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-result-object v1

    .line 909
    .local v1, "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-wide v6, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->lastSubFP:J

    iput-wide v6, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fpOrig:J

    iput-wide v6, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    .line 910
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    .line 912
    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->setState(I)V

    .line 918
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v0, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 919
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v2, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    .line 920
    .local v2, "idx":I
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v5

    if-gtz v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 907
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v1    # "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .end local v2    # "idx":I
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->ord:I

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 921
    .restart local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .restart local v1    # "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
    .restart local v2    # "idx":I
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v3, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->outputPrefix:Lorg/apache/lucene/util/BytesRef;

    .line 922
    .local v3, "output":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    iget v5, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    if-lt v2, v5, :cond_2

    .line 933
    iput-object v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 934
    iput-object v3, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->outputPrefix:Lorg/apache/lucene/util/BytesRef;

    .line 935
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 923
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v5, v5, v2

    and-int/lit16 v4, v5, 0xff

    .line 927
    .local v4, "target":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {p0, v6}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v5, v4, v0, v6, v7}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 928
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-nez v0, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 929
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v5

    iget-object v6, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6, v3, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "output":Lorg/apache/lucene/util/BytesRef;
    check-cast v3, Lorg/apache/lucene/util/BytesRef;

    .line 930
    .restart local v3    # "output":Lorg/apache/lucene/util/BytesRef;
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 936
    .end local v4    # "target":I
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v5

    iget-object v6, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v5, v0, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6, v3, v5}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->load(Lorg/apache/lucene/util/BytesRef;)V

    .line 937
    return-object v1
.end method

.method private seekToStartTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 17
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 991
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->ord:I

    if-eqz v12, :cond_0

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 992
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v12, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v12, v13, :cond_1

    .line 993
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v13, v13, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v13

    iput-object v13, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 995
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    const/4 v13, 0x0

    aget-object v2, v12, v13

    .line 996
    .local v2, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    if-eq v2, v12, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 998
    :cond_2
    const/4 v4, 0x0

    .local v4, "idx":I
    :goto_0
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v4, v12, :cond_4

    .line 1061
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_7

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1022
    .local v5, "isSubBlock":Z
    .local v6, "saveLastSubFP":J
    .local v8, "savePos":I
    .local v9, "saveStartBytePos":I
    .local v10, "saveSuffix":I
    .local v11, "saveTermBlockOrd":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v3

    .line 1023
    .local v3, "cmp":I
    if-gez v3, :cond_6

    .line 1024
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    if-ne v12, v13, :cond_4

    .line 1025
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-boolean v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLastInFloor:Z

    if-nez v12, :cond_7

    .line 1027
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->loadNextFloorBlock()V

    .line 1001
    .end local v3    # "cmp":I
    .end local v5    # "isSubBlock":Z
    .end local v6    # "saveLastSubFP":J
    .end local v8    # "savePos":I
    .end local v9    # "saveStartBytePos":I
    .end local v10    # "saveSuffix":I
    .end local v11    # "saveTermBlockOrd":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v8

    .line 1002
    .restart local v8    # "savePos":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v9

    .line 1003
    .restart local v9    # "saveStartBytePos":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v10

    .line 1004
    .restart local v10    # "saveSuffix":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-wide v6, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->lastSubFP:J

    .line 1005
    .restart local v6    # "saveLastSubFP":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v11, v12, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 1007
    .restart local v11    # "saveTermBlockOrd":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->next()Z

    move-result v5

    .line 1010
    .restart local v5    # "isSubBlock":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v14}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v14

    add-int/2addr v13, v14

    iput v13, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1011
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v12, v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v13, v13, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v12, v13, :cond_5

    .line 1012
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v13, v13, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v14, v14, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v13

    iput-object v13, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1014
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static {v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v14, v14, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v15, v15, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v16, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v16

    invoke-static/range {v12 .. v16}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1016
    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1019
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->getState()I

    move-result v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->pushFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    .line 998
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1035
    .restart local v3    # "cmp":I
    :cond_6
    if-nez v3, :cond_8

    .line 1062
    .end local v3    # "cmp":I
    .end local v5    # "isSubBlock":Z
    .end local v6    # "saveLastSubFP":J
    .end local v8    # "savePos":I
    .end local v9    # "saveStartBytePos":I
    .end local v10    # "saveSuffix":I
    .end local v11    # "saveTermBlockOrd":I
    :cond_7
    :goto_1
    return-void

    .line 1043
    .restart local v3    # "cmp":I
    .restart local v5    # "isSubBlock":Z
    .restart local v6    # "saveLastSubFP":J
    .restart local v8    # "savePos":I
    .restart local v9    # "saveStartBytePos":I
    .restart local v10    # "saveSuffix":I
    .restart local v11    # "saveTermBlockOrd":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    add-int/lit8 v13, v13, -0x1

    iput v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    .line 1044
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iput-wide v6, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->lastSubFP:J

    .line 1045
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-static {v12, v9}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;I)V

    .line 1046
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-static {v12, v10}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;I)V

    .line 1047
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v12, v8}, Lorg/apache/lucene/store/ByteArrayDataInput;->setPosition(I)V

    .line 1048
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iput v11, v12, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 1049
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static {v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v14, v14, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v15, v15, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v16, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v16

    invoke-static/range {v12 .. v16}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1050
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static {v14}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v14

    add-int/2addr v13, v14

    iput v13, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1
.end method

.method private setSavedStartTerm(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "startTerm"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 870
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->savedStartTerm:Lorg/apache/lucene/util/BytesRef;

    .line 871
    const/4 v0, 0x1

    return v0

    .line 870
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 948
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->decodeMetaData()V

    .line 950
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 6
    .param p1, "skipDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 961
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->decodeMetaData()V

    .line 962
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/PostingsReaderBase;->docs(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 6
    .param p1, "skipDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 967
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_0

    .line 969
    const/4 v0, 0x0

    .line 973
    :goto_0
    return-object v0

    .line 972
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->decodeMetaData()V

    .line 973
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/PostingsReaderBase;->docsAndPositions(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1221
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1075
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_2

    .line 1092
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->next()Z

    move-result v7

    .line 1101
    .local v7, "isSubBlock":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v21

    if-eqz v21, :cond_1

    .line 1102
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v22

    aget-byte v21, v21, v22

    move/from16 v0, v21

    and-int/lit16 v8, v0, 0xff

    .line 1103
    .local v8, "label":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->curTransitionMax:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-gt v8, v0, :cond_5

    .line 1122
    .end local v8    # "label":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    if-nez v7, :cond_9

    .line 1123
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v22

    add-int v20, v21, v22

    .line 1124
    .local v20, "termLen":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_0

    .line 1132
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    .line 1133
    .local v13, "suffixBytes":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v2, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1135
    .local v2, "commonSuffixBytes":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v22

    sub-int v9, v21, v22

    .line 1136
    .local v9, "lenInPrefix":I
    sget-boolean v21, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v21, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v21, v0

    if-eqz v21, :cond_7

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 1076
    .end local v2    # "commonSuffixBytes":[B
    .end local v7    # "isSubBlock":Z
    .end local v9    # "lenInPrefix":I
    .end local v13    # "suffixBytes":[B
    .end local v20    # "termLen":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLastInFloor:Z

    move/from16 v21, v0

    if-nez v21, :cond_3

    .line 1078
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->loadNextFloorBlock()V

    goto/16 :goto_0

    .line 1082
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->ord:I

    move/from16 v21, v0

    if-nez v21, :cond_4

    .line 1083
    const/16 v21, 0x0

    .line 1202
    :goto_2
    return-object v21

    .line 1085
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fpOrig:J

    .line 1086
    .local v10, "lastFP":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->ord:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget-object v21, v21, v22

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    .line 1087
    sget-boolean v21, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->lastSubFP:J

    move-wide/from16 v22, v0

    cmp-long v21, v22, v10

    if-eqz v21, :cond_0

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 1104
    .end local v10    # "lastFP":J
    .restart local v7    # "isSubBlock":Z
    .restart local v8    # "label":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitionIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    array-length v0, v0

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_6

    .line 1111
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLastInFloor:Z

    .line 1112
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    goto/16 :goto_0

    .line 1115
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitionIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitionIndex:I

    .line 1116
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitionIndex:I

    move/from16 v23, v0

    aget-object v22, v22, v23

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/util/automaton/Transition;->getMax()I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->curTransitionMax:I

    goto/16 :goto_1

    .line 1138
    .end local v8    # "label":I
    .restart local v2    # "commonSuffixBytes":[B
    .restart local v9    # "lenInPrefix":I
    .restart local v13    # "suffixBytes":[B
    .restart local v20    # "termLen":I
    :cond_7
    const/4 v3, 0x0

    .line 1140
    .local v3, "commonSuffixBytesPos":I
    if-lez v9, :cond_b

    .line 1144
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v16, v0

    .line 1145
    .local v16, "termBytes":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move/from16 v21, v0

    sub-int v17, v21, v9

    .line 1146
    .local v17, "termBytesPos":I
    sget-boolean v21, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v21, :cond_8

    if-gez v17, :cond_8

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 1147
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->prefix:I

    move/from16 v19, v0

    .local v19, "termBytesPosEnd":I
    move/from16 v18, v17

    .end local v17    # "termBytesPos":I
    .local v18, "termBytesPos":I
    move v4, v3

    .line 1148
    .end local v3    # "commonSuffixBytesPos":I
    .local v4, "commonSuffixBytesPos":I
    :goto_3
    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_a

    .line 1156
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v14

    .local v14, "suffixBytesPos":I
    move v3, v4

    .line 1162
    .end local v4    # "commonSuffixBytesPos":I
    .end local v16    # "termBytes":[B
    .end local v18    # "termBytesPos":I
    .end local v19    # "termBytesPosEnd":I
    .restart local v3    # "commonSuffixBytesPos":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v5, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .local v5, "commonSuffixBytesPosEnd":I
    move v4, v3

    .end local v3    # "commonSuffixBytesPos":I
    .restart local v4    # "commonSuffixBytesPos":I
    move v15, v14

    .line 1163
    .end local v14    # "suffixBytesPos":I
    .local v15, "suffixBytesPos":I
    :goto_5
    if-lt v4, v5, :cond_c

    .line 1180
    .end local v2    # "commonSuffixBytes":[B
    .end local v4    # "commonSuffixBytesPos":I
    .end local v5    # "commonSuffixBytesPosEnd":I
    .end local v9    # "lenInPrefix":I
    .end local v13    # "suffixBytes":[B
    .end local v15    # "suffixBytesPos":I
    .end local v20    # "termLen":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->state:I

    .line 1181
    .local v12, "state":I
    const/4 v6, 0x0

    .local v6, "idx":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v21

    move/from16 v0, v21

    if-lt v6, v0, :cond_d

    .line 1192
    if-eqz v7, :cond_e

    .line 1195
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->copyTerm()V

    .line 1196
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->pushFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    goto/16 :goto_0

    .line 1149
    .end local v6    # "idx":I
    .end local v12    # "state":I
    .restart local v2    # "commonSuffixBytes":[B
    .restart local v4    # "commonSuffixBytesPos":I
    .restart local v9    # "lenInPrefix":I
    .restart local v13    # "suffixBytes":[B
    .restart local v16    # "termBytes":[B
    .restart local v18    # "termBytesPos":I
    .restart local v19    # "termBytesPosEnd":I
    .restart local v20    # "termLen":I
    :cond_a
    add-int/lit8 v17, v18, 0x1

    .end local v18    # "termBytesPos":I
    .restart local v17    # "termBytesPos":I
    aget-byte v21, v16, v18

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "commonSuffixBytesPos":I
    .restart local v3    # "commonSuffixBytesPos":I
    aget-byte v22, v2, v4

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    move/from16 v18, v17

    .end local v17    # "termBytesPos":I
    .restart local v18    # "termBytesPos":I
    move v4, v3

    .end local v3    # "commonSuffixBytesPos":I
    .restart local v4    # "commonSuffixBytesPos":I
    goto :goto_3

    .line 1158
    .end local v4    # "commonSuffixBytesPos":I
    .end local v16    # "termBytes":[B
    .end local v18    # "termBytesPos":I
    .end local v19    # "termBytesPosEnd":I
    .restart local v3    # "commonSuffixBytesPos":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v21, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I
    invoke-static/range {v22 .. v22}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v22

    add-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v22, v0

    sub-int v14, v21, v22

    .restart local v14    # "suffixBytesPos":I
    goto :goto_4

    .line 1164
    .end local v3    # "commonSuffixBytesPos":I
    .end local v14    # "suffixBytesPos":I
    .restart local v4    # "commonSuffixBytesPos":I
    .restart local v5    # "commonSuffixBytesPosEnd":I
    .restart local v15    # "suffixBytesPos":I
    :cond_c
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "suffixBytesPos":I
    .restart local v14    # "suffixBytesPos":I
    aget-byte v21, v13, v15

    add-int/lit8 v3, v4, 0x1

    .end local v4    # "commonSuffixBytesPos":I
    .restart local v3    # "commonSuffixBytesPos":I
    aget-byte v22, v2, v4

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    move v4, v3

    .end local v3    # "commonSuffixBytesPos":I
    .restart local v4    # "commonSuffixBytesPos":I
    move v15, v14

    .end local v14    # "suffixBytesPos":I
    .restart local v15    # "suffixBytesPos":I
    goto :goto_5

    .line 1182
    .end local v2    # "commonSuffixBytes":[B
    .end local v4    # "commonSuffixBytesPos":I
    .end local v5    # "commonSuffixBytesPosEnd":I
    .end local v9    # "lenInPrefix":I
    .end local v13    # "suffixBytes":[B
    .end local v15    # "suffixBytesPos":I
    .end local v20    # "termLen":I
    .restart local v6    # "idx":I
    .restart local v12    # "state":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    move-object/from16 v23, v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I
    invoke-static/range {v23 .. v23}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I

    move-result v23

    add-int v23, v23, v6

    aget-byte v22, v22, v23

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v12, v1}, Lorg/apache/lucene/util/automaton/RunAutomaton;->step(II)I

    move-result v12

    .line 1183
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v12, v0, :cond_0

    .line 1181
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_6

    .line 1198
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lorg/apache/lucene/util/automaton/RunAutomaton;->isAccept(I)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 1199
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->copyTerm()V

    .line 1201
    sget-boolean v21, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->$assertionsDisabled:Z

    if-nez v21, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->savedStartTerm:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    if-eqz v21, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->savedStartTerm:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v21

    if-gtz v21, :cond_f

    new-instance v21, Ljava/lang/AssertionError;

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "saveStartTerm="

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->savedStartTerm:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " term="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v21

    .line 1202
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v21, v0

    goto/16 :goto_2
.end method

.method public ord()J
    .locals 1

    .prologue
    .line 1236
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z

    .prologue
    .line 1241
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J

    .prologue
    .line 1231
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z

    .prologue
    .line 1226
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 876
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->decodeMetaData()V

    .line 877
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTermState;->clone()Lorg/apache/lucene/index/TermState;

    move-result-object v0

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 955
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->decodeMetaData()V

    .line 956
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    return-wide v0
.end method
