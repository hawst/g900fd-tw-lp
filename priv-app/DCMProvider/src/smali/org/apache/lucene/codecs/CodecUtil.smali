.class public final Lorg/apache/lucene/codecs/CodecUtil;
.super Ljava/lang/Object;
.source "CodecUtil.java"


# static fields
.field public static final CODEC_MAGIC:I = 0x3fd76c17


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I
    .locals 5
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "codec"    # Ljava/lang/String;
    .param p2, "minVersion"    # I
    .param p3, "maxVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v4, 0x3fd76c17

    .line 126
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v0

    .line 127
    .local v0, "actualHeader":I
    if-eq v0, v4, :cond_0

    .line 128
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "codec header mismatch: actual header="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs expected header="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeaderNoMagic(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v1

    return v1
.end method

.method public static checkHeaderNoMagic(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I
    .locals 5
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "codec"    # Ljava/lang/String;
    .param p2, "minVersion"    # I
    .param p3, "maxVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "actualCodec":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 140
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "codec mismatch: actual codec="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vs expected codec="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (resource: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 143
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v1

    .line 144
    .local v1, "actualVersion":I
    if-ge v1, p2, :cond_1

    .line 145
    new-instance v2, Lorg/apache/lucene/index/IndexFormatTooOldException;

    invoke-direct {v2, p0, v1, p2, p3}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v2

    .line 147
    :cond_1
    if-le v1, p3, :cond_2

    .line 148
    new-instance v2, Lorg/apache/lucene/index/IndexFormatTooNewException;

    invoke-direct {v2, p0, v1, p2, p3}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v2

    .line 151
    :cond_2
    return v1
.end method

.method public static headerLength(Ljava/lang/String;)I
    .locals 1
    .param p0, "codec"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x9

    return v0
.end method

.method public static writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V
    .locals 4
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "codec"    # Ljava/lang/String;
    .param p2, "version"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    .line 76
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    iget v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    const/16 v2, 0x80

    if-lt v1, v2, :cond_1

    .line 77
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "codec must be simple ASCII, less than 128 characters in length [got "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :cond_1
    const v1, 0x3fd76c17

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 80
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0, p2}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 82
    return-void
.end method
