.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesVarDeref(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final synthetic val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 452
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 12
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const-wide/16 v10, 0x1

    const/4 v8, 0x1

    .line 455
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v3, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    .line 456
    .local v4, "startAddress":J
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 457
    .local v2, "lengthBytes":Lorg/apache/lucene/util/BytesRef;
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    invoke-virtual {v3, v2, v4, v5, v8}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 458
    iget-object v3, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    aget-byte v0, v3, v6

    .line 459
    .local v0, "code":B
    and-int/lit16 v3, v0, 0x80

    if-nez v3, :cond_0

    .line 461
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    add-long v6, v4, v10

    invoke-virtual {v3, p2, v6, v7, v0}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 467
    :goto_0
    return-void

    .line 463
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    add-long v6, v4, v10

    invoke-virtual {v3, v2, v6, v7, v8}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 464
    and-int/lit8 v3, v0, 0x7f

    shl-int/lit8 v3, v3, 0x8

    iget-object v6, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v7, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    or-int v1, v3, v6

    .line 465
    .local v1, "length":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    const-wide/16 v6, 0x2

    add-long/2addr v6, v4

    invoke-virtual {v3, p2, v6, v7, v1}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    goto :goto_0
.end method
