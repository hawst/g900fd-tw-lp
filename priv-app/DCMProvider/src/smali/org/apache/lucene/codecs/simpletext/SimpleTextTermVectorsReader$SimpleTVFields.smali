.class Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;
.super Lorg/apache/lucene/index/Fields;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTVFields"
.end annotation


# instance fields
.field private final fields:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;Ljava/util/SortedMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p2, "fields":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Ljava/lang/String;Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;>;"
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Fields;-><init>()V

    .line 236
    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;->fields:Ljava/util/SortedMap;

    .line 237
    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;->fields:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;->fields:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVFields;->fields:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Terms;

    return-object v0
.end method
