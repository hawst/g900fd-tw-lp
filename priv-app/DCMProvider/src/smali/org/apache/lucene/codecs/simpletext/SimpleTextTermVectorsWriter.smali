.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;
.super Lorg/apache/lucene/codecs/TermVectorsWriter;
.source "SimpleTextTermVectorsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final DOC:Lorg/apache/lucene/util/BytesRef;

.field static final END:Lorg/apache/lucene/util/BytesRef;

.field static final ENDOFFSET:Lorg/apache/lucene/util/BytesRef;

.field static final FIELD:Lorg/apache/lucene/util/BytesRef;

.field static final FIELDNAME:Lorg/apache/lucene/util/BytesRef;

.field static final FIELDOFFSETS:Lorg/apache/lucene/util/BytesRef;

.field static final FIELDPAYLOADS:Lorg/apache/lucene/util/BytesRef;

.field static final FIELDPOSITIONS:Lorg/apache/lucene/util/BytesRef;

.field static final FIELDTERMCOUNT:Lorg/apache/lucene/util/BytesRef;

.field static final NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

.field static final PAYLOAD:Lorg/apache/lucene/util/BytesRef;

.field static final POSITION:Lorg/apache/lucene/util/BytesRef;

.field static final STARTOFFSET:Lorg/apache/lucene/util/BytesRef;

.field static final TERMFREQ:Lorg/apache/lucene/util/BytesRef;

.field static final TERMTEXT:Lorg/apache/lucene/util/BytesRef;

.field static final VECTORS_EXTENSION:Ljava/lang/String; = "vec"


# instance fields
.field private final directory:Lorg/apache/lucene/store/Directory;

.field private numDocsWritten:I

.field private offsets:Z

.field private out:Lorg/apache/lucene/store/IndexOutput;

.field private payloads:Z

.field private positions:Z

.field private final scratch:Lorg/apache/lucene/util/BytesRef;

.field private final segment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->$assertionsDisabled:Z

    .line 41
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "END"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    .line 42
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "doc "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    .line 43
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  numfields "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    .line 44
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  field "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    .line 45
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    name "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDNAME:Lorg/apache/lucene/util/BytesRef;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    positions "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPOSITIONS:Lorg/apache/lucene/util/BytesRef;

    .line 47
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    offsets   "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDOFFSETS:Lorg/apache/lucene/util/BytesRef;

    .line 48
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    payloads  "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPAYLOADS:Lorg/apache/lucene/util/BytesRef;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    numterms "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDTERMCOUNT:Lorg/apache/lucene/util/BytesRef;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    term "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMTEXT:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      freq "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMFREQ:Lorg/apache/lucene/util/BytesRef;

    .line 52
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      position "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->POSITION:Lorg/apache/lucene/util/BytesRef;

    .line 53
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "        payload "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "        startoffset "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->STARTOFFSET:Lorg/apache/lucene/util/BytesRef;

    .line 55
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "        endoffset "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->ENDOFFSET:Lorg/apache/lucene/util/BytesRef;

    .line 57
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 3
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;-><init>()V

    .line 62
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->numDocsWritten:I

    .line 63
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 70
    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->segment:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    .line 73
    .local v0, "success":Z
    :try_start_0
    const-string v1, ""

    const-string v2, "vec"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    const/4 v0, 0x1

    .line 76
    if-nez v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->abort()V

    .line 80
    :cond_0
    return-void

    .line 75
    :catchall_0
    move-exception v1

    .line 76
    if-nez v0, :cond_1

    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->abort()V

    .line 79
    :cond_1
    throw v1
.end method

.method private newLine()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 207
    return-void
.end method

.method private write(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 199
    return-void
.end method

.method private write(Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 203
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 6

    .prologue
    .line 169
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "vec"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    .line 172
    return-void

    .line 170
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public addPosition(IIILorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "startOffset"    # I
    .param p3, "endOffset"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->positions:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->offsets:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 140
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->positions:Z

    if-eqz v0, :cond_3

    .line 141
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->POSITION:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 142
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 143
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 145
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->payloads:Z

    if-eqz v0, :cond_3

    .line 146
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 147
    if-eqz p4, :cond_2

    .line 148
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p4, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 149
    :cond_1
    invoke-direct {p0, p4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 151
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 155
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->offsets:Z

    if-eqz v0, :cond_4

    .line 156
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->STARTOFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 157
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 160
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->ENDOFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 161
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 162
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 164
    :cond_4
    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 185
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 186
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 190
    return-void

    .line 187
    :catchall_0
    move-exception v0

    .line 188
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 189
    throw v0
.end method

.method public finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 3
    .param p1, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->numDocsWritten:I

    if-eq v0, p2, :cond_0

    .line 177
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mergeVectors produced an invalid result: mergedDocs is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but vec numDocs is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->numDocsWritten:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " file="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; now aborting this merge to prevent index corruption"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 180
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 181
    return-void
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public startDocument(I)V
    .locals 1
    .param p1, "numVectorFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 85
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->numDocsWritten:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 88
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 89
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 91
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->numDocsWritten:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->numDocsWritten:I

    .line 92
    return-void
.end method

.method public startField(Lorg/apache/lucene/index/FieldInfo;IZZZ)V
    .locals 1
    .param p1, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "numTerms"    # I
    .param p3, "positions"    # Z
    .param p4, "offsets"    # Z
    .param p5, "payloads"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 97
    iget v0, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 100
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDNAME:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 101
    iget-object v0, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 104
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPOSITIONS:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 105
    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 108
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDOFFSETS:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 109
    invoke-static {p4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 112
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDPAYLOADS:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 113
    invoke-static {p5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 114
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 116
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->FIELDTERMCOUNT:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 117
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 120
    iput-boolean p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->positions:Z

    .line 121
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->offsets:Z

    .line 122
    iput-boolean p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->payloads:Z

    .line 123
    return-void
.end method

.method public startTerm(Lorg/apache/lucene/util/BytesRef;I)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "freq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMTEXT:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 128
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 129
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 131
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->TERMFREQ:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 132
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->write(Ljava/lang/String;)V

    .line 133
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsWriter;->newLine()V

    .line 134
    return-void
.end method
