.class public final Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;
.super Ljava/lang/Object;
.source "CompressingStoredFieldsIndexWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BLOCK_SIZE:I = 0x400


# instance fields
.field blockChunks:I

.field blockDocs:I

.field final docBaseDeltas:[I

.field final fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

.field firstStartPointer:J

.field maxStartPointer:J

.field final startPointerDeltas:[J

.field totalDocs:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->$assertionsDisabled:Z

    .line 72
    return-void

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 2
    .param p1, "indexOutput"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x400

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    .line 89
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->reset()V

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->totalDocs:I

    .line 91
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->docBaseDeltas:[I

    .line 92
    new-array v0, v1, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->startPointerDeltas:[J

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 94
    return-void
.end method

.method static moveSignToLowOrderBit(J)J
    .locals 4
    .param p0, "n"    # J

    .prologue
    .line 75
    const/16 v0, 0x3f

    shr-long v0, p0, v0

    const/4 v2, 0x1

    shl-long v2, p0, v2

    xor-long/2addr v0, v2

    return-wide v0
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 97
    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    .line 98
    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockDocs:I

    .line 99
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    .line 100
    return-void
.end method

.method private writeBlock()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    sget-boolean v18, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->$assertionsDisabled:Z

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    if-gtz v18, :cond_0

    new-instance v18, Ljava/lang/AssertionError;

    invoke-direct/range {v18 .. v18}, Ljava/lang/AssertionError;-><init>()V

    throw v18

    .line 104
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 115
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 116
    const/4 v4, 0x0

    .line 120
    .local v4, "avgChunkDocs":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->totalDocs:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockDocs:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    invoke-virtual/range {v18 .. v19}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 122
    const/4 v9, 0x0

    .line 123
    .local v9, "docBase":I
    const-wide/16 v14, 0x0

    .line 124
    .local v14, "maxDelta":J
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v12, v0, :cond_2

    .line 130
    invoke-static {v14, v15}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v5

    .line 131
    .local v5, "bitsPerDocBase":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 132
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    .line 133
    sget-object v19, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v20, v0

    const/16 v21, 0x1

    .line 132
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v2, v5, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v13

    .line 134
    .local v13, "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    const/4 v9, 0x0

    .line 135
    const/4 v12, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v12, v0, :cond_3

    .line 141
    invoke-virtual {v13}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 146
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 147
    const-wide/16 v6, 0x0

    .line 151
    .local v6, "avgChunkSize":J
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 152
    const-wide/16 v16, 0x0

    .line 153
    .local v16, "startPointer":J
    const-wide/16 v14, 0x0

    .line 154
    const/4 v12, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v12, v0, :cond_6

    .line 160
    invoke-static {v14, v15}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v8

    .line 161
    .local v8, "bitsPerStartPointer":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v18, v0

    sget-object v19, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    .line 163
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v20, v0

    const/16 v21, 0x1

    .line 162
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v2, v8, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v13

    .line 164
    const-wide/16 v16, 0x0

    .line 165
    const/4 v12, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v12, v0, :cond_7

    .line 171
    invoke-virtual {v13}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    .line 172
    return-void

    .line 118
    .end local v4    # "avgChunkDocs":I
    .end local v5    # "bitsPerDocBase":I
    .end local v6    # "avgChunkSize":J
    .end local v8    # "bitsPerStartPointer":I
    .end local v9    # "docBase":I
    .end local v12    # "i":I
    .end local v13    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    .end local v14    # "maxDelta":J
    .end local v16    # "startPointer":J
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockDocs:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->docBaseDeltas:[I

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    aget v19, v19, v20

    sub-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v4

    .restart local v4    # "avgChunkDocs":I
    goto/16 :goto_0

    .line 125
    .restart local v9    # "docBase":I
    .restart local v12    # "i":I
    .restart local v14    # "maxDelta":J
    :cond_2
    mul-int v18, v4, v12

    sub-int v10, v9, v18

    .line 126
    .local v10, "delta":I
    int-to-long v0, v10

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->moveSignToLowOrderBit(J)J

    move-result-wide v18

    or-long v14, v14, v18

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->docBaseDeltas:[I

    move-object/from16 v18, v0

    aget v18, v18, v12

    add-int v9, v9, v18

    .line 124
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 136
    .end local v10    # "delta":I
    .restart local v5    # "bitsPerDocBase":I
    .restart local v13    # "writer":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_3
    mul-int v18, v4, v12

    sub-int v18, v9, v18

    move/from16 v0, v18

    int-to-long v10, v0

    .line 137
    .local v10, "delta":J
    sget-boolean v18, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->$assertionsDisabled:Z

    if-nez v18, :cond_4

    invoke-static {v10, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->moveSignToLowOrderBit(J)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v18

    invoke-virtual {v13}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->bitsPerValue()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_4

    new-instance v18, Ljava/lang/AssertionError;

    invoke-direct/range {v18 .. v18}, Ljava/lang/AssertionError;-><init>()V

    throw v18

    .line 138
    :cond_4
    invoke-static {v10, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->moveSignToLowOrderBit(J)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->docBaseDeltas:[I

    move-object/from16 v18, v0

    aget v18, v18, v12

    add-int v9, v9, v18

    .line 135
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 149
    .end local v10    # "delta":J
    :cond_5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->maxStartPointer:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    div-long v6, v18, v20

    .restart local v6    # "avgChunkSize":J
    goto/16 :goto_3

    .line 155
    .restart local v16    # "startPointer":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->startPointerDeltas:[J

    move-object/from16 v18, v0

    aget-wide v18, v18, v12

    add-long v16, v16, v18

    .line 156
    int-to-long v0, v12

    move-wide/from16 v18, v0

    mul-long v18, v18, v6

    sub-long v10, v16, v18

    .line 157
    .restart local v10    # "delta":J
    invoke-static {v10, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->moveSignToLowOrderBit(J)J

    move-result-wide v18

    or-long v14, v14, v18

    .line 154
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    .line 166
    .end local v10    # "delta":J
    .restart local v8    # "bitsPerStartPointer":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->startPointerDeltas:[J

    move-object/from16 v18, v0

    aget-wide v18, v18, v12

    add-long v16, v16, v18

    .line 167
    int-to-long v0, v12

    move-wide/from16 v18, v0

    mul-long v18, v18, v6

    sub-long v10, v16, v18

    .line 168
    .restart local v10    # "delta":J
    sget-boolean v18, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->$assertionsDisabled:Z

    if-nez v18, :cond_8

    invoke-static {v10, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->moveSignToLowOrderBit(J)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v18

    invoke-virtual {v13}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->bitsPerValue()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_8

    new-instance v18, Ljava/lang/AssertionError;

    invoke-direct/range {v18 .. v18}, Ljava/lang/AssertionError;-><init>()V

    throw v18

    .line 169
    :cond_8
    invoke-static {v10, v11}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->moveSignToLowOrderBit(J)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 165
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_5
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 207
    return-void
.end method

.method finish(I)V
    .locals 3
    .param p1, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->totalDocs:I

    if-eq p1, v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " docs, but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->totalDocs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    if-lez v0, :cond_1

    .line 199
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->writeBlock()V

    .line 201
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->fieldsIndexOut:Lorg/apache/lucene/store/IndexOutput;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 202
    return-void
.end method

.method writeIndex(IJ)V
    .locals 4
    .param p1, "numDocs"    # I
    .param p2, "startPointer"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    .line 176
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->writeBlock()V

    .line 177
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->reset()V

    .line 180
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 181
    iput-wide p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->maxStartPointer:J

    iput-wide p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    .line 183
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-wide v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->firstStartPointer:J

    cmp-long v0, p2, v0

    if-gez v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 185
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->docBaseDeltas:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    aput p1, v0, v1

    .line 186
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->startPointerDeltas:[J

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    iget-wide v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->maxStartPointer:J

    sub-long v2, p2, v2

    aput-wide v2, v0, v1

    .line 188
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockChunks:I

    .line 189
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockDocs:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->blockDocs:I

    .line 190
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->totalDocs:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->totalDocs:I

    .line 191
    iput-wide p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->maxStartPointer:J

    .line 192
    return-void
.end method
