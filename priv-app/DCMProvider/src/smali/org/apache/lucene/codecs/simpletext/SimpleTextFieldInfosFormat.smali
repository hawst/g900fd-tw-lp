.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;
.super Lorg/apache/lucene/codecs/FieldInfosFormat;
.source "SimpleTextFieldInfosFormat.java"


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/FieldInfosReader;

.field private final writer:Lorg/apache/lucene/codecs/FieldInfosWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosFormat;-><init>()V

    .line 33
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    .line 34
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;->writer:Lorg/apache/lucene/codecs/FieldInfosWriter;

    .line 32
    return-void
.end method


# virtual methods
.method public getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    return-object v0
.end method

.method public getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;->writer:Lorg/apache/lucene/codecs/FieldInfosWriter;

    return-object v0
.end method
