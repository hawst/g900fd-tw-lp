.class Lorg/apache/lucene/codecs/lucene41/Lucene41Codec$1;
.super Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsFormat;
.source "Lucene41Codec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V
    .locals 0
    .param p2, "$anonymous0"    # Ljava/lang/String;
    .param p3, "$anonymous1"    # Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .param p4, "$anonymous2"    # I

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec$1;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;

    .line 59
    invoke-direct {p0, p2, p3, p4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsFormat;-><init>(Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V

    return-void
.end method


# virtual methods
.method public fieldsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/StoredFieldsWriter;
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
