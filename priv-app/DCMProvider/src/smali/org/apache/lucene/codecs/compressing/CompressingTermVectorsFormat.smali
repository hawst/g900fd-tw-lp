.class public Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;
.super Lorg/apache/lucene/codecs/TermVectorsFormat;
.source "CompressingTermVectorsFormat.java"


# instance fields
.field private final chunkSize:I

.field private final compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field private final formatName:Ljava/lang/String;

.field private final segmentSuffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V
    .locals 2
    .param p1, "formatName"    # Ljava/lang/String;
    .param p2, "segmentSuffix"    # Ljava/lang/String;
    .param p3, "compressionMode"    # Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .param p4, "chunkSize"    # I

    .prologue
    .line 70
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsFormat;-><init>()V

    .line 72
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->formatName:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->segmentSuffix:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 75
    const/4 v0, 0x1

    if-ge p4, v0, :cond_0

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "chunkSize must be >= 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    iput p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->chunkSize:I

    .line 79
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "(compressionMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 99
    const-string v1, ", chunkSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->chunkSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final vectorsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 8
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->segmentSuffix:Ljava/lang/String;

    .line 86
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->formatName:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    .line 85
    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;)V

    return-object v0
.end method

.method public final vectorsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsWriter;
    .locals 8
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->segmentSuffix:Ljava/lang/String;

    .line 93
    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->formatName:Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    iget v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsFormat;->chunkSize:I

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    .line 92
    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V

    return-object v0
.end method
