.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;
.super Lorg/apache/lucene/index/Terms;
.source "Lucene3xTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVTerms"
.end annotation


# instance fields
.field private final numTerms:I

.field private final storeOffsets:Z

.field private final storePositions:Z

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

.field private final tvfFPStart:J

.field private final unicodeSortOrder:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;J)V
    .locals 4
    .param p2, "tvfFP"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 295
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 296
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 297
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->numTerms:I

    .line 298
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v0

    .line 299
    .local v0, "bits":B
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->storePositions:Z

    .line 300
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->storeOffsets:Z

    .line 301
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->tvfFPStart:J

    .line 302
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->sortTermsByUnicode()Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->unicodeSortOrder:Z

    .line 303
    return-void

    :cond_0
    move v1, v3

    .line 299
    goto :goto_0

    :cond_1
    move v2, v3

    .line 300
    goto :goto_1
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->unicodeSortOrder:Z

    if-eqz v0, :cond_0

    .line 344
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 346
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_0
.end method

.method public getDocCount()I
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x1

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2

    .prologue
    .line 333
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->numTerms:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2

    .prologue
    .line 327
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 352
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->storeOffsets:Z

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x0

    return v0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->storePositions:Z

    return v0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 7
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 308
    instance-of v1, p1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 309
    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;

    .line 310
    .local v0, "termsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;

    .end local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)V

    .line 316
    .restart local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;
    :cond_0
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->numTerms:I

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->tvfFPStart:J

    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->storePositions:Z

    iget-boolean v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->storeOffsets:Z

    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->unicodeSortOrder:Z

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->reset(IJZZZ)V

    .line 317
    return-object v0

    .line 314
    .end local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)V

    .restart local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;
    goto :goto_0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 322
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTerms;->numTerms:I

    int-to-long v0, v0

    return-wide v0
.end method
