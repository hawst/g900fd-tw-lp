.class public Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
.super Lorg/apache/lucene/codecs/FieldsConsumer;
.source "BlockTreeTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;,
        Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;,
        Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;,
        Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;,
        Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_BLOCK_SIZE:I = 0x30

.field public static final DEFAULT_MIN_BLOCK_SIZE:I = 0x19

.field static final OUTPUT_FLAGS_MASK:I = 0x3

.field static final OUTPUT_FLAGS_NUM_BITS:I = 0x2

.field static final OUTPUT_FLAG_HAS_TERMS:I = 0x2

.field static final OUTPUT_FLAG_IS_FLOOR:I = 0x1

.field static final TERMS_CODEC_NAME:Ljava/lang/String; = "BLOCK_TREE_TERMS_DICT"

.field static final TERMS_EXTENSION:Ljava/lang/String; = "tim"

.field static final TERMS_INDEX_CODEC_NAME:Ljava/lang/String; = "BLOCK_TREE_TERMS_INDEX"

.field static final TERMS_INDEX_EXTENSION:Ljava/lang/String; = "tip"

.field public static final TERMS_INDEX_VERSION_APPEND_ONLY:I = 0x1

.field public static final TERMS_INDEX_VERSION_CURRENT:I = 0x1

.field public static final TERMS_INDEX_VERSION_START:I = 0x0

.field public static final TERMS_VERSION_APPEND_ONLY:I = 0x1

.field public static final TERMS_VERSION_CURRENT:I = 0x1

.field public static final TERMS_VERSION_START:I


# instance fields
.field currentField:Lorg/apache/lucene/index/FieldInfo;

.field final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;",
            ">;"
        }
    .end annotation
.end field

.field private final indexOut:Lorg/apache/lucene/store/IndexOutput;

.field final maxItemsInBlock:I

.field final minItemsInBlock:I

.field private final out:Lorg/apache/lucene/store/IndexOutput;

.field final postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

.field final scratchBytes:Lorg/apache/lucene/store/RAMOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->$assertionsDisabled:Z

    .line 221
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/PostingsWriterBase;II)V
    .locals 10
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "postingsWriter"    # Lorg/apache/lucene/codecs/PostingsWriterBase;
    .param p3, "minItemsInBlock"    # I
    .param p4, "maxItemsInBlock"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 261
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldsConsumer;-><init>()V

    .line 254
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->fields:Ljava/util/List;

    .line 479
    new-instance v4, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v4}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->scratchBytes:Lorg/apache/lucene/store/RAMOutputStream;

    .line 268
    if-gt p3, v7, :cond_0

    .line 269
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "minItemsInBlock must be >= 2; got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 271
    :cond_0
    if-gtz p4, :cond_1

    .line 272
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "maxItemsInBlock must be >= 1; got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 274
    :cond_1
    if-le p3, p4, :cond_2

    .line 275
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "maxItemsInBlock must be >= minItemsInBlock; got maxItemsInBlock="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " minItemsInBlock="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 277
    :cond_2
    add-int/lit8 v4, p3, -0x1

    mul-int/lit8 v4, v4, 0x2

    if-le v4, p4, :cond_3

    .line 278
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "maxItemsInBlock must be at least 2*(minItemsInBlock-1); got maxItemsInBlock="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " minItemsInBlock="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 281
    :cond_3
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    const-string v6, "tim"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "termsFileName":Ljava/lang/String;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v2, v5}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 283
    const/4 v1, 0x0

    .line 284
    .local v1, "success":Z
    const/4 v0, 0x0

    .line 286
    .local v0, "indexOut":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 287
    iput p3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->minItemsInBlock:I

    .line 288
    iput p4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->maxItemsInBlock:I

    .line 289
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->writeHeader(Lorg/apache/lucene/store/IndexOutput;)V

    .line 293
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    const-string v6, "tip"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 294
    .local v3, "termsIndexFileName":Ljava/lang/String;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v3, v5}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    .line 295
    invoke-virtual {p0, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->writeIndexHeader(Lorg/apache/lucene/store/IndexOutput;)V

    .line 297
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->currentField:Lorg/apache/lucene/index/FieldInfo;

    .line 298
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    .line 303
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p2, v4}, Lorg/apache/lucene/codecs/PostingsWriterBase;->start(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    const/4 v1, 0x1

    .line 306
    if-nez v1, :cond_4

    new-array v4, v9, [Ljava/io/Closeable;

    .line 307
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v5, v4, v8

    aput-object v0, v4, v7

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 310
    :cond_4
    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    .line 311
    return-void

    .line 305
    .end local v3    # "termsIndexFileName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 306
    if-nez v1, :cond_5

    new-array v5, v9, [Ljava/io/Closeable;

    .line 307
    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v6, v5, v8

    aput-object v0, v5, v7

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 309
    :cond_5
    throw v4
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->fields:Ljava/util/List;

    return-object v0
.end method

.method static encodeOutput(JZZ)J
    .locals 6
    .param p0, "fp"    # J
    .param p2, "hasTerms"    # Z
    .param p3, "isFloor"    # Z

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x0

    .line 343
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    cmp-long v2, p0, v2

    if-ltz v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 344
    :cond_0
    shl-long v2, p0, v1

    if-eqz p2, :cond_2

    :goto_0
    int-to-long v4, v1

    or-long/2addr v2, v4

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :cond_1
    int-to-long v0, v0

    or-long/2addr v0, v2

    return-wide v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->currentField:Lorg/apache/lucene/index/FieldInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->currentField:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 338
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->currentField:Lorg/apache/lucene/index/FieldInfo;

    .line 339
    new-instance v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method public close()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1056
    const/4 v3, 0x0

    .line 1059
    .local v3, "ioe":Ljava/io/IOException;
    :try_start_0
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    .line 1060
    .local v0, "dirStart":J
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 1062
    .local v4, "indexDirStart":J
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->fields:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 1064
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->fields:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 1077
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p0, v7, v0, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->writeTrailer(Lorg/apache/lucene/store/IndexOutput;J)V

    .line 1078
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p0, v7, v4, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->writeIndexTrailer(Lorg/apache/lucene/store/IndexOutput;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1082
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/io/Closeable;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    aput-object v9, v7, v8

    invoke-static {v3, v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 1084
    .end local v0    # "dirStart":J
    .end local v4    # "indexDirStart":J
    :goto_1
    return-void

    .line 1064
    .restart local v0    # "dirStart":J
    .restart local v4    # "indexDirStart":J
    :cond_0
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;

    .line 1066
    .local v2, "field":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v9, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget v9, v9, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 1067
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v10, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->numTerms:J

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 1068
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v9, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->rootCode:Lorg/apache/lucene/util/BytesRef;

    iget v9, v9, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 1069
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v9, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->rootCode:Lorg/apache/lucene/util/BytesRef;

    iget-object v9, v9, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v10, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->rootCode:Lorg/apache/lucene/util/BytesRef;

    iget v10, v10, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v11, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->rootCode:Lorg/apache/lucene/util/BytesRef;

    iget v11, v11, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v8, v9, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 1070
    iget-object v8, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v8, v9, :cond_1

    .line 1071
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v10, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->sumTotalTermFreq:J

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 1073
    :cond_1
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v10, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->sumDocFreq:J

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 1074
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget v9, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->docCount:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 1075
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v10, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;->indexStartFP:J

    invoke-virtual {v8, v10, v11}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1079
    .end local v0    # "dirStart":J
    .end local v2    # "field":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;
    .end local v4    # "indexDirStart":J
    :catch_0
    move-exception v6

    .line 1080
    .local v6, "ioe2":Ljava/io/IOException;
    move-object v3, v6

    .line 1082
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/io/Closeable;

    const/4 v8, 0x0

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    aput-object v9, v7, v8

    invoke-static {v3, v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    goto :goto_1

    .line 1081
    .end local v6    # "ioe2":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 1082
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/io/Closeable;

    const/4 v9, 0x0

    iget-object v10, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    aput-object v10, v8, v9

    invoke-static {v3, v8}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 1083
    throw v7
.end method

.method protected writeHeader(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 2
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    const-string v0, "BLOCK_TREE_TERMS_DICT"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 316
    return-void
.end method

.method protected writeIndexHeader(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 2
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 320
    const-string v0, "BLOCK_TREE_TERMS_INDEX"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 321
    return-void
.end method

.method protected writeIndexTrailer(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 0
    .param p1, "indexOut"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "dirStart"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    invoke-virtual {p1, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 331
    return-void
.end method

.method protected writeTrailer(Lorg/apache/lucene/store/IndexOutput;J)V
    .locals 0
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "dirStart"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    invoke-virtual {p1, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 326
    return-void
.end method
