.class public final Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;
.super Lorg/apache/lucene/codecs/FieldInfosFormat;
.source "Lucene42FieldInfosFormat.java"


# static fields
.field static final CODEC_NAME:Ljava/lang/String; = "Lucene42FieldInfos"

.field static final EXTENSION:Ljava/lang/String; = "fnm"

.field static final FORMAT_CURRENT:I = 0x0

.field static final FORMAT_START:I = 0x0

.field static final IS_INDEXED:B = 0x1t

.field static final OMIT_NORMS:B = 0x10t

.field static final OMIT_POSITIONS:B = -0x80t

.field static final OMIT_TERM_FREQ_AND_POSITIONS:B = 0x40t

.field static final STORE_OFFSETS_IN_POSTINGS:B = 0x4t

.field static final STORE_PAYLOADS:B = 0x20t

.field static final STORE_TERMVECTOR:B = 0x2t


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/FieldInfosReader;

.field private final writer:Lorg/apache/lucene/codecs/FieldInfosWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosFormat;-><init>()V

    .line 88
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    .line 89
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosWriter;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;->writer:Lorg/apache/lucene/codecs/FieldInfosWriter;

    .line 93
    return-void
.end method


# virtual methods
.method public getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    return-object v0
.end method

.method public getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;->writer:Lorg/apache/lucene/codecs/FieldInfosWriter;

    return-object v0
.end method
