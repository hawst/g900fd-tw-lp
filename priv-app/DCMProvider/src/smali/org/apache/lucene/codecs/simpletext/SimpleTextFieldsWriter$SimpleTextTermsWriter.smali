.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;
.super Lorg/apache/lucene/codecs/TermsConsumer;
.source "SimpleTextFieldsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTextTermsWriter"
.end annotation


# instance fields
.field private final postingsWriter:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p2, "field"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 76
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermsConsumer;-><init>()V

    .line 77
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;

    invoke-direct {v0, p1, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/index/FieldInfo;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;

    .line 78
    return-void
.end method


# virtual methods
.method public finish(JJI)V
    .locals 0
    .param p1, "sumTotalTermFreq"    # J
    .param p3, "sumDocFreq"    # J
    .param p5, "docCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    return-void
.end method

.method public finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "stats"    # Lorg/apache/lucene/codecs/TermStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    return-void
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->reset(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;

    move-result-object v0

    return-object v0
.end method
