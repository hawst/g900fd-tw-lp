.class final Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;
.super Ljava/lang/Object;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Frame"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field arc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field curTransitionMax:I

.field entCount:I

.field floorData:[B

.field final floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field fp:J

.field fpEnd:J

.field fpOrig:J

.field isLastInFloor:Z

.field isLeafBlock:Z

.field lastSubFP:J

.field metaDataUpto:I

.field nextEnt:I

.field nextFloorLabel:I

.field numFollowFloorBlocks:I

.field final ord:I

.field outputPrefix:Lorg/apache/lucene/util/BytesRef;

.field prefix:I

.field private startBytePos:I

.field statBytes:[B

.field state:I

.field final statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field private suffix:I

.field suffixBytes:[B

.field final suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field final termState:Lorg/apache/lucene/codecs/BlockTermState;

.field final synthetic this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

.field transitionIndex:I

.field transitions:[Lorg/apache/lucene/util/automaton/Transition;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 574
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;I)V
    .locals 4
    .param p2, "ord"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 628
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 586
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    .line 587
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 589
    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statBytes:[B

    .line 590
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 592
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorData:[B

    .line 593
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 629
    iput p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->ord:I

    .line 630
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/PostingsReaderBase;->newTermState()Lorg/apache/lucene/codecs/BlockTermState;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    .line 631
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    .line 632
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I
    .locals 1

    .prologue
    .line 626
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;)I
    .locals 1

    .prologue
    .line 625
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I

    return v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;I)V
    .locals 0

    .prologue
    .line 625
    iput p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I

    return-void
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;I)V
    .locals 0

    .prologue
    .line 626
    iput p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I

    return-void
.end method


# virtual methods
.method public decodeMetaData()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 782
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->getTermBlockOrd()I

    move-result v0

    .line 783
    .local v0, "limit":I
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 787
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->metaDataUpto:I

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 790
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->metaDataUpto:I

    if-lt v1, v0, :cond_1

    .line 811
    return-void

    .line 800
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    .line 802
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_2

    .line 803
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    int-to-long v2, v2

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    .line 807
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v1

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v1

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/codecs/PostingsReaderBase;->nextTerm(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V

    .line 808
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->metaDataUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->metaDataUpto:I

    .line 809
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    goto :goto_0
.end method

.method public getTermBlockOrd()I
    .locals 1

    .prologue
    .line 776
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLeafBlock:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    goto :goto_0
.end method

.method load(Lorg/apache/lucene/util/BytesRef;)V
    .locals 10
    .param p1, "frameIndexData"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 668
    if-eqz p1, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    array-length v3, v3

    if-eqz v3, :cond_1

    .line 670
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorData:[B

    array-length v3, v3

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v3, v6, :cond_0

    .line 671
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorData:[B

    .line 673
    :cond_0
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorData:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v3, v6, v7, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 674
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorData:[B

    iget v7, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v3, v6, v5, v7}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 677
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v0

    .line 678
    .local v0, "code":J
    const-wide/16 v6, 0x1

    and-long/2addr v6, v0

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_1

    .line 679
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    .line 680
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readByte()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    .line 685
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/RunAutomaton;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/util/automaton/RunAutomaton;

    move-result-object v3

    iget v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->state:I

    invoke-virtual {v3, v6}, Lorg/apache/lucene/util/automaton/RunAutomaton;->isAccept(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 687
    :goto_0
    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    if-eqz v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v6, v6, v5

    invoke-virtual {v6}, Lorg/apache/lucene/util/automaton/Transition;->getMin()I

    move-result v6

    if-le v3, v6, :cond_2

    .line 701
    .end local v0    # "code":J
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 702
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 703
    .local v0, "code":I
    ushr-int/lit8 v3, v0, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    .line 704
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->$assertionsDisabled:Z

    if-nez v3, :cond_4

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    if-gtz v3, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 688
    .local v0, "code":J
    :cond_2
    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fpOrig:J

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v8

    ushr-long/2addr v8, v4

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    .line 689
    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    .line 691
    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    if-eqz v3, :cond_3

    .line 692
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readByte()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    goto :goto_0

    .line 694
    :cond_3
    const/16 v3, 0x100

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    goto :goto_0

    .line 705
    .local v0, "code":I
    :cond_4
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_8

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLastInFloor:Z

    .line 708
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 709
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_9

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLeafBlock:Z

    .line 710
    ushr-int/lit8 v2, v0, 0x1

    .line 712
    .local v2, "numBytes":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    array-length v3, v3

    if-ge v3, v2, :cond_5

    .line 713
    invoke-static {v2, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    .line 715
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    invoke-virtual {v3, v6, v5, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 716
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixBytes:[B

    invoke-virtual {v3, v6, v5, v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 719
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 720
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statBytes:[B

    array-length v3, v3

    if-ge v3, v2, :cond_6

    .line 721
    invoke-static {v2, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statBytes:[B

    .line 723
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statBytes:[B

    invoke-virtual {v3, v4, v5, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 724
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->statBytes:[B

    invoke-virtual {v3, v4, v5, v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 725
    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->metaDataUpto:I

    .line 727
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iput v5, v3, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 728
    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    .line 730
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v3

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v3

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v5

    iget-object v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/lucene/codecs/PostingsReaderBase;->readTermsBlock(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V

    .line 732
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLastInFloor:Z

    if-nez v3, :cond_7

    .line 735
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fpEnd:J

    .line 737
    :cond_7
    return-void

    .end local v2    # "numBytes":I
    :cond_8
    move v3, v5

    .line 705
    goto/16 :goto_1

    :cond_9
    move v3, v5

    .line 709
    goto/16 :goto_2
.end method

.method loadNextFloorBlock()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 635
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 639
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fpOrig:J

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v2

    const/4 v4, 0x1

    ushr-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    .line 640
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    .line 642
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    if-eqz v0, :cond_2

    .line 643
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    .line 648
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->numFollowFloorBlocks:I

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitionIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/Transition;->getMin()I

    move-result v1

    .line 638
    if-le v0, v1, :cond_0

    .line 650
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->load(Lorg/apache/lucene/util/BytesRef;)V

    .line 651
    return-void

    .line 645
    :cond_2
    const/16 v0, 0x100

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextFloorLabel:I

    goto :goto_0
.end method

.method public next()Z
    .locals 1

    .prologue
    .line 742
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->isLeafBlock:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextLeaf()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextNonLeaf()Z

    move-result v0

    goto :goto_0
.end method

.method public nextLeaf()Z
    .locals 4

    .prologue
    .line 748
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    if-lt v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "nextEnt="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " entCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 749
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    .line 750
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I

    .line 751
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I

    .line 752
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->skipBytes(I)V

    .line 753
    const/4 v0, 0x0

    return v0
.end method

.method public nextNonLeaf()Z
    .locals 6

    .prologue
    .line 758
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    if-lt v1, v2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nextEnt="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " entCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->entCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 759
    :cond_1
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->nextEnt:I

    .line 760
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    .line 761
    .local v0, "code":I
    ushr-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I

    .line 762
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->startBytePos:I

    .line 763
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffix:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->skipBytes(I)V

    .line 764
    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_2

    .line 766
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->termState:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 767
    const/4 v1, 0x0

    .line 771
    :goto_0
    return v1

    .line 770
    :cond_2
    iget-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->fp:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->lastSubFP:J

    .line 771
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x0

    .line 654
    iput p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->state:I

    .line 655
    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitionIndex:I

    .line 656
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->compiledAutomaton:Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;)Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v0, v0, p1

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    .line 657
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 658
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->transitions:[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/Transition;->getMax()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->curTransitionMax:I

    .line 662
    :goto_0
    return-void

    .line 660
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum$Frame;->curTransitionMax:I

    goto :goto_0
.end method
