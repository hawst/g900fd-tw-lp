.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;
.super Lorg/apache/lucene/codecs/SegmentInfoReader;
.source "SimpleTextSegmentInfoReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoReader;-><init>()V

    return-void
.end method

.method private readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 5
    .param p1, "offset"    # I
    .param p2, "scratch"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 125
    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v2, p1

    iget v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, p1

    sget-object v4, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method


# virtual methods
.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentInfo;
    .locals 25
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v21, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 50
    .local v21, "scratch":Lorg/apache/lucene/util/BytesRef;
    const-string v4, ""

    const-string v6, "si"

    move-object/from16 v0, p2

    invoke-static {v0, v4, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 51
    .local v22, "segFileName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v16

    .line 52
    .local v16, "input":Lorg/apache/lucene/store/IndexInput;
    const/16 v23, 0x0

    .line 54
    .local v23, "success":Z
    :try_start_0
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 55
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_VERSION:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :catchall_0
    move-exception v4

    .line 116
    if-nez v23, :cond_f

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/io/Closeable;

    const/4 v9, 0x0

    .line 117
    aput-object v16, v6, v9

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 121
    :goto_0
    throw v4

    .line 56
    :cond_0
    :try_start_1
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_VERSION:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v5

    .line 58
    .local v5, "version":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 59
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DOCCOUNT:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 60
    :cond_1
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DOCCOUNT:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 62
    .local v7, "docCount":I
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 63
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_USECOMPOUND:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 64
    :cond_2
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_USECOMPOUND:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 66
    .local v8, "isCompoundFile":Z
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 67
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_DIAG:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 68
    :cond_3
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_DIAG:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 69
    .local v19, "numDiag":I
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 71
    .local v10, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move/from16 v0, v19

    if-lt v15, v0, :cond_4

    .line 82
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 83
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_7

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 72
    :cond_4
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 73
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_KEY:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 74
    :cond_5
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_KEY:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v17

    .line 76
    .local v17, "key":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 77
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_VALUE:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 78
    :cond_6
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v24

    .line 79
    .local v24, "value":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-interface {v10, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 84
    .end local v17    # "key":Ljava/lang/String;
    .end local v24    # "value":Ljava/lang/String;
    :cond_7
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    .line 85
    .local v18, "numAtts":I
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 87
    .local v12, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v15, 0x0

    :goto_2
    move/from16 v0, v18

    if-lt v15, v0, :cond_8

    .line 98
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 99
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_FILES:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 88
    :cond_8
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 89
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_9

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 90
    :cond_9
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v17

    .line 92
    .restart local v17    # "key":Ljava/lang/String;
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 93
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_a

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_a

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 94
    :cond_a
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v24

    .line 95
    .restart local v24    # "value":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-interface {v12, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 100
    .end local v17    # "key":Ljava/lang/String;
    .end local v24    # "value":Ljava/lang/String;
    :cond_b
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_FILES:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 101
    .local v20, "numFiles":I
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 103
    .local v14, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v15, 0x0

    :goto_3
    move/from16 v0, v20

    if-lt v15, v0, :cond_c

    .line 110
    new-instance v3, Lorg/apache/lucene/index/SegmentInfo;

    .line 111
    const/4 v9, 0x0

    invoke-static {v12}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v11

    move-object/from16 v4, p1

    move-object/from16 v6, p2

    .line 110
    invoke-direct/range {v3 .. v11}, Lorg/apache/lucene/index/SegmentInfo;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;IZLorg/apache/lucene/codecs/Codec;Ljava/util/Map;Ljava/util/Map;)V

    .line 112
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v3, v14}, Lorg/apache/lucene/index/SegmentInfo;->setFiles(Ljava/util/Set;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    const/16 v23, 0x1

    .line 116
    if-nez v23, :cond_e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 117
    aput-object v16, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 114
    :goto_4
    return-object v3

    .line 104
    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_c
    :try_start_2
    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 105
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->$assertionsDisabled:Z

    if-nez v4, :cond_d

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_FILE:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 106
    :cond_d
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_FILE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v13

    .line 107
    .local v13, "fileName":Ljava/lang/String;
    invoke-interface {v14, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 119
    .end local v13    # "fileName":Ljava/lang/String;
    .restart local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    :cond_e
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto :goto_4

    .end local v3    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v5    # "version":Ljava/lang/String;
    .end local v7    # "docCount":I
    .end local v8    # "isCompoundFile":Z
    .end local v10    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v14    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v15    # "i":I
    .end local v18    # "numAtts":I
    .end local v19    # "numDiag":I
    .end local v20    # "numFiles":I
    :cond_f
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto/16 :goto_0
.end method
