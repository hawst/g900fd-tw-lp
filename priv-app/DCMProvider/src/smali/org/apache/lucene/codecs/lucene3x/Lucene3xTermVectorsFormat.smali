.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsFormat;
.super Lorg/apache/lucene/codecs/TermVectorsFormat;
.source "Lucene3xTermVectorsFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsFormat;-><init>()V

    return-void
.end method


# virtual methods
.method public vectorsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 7
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, "tvf"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 51
    .local v3, "fileName":Ljava/lang/String;
    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreIsCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 52
    invoke-static {p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, "cfx"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "cfxFileName":Ljava/lang/String;
    iget-object v4, p2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    new-instance v0, Lorg/apache/lucene/store/CompoundFileDirectory;

    iget-object v4, p2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    const/4 v5, 0x0

    invoke-direct {v0, v4, v1, p4, v5}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .line 56
    .local v0, "cfsDir":Lorg/apache/lucene/store/Directory;
    :try_start_0
    invoke-virtual {v0, v3}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 58
    .local v2, "exists":Z
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 67
    .end local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .end local v1    # "cfxFileName":Ljava/lang/String;
    :goto_0
    if-nez v2, :cond_2

    .line 70
    const/4 v4, 0x0

    .line 72
    :goto_1
    return-object v4

    .line 57
    .end local v2    # "exists":Z
    .restart local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    .restart local v1    # "cfxFileName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 58
    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 59
    throw v4

    .line 61
    .end local v0    # "cfsDir":Lorg/apache/lucene/store/Directory;
    :cond_0
    const/4 v2, 0x0

    .line 63
    .restart local v2    # "exists":Z
    goto :goto_0

    .line 64
    .end local v1    # "cfxFileName":Ljava/lang/String;
    .end local v2    # "exists":Z
    :cond_1
    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v2

    .restart local v2    # "exists":Z
    goto :goto_0

    .line 72
    :cond_2
    new-instance v4, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-direct {v4, p1, p2, p3, p4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V

    goto :goto_1
.end method

.method public vectorsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsWriter;
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
