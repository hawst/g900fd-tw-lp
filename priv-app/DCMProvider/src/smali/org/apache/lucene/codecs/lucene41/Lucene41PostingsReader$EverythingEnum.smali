.class final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "Lucene41PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "EverythingEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private accum:I

.field private doc:I

.field private docBufferUpto:I

.field private final docDeltaBuffer:[I

.field private docFreq:I

.field docIn:Lorg/apache/lucene/store/IndexInput;

.field private docTermStartFP:J

.field private docUpto:I

.field private final encoded:[B

.field private endOffset:I

.field private freq:I

.field private final freqBuffer:[I

.field final indexHasOffsets:Z

.field final indexHasPayloads:Z

.field private lastPosBlockFP:J

.field private lastStartOffset:I

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private needsOffsets:Z

.field private needsPayloads:Z

.field private nextSkipDoc:I

.field private final offsetLengthBuffer:[I

.field private final offsetStartDeltaBuffer:[I

.field final payIn:Lorg/apache/lucene/store/IndexInput;

.field private payPendingFP:J

.field private payTermStartFP:J

.field final payload:Lorg/apache/lucene/util/BytesRef;

.field private payloadByteUpto:I

.field private payloadBytes:[B

.field private payloadLength:I

.field private final payloadLengthBuffer:[I

.field private posBufferUpto:I

.field private final posDeltaBuffer:[I

.field final posIn:Lorg/apache/lucene/store/IndexInput;

.field private posPendingCount:I

.field private posPendingFP:J

.field private posTermStartFP:J

.field private position:I

.field private singletonDocID:I

.field private skipOffset:J

.field private skipped:Z

.field private skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

.field final startDocIn:Lorg/apache/lucene/store/IndexInput;

.field private startOffset:I

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

.field private totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1026
    const-class v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 4
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1110
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 1030
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docDeltaBuffer:[I

    .line 1031
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freqBuffer:[I

    .line 1032
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posDeltaBuffer:[I

    .line 1111
    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$1(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    .line 1112
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 1113
    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->posIn:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$3(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    .line 1114
    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->payIn:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$4(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    .line 1115
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    .line 1116
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    .line 1117
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    if-eqz v0, :cond_1

    .line 1118
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetStartDeltaBuffer:[I

    .line 1119
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetLengthBuffer:[I

    .line 1127
    :goto_1
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    .line 1128
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v0, :cond_2

    .line 1129
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    .line 1130
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    .line 1131
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 1137
    :goto_2
    return-void

    .line 1116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1121
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetStartDeltaBuffer:[I

    .line 1122
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetLengthBuffer:[I

    .line 1123
    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startOffset:I

    .line 1124
    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->endOffset:I

    goto :goto_1

    .line 1133
    :cond_2
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    .line 1134
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    .line 1135
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    goto :goto_2
.end method

.method private refillDocs()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1198
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    sub-int v0, v1, v2

    .line 1199
    .local v0, "left":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1201
    :cond_0
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    .line 1205
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docDeltaBuffer:[I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 1209
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freqBuffer:[I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 1219
    :goto_0
    iput v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    .line 1220
    return-void

    .line 1210
    :cond_1
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    if-ne v1, v4, :cond_2

    .line 1211
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->singletonDocID:I

    aput v2, v1, v5

    .line 1212
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freqBuffer:[I

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->totalTermFreq:J

    long-to-int v2, v2

    aput v2, v1, v5

    goto :goto_0

    .line 1217
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docDeltaBuffer:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freqBuffer:[I

    invoke-static {v1, v2, v3, v0, v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->readVIntBlock(Lorg/apache/lucene/store/IndexInput;[I[IIZ)V

    goto :goto_0
.end method

.method private refillPositions()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 1226
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    iget-wide v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastPosBlockFP:J

    cmp-long v7, v8, v10

    if-nez v7, :cond_8

    .line 1230
    iget-wide v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->totalTermFreq:J

    const-wide/16 v10, 0x80

    rem-long/2addr v8, v10

    long-to-int v1, v8

    .line 1231
    .local v1, "count":I
    const/4 v6, 0x0

    .line 1232
    .local v6, "payloadLength":I
    const/4 v5, 0x0

    .line 1233
    .local v5, "offsetLength":I
    iput v12, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1234
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v1, :cond_1

    .line 1272
    iput v12, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1316
    .end local v1    # "count":I
    .end local v3    # "i":I
    .end local v5    # "offsetLength":I
    .end local v6    # "payloadLength":I
    :cond_0
    :goto_1
    return-void

    .line 1235
    .restart local v1    # "count":I
    .restart local v3    # "i":I
    .restart local v5    # "offsetLength":I
    .restart local v6    # "payloadLength":I
    :cond_1
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 1236
    .local v0, "code":I
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v7, :cond_7

    .line 1237
    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_2

    .line 1238
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v6

    .line 1243
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    aput v6, v7, v3

    .line 1244
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posDeltaBuffer:[I

    ushr-int/lit8 v8, v0, 0x1

    aput v8, v7, v3

    .line 1245
    if-eqz v6, :cond_4

    .line 1246
    iget v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    add-int/2addr v7, v6

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    array-length v8, v8

    if-le v7, v8, :cond_3

    .line 1247
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    iget v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    add-int/2addr v8, v6

    invoke-static {v7, v8}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    .line 1250
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    iget v9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    invoke-virtual {v7, v8, v9, v6}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 1251
    iget v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    add-int/2addr v7, v6

    iput v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1257
    :cond_4
    :goto_2
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    if-eqz v7, :cond_6

    .line 1261
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 1262
    .local v2, "deltaCode":I
    and-int/lit8 v7, v2, 0x1

    if-eqz v7, :cond_5

    .line 1263
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v5

    .line 1265
    :cond_5
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetStartDeltaBuffer:[I

    ushr-int/lit8 v8, v2, 0x1

    aput v8, v7, v3

    .line 1266
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetLengthBuffer:[I

    aput v5, v7, v3

    .line 1234
    .end local v2    # "deltaCode":I
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1254
    :cond_7
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posDeltaBuffer:[I

    aput v0, v7, v3

    goto :goto_2

    .line 1277
    .end local v0    # "code":I
    .end local v1    # "count":I
    .end local v3    # "i":I
    .end local v5    # "offsetLength":I
    .end local v6    # "payloadLength":I
    :cond_8
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posDeltaBuffer:[I

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 1279
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v7, :cond_a

    .line 1283
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->needsPayloads:Z

    if-eqz v7, :cond_b

    .line 1284
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 1285
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 1289
    .local v4, "numBytes":I
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    array-length v7, v7

    if-le v4, v7, :cond_9

    .line 1290
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    invoke-static {v7, v4}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v7

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    .line 1292
    :cond_9
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    invoke-virtual {v7, v8, v12, v4}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 1299
    :goto_3
    iput v12, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1302
    .end local v4    # "numBytes":I
    :cond_a
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    if-eqz v7, :cond_0

    .line 1306
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->needsOffsets:Z

    if-eqz v7, :cond_c

    .line 1307
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetStartDeltaBuffer:[I

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 1308
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->encoded:[B

    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetLengthBuffer:[I

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    goto/16 :goto_1

    .line 1295
    :cond_b
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 1296
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 1297
    .restart local v4    # "numBytes":I
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    int-to-long v10, v4

    add-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_3

    .line 1311
    .end local v4    # "numBytes":I
    :cond_c
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 1312
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    goto/16 :goto_1
.end method

.method private skipPositions()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 1464
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    iget v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    sub-int v3, v4, v5

    .line 1469
    .local v3, "toSkip":I
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    rsub-int v1, v4, 0x80

    .line 1470
    .local v1, "leftInBlock":I
    if-ge v3, v1, :cond_3

    .line 1471
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    add-int v0, v4, v3

    .line 1472
    .local v0, "end":I
    :goto_0
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    if-lt v4, v0, :cond_1

    .line 1519
    .end local v0    # "end":I
    :cond_0
    iput v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->position:I

    .line 1520
    iput v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastStartOffset:I

    .line 1521
    return-void

    .line 1473
    .restart local v0    # "end":I
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v4, :cond_2

    .line 1474
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    iget v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    aget v5, v5, v6

    add-int/2addr v4, v5

    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1476
    :cond_2
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    goto :goto_0

    .line 1482
    .end local v0    # "end":I
    :cond_3
    sub-int/2addr v3, v1

    .line 1483
    :goto_1
    const/16 v4, 0x80

    if-ge v3, v4, :cond_5

    .line 1505
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->refillPositions()V

    .line 1506
    iput v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1507
    iput v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    .line 1508
    :goto_2
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    if-ge v4, v3, :cond_0

    .line 1509
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v4, :cond_4

    .line 1510
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    iget v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    aget v5, v5, v6

    add-int/2addr v4, v5

    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1512
    :cond_4
    iget v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    goto :goto_2

    .line 1487
    :cond_5
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastPosBlockFP:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1488
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 1490
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v4, :cond_7

    .line 1492
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 1495
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 1496
    .local v2, "numBytes":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    int-to-long v8, v2

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1499
    .end local v2    # "numBytes":I
    :cond_7
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    if-eqz v4, :cond_8

    .line 1500
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 1501
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    .line 1503
    :cond_8
    add-int/lit8 v3, v3, -0x80

    goto/16 :goto_1
.end method


# virtual methods
.method public advance(I)I
    .locals 12
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1365
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    const/16 v1, 0x80

    if-le v0, v1, :cond_5

    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->nextSkipDoc:I

    if-le p1, v0, :cond_5

    .line 1371
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    if-nez v0, :cond_0

    .line 1376
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 1377
    const/16 v2, 0xa

    .line 1378
    const/16 v3, 0x80

    .line 1379
    const/4 v4, 0x1

    .line 1380
    iget-boolean v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    .line 1381
    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;-><init>(Lorg/apache/lucene/store/IndexInput;IIZZZ)V

    .line 1376
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    .line 1384
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipped:Z

    if-nez v0, :cond_2

    .line 1385
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipOffset:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1391
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docTermStartFP:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipOffset:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docTermStartFP:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posTermStartFP:J

    iget-wide v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payTermStartFP:J

    iget v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    invoke-virtual/range {v1 .. v10}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->init(JJJJI)V

    .line 1392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipped:Z

    .line 1395
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->skipTo(I)I

    move-result v0

    add-int/lit8 v11, v0, 0x1

    .line 1397
    .local v11, "newDocUpto":I
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    if-le v11, v0, :cond_4

    .line 1402
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    rem-int/lit16 v0, v11, 0x80

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1403
    :cond_3
    iput v11, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    .line 1406
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    .line 1407
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    .line 1408
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getDocPointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1409
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getPosPointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingFP:J

    .line 1410
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getPayPointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payPendingFP:J

    .line 1411
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getPosBufferUpto()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    .line 1412
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastStartOffset:I

    .line 1413
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getPayloadByteUpto()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1415
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getNextSkipDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->nextSkipDoc:I

    .line 1417
    .end local v11    # "newDocUpto":I
    :cond_5
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    if-ne v0, v1, :cond_6

    .line 1418
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    .line 1454
    :goto_0
    return v0

    .line 1420
    :cond_6
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 1421
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->refillDocs()V

    .line 1429
    :cond_7
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    .line 1430
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freqBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    .line 1431
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    .line 1432
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    .line 1433
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    .line 1435
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    if-lt v0, p1, :cond_9

    .line 1443
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1447
    :cond_8
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->position:I

    .line 1448
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastStartOffset:I

    .line 1449
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    goto :goto_0

    .line 1438
    :cond_9
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    if-ne v0, v1, :cond_7

    .line 1439
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    goto :goto_0

    .line 1454
    :cond_a
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->nextDoc()I

    move-result v0

    goto :goto_0
.end method

.method public canReuse(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;)Z
    .locals 5
    .param p1, "docIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1140
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v2, :cond_1

    .line 1141
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    if-ne v3, v2, :cond_1

    .line 1142
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v3

    if-ne v2, v3, :cond_1

    .line 1140
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 1141
    goto :goto_0

    :cond_1
    move v0, v1

    .line 1140
    goto :goto_1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 1604
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 1194
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 1

    .prologue
    .line 1587
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->endOffset:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1189
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 1595
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLength:I

    if-nez v0, :cond_0

    .line 1596
    const/4 v0, 0x0

    .line 1598
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1327
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    if-ne v0, v1, :cond_1

    .line 1328
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    .line 1349
    :goto_0
    return v0

    .line 1330
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2

    .line 1331
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->refillDocs()V

    .line 1336
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    .line 1337
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freqBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    .line 1338
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    .line 1339
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    .line 1340
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    .line 1342
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1343
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    .line 1347
    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->position:I

    .line 1348
    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastStartOffset:I

    .line 1349
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x80

    const-wide/16 v4, -0x1

    .line 1528
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingFP:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 1532
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingFP:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1533
    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingFP:J

    .line 1535
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payPendingFP:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 1539
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payPendingFP:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1540
    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payPendingFP:J

    .line 1544
    :cond_0
    iput v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    .line 1547
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    if-le v0, v1, :cond_2

    .line 1548
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipPositions()V

    .line 1549
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->freq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    .line 1552
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    if-ne v0, v6, :cond_3

    .line 1553
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->refillPositions()V

    .line 1554
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    .line 1556
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->position:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->position:I

    .line 1558
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasPayloads:Z

    if-eqz v0, :cond_4

    .line 1559
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLengthBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLength:I

    .line 1560
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadBytes:[B

    iput-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1561
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 1562
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLength:I

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1563
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payloadByteUpto:I

    .line 1566
    :cond_4
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->indexHasOffsets:Z

    if-eqz v0, :cond_5

    .line 1567
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastStartOffset:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetStartDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startOffset:I

    .line 1568
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startOffset:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->offsetLengthBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->endOffset:I

    .line 1569
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startOffset:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastStartOffset:I

    .line 1572
    :cond_5
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posBufferUpto:I

    .line 1573
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    .line 1577
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->position:I

    return v0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;I)Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
    .locals 8
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x80

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1146
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 1150
    iget v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    .line 1151
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docTermStartFP:J

    .line 1152
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posTermStartFP:J

    .line 1153
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payTermStartFP:J

    .line 1154
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipOffset:J

    .line 1155
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->totalTermFreq:J

    .line 1156
    iget v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->singletonDocID:I

    .line 1157
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docFreq:I

    if-le v0, v1, :cond_1

    .line 1158
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 1160
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 1162
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docTermStartFP:J

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 1164
    :cond_1
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posTermStartFP:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingFP:J

    .line 1165
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payTermStartFP:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->payPendingFP:J

    .line 1166
    iput v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posPendingCount:I

    .line 1167
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    .line 1168
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posTermStartFP:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastPosBlockFP:J

    .line 1175
    :goto_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->needsOffsets:Z

    .line 1176
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_5

    :goto_2
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->needsPayloads:Z

    .line 1178
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->doc:I

    .line 1179
    iput v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->accum:I

    .line 1180
    iput v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docUpto:I

    .line 1181
    const/16 v0, 0x7f

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->nextSkipDoc:I

    .line 1182
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->docBufferUpto:I

    .line 1183
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->skipped:Z

    .line 1184
    return-object p0

    .line 1169
    :cond_2
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 1170
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastPosBlockFP:J

    goto :goto_0

    .line 1172
    :cond_3
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->posTermStartFP:J

    iget-wide v6, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->lastPosBlockFP:J

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1175
    goto :goto_1

    :cond_5
    move v1, v2

    .line 1176
    goto :goto_2
.end method

.method public startOffset()I
    .locals 1

    .prologue
    .line 1582
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->startOffset:I

    return v0
.end method
