.class public abstract Lorg/apache/lucene/codecs/FieldInfosFormat;
.super Ljava/lang/Object;
.source "FieldInfosFormat.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public abstract getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
