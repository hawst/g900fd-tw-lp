.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;
.super Lorg/apache/lucene/index/Fields;
.source "CompressingTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVFields"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final fieldFlags:[I

.field private final fieldLengths:[I

.field private final fieldNumOffs:[I

.field private final fieldNums:[I

.field private final lengths:[[I

.field private final numTerms:[I

.field private final payloadBytes:Lorg/apache/lucene/util/BytesRef;

.field private final payloadIndex:[[I

.field private final positionIndex:[[I

.field private final positions:[[I

.field private final prefixLengths:[[I

.field private final startOffsets:[[I

.field private final suffixBytes:Lorg/apache/lucene/util/BytesRef;

.field private final suffixLengths:[[I

.field private final termFreqs:[[I

.field final synthetic this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 590
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;[I[I[I[I[I[[I[[I[[I[[I[[I[[I[[ILorg/apache/lucene/util/BytesRef;[[ILorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p2, "fieldNums"    # [I
    .param p3, "fieldFlags"    # [I
    .param p4, "fieldNumOffs"    # [I
    .param p5, "numTerms"    # [I
    .param p6, "fieldLengths"    # [I
    .param p7, "prefixLengths"    # [[I
    .param p8, "suffixLengths"    # [[I
    .param p9, "termFreqs"    # [[I
    .param p10, "positionIndex"    # [[I
    .param p11, "positions"    # [[I
    .param p12, "startOffsets"    # [[I
    .param p13, "lengths"    # [[I
    .param p14, "payloadBytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p15, "payloadIndex"    # [[I
    .param p16, "suffixBytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 600
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    .line 596
    invoke-direct {p0}, Lorg/apache/lucene/index/Fields;-><init>()V

    .line 601
    iput-object p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNums:[I

    .line 602
    iput-object p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldFlags:[I

    .line 603
    iput-object p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I

    .line 604
    iput-object p5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->numTerms:[I

    .line 605
    iput-object p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldLengths:[I

    .line 606
    iput-object p7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->prefixLengths:[[I

    .line 607
    iput-object p8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->suffixLengths:[[I

    .line 608
    iput-object p9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->termFreqs:[[I

    .line 609
    iput-object p10, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->positionIndex:[[I

    .line 610
    iput-object p11, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->positions:[[I

    .line 611
    iput-object p12, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->startOffsets:[[I

    .line 612
    iput-object p13, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->lengths:[[I

    .line 613
    iput-object p14, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->payloadBytes:Lorg/apache/lucene/util/BytesRef;

    .line 614
    move-object/from16 v0, p15

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->payloadIndex:[[I

    .line 615
    move-object/from16 v0, p16

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->suffixBytes:Lorg/apache/lucene/util/BytesRef;

    .line 616
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)[I
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)[I
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNums:[I

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    return-object v0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 620
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I

    array-length v0, v0

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 25
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 643
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {v4}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v18

    .line 644
    .local v18, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    if-nez v18, :cond_0

    .line 645
    const/4 v4, 0x0

    .line 669
    :goto_0
    return-object v4

    .line 647
    :cond_0
    const/16 v22, -0x1

    .line 648
    .local v22, "idx":I
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I

    array-length v4, v4

    move/from16 v0, v21

    if-lt v0, v4, :cond_2

    .line 655
    :goto_2
    const/4 v4, -0x1

    move/from16 v0, v22

    if-eq v0, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->numTerms:[I

    aget v4, v4, v22

    if-nez v4, :cond_4

    .line 657
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 649
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNums:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I

    aget v5, v5, v21

    aget v4, v4, v5

    move-object/from16 v0, v18

    iget v5, v0, Lorg/apache/lucene/index/FieldInfo;->number:I

    if-ne v4, v5, :cond_3

    .line 650
    move/from16 v22, v21

    .line 651
    goto :goto_2

    .line 648
    :cond_3
    add-int/lit8 v21, v21, 0x1

    goto :goto_1

    .line 659
    :cond_4
    const/16 v20, 0x0

    .local v20, "fieldOff":I
    const/16 v19, -0x1

    .line 660
    .local v19, "fieldLen":I
    const/16 v21, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I

    array-length v4, v4

    move/from16 v0, v21

    if-lt v0, v4, :cond_5

    .line 668
    :goto_4
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->$assertionsDisabled:Z

    if-nez v4, :cond_7

    if-gez v19, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 661
    :cond_5
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_6

    .line 662
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldLengths:[I

    aget v4, v4, v21

    add-int v20, v20, v4

    .line 660
    add-int/lit8 v21, v21, 0x1

    goto :goto_3

    .line 664
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldLengths:[I

    aget v19, v4, v21

    .line 665
    goto :goto_4

    .line 669
    :cond_7
    new-instance v4, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->numTerms:[I

    aget v6, v6, v22

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldFlags:[I

    aget v7, v7, v22

    .line 670
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->prefixLengths:[[I

    aget-object v8, v8, v22

    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->suffixLengths:[[I

    aget-object v9, v9, v22

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->termFreqs:[[I

    aget-object v10, v10, v22

    .line 671
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->positionIndex:[[I

    aget-object v11, v11, v22

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->positions:[[I

    aget-object v12, v12, v22

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->startOffsets:[[I

    aget-object v13, v13, v22

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->lengths:[[I

    aget-object v14, v14, v22

    .line 672
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->payloadIndex:[[I

    aget-object v15, v15, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->payloadBytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v16, v0

    .line 673
    new-instance v17, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->suffixBytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->suffixBytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v24, v0

    add-int v24, v24, v20

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    .line 669
    invoke-direct/range {v4 .. v17}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;II[I[I[I[I[I[I[I[ILorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V

    goto/16 :goto_0
.end method
