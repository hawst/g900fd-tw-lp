.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesFormat;
.super Lorg/apache/lucene/codecs/DocValuesFormat;
.source "SimpleTextDocValuesFormat.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    const-string v0, "SimpleText"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/DocValuesFormat;-><init>(Ljava/lang/String;)V

    .line 119
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;

    const-string v1, "dat"

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;)V

    return-object v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    const-string v1, "dat"

    invoke-direct {v0, p1, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;)V

    return-object v0
.end method
