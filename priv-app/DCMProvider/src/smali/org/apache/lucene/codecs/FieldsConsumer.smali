.class public abstract Lorg/apache/lucene/codecs/FieldsConsumer;
.super Ljava/lang/Object;
.source "FieldsConsumer.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/codecs/FieldsConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/FieldsConsumer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method


# virtual methods
.method public abstract addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/Fields;)V
    .locals 7
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "fields"    # Lorg/apache/lucene/index/Fields;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p2}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 75
    return-void

    .line 66
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 67
    .local v0, "field":Ljava/lang/String;
    iget-object v5, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    .line 68
    .local v1, "info":Lorg/apache/lucene/index/FieldInfo;
    sget-boolean v5, Lorg/apache/lucene/codecs/FieldsConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    if-nez v1, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FieldInfo for field is null: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 69
    :cond_2
    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v2

    .line 70
    .local v2, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v2, :cond_0

    .line 71
    invoke-virtual {p0, v1}, Lorg/apache/lucene/codecs/FieldsConsumer;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;

    move-result-object v3

    .line 72
    .local v3, "termsConsumer":Lorg/apache/lucene/codecs/TermsConsumer;
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v6

    invoke-virtual {v3, p1, v5, v6}, Lorg/apache/lucene/codecs/TermsConsumer;->merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/TermsEnum;)V

    goto :goto_0
.end method
