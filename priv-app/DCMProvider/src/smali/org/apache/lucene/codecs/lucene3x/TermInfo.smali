.class Lorg/apache/lucene/codecs/lucene3x/TermInfo;
.super Ljava/lang/Object;
.source "TermInfo.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public docFreq:I

.field public freqPointer:J

.field public proxPointer:J

.field public skipOffset:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 30
    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 31
    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 34
    return-void
.end method

.method public constructor <init>(IJJ)V
    .locals 4
    .param p1, "df"    # I
    .param p2, "fp"    # J
    .param p4, "pp"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 30
    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 31
    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 37
    iput p1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 38
    iput-wide p2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 39
    iput-wide p4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 40
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V
    .locals 4
    .param p1, "ti"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 30
    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 31
    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 43
    iget v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 44
    iget-wide v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 45
    iget-wide v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 46
    iget v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    .line 47
    return-void
.end method


# virtual methods
.method public final set(IJJI)V
    .locals 0
    .param p1, "docFreq"    # I
    .param p2, "freqPointer"    # J
    .param p4, "proxPointer"    # J
    .param p6, "skipOffset"    # I

    .prologue
    .line 51
    iput p1, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 52
    iput-wide p2, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 53
    iput-wide p4, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 54
    iput p6, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    .line 55
    return-void
.end method

.method public final set(Lorg/apache/lucene/codecs/lucene3x/TermInfo;)V
    .locals 2
    .param p1, "ti"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .prologue
    .line 58
    iget v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    .line 59
    iget-wide v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    .line 60
    iget-wide v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    .line 61
    iget v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    .line 62
    return-void
.end method
