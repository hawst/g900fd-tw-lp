.class Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;
.super Ljava/lang/Object;
.source "SimpleTextUtil.java"


# static fields
.field public static final ESCAPE:B = 0x5ct

.field public static final NEWLINE:B = 0xat


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p0, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p1, "scratch"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    const/4 v1, 0x0

    .line 53
    .local v1, "upto":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 54
    .local v0, "b":B
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v3, v3

    if-ne v3, v1, :cond_0

    .line 55
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 57
    :cond_0
    const/16 v3, 0x5c

    if-ne v0, v3, :cond_1

    .line 58
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "upto":I
    .local v2, "upto":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v4

    aput-byte v4, v3, v1

    move v1, v2

    .line 59
    .end local v2    # "upto":I
    .restart local v1    # "upto":I
    goto :goto_0

    .line 60
    :cond_1
    const/16 v3, 0xa

    if-ne v0, v3, :cond_2

    .line 67
    const/4 v3, 0x0

    iput v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 68
    iput v1, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 69
    return-void

    .line 63
    :cond_2
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "upto":I
    .restart local v2    # "upto":I
    aput-byte v0, v3, v1

    move v1, v2

    .line 52
    .end local v2    # "upto":I
    .restart local v1    # "upto":I
    goto :goto_0
.end method

.method public static write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "scratch"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v0, v1, p2}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V

    .line 33
    invoke-static {p0, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 34
    return-void
.end method

.method public static write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x5c

    .line 37
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v1, v2, :cond_0

    .line 44
    return-void

    .line 38
    :cond_0
    iget-object v2, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v3, v1

    aget-byte v0, v2, v3

    .line 39
    .local v0, "bx":B
    const/16 v2, 0xa

    if-eq v0, v2, :cond_1

    if-ne v0, v4, :cond_2

    .line 40
    :cond_1
    invoke-virtual {p0, v4}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 42
    :cond_2
    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static writeNewline(Lorg/apache/lucene/store/DataOutput;)V
    .locals 1
    .param p0, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 48
    return-void
.end method
