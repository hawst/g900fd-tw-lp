.class public Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesFormat;
.super Lorg/apache/lucene/codecs/DocValuesFormat;
.source "Lucene40DocValuesFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final BYTES_FIXED_DEREF_CODEC_NAME_DAT:Ljava/lang/String; = "FixedDerefBytesDat"

.field static final BYTES_FIXED_DEREF_CODEC_NAME_IDX:Ljava/lang/String; = "FixedDerefBytesIdx"

.field static final BYTES_FIXED_DEREF_VERSION_CURRENT:I = 0x0

.field static final BYTES_FIXED_DEREF_VERSION_START:I = 0x0

.field static final BYTES_FIXED_SORTED_CODEC_NAME_DAT:Ljava/lang/String; = "FixedSortedBytesDat"

.field static final BYTES_FIXED_SORTED_CODEC_NAME_IDX:Ljava/lang/String; = "FixedSortedBytesIdx"

.field static final BYTES_FIXED_SORTED_VERSION_CURRENT:I = 0x0

.field static final BYTES_FIXED_SORTED_VERSION_START:I = 0x0

.field static final BYTES_FIXED_STRAIGHT_CODEC_NAME:Ljava/lang/String; = "FixedStraightBytes"

.field static final BYTES_FIXED_STRAIGHT_VERSION_CURRENT:I = 0x0

.field static final BYTES_FIXED_STRAIGHT_VERSION_START:I = 0x0

.field static final BYTES_VAR_DEREF_CODEC_NAME_DAT:Ljava/lang/String; = "VarDerefBytesDat"

.field static final BYTES_VAR_DEREF_CODEC_NAME_IDX:Ljava/lang/String; = "VarDerefBytesIdx"

.field static final BYTES_VAR_DEREF_VERSION_CURRENT:I = 0x0

.field static final BYTES_VAR_DEREF_VERSION_START:I = 0x0

.field static final BYTES_VAR_SORTED_CODEC_NAME_DAT:Ljava/lang/String; = "VarDerefBytesDat"

.field static final BYTES_VAR_SORTED_CODEC_NAME_IDX:Ljava/lang/String; = "VarDerefBytesIdx"

.field static final BYTES_VAR_SORTED_VERSION_CURRENT:I = 0x0

.field static final BYTES_VAR_SORTED_VERSION_START:I = 0x0

.field static final BYTES_VAR_STRAIGHT_CODEC_NAME_DAT:Ljava/lang/String; = "VarStraightBytesDat"

.field static final BYTES_VAR_STRAIGHT_CODEC_NAME_IDX:Ljava/lang/String; = "VarStraightBytesIdx"

.field static final BYTES_VAR_STRAIGHT_VERSION_CURRENT:I = 0x0

.field static final BYTES_VAR_STRAIGHT_VERSION_START:I = 0x0

.field static final FLOATS_CODEC_NAME:Ljava/lang/String; = "Floats"

.field static final FLOATS_VERSION_CURRENT:I = 0x0

.field static final FLOATS_VERSION_START:I = 0x0

.field static final INTS_CODEC_NAME:Ljava/lang/String; = "Ints"

.field static final INTS_VERSION_CURRENT:I = 0x0

.field static final INTS_VERSION_START:I = 0x0

.field static final VAR_INTS_CODEC_NAME:Ljava/lang/String; = "PackedInts"

.field static final VAR_INTS_FIXED_64:B = 0x1t

.field static final VAR_INTS_PACKED:B

.field static final VAR_INTS_VERSION_CURRENT:I

.field static final VAR_INTS_VERSION_START:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 130
    const-string v0, "Lucene40"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/DocValuesFormat;-><init>(Ljava/lang/String;)V

    .line 131
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 141
    const-string v2, "dv"

    .line 142
    const-string v3, "cfs"

    .line 140
    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    sget-object v2, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;->LEGACY_DV_TYPE_KEY:Ljava/lang/String;

    invoke-direct {v1, p1, v0, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
