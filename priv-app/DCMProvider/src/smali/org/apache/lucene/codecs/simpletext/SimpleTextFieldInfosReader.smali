.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;
.super Lorg/apache/lucene/codecs/FieldInfosReader;
.source "SimpleTextFieldInfosReader.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosReader;-><init>()V

    return-void
.end method

.method private readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 5
    .param p1, "offset"    # I
    .param p2, "scratch"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 152
    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v2, p1

    iget v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, p1

    sget-object v4, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method


# virtual methods
.method public docValuesType(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 1
    .param p1, "dvType"    # Ljava/lang/String;

    .prologue
    .line 144
    const-string v0, "false"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 147
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v0

    goto :goto_0
.end method

.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/FieldInfos;
    .locals 34
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "iocontext"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    const-string v4, ""

    const-string v14, "inf"

    move-object/from16 v0, p2

    invoke-static {v0, v4, v14}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 52
    .local v18, "fileName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v21

    .line 53
    .local v21, "input":Lorg/apache/lucene/store/IndexInput;
    new-instance v26, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v26 .. v26}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 55
    .local v26, "scratch":Lorg/apache/lucene/util/BytesRef;
    const/16 v28, 0x0

    .line 58
    .local v28, "success":Z
    :try_start_0
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 59
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :catchall_0
    move-exception v4

    .line 135
    if-eqz v28, :cond_13

    .line 136
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 140
    :goto_0
    throw v4

    .line 60
    :cond_0
    :try_start_1
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMFIELDS:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v27

    .line 61
    .local v27, "size":I
    move/from16 v0, v27

    new-array v0, v0, [Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v20, v0

    .line 63
    .local v20, "infos":[Lorg/apache/lucene/index/FieldInfo;
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_1
    move/from16 v0, v19

    move/from16 v1, v27

    if-lt v0, v1, :cond_1

    .line 127
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v30

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v32

    cmp-long v4, v30, v32

    if-eqz v4, :cond_11

    .line 128
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v30, "did not read all bytes from file \""

    move-object/from16 v0, v30

    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v30, "\": read "

    move-object/from16 v0, v30

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v30

    move-wide/from16 v0, v30

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v30, " vs size "

    move-object/from16 v0, v30

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v30

    move-wide/from16 v0, v30

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v30, " (resource: "

    move-object/from16 v0, v30

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v30, ")"

    move-object/from16 v0, v30

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v4, v14}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 64
    :cond_1
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 65
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 66
    :cond_2
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v5

    .line 68
    .local v5, "name":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 69
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMBER:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 70
    :cond_3
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUMBER:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 72
    .local v7, "fieldNumber":I
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 73
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ISINDEXED:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 74
    :cond_4
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ISINDEXED:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 77
    .local v6, "isIndexed":Z
    if-eqz v6, :cond_6

    .line 78
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 79
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->INDEXOPTIONS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 80
    :cond_5
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->INDEXOPTIONS:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v11

    .line 85
    .local v11, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :goto_2
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 86
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_7

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->STORETV:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 82
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_6
    const/4 v11, 0x0

    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_2

    .line 87
    :cond_7
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->STORETV:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 89
    .local v8, "storeTermVector":Z
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 90
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_8

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->PAYLOADS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_8

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 91
    :cond_8
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->PAYLOADS:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v10

    .line 93
    .local v10, "storePayloads":Z
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 94
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_9

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 95
    :cond_9
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v9, 0x0

    .line 97
    .local v9, "omitNorms":Z
    :goto_3
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 98
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS_TYPE:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 95
    .end local v9    # "omitNorms":Z
    :cond_a
    const/4 v9, 0x1

    goto :goto_3

    .line 99
    .restart local v9    # "omitNorms":Z
    :cond_b
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NORMS_TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v24

    .line 100
    .local v24, "nrmType":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->docValuesType(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v13

    .line 102
    .local v13, "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 103
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->DOCVALUES:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_c

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 104
    :cond_c
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->DOCVALUES:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v16

    .line 105
    .local v16, "dvType":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->docValuesType(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v12

    .line 107
    .local v12, "docValuesType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 108
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_d

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 109
    :cond_d
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v25

    .line 110
    .local v25, "numAtts":I
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 112
    .local v15, "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_4
    move/from16 v0, v22

    move/from16 v1, v25

    if-lt v0, v1, :cond_e

    .line 123
    new-instance v4, Lorg/apache/lucene/index/FieldInfo;

    .line 124
    invoke-static {v15}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v14

    invoke-direct/range {v4 .. v14}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 123
    aput-object v4, v20, v19

    .line 63
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_1

    .line 113
    :cond_e
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 114
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_f

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_f

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 115
    :cond_f
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v23

    .line 117
    .local v23, "key":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 118
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->$assertionsDisabled:Z

    if-nez v4, :cond_10

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v26

    invoke-static {v0, v4}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_10

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 119
    :cond_10
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosWriter;->ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosReader;->readString(ILorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v29

    .line 120
    .local v29, "value":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-interface {v15, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    add-int/lit8 v22, v22, 0x1

    goto :goto_4

    .line 131
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "isIndexed":Z
    .end local v7    # "fieldNumber":I
    .end local v8    # "storeTermVector":Z
    .end local v9    # "omitNorms":Z
    .end local v10    # "storePayloads":Z
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v12    # "docValuesType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .end local v13    # "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .end local v15    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "dvType":Ljava/lang/String;
    .end local v22    # "j":I
    .end local v23    # "key":Ljava/lang/String;
    .end local v24    # "nrmType":Ljava/lang/String;
    .end local v25    # "numAtts":I
    .end local v29    # "value":Ljava/lang/String;
    :cond_11
    new-instance v17, Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/FieldInfos;-><init>([Lorg/apache/lucene/index/FieldInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    .local v17, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    const/16 v28, 0x1

    .line 135
    if-eqz v28, :cond_12

    .line 136
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 133
    :goto_5
    return-object v17

    .line 137
    :cond_12
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v14, 0x0

    .line 138
    aput-object v21, v4, v14

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_5

    .line 137
    .end local v17    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v19    # "i":I
    .end local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .end local v27    # "size":I
    :cond_13
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/io/Closeable;

    const/16 v30, 0x0

    .line 138
    aput-object v21, v14, v30

    invoke-static {v14}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_0
.end method
