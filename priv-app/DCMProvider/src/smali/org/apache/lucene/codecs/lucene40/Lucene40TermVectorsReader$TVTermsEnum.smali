.class Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "Lucene40TermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVTermsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private endOffsets:[I

.field private freq:I

.field private lastPayloadLength:I

.field private lastTerm:Lorg/apache/lucene/util/BytesRef;

.field private nextTerm:I

.field private numTerms:I

.field private final origTVF:Lorg/apache/lucene/store/IndexInput;

.field private payloadData:[B

.field private payloadOffsets:[I

.field private positions:[I

.field private startOffsets:[I

.field private storeOffsets:Z

.field private storePayloads:Z

.field private storePositions:Z

.field private term:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

.field private final tvf:Lorg/apache/lucene/store/IndexInput;

.field private tvfFP:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 386
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)V
    .locals 1

    .prologue
    .line 409
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 392
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    .line 393
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 410
    # getter for: Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->origTVF:Lorg/apache/lucene/store/IndexInput;

    .line 411
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->origTVF:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 412
    return-void
.end method


# virtual methods
.method public canReuse(Lorg/apache/lucene/store/IndexInput;)Z
    .locals 1
    .param p1, "tvf"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 415
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->origTVF:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x1

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 547
    if-eqz p2, :cond_0

    instance-of v1, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 548
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;

    .line 552
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;->reset(Lorg/apache/lucene/util/Bits;I)V

    .line 553
    return-object v0

    .line 550
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;)V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;
    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 7
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 559
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storePositions:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storeOffsets:Z

    if-nez v2, :cond_0

    move-object v0, v1

    .line 570
    :goto_0
    return-object v0

    .line 564
    :cond_0
    if-eqz p2, :cond_1

    instance-of v2, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;

    if-eqz v2, :cond_1

    move-object v0, p2

    .line 565
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;

    .line 569
    .local v0, "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->positions:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->startOffsets:[I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->endOffsets:[I

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadOffsets:[I

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadData:[B

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;->reset(Lorg/apache/lucene/util/Bits;[I[I[I[I[B)V

    goto :goto_0

    .line 567
    .end local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;)V

    .restart local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;
    goto :goto_1
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 575
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 467
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->nextTerm:I

    iget v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->numTerms:I

    if-lt v7, v8, :cond_0

    .line 468
    const/4 v7, 0x0

    .line 521
    :goto_0
    return-object v7

    .line 470
    :cond_0
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 471
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v5

    .line 472
    .local v5, "start":I
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 473
    .local v1, "deltaLen":I
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    add-int v8, v5, v1

    iput v8, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 474
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 475
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, v8, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-virtual {v7, v8, v5, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 476
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    .line 478
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storePayloads:Z

    if-eqz v7, :cond_6

    .line 479
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->positions:[I

    .line 480
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadOffsets:[I

    .line 481
    const/4 v6, 0x0

    .line 482
    .local v6, "totalPayloadLength":I
    const/4 v3, 0x0

    .line 483
    .local v3, "pos":I
    const/4 v4, 0x0

    .local v4, "posUpto":I
    :goto_1
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    if-lt v4, v7, :cond_3

    .line 495
    new-array v7, v6, [B

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadData:[B

    .line 496
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadData:[B

    const/4 v9, 0x0

    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadData:[B

    array-length v10, v10

    invoke-virtual {v7, v8, v9, v10}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 509
    .end local v3    # "pos":I
    .end local v4    # "posUpto":I
    .end local v6    # "totalPayloadLength":I
    :cond_1
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storeOffsets:Z

    if-eqz v7, :cond_2

    .line 510
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->startOffsets:[I

    .line 511
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->endOffsets:[I

    .line 512
    const/4 v2, 0x0

    .line 513
    .local v2, "offset":I
    const/4 v4, 0x0

    .restart local v4    # "posUpto":I
    :goto_2
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    if-lt v4, v7, :cond_7

    .line 519
    .end local v2    # "offset":I
    .end local v4    # "posUpto":I
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 520
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->nextTerm:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->nextTerm:I

    .line 521
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 484
    .restart local v3    # "pos":I
    .restart local v4    # "posUpto":I
    .restart local v6    # "totalPayloadLength":I
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 485
    .local v0, "code":I
    ushr-int/lit8 v7, v0, 0x1

    add-int/2addr v3, v7

    .line 486
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->positions:[I

    aput v3, v7, v4

    .line 487
    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_4

    .line 489
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    iput v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->lastPayloadLength:I

    .line 491
    :cond_4
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadOffsets:[I

    aput v6, v7, v4

    .line 492
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->lastPayloadLength:I

    add-int/2addr v6, v7

    .line 493
    sget-boolean v7, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->$assertionsDisabled:Z

    if-nez v7, :cond_5

    if-gez v6, :cond_5

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 483
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 497
    .end local v0    # "code":I
    .end local v3    # "pos":I
    .end local v4    # "posUpto":I
    .end local v6    # "totalPayloadLength":I
    :cond_6
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storePositions:Z

    if-eqz v7, :cond_1

    .line 501
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    new-array v7, v7, [I

    iput-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->positions:[I

    .line 502
    const/4 v3, 0x0

    .line 503
    .restart local v3    # "pos":I
    const/4 v4, 0x0

    .restart local v4    # "posUpto":I
    :goto_3
    iget v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    if-ge v4, v7, :cond_1

    .line 504
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    add-int/2addr v3, v7

    .line 505
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->positions:[I

    aput v3, v7, v4

    .line 503
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 514
    .end local v3    # "pos":I
    .restart local v2    # "offset":I
    :cond_7
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->startOffsets:[I

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v8

    add-int/2addr v8, v2

    aput v8, v7, v4

    .line 515
    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->endOffsets:[I

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->startOffsets:[I

    aget v8, v8, v4

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v9}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v9

    add-int v2, v8, v9

    aput v2, v7, v4

    .line 513
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public ord()J
    .locals 1

    .prologue
    .line 531
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public reset(IJZZZ)V
    .locals 2
    .param p1, "numTerms"    # I
    .param p2, "tvfFPStart"    # J
    .param p4, "storePositions"    # Z
    .param p5, "storeOffsets"    # Z
    .param p6, "storePayloads"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 419
    iput p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->numTerms:I

    .line 420
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storePositions:Z

    .line 421
    iput-boolean p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storeOffsets:Z

    .line 422
    iput-boolean p6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->storePayloads:Z

    .line 423
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->nextTerm:I

    .line 424
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 425
    iput-wide p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvfFP:J

    .line 426
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->positions:[I

    .line 427
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->startOffsets:[I

    .line 428
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->endOffsets:[I

    .line 429
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadOffsets:[I

    .line 430
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->payloadData:[B

    .line 431
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->lastPayloadLength:I

    .line 432
    return-void
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 4
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->nextTerm:I

    if-eqz v1, :cond_0

    .line 439
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 440
    .local v0, "cmp":I
    if-gez v0, :cond_1

    .line 441
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->nextTerm:I

    .line 442
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->tvfFP:J

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 448
    .end local v0    # "cmp":I
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    if-nez v1, :cond_2

    .line 457
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    :goto_0
    return-object v1

    .line 443
    .restart local v0    # "cmp":I
    :cond_1
    if-nez v0, :cond_0

    .line 444
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 449
    .end local v0    # "cmp":I
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 450
    .restart local v0    # "cmp":I
    if-gez v0, :cond_3

    .line 451
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 452
    :cond_3
    if-nez v0, :cond_0

    .line 453
    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J

    .prologue
    .line 462
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 541
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->freq:I

    int-to-long v0, v0

    return-wide v0
.end method
