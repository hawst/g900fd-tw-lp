.class public Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.super Lorg/apache/lucene/codecs/PostingsReaderBase;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BUFFERSIZE:I = 0x40

.field static final FRQ_CODEC:Ljava/lang/String; = "Lucene40PostingsWriterFrq"

.field static final PRX_CODEC:Ljava/lang/String; = "Lucene40PostingsWriterPrx"

.field static final TERMS_CODEC:Ljava/lang/String; = "Lucene40PostingsWriterTerms"

.field static final VERSION_CURRENT:I = 0x1

.field static final VERSION_LONG_SKIP:I = 0x1

.field static final VERSION_START:I


# instance fields
.field private final freqIn:Lorg/apache/lucene/store/IndexInput;

.field maxSkipLevels:I

.field private final proxIn:Lorg/apache/lucene/store/IndexInput;

.field skipInterval:I

.field skipMinimum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->$assertionsDisabled:Z

    .line 305
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;)V
    .locals 9
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "ioContext"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "segmentSuffix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/codecs/PostingsReaderBase;-><init>()V

    .line 75
    const/4 v2, 0x0

    .line 76
    .local v2, "success":Z
    const/4 v0, 0x0

    .line 77
    .local v0, "freqIn":Lorg/apache/lucene/store/IndexInput;
    const/4 v1, 0x0

    .line 79
    .local v1, "proxIn":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v4, "frq"

    invoke-static {v3, p5, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 81
    const-string v3, "Lucene40PostingsWriterFrq"

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 90
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 91
    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v4, "prx"

    invoke-static {v3, p5, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 93
    const-string v3, "Lucene40PostingsWriterPrx"

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v1, v3, v4, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 97
    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 98
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    const/4 v2, 0x1

    .line 101
    if-nez v2, :cond_0

    new-array v3, v8, [Ljava/io/Closeable;

    .line 102
    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 105
    :cond_0
    return-void

    .line 95
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v3

    .line 101
    if-nez v2, :cond_2

    new-array v4, v8, [Ljava/io/Closeable;

    .line 102
    aput-object v0, v4, v6

    aput-object v1, v4, v7

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 104
    :cond_2
    throw v3
.end method

.method private canReuse(Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/Bits;)Z
    .locals 4
    .param p1, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    const/4 v1, 0x0

    .line 242
    if-eqz p1, :cond_0

    instance-of v2, p1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 243
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;

    .line 247
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;
    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->startFreqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    if-ne v2, v3, :cond_0

    .line 249
    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-ne p2, v2, :cond_0

    const/4 v1, 0x1

    .line 252
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;
    :cond_0
    return v1
.end method

.method private newDocsEnum(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)Lorg/apache/lucene/index/DocsEnum;
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "termState"    # Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 256
    if-nez p1, :cond_0

    .line 257
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;)V

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$AllDocsSegmentDocsEnum;->reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    .line 259
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {v0, p0, v1, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/Bits;)V

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 172
    :cond_1
    return-void

    .line 167
    :catchall_0
    move-exception v0

    .line 168
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    if-eqz v1, :cond_2

    .line 169
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 171
    :cond_2
    throw v0
.end method

.method public docs(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p5, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 234
    invoke-direct {p0, p4, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->canReuse(Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/Bits;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    check-cast p4, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;

    .end local p4    # "reuse":Lorg/apache/lucene/index/DocsEnum;
    check-cast p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-virtual {p4, p1, p2}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    .line 238
    :goto_0
    return-object v0

    .restart local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    .restart local p4    # "reuse":Lorg/apache/lucene/index/DocsEnum;
    :cond_0
    check-cast p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-direct {p0, p3, p1, p2}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->newDocsEnum(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 4
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p5, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 268
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_3

    const/4 v1, 0x1

    .line 274
    .local v1, "hasOffsets":Z
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_5

    .line 276
    :cond_0
    if-eqz p4, :cond_1

    instance-of v2, p4, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;

    if-nez v2, :cond_4

    .line 277
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {v0, p0, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    .line 287
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    :cond_2
    :goto_1
    check-cast p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;

    move-result-object v2

    .line 301
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    :goto_2
    return-object v2

    .line 268
    .end local v1    # "hasOffsets":Z
    .restart local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .restart local v1    # "hasOffsets":Z
    :cond_4
    move-object v0, p4

    .line 279
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;

    .line 280
    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;->startFreqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    if-eq v2, v3, :cond_2

    .line 284
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;

    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {v0, p0, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    goto :goto_1

    .line 290
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentFullPositionsEnum;
    :cond_5
    if-eqz p4, :cond_6

    instance-of v2, p4, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;

    if-nez v2, :cond_8

    .line 291
    :cond_6
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {v0, p0, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    .line 301
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
    :cond_7
    :goto_3
    check-cast p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;

    move-result-object v2

    goto :goto_2

    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
    .restart local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    :cond_8
    move-object v0, p4

    .line 293
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;

    .line 294
    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
    iget-object v2, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->startFreqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    if-eq v2, v3, :cond_7

    .line 298
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;

    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-direct {v0, p0, v2, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
    goto :goto_3
.end method

.method public init(Lorg/apache/lucene/store/IndexInput;)V
    .locals 3
    .param p1, "termsIn"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    const-string v0, "Lucene40PostingsWriterTerms"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 113
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    .line 114
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->maxSkipLevels:I

    .line 115
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipMinimum:I

    .line 116
    return-void
.end method

.method public newTermState()Lorg/apache/lucene/codecs/BlockTermState;
    .locals 2

    .prologue
    .line 158
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)V

    return-object v0
.end method

.method public nextTerm(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V
    .locals 6
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "_termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    move-object v1, p2

    check-cast v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .line 199
    .local v1, "termState":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    iget v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->termBlockOrd:I

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 201
    .local v0, "isFirstTerm":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 202
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v2

    iput-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    .line 212
    :goto_1
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 199
    .end local v0    # "isFirstTerm":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 204
    .restart local v0    # "isFirstTerm":Z
    :cond_1
    iget-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iget-object v4, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    goto :goto_1

    .line 214
    :cond_2
    iget v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->docFreq:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipMinimum:I

    if-lt v2, v3, :cond_3

    .line 215
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v2

    iput-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    .line 217
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iget-wide v4, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    add-long/2addr v2, v4

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 222
    :cond_3
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_4

    .line 223
    if-eqz v0, :cond_5

    .line 224
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v2

    iput-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    .line 230
    :cond_4
    :goto_2
    return-void

    .line 226
    :cond_5
    iget-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    iget-object v4, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    goto :goto_2
.end method

.method public readTermsBlock(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V
    .locals 5
    .param p1, "termsIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "_termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 178
    move-object v1, p3

    check-cast v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;

    .line 180
    .local v1, "termState":Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 183
    .local v0, "len":I
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytes:[B

    if-nez v2, :cond_1

    .line 184
    invoke-static {v0, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytes:[B

    .line 185
    new-instance v2, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 190
    :cond_0
    :goto_0
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytes:[B

    invoke-virtual {p1, v2, v4, v0}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 191
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v3, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytes:[B

    invoke-virtual {v2, v3, v4, v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 192
    return-void

    .line 186
    :cond_1
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytes:[B

    array-length v2, v2

    if-ge v2, v0, :cond_0

    .line 187
    invoke-static {v0, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->bytes:[B

    goto :goto_0
.end method
