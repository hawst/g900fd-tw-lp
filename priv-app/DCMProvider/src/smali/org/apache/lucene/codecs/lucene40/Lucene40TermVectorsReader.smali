.class public Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
.super Lorg/apache/lucene/codecs/TermVectorsReader;
.source "Lucene40TermVectorsReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVDocsEnum;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVFields;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;,
        Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final CODEC_NAME_DOCS:Ljava/lang/String; = "Lucene40TermVectorsDocs"

.field static final CODEC_NAME_FIELDS:Ljava/lang/String; = "Lucene40TermVectorsFields"

.field static final CODEC_NAME_INDEX:Ljava/lang/String; = "Lucene40TermVectorsIndex"

.field static final HEADER_LENGTH_DOCS:J

.field static final HEADER_LENGTH_FIELDS:J

.field static final HEADER_LENGTH_INDEX:J

.field static final STORE_OFFSET_WITH_TERMVECTOR:B = 0x2t

.field static final STORE_PAYLOAD_WITH_TERMVECTOR:B = 0x4t

.field static final STORE_POSITIONS_WITH_TERMVECTOR:B = 0x1t

.field static final VECTORS_DOCUMENTS_EXTENSION:Ljava/lang/String; = "tvd"

.field static final VECTORS_FIELDS_EXTENSION:Ljava/lang/String; = "tvf"

.field static final VECTORS_INDEX_EXTENSION:Ljava/lang/String; = "tvx"

.field static final VERSION_CURRENT:I = 0x1

.field static final VERSION_NO_PAYLOADS:I = 0x0

.field static final VERSION_PAYLOADS:I = 0x1

.field static final VERSION_START:I


# instance fields
.field private fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private numTotalDocs:I

.field private size:I

.field private tvd:Lorg/apache/lucene/store/IndexInput;

.field private tvf:Lorg/apache/lucene/store/IndexInput;

.field private tvx:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    .line 80
    const-string v0, "Lucene40TermVectorsFields"

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_FIELDS:J

    .line 81
    const-string v0, "Lucene40TermVectorsDocs"

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_DOCS:J

    .line 82
    const-string v0, "Lucene40TermVectorsIndex"

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_INDEX:J

    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;II)V
    .locals 0
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "tvx"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "tvd"    # Lorg/apache/lucene/store/IndexInput;
    .param p4, "tvf"    # Lorg/apache/lucene/store/IndexInput;
    .param p5, "size"    # I
    .param p6, "numTotalDocs"    # I

    .prologue
    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 95
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 96
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    .line 97
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    .line 98
    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 99
    iput p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->size:I

    .line 100
    iput p6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    .line 101
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 14
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 106
    move-object/from16 v0, p2

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 107
    .local v4, "segment":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v5

    .line 109
    .local v5, "size":I
    const/4 v6, 0x0

    .line 112
    .local v6, "success":Z
    :try_start_0
    const-string v10, ""

    const-string v11, "tvx"

    invoke-static {v4, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 113
    .local v3, "idxName":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {p1, v3, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    .line 114
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    const-string v11, "Lucene40TermVectorsIndex"

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v10, v11, v12, v13}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v9

    .line 116
    .local v9, "tvxVersion":I
    const-string v10, ""

    const-string v11, "tvd"

    invoke-static {v4, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "fn":Ljava/lang/String;
    move-object/from16 v0, p4

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    .line 118
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    const-string v11, "Lucene40TermVectorsDocs"

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v10, v11, v12, v13}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v7

    .line 119
    .local v7, "tvdVersion":I
    const-string v10, ""

    const-string v11, "tvf"

    invoke-static {v4, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 120
    move-object/from16 v0, p4

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 121
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    const-string v11, "Lucene40TermVectorsFields"

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v10, v11, v12, v13}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v8

    .line 122
    .local v8, "tvfVersion":I
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_1

    sget-wide v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_INDEX:J

    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_1

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    .end local v2    # "fn":Ljava/lang/String;
    .end local v3    # "idxName":Ljava/lang/String;
    .end local v7    # "tvdVersion":I
    .end local v8    # "tvfVersion":I
    .end local v9    # "tvxVersion":I
    :catchall_0
    move-exception v10

    .line 141
    if-nez v6, :cond_0

    .line 143
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 146
    :cond_0
    :goto_0
    throw v10

    .line 123
    .restart local v2    # "fn":Ljava/lang/String;
    .restart local v3    # "idxName":Ljava/lang/String;
    .restart local v7    # "tvdVersion":I
    .restart local v8    # "tvfVersion":I
    .restart local v9    # "tvxVersion":I
    :cond_1
    :try_start_2
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_2

    sget-wide v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_DOCS:J

    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_2

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 124
    :cond_2
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_3

    sget-wide v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_FIELDS:J

    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_3

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 125
    :cond_3
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_4

    if-eq v9, v7, :cond_4

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 126
    :cond_4
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_5

    if-eq v9, v8, :cond_5

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 128
    :cond_5
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v10}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v10

    sget-wide v12, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_INDEX:J

    sub-long/2addr v10, v12

    const/4 v12, 0x4

    shr-long/2addr v10, v12

    long-to-int v10, v10

    iput v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    .line 130
    iget v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    iput v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->size:I

    .line 131
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_6

    if-eqz v5, :cond_6

    iget v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    if-eq v10, v5, :cond_6

    new-instance v10, Ljava/lang/AssertionError;

    invoke-direct {v10}, Ljava/lang/AssertionError;-><init>()V

    throw v10

    .line 133
    :cond_6
    move-object/from16 v0, p3

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 134
    const/4 v6, 0x1

    .line 141
    if-nez v6, :cond_7

    .line 143
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 147
    :cond_7
    :goto_1
    return-void

    .line 144
    .end local v2    # "fn":Ljava/lang/String;
    .end local v3    # "idxName":Ljava/lang/String;
    .end local v7    # "tvdVersion":I
    .end local v8    # "tvfVersion":I
    .end local v9    # "tvxVersion":I
    :catch_0
    move-exception v11

    goto :goto_0

    .restart local v2    # "fn":Ljava/lang/String;
    .restart local v3    # "idxName":Ljava/lang/String;
    .restart local v7    # "tvdVersion":I
    .restart local v8    # "tvfVersion":I
    .restart local v9    # "tvxVersion":I
    :catch_1
    move-exception v10

    goto :goto_1
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 7

    .prologue
    .line 752
    const/4 v2, 0x0

    .line 753
    .local v2, "cloneTvx":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 754
    .local v3, "cloneTvd":Lorg/apache/lucene/store/IndexInput;
    const/4 v4, 0x0

    .line 758
    .local v4, "cloneTvf":Lorg/apache/lucene/store/IndexInput;
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 760
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    .line 761
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 764
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->size:I

    iget v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;-><init>(Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;II)V

    return-object v0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 211
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method public get(I)Lorg/apache/lucene/index/Fields;
    .locals 3
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 735
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-eqz v2, :cond_1

    .line 736
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVFields;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVFields;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;I)V

    .line 737
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    invoke-virtual {v0}, Lorg/apache/lucene/index/Fields;->size()I

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 746
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method getTvdStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method getTvfStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method final rawDocs([I[III)V
    .locals 14
    .param p1, "tvdLengths"    # [I
    .param p2, "tvfLengths"    # [I
    .param p3, "startDocID"    # I
    .param p4, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    if-nez v12, :cond_1

    .line 173
    const/4 v12, 0x0

    invoke-static {p1, v12}, Ljava/util/Arrays;->fill([II)V

    .line 174
    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v12}, Ljava/util/Arrays;->fill([II)V

    .line 207
    :cond_0
    return-void

    .line 178
    :cond_1
    move/from16 v0, p3

    invoke-virtual {p0, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->seekTvx(I)V

    .line 180
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 181
    .local v8, "tvdPosition":J
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 183
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v10

    .line 184
    .local v10, "tvfPosition":J
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12, v10, v11}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 186
    move-wide v4, v8

    .line 187
    .local v4, "lastTvdPosition":J
    move-wide v6, v10

    .line 189
    .local v6, "lastTvfPosition":J
    const/4 v2, 0x0

    .line 190
    .local v2, "count":I
    :goto_0
    move/from16 v0, p4

    if-ge v2, v0, :cond_0

    .line 191
    add-int v12, p3, v2

    add-int/lit8 v3, v12, 0x1

    .line 192
    .local v3, "docID":I
    sget-boolean v12, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    iget v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    if-le v3, v12, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 193
    :cond_2
    iget v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->numTotalDocs:I

    if-ge v3, v12, :cond_4

    .line 194
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 195
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v10

    .line 201
    :cond_3
    sub-long v12, v8, v4

    long-to-int v12, v12

    aput v12, p1, v2

    .line 202
    sub-long v12, v10, v6

    long-to-int v12, v12

    aput v12, p2, v2

    .line 203
    add-int/lit8 v2, v2, 0x1

    .line 204
    move-wide v4, v8

    .line 205
    move-wide v6, v10

    goto :goto_0

    .line 197
    :cond_4
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvd:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    .line 198
    iget-object v12, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v12}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v10

    .line 199
    sget-boolean v12, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->$assertionsDisabled:Z

    if-nez v12, :cond_3

    add-int/lit8 v12, p4, -0x1

    if-eq v2, v12, :cond_3

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12
.end method

.method seekTvx(I)V
    .locals 6
    .param p1, "docNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvx:Lorg/apache/lucene/store/IndexInput;

    int-to-long v2, p1

    const-wide/16 v4, 0x10

    mul-long/2addr v2, v4

    sget-wide v4, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_INDEX:J

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 162
    return-void
.end method

.method size()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->size:I

    return v0
.end method
