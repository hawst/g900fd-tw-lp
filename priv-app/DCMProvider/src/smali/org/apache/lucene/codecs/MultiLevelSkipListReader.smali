.class public abstract Lorg/apache/lucene/codecs/MultiLevelSkipListReader;
.super Ljava/lang/Object;
.source "MultiLevelSkipListReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private childPointer:[J

.field private docCount:I

.field private haveSkipped:Z

.field private inputIsBuffered:Z

.field private lastChildPointer:J

.field private lastDoc:I

.field protected maxNumberOfSkipLevels:I

.field private numSkipped:[I

.field private numberOfLevelsToBuffer:I

.field private numberOfSkipLevels:I

.field protected skipDoc:[I

.field private skipInterval:[I

.field private final skipMultiplier:I

.field private skipPointer:[J

.field private skipStream:[Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/store/IndexInput;II)V
    .locals 0
    .param p1, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "maxSkipLevels"    # I
    .param p3, "skipInterval"    # I

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3, p3}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;III)V

    .line 110
    return-void
.end method

.method protected constructor <init>(Lorg/apache/lucene/store/IndexInput;III)V
    .locals 4
    .param p1, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "maxSkipLevels"    # I
    .param p3, "skipInterval"    # I
    .param p4, "skipMultiplier"    # I

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfLevelsToBuffer:I

    .line 88
    new-array v1, p2, [Lorg/apache/lucene/store/IndexInput;

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    .line 89
    new-array v1, p2, [J

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    .line 90
    new-array v1, p2, [J

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->childPointer:[J

    .line 91
    new-array v1, p2, [I

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    .line 92
    iput p2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->maxNumberOfSkipLevels:I

    .line 93
    new-array v1, p2, [I

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    .line 94
    iput p4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipMultiplier:I

    .line 95
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aput-object p1, v1, v2

    .line 96
    instance-of v1, p1, Lorg/apache/lucene/store/BufferedIndexInput;

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->inputIsBuffered:Z

    .line 97
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    aput p3, v1, v2

    .line 98
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-lt v0, p2, :cond_0

    .line 102
    new-array v1, p2, [I

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    .line 103
    return-void

    .line 100
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    iget-object v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    mul-int/2addr v2, p4

    aput v2, v1, v0

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadNextSkip(I)Z
    .locals 6
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->setLastSkipData(I)V

    .line 158
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    aget v1, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    aget v2, v2, p1

    add-int/2addr v1, v2

    aput v1, v0, p1

    .line 160
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    aget v0, v0, p1

    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->docCount:I

    if-le v0, v1, :cond_1

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    const v1, 0x7fffffff

    aput v1, v0, p1

    .line 163
    iget v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    if-le v0, p1, :cond_0

    iput p1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    .line 164
    :cond_0
    const/4 v0, 0x0

    .line 175
    :goto_0
    return v0

    .line 168
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    aget v1, v0, p1

    iget-object v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v2, v2, p1

    invoke-virtual {p0, p1, v2}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->readSkipData(ILorg/apache/lucene/store/IndexInput;)I

    move-result v2

    add-int/2addr v1, v2

    aput v1, v0, p1

    .line 170
    if-eqz p1, :cond_2

    .line 172
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->childPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    add-int/lit8 v4, p1, -0x1

    aget-wide v4, v1, v4

    add-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 175
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadSkipLevels()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 216
    iget v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->docCount:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    aget v5, v5, v8

    if-gt v4, v5, :cond_1

    .line 217
    const/4 v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    .line 222
    :goto_0
    iget v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    iget v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->maxNumberOfSkipLevels:I

    if-le v4, v5, :cond_0

    .line 223
    iget v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->maxNumberOfSkipLevels:I

    iput v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    .line 226
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v8

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    aget-wide v6, v5, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 228
    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfLevelsToBuffer:I

    .line 230
    .local v1, "toBuffer":I
    iget v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_1
    if-gtz v0, :cond_2

    .line 253
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    aput-wide v6, v4, v8

    .line 254
    return-void

    .line 219
    .end local v0    # "i":I
    .end local v1    # "toBuffer":I
    :cond_1
    iget v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->docCount:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    aget v5, v5, v8

    div-int/2addr v4, v5

    int-to-long v4, v4

    iget v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipMultiplier:I

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/util/MathUtil;->log(JI)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    goto :goto_0

    .line 232
    .restart local v0    # "i":I
    .restart local v1    # "toBuffer":I
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v8

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    .line 235
    .local v2, "length":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    aput-wide v6, v4, v0

    .line 236
    if-lez v1, :cond_3

    .line 238
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    new-instance v5, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;

    iget-object v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v6, v6, v8

    long-to-int v7, v2

    invoke-direct {v5, v6, v7}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader$SkipBuffer;-><init>(Lorg/apache/lucene/store/IndexInput;I)V

    aput-object v5, v4, v0

    .line 239
    add-int/lit8 v1, v1, -0x1

    .line 230
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 242
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v5

    aput-object v5, v4, v0

    .line 243
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->inputIsBuffered:Z

    if-eqz v4, :cond_4

    const-wide/16 v4, 0x400

    cmp-long v4, v2, v4

    if-gez v4, :cond_4

    .line 244
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v0

    check-cast v4, Lorg/apache/lucene/store/BufferedIndexInput;

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/BufferedIndexInput;->setBufferSize(I)V

    .line 248
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v4, v4, v8

    iget-object v5, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_2
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 196
    return-void

    .line 192
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 193
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 191
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getDoc()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->lastDoc:I

    return v0
.end method

.method public init(JI)V
    .locals 9
    .param p1, "skipPointer"    # J
    .param p3, "df"    # I

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 200
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    aput-wide p1, v1, v4

    .line 201
    iput p3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->docCount:I

    .line 202
    sget-boolean v1, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    cmp-long v1, p1, v6

    if-ltz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v2

    cmp-long v1, p1, v2

    if-lez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    .line 203
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invalid skip pointer: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 204
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    .line 205
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([II)V

    .line 206
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->childPointer:[J

    invoke-static {v1, v6, v7}, Ljava/util/Arrays;->fill([JJ)V

    .line 208
    iput-boolean v4, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->haveSkipped:Z

    .line 209
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    if-lt v0, v1, :cond_2

    .line 212
    return-void

    .line 210
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 209
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected abstract readSkipData(ILorg/apache/lucene/store/IndexInput;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected seekChild(I)V
    .locals 6
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v0, v0, p1

    iget-wide v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->lastChildPointer:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 182
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    add-int/lit8 v2, p1, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    add-int/lit8 v3, p1, 0x1

    aget v2, v2, v3

    sub-int/2addr v1, v2

    aput v1, v0, p1

    .line 183
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->lastDoc:I

    aput v1, v0, p1

    .line 184
    if-lez p1, :cond_0

    .line 185
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->childPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipPointer:[J

    add-int/lit8 v4, p1, -0x1

    aget-wide v4, v1, v4

    add-long/2addr v2, v4

    aput-wide v2, v0, p1

    .line 187
    :cond_0
    return-void
.end method

.method protected setLastSkipData(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->lastDoc:I

    .line 267
    iget-object v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->childPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->lastChildPointer:J

    .line 268
    return-void
.end method

.method public skipTo(I)I
    .locals 7
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 123
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->haveSkipped:Z

    if-nez v1, :cond_0

    .line 125
    invoke-direct {p0}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->loadSkipLevels()V

    .line 126
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->haveSkipped:Z

    .line 131
    :cond_0
    const/4 v0, 0x0

    .line 132
    .local v0, "level":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numberOfSkipLevels:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    if-gt p1, v1, :cond_2

    .line 136
    :cond_1
    :goto_1
    if-gez v0, :cond_3

    .line 150
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->numSkipped:[I

    aget v1, v1, v6

    iget-object v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipInterval:[I

    aget v2, v2, v6

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    return v1

    .line 133
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipDoc:[I

    aget v1, v1, v0

    if-le p1, v1, :cond_4

    .line 138
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->loadNextSkip(I)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 143
    :cond_4
    if-lez v0, :cond_5

    iget-wide v2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->lastChildPointer:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->skipStream:[Lorg/apache/lucene/store/IndexInput;

    add-int/lit8 v4, v0, -0x1

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_5

    .line 144
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->seekChild(I)V

    .line 146
    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method
