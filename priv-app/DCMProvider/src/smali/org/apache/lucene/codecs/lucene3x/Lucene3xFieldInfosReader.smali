.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosReader;
.super Lorg/apache/lucene/codecs/FieldInfosReader;
.source "Lucene3xFieldInfosReader.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final FIELD_INFOS_EXTENSION:Ljava/lang/String; = "fnm"

.field static final FORMAT_CURRENT:I = -0x3

.field static final FORMAT_MINIMUM:I = -0x2

.field static final FORMAT_OMIT_POSITIONS:I = -0x3

.field static final FORMAT_START:I = -0x2

.field static final IS_INDEXED:B = 0x1t

.field static final OMIT_NORMS:B = 0x10t

.field static final OMIT_POSITIONS:B = -0x80t

.field static final OMIT_TERM_FREQ_AND_POSITIONS:B = 0x40t

.field static final STORE_PAYLOADS:B = 0x20t

.field static final STORE_TERMVECTOR:B = 0x2t


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosReader;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/FieldInfos;
    .locals 24
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "iocontext"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    const-string v2, ""

    const-string v10, "fnm"

    move-object/from16 v0, p2

    invoke-static {v0, v2, v10}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 62
    .local v15, "fileName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v15, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v19

    .line 64
    .local v19, "input":Lorg/apache/lucene/store/IndexInput;
    const/16 v21, 0x0

    .line 66
    .local v21, "success":Z
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v16

    .line 68
    .local v16, "format":I
    const/4 v2, -0x2

    move/from16 v0, v16

    if-le v0, v2, :cond_0

    .line 69
    new-instance v2, Lorg/apache/lucene/index/IndexFormatTooOldException;

    const/4 v10, -0x2

    const/4 v11, -0x3

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-direct {v2, v0, v1, v10, v11}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    .end local v16    # "format":I
    :catchall_0
    move-exception v2

    .line 118
    if-eqz v21, :cond_f

    .line 119
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 123
    :goto_0
    throw v2

    .line 71
    .restart local v16    # "format":I
    :cond_0
    const/4 v2, -0x3

    move/from16 v0, v16

    if-ge v0, v2, :cond_1

    .line 72
    :try_start_1
    new-instance v2, Lorg/apache/lucene/index/IndexFormatTooNewException;

    const/4 v10, -0x2

    const/4 v11, -0x3

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-direct {v2, v0, v1, v10, v11}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v2

    .line 75
    :cond_1
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v20

    .line 76
    .local v20, "size":I
    move/from16 v0, v20

    new-array v0, v0, [Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v18, v0

    .line 78
    .local v18, "infos":[Lorg/apache/lucene/index/FieldInfo;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v20

    if-lt v0, v1, :cond_2

    .line 111
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v22

    cmp-long v2, v10, v22

    if-eqz v2, :cond_d

    .line 112
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "did not read all bytes from file \""

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\": read "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " vs size "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " (resource: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 79
    :cond_2
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "name":Ljava/lang/String;
    move/from16 v5, v17

    .line 81
    .local v5, "fieldNumber":I
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v13

    .line 82
    .local v13, "bits":B
    and-int/lit8 v2, v13, 0x1

    if-eqz v2, :cond_4

    const/4 v4, 0x1

    .line 83
    .local v4, "isIndexed":Z
    :goto_2
    and-int/lit8 v2, v13, 0x2

    if-eqz v2, :cond_5

    const/4 v6, 0x1

    .line 84
    .local v6, "storeTermVector":Z
    :goto_3
    and-int/lit8 v2, v13, 0x10

    if-eqz v2, :cond_6

    const/4 v7, 0x1

    .line 85
    .local v7, "omitNorms":Z
    :goto_4
    and-int/lit8 v2, v13, 0x20

    if-eqz v2, :cond_7

    const/4 v8, 0x1

    .line 87
    .local v8, "storePayloads":Z
    :goto_5
    if-nez v4, :cond_8

    .line 88
    const/4 v9, 0x0

    .line 104
    .local v9, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :goto_6
    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v9, v2, :cond_3

    .line 105
    const/4 v8, 0x0

    .line 107
    :cond_3
    new-instance v2, Lorg/apache/lucene/index/FieldInfo;

    .line 108
    const/4 v10, 0x0

    if-eqz v4, :cond_c

    if-nez v7, :cond_c

    sget-object v11, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    :goto_7
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v12

    invoke-direct/range {v2 .. v12}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 107
    aput-object v2, v18, v17

    .line 78
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 82
    .end local v4    # "isIndexed":Z
    .end local v6    # "storeTermVector":Z
    .end local v7    # "omitNorms":Z
    .end local v8    # "storePayloads":Z
    .end local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    .line 83
    .restart local v4    # "isIndexed":Z
    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    .line 84
    .restart local v6    # "storeTermVector":Z
    :cond_6
    const/4 v7, 0x0

    goto :goto_4

    .line 85
    .restart local v7    # "omitNorms":Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_5

    .line 89
    .restart local v8    # "storePayloads":Z
    :cond_8
    and-int/lit8 v2, v13, 0x40

    if-eqz v2, :cond_9

    .line 90
    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 91
    .restart local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .end local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_9
    and-int/lit8 v2, v13, -0x80

    if-eqz v2, :cond_b

    .line 92
    const/4 v2, -0x3

    move/from16 v0, v16

    if-gt v0, v2, :cond_a

    .line 93
    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 94
    .restart local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .line 95
    .end local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_a
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Corrupt fieldinfos, OMIT_POSITIONS set but format="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " (resource: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_b
    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .restart local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .line 108
    :cond_c
    const/4 v11, 0x0

    goto :goto_7

    .line 114
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "isIndexed":Z
    .end local v5    # "fieldNumber":I
    .end local v6    # "storeTermVector":Z
    .end local v7    # "omitNorms":Z
    .end local v8    # "storePayloads":Z
    .end local v9    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v13    # "bits":B
    :cond_d
    new-instance v14, Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Lorg/apache/lucene/index/FieldInfos;-><init>([Lorg/apache/lucene/index/FieldInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    .local v14, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    const/16 v21, 0x1

    .line 118
    if-eqz v21, :cond_e

    .line 119
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 116
    :goto_8
    return-object v14

    .line 120
    :cond_e
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 121
    aput-object v19, v2, v10

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_8

    .line 120
    .end local v14    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v16    # "format":I
    .end local v17    # "i":I
    .end local v18    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .end local v20    # "size":I
    :cond_f
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 121
    aput-object v19, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_0
.end method
