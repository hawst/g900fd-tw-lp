.class Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleTVTermsEnum"
.end annotation


# instance fields
.field current:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;",
            ">;"
        }
    .end annotation
.end field

.field iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;",
            ">;>;"
        }
    .end annotation
.end field

.field terms:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/SortedMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedMap",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p1, "terms":Ljava/util/SortedMap;, "Ljava/util/SortedMap<Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 329
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->terms:Ljava/util/SortedMap;

    .line 330
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->iterator:Ljava/util/Iterator;

    .line 331
    return-void
.end method


# virtual methods
.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 370
    const/4 v0, 0x1

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 381
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;)V

    .line 382
    .local v0, "e":Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;
    and-int/lit8 v1, p3, 0x1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsEnum;->reset(Lorg/apache/lucene/util/Bits;I)V

    .line 383
    return-object v0

    .line 382
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->current:Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static {v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v1

    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 7
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 388
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->current:Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;

    .line 389
    .local v6, "postings":Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->positions:[I
    invoke-static {v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v2

    if-nez v2, :cond_0

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->startOffsets:[I
    invoke-static {v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 395
    :goto_0
    return-object v0

    .line 393
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;)V

    .line 394
    .local v0, "e":Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->positions:[I
    invoke-static {v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v2

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->startOffsets:[I
    invoke-static {v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v3

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->endOffsets:[I
    invoke-static {v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$3(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[I

    move-result-object v4

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->payloads:[Lorg/apache/lucene/util/BytesRef;
    invoke-static {v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$4(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)[Lorg/apache/lucene/util/BytesRef;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->reset(Lorg/apache/lucene/util/Bits;[I[I[I[Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 400
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 351
    const/4 v0, 0x0

    .line 354
    :goto_0
    return-object v0

    .line 353
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->current:Ljava/util/Map$Entry;

    .line 354
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->current:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public ord()J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->terms:Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->iterator:Ljava/util/Iterator;

    .line 336
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 339
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->current:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;->current:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->freq:I
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
