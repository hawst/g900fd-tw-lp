.class public abstract Lorg/apache/lucene/codecs/TermVectorsFormat;
.super Ljava/lang/Object;
.source "TermVectorsFormat.java"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method


# virtual methods
.method public abstract vectorsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract vectorsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
