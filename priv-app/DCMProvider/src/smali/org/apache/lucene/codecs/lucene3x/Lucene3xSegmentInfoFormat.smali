.class public Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;
.super Lorg/apache/lucene/codecs/SegmentInfoFormat;
.source "Lucene3xSegmentInfoFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final DS_COMPOUND_KEY:Ljava/lang/String;

.field public static final DS_NAME_KEY:Ljava/lang/String;

.field public static final DS_OFFSET_KEY:Ljava/lang/String;

.field public static final FORMAT_3_1:I = -0xb

.field public static final FORMAT_DIAGNOSTICS:I = -0x9

.field public static final FORMAT_HAS_VECTORS:I = -0xa

.field public static final NORMGEN_KEY:Ljava/lang/String;

.field public static final NORMGEN_PREFIX:Ljava/lang/String;

.field public static final UPGRADED_SI_CODEC_NAME:Ljava/lang/String; = "Lucene3xSegmentInfo"

.field public static final UPGRADED_SI_EXTENSION:Ljava/lang/String; = "si"

.field public static final UPGRADED_SI_VERSION_CURRENT:I

.field public static final UPGRADED_SI_VERSION_START:I


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/SegmentInfoReader;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".dsoffset"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_OFFSET_KEY:Ljava/lang/String;

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".dsname"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_NAME_KEY:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".dscompound"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_COMPOUND_KEY:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".normgen"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_KEY:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".normfield"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_PREFIX:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoFormat;-><init>()V

    .line 33
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

    .line 32
    return-void
.end method

.method public static getDocStoreIsCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 2
    .param p0, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 86
    sget-object v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_COMPOUND_KEY:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "v":Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public static getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I
    .locals 2
    .param p0, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 74
    sget-object v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_OFFSET_KEY:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "v":Ljava/lang/String;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public static getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    .line 80
    sget-object v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_NAME_KEY:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "v":Ljava/lang/String;
    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .end local v0    # "v":Ljava/lang/String;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getSegmentInfoReader()Lorg/apache/lucene/codecs/SegmentInfoReader;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

    return-object v0
.end method

.method public getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
