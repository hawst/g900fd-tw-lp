.class Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;
.super Lorg/apache/lucene/codecs/DocValuesProducer;
.source "PerFieldDocValuesFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FieldsReader"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/DocValuesProducer;",
            ">;"
        }
    .end annotation
.end field

.field private final formats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/DocValuesProducer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 191
    const-class v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;)V
    .locals 7
    .param p2, "other"    # Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;

    .prologue
    .line 227
    iput-object p1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 193
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    .line 194
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    .line 229
    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1}, Ljava/util/IdentityHashMap;-><init>()V

    .line 231
    .local v1, "oldToNew":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/codecs/DocValuesProducer;Lorg/apache/lucene/codecs/DocValuesProducer;>;"
    iget-object v4, p2, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 238
    iget-object v4, p2, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 243
    return-void

    .line 231
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 232
    .local v0, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/codecs/DocValuesProducer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 233
    .local v3, "values":Lorg/apache/lucene/codecs/DocValuesProducer;
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v6, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 238
    .end local v0    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/codecs/DocValuesProducer;>;"
    .end local v3    # "values":Lorg/apache/lucene/codecs/DocValuesProducer;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 239
    .restart local v0    # "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/codecs/DocValuesProducer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 240
    .local v2, "producer":Lorg/apache/lucene/codecs/DocValuesProducer;
    sget-boolean v4, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-nez v2, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 241
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v6, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;Lorg/apache/lucene/index/SegmentReadState;)V
    .locals 10
    .param p2, "readState"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    iput-object p1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 193
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    .line 194
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    .line 199
    const/4 v5, 0x0

    .line 202
    .local v5, "success":Z
    :try_start_0
    iget-object v7, p2, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_2

    .line 219
    const/4 v5, 0x1

    .line 221
    if-nez v5, :cond_1

    .line 222
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 225
    :cond_1
    return-void

    .line 202
    :cond_2
    :try_start_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 203
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 204
    iget-object v1, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 205
    .local v1, "fieldName":Ljava/lang/String;
    sget-object v7, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->PER_FIELD_FORMAT_KEY:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 206
    .local v3, "formatName":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 208
    sget-object v7, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->PER_FIELD_SUFFIX_KEY:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 209
    .local v6, "suffix":Ljava/lang/String;
    sget-boolean v7, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    if-nez v6, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 220
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v1    # "fieldName":Ljava/lang/String;
    .end local v3    # "formatName":Ljava/lang/String;
    .end local v6    # "suffix":Ljava/lang/String;
    :catchall_0
    move-exception v7

    .line 221
    if-nez v5, :cond_3

    .line 222
    iget-object v8, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 224
    :cond_3
    throw v7

    .line 210
    .restart local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .restart local v1    # "fieldName":Ljava/lang/String;
    .restart local v3    # "formatName":Ljava/lang/String;
    .restart local v6    # "suffix":Ljava/lang/String;
    :cond_4
    :try_start_2
    invoke-static {v3}, Lorg/apache/lucene/codecs/DocValuesFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v2

    .line 211
    .local v2, "format":Lorg/apache/lucene/codecs/DocValuesFormat;
    invoke-static {v3, v6}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->getSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 212
    .local v4, "segmentSuffix":Ljava/lang/String;
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 213
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    new-instance v9, Lorg/apache/lucene/index/SegmentReadState;

    invoke-direct {v9, p2, v4}, Lorg/apache/lucene/index/SegmentReadState;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lorg/apache/lucene/codecs/DocValuesFormat;->fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;

    move-result-object v9

    invoke-interface {v7, v4, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/codecs/DocValuesProducer;

    invoke-interface {v9, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->clone()Lorg/apache/lucene/codecs/DocValuesProducer;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 2

    .prologue
    .line 276
    new-instance v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-direct {v0, v1, p0}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;-><init>(Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;)V

    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V

    .line 272
    return-void
.end method

.method public getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    iget-object v1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 254
    .local v0, "producer":Lorg/apache/lucene/codecs/DocValuesProducer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/DocValuesProducer;->getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 248
    .local v0, "producer":Lorg/apache/lucene/codecs/DocValuesProducer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/DocValuesProducer;->getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    iget-object v1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 260
    .local v0, "producer":Lorg/apache/lucene/codecs/DocValuesProducer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/DocValuesProducer;->getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    goto :goto_0
.end method

.method public getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    iget-object v1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;->fields:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/DocValuesProducer;

    .line 266
    .local v0, "producer":Lorg/apache/lucene/codecs/DocValuesProducer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/DocValuesProducer;->getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    goto :goto_0
.end method
