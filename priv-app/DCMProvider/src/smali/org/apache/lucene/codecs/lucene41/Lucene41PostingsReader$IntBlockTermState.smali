.class final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
.super Lorg/apache/lucene/codecs/BlockTermState;
.source "Lucene41PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IntBlockTermState"
.end annotation


# instance fields
.field bytes:[B

.field bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field docStartFP:J

.field lastPosBlockOffset:J

.field payStartFP:J

.field posStartFP:J

.field singletonDocID:I

.field skipOffset:J


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTermState;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;-><init>()V

    .line 163
    .local v0, "other":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    invoke-virtual {v0, p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->copyFrom(Lorg/apache/lucene/index/TermState;)V

    .line 164
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/index/TermState;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->clone()Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    move-result-object v0

    return-object v0
.end method

.method public copyFrom(Lorg/apache/lucene/index/TermState;)V
    .locals 4
    .param p1, "_other"    # Lorg/apache/lucene/index/TermState;

    .prologue
    .line 169
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/BlockTermState;->copyFrom(Lorg/apache/lucene/index/TermState;)V

    move-object v0, p1

    .line 170
    check-cast v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    .line 171
    .local v0, "other":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    .line 172
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    .line 173
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    .line 174
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    .line 175
    iget-wide v2, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    .line 176
    iget v1, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    .line 182
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lorg/apache/lucene/codecs/BlockTermState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " docStartFP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " posStartFP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " payStartFP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastPosBlockOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " singletonDocID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
