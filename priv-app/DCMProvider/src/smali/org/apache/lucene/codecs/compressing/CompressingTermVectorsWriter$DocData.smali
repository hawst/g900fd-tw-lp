.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;
.super Ljava/lang/Object;
.source "CompressingTermVectorsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DocData"
.end annotation


# instance fields
.field final fields:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;",
            ">;"
        }
    .end annotation
.end field

.field final numFields:I

.field final offStart:I

.field final payStart:I

.field final posStart:I

.field final synthetic this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;IIII)V
    .locals 1
    .param p2, "numFields"    # I
    .param p3, "posStart"    # I
    .param p4, "offStart"    # I
    .param p5, "payStart"    # I

    .prologue
    .line 93
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->numFields:I

    .line 95
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0, p2}, Ljava/util/ArrayDeque;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    .line 96
    iput p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->posStart:I

    .line 97
    iput p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->offStart:I

    .line 98
    iput p5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->payStart:I

    .line 99
    return-void
.end method


# virtual methods
.method addField(IIZZZ)Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .locals 11
    .param p1, "fieldNum"    # I
    .param p2, "numTerms"    # I
    .param p3, "positions"    # Z
    .param p4, "offsets"    # Z
    .param p5, "payloads"    # Z

    .prologue
    .line 102
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    iget v7, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->posStart:I

    iget v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->offStart:I

    iget v9, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->payStart:I

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v9}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;IIZZZIII)V

    .line 111
    .local v0, "field":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v1, v0}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 112
    return-object v0

    .line 105
    .end local v0    # "field":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->fields:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    .line 106
    .local v10, "last":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    iget v2, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->posStart:I

    iget-boolean v1, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPositions:Z

    if-eqz v1, :cond_1

    iget v1, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    :goto_1
    add-int v7, v2, v1

    .line 107
    .local v7, "posStart":I
    iget v2, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->offStart:I

    iget-boolean v1, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasOffsets:Z

    if-eqz v1, :cond_2

    iget v1, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    :goto_2
    add-int v8, v2, v1

    .line 108
    .local v8, "offStart":I
    iget v2, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->payStart:I

    iget-boolean v1, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->hasPayloads:Z

    if-eqz v1, :cond_3

    iget v1, v10, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;->totalPositions:I

    :goto_3
    add-int v9, v2, v1

    .line 109
    .local v9, "payStart":I
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$DocData;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    invoke-direct/range {v0 .. v9}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;IIZZZIII)V

    .restart local v0    # "field":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    goto :goto_0

    .line 106
    .end local v0    # "field":Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter$FieldData;
    .end local v7    # "posStart":I
    .end local v8    # "offStart":I
    .end local v9    # "payStart":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 107
    .restart local v7    # "posStart":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 108
    .restart local v8    # "offStart":I
    :cond_3
    const/4 v1, 0x0

    goto :goto_3
.end method
