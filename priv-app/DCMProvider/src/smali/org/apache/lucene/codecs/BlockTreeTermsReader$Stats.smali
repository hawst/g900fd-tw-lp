.class public Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
.super Ljava/lang/Object;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Stats"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public blockCountByPrefixLen:[I

.field private endBlockCount:I

.field public final field:Ljava/lang/String;

.field public floorBlockCount:I

.field public floorSubBlockCount:I

.field public indexArcCount:J

.field public indexNodeCount:J

.field public indexNumBytes:J

.field public mixedBlockCount:I

.field public nonFloorBlockCount:I

.field public final segment:Ljava/lang/String;

.field private startBlockCount:I

.field public subBlocksOnlyBlockCount:I

.field public termsOnlyBlockCount:I

.field public totalBlockCount:I

.field public totalBlockOtherBytes:J

.field public totalBlockStatsBytes:J

.field public totalBlockSuffixBytes:J

.field public totalTermBytes:J

.field public totalTermCount:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 277
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "segment"    # Ljava/lang/String;
    .param p2, "field"    # Ljava/lang/String;

    .prologue
    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    .line 342
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->segment:Ljava/lang/String;

    .line 343
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->field:Ljava/lang/String;

    .line 344
    return-void
.end method


# virtual methods
.method endBlock(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V
    .locals 8
    .param p1, "frame"    # Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .prologue
    .line 367
    iget-boolean v4, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    if-eqz v4, :cond_0

    iget v3, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    .line 368
    .local v3, "termCount":I
    :goto_0
    iget v4, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    sub-int v2, v4, v3

    .line 369
    .local v2, "subBlockCount":I
    iget-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermCount:J

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermCount:J

    .line 370
    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 371
    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->mixedBlockCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->mixedBlockCount:I

    .line 379
    :goto_1
    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->endBlockCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->endBlockCount:I

    .line 380
    iget-wide v4, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpEnd:J

    iget-wide v6, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    sub-long/2addr v4, v6

    iget-object v6, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/ByteArrayDataInput;->length()I

    move-result v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    iget-object v6, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/ByteArrayDataInput;->length()I

    move-result v6

    int-to-long v6, v6

    sub-long v0, v4, v6

    .line 381
    .local v0, "otherBytes":J
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-gtz v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "otherBytes="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " frame.fp="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " frame.fpEnd="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 367
    .end local v0    # "otherBytes":J
    .end local v2    # "subBlockCount":I
    .end local v3    # "termCount":I
    :cond_0
    iget-object v4, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v3, v4, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    goto :goto_0

    .line 372
    .restart local v2    # "subBlockCount":I
    .restart local v3    # "termCount":I
    :cond_1
    if-eqz v3, :cond_2

    .line 373
    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->termsOnlyBlockCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->termsOnlyBlockCount:I

    goto :goto_1

    .line 374
    :cond_2
    if-eqz v2, :cond_3

    .line 375
    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->subBlocksOnlyBlockCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->subBlocksOnlyBlockCount:I

    goto :goto_1

    .line 377
    :cond_3
    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4}, Ljava/lang/IllegalStateException;-><init>()V

    throw v4

    .line 382
    .restart local v0    # "otherBytes":J
    :cond_4
    iget-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockOtherBytes:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockOtherBytes:J

    .line 383
    return-void
.end method

.method finish()V
    .locals 3

    .prologue
    .line 390
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlockCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->endBlockCount:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startBlockCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endBlockCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->endBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 391
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorSubBlockCount:I

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->nonFloorBlockCount:I

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "floorSubBlockCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorSubBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nonFloorBlockCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->nonFloorBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " totalBlockCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 392
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->mixedBlockCount:I

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->termsOnlyBlockCount:I

    add-int/2addr v1, v2

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->subBlocksOnlyBlockCount:I

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "totalBlockCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mixedBlockCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->mixedBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " subBlocksOnlyBlockCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->subBlocksOnlyBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " termsOnlyBlockCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->termsOnlyBlockCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 393
    :cond_2
    return-void
.end method

.method startBlock(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;Z)V
    .locals 4
    .param p1, "frame"    # Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .param p2, "isFloor"    # Z

    .prologue
    .line 347
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    .line 348
    if-eqz p2, :cond_2

    .line 349
    iget-wide v0, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    iget-wide v2, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 350
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorBlockCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorBlockCount:I

    .line 352
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorSubBlockCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorSubBlockCount:I

    .line 357
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    array-length v0, v0

    iget v1, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    if-gt v0, v1, :cond_1

    .line 358
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    iget v1, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    .line 360
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    iget v1, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 361
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlockCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlockCount:I

    .line 362
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockSuffixBytes:J

    iget-object v2, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->length()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockSuffixBytes:J

    .line 363
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockStatsBytes:J

    iget-object v2, p1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->length()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockStatsBytes:J

    .line 364
    return-void

    .line 354
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->nonFloorBlockCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->nonFloorBlockCount:I

    goto :goto_0
.end method

.method term(Lorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 386
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermBytes:J

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermBytes:J

    .line 387
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 18

    .prologue
    .line 397
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    const/16 v8, 0x400

    invoke-direct {v4, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 400
    .local v4, "bos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/PrintStream;

    const/4 v8, 0x0

    const-string v9, "UTF-8"

    invoke-direct {v5, v4, v8, v9}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 405
    .local v5, "out":Ljava/io/PrintStream;
    const-string v8, "  index FST:"

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 406
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->indexNodeCount:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " nodes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 407
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->indexArcCount:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " arcs"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 408
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->indexNumBytes:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 409
    const-string v8, "  terms:"

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 410
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermCount:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " terms"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 411
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermBytes:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermCount:J

    const-wide/16 v12, 0x0

    cmp-long v8, v10, v12

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, " ("

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v11, "%.1f"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermBytes:J

    long-to-double v14, v14

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalTermCount:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " bytes/term)"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_0
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 412
    const-string v8, "  blocks:"

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 413
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 414
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->termsOnlyBlockCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " terms-only blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 415
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->subBlocksOnlyBlockCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " sub-block-only blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 416
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->mixedBlockCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mixed blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 417
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorBlockCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " floor blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 418
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorSubBlockCount:I

    sub-int/2addr v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " non-floor blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 419
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->floorSubBlockCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " floor sub-blocks"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 420
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockSuffixBytes:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " term suffix bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, " ("

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v11, "%.1f"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockSuffixBytes:J

    long-to-double v14, v14

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " suffix-bytes/block)"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_1
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 421
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockStatsBytes:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " term stats bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, " ("

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v11, "%.1f"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockStatsBytes:J

    long-to-double v14, v14

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " stats-bytes/block)"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 422
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "    "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockOtherBytes:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " other bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    if-eqz v8, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, " ("

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v11, "%.1f"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockOtherBytes:J

    long-to-double v14, v14

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " other-bytes/block)"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_3
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 423
    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    if-eqz v8, :cond_6

    .line 424
    const-string v8, "    by prefix length:"

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 425
    const/4 v7, 0x0

    .line 426
    .local v7, "total":I
    const/4 v6, 0x0

    .local v6, "prefix":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    array-length v8, v8

    if-lt v6, v8, :cond_4

    .line 433
    sget-boolean v8, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->$assertionsDisabled:Z

    if-nez v8, :cond_6

    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->totalBlockCount:I

    if-eq v8, v7, :cond_6

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 401
    .end local v5    # "out":Ljava/io/PrintStream;
    .end local v6    # "prefix":I
    .end local v7    # "total":I
    :catch_0
    move-exception v3

    .line 402
    .local v3, "bogus":Ljava/io/UnsupportedEncodingException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 411
    .end local v3    # "bogus":Ljava/io/UnsupportedEncodingException;
    .restart local v5    # "out":Ljava/io/PrintStream;
    :cond_0
    const-string v8, ""

    goto/16 :goto_0

    .line 420
    :cond_1
    const-string v8, ""

    goto/16 :goto_1

    .line 421
    :cond_2
    const-string v8, ""

    goto/16 :goto_2

    .line 422
    :cond_3
    const-string v8, ""

    goto :goto_3

    .line 427
    .restart local v6    # "prefix":I
    .restart local v7    # "total":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->blockCountByPrefixLen:[I

    aget v2, v8, v6

    .line 428
    .local v2, "blockCount":I
    add-int/2addr v7, v2

    .line 429
    if-eqz v2, :cond_5

    .line 430
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "      "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v10, "%2d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 426
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 437
    .end local v2    # "blockCount":I
    .end local v6    # "prefix":I
    .end local v7    # "total":I
    :cond_6
    :try_start_1
    const-string v8, "UTF-8"

    invoke-virtual {v4, v8}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    return-object v8

    .line 438
    :catch_1
    move-exception v3

    .line 439
    .restart local v3    # "bogus":Ljava/io/UnsupportedEncodingException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8
.end method
