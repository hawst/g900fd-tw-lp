.class public abstract Lorg/apache/lucene/codecs/DocValuesConsumer;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lorg/apache/lucene/codecs/DocValuesConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public mergeBinaryField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V
    .locals 1
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Lorg/apache/lucene/index/MergeState;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/BinaryDocValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    .line 187
    .local p3, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/BinaryDocValues;>;"
    new-instance v0, Lorg/apache/lucene/codecs/DocValuesConsumer$2;

    invoke-direct {v0, p0, p3, p2}, Lorg/apache/lucene/codecs/DocValuesConsumer$2;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;Ljava/util/List;Lorg/apache/lucene/index/MergeState;)V

    .line 186
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 250
    return-void
.end method

.method public mergeNumericField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V
    .locals 1
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Lorg/apache/lucene/index/MergeState;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/NumericDocValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    .line 113
    .local p3, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/NumericDocValues;>;"
    new-instance v0, Lorg/apache/lucene/codecs/DocValuesConsumer$1;

    invoke-direct {v0, p0, p3, p2}, Lorg/apache/lucene/codecs/DocValuesConsumer$1;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;Ljava/util/List;Lorg/apache/lucene/index/MergeState;)V

    .line 112
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 176
    return-void
.end method

.method public mergeSortedField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V
    .locals 12
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Lorg/apache/lucene/index/MergeState;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SortedDocValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    .local p3, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SortedDocValues;>;"
    iget-object v10, p2, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [Lorg/apache/lucene/index/AtomicReader;

    invoke-interface {v10, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/lucene/index/AtomicReader;

    .line 261
    .local v8, "readers":[Lorg/apache/lucene/index/AtomicReader;
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Lorg/apache/lucene/index/SortedDocValues;

    invoke-interface {p3, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/apache/lucene/index/SortedDocValues;

    .line 264
    .local v2, "dvs":[Lorg/apache/lucene/index/SortedDocValues;
    array-length v10, v2

    new-array v5, v10, [Lorg/apache/lucene/index/TermsEnum;

    .line 265
    .local v5, "liveTerms":[Lorg/apache/lucene/index/TermsEnum;
    const/4 v9, 0x0

    .local v9, "sub":I
    :goto_0
    array-length v10, v5

    if-lt v9, v10, :cond_0

    .line 283
    new-instance v6, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-direct {v6, p0, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;-><init>(Ljava/lang/Object;[Lorg/apache/lucene/index/TermsEnum;)V

    .line 288
    .local v6, "map":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    new-instance v10, Lorg/apache/lucene/codecs/DocValuesConsumer$3;

    invoke-direct {v10, p0, v6, v2}, Lorg/apache/lucene/codecs/DocValuesConsumer$3;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;[Lorg/apache/lucene/index/SortedDocValues;)V

    .line 320
    new-instance v11, Lorg/apache/lucene/codecs/DocValuesConsumer$4;

    invoke-direct {v11, p0, v8, v2, v6}, Lorg/apache/lucene/codecs/DocValuesConsumer$4;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/SortedDocValues;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V

    .line 286
    invoke-virtual {p0, p1, v10, v11}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addSortedField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 383
    return-void

    .line 266
    .end local v6    # "map":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    :cond_0
    aget-object v7, v8, v9

    .line 267
    .local v7, "reader":Lorg/apache/lucene/index/AtomicReader;
    aget-object v1, v2, v9

    .line 268
    .local v1, "dv":Lorg/apache/lucene/index/SortedDocValues;
    invoke-virtual {v7}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v4

    .line 269
    .local v4, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-nez v4, :cond_1

    .line 270
    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v10

    aput-object v10, v5, v9

    .line 265
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 272
    :cond_1
    new-instance v0, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v10

    int-to-long v10, v10

    invoke-direct {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 273
    .local v0, "bitset":Lorg/apache/lucene/util/OpenBitSet;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-virtual {v7}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v10

    if-lt v3, v10, :cond_2

    .line 278
    new-instance v10, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SortedDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v11

    invoke-direct {v10, v11, v0}, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/OpenBitSet;)V

    aput-object v10, v5, v9

    goto :goto_1

    .line 274
    :cond_2
    invoke-interface {v4, v3}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 275
    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v0, v10, v11}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    .line 273
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method public mergeSortedSetField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/MergeState;Ljava/util/List;)V
    .locals 21
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Lorg/apache/lucene/index/MergeState;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SortedSetDocValues;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 392
    .local p3, "toMerge":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SortedSetDocValues;>;"
    move-object/from16 v0, p2

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v0, v0, [Lorg/apache/lucene/index/AtomicReader;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Lorg/apache/lucene/index/AtomicReader;

    .line 393
    .local v16, "readers":[Lorg/apache/lucene/index/AtomicReader;
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v0, v0, [Lorg/apache/lucene/index/SortedSetDocValues;

    move-object/from16 v18, v0

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/apache/lucene/index/SortedSetDocValues;

    .line 396
    .local v8, "dvs":[Lorg/apache/lucene/index/SortedSetDocValues;
    array-length v0, v8

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v11, v0, [Lorg/apache/lucene/index/TermsEnum;

    .line 397
    .local v11, "liveTerms":[Lorg/apache/lucene/index/TermsEnum;
    const/16 v17, 0x0

    .local v17, "sub":I
    :goto_0
    array-length v0, v11

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_0

    .line 419
    new-instance v12, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v11}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;-><init>(Ljava/lang/Object;[Lorg/apache/lucene/index/TermsEnum;)V

    .line 424
    .local v12, "map":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    new-instance v18, Lorg/apache/lucene/codecs/DocValuesConsumer$5;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12, v8}, Lorg/apache/lucene/codecs/DocValuesConsumer$5;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;[Lorg/apache/lucene/index/SortedSetDocValues;)V

    .line 456
    new-instance v19, Lorg/apache/lucene/codecs/DocValuesConsumer$6;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v8}, Lorg/apache/lucene/codecs/DocValuesConsumer$6;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/SortedSetDocValues;)V

    .line 523
    new-instance v20, Lorg/apache/lucene/codecs/DocValuesConsumer$7;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v8, v12}, Lorg/apache/lucene/codecs/DocValuesConsumer$7;-><init>(Lorg/apache/lucene/codecs/DocValuesConsumer;[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/SortedSetDocValues;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V

    .line 422
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    move-object/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addSortedSetField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)V

    .line 605
    return-void

    .line 398
    .end local v12    # "map":Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    :cond_0
    aget-object v13, v16, v17

    .line 399
    .local v13, "reader":Lorg/apache/lucene/index/AtomicReader;
    aget-object v7, v8, v17

    .line 400
    .local v7, "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    invoke-virtual {v13}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v10

    .line 401
    .local v10, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-nez v10, :cond_1

    .line 402
    invoke-virtual {v7}, Lorg/apache/lucene/index/SortedSetDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v18

    aput-object v18, v11, v17

    .line 397
    :goto_1
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 404
    :cond_1
    new-instance v6, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-direct {v6, v0, v1}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 405
    .local v6, "bitset":Lorg/apache/lucene/util/OpenBitSet;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    invoke-virtual {v13}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v18

    move/from16 v0, v18

    if-lt v9, v0, :cond_2

    .line 414
    new-instance v18, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;

    invoke-virtual {v7}, Lorg/apache/lucene/index/SortedSetDocValues;->termsEnum()Lorg/apache/lucene/index/TermsEnum;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/OpenBitSet;)V

    aput-object v18, v11, v17

    goto :goto_1

    .line 406
    :cond_2
    invoke-interface {v10, v9}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 407
    invoke-virtual {v7, v9}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 409
    :goto_3
    invoke-virtual {v7}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    .local v14, "ord":J
    const-wide/16 v18, -0x1

    cmp-long v18, v14, v18

    if-nez v18, :cond_4

    .line 405
    .end local v14    # "ord":J
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 410
    .restart local v14    # "ord":J
    :cond_4
    invoke-virtual {v6, v14, v15}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    goto :goto_3
.end method
