.class Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;
.super Lorg/apache/lucene/codecs/compressing/Compressor;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeflateCompressor"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field compressed:[B

.field final compressor:Ljava/util/zip/Deflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 238
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 243
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/Compressor;-><init>()V

    .line 244
    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0, p1}, Ljava/util/zip/Deflater;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    .line 245
    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    .line 246
    return-void
.end method


# virtual methods
.method public compress([BIILorg/apache/lucene/store/DataOutput;)V
    .locals 5
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .param p4, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->reset()V

    .line 251
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    invoke-virtual {v2, p1, p2, p3}, Ljava/util/zip/Deflater;->setInput([BII)V

    .line 252
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->finish()V

    .line 254
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->needsInput()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 256
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-eqz p3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, p3}, Ljava/lang/AssertionError;-><init>(I)V

    throw v2

    .line 257
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p4, v2}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 275
    :goto_0
    return-void

    .line 261
    :cond_1
    const/4 v1, 0x0

    .line 263
    .local v1, "totalCount":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    array-length v4, v4

    sub-int/2addr v4, v1

    invoke-virtual {v2, v3, v1, v4}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v0

    .line 264
    .local v0, "count":I
    add-int/2addr v1, v0

    .line 265
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    array-length v2, v2

    if-le v1, v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 266
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressor:Ljava/util/zip/Deflater;

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->finished()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 273
    invoke-virtual {p4, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 274
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    invoke-virtual {p4, v2, v1}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BI)V

    goto :goto_0

    .line 269
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([B)[B

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;->compressed:[B

    goto :goto_1
.end method
