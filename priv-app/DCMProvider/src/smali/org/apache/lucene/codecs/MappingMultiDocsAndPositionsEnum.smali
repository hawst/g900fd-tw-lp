.class public final Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "MappingMultiDocsAndPositionsEnum.java"


# instance fields
.field current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field currentBase:I

.field currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

.field doc:I

.field private mergeState:Lorg/apache/lucene/index/MergeState;

.field numSubs:I

.field private subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

.field upto:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->doc:I

    .line 47
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 86
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public cost()J
    .locals 8

    .prologue
    .line 140
    const-wide/16 v0, 0x0

    .line 141
    .local v0, "cost":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 144
    return-wide v0

    .line 141
    :cond_0
    aget-object v2, v4, v3

    .line 142
    .local v2, "enumWithSlice":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;
    iget-object v6, v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 141
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public getNumSubs()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->numSubs:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public getSubs()[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    return-object v0
.end method

.method public nextDoc()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v2, 0x7fffffff

    .line 92
    :cond_0
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    if-nez v3, :cond_2

    .line 93
    iget v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->upto:I

    iget v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->numSubs:I

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    .line 94
    iput v2, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->doc:I

    .line 111
    :goto_1
    return v2

    .line 96
    :cond_1
    iget v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->upto:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->upto:I

    .line 97
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iget v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->upto:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v1, v3, Lorg/apache/lucene/index/ReaderSlice;->readerIndex:I

    .line 98
    .local v1, "reader":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iget v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->upto:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iput-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 99
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->docBase:[I

    aget v3, v3, v1

    iput v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->currentBase:I

    .line 100
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->docMaps:[Lorg/apache/lucene/index/MergeState$DocMap;

    aget-object v3, v3, v1

    iput-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

    .line 104
    .end local v1    # "reader":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v0

    .line 105
    .local v0, "doc":I
    if-eq v0, v2, :cond_3

    .line 107
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/MergeState$DocMap;->get(I)I

    move-result v0

    .line 108
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 111
    iget v2, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->currentBase:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->doc:I

    goto :goto_1

    .line 113
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v0

    return v0
.end method

.method reset(Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;)Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;
    .locals 1
    .param p1, "postingsEnum"    # Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    .prologue
    .line 50
    invoke-virtual {p1}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->getNumSubs()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->numSubs:I

    .line 51
    invoke-virtual {p1}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->getSubs()[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->upto:I

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 54
    return-object p0
.end method

.method public setMergeState(Lorg/apache/lucene/index/MergeState;)V
    .locals 0
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;

    .prologue
    .line 60
    iput-object p1, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->mergeState:Lorg/apache/lucene/index/MergeState;

    .line 61
    return-void
.end method

.method public startOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsAndPositionsEnum;->current:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v0

    return v0
.end method
