.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "Lucene3xTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVTermsEnum"
.end annotation


# instance fields
.field private currentTerm:I

.field private numTerms:I

.field private final origTVF:Lorg/apache/lucene/store/IndexInput;

.field private storeOffsets:Z

.field private storePositions:Z

.field private termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

.field private final tvf:Lorg/apache/lucene/store/IndexInput;

.field private unicodeSortOrder:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 386
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 387
    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->origTVF:Lorg/apache/lucene/store/IndexInput;

    .line 388
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->origTVF:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    .line 389
    return-void
.end method

.method private readVectors()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 413
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->numTerms:I

    new-array v15, v15, [Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    .line 414
    new-instance v6, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v6}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 415
    .local v6, "lastTerm":Lorg/apache/lucene/util/BytesRef;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->numTerms:I

    if-lt v5, v15, :cond_0

    .line 456
    return-void

    .line 416
    :cond_0
    new-instance v13, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    invoke-direct {v13}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;-><init>()V

    .line 417
    .local v13, "t":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;
    new-instance v14, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v14}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 418
    .local v14, "term":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v14, v6}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 419
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v15}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v11

    .line 420
    .local v11, "start":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v15}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    .line 421
    .local v2, "deltaLen":I
    add-int v15, v11, v2

    iput v15, v14, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 422
    iget v15, v14, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v14, v15}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 423
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    iget-object v0, v14, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v11, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 424
    iput-object v14, v13, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->term:Lorg/apache/lucene/util/BytesRef;

    .line 425
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v15}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 426
    .local v4, "freq":I
    iput v4, v13, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->freq:I

    .line 428
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->storePositions:Z

    if-eqz v15, :cond_1

    .line 429
    new-array v10, v4, [I

    .line 430
    .local v10, "positions":[I
    const/4 v8, 0x0

    .line 431
    .local v8, "pos":I
    const/4 v9, 0x0

    .local v9, "posUpto":I
    :goto_1
    if-lt v9, v4, :cond_3

    .line 439
    iput-object v10, v13, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->positions:[I

    .line 442
    .end local v8    # "pos":I
    .end local v9    # "posUpto":I
    .end local v10    # "positions":[I
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->storeOffsets:Z

    if-eqz v15, :cond_2

    .line 443
    new-array v12, v4, [I

    .line 444
    .local v12, "startOffsets":[I
    new-array v3, v4, [I

    .line 445
    .local v3, "endOffsets":[I
    const/4 v7, 0x0

    .line 446
    .local v7, "offset":I
    const/4 v9, 0x0

    .restart local v9    # "posUpto":I
    :goto_2
    if-lt v9, v4, :cond_5

    .line 450
    iput-object v12, v13, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->startOffsets:[I

    .line 451
    iput-object v3, v13, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->endOffsets:[I

    .line 453
    .end local v3    # "endOffsets":[I
    .end local v7    # "offset":I
    .end local v9    # "posUpto":I
    .end local v12    # "startOffsets":[I
    :cond_2
    invoke-virtual {v6, v14}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 454
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    aput-object v13, v15, v5

    .line 415
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 432
    .restart local v8    # "pos":I
    .restart local v9    # "posUpto":I
    .restart local v10    # "positions":[I
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v15}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 433
    .local v1, "delta":I
    const/4 v15, -0x1

    if-ne v1, v15, :cond_4

    .line 434
    const/4 v1, 0x0

    .line 436
    :cond_4
    add-int/2addr v8, v1

    .line 437
    aput v8, v10, v9

    .line 431
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 447
    .end local v1    # "delta":I
    .end local v8    # "pos":I
    .end local v10    # "positions":[I
    .restart local v3    # "endOffsets":[I
    .restart local v7    # "offset":I
    .restart local v12    # "startOffsets":[I
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v15}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v15

    add-int/2addr v15, v7

    aput v15, v12, v9

    .line 448
    aget v15, v12, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v16

    add-int v7, v15, v16

    aput v7, v3, v9

    .line 446
    add-int/lit8 v9, v9, 0x1

    goto :goto_2
.end method


# virtual methods
.method public canReuse(Lorg/apache/lucene/store/IndexInput;)Z
    .locals 1
    .param p1, "tvf"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 392
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->origTVF:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public docFreq()I
    .locals 1

    .prologue
    .line 501
    const/4 v0, 0x1

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 3
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 512
    if-eqz p2, :cond_0

    instance-of v1, p2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 513
    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;

    .line 517
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    aget-object v1, v1, v2

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;)V

    .line 518
    return-object v0

    .line 515
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;)V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;
    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 3
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 523
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->storePositions:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->storeOffsets:Z

    if-nez v2, :cond_0

    move-object v0, v1

    .line 534
    :goto_0
    return-object v0

    .line 528
    :cond_0
    if-eqz p2, :cond_1

    instance-of v2, p2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;

    if-eqz v2, :cond_1

    move-object v0, p2

    .line 529
    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;

    .line 533
    .local v0, "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    aget-object v1, v1, v2

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;->reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;)V

    goto :goto_0

    .line 531
    .end local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;)V

    .restart local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsAndPositionsEnum;
    goto :goto_1
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 539
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->unicodeSortOrder:Z

    if-eqz v0, :cond_0

    .line 540
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 542
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 483
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->numTerms:I

    if-lt v0, v1, :cond_0

    .line 484
    const/4 v0, 0x0

    .line 486
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    goto :goto_0
.end method

.method public ord()J
    .locals 1

    .prologue
    .line 496
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public reset(IJZZZ)V
    .locals 2
    .param p1, "numTerms"    # I
    .param p2, "tvfFPStart"    # J
    .param p4, "storePositions"    # Z
    .param p5, "storeOffsets"    # Z
    .param p6, "unicodeSortOrder"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    iput p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->numTerms:I

    .line 397
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->storePositions:Z

    .line 398
    iput-boolean p5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->storeOffsets:Z

    .line 399
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    .line 400
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->tvf:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p2, p3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 401
    iput-boolean p6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->unicodeSortOrder:Z

    .line 402
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->readVectors()V

    .line 403
    if-eqz p6, :cond_0

    .line 404
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum$1;

    invoke-direct {v1, p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum$1;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 410
    :cond_0
    return-void
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 4
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 461
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v1

    .line 462
    .local v1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->numTerms:I

    if-lt v2, v3, :cond_0

    .line 472
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    array-length v3, v3

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    .line 473
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    :goto_1
    return-object v3

    .line 463
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v1, p1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 464
    .local v0, "cmp":I
    if-gez v0, :cond_1

    .line 465
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    .line 466
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_1

    .line 467
    :cond_1
    if-nez v0, :cond_2

    .line 468
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    .line 469
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_1

    .line 462
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J

    .prologue
    .line 478
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 2

    .prologue
    .line 491
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->termAndPostings:[Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVTermsEnum;->currentTerm:I

    aget-object v0, v0, v1

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->freq:I

    int-to-long v0, v0

    return-wide v0
.end method
