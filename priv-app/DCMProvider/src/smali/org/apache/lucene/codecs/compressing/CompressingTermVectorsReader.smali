.class public final Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
.super Lorg/apache/lucene/codecs/TermVectorsReader;
.source "CompressingTermVectorsReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;,
        Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;,
        Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTerms;,
        Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVTermsEnum;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final chunkSize:I

.field private closed:Z

.field private final compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field private final decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field final indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

.field private final numDocs:I

.field private final packedIntsVersion:I

.field private final reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

.field final vectorsStream:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    .prologue
    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 83
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 84
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    .line 85
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->clone()Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    .line 86
    iget v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    .line 87
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 88
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/compressing/Decompressor;->clone()Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    .line 89
    iget v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->chunkSize:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->chunkSize:I

    .line 90
    iget v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->numDocs:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->numDocs:I

    .line 91
    new-instance v0, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    const/16 v3, 0x40

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;-><init>(Lorg/apache/lucene/store/DataInput;IIJ)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->closed:Z

    .line 93
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;)V
    .locals 14
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "segmentSuffix"    # Ljava/lang/String;
    .param p4, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p5, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p6, "formatName"    # Ljava/lang/String;
    .param p7, "compressionMode"    # Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsReader;-><init>()V

    .line 98
    move-object/from16 v0, p7

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 99
    move-object/from16 v0, p2

    iget-object v12, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 100
    .local v12, "segment":Ljava/lang/String;
    const/4 v13, 0x0

    .line 101
    .local v13, "success":Z
    move-object/from16 v0, p4

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 102
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->numDocs:I

    .line 103
    const/4 v10, 0x0

    .line 105
    .local v10, "indexStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    const-string v2, "tvd"

    move-object/from16 v0, p3

    invoke-static {v12, v0, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {p1, v2, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    .line 106
    const-string v2, "tvx"

    move-object/from16 v0, p3

    invoke-static {v12, v0, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 107
    .local v11, "indexStreamFN":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {p1, v11, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v10

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Index"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 110
    .local v9, "codecNameIdx":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "Data"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 111
    .local v8, "codecNameDat":Ljava/lang/String;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v10, v9, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 112
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v8, v3, v4}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 113
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-static {v8}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    .end local v8    # "codecNameDat":Ljava/lang/String;
    .end local v9    # "codecNameIdx":Ljava/lang/String;
    .end local v11    # "indexStreamFN":Ljava/lang/String;
    :catchall_0
    move-exception v2

    .line 126
    if-nez v13, :cond_0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/io/Closeable;

    const/4 v4, 0x0

    .line 127
    aput-object p0, v3, v4

    const/4 v4, 0x1

    aput-object v10, v3, v4

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 129
    :cond_0
    throw v2

    .line 114
    .restart local v8    # "codecNameDat":Ljava/lang/String;
    .restart local v9    # "codecNameIdx":Ljava/lang/String;
    .restart local v11    # "indexStreamFN":Ljava/lang/String;
    :cond_1
    :try_start_1
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    invoke-static {v9}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v10}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 116
    :cond_2
    new-instance v2, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-object/from16 v0, p2

    invoke-direct {v2, v10, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/SegmentInfo;)V

    iput-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    .line 117
    const/4 v10, 0x0

    .line 119
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    .line 120
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->chunkSize:I

    .line 121
    invoke-virtual/range {p7 .. p7}, Lorg/apache/lucene/codecs/compressing/CompressionMode;->newDecompressor()Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    .line 122
    new-instance v2, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    const/16 v5, 0x40

    const-wide/16 v6, 0x0

    invoke-direct/range {v2 .. v7}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;-><init>(Lorg/apache/lucene/store/DataInput;IIJ)V

    iput-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    const/4 v13, 0x1

    .line 126
    if-nez v13, :cond_3

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v3, 0x0

    .line 127
    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object v10, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 130
    :cond_3
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    return-object v0
.end method

.method private ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 156
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->closed:Z

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this FieldsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    return-void
.end method

.method private positionIndex(IILorg/apache/lucene/util/packed/PackedInts$Reader;[I)[[I
    .locals 9
    .param p1, "skip"    # I
    .param p2, "numFields"    # I
    .param p3, "numTerms"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .param p4, "termFreqs"    # [I

    .prologue
    .line 533
    new-array v3, p2, [[I

    .line 534
    .local v3, "positionIndex":[[I
    const/4 v5, 0x0

    .line 535
    .local v5, "termIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_0

    .line 539
    const/4 v1, 0x0

    :goto_1
    if-lt v1, p2, :cond_1

    .line 548
    return-object v3

    .line 536
    :cond_0
    invoke-interface {p3, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v6

    long-to-int v4, v6

    .line 537
    .local v4, "termCount":I
    add-int/2addr v5, v4

    .line 535
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 540
    .end local v4    # "termCount":I
    :cond_1
    add-int v6, p1, v1

    invoke-interface {p3, v6}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v6

    long-to-int v4, v6

    .line 541
    .restart local v4    # "termCount":I
    add-int/lit8 v6, v4, 0x1

    new-array v6, v6, [I

    aput-object v6, v3, v1

    .line 542
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    if-lt v2, v4, :cond_2

    .line 546
    add-int/2addr v5, v4

    .line 539
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 543
    :cond_2
    add-int v6, v5, v2

    aget v0, p4, v6

    .line 544
    .local v0, "freq":I
    aget-object v6, v3, v1

    add-int/lit8 v7, v2, 0x1

    aget-object v8, v3, v1

    aget v8, v8, v2

    add-int/2addr v8, v0

    aput v8, v6, v7

    .line 542
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method private readPositions(IILorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;[III[[I)[[I
    .locals 22
    .param p1, "skip"    # I
    .param p2, "numFields"    # I
    .param p3, "flags"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .param p4, "numTerms"    # Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .param p5, "termFreqs"    # [I
    .param p6, "flag"    # I
    .param p7, "totalPositions"    # I
    .param p8, "positionIndex"    # [[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 552
    move/from16 v0, p2

    new-array v12, v0, [[I

    .line 553
    .local v12, "positions":[[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v18, v0

    move/from16 v0, p7

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 555
    const/4 v15, 0x0

    .line 556
    .local v15, "toSkip":I
    const/4 v14, 0x0

    .line 557
    .local v14, "termIndex":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move/from16 v0, p1

    if-lt v7, v0, :cond_0

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v17, v0

    int-to-long v0, v15

    move-wide/from16 v18, v0

    invoke-virtual/range {v17 .. v19}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->skip(J)V

    .line 570
    const/4 v7, 0x0

    :goto_1
    move/from16 v0, p2

    if-lt v7, v0, :cond_3

    .line 586
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v17, v0

    move/from16 v0, p7

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord()J

    move-result-wide v20

    sub-long v18, v18, v20

    invoke-virtual/range {v17 .. v19}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->skip(J)V

    .line 587
    return-object v12

    .line 558
    :cond_0
    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v4, v0

    .line 559
    .local v4, "f":I
    move-object/from16 v0, p4

    invoke-interface {v0, v7}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 560
    .local v13, "termCount":I
    and-int v17, v4, p6

    if-eqz v17, :cond_1

    .line 561
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    if-lt v8, v13, :cond_2

    .line 566
    .end local v8    # "j":I
    :cond_1
    add-int/2addr v14, v13

    .line 557
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 562
    .restart local v8    # "j":I
    :cond_2
    add-int v17, v14, v8

    aget v6, p5, v17

    .line 563
    .local v6, "freq":I
    add-int/2addr v15, v6

    .line 561
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 571
    .end local v4    # "f":I
    .end local v6    # "freq":I
    .end local v8    # "j":I
    .end local v13    # "termCount":I
    :cond_3
    add-int v17, p1, v7

    move-object/from16 v0, p3

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v4, v0

    .line 572
    .restart local v4    # "f":I
    add-int v17, p1, v7

    move-object/from16 v0, p4

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v13, v0

    .line 573
    .restart local v13    # "termCount":I
    and-int v17, v4, p6

    if-eqz v17, :cond_5

    .line 574
    aget-object v17, p8, v7

    aget v16, v17, v13

    .line 575
    .local v16, "totalFreq":I
    move/from16 v0, v16

    new-array v5, v0, [I

    .line 576
    .local v5, "fieldPositions":[I
    aput-object v5, v12, v7

    .line 577
    const/4 v8, 0x0

    .restart local v8    # "j":I
    :cond_4
    move/from16 v0, v16

    if-lt v8, v0, :cond_6

    .line 584
    .end local v5    # "fieldPositions":[I
    .end local v8    # "j":I
    .end local v16    # "totalFreq":I
    :cond_5
    add-int/2addr v14, v13

    .line 570
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 578
    .restart local v5    # "fieldPositions":[I
    .restart local v8    # "j":I
    .restart local v16    # "totalFreq":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v17, v0

    sub-int v18, v16, v8

    invoke-virtual/range {v17 .. v18}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next(I)Lorg/apache/lucene/util/LongsRef;

    move-result-object v11

    .line 579
    .local v11, "nextPositions":Lorg/apache/lucene/util/LongsRef;
    const/4 v10, 0x0

    .local v10, "k":I
    :goto_3
    iget v0, v11, Lorg/apache/lucene/util/LongsRef;->length:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v10, v0, :cond_4

    .line 580
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "j":I
    .local v9, "j":I
    iget-object v0, v11, Lorg/apache/lucene/util/LongsRef;->longs:[J

    move-object/from16 v17, v0

    iget v0, v11, Lorg/apache/lucene/util/LongsRef;->offset:I

    move/from16 v18, v0

    add-int v18, v18, v10

    aget-wide v18, v17, v18

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v17, v0

    aput v17, v5, v8

    .line 579
    add-int/lit8 v10, v10, 0x1

    move v8, v9

    .end local v9    # "j":I
    .restart local v8    # "j":I
    goto :goto_3
.end method

.method private static sum([I)I
    .locals 4
    .param p0, "arr"    # [I

    .prologue
    .line 1043
    const/4 v1, 0x0

    .line 1044
    .local v1, "sum":I
    array-length v3, p0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 1047
    return v1

    .line 1044
    :cond_0
    aget v0, p0, v2

    .line 1045
    .local v0, "el":I
    add-int/2addr v1, v0

    .line 1044
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/TermVectorsReader;
    .locals 1

    .prologue
    .line 171
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;)V

    return-object v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 163
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->closed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 164
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    aput-object v1, v0, v3

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 165
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->closed:Z

    .line 167
    :cond_0
    return-void
.end method

.method public get(I)Lorg/apache/lucene/index/Fields;
    .locals 92
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->ensureOpen()V

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v76

    .line 181
    .local v76, "startPointer":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move-wide/from16 v0, v76

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 187
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v46

    .line 188
    .local v46, "docBase":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v45

    .line 189
    .local v45, "chunkDocs":I
    move/from16 v0, p1

    move/from16 v1, v46

    if-lt v0, v1, :cond_0

    add-int v2, v46, v45

    move/from16 v0, p1

    if-ge v0, v2, :cond_0

    add-int v2, v46, v45

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->numDocs:I

    if-le v2, v3, :cond_1

    .line 190
    :cond_0
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "docBase="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v46

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",chunkDocs="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v45

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",doc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 196
    :cond_1
    const/4 v2, 0x1

    move/from16 v0, v45

    if-ne v0, v2, :cond_2

    .line 197
    const/4 v8, 0x0

    .line 198
    .local v8, "skip":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v86

    .local v86, "totalFields":I
    move/from16 v9, v86

    .line 214
    .local v9, "numFields":I
    :goto_0
    if-nez v9, :cond_5

    .line 216
    const/16 v24, 0x0

    .line 524
    :goto_1
    return-object v24

    .line 200
    .end local v8    # "skip":I
    .end local v9    # "numFields":I
    .end local v86    # "totalFields":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v45

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 201
    const/16 v75, 0x0

    .line 202
    .local v75, "sum":I
    move/from16 v63, v46

    .local v63, "i":I
    :goto_2
    move/from16 v0, v63

    move/from16 v1, p1

    if-lt v0, v1, :cond_3

    .line 205
    move/from16 v8, v75

    .line 206
    .restart local v8    # "skip":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v2

    long-to-int v9, v2

    .line 207
    .restart local v9    # "numFields":I
    add-int v75, v75, v9

    .line 208
    add-int/lit8 v63, p1, 0x1

    :goto_3
    add-int v2, v46, v45

    move/from16 v0, v63

    if-lt v0, v2, :cond_4

    .line 211
    move/from16 v86, v75

    .restart local v86    # "totalFields":I
    goto :goto_0

    .line 203
    .end local v8    # "skip":I
    .end local v9    # "numFields":I
    .end local v86    # "totalFields":I
    :cond_3
    move/from16 v0, v75

    int-to-long v2, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v75, v0

    .line 202
    add-int/lit8 v63, v63, 0x1

    goto :goto_2

    .line 209
    .restart local v8    # "skip":I
    .restart local v9    # "numFields":I
    :cond_4
    move/from16 v0, v75

    int-to-long v2, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v75, v0

    .line 208
    add-int/lit8 v63, v63, 0x1

    goto :goto_3

    .line 222
    .end local v63    # "i":I
    .end local v75    # "sum":I
    .restart local v86    # "totalFields":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v2

    and-int/lit16 v0, v2, 0xff

    move/from16 v85, v0

    .line 223
    .local v85, "token":I
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_6

    if-nez v85, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 224
    :cond_6
    and-int/lit8 v6, v85, 0x1f

    .line 225
    .local v6, "bitsPerFieldNum":I
    ushr-int/lit8 v5, v85, 0x5

    .line 226
    .local v5, "totalDistinctFields":I
    const/4 v2, 0x7

    if-ne v5, v2, :cond_7

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    add-int/2addr v5, v2

    .line 229
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 230
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderIteratorNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;IIII)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;

    move-result-object v65

    .line 231
    .local v65, "it":Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    new-array v0, v5, [I

    move-object/from16 v26, v0

    .line 232
    .local v26, "fieldNums":[I
    const/16 v63, 0x0

    .restart local v63    # "i":I
    :goto_4
    move/from16 v0, v63

    if-lt v0, v5, :cond_8

    .line 238
    new-array v0, v9, [I

    move-object/from16 v28, v0

    .line 241
    .local v28, "fieldNumOffs":[I
    move-object/from16 v0, v26

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    invoke-static {v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v42

    .line 242
    .local v42, "bitsPerOff":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    move/from16 v0, v86

    move/from16 v1, v42

    invoke-static {v2, v3, v4, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v41

    .line 243
    .local v41, "allFieldNumOffs":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 259
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 233
    .end local v28    # "fieldNumOffs":[I
    .end local v41    # "allFieldNumOffs":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v42    # "bitsPerOff":I
    :cond_8
    invoke-interface/range {v65 .. v65}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;->next()J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v26, v63

    .line 232
    add-int/lit8 v63, v63, 0x1

    goto :goto_4

    .line 245
    .restart local v28    # "fieldNumOffs":[I
    .restart local v41    # "allFieldNumOffs":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v42    # "bitsPerOff":I
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    move-object/from16 v0, v26

    array-length v7, v0

    sget v13, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->FLAGS_BITS:I

    invoke-static {v2, v3, v4, v7, v13}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v27

    .line 246
    .local v27, "fieldFlags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    sget v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->FLAGS_BITS:I

    const/4 v3, 0x0

    move/from16 v0, v86

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/util/packed/PackedInts;->getMutable(IIF)Lorg/apache/lucene/util/packed/PackedInts$Mutable;

    move-result-object v50

    .line 247
    .local v50, "f":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    const/16 v63, 0x0

    :goto_5
    move/from16 v0, v63

    move/from16 v1, v86

    if-lt v0, v1, :cond_b

    .line 253
    move-object/from16 v10, v50

    .line 261
    .end local v27    # "fieldFlags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v50    # "f":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .local v10, "flags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    :goto_6
    const/16 v63, 0x0

    :goto_7
    move/from16 v0, v63

    if-lt v0, v9, :cond_e

    .line 270
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v43

    .line 271
    .local v43, "bitsRequired":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    move/from16 v0, v86

    move/from16 v1, v43

    invoke-static {v2, v3, v4, v0, v1}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v11

    .line 272
    .local v11, "numTerms":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    const/16 v75, 0x0

    .line 273
    .restart local v75    # "sum":I
    const/16 v63, 0x0

    :goto_8
    move/from16 v0, v63

    move/from16 v1, v86

    if-lt v0, v1, :cond_f

    .line 276
    move/from16 v91, v75

    .line 280
    .local v91, "totalTerms":I
    const/16 v48, 0x0

    .local v48, "docOff":I
    const/16 v47, 0x0

    .line 281
    .local v47, "docLen":I
    new-array v0, v9, [I

    move-object/from16 v30, v0

    .line 282
    .local v30, "fieldLengths":[I
    new-array v0, v9, [[I

    move-object/from16 v31, v0

    .line 283
    .local v31, "prefixLengths":[[I
    new-array v0, v9, [[I

    move-object/from16 v32, v0

    .line 285
    .local v32, "suffixLengths":[[I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v91

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 287
    const/16 v84, 0x0

    .line 288
    .local v84, "toSkip":I
    const/16 v63, 0x0

    :goto_9
    move/from16 v0, v63

    if-lt v0, v8, :cond_10

    .line 291
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move/from16 v0, v84

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->skip(J)V

    .line 293
    const/16 v63, 0x0

    :goto_a
    move/from16 v0, v63

    if-lt v0, v9, :cond_11

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move/from16 v0, v91

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->ord()J

    move-result-wide v18

    sub-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->skip(J)V

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v91

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 308
    const/16 v84, 0x0

    .line 309
    const/16 v63, 0x0

    :goto_b
    move/from16 v0, v63

    if-lt v0, v8, :cond_14

    .line 314
    const/16 v63, 0x0

    :goto_c
    move/from16 v0, v63

    if-lt v0, v9, :cond_16

    .line 327
    add-int v88, v48, v47

    .line 328
    .local v88, "totalLen":I
    add-int v63, v8, v9

    :goto_d
    move/from16 v0, v63

    move/from16 v1, v86

    if-lt v0, v1, :cond_19

    .line 336
    move/from16 v0, v91

    new-array v12, v0, [I

    .line 338
    .local v12, "termFreqs":[I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v91

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 339
    const/16 v63, 0x0

    :cond_9
    move/from16 v0, v63

    move/from16 v1, v91

    if-lt v0, v1, :cond_1b

    .line 348
    const/4 v14, 0x0

    .local v14, "totalPositions":I
    const/16 v23, 0x0

    .local v23, "totalOffsets":I
    const/16 v90, 0x0

    .line 349
    .local v90, "totalPayloads":I
    const/16 v63, 0x0

    const/16 v81, 0x0

    .local v81, "termIndex":I
    :goto_e
    move/from16 v0, v63

    move/from16 v1, v86

    if-lt v0, v1, :cond_1c

    .line 367
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v11, v12}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->positionIndex(IILorg/apache/lucene/util/packed/PackedInts$Reader;[I)[[I

    move-result-object v15

    .line 369
    .local v15, "positionIndex":[[I
    if-lez v14, :cond_22

    .line 370
    const/4 v13, 0x1

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v15}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->readPositions(IILorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;[III[[I)[[I

    move-result-object v35

    .line 375
    .local v35, "positions":[[I
    :goto_f
    if-lez v23, :cond_2a

    .line 377
    move-object/from16 v0, v26

    array-length v2, v0

    new-array v0, v2, [F

    move-object/from16 v44, v0

    .line 378
    .local v44, "charsPerTerm":[F
    const/16 v63, 0x0

    :goto_10
    move-object/from16 v0, v44

    array-length v2, v0

    move/from16 v0, v63

    if-lt v0, v2, :cond_23

    .line 381
    const/16 v22, 0x2

    move-object/from16 v16, p0

    move/from16 v17, v8

    move/from16 v18, v9

    move-object/from16 v19, v10

    move-object/from16 v20, v11

    move-object/from16 v21, v12

    move-object/from16 v24, v15

    invoke-direct/range {v16 .. v24}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->readPositions(IILorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;[III[[I)[[I

    move-result-object v36

    .line 382
    .local v36, "startOffsets":[[I
    const/16 v22, 0x2

    move-object/from16 v16, p0

    move/from16 v17, v8

    move/from16 v18, v9

    move-object/from16 v19, v10

    move-object/from16 v20, v11

    move-object/from16 v21, v12

    move-object/from16 v24, v15

    invoke-direct/range {v16 .. v24}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->readPositions(IILorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;[III[[I)[[I

    move-result-object v37

    .line 384
    .local v37, "lengths":[[I
    const/16 v63, 0x0

    :goto_11
    move/from16 v0, v63

    if-lt v0, v9, :cond_24

    .line 412
    .end local v44    # "charsPerTerm":[F
    :goto_12
    if-lez v14, :cond_a

    .line 414
    const/16 v63, 0x0

    :goto_13
    move/from16 v0, v63

    if-lt v0, v9, :cond_2b

    .line 429
    :cond_a
    new-array v0, v9, [[I

    move-object/from16 v39, v0

    .line 430
    .local v39, "payloadIndex":[[I
    const/16 v89, 0x0

    .line 431
    .local v89, "totalPayloadLength":I
    const/16 v73, 0x0

    .line 432
    .local v73, "payloadOff":I
    const/16 v71, 0x0

    .line 433
    .local v71, "payloadLen":I
    if-lez v90, :cond_3b

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move/from16 v0, v90

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->reset(Lorg/apache/lucene/store/DataInput;J)V

    .line 436
    const/16 v81, 0x0

    .line 437
    const/16 v63, 0x0

    :goto_14
    move/from16 v0, v63

    if-lt v0, v8, :cond_2f

    .line 451
    move/from16 v89, v73

    .line 453
    const/16 v63, 0x0

    :goto_15
    move/from16 v0, v63

    if-lt v0, v9, :cond_33

    .line 474
    add-int v89, v89, v71

    .line 475
    add-int v63, v8, v9

    :goto_16
    move/from16 v0, v63

    move/from16 v1, v86

    if-lt v0, v1, :cond_37

    .line 488
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_3b

    move/from16 v0, v81

    move/from16 v1, v91

    if-eq v0, v1, :cond_3b

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v81 .. v81}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v91

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 248
    .end local v10    # "flags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v11    # "numTerms":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v12    # "termFreqs":[I
    .end local v14    # "totalPositions":I
    .end local v15    # "positionIndex":[[I
    .end local v23    # "totalOffsets":I
    .end local v30    # "fieldLengths":[I
    .end local v31    # "prefixLengths":[[I
    .end local v32    # "suffixLengths":[[I
    .end local v35    # "positions":[[I
    .end local v36    # "startOffsets":[[I
    .end local v37    # "lengths":[[I
    .end local v39    # "payloadIndex":[[I
    .end local v43    # "bitsRequired":I
    .end local v47    # "docLen":I
    .end local v48    # "docOff":I
    .end local v71    # "payloadLen":I
    .end local v73    # "payloadOff":I
    .end local v75    # "sum":I
    .end local v81    # "termIndex":I
    .end local v84    # "toSkip":I
    .end local v88    # "totalLen":I
    .end local v89    # "totalPayloadLength":I
    .end local v90    # "totalPayloads":I
    .end local v91    # "totalTerms":I
    .restart local v27    # "fieldFlags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v50    # "f":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    :cond_b
    move-object/from16 v0, v41

    move/from16 v1, v63

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v58, v0

    .line 249
    .local v58, "fieldNumOff":I
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_d

    if-ltz v58, :cond_c

    move-object/from16 v0, v26

    array-length v2, v0

    move/from16 v0, v58

    if-lt v0, v2, :cond_d

    :cond_c
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 250
    :cond_d
    move-object/from16 v0, v27

    move/from16 v1, v58

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v56, v0

    .line 251
    .local v56, "fgs":I
    move/from16 v0, v56

    int-to-long v2, v0

    move-object/from16 v0, v50

    move/from16 v1, v63

    invoke-interface {v0, v1, v2, v3}, Lorg/apache/lucene/util/packed/PackedInts$Mutable;->set(IJ)V

    .line 247
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_5

    .line 256
    .end local v27    # "fieldFlags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v50    # "f":Lorg/apache/lucene/util/packed/PackedInts$Mutable;
    .end local v56    # "fgs":I
    .end local v58    # "fieldNumOff":I
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    sget v7, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsWriter;->FLAGS_BITS:I

    move/from16 v0, v86

    invoke-static {v2, v3, v4, v0, v7}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v10

    .line 257
    .restart local v10    # "flags":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    goto/16 :goto_6

    .line 262
    :cond_e
    add-int v2, v8, v63

    move-object/from16 v0, v41

    invoke-interface {v0, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v28, v63

    .line 261
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_7

    .line 274
    .restart local v11    # "numTerms":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v43    # "bitsRequired":I
    .restart local v75    # "sum":I
    :cond_f
    move/from16 v0, v75

    int-to-long v2, v0

    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v75, v0

    .line 273
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_8

    .line 289
    .restart local v30    # "fieldLengths":[I
    .restart local v31    # "prefixLengths":[[I
    .restart local v32    # "suffixLengths":[[I
    .restart local v47    # "docLen":I
    .restart local v48    # "docOff":I
    .restart local v84    # "toSkip":I
    .restart local v91    # "totalTerms":I
    :cond_10
    move/from16 v0, v84

    int-to-long v2, v0

    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v84, v0

    .line 288
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_9

    .line 294
    :cond_11
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 295
    .local v78, "termCount":I
    move/from16 v0, v78

    new-array v0, v0, [I

    move-object/from16 v59, v0

    .line 296
    .local v59, "fieldPrefixLengths":[I
    aput-object v59, v31, v63

    .line 297
    const/16 v66, 0x0

    .local v66, "j":I
    :cond_12
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_13

    .line 293
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_a

    .line 298
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    sub-int v3, v78, v66

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next(I)Lorg/apache/lucene/util/LongsRef;

    move-result-object v70

    .line 299
    .local v70, "next":Lorg/apache/lucene/util/LongsRef;
    const/16 v68, 0x0

    .local v68, "k":I
    :goto_17
    move-object/from16 v0, v70

    iget v2, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    move/from16 v0, v68

    if-ge v0, v2, :cond_12

    .line 300
    add-int/lit8 v67, v66, 0x1

    .end local v66    # "j":I
    .local v67, "j":I
    move-object/from16 v0, v70

    iget-object v2, v0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    move-object/from16 v0, v70

    iget v3, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    add-int v3, v3, v68

    aget-wide v2, v2, v3

    long-to-int v2, v2

    aput v2, v59, v66

    .line 299
    add-int/lit8 v68, v68, 0x1

    move/from16 v66, v67

    .end local v67    # "j":I
    .restart local v66    # "j":I
    goto :goto_17

    .line 310
    .end local v59    # "fieldPrefixLengths":[I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .end local v70    # "next":Lorg/apache/lucene/util/LongsRef;
    .end local v78    # "termCount":I
    :cond_14
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :goto_18
    move/from16 v0, v66

    int-to-long v2, v0

    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v16

    cmp-long v2, v2, v16

    if-ltz v2, :cond_15

    .line 309
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_b

    .line 311
    :cond_15
    move/from16 v0, v48

    int-to-long v2, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v48, v0

    .line 310
    add-int/lit8 v66, v66, 0x1

    goto :goto_18

    .line 315
    .end local v66    # "j":I
    :cond_16
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 316
    .restart local v78    # "termCount":I
    move/from16 v0, v78

    new-array v0, v0, [I

    move-object/from16 v60, v0

    .line 317
    .local v60, "fieldSuffixLengths":[I
    aput-object v60, v32, v63

    .line 318
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :cond_17
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_18

    .line 324
    aget-object v2, v32, v63

    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->sum([I)I

    move-result v2

    aput v2, v30, v63

    .line 325
    aget v2, v30, v63

    add-int v47, v47, v2

    .line 314
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_c

    .line 319
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    sub-int v3, v78, v66

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next(I)Lorg/apache/lucene/util/LongsRef;

    move-result-object v70

    .line 320
    .restart local v70    # "next":Lorg/apache/lucene/util/LongsRef;
    const/16 v68, 0x0

    .restart local v68    # "k":I
    :goto_19
    move-object/from16 v0, v70

    iget v2, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    move/from16 v0, v68

    if-ge v0, v2, :cond_17

    .line 321
    add-int/lit8 v67, v66, 0x1

    .end local v66    # "j":I
    .restart local v67    # "j":I
    move-object/from16 v0, v70

    iget-object v2, v0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    move-object/from16 v0, v70

    iget v3, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    add-int v3, v3, v68

    aget-wide v2, v2, v3

    long-to-int v2, v2

    aput v2, v60, v66

    .line 320
    add-int/lit8 v68, v68, 0x1

    move/from16 v66, v67

    .end local v67    # "j":I
    .restart local v66    # "j":I
    goto :goto_19

    .line 329
    .end local v60    # "fieldSuffixLengths":[I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .end local v70    # "next":Lorg/apache/lucene/util/LongsRef;
    .end local v78    # "termCount":I
    .restart local v88    # "totalLen":I
    :cond_19
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :goto_1a
    move/from16 v0, v66

    int-to-long v2, v0

    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v16

    cmp-long v2, v2, v16

    if-ltz v2, :cond_1a

    .line 328
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_d

    .line 330
    :cond_1a
    move/from16 v0, v88

    int-to-long v2, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v88, v0

    .line 329
    add-int/lit8 v66, v66, 0x1

    goto :goto_1a

    .line 340
    .end local v66    # "j":I
    .restart local v12    # "termFreqs":[I
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    sub-int v3, v91, v63

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next(I)Lorg/apache/lucene/util/LongsRef;

    move-result-object v70

    .line 341
    .restart local v70    # "next":Lorg/apache/lucene/util/LongsRef;
    const/16 v68, 0x0

    .restart local v68    # "k":I
    :goto_1b
    move-object/from16 v0, v70

    iget v2, v0, Lorg/apache/lucene/util/LongsRef;->length:I

    move/from16 v0, v68

    if-ge v0, v2, :cond_9

    .line 342
    add-int/lit8 v64, v63, 0x1

    .end local v63    # "i":I
    .local v64, "i":I
    move-object/from16 v0, v70

    iget-object v2, v0, Lorg/apache/lucene/util/LongsRef;->longs:[J

    move-object/from16 v0, v70

    iget v3, v0, Lorg/apache/lucene/util/LongsRef;->offset:I

    add-int v3, v3, v68

    aget-wide v2, v2, v3

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    aput v2, v12, v63

    .line 341
    add-int/lit8 v68, v68, 0x1

    move/from16 v63, v64

    .end local v64    # "i":I
    .restart local v63    # "i":I
    goto :goto_1b

    .line 350
    .end local v68    # "k":I
    .end local v70    # "next":Lorg/apache/lucene/util/LongsRef;
    .restart local v14    # "totalPositions":I
    .restart local v23    # "totalOffsets":I
    .restart local v81    # "termIndex":I
    .restart local v90    # "totalPayloads":I
    :cond_1c
    move/from16 v0, v63

    invoke-interface {v10, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v50, v0

    .line 351
    .local v50, "f":I
    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 352
    .restart local v78    # "termCount":I
    const/16 v66, 0x0

    .restart local v66    # "j":I
    move/from16 v82, v81

    .end local v81    # "termIndex":I
    .local v82, "termIndex":I
    :goto_1c
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_1d

    .line 364
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_21

    add-int/lit8 v2, v86, -0x1

    move/from16 v0, v63

    if-ne v0, v2, :cond_21

    move/from16 v0, v82

    move/from16 v1, v91

    if-eq v0, v1, :cond_21

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v82 .. v82}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v91

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 353
    :cond_1d
    add-int/lit8 v81, v82, 0x1

    .end local v82    # "termIndex":I
    .restart local v81    # "termIndex":I
    aget v62, v12, v82

    .line 354
    .local v62, "freq":I
    and-int/lit8 v2, v50, 0x1

    if-eqz v2, :cond_1e

    .line 355
    add-int v14, v14, v62

    .line 357
    :cond_1e
    and-int/lit8 v2, v50, 0x2

    if-eqz v2, :cond_1f

    .line 358
    add-int v23, v23, v62

    .line 360
    :cond_1f
    and-int/lit8 v2, v50, 0x4

    if-eqz v2, :cond_20

    .line 361
    add-int v90, v90, v62

    .line 352
    :cond_20
    add-int/lit8 v66, v66, 0x1

    move/from16 v82, v81

    .end local v81    # "termIndex":I
    .restart local v82    # "termIndex":I
    goto :goto_1c

    .line 349
    .end local v62    # "freq":I
    :cond_21
    add-int/lit8 v63, v63, 0x1

    move/from16 v81, v82

    .end local v82    # "termIndex":I
    .restart local v81    # "termIndex":I
    goto/16 :goto_e

    .line 372
    .end local v50    # "f":I
    .end local v66    # "j":I
    .end local v78    # "termCount":I
    .restart local v15    # "positionIndex":[[I
    :cond_22
    new-array v0, v9, [[I

    move-object/from16 v35, v0

    .restart local v35    # "positions":[[I
    goto/16 :goto_f

    .line 379
    .restart local v44    # "charsPerTerm":[F
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v2

    aput v2, v44, v63

    .line 378
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_10

    .line 385
    .restart local v36    # "startOffsets":[[I
    .restart local v37    # "lengths":[[I
    :cond_24
    aget-object v54, v36, v63

    .line 386
    .local v54, "fStartOffsets":[I
    aget-object v52, v35, v63

    .line 388
    .local v52, "fPositions":[I
    if-eqz v54, :cond_25

    if-eqz v52, :cond_25

    .line 389
    aget v2, v28, v63

    aget v57, v44, v2

    .line 390
    .local v57, "fieldCharsPerTerm":F
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :goto_1d
    aget-object v2, v36, v63

    array-length v2, v2

    move/from16 v0, v66

    if-lt v0, v2, :cond_27

    .line 394
    .end local v57    # "fieldCharsPerTerm":F
    .end local v66    # "j":I
    :cond_25
    if-eqz v54, :cond_26

    .line 395
    aget-object v53, v31, v63

    .line 396
    .local v53, "fPrefixLengths":[I
    aget-object v55, v32, v63

    .line 397
    .local v55, "fSuffixLengths":[I
    aget-object v51, v37, v63

    .line 398
    .local v51, "fLengths":[I
    const/16 v66, 0x0

    .restart local v66    # "j":I
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v49, v0

    .local v49, "end":I
    :goto_1e
    move/from16 v0, v66

    move/from16 v1, v49

    if-lt v0, v1, :cond_28

    .line 384
    .end local v49    # "end":I
    .end local v51    # "fLengths":[I
    .end local v53    # "fPrefixLengths":[I
    .end local v55    # "fSuffixLengths":[I
    .end local v66    # "j":I
    :cond_26
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_11

    .line 391
    .restart local v57    # "fieldCharsPerTerm":F
    .restart local v66    # "j":I
    :cond_27
    aget v2, v54, v66

    aget v3, v52, v66

    int-to-float v3, v3

    mul-float v3, v3, v57

    float-to-int v3, v3

    add-int/2addr v2, v3

    aput v2, v54, v66

    .line 390
    add-int/lit8 v66, v66, 0x1

    goto :goto_1d

    .line 400
    .end local v57    # "fieldCharsPerTerm":F
    .restart local v49    # "end":I
    .restart local v51    # "fLengths":[I
    .restart local v53    # "fPrefixLengths":[I
    .restart local v55    # "fSuffixLengths":[I
    :cond_28
    aget v2, v53, v66

    aget v3, v55, v66

    add-int v83, v2, v3

    .line 401
    .local v83, "termLength":I
    aget-object v2, v37, v63

    aget-object v3, v15, v63

    aget v3, v3, v66

    aget v4, v2, v3

    add-int v4, v4, v83

    aput v4, v2, v3

    .line 402
    aget-object v2, v15, v63

    aget v2, v2, v66

    add-int/lit8 v68, v2, 0x1

    .restart local v68    # "k":I
    :goto_1f
    aget-object v2, v15, v63

    add-int/lit8 v3, v66, 0x1

    aget v2, v2, v3

    move/from16 v0, v68

    if-lt v0, v2, :cond_29

    .line 398
    add-int/lit8 v66, v66, 0x1

    goto :goto_1e

    .line 403
    :cond_29
    aget v2, v54, v68

    add-int/lit8 v3, v68, -0x1

    aget v3, v54, v3

    add-int/2addr v2, v3

    aput v2, v54, v68

    .line 404
    aget v2, v51, v68

    add-int v2, v2, v83

    aput v2, v51, v68

    .line 402
    add-int/lit8 v68, v68, 0x1

    goto :goto_1f

    .line 410
    .end local v36    # "startOffsets":[[I
    .end local v37    # "lengths":[[I
    .end local v44    # "charsPerTerm":[F
    .end local v49    # "end":I
    .end local v51    # "fLengths":[I
    .end local v52    # "fPositions":[I
    .end local v53    # "fPrefixLengths":[I
    .end local v54    # "fStartOffsets":[I
    .end local v55    # "fSuffixLengths":[I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .end local v83    # "termLength":I
    :cond_2a
    new-array v0, v9, [[I

    move-object/from16 v37, v0

    .restart local v37    # "lengths":[[I
    move-object/from16 v36, v37

    .restart local v36    # "startOffsets":[[I
    goto/16 :goto_12

    .line 415
    :cond_2b
    aget-object v52, v35, v63

    .line 416
    .restart local v52    # "fPositions":[I
    aget-object v61, v15, v63

    .line 417
    .local v61, "fpositionIndex":[I
    if-eqz v52, :cond_2c

    .line 418
    const/16 v66, 0x0

    .restart local v66    # "j":I
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v49, v0

    .restart local v49    # "end":I
    :goto_20
    move/from16 v0, v66

    move/from16 v1, v49

    if-lt v0, v1, :cond_2d

    .line 414
    .end local v49    # "end":I
    .end local v66    # "j":I
    :cond_2c
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_13

    .line 420
    .restart local v49    # "end":I
    .restart local v66    # "j":I
    :cond_2d
    aget v2, v61, v66

    add-int/lit8 v68, v2, 0x1

    .restart local v68    # "k":I
    :goto_21
    add-int/lit8 v2, v66, 0x1

    aget v2, v61, v2

    move/from16 v0, v68

    if-lt v0, v2, :cond_2e

    .line 418
    add-int/lit8 v66, v66, 0x1

    goto :goto_20

    .line 421
    :cond_2e
    aget v2, v52, v68

    add-int/lit8 v3, v68, -0x1

    aget v3, v52, v3

    add-int/2addr v2, v3

    aput v2, v52, v68

    .line 420
    add-int/lit8 v68, v68, 0x1

    goto :goto_21

    .line 438
    .end local v49    # "end":I
    .end local v52    # "fPositions":[I
    .end local v61    # "fpositionIndex":[I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .restart local v39    # "payloadIndex":[[I
    .restart local v71    # "payloadLen":I
    .restart local v73    # "payloadOff":I
    .restart local v89    # "totalPayloadLength":I
    :cond_2f
    move/from16 v0, v63

    invoke-interface {v10, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v50, v0

    .line 439
    .restart local v50    # "f":I
    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 440
    .restart local v78    # "termCount":I
    and-int/lit8 v2, v50, 0x4

    if-eqz v2, :cond_30

    .line 441
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :goto_22
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_31

    .line 449
    .end local v66    # "j":I
    :cond_30
    add-int v81, v81, v78

    .line 437
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_14

    .line 442
    .restart local v66    # "j":I
    :cond_31
    add-int v2, v81, v66

    aget v62, v12, v2

    .line 443
    .restart local v62    # "freq":I
    const/16 v68, 0x0

    .restart local v68    # "k":I
    :goto_23
    move/from16 v0, v68

    move/from16 v1, v62

    if-lt v0, v1, :cond_32

    .line 441
    add-int/lit8 v66, v66, 0x1

    goto :goto_22

    .line 444
    :cond_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v69, v0

    .line 445
    .local v69, "l":I
    add-int v73, v73, v69

    .line 443
    add-int/lit8 v68, v68, 0x1

    goto :goto_23

    .line 454
    .end local v50    # "f":I
    .end local v62    # "freq":I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .end local v69    # "l":I
    .end local v78    # "termCount":I
    :cond_33
    add-int v2, v8, v63

    invoke-interface {v10, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v50, v0

    .line 455
    .restart local v50    # "f":I
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 456
    .restart local v78    # "termCount":I
    and-int/lit8 v2, v50, 0x4

    if-eqz v2, :cond_36

    .line 457
    aget-object v2, v15, v63

    aget v87, v2, v78

    .line 458
    .local v87, "totalFreq":I
    add-int/lit8 v2, v87, 0x1

    new-array v2, v2, [I

    aput-object v2, v39, v63

    .line 459
    const/16 v74, 0x0

    .line 460
    .local v74, "posIdx":I
    aget-object v2, v39, v63

    aput v71, v2, v74

    .line 461
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :goto_24
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_34

    .line 470
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_36

    move/from16 v0, v74

    move/from16 v1, v87

    if-eq v0, v1, :cond_36

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 462
    :cond_34
    add-int v2, v81, v66

    aget v62, v12, v2

    .line 463
    .restart local v62    # "freq":I
    const/16 v68, 0x0

    .restart local v68    # "k":I
    :goto_25
    move/from16 v0, v68

    move/from16 v1, v62

    if-lt v0, v1, :cond_35

    .line 461
    add-int/lit8 v66, v66, 0x1

    goto :goto_24

    .line 464
    :cond_35
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v72, v0

    .line 465
    .local v72, "payloadLength":I
    add-int v71, v71, v72

    .line 466
    aget-object v2, v39, v63

    add-int/lit8 v3, v74, 0x1

    aput v71, v2, v3

    .line 467
    add-int/lit8 v74, v74, 0x1

    .line 463
    add-int/lit8 v68, v68, 0x1

    goto :goto_25

    .line 472
    .end local v62    # "freq":I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .end local v72    # "payloadLength":I
    .end local v74    # "posIdx":I
    .end local v87    # "totalFreq":I
    :cond_36
    add-int v81, v81, v78

    .line 453
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_15

    .line 476
    .end local v50    # "f":I
    .end local v78    # "termCount":I
    :cond_37
    move/from16 v0, v63

    invoke-interface {v10, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v50, v0

    .line 477
    .restart local v50    # "f":I
    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 478
    .restart local v78    # "termCount":I
    and-int/lit8 v2, v50, 0x4

    if-eqz v2, :cond_38

    .line 479
    const/16 v66, 0x0

    .restart local v66    # "j":I
    :goto_26
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_39

    .line 486
    .end local v66    # "j":I
    :cond_38
    add-int v81, v81, v78

    .line 475
    add-int/lit8 v63, v63, 0x1

    goto/16 :goto_16

    .line 480
    .restart local v66    # "j":I
    :cond_39
    add-int v2, v81, v66

    aget v62, v12, v2

    .line 481
    .restart local v62    # "freq":I
    const/16 v68, 0x0

    .restart local v68    # "k":I
    :goto_27
    move/from16 v0, v68

    move/from16 v1, v62

    if-lt v0, v1, :cond_3a

    .line 479
    add-int/lit8 v66, v66, 0x1

    goto :goto_26

    .line 482
    :cond_3a
    move/from16 v0, v89

    int-to-long v2, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->reader:Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;

    invoke-virtual {v4}, Lorg/apache/lucene/util/packed/BlockPackedReaderIterator;->next()J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v89, v0

    .line 481
    add-int/lit8 v68, v68, 0x1

    goto :goto_27

    .line 492
    .end local v50    # "f":I
    .end local v62    # "freq":I
    .end local v66    # "j":I
    .end local v68    # "k":I
    .end local v78    # "termCount":I
    :cond_3b
    new-instance v21, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v21 .. v21}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 493
    .local v21, "suffixBytes":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v17, v0

    add-int v18, v88, v89

    add-int v19, v48, v73

    add-int v20, v47, v71

    invoke-virtual/range {v16 .. v21}, Lorg/apache/lucene/codecs/compressing/Decompressor;->decompress(Lorg/apache/lucene/store/DataInput;IIILorg/apache/lucene/util/BytesRef;)V

    .line 494
    move/from16 v0, v47

    move-object/from16 v1, v21

    iput v0, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 495
    new-instance v38, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v21

    iget-object v2, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, v21

    iget v3, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int v3, v3, v47

    move-object/from16 v0, v38

    move/from16 v1, v71

    invoke-direct {v0, v2, v3, v1}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    .line 497
    .local v38, "payloadBytes":Lorg/apache/lucene/util/BytesRef;
    new-array v0, v9, [I

    move-object/from16 v27, v0

    .line 498
    .local v27, "fieldFlags":[I
    const/16 v63, 0x0

    :goto_28
    move/from16 v0, v63

    if-lt v0, v9, :cond_3c

    .line 502
    new-array v0, v9, [I

    move-object/from16 v29, v0

    .line 503
    .local v29, "fieldNumTerms":[I
    const/16 v63, 0x0

    :goto_29
    move/from16 v0, v63

    if-lt v0, v9, :cond_3d

    .line 507
    new-array v0, v9, [[I

    move-object/from16 v33, v0

    .line 509
    .local v33, "fieldTermFreqs":[[I
    const/16 v79, 0x0

    .line 510
    .local v79, "termIdx":I
    const/16 v63, 0x0

    :goto_2a
    move/from16 v0, v63

    if-lt v0, v8, :cond_3e

    .line 513
    const/16 v63, 0x0

    :goto_2b
    move/from16 v0, v63

    if-lt v0, v9, :cond_3f

    .line 522
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->$assertionsDisabled:Z

    if-nez v2, :cond_41

    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->sum([I)I

    move-result v2

    move/from16 v0, v47

    if-eq v2, v0, :cond_41

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {v30 .. v30}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->sum([I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v47

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 499
    .end local v29    # "fieldNumTerms":[I
    .end local v33    # "fieldTermFreqs":[[I
    .end local v79    # "termIdx":I
    :cond_3c
    add-int v2, v8, v63

    invoke-interface {v10, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v27, v63

    .line 498
    add-int/lit8 v63, v63, 0x1

    goto :goto_28

    .line 504
    .restart local v29    # "fieldNumTerms":[I
    :cond_3d
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v2, v2

    aput v2, v29, v63

    .line 503
    add-int/lit8 v63, v63, 0x1

    goto :goto_29

    .line 511
    .restart local v33    # "fieldTermFreqs":[[I
    .restart local v79    # "termIdx":I
    :cond_3e
    move/from16 v0, v79

    int-to-long v2, v0

    move/from16 v0, v63

    invoke-interface {v11, v0}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v16

    add-long v2, v2, v16

    long-to-int v0, v2

    move/from16 v79, v0

    .line 510
    add-int/lit8 v63, v63, 0x1

    goto :goto_2a

    .line 514
    :cond_3f
    add-int v2, v8, v63

    invoke-interface {v11, v2}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v78, v0

    .line 515
    .restart local v78    # "termCount":I
    move/from16 v0, v78

    new-array v2, v0, [I

    aput-object v2, v33, v63

    .line 516
    const/16 v66, 0x0

    .restart local v66    # "j":I
    move/from16 v80, v79

    .end local v79    # "termIdx":I
    .local v80, "termIdx":I
    :goto_2c
    move/from16 v0, v66

    move/from16 v1, v78

    if-lt v0, v1, :cond_40

    .line 513
    add-int/lit8 v63, v63, 0x1

    move/from16 v79, v80

    .end local v80    # "termIdx":I
    .restart local v79    # "termIdx":I
    goto :goto_2b

    .line 517
    .end local v79    # "termIdx":I
    .restart local v80    # "termIdx":I
    :cond_40
    aget-object v2, v33, v63

    add-int/lit8 v79, v80, 0x1

    .end local v80    # "termIdx":I
    .restart local v79    # "termIdx":I
    aget v3, v12, v80

    aput v3, v2, v66

    .line 516
    add-int/lit8 v66, v66, 0x1

    move/from16 v80, v79

    .end local v79    # "termIdx":I
    .restart local v80    # "termIdx":I
    goto :goto_2c

    .line 524
    .end local v66    # "j":I
    .end local v78    # "termCount":I
    .end local v80    # "termIdx":I
    .restart local v79    # "termIdx":I
    :cond_41
    new-instance v24, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;

    move-object/from16 v25, p0

    move-object/from16 v34, v15

    move-object/from16 v40, v21

    invoke-direct/range {v24 .. v40}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;[I[I[I[I[I[[I[[I[[I[[I[[I[[I[[ILorg/apache/lucene/util/BytesRef;[[ILorg/apache/lucene/util/BytesRef;)V

    goto/16 :goto_1

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method getChunkSize()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->chunkSize:I

    return v0
.end method

.method getCompressionMode()Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    return-object v0
.end method

.method getIndex()Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    return-object v0
.end method

.method getPackedIntsVersion()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->packedIntsVersion:I

    return v0
.end method

.method getVectorsStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->vectorsStream:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method
