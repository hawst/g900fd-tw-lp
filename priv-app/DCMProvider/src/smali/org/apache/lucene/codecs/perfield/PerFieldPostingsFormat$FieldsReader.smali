.class Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;
.super Lorg/apache/lucene/codecs/FieldsProducer;
.source "PerFieldPostingsFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FieldsReader"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/FieldsProducer;",
            ">;"
        }
    .end annotation
.end field

.field private final formats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/FieldsProducer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    const-class v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;Lorg/apache/lucene/index/SegmentReadState;)V
    .locals 10
    .param p2, "readState"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    iput-object p1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->this$0:Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldsProducer;-><init>()V

    .line 174
    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->fields:Ljava/util/Map;

    .line 175
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    .line 180
    const/4 v5, 0x0

    .line 183
    .local v5, "success":Z
    :try_start_0
    iget-object v7, p2, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_2

    .line 200
    const/4 v5, 0x1

    .line 202
    if-nez v5, :cond_1

    .line 203
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 206
    :cond_1
    return-void

    .line 183
    :cond_2
    :try_start_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 184
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 185
    iget-object v1, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    .line 186
    .local v1, "fieldName":Ljava/lang/String;
    sget-object v7, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->PER_FIELD_FORMAT_KEY:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "formatName":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 189
    sget-object v7, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->PER_FIELD_SUFFIX_KEY:Ljava/lang/String;

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 190
    .local v6, "suffix":Ljava/lang/String;
    sget-boolean v7, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->$assertionsDisabled:Z

    if-nez v7, :cond_4

    if-nez v6, :cond_4

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 201
    .end local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v1    # "fieldName":Ljava/lang/String;
    .end local v3    # "formatName":Ljava/lang/String;
    .end local v6    # "suffix":Ljava/lang/String;
    :catchall_0
    move-exception v7

    .line 202
    if-nez v5, :cond_3

    .line 203
    iget-object v8, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 205
    :cond_3
    throw v7

    .line 191
    .restart local v0    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .restart local v1    # "fieldName":Ljava/lang/String;
    .restart local v3    # "formatName":Ljava/lang/String;
    .restart local v6    # "suffix":Ljava/lang/String;
    :cond_4
    :try_start_2
    invoke-static {v3}, Lorg/apache/lucene/codecs/PostingsFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v2

    .line 192
    .local v2, "format":Lorg/apache/lucene/codecs/PostingsFormat;
    invoke-static {v3, v6}, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;->getSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 193
    .local v4, "segmentSuffix":Ljava/lang/String;
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 194
    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    new-instance v9, Lorg/apache/lucene/index/SegmentReadState;

    invoke-direct {v9, p2, v4}, Lorg/apache/lucene/index/SegmentReadState;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lorg/apache/lucene/codecs/PostingsFormat;->fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;

    move-result-object v9

    invoke-interface {v7, v4, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    :cond_5
    iget-object v9, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->fields:Ljava/util/Map;

    iget-object v7, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v7, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/codecs/FieldsProducer;

    invoke-interface {v9, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->formats:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V

    .line 227
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->fields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->fields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    iget-object v1, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsReader;->fields:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/FieldsProducer;

    .line 216
    .local v0, "fieldsProducer":Lorg/apache/lucene/codecs/FieldsProducer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/FieldsProducer;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    goto :goto_0
.end method
