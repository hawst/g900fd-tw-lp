.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "SimpleTextFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTextTermsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private docFreq:I

.field private docsStart:J

.field private ended:Z

.field private final fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum",
            "<",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<",
            "Ljava/lang/Long;",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

.field private totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 1
    .param p3, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<",
            "Ljava/lang/Long;",
            "Lorg/apache/lucene/util/fst/PairOutputs$Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;>;",
            "Lorg/apache/lucene/index/FieldInfo$IndexOptions;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    .local p2, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;>;"
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 107
    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 108
    new-instance v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-direct {v0, p2}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    .line 109
    return-void
.end method


# virtual methods
.method public docFreq()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docFreq:I

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 7
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    if-eqz p2, :cond_0

    instance-of v0, p2, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v1, p2

    .line 198
    check-cast v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;

    .line 202
    .local v1, "docsEnum":Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;
    :goto_0
    iget-wide v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docsStart:J

    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v4, :cond_1

    const/4 v5, 0x1

    :goto_1
    iget v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docFreq:I

    move-object v4, p1

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->reset(JLorg/apache/lucene/util/Bits;ZI)Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;

    move-result-object v0

    return-object v0

    .line 200
    .end local v1    # "docsEnum":Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;
    :cond_0
    new-instance v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;

    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {v1, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)V

    .restart local v1    # "docsEnum":Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;
    goto :goto_0

    .line 202
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 7
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_0

    .line 210
    const/4 v0, 0x0

    .line 219
    :goto_0
    return-object v0

    .line 214
    :cond_0
    if-eqz p2, :cond_1

    instance-of v0, p2, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;

    if-eqz v0, :cond_1

    move-object v0, p2

    check-cast v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v1, p2

    .line 215
    check-cast v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;

    .line 219
    .local v1, "docsAndPositionsEnum":Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;
    :goto_1
    iget-wide v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docsStart:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iget v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docFreq:I

    move-object v4, p1

    invoke-virtual/range {v1 .. v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->reset(JLorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/FieldInfo$IndexOptions;I)Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;

    move-result-object v0

    goto :goto_0

    .line 217
    .end local v1    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;
    :cond_1
    new-instance v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;

    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {v1, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)V

    .restart local v1    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;
    goto :goto_1
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    sget-boolean v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->ended:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 156
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->next()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v2

    .line 157
    .local v2, "result":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;>;"
    if-eqz v2, :cond_1

    .line 158
    iget-object v0, v2, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 159
    .local v0, "pair1":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    iget-object v1, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    check-cast v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 160
    .local v1, "pair2":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v3, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docsStart:J

    .line 161
    iget-object v3, v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docFreq:I

    .line 162
    iget-object v3, v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->totalTermFreq:J

    .line 163
    iget-object v3, v2, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    .line 165
    .end local v0    # "pair1":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    .end local v1    # "pair2":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public ord()J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 6
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v2

    .line 132
    .local v2, "result":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;>;"
    if-nez v2, :cond_0

    .line 134
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 148
    :goto_0
    return-object v3

    .line 137
    :cond_0
    iget-object v0, v2, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 138
    .local v0, "pair1":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    iget-object v1, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    check-cast v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 139
    .local v1, "pair2":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v3, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docsStart:J

    .line 140
    iget-object v3, v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docFreq:I

    .line 141
    iget-object v3, v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->totalTermFreq:J

    .line 143
    iget-object v3, v2, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 148
    :cond_1
    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J

    .prologue
    .line 181
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 6
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v2

    .line 115
    .local v2, "result":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;>;"
    if-eqz v2, :cond_0

    .line 116
    iget-object v0, v2, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 117
    .local v0, "pair1":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    iget-object v1, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    check-cast v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;

    .line 118
    .local v1, "pair2":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    iget-object v3, v0, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docsStart:J

    .line 119
    iget-object v3, v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output1:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->docFreq:I

    .line 120
    iget-object v3, v1, Lorg/apache/lucene/util/fst/PairOutputs$Pair;->output2:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->totalTermFreq:J

    .line 121
    const/4 v3, 0x1

    .line 123
    .end local v0    # "pair1":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;>;"
    .end local v1    # "pair2":Lorg/apache/lucene/util/fst/PairOutputs$Pair;, "Lorg/apache/lucene/util/fst/PairOutputs$Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v1, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextTermsEnum;->totalTermFreq:J

    goto :goto_0
.end method
