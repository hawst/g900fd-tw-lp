.class final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "Lucene41PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "BlockDocsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private accum:I

.field private doc:I

.field private docBufferUpto:I

.field private final docDeltaBuffer:[I

.field private docFreq:I

.field docIn:Lorg/apache/lucene/store/IndexInput;

.field private docTermStartFP:J

.field private docUpto:I

.field private final encoded:[B

.field private freq:I

.field private final freqBuffer:[I

.field final indexHasFreq:Z

.field final indexHasOffsets:Z

.field final indexHasPayloads:Z

.field final indexHasPos:Z

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private needsFreq:Z

.field private nextSkipDoc:I

.field private singletonDocID:I

.field private skipOffset:J

.field private skipped:Z

.field private skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

.field final startDocIn:Lorg/apache/lucene/store/IndexInput;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

.field private totalTermFreq:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 332
    const-class v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 4
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 375
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 335
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docDeltaBuffer:[I

    .line 336
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    .line 376
    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$1(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    .line 377
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 378
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasFreq:Z

    .line 379
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasPos:Z

    .line 380
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasOffsets:Z

    .line 381
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasPayloads:Z

    .line 382
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->encoded:[B

    .line 383
    return-void

    :cond_0
    move v0, v2

    .line 378
    goto :goto_0

    :cond_1
    move v0, v2

    .line 379
    goto :goto_1

    :cond_2
    move v1, v2

    .line 380
    goto :goto_2
.end method

.method private refillDocs()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 434
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    sub-int v0, v1, v2

    .line 435
    .local v0, "left":I
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 437
    :cond_0
    const/16 v1, 0x80

    if-lt v0, v1, :cond_3

    .line 441
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docDeltaBuffer:[I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 443
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasFreq:Z

    if-eqz v1, :cond_1

    .line 447
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->needsFreq:Z

    if-eqz v1, :cond_2

    .line 448
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->readBlock(Lorg/apache/lucene/store/IndexInput;[B[I)V

    .line 463
    :cond_1
    :goto_0
    iput v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 464
    return-void

    .line 450
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->skipBlock(Lorg/apache/lucene/store/IndexInput;)V

    goto :goto_0

    .line 453
    :cond_3
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 454
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->singletonDocID:I

    aput v2, v1, v5

    .line 455
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->totalTermFreq:J

    long-to-int v2, v2

    aput v2, v1, v5

    goto :goto_0

    .line 461
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docDeltaBuffer:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasFreq:Z

    invoke-static {v1, v2, v3, v0, v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->readVIntBlock(Lorg/apache/lucene/store/IndexInput;[I[IIZ)V

    goto :goto_0
.end method


# virtual methods
.method public advance(I)I
    .locals 12
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 517
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    const/16 v1, 0x80

    if-le v0, v1, :cond_5

    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->nextSkipDoc:I

    if-le p1, v0, :cond_5

    .line 523
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    if-nez v0, :cond_0

    .line 525
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 526
    const/16 v2, 0xa

    .line 527
    const/16 v3, 0x80

    .line 528
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasPos:Z

    .line 529
    iget-boolean v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasOffsets:Z

    .line 530
    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasPayloads:Z

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;-><init>(Lorg/apache/lucene/store/IndexInput;IIZZZ)V

    .line 525
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    .line 533
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipped:Z

    if-nez v0, :cond_2

    .line 534
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipOffset:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 537
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docTermStartFP:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipOffset:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docTermStartFP:J

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    iget v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    invoke-virtual/range {v1 .. v10}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->init(JJJJI)V

    .line 538
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipped:Z

    .line 543
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->skipTo(I)I

    move-result v0

    add-int/lit8 v11, v0, 0x1

    .line 545
    .local v11, "newDocUpto":I
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    if-le v11, v0, :cond_4

    .line 550
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    rem-int/lit16 v0, v11, 0x80

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 551
    :cond_3
    iput v11, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    .line 554
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 555
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    .line 556
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getDocPointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 560
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipper:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipReader;->getNextSkipDoc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->nextSkipDoc:I

    .line 562
    .end local v11    # "newDocUpto":I
    :cond_5
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    if-ne v0, v1, :cond_6

    .line 563
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    .line 599
    :goto_0
    return v0

    .line 565
    :cond_6
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 566
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->refillDocs()V

    .line 575
    :cond_7
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    .line 576
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    .line 578
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    if-lt v0, p1, :cond_9

    .line 587
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 591
    :cond_8
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freq:I

    .line 592
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 593
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    goto :goto_0

    .line 581
    :cond_9
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 582
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    if-ne v0, v1, :cond_7

    .line 583
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    goto :goto_0

    .line 598
    :cond_a
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 599
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->nextDoc()I

    move-result v0

    goto :goto_0
.end method

.method public canReuse(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;)Z
    .locals 5
    .param p1, "docIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 386
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v2, :cond_2

    .line 387
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasFreq:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    if-ne v3, v2, :cond_2

    .line 388
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasPos:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v2

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    .line 389
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasPayloads:Z

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v3

    if-ne v2, v3, :cond_2

    .line 386
    :goto_2
    return v0

    :cond_0
    move v2, v1

    .line 387
    goto :goto_0

    :cond_1
    move v2, v1

    .line 388
    goto :goto_1

    :cond_2
    move v0, v1

    .line 386
    goto :goto_2
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 605
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 425
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freq:I

    return v0
.end method

.method public nextDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 476
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    if-ne v0, v1, :cond_0

    .line 480
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    .line 499
    :goto_1
    return v0

    .line 482
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_1

    .line 483
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->refillDocs()V

    .line 489
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    aget v1, v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    .line 490
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    .line 492
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    .line 494
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    aget v0, v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freq:I

    .line 495
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 499
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    goto :goto_1

    .line 504
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 6
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 393
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 397
    iget v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    .line 398
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasFreq:Z

    if-eqz v0, :cond_3

    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->totalTermFreq:J

    .line 399
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docTermStartFP:J

    .line 400
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipOffset:J

    .line 401
    iget v0, p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->singletonDocID:I

    .line 402
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    if-le v0, v2, :cond_1

    .line 403
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 405
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->startDocIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 407
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docTermStartFP:J

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 410
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->doc:I

    .line 411
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->needsFreq:Z

    .line 412
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->indexHasFreq:Z

    if-nez v0, :cond_2

    .line 413
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->freqBuffer:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 415
    :cond_2
    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->accum:I

    .line 416
    iput v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docUpto:I

    .line 417
    const/16 v0, 0x7f

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->nextSkipDoc:I

    .line 418
    const/16 v0, 0x80

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docBufferUpto:I

    .line 419
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->skipped:Z

    .line 420
    return-object p0

    .line 398
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->docFreq:I

    int-to-long v0, v0

    goto :goto_0

    :cond_4
    move v0, v3

    .line 411
    goto :goto_1
.end method
