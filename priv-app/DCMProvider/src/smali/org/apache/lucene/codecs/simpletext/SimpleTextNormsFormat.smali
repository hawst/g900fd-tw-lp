.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat;
.super Lorg/apache/lucene/codecs/NormsFormat;
.source "SimpleTextNormsFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsConsumer;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsProducer;
    }
.end annotation


# static fields
.field private static final NORMS_SEG_EXTENSION:Ljava/lang/String; = "len"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/codecs/NormsFormat;-><init>()V

    return-void
.end method


# virtual methods
.method public normsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsConsumer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsConsumer;-><init>(Lorg/apache/lucene/index/SegmentWriteState;)V

    return-object v0
.end method

.method public normsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsProducer;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsProducer;-><init>(Lorg/apache/lucene/index/SegmentReadState;)V

    return-object v0
.end method
