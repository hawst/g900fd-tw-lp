.class public final Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;
.super Lorg/apache/lucene/codecs/StoredFieldsWriter;
.source "CompressingStoredFieldsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BYTE_ARR:I = 0x1

.field static final CODEC_SFX_DAT:Ljava/lang/String; = "Data"

.field static final CODEC_SFX_IDX:Ljava/lang/String; = "Index"

.field static final MAX_DOCUMENTS_PER_CHUNK:I = 0x80

.field static final NUMERIC_DOUBLE:I = 0x5

.field static final NUMERIC_FLOAT:I = 0x3

.field static final NUMERIC_INT:I = 0x2

.field static final NUMERIC_LONG:I = 0x4

.field static final STRING:I

.field static final TYPE_BITS:I

.field static final TYPE_MASK:I

.field static final VERSION_CURRENT:I

.field static final VERSION_START:I


# instance fields
.field private final bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

.field private final chunkSize:I

.field private final compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field private final compressor:Lorg/apache/lucene/codecs/compressing/Compressor;

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private docBase:I

.field private endOffsets:[I

.field private fieldsStream:Lorg/apache/lucene/store/IndexOutput;

.field private indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

.field private numBufferedDocs:I

.field private numStoredFields:[I

.field private final segment:Ljava/lang/String;

.field private final segmentSuffix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    .line 66
    const-wide/16 v0, 0x5

    invoke-static {v0, v1}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v0

    sput v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_BITS:I

    .line 67
    sget v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_BITS:I

    invoke-static {v0}, Lorg/apache/lucene/util/packed/PackedInts;->maxValue(I)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_MASK:I

    .line 72
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V
    .locals 8
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "segmentSuffix"    # Ljava/lang/String;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "formatName"    # Ljava/lang/String;
    .param p6, "compressionMode"    # Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .param p7, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;-><init>()V

    .line 93
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez p1, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 94
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 95
    iget-object v4, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segment:Ljava/lang/String;

    .line 96
    iput-object p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segmentSuffix:Ljava/lang/String;

    .line 97
    iput-object p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 98
    invoke-virtual {p6}, Lorg/apache/lucene/codecs/compressing/CompressionMode;->newCompressor()Lorg/apache/lucene/codecs/compressing/Compressor;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->compressor:Lorg/apache/lucene/codecs/compressing/Compressor;

    .line 99
    iput p7, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->chunkSize:I

    .line 100
    const/4 v4, 0x0

    iput v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    .line 101
    new-instance v4, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-direct {v4, p7}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;-><init>(I)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    .line 102
    const/16 v4, 0x10

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numStoredFields:[I

    .line 103
    const/16 v4, 0x10

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    .line 104
    const/4 v4, 0x0

    iput v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    .line 106
    const/4 v3, 0x0

    .line 107
    .local v3, "success":Z
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segment:Ljava/lang/String;

    const-string v5, "fdx"

    invoke-static {v4, p3, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    .line 109
    .local v2, "indexStream":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segment:Ljava/lang/String;

    const-string v5, "fdt"

    invoke-static {v4, p3, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 111
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "Index"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "codecNameIdx":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "Data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "codecNameDat":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v2, v1, v4}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 114
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 115
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    invoke-static {v0}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    .end local v0    # "codecNameDat":Ljava/lang/String;
    .end local v1    # "codecNameIdx":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 125
    if-nez v3, :cond_1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 126
    aput-object v2, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->abort()V

    .line 129
    :cond_1
    throw v4

    .line 116
    .restart local v0    # "codecNameDat":Ljava/lang/String;
    .restart local v1    # "codecNameIdx":Ljava/lang/String;
    :cond_2
    :try_start_1
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    invoke-static {v1}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 118
    :cond_3
    new-instance v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    invoke-direct {v4, v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;-><init>(Lorg/apache/lucene/store/IndexOutput;)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    .line 119
    const/4 v2, 0x0

    .line 121
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    const/4 v3, 0x1

    .line 125
    if-nez v3, :cond_4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 126
    aput-object v2, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 127
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->abort()V

    .line 130
    :cond_4
    return-void
.end method

.method private flush()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 210
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->writeIndex(IJ)V

    .line 213
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    .line 214
    .local v1, "lengths":[I
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-gtz v0, :cond_0

    .line 218
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numStoredFields:[I

    invoke-direct {p0, v2, v3, v4, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->writeHeader(II[I[I)V

    .line 221
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->compressor:Lorg/apache/lucene/codecs/compressing/Compressor;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v3, v3, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v4, v4, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v3, v6, v4, v5}, Lorg/apache/lucene/codecs/compressing/Compressor;->compress([BIILorg/apache/lucene/store/DataOutput;)V

    .line 224
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    .line 225
    iput v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    .line 226
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iput v6, v2, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    .line 227
    return-void

    .line 215
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    aget v2, v2, v0

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    add-int/lit8 v4, v0, -0x1

    aget v3, v3, v4

    sub-int/2addr v2, v3

    aput v2, v1, v0

    .line 216
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    aget v2, v1, v0

    if-gez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 214
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private static nextDeletedDoc(ILorg/apache/lucene/util/Bits;I)I
    .locals 1
    .param p0, "doc"    # I
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "maxDoc"    # I

    .prologue
    .line 404
    if-nez p1, :cond_1

    .line 410
    .end local p2    # "maxDoc":I
    :goto_0
    return p2

    .line 408
    .restart local p2    # "maxDoc":I
    :cond_0
    add-int/lit8 p0, p0, 0x1

    .line 407
    :cond_1
    if-ge p0, p2, :cond_2

    invoke-interface {p1, p0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    move p2, p0

    .line 410
    goto :goto_0
.end method

.method private static nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I
    .locals 1
    .param p0, "doc"    # I
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "maxDoc"    # I

    .prologue
    .line 394
    if-nez p1, :cond_2

    .line 400
    :cond_0
    :goto_0
    return p0

    .line 398
    :cond_1
    add-int/lit8 p0, p0, 0x1

    .line 397
    :cond_2
    if-ge p0, p2, :cond_0

    invoke-interface {p1, p0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0
.end method

.method private static saveInts([IILorg/apache/lucene/store/DataOutput;)V
    .locals 10
    .param p0, "values"    # [I
    .param p1, "length"    # I
    .param p2, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 162
    sget-boolean v6, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    if-gtz p1, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 163
    :cond_0
    if-ne p1, v9, :cond_1

    .line 164
    aget v6, p0, v8

    invoke-virtual {p2, v6}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 190
    :goto_0
    return-void

    .line 166
    :cond_1
    const/4 v0, 0x1

    .line 167
    .local v0, "allEqual":Z
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    if-lt v2, p1, :cond_2

    .line 173
    :goto_2
    if-eqz v0, :cond_4

    .line 174
    invoke-virtual {p2, v8}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 175
    aget v6, p0, v8

    invoke-virtual {p2, v6}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    goto :goto_0

    .line 168
    :cond_2
    aget v6, p0, v2

    aget v7, p0, v8

    if-eq v6, v7, :cond_3

    .line 169
    const/4 v0, 0x0

    .line 170
    goto :goto_2

    .line 167
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 177
    :cond_4
    const-wide/16 v4, 0x0

    .line 178
    .local v4, "max":J
    const/4 v2, 0x0

    :goto_3
    if-lt v2, p1, :cond_5

    .line 181
    invoke-static {v4, v5}, Lorg/apache/lucene/util/packed/PackedInts;->bitsRequired(J)I

    move-result v1

    .line 182
    .local v1, "bitsRequired":I
    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 183
    sget-object v6, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    invoke-static {p2, v6, p1, v1, v9}, Lorg/apache/lucene/util/packed/PackedInts;->getWriterNoHeader(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Writer;

    move-result-object v3

    .line 184
    .local v3, "w":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    const/4 v2, 0x0

    :goto_4
    if-lt v2, p1, :cond_6

    .line 187
    invoke-virtual {v3}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->finish()V

    goto :goto_0

    .line 179
    .end local v1    # "bitsRequired":I
    .end local v3    # "w":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_5
    aget v6, p0, v2

    int-to-long v6, v6

    or-long/2addr v4, v6

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 185
    .restart local v1    # "bitsRequired":I
    .restart local v3    # "w":Lorg/apache/lucene/util/packed/PackedInts$Writer;
    :cond_6
    aget v6, p0, v2

    int-to-long v6, v6

    invoke-virtual {v3, v6, v7}, Lorg/apache/lucene/util/packed/PackedInts$Writer;->add(J)V

    .line 184
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method private triggerFlush()Z
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->chunkSize:I

    if-ge v0, v1, :cond_0

    .line 206
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    const/16 v1, 0x80

    .line 205
    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private writeHeader(II[I[I)V
    .locals 1
    .param p1, "docBase"    # I
    .param p2, "numBufferedDocs"    # I
    .param p3, "numStoredFields"    # [I
    .param p4, "lengths"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 195
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 198
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {p3, p2, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->saveInts([IILorg/apache/lucene/store/DataOutput;)V

    .line 201
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {p4, p2, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->saveInts([IILorg/apache/lucene/store/DataOutput;)V

    .line 202
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 294
    new-array v0, v6, [Ljava/io/Closeable;

    .line 290
    aput-object p0, v0, v5

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 291
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    .line 292
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segment:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segmentSuffix:Ljava/lang/String;

    const-string v4, "fdt"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 293
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segment:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->segmentSuffix:Ljava/lang/String;

    const-string v4, "fdx"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 291
    invoke-static {v0, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 134
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 135
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 138
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    .line 140
    return-void

    .line 136
    :catchall_0
    move-exception v0

    .line 137
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    .line 138
    iput-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    .line 139
    throw v0
.end method

.method public finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 3
    .param p1, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 298
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    if-lez v0, :cond_1

    .line 299
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->flush()V

    .line 303
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    if-eq v0, p2, :cond_2

    .line 304
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrote "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " docs, finish called with numDocs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 306
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->finish(I)V

    .line 307
    sget-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 308
    :cond_3
    return-void
.end method

.method public finishDocument()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v2, v2, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    aput v2, v0, v1

    .line 156
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->triggerFlush()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->flush()V

    .line 159
    :cond_0
    return-void
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;)I
    .locals 26
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    const/4 v8, 0x0

    .line 313
    .local v8, "docCount":I
    const/4 v12, 0x0

    .line 315
    .local v12, "idx":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-nez v22, :cond_0

    .line 389
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v8}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V

    .line 390
    return v8

    .line 315
    :cond_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/AtomicReader;

    .line 316
    .local v19, "reader":Lorg/apache/lucene/index/AtomicReader;
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v22, v0

    add-int/lit8 v13, v12, 0x1

    .end local v12    # "idx":I
    .local v13, "idx":I
    aget-object v17, v22, v12

    .line 317
    .local v17, "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/16 v16, 0x0

    .line 318
    .local v16, "matchingFieldsReader":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;
    if-eqz v17, :cond_1

    .line 319
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/SegmentReader;->getFieldsReader()Lorg/apache/lucene/codecs/StoredFieldsReader;

    move-result-object v10

    .line 321
    .local v10, "fieldsReader":Lorg/apache/lucene/codecs/StoredFieldsReader;
    if-eqz v10, :cond_1

    instance-of v0, v10, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    move/from16 v22, v0

    if-eqz v22, :cond_1

    move-object/from16 v16, v10

    .line 322
    check-cast v16, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    .line 326
    .end local v10    # "fieldsReader":Lorg/apache/lucene/codecs/StoredFieldsReader;
    :cond_1
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v18

    .line 327
    .local v18, "maxDoc":I
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v15

    .line 329
    .local v15, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-nez v16, :cond_3

    .line 331
    const/16 v22, 0x0

    move/from16 v0, v22

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    .local v11, "i":I
    :goto_1
    move/from16 v0, v18

    if-lt v11, v0, :cond_2

    move v12, v13

    .line 337
    .end local v13    # "idx":I
    .restart local v12    # "idx":I
    goto :goto_0

    .line 332
    .end local v12    # "idx":I
    .restart local v13    # "idx":I
    :cond_2
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/AtomicReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v7

    .line 333
    .local v7, "doc":Lorg/apache/lucene/document/Document;
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v7, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->addDocument(Ljava/lang/Iterable;Lorg/apache/lucene/index/FieldInfos;)V

    .line 334
    add-int/lit8 v8, v8, 0x1

    .line 335
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v22, v0

    const-wide v24, 0x4072c00000000000L    # 300.0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 331
    add-int/lit8 v22, v11, 0x1

    move/from16 v0, v22

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v11

    goto :goto_1

    .line 338
    .end local v7    # "doc":Lorg/apache/lucene/document/Document;
    .end local v11    # "i":I
    :cond_3
    const/16 v22, 0x0

    move/from16 v0, v22

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v9

    .line 339
    .local v9, "docID":I
    move/from16 v0, v18

    if-ge v9, v0, :cond_8

    .line 341
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->chunkIterator(I)Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;

    move-result-object v14

    .line 342
    .local v14, "it":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;
    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 345
    .local v20, "startOffsets":[I
    :cond_4
    invoke-virtual {v14, v9}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->next(I)V

    .line 347
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_5

    .line 348
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v22, v0

    const/16 v23, 0x4

    invoke-static/range {v22 .. v23}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 350
    :cond_5
    const/4 v11, 0x1

    .restart local v11    # "i":I
    :goto_2
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-lt v11, v0, :cond_6

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    move-object/from16 v22, v0

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->getCompressionMode()Lorg/apache/lucene/codecs/compressing/CompressionMode;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_9

    .line 355
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    move/from16 v22, v0

    if-nez v22, :cond_9

    .line 356
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget v22, v20, v22

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->chunkSize:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_9

    .line 357
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget v22, v20, v22

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    move-object/from16 v23, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    aget v23, v23, v24

    add-int v22, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->chunkSize:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_9

    .line 358
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v23, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->nextDeletedDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v22

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v23, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_9

    .line 359
    sget-boolean v22, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->$assertionsDisabled:Z

    if-nez v22, :cond_7

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-eq v9, v0, :cond_7

    new-instance v21, Ljava/lang/AssertionError;

    invoke-direct/range {v21 .. v21}, Ljava/lang/AssertionError;-><init>()V

    throw v21

    .line 351
    :cond_6
    add-int/lit8 v22, v11, -0x1

    aget v22, v20, v22

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    move-object/from16 v23, v0

    add-int/lit8 v24, v11, -0x1

    aget v23, v23, v24

    add-int v22, v22, v23

    aput v22, v20, v11

    .line 350
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 362
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->indexWriter:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;

    move-object/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v24

    invoke-virtual/range {v22 .. v25}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexWriter;->writeIndex(IJ)V

    .line 363
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    move/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    move-object/from16 v24, v0

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    move-object/from16 v3, v24

    move-object/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->writeHeader(II[I[I)V

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->fieldsStream:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->copyCompressedData(Lorg/apache/lucene/store/DataOutput;)V

    .line 365
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    move/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->docBase:I

    .line 366
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v9

    .line 367
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v22, v0

    add-int v8, v8, v22

    .line 368
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    move/from16 v0, v23

    mul-int/lit16 v0, v0, 0x12c

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 343
    :goto_3
    move/from16 v0, v18

    if-lt v9, v0, :cond_4

    .end local v11    # "i":I
    .end local v14    # "it":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;
    .end local v20    # "startOffsets":[I
    :cond_8
    move v12, v13

    .end local v13    # "idx":I
    .restart local v12    # "idx":I
    goto/16 :goto_0

    .line 371
    .end local v12    # "idx":I
    .restart local v11    # "i":I
    .restart local v13    # "idx":I
    .restart local v14    # "it":Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;
    .restart local v20    # "startOffsets":[I
    :cond_9
    invoke-virtual {v14}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->decompress()V

    .line 372
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    aget v22, v20, v22

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    move-object/from16 v23, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    aget v23, v23, v24

    add-int v22, v22, v23

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_b

    .line 373
    new-instance v21, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Corrupted: expected chunk size="

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    aget v23, v20, v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    move-object/from16 v23, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    aget v23, v23, v24

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", got "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v21

    .line 377
    :cond_a
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v22, v0

    sub-int v6, v9, v22

    .line 378
    .local v6, "diff":I
    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    move-object/from16 v22, v0

    aget v22, v22, v6

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->startDocument(I)V

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    move-object/from16 v22, v0

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v23, v0

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move/from16 v24, v0

    aget v25, v20, v6

    add-int v24, v24, v25

    iget-object v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    move-object/from16 v25, v0

    aget v25, v25, v6

    invoke-virtual/range {v22 .. v25}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeBytes([BII)V

    .line 380
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->finishDocument()V

    .line 381
    add-int/lit8 v8, v8, 0x1

    .line 382
    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    move-object/from16 v22, v0

    const-wide v24, 0x4072c00000000000L    # 300.0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 376
    add-int/lit8 v22, v9, 0x1

    move/from16 v0, v22

    move/from16 v1, v18

    invoke-static {v0, v15, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->nextLiveDoc(ILorg/apache/lucene/util/Bits;I)I

    move-result v9

    .end local v6    # "diff":I
    :cond_b
    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move/from16 v22, v0

    iget v0, v14, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move/from16 v0, v22

    if-lt v9, v0, :cond_a

    goto/16 :goto_3
.end method

.method public startDocument(I)V
    .locals 3
    .param p1, "numStoredFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numStoredFields:[I

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 145
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    .line 146
    .local v0, "newLength":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numStoredFields:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numStoredFields:[I

    .line 147
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->endOffsets:[I

    .line 149
    .end local v0    # "newLength":I
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numStoredFields:[I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    aput p1, v1, v2

    .line 150
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->numBufferedDocs:I

    .line 151
    return-void
.end method

.method public writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/IndexableField;)V
    .locals 10
    .param p1, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "field"    # Lorg/apache/lucene/index/IndexableField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    const/4 v0, 0x0

    .line 236
    .local v0, "bits":I
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v4

    .line 237
    .local v4, "number":Ljava/lang/Number;
    if-eqz v4, :cond_6

    .line 238
    instance-of v6, v4, Ljava/lang/Byte;

    if-nez v6, :cond_0

    instance-of v6, v4, Ljava/lang/Short;

    if-nez v6, :cond_0

    instance-of v6, v4, Ljava/lang/Integer;

    if-eqz v6, :cond_2

    .line 239
    :cond_0
    const/4 v0, 0x2

    .line 249
    :goto_0
    const/4 v5, 0x0

    .line 250
    .local v5, "string":Ljava/lang/String;
    const/4 v1, 0x0

    .line 265
    .local v1, "bytes":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    :goto_1
    iget v6, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    int-to-long v6, v6

    sget v8, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_BITS:I

    shl-long/2addr v6, v8

    int-to-long v8, v0

    or-long v2, v6, v8

    .line 266
    .local v2, "infoAndBits":J
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-virtual {v6, v2, v3}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeVLong(J)V

    .line 268
    if-eqz v1, :cond_8

    .line 269
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget v7, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeVInt(I)V

    .line 270
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    iget-object v7, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v9, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v6, v7, v8, v9}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeBytes([BII)V

    .line 286
    :goto_2
    return-void

    .line 240
    .end local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    .end local v2    # "infoAndBits":J
    .end local v5    # "string":Ljava/lang/String;
    :cond_2
    instance-of v6, v4, Ljava/lang/Long;

    if-eqz v6, :cond_3

    .line 241
    const/4 v0, 0x4

    .line 242
    goto :goto_0

    :cond_3
    instance-of v6, v4, Ljava/lang/Float;

    if-eqz v6, :cond_4

    .line 243
    const/4 v0, 0x3

    .line 244
    goto :goto_0

    :cond_4
    instance-of v6, v4, Ljava/lang/Double;

    if-eqz v6, :cond_5

    .line 245
    const/4 v0, 0x5

    .line 246
    goto :goto_0

    .line 247
    :cond_5
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cannot store numeric type "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 252
    :cond_6
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->binaryValue()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 253
    .restart local v1    # "bytes":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_7

    .line 254
    const/4 v0, 0x1

    .line 255
    const/4 v5, 0x0

    .line 256
    .restart local v5    # "string":Ljava/lang/String;
    goto :goto_1

    .line 257
    .end local v5    # "string":Ljava/lang/String;
    :cond_7
    const/4 v0, 0x0

    .line 258
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v5

    .line 259
    .restart local v5    # "string":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 260
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "field "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is stored but does not have binaryValue, stringValue nor numericValue"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 271
    .restart local v2    # "infoAndBits":J
    :cond_8
    if-eqz v5, :cond_9

    .line 272
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 274
    :cond_9
    instance-of v6, v4, Ljava/lang/Byte;

    if-nez v6, :cond_a

    instance-of v6, v4, Ljava/lang/Short;

    if-nez v6, :cond_a

    instance-of v6, v4, Ljava/lang/Integer;

    if-eqz v6, :cond_b

    .line 275
    :cond_a
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeInt(I)V

    goto :goto_2

    .line 276
    :cond_b
    instance-of v6, v4, Ljava/lang/Long;

    if-eqz v6, :cond_c

    .line 277
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-virtual {v4}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeLong(J)V

    goto/16 :goto_2

    .line 278
    :cond_c
    instance-of v6, v4, Ljava/lang/Float;

    if-eqz v6, :cond_d

    .line 279
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-virtual {v4}, Ljava/lang/Number;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeInt(I)V

    goto/16 :goto_2

    .line 280
    :cond_d
    instance-of v6, v4, Ljava/lang/Double;

    if-eqz v6, :cond_e

    .line 281
    iget-object v6, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->bufferedDocs:Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;

    invoke-virtual {v4}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->writeLong(J)V

    goto/16 :goto_2

    .line 283
    :cond_e
    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "Cannot get here"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6
.end method
