.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;
.super Ljava/lang/Object;
.source "Lucene3xTermVectorsReader.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private fieldUpto:I

.field final synthetic this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I
    invoke-static {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->fieldUpto:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)[I

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->next()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/lang/String;
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I
    invoke-static {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->fieldUpto:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)[I

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;->access$2(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->fieldNumbers:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields;)[I

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->fieldUpto:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVFields$1;->fieldUpto:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    return-object v0

    .line 245
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 256
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
