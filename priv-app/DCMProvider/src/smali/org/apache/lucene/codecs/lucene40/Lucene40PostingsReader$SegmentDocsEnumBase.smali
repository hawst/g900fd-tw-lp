.class abstract Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;
.super Lorg/apache/lucene/index/DocsEnum;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "SegmentDocsEnumBase"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected accum:I

.field protected count:I

.field protected doc:I

.field protected final docs:[I

.field protected freq:I

.field final freqIn:Lorg/apache/lucene/store/IndexInput;

.field protected freqOffset:J

.field protected final freqs:[I

.field protected indexOmitsTF:Z

.field protected limit:I

.field protected final liveDocs:Lorg/apache/lucene/util/Bits;

.field protected maxBufferedDocId:I

.field protected ord:I

.field protected skipOffset:J

.field protected skipped:Z

.field skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

.field protected start:I

.field final startFreqIn:Lorg/apache/lucene/store/IndexInput;

.field protected storeOffsets:Z

.field protected storePayloads:Z

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 307
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/Bits;)V
    .locals 2
    .param p2, "startFreqIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    const/16 v1, 0x40

    .line 337
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 309
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->docs:[I

    .line 310
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqs:[I

    .line 338
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->startFreqIn:Lorg/apache/lucene/store/IndexInput;

    .line 339
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 340
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 342
    return-void
.end method

.method private final binarySearch(III[I)I
    .locals 3
    .param p1, "hi"    # I
    .param p2, "low"    # I
    .param p3, "target"    # I
    .param p4, "docs"    # [I

    .prologue
    .line 402
    :goto_0
    if-le p2, p1, :cond_0

    .line 414
    :goto_1
    add-int/lit8 v2, p2, -0x1

    return v2

    .line 403
    :cond_0
    add-int v2, p1, p2

    ushr-int/lit8 v1, v2, 0x1

    .line 404
    .local v1, "mid":I
    aget v0, p4, v1

    .line 405
    .local v0, "doc":I
    if-ge v0, p3, :cond_1

    .line 406
    add-int/lit8 p2, v1, 0x1

    .line 407
    goto :goto_0

    :cond_1
    if-le v0, p3, :cond_2

    .line 408
    add-int/lit8 p1, v1, -0x1

    .line 409
    goto :goto_0

    .line 410
    :cond_2
    move p2, v1

    .line 411
    goto :goto_1
.end method

.method private final fillDocs(I)I
    .locals 5
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 453
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 454
    .local v2, "freqIn":Lorg/apache/lucene/store/IndexInput;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->docs:[I

    .line 455
    .local v1, "docs":[I
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    .line 456
    .local v0, "docAc":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, p1, :cond_0

    .line 460
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    .line 461
    return p1

    .line 457
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    add-int/2addr v0, v4

    .line 458
    aput v0, v1, v3

    .line 456
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private final fillDocsAndFreqs(I)I
    .locals 7
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 465
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 466
    .local v3, "freqIn":Lorg/apache/lucene/store/IndexInput;
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->docs:[I

    .line 467
    .local v2, "docs":[I
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqs:[I

    .line 468
    .local v4, "freqs":[I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    .line 469
    .local v1, "docAc":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, p1, :cond_0

    .line 475
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    .line 476
    return p1

    .line 470
    :cond_0
    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 471
    .local v0, "code":I
    ushr-int/lit8 v6, v0, 0x1

    add-int/2addr v1, v6

    .line 472
    invoke-virtual {p0, v3, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->readFreq(Lorg/apache/lucene/store/IndexInput;I)I

    move-result v6

    aput v6, v4, v5

    .line 473
    aput v1, v2, v5

    .line 469
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private final skipTo(I)I
    .locals 11
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 481
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v1, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    sub-int v1, p1, v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->limit:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v2, v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipMinimum:I

    if-lt v1, v2, :cond_2

    .line 486
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    if-nez v1, :cond_0

    .line 488
    new-instance v1, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v3, v3, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->maxSkipLevels:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v4, v4, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    .line 491
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipped:Z

    if-nez v1, :cond_1

    .line 497
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqOffset:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipOffset:J

    add-long/2addr v2, v4

    .line 498
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqOffset:J

    const-wide/16 v6, 0x0

    .line 499
    iget v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->limit:I

    iget-boolean v9, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->storePayloads:Z

    iget-boolean v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->storeOffsets:Z

    .line 497
    invoke-virtual/range {v1 .. v10}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->init(JJJIZZ)V

    .line 501
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipped:Z

    .line 504
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->skipTo(I)I

    move-result v0

    .line 506
    .local v0, "newOrd":I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->ord:I

    if-le v0, v1, :cond_2

    .line 509
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->ord:I

    .line 510
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    .line 511
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getFreqPointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 514
    .end local v0    # "newOrd":I
    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->scanTo(I)I

    move-result v1

    return v1
.end method


# virtual methods
.method public final advance(I)I
    .locals 3
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    if-ge v0, v1, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->maxBufferedDocId:I

    if-lt v0, p1, :cond_1

    .line 388
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    sub-int/2addr v0, v1

    const/16 v1, 0x20

    if-le v0, v1, :cond_0

    .line 389
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->docs:[I

    invoke-direct {p0, v0, v1, p1, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->binarySearch(III[I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    .line 390
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->nextDoc()I

    move-result v0

    .line 398
    :goto_0
    return v0

    .line 392
    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->linearScan(I)I

    move-result v0

    goto :goto_0

    .line 396
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    .line 398
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipTo(I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 519
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->limit:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final docID()I
    .locals 1

    .prologue
    .line 381
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->doc:I

    return v0
.end method

.method public final freq()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freq:I

    return v0
.end method

.method protected abstract linearScan(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract nextUnreadDoc()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method final readFreq(Lorg/apache/lucene/store/IndexInput;I)I
    .locals 1
    .param p1, "freqIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 419
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    .line 420
    const/4 v0, 0x1

    .line 422
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    goto :goto_0
.end method

.method protected final refill()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v2, 0x7fffffff

    .line 431
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->nextUnreadDoc()I

    move-result v0

    .line 432
    .local v0, "doc":I
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    .line 433
    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    .line 434
    if-ne v0, v2, :cond_0

    .line 445
    :goto_0
    return v2

    .line 437
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->docs:[I

    array-length v3, v3

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->limit:I

    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->ord:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 438
    .local v1, "numDocs":I
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->ord:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->ord:I

    .line 439
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->indexOmitsTF:Z

    if-eqz v3, :cond_2

    .line 440
    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->fillDocs(I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    .line 444
    :goto_1
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    if-lez v3, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->docs:[I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    :cond_1
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->maxBufferedDocId:I

    move v2, v0

    .line 445
    goto :goto_0

    .line 442
    :cond_2
    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->fillDocsAndFreqs(I)I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    goto :goto_1
.end method

.method reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;)Lorg/apache/lucene/index/DocsEnum;
    .locals 7
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 346
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->indexOmitsTF:Z

    .line 347
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->storePayloads:Z

    .line 348
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->storeOffsets:Z

    .line 349
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqOffset:J

    .line 350
    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipOffset:J

    .line 355
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 356
    iget v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->limit:I

    .line 357
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->limit:I

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    .line 346
    goto :goto_0

    :cond_1
    move v0, v2

    .line 348
    goto :goto_1

    .line 358
    :cond_2
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->ord:I

    .line 359
    iput v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->doc:I

    .line 360
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->accum:I

    .line 362
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->skipped:Z

    .line 364
    iput v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->start:I

    .line 365
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->count:I

    .line 366
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freq:I

    .line 367
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->indexOmitsTF:Z

    if-eqz v0, :cond_3

    .line 368
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->freqs:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 370
    :cond_3
    iput v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;->maxBufferedDocId:I

    .line 371
    return-object p0
.end method

.method protected abstract scanTo(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
