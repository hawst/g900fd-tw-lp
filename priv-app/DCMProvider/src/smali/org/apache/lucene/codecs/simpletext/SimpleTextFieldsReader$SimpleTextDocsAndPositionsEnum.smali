.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "SimpleTextFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTextDocsAndPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private cost:I

.field private docID:I

.field private endOffset:I

.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private final inStart:Lorg/apache/lucene/store/IndexInput;

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private nextDocStart:J

.field private payload:Lorg/apache/lucene/util/BytesRef;

.field private readOffsets:Z

.field private readPositions:Z

.field private final scratch:Lorg/apache/lucene/util/BytesRef;

.field private final scratch2:Lorg/apache/lucene/util/BytesRef;

.field private final scratchUTF16:Lorg/apache/lucene/util/CharsRef;

.field private final scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

.field private startOffset:I

.field private tf:I

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 327
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 345
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 330
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    .line 333
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 334
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch2:Lorg/apache/lucene/util/BytesRef;

    .line 335
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 336
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    .line 346
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->inStart:Lorg/apache/lucene/store/IndexInput;

    .line 347
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->inStart:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    .line 348
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 424
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public canReuse(Lorg/apache/lucene/store/IndexInput;)Z
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 351
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->inStart:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 484
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->cost:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    return v0
.end method

.method public endOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 474
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->endOffset:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->tf:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public nextDoc()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 380
    const/4 v0, 0x1

    .line 381
    .local v0, "first":Z
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-wide v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->nextDocStart:J

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 382
    const-wide/16 v4, 0x0

    .line 384
    .local v4, "posStart":J
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    .line 385
    .local v2, "lineStart":J
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 387
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 388
    if-nez v0, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    invoke-interface {v1, v6}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 389
    :cond_1
    iput-wide v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->nextDocStart:J

    .line 390
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 391
    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    .line 416
    :goto_1
    return v1

    .line 393
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v1, v6, v7, v8}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 394
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget v6, v6, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v1, v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    .line 395
    iput v9, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->tf:I

    .line 396
    const/4 v0, 0x0

    .line 397
    goto :goto_0

    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 398
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v1, v6, v7, v8}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 399
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget-object v1, v1, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget v6, v6, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v1, v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->tf:I

    .line 400
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    .line 401
    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->POS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 403
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 405
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 407
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 410
    sget-boolean v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->TERM:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 411
    :cond_5
    if-nez v0, :cond_7

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    invoke-interface {v1, v6}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 412
    :cond_6
    iput-wide v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->nextDocStart:J

    .line 413
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 414
    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    goto/16 :goto_1

    .line 416
    :cond_7
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    goto/16 :goto_1
.end method

.method public nextPosition()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 430
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->readPositions:Z

    if-eqz v4, :cond_1

    .line 431
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 432
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->POS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "got line="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 433
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->POS:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->POS:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v4, v5, v6, v7}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 434
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    iget v5, v5, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v4, v8, v5}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v3

    .line 439
    .local v3, "pos":I
    :goto_0
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->readOffsets:Z

    if-eqz v4, :cond_4

    .line 440
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 441
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "got line="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 436
    .end local v3    # "pos":I
    :cond_1
    const/4 v3, -0x1

    .restart local v3    # "pos":I
    goto :goto_0

    .line 442
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v4, v5, v6, v7}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 443
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    iget v5, v5, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v4, v8, v5}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->startOffset:I

    .line 444
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 445
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "got line="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 446
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v4, v5, v6, v7}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 447
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    iget-object v4, v4, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratchUTF16_2:Lorg/apache/lucene/util/CharsRef;

    iget v5, v5, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v4, v8, v5}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->endOffset:I

    .line 450
    :cond_4
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    .line 451
    .local v0, "fp":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 452
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 453
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v2, v4, v5

    .line 454
    .local v2, "len":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v4, v4

    if-ge v4, v2, :cond_5

    .line 455
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch2:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 457
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-static {v4, v5, v6, v8, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 458
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iput v2, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 459
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->scratch2:Lorg/apache/lucene/util/BytesRef;

    iput-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 464
    .end local v2    # "len":I
    :goto_1
    return v3

    .line 461
    :cond_6
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 462
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_1
.end method

.method public reset(JLorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/FieldInfo$IndexOptions;I)Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;
    .locals 5
    .param p1, "fp"    # J
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .param p5, "docFreq"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 355
    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 356
    iput-wide p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->nextDocStart:J

    .line 357
    iput v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->docID:I

    .line 358
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {p4, v0}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->readPositions:Z

    .line 359
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {p4, v0}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->readOffsets:Z

    .line 360
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->readOffsets:Z

    if-nez v0, :cond_0

    .line 361
    iput v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->startOffset:I

    .line 362
    iput v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->endOffset:I

    .line 364
    :cond_0
    iput p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->cost:I

    .line 365
    return-object p0

    :cond_1
    move v0, v2

    .line 358
    goto :goto_0

    :cond_2
    move v1, v2

    .line 359
    goto :goto_1
.end method

.method public startOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 469
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsAndPositionsEnum;->startOffset:I

    return v0
.end method
