.class final Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4HighCompressor;
.super Lorg/apache/lucene/codecs/compressing/Compressor;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LZ4HighCompressor"
.end annotation


# instance fields
.field private final ht:Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/Compressor;-><init>()V

    .line 171
    new-instance v0, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4HighCompressor;->ht:Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;

    .line 172
    return-void
.end method


# virtual methods
.method public compress([BIILorg/apache/lucene/store/DataOutput;)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .param p4, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4HighCompressor;->ht:Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;

    invoke-static {p1, p2, p3, p4, v0}, Lorg/apache/lucene/codecs/compressing/LZ4;->compressHC([BIILorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/codecs/compressing/LZ4$HCHashTable;)V

    .line 178
    return-void
.end method
