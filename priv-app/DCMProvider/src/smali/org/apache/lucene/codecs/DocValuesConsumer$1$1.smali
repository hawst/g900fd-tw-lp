.class Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer$1;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field currentLiveDocs:Lorg/apache/lucene/util/Bits;

.field currentReader:Lorg/apache/lucene/index/AtomicReader;

.field currentValues:Lorg/apache/lucene/index/NumericDocValues;

.field docIDUpto:I

.field nextIsSet:Z

.field nextValue:J

.field readerUpto:I

.field final synthetic this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$1;

.field private final synthetic val$mergeState:Lorg/apache/lucene/index/MergeState;

.field private final synthetic val$toMerge:Ljava/util/List;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/DocValuesConsumer$1;Ljava/util/List;Lorg/apache/lucene/index/MergeState;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$1;

    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->val$toMerge:Ljava/util/List;

    iput-object p3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->val$mergeState:Lorg/apache/lucene/index/MergeState;

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    return-void
.end method

.method private setNext()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 148
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->val$toMerge:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 167
    :goto_1
    return v0

    .line 152
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    if-ne v0, v3, :cond_3

    .line 153
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    .line 154
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->val$toMerge:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->val$mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    .line 156
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->val$toMerge:Ljava/util/List;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->readerUpto:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/NumericDocValues;

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentValues:Lorg/apache/lucene/index/NumericDocValues;

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    .line 159
    :cond_2
    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    goto :goto_0

    .line 163
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    invoke-interface {v0, v3}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164
    :cond_4
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->nextIsSet:Z

    .line 165
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->currentValues:Lorg/apache/lucene/index/NumericDocValues;

    iget v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->nextValue:J

    .line 166
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    move v0, v2

    .line 167
    goto :goto_1

    .line 170
    :cond_5
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->docIDUpto:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->nextIsSet:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->setNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 137
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 140
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->nextIsSet:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 141
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->nextIsSet:Z

    .line 143
    iget-wide v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->nextValue:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$1$1;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
