.class public final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;
.super Lorg/apache/lucene/codecs/PostingsWriterBase;
.source "Lucene41PostingsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final DOC_CODEC:Ljava/lang/String; = "Lucene41PostingsWriterDoc"

.field static final PAY_CODEC:Ljava/lang/String; = "Lucene41PostingsWriterPay"

.field static final POS_CODEC:Ljava/lang/String; = "Lucene41PostingsWriterPos"

.field static final TERMS_CODEC:Ljava/lang/String; = "Lucene41PostingsWriterTerms"

.field static final VERSION_CURRENT:I = 0x0

.field static final VERSION_START:I = 0x0

.field static final maxSkipLevels:I = 0xa


# instance fields
.field private final bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

.field private docBufferUpto:I

.field private docCount:I

.field final docDeltaBuffer:[I

.field final docOut:Lorg/apache/lucene/store/IndexOutput;

.field private docTermStartFP:J

.field final encoded:[B

.field private fieldHasFreqs:Z

.field private fieldHasOffsets:Z

.field private fieldHasPayloads:Z

.field private fieldHasPositions:Z

.field private final forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

.field final freqBuffer:[I

.field private lastBlockDocID:I

.field private lastBlockPayFP:J

.field private lastBlockPayloadByteUpto:I

.field private lastBlockPosBufferUpto:I

.field private lastBlockPosFP:J

.field private lastDocID:I

.field private lastPosition:I

.field private lastStartOffset:I

.field final offsetLengthBuffer:[I

.field final offsetStartDeltaBuffer:[I

.field final payOut:Lorg/apache/lucene/store/IndexOutput;

.field private payTermStartFP:J

.field private payloadByteUpto:I

.field private payloadBytes:[B

.field final payloadLengthBuffer:[I

.field private final pendingTerms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;",
            ">;"
        }
    .end annotation
.end field

.field private posBufferUpto:I

.field final posDeltaBuffer:[I

.field final posOut:Lorg/apache/lucene/store/IndexOutput;

.field private posTermStartFP:J

.field private final skipWriter:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

.field private termsOut:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    .line 68
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 188
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;F)V

    .line 189
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;F)V
    .locals 11
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "acceptableOverheadRatio"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 119
    invoke-direct {p0}, Lorg/apache/lucene/codecs/PostingsWriterBase;-><init>()V

    .line 369
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->pendingTerms:Ljava/util/List;

    .line 527
    new-instance v0, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    .line 121
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    const-string v3, "doc"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    .line 121
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    .line 123
    const/4 v5, 0x0

    .line 124
    .local v5, "posOut":Lorg/apache/lucene/store/IndexOutput;
    const/4 v6, 0x0

    .line 125
    .local v6, "payOut":Lorg/apache/lucene/store/IndexOutput;
    const/4 v7, 0x0

    .line 127
    .local v7, "success":Z
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    const-string v1, "Lucene41PostingsWriterDoc"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 128
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-direct {v0, p2, v1}, Lorg/apache/lucene/codecs/lucene41/ForUtil;-><init>(FLorg/apache/lucene/store/DataOutput;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    .line 129
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 130
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posDeltaBuffer:[I

    .line 131
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    const-string v3, "pos"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    .line 131
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v5

    .line 133
    const-string v0, "Lucene41PostingsWriterPos"

    const/4 v1, 0x0

    invoke-static {v5, v0, v1}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 135
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasPayloads()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    .line 137
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I

    .line 143
    :goto_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasOffsets()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 144
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetStartDeltaBuffer:[I

    .line 145
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetLengthBuffer:[I

    .line 151
    :goto_1
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasPayloads()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos;->hasOffsets()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    const-string v3, "pay"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 153
    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    .line 152
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    .line 154
    const-string v0, "Lucene41PostingsWriterPay"

    const/4 v1, 0x0

    invoke-static {v6, v0, v1}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 163
    :cond_1
    :goto_2
    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    .line 164
    iput-object v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    const/4 v7, 0x1

    .line 167
    if-nez v7, :cond_2

    new-array v0, v10, [Ljava/io/Closeable;

    .line 168
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v1, v0, v4

    aput-object v5, v0, v8

    aput-object v6, v0, v9

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 172
    :cond_2
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docDeltaBuffer:[I

    .line 173
    sget v0, Lorg/apache/lucene/codecs/lucene41/ForUtil;->MAX_DATA_SIZE:I

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->freqBuffer:[I

    .line 176
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

    const/16 v1, 0xa

    .line 177
    const/16 v2, 0x80

    .line 178
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    .line 179
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    .line 181
    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;-><init>(IIILorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;)V

    .line 176
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->skipWriter:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

    .line 183
    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    .line 184
    return-void

    .line 139
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    .line 167
    if-nez v7, :cond_4

    new-array v1, v10, [Ljava/io/Closeable;

    .line 168
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v1, v4

    aput-object v5, v1, v8

    aput-object v6, v1, v9

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 170
    :cond_4
    throw v0

    .line 147
    :cond_5
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetStartDeltaBuffer:[I

    .line 148
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetLengthBuffer:[I

    goto/16 :goto_1

    .line 157
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posDeltaBuffer:[I

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetStartDeltaBuffer:[I

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetLengthBuffer:[I

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method


# virtual methods
.method public addPosition(ILorg/apache/lucene/util/BytesRef;II)V
    .locals 6
    .param p1, "position"    # I
    .param p2, "payload"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "startOffset"    # I
    .param p4, "endOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 284
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posDeltaBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastPosition:I

    sub-int v2, p1, v2

    aput v2, v0, v1

    .line 285
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    if-eqz v0, :cond_1

    .line 286
    if-eqz p2, :cond_0

    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v0, :cond_2

    .line 288
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    aput v5, v0, v1

    .line 299
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    if-eqz v0, :cond_6

    .line 300
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastStartOffset:I

    if-ge p3, v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 290
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    iget v2, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    aput v2, v0, v1

    .line 291
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_3

    .line 292
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    iget v2, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    .line 294
    :cond_3
    iget-object v0, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    iget v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 295
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    goto :goto_0

    .line 301
    :cond_4
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-ge p4, p3, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 302
    :cond_5
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetStartDeltaBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastStartOffset:I

    sub-int v2, p3, v2

    aput v2, v0, v1

    .line 303
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetLengthBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    sub-int v2, p4, p3

    aput v2, v0, v1

    .line 304
    iput p3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastStartOffset:I

    .line 307
    :cond_6
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    .line 308
    iput p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastPosition:I

    .line 309
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 313
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posDeltaBuffer:[I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V

    .line 315
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    if-eqz v0, :cond_7

    .line 316
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V

    .line 317
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 318
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    invoke-virtual {v0, v1, v5, v2}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 319
    iput v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    .line 321
    :cond_7
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    if-eqz v0, :cond_8

    .line 322
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetStartDeltaBuffer:[I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V

    .line 323
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetLengthBuffer:[I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V

    .line 325
    :cond_8
    iput v5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    .line 327
    :cond_9
    return-void
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 583
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 582
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method public finishDoc()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 334
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    const/16 v1, 0x80

    if-ne v0, v1, :cond_2

    .line 335
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastDocID:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockDocID:I

    .line 336
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPayFP:J

    .line 340
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPosFP:J

    .line 341
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPosBufferUpto:I

    .line 342
    iget v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPayloadByteUpto:I

    .line 347
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    .line 349
    :cond_2
    return-void
.end method

.method public finishTerm(Lorg/apache/lucene/codecs/TermStats;)V
    .locals 25
    .param p1, "stats"    # Lorg/apache/lucene/codecs/TermStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 374
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 378
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    if-eq v3, v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    move-object/from16 v0, p1

    iget v5, v0, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " vs "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 392
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 394
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docDeltaBuffer:[I

    const/4 v4, 0x0

    aget v14, v3, v4

    .line 414
    .local v14, "singletonDocID":I
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPositions:Z

    if-eqz v3, :cond_10

    .line 423
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 396
    .end local v14    # "singletonDocID":I
    :cond_3
    const/4 v14, -0x1

    .line 398
    .restart local v14    # "singletonDocID":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    move/from16 v0, v17

    if-ge v0, v3, :cond_2

    .line 399
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docDeltaBuffer:[I

    aget v15, v3, v17

    .line 400
    .local v15, "docDelta":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->freqBuffer:[I

    aget v16, v3, v17

    .line 401
    .local v16, "freq":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasFreqs:Z

    if-nez v3, :cond_4

    .line 402
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v15}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 398
    :goto_1
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 403
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->freqBuffer:[I

    aget v3, v3, v17

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 404
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v4, v15, 0x1

    or-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_1

    .line 406
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v4, v15, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 407
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_1

    .line 424
    .end local v15    # "docDelta":I
    .end local v16    # "freq":I
    .end local v17    # "i":I
    :cond_6
    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    const-wide/16 v6, 0x80

    cmp-long v3, v4, v6

    if-lez v3, :cond_7

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posTermStartFP:J

    sub-long v12, v4, v6

    .line 430
    .local v12, "lastPosBlockOffset":J
    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    if-lez v3, :cond_f

    .line 437
    const/16 v19, -0x1

    .line 438
    .local v19, "lastPayloadLength":I
    const/16 v18, -0x1

    .line 439
    .local v18, "lastOffsetLength":I
    const/16 v21, 0x0

    .line 440
    .local v21, "payloadBytesReadUpto":I
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    move/from16 v0, v17

    if-lt v0, v3, :cond_8

    .line 483
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    if-eqz v3, :cond_f

    .line 484
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_e

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    move/from16 v0, v21

    if-eq v0, v3, :cond_e

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 428
    .end local v12    # "lastPosBlockOffset":J
    .end local v17    # "i":I
    .end local v18    # "lastOffsetLength":I
    .end local v19    # "lastPayloadLength":I
    .end local v21    # "payloadBytesReadUpto":I
    :cond_7
    const-wide/16 v12, -0x1

    .restart local v12    # "lastPosBlockOffset":J
    goto :goto_2

    .line 441
    .restart local v17    # "i":I
    .restart local v18    # "lastOffsetLength":I
    .restart local v19    # "lastPayloadLength":I
    .restart local v21    # "payloadBytesReadUpto":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posDeltaBuffer:[I

    aget v23, v3, v17

    .line 442
    .local v23, "posDelta":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    if-eqz v3, :cond_c

    .line 443
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadLengthBuffer:[I

    aget v22, v3, v17

    .line 444
    .local v22, "payloadLength":I
    move/from16 v0, v22

    move/from16 v1, v19

    if-eq v0, v1, :cond_b

    .line 445
    move/from16 v19, v22

    .line 446
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v4, v23, 0x1

    or-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 447
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 456
    :goto_4
    if-eqz v22, :cond_9

    .line 460
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadBytes:[B

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v3, v4, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 461
    add-int v21, v21, v22

    .line 467
    .end local v22    # "payloadLength":I
    :cond_9
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    if-eqz v3, :cond_a

    .line 471
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetStartDeltaBuffer:[I

    aget v2, v3, v17

    .line 472
    .local v2, "delta":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->offsetLengthBuffer:[I

    aget v20, v3, v17

    .line 473
    .local v20, "length":I
    move/from16 v0, v20

    move/from16 v1, v18

    if-ne v0, v1, :cond_d

    .line 474
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v4, v2, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 440
    .end local v2    # "delta":I
    .end local v20    # "length":I
    :cond_a
    :goto_6
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3

    .line 449
    .restart local v22    # "payloadLength":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v4, v23, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_4

    .line 464
    .end local v22    # "payloadLength":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_5

    .line 476
    .restart local v2    # "delta":I
    .restart local v20    # "length":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v4, v2, 0x1

    or-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 477
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 478
    move/from16 v18, v20

    goto :goto_6

    .line 485
    .end local v2    # "delta":I
    .end local v20    # "length":I
    .end local v23    # "posDelta":I
    :cond_e
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payloadByteUpto:I

    .line 496
    .end local v17    # "i":I
    .end local v18    # "lastOffsetLength":I
    .end local v19    # "lastPayloadLength":I
    .end local v21    # "payloadBytesReadUpto":I
    :cond_f
    :goto_7
    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    const/16 v4, 0x80

    if-le v3, v4, :cond_11

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->skipWriter:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->writeSkip(Lorg/apache/lucene/store/IndexOutput;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docTermStartFP:J

    sub-long v10, v4, v6

    .line 510
    .local v10, "skipOffset":J
    :goto_8
    move-object/from16 v0, p1

    iget-wide v4, v0, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    const-wide/16 v6, 0x80

    cmp-long v3, v4, v6

    if-ltz v3, :cond_12

    .line 511
    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payTermStartFP:J

    .line 520
    .local v8, "payStartFP":J
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->pendingTerms:Ljava/util/List;

    move-object/from16 v24, v0

    new-instance v3, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docTermStartFP:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posTermStartFP:J

    invoke-direct/range {v3 .. v14}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;-><init>(JJJJJI)V

    move-object/from16 v0, v24

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 521
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    .line 522
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posBufferUpto:I

    .line 523
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastDocID:I

    .line 524
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    .line 525
    return-void

    .line 492
    .end local v8    # "payStartFP":J
    .end local v10    # "skipOffset":J
    .end local v12    # "lastPosBlockOffset":J
    :cond_10
    const-wide/16 v12, -0x1

    .restart local v12    # "lastPosBlockOffset":J
    goto :goto_7

    .line 503
    :cond_11
    const-wide/16 v10, -0x1

    .restart local v10    # "skipOffset":J
    goto :goto_8

    .line 513
    :cond_12
    const-wide/16 v8, -0x1

    .restart local v8    # "payStartFP":J
    goto :goto_9
.end method

.method public flushTermsBlock(II)V
    .locals 16
    .param p1, "start"    # I
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 532
    if-nez p2, :cond_0

    .line 533
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->termsOut:Lorg/apache/lucene/store/IndexOutput;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 578
    :goto_0
    return-void

    .line 537
    :cond_0
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v11, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->pendingTerms:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    move/from16 v0, p1

    if-le v0, v11, :cond_1

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 538
    :cond_1
    sget-boolean v11, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->$assertionsDisabled:Z

    if-nez v11, :cond_2

    move/from16 v0, p2

    move/from16 v1, p1

    if-le v0, v1, :cond_2

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 540
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->pendingTerms:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    sub-int v11, v11, p1

    add-int v3, v11, p2

    .line 542
    .local v3, "limit":I
    const-wide/16 v4, 0x0

    .line 543
    .local v4, "lastDocStartFP":J
    const-wide/16 v8, 0x0

    .line 544
    .local v8, "lastPosStartFP":J
    const-wide/16 v6, 0x0

    .line 545
    .local v6, "lastPayStartFP":J
    sub-int v2, v3, p2

    .local v2, "idx":I
    :goto_1
    if-lt v2, v3, :cond_3

    .line 572
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->termsOut:Lorg/apache/lucene/store/IndexOutput;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v12}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v12

    long-to-int v12, v12

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 573
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->termsOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 574
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v11}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 577
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->pendingTerms:Ljava/util/List;

    sub-int v12, v3, p2

    invoke-interface {v11, v12, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 546
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->pendingTerms:Ljava/util/List;

    invoke-interface {v11, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;

    .line 548
    .local v10, "term":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;
    iget v11, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->singletonDocID:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_8

    .line 549
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->docStartFP:J

    sub-long/2addr v12, v4

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 550
    iget-wide v4, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->docStartFP:J

    .line 555
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPositions:Z

    if-eqz v11, :cond_6

    .line 556
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->posStartFP:J

    sub-long/2addr v12, v8

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 557
    iget-wide v8, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->posStartFP:J

    .line 558
    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->lastPosBlockOffset:J

    const-wide/16 v14, -0x1

    cmp-long v11, v12, v14

    if-eqz v11, :cond_4

    .line 559
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->lastPosBlockOffset:J

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 561
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    if-nez v11, :cond_5

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    if-eqz v11, :cond_6

    :cond_5
    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->payStartFP:J

    const-wide/16 v14, -0x1

    cmp-long v11, v12, v14

    if-eqz v11, :cond_6

    .line 562
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->payStartFP:J

    sub-long/2addr v12, v6

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 563
    iget-wide v6, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->payStartFP:J

    .line 567
    :cond_6
    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->skipOffset:J

    const-wide/16 v14, -0x1

    cmp-long v11, v12, v14

    if-eqz v11, :cond_7

    .line 568
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-wide v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->skipOffset:J

    invoke-virtual {v11, v12, v13}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 545
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 552
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget v12, v10, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter$PendingTerm;->singletonDocID:I

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    goto :goto_2
.end method

.method public setField(Lorg/apache/lucene/index/FieldInfo;)V
    .locals 5
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 200
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    .line 201
    .local v0, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-ltz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasFreqs:Z

    .line 202
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-ltz v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPositions:Z

    .line 203
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-ltz v1, :cond_2

    :goto_2
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    .line 204
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    .line 205
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->skipWriter:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPositions:Z

    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->setField(ZZZ)V

    .line 206
    return-void

    :cond_0
    move v1, v3

    .line 201
    goto :goto_0

    :cond_1
    move v1, v3

    .line 202
    goto :goto_1

    :cond_2
    move v2, v3

    .line 203
    goto :goto_2
.end method

.method public start(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 2
    .param p1, "termsOut"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->termsOut:Lorg/apache/lucene/store/IndexOutput;

    .line 194
    const-string v0, "Lucene41PostingsWriterTerms"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 195
    const/16 v0, 0x80

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 196
    return-void
.end method

.method public startDoc(II)V
    .locals 11
    .param p1, "docID"    # I
    .param p2, "termDocFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 233
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockDocID:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    if-nez v1, :cond_0

    .line 237
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->skipWriter:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockDocID:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPosFP:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPayFP:J

    iget v8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPosBufferUpto:I

    iget v9, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockPayloadByteUpto:I

    invoke-virtual/range {v1 .. v9}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->bufferSkip(IIJJII)V

    .line 240
    :cond_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastDocID:I

    sub-int v0, p1, v1

    .line 242
    .local v0, "docDelta":I
    if-ltz p1, :cond_1

    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    if-lez v1, :cond_2

    if-gtz v0, :cond_2

    .line 243
    :cond_1
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "docs out of order ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastDocID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) (docOut: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 246
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docDeltaBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    aput v0, v1, v2

    .line 250
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasFreqs:Z

    if-eqz v1, :cond_3

    .line 251
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->freqBuffer:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    aput p2, v1, v2

    .line 253
    :cond_3
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    .line 254
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docCount:I

    .line 256
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docBufferUpto:I

    const/16 v2, 0x80

    if-ne v1, v2, :cond_4

    .line 260
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docDeltaBuffer:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V

    .line 261
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasFreqs:Z

    if-eqz v1, :cond_4

    .line 265
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->freqBuffer:[I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->encoded:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene41/ForUtil;->writeBlock([I[BLorg/apache/lucene/store/IndexOutput;)V

    .line 273
    :cond_4
    iput p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastDocID:I

    .line 274
    iput v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastPosition:I

    .line 275
    iput v10, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastStartOffset:I

    .line 276
    return-void
.end method

.method public startTerm()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->docTermStartFP:J

    .line 211
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPositions:Z

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->posTermStartFP:J

    .line 213
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasPayloads:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->fieldHasOffsets:Z

    if-eqz v0, :cond_1

    .line 214
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->payTermStartFP:J

    .line 217
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastDocID:I

    .line 218
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->lastBlockDocID:I

    .line 222
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsWriter;->skipWriter:Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->resetSkip()V

    .line 223
    return-void
.end method
