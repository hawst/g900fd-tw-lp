.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;
.super Ljava/lang/Object;
.source "Lucene3xNormsProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NormsDocValues"
.end annotation


# instance fields
.field private final file:Lorg/apache/lucene/store/IndexInput;

.field private instance:Lorg/apache/lucene/index/NumericDocValues;

.field private final offset:J

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;Lorg/apache/lucene/store/IndexInput;J)V
    .locals 1
    .param p2, "normInput"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "normSeek"    # J

    .prologue
    .line 165
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    .line 167
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->offset:J

    .line 168
    return-void
.end method


# virtual methods
.method declared-synchronized getInstance()Lorg/apache/lucene/index/NumericDocValues;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->instance:Lorg/apache/lucene/index/NumericDocValues;

    if-nez v1, :cond_1

    .line 172
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;

    iget v1, v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->maxdoc:I

    new-array v0, v1, [B

    .line 174
    .local v0, "bytes":[B
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 175
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->offset:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 176
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x0

    array-length v4, v0

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v3, v4, v5}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 174
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;

    iget-object v2, v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    if-eq v1, v2, :cond_0

    .line 180
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;

    iget-object v1, v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 181
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->file:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 183
    :cond_0
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues$1;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;[B)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->instance:Lorg/apache/lucene/index/NumericDocValues;

    .line 190
    .end local v0    # "bytes":[B
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->instance:Lorg/apache/lucene/index/NumericDocValues;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object v1

    .line 174
    .restart local v0    # "bytes":[B
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 171
    .end local v0    # "bytes":[B
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method
