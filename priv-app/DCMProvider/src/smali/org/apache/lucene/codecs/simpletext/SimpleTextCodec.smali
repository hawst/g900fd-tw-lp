.class public final Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;
.super Lorg/apache/lucene/codecs/Codec;
.source "SimpleTextCodec.java"


# instance fields
.field private final dvFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

.field private final liveDocs:Lorg/apache/lucene/codecs/LiveDocsFormat;

.field private final normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

.field private final postings:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final segmentInfos:Lorg/apache/lucene/codecs/SegmentInfoFormat;

.field private final storedFields:Lorg/apache/lucene/codecs/StoredFieldsFormat;

.field private final vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "SimpleText"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 37
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextPostingsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextPostingsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->postings:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 38
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->storedFields:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 39
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->segmentInfos:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    .line 40
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldInfosFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    .line 41
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    .line 42
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    .line 43
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->liveDocs:Lorg/apache/lucene/codecs/LiveDocsFormat;

    .line 44
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->dvFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 48
    return-void
.end method


# virtual methods
.method public docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->dvFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    return-object v0
.end method

.method public liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->liveDocs:Lorg/apache/lucene/codecs/LiveDocsFormat;

    return-object v0
.end method

.method public normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    return-object v0
.end method

.method public postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->postings:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->segmentInfos:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    return-object v0
.end method

.method public storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->storedFields:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    return-object v0
.end method

.method public termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextCodec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    return-object v0
.end method
