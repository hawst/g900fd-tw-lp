.class Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer$3;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field currentOrd:I

.field final scratch:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$3;

.field private final synthetic val$dvs:[Lorg/apache/lucene/index/SortedDocValues;

.field private final synthetic val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/DocValuesConsumer$3;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;[Lorg/apache/lucene/index/SortedDocValues;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$3;

    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iput-object p3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->val$dvs:[Lorg/apache/lucene/index/SortedDocValues;

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->scratch:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 4

    .prologue
    .line 297
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->currentOrd:I

    int-to-long v0, v0

    iget-object v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {v2}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getValueCount()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 6

    .prologue
    .line 302
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 303
    new-instance v2, Ljava/util/NoSuchElementException;

    invoke-direct {v2}, Ljava/util/NoSuchElementException;-><init>()V

    throw v2

    .line 305
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->currentOrd:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentNumber(J)I

    move-result v0

    .line 306
    .local v0, "segmentNumber":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->currentOrd:I

    int-to-long v4, v3

    invoke-virtual {v2, v0, v4, v5}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentOrd(IJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 307
    .local v1, "segmentOrd":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->val$dvs:[Lorg/apache/lucene/index/SortedDocValues;

    aget-object v2, v2, v0

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v1, v3}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 308
    iget v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->currentOrd:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->currentOrd:I

    .line 309
    iget-object v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$3$1;->scratch:Lorg/apache/lucene/util/BytesRef;

    return-object v2
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 314
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
