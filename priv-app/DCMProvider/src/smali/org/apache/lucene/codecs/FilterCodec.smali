.class public abstract Lorg/apache/lucene/codecs/FilterCodec;
.super Lorg/apache/lucene/codecs/Codec;
.source "FilterCodec.java"


# instance fields
.field protected final delegate:Lorg/apache/lucene/codecs/Codec;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lorg/apache/lucene/codecs/Codec;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "delegate"    # Lorg/apache/lucene/codecs/Codec;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    .line 60
    return-void
.end method


# virtual methods
.method public docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v0

    return-object v0
.end method

.method public fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;

    move-result-object v0

    return-object v0
.end method

.method public liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v0

    return-object v0
.end method

.method public normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->normsFormat()Lorg/apache/lucene/codecs/NormsFormat;

    move-result-object v0

    return-object v0
.end method

.method public postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v0

    return-object v0
.end method

.method public segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;

    move-result-object v0

    return-object v0
.end method

.method public storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;

    move-result-object v0

    return-object v0
.end method

.method public termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/codecs/FilterCodec;->delegate:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;

    move-result-object v0

    return-object v0
.end method
