.class public Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;
.super Lorg/apache/lucene/codecs/LiveDocsFormat;
.source "Lucene40LiveDocsFormat.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final DELETES_EXTENSION:Ljava/lang/String; = "del"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;->$assertionsDisabled:Z

    .line 68
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/codecs/LiveDocsFormat;-><init>()V

    .line 72
    return-void
.end method


# virtual methods
.method public files(Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/util/Collection;)V
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    .local p2, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v1, "del"

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_0
    return-void
.end method

.method public newLiveDocs(I)Lorg/apache/lucene/util/MutableBits;
    .locals 1
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/BitVector;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/lucene40/BitVector;-><init>(I)V

    .line 77
    .local v0, "bitVector":Lorg/apache/lucene/codecs/lucene40/BitVector;
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->invertAll()V

    .line 78
    return-object v0
.end method

.method public newLiveDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/MutableBits;
    .locals 2
    .param p1, "existing"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/codecs/lucene40/BitVector;

    .line 84
    .local v0, "liveDocs":Lorg/apache/lucene/codecs/lucene40/BitVector;
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->clone()Lorg/apache/lucene/codecs/lucene40/BitVector;

    move-result-object v1

    return-object v1
.end method

.method public readLiveDocs(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/util/Bits;
    .locals 6
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v2, p2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v3, "del"

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "filename":Ljava/lang/String;
    new-instance v1, Lorg/apache/lucene/codecs/lucene40/BitVector;

    invoke-direct {v1, p1, v0, p3}, Lorg/apache/lucene/codecs/lucene40/BitVector;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    .line 91
    .local v1, "liveDocs":Lorg/apache/lucene/codecs/lucene40/BitVector;
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v2

    iget-object v3, p2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v4

    sub-int/2addr v3, v4

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    .line 92
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "liveDocs.count()="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " info.docCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " info.getDelCount()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 93
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->length()I

    move-result v2

    iget-object v3, p2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    if-eq v2, v3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 94
    :cond_1
    return-object v1
.end method

.method public writeLiveDocs(Lorg/apache/lucene/util/MutableBits;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V
    .locals 6
    .param p1, "bits"    # Lorg/apache/lucene/util/MutableBits;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p4, "newDelCount"    # I
    .param p5, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v2, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v3, "del"

    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getNextDelGen()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .local v0, "filename":Ljava/lang/String;
    move-object v1, p1

    .line 100
    check-cast v1, Lorg/apache/lucene/codecs/lucene40/BitVector;

    .line 101
    .local v1, "liveDocs":Lorg/apache/lucene/codecs/lucene40/BitVector;
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v2

    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {p3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, p4

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 102
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->length()I

    move-result v2

    iget-object v3, p3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    if-eq v2, v3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 103
    :cond_1
    invoke-virtual {v1, p2, v0, p5}, Lorg/apache/lucene/codecs/lucene40/BitVector;->write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    .line 104
    return-void
.end method
