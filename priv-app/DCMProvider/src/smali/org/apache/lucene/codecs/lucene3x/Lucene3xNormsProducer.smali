.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;
.super Lorg/apache/lucene/codecs/DocValuesProducer;
.source "Lucene3xNormsProducer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final NORMS_EXTENSION:Ljava/lang/String; = "nrm"

.field static final NORMS_HEADER:[B

.field static final SEPARATE_NORMS_EXTENSION:Ljava/lang/String; = "s"


# instance fields
.field final maxdoc:I

.field final norms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;",
            ">;"
        }
    .end annotation
.end field

.field final openFiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/store/IndexInput;",
            ">;"
        }
    .end annotation
.end field

.field singleNormStream:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->$assertionsDisabled:Z

    .line 51
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->NORMS_HEADER:[B

    .line 57
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :array_0
    .array-data 1
        0x4et
        0x52t
        0x4dt
        -0x1t
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 24
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fields"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 59
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->norms:Ljava/util/Map;

    .line 62
    new-instance v19, Ljava/util/IdentityHashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static/range {v19 .. v19}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    .line 70
    move-object/from16 v0, p2

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 71
    .local v15, "separateNormsDir":Lorg/apache/lucene/store/Directory;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->maxdoc:I

    .line 72
    move-object/from16 v0, p2

    iget-object v14, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 73
    .local v14, "segmentName":Ljava/lang/String;
    const/16 v17, 0x0

    .line 75
    .local v17, "success":Z
    :try_start_0
    sget-object v19, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->NORMS_HEADER:[B

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v8, v0

    .line 76
    .local v8, "nextNormSeek":J
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_2

    .line 119
    sget-boolean v19, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->$assertionsDisabled:Z

    if-nez v19, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v20

    cmp-long v19, v8, v20

    if-eqz v19, :cond_a

    new-instance v20, Ljava/lang/AssertionError;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v21, "len: "

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v22

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v21, " expected: "

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    :goto_1
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    .end local v8    # "nextNormSeek":J
    :catchall_0
    move-exception v19

    .line 122
    if-nez v17, :cond_1

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 125
    :cond_1
    throw v19

    .line 76
    .restart local v8    # "nextNormSeek":J
    :cond_2
    :try_start_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/FieldInfo;

    .line 77
    .local v5, "fi":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 78
    iget v0, v5, Lorg/apache/lucene/index/FieldInfo;->number:I

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->getNormFilename(Lorg/apache/lucene/index/SegmentInfo;I)Ljava/lang/String;

    move-result-object v6

    .line 79
    .local v6, "fileName":Ljava/lang/String;
    iget v0, v5, Lorg/apache/lucene/index/FieldInfo;->number:I

    move/from16 v20, v0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->hasSeparateNorms(Lorg/apache/lucene/index/SegmentInfo;I)Z

    move-result v20

    if-eqz v20, :cond_4

    move-object v4, v15

    .line 82
    .local v4, "d":Lorg/apache/lucene/store/Directory;
    :goto_2
    const-string v20, "nrm"

    move-object/from16 v0, v20

    invoke-static {v6, v0}, Lorg/apache/lucene/index/IndexFileNames;->matchesExtension(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v16

    .line 83
    .local v16, "singleNormFile":Z
    const/4 v11, 0x0

    .line 86
    .local v11, "normInput":Lorg/apache/lucene/store/IndexInput;
    if-eqz v16, :cond_5

    .line 87
    move-wide v12, v8

    .line 88
    .local v12, "normSeek":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v20, v0

    if-nez v20, :cond_3

    .line 89
    move-object/from16 v0, p4

    invoke-virtual {v4, v6, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v21, v0

    invoke-interface/range {v20 .. v21}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->singleNormStream:Lorg/apache/lucene/store/IndexInput;

    .line 113
    :goto_3
    new-instance v10, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v11, v12, v13}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;Lorg/apache/lucene/store/IndexInput;J)V

    .line 114
    .local v10, "norm":Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->norms:Ljava/util/Map;

    move-object/from16 v20, v0

    iget-object v0, v5, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->maxdoc:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v8, v8, v20

    goto/16 :goto_0

    .end local v4    # "d":Lorg/apache/lucene/store/Directory;
    .end local v10    # "norm":Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;
    .end local v11    # "normInput":Lorg/apache/lucene/store/IndexInput;
    .end local v12    # "normSeek":J
    .end local v16    # "singleNormFile":Z
    :cond_4
    move-object/from16 v4, p1

    .line 79
    goto :goto_2

    .line 97
    .restart local v4    # "d":Lorg/apache/lucene/store/Directory;
    .restart local v11    # "normInput":Lorg/apache/lucene/store/IndexInput;
    .restart local v16    # "singleNormFile":Z
    :cond_5
    move-object/from16 v0, p4

    invoke-virtual {v4, v6, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v11

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v18

    .line 105
    .local v18, "version":Ljava/lang/String;
    if-eqz v18, :cond_6

    invoke-static {}, Lorg/apache/lucene/util/StringHelper;->getVersionComparator()Ljava/util/Comparator;

    move-result-object v20

    const-string v21, "3.2"

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v20

    if-gez v20, :cond_7

    .line 106
    :cond_6
    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v20

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->maxdoc:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    cmp-long v20, v20, v22

    if-nez v20, :cond_7

    .line 104
    const/4 v7, 0x1

    .line 107
    .local v7, "isUnversioned":Z
    :goto_4
    if-eqz v7, :cond_8

    .line 108
    const-wide/16 v12, 0x0

    .line 109
    .restart local v12    # "normSeek":J
    goto :goto_3

    .line 104
    .end local v7    # "isUnversioned":Z
    .end local v12    # "normSeek":J
    :cond_7
    const/4 v7, 0x0

    goto :goto_4

    .line 110
    .restart local v7    # "isUnversioned":Z
    :cond_8
    sget-object v20, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->NORMS_HEADER:[B

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-long v12, v0

    .restart local v12    # "normSeek":J
    goto :goto_3

    .line 119
    .end local v4    # "d":Lorg/apache/lucene/store/Directory;
    .end local v5    # "fi":Lorg/apache/lucene/index/FieldInfo;
    .end local v6    # "fileName":Ljava/lang/String;
    .end local v7    # "isUnversioned":Z
    .end local v11    # "normInput":Lorg/apache/lucene/store/IndexInput;
    .end local v12    # "normSeek":J
    .end local v16    # "singleNormFile":Z
    .end local v18    # "version":Ljava/lang/String;
    :cond_9
    const-string v19, "null"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 120
    :cond_a
    const/16 v17, 0x1

    .line 122
    if-nez v17, :cond_b

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Iterable;)V

    .line 126
    :cond_b
    return-void
.end method

.method private static getNormFilename(Lorg/apache/lucene/index/SegmentInfo;I)Ljava/lang/String;
    .locals 5
    .param p0, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p1, "number"    # I

    .prologue
    .line 139
    invoke-static {p0, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->hasSeparateNorms(Lorg/apache/lucene/index/SegmentInfo;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_PREFIX:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 141
    .local v0, "gen":J
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "s"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0, v1}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 144
    .end local v0    # "gen":J
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v3, ""

    const-string v4, "nrm"

    invoke-static {v2, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private static hasSeparateNorms(Lorg/apache/lucene/index/SegmentInfo;I)Z
    .locals 6
    .param p0, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p1, "number"    # I

    .prologue
    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->NORMGEN_PREFIX:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "v":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 151
    const/4 v1, 0x0

    .line 154
    :goto_0
    return v1

    .line 153
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 154
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close(Ljava/lang/Iterable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->norms:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 134
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 136
    return-void

    .line 132
    :catchall_0
    move-exception v0

    .line 133
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->norms:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 134
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->openFiles:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 135
    throw v0
.end method

.method public getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->norms:Ljava/util/Map;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;

    .line 197
    .local v0, "dv":Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 198
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsProducer$NormsDocValues;->getInstance()Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    return-object v1
.end method

.method public getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
