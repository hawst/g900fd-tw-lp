.class final Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
.super Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
.source "BlockTreeTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PendingTerm"
.end annotation


# instance fields
.field public final stats:Lorg/apache/lucene/codecs/TermStats;

.field public final term:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "stats"    # Lorg/apache/lucene/codecs/TermStats;

    .prologue
    .line 360
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;-><init>(Z)V

    .line 361
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    .line 362
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    .line 363
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
