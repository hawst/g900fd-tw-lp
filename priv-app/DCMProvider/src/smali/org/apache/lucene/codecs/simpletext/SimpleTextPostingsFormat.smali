.class public final Lorg/apache/lucene/codecs/simpletext/SimpleTextPostingsFormat;
.super Lorg/apache/lucene/codecs/PostingsFormat;
.source "SimpleTextPostingsFormat.java"


# static fields
.field static final POSTINGS_EXTENSION:Ljava/lang/String; = "pst"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "SimpleText"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/PostingsFormat;-><init>(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method static getPostingsFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "segment"    # Ljava/lang/String;
    .param p1, "segmentSuffix"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v0, "pst"

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;-><init>(Lorg/apache/lucene/index/SegmentWriteState;)V

    return-object v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;-><init>(Lorg/apache/lucene/index/SegmentReadState;)V

    return-object v0
.end method
