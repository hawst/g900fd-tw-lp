.class public abstract Lorg/apache/lucene/codecs/DocValuesFormat;
.super Ljava/lang/Object;
.source "DocValuesFormat.java"

# interfaces
.implements Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;"
    }
.end annotation


# static fields
.field private static final loader:Lorg/apache/lucene/util/NamedSPILoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/NamedSPILoader",
            "<",
            "Lorg/apache/lucene/codecs/DocValuesFormat;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lorg/apache/lucene/util/NamedSPILoader;

    const-class v1, Lorg/apache/lucene/codecs/DocValuesFormat;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/NamedSPILoader;-><init>(Ljava/lang/Class;)V

    .line 43
    sput-object v0, Lorg/apache/lucene/codecs/DocValuesFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    .line 44
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1}, Lorg/apache/lucene/util/NamedSPILoader;->checkServiceName(Ljava/lang/String;)V

    .line 62
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesFormat;->name:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public static availableDocValuesFormats()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    sget-object v0, Lorg/apache/lucene/codecs/DocValuesFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You called DocValuesFormat.availableDocValuesFormats() before all formats could be initialized. This likely happens if you call it from a DocValuesFormat\'s ctor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/DocValuesFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0}, Lorg/apache/lucene/util/NamedSPILoader;->availableServices()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    sget-object v0, Lorg/apache/lucene/codecs/DocValuesFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You called DocValuesFormat.forName() before all formats could be initialized. This likely happens if you call it from a DocValuesFormat\'s ctor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/DocValuesFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/NamedSPILoader;->lookup(Ljava/lang/String;)Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public static reloadDocValuesFormats(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p0, "classloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 121
    sget-object v0, Lorg/apache/lucene/codecs/DocValuesFormat;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/NamedSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 122
    return-void
.end method


# virtual methods
.method public abstract fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesFormat;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DocValuesFormat(name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/codecs/DocValuesFormat;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
