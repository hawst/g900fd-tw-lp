.class public Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;
.super Lorg/apache/lucene/codecs/SegmentInfoFormat;
.source "Lucene40SegmentInfoFormat.java"


# static fields
.field static final CODEC_NAME:Ljava/lang/String; = "Lucene40SegmentInfo"

.field public static final SI_EXTENSION:Ljava/lang/String; = "si"

.field static final VERSION_CURRENT:I

.field static final VERSION_START:I


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

.field private final writer:Lorg/apache/lucene/codecs/SegmentInfoWriter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoFormat;-><init>()V

    .line 72
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;->reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

    .line 73
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoWriter;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoWriter;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;->writer:Lorg/apache/lucene/codecs/SegmentInfoWriter;

    .line 77
    return-void
.end method


# virtual methods
.method public getSegmentInfoReader()Lorg/apache/lucene/codecs/SegmentInfoReader;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;->reader:Lorg/apache/lucene/codecs/SegmentInfoReader;

    return-object v0
.end method

.method public getSegmentInfoWriter()Lorg/apache/lucene/codecs/SegmentInfoWriter;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;->writer:Lorg/apache/lucene/codecs/SegmentInfoWriter;

    return-object v0
.end method
