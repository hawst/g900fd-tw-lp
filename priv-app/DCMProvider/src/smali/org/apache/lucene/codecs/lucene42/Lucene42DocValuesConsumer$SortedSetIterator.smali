.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;
.super Ljava/lang/Object;
.source "Lucene42DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SortedSetIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field buffer:[B

.field final counts:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field final ords:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field out:Lorg/apache/lucene/store/ByteArrayDataOutput;

.field ref:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method constructor <init>(Ljava/util/Iterator;Ljava/util/Iterator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Number;",
            ">;",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Number;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p1, "counts":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Number;>;"
    .local p2, "ords":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Number;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const/16 v0, 0xa

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->buffer:[B

    .line 249
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataOutput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->out:Lorg/apache/lucene/store/ByteArrayDataOutput;

    .line 250
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ref:Lorg/apache/lucene/util/BytesRef;

    .line 256
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->counts:Ljava/util/Iterator;

    .line 257
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ords:Ljava/util/Iterator;

    .line 258
    return-void
.end method

.method private encodeValues(I)V
    .locals 8
    .param p1, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->out:Lorg/apache/lucene/store/ByteArrayDataOutput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->buffer:[B

    invoke-virtual {v1, v6}, Lorg/apache/lucene/store/ByteArrayDataOutput;->reset([B)V

    .line 293
    const-wide/16 v2, 0x0

    .line 294
    .local v2, "lastOrd":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_0

    .line 299
    return-void

    .line 295
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ords:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    .line 296
    .local v4, "ord":J
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->out:Lorg/apache/lucene/store/ByteArrayDataOutput;

    sub-long v6, v4, v2

    invoke-virtual {v1, v6, v7}, Lorg/apache/lucene/store/ByteArrayDataOutput;->writeVLong(J)V

    .line 297
    move-wide v2, v4

    .line 294
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->counts:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 5

    .prologue
    .line 267
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 268
    new-instance v3, Ljava/util/NoSuchElementException;

    invoke-direct {v3}, Ljava/util/NoSuchElementException;-><init>()V

    throw v3

    .line 271
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->counts:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 272
    .local v1, "count":I
    mul-int/lit8 v2, v1, 0x9

    .line 273
    .local v2, "maxSize":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->buffer:[B

    array-length v3, v3

    if-le v2, v3, :cond_1

    .line 274
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->buffer:[B

    invoke-static {v3, v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->buffer:[B

    .line 278
    :cond_1
    :try_start_0
    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->encodeValues(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ref:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->buffer:[B

    iput-object v4, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 284
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ref:Lorg/apache/lucene/util/BytesRef;

    const/4 v4, 0x0

    iput v4, v3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 285
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ref:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->out:Lorg/apache/lucene/store/ByteArrayDataOutput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ByteArrayDataOutput;->getPosition()I

    move-result v4

    iput v4, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 287
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer$SortedSetIterator;->ref:Lorg/apache/lucene/util/BytesRef;

    return-object v3

    .line 279
    :catch_0
    move-exception v0

    .line 280
    .local v0, "bogus":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 303
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
