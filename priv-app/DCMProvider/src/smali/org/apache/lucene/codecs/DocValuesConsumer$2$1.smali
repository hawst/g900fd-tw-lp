.class Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer$2;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field currentLiveDocs:Lorg/apache/lucene/util/Bits;

.field currentReader:Lorg/apache/lucene/index/AtomicReader;

.field currentValues:Lorg/apache/lucene/index/BinaryDocValues;

.field docIDUpto:I

.field nextIsSet:Z

.field nextValue:Lorg/apache/lucene/util/BytesRef;

.field readerUpto:I

.field final synthetic this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$2;

.field private final synthetic val$mergeState:Lorg/apache/lucene/index/MergeState;

.field private final synthetic val$toMerge:Ljava/util/List;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/DocValuesConsumer$2;Ljava/util/List;Lorg/apache/lucene/index/MergeState;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$2;

    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->val$toMerge:Ljava/util/List;

    iput-object p3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->val$mergeState:Lorg/apache/lucene/index/MergeState;

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    .line 193
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextValue:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method private setNext()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 222
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->val$toMerge:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 241
    :goto_1
    return v0

    .line 226
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    if-ne v0, v3, :cond_3

    .line 227
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    .line 228
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->val$toMerge:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 229
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->val$mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v0, v0, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReader;

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    .line 230
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->val$toMerge:Ljava/util/List;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->readerUpto:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/BinaryDocValues;

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentValues:Lorg/apache/lucene/index/BinaryDocValues;

    .line 231
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    .line 233
    :cond_2
    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    goto :goto_0

    .line 237
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    iget v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    invoke-interface {v0, v3}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 238
    :cond_4
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextIsSet:Z

    .line 239
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->currentValues:Lorg/apache/lucene/index/BinaryDocValues;

    iget v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextValue:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1, v3}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 240
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    move v0, v2

    .line 241
    goto :goto_1

    .line 244
    :cond_5
    iget v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->docIDUpto:I

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextIsSet:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->setNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 214
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextIsSet:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 215
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextIsSet:Z

    .line 217
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$2$1;->nextValue:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
