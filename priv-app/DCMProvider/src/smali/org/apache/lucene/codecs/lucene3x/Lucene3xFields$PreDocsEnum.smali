.class final Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "Lucene3xFields.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PreDocsEnum"
.end annotation


# instance fields
.field private docID:I

.field private final docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 949
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 948
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    .line 950
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    iget-object v1, p1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;Lorg/apache/lucene/index/FieldInfos;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    .line 951
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 976
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipTo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 977
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    .line 979
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 995
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 990
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 985
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq()I

    move-result v0

    return v0
.end method

.method getFreqStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 967
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->next()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 968
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    .line 970
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
    .locals 2
    .param p1, "termEnum"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 958
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->setLiveDocs(Lorg/apache/lucene/util/Bits;)V

    .line 959
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->seek(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)V

    .line 960
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docs:Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    const/4 v1, 0x1

    iput v1, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    .line 961
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->docID:I

    .line 962
    return-object p0
.end method
