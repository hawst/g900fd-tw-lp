.class public final Lorg/apache/lucene/codecs/MappingMultiDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "MappingMultiDocsEnum.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field current:Lorg/apache/lucene/index/DocsEnum;

.field currentBase:I

.field currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

.field doc:I

.field private mergeState:Lorg/apache/lucene/index/MergeState;

.field numSubs:I

.field private subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

.field upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->doc:I

    .line 46
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public cost()J
    .locals 8

    .prologue
    .line 120
    const-wide/16 v0, 0x0

    .line 121
    .local v0, "cost":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 124
    return-wide v0

    .line 121
    :cond_0
    aget-object v2, v4, v3

    .line 122
    .local v2, "enumWithSlice":Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;
    iget-object v6, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocsEnum;->cost()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 121
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public getNumSubs()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->numSubs:I

    return v0
.end method

.method public getSubs()[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    return-object v0
.end method

.method public nextDoc()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v2, 0x7fffffff

    .line 91
    :cond_0
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    if-nez v3, :cond_2

    .line 92
    iget v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    iget v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->numSubs:I

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    .line 93
    iput v2, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->doc:I

    .line 111
    :goto_1
    return v2

    .line 95
    :cond_1
    iget v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    .line 96
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v1, v3, Lorg/apache/lucene/index/ReaderSlice;->readerIndex:I

    .line 97
    .local v1, "reader":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    iput-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    .line 98
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->docBase:[I

    aget v3, v3, v1

    iput v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->currentBase:I

    .line 99
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->mergeState:Lorg/apache/lucene/index/MergeState;

    iget-object v3, v3, Lorg/apache/lucene/index/MergeState;->docMaps:[Lorg/apache/lucene/index/MergeState$DocMap;

    aget-object v3, v3, v1

    iput-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

    .line 100
    sget-boolean v3, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

    invoke-virtual {v3}, Lorg/apache/lucene/index/MergeState$DocMap;->maxDoc()I

    move-result v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v5, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v4, v4, Lorg/apache/lucene/index/ReaderSlice;->length:I

    if-eq v3, v4, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "readerIndex="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " subs.len="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " len1="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

    invoke-virtual {v4}, Lorg/apache/lucene/index/MergeState$DocMap;->maxDoc()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " vs "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v5, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v4, v4, Lorg/apache/lucene/index/ReaderSlice;->length:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 104
    .end local v1    # "reader":I
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .line 105
    .local v0, "doc":I
    if-eq v0, v2, :cond_3

    .line 107
    iget-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->currentMap:Lorg/apache/lucene/index/MergeState$DocMap;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/MergeState$DocMap;->get(I)I

    move-result v0

    .line 108
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 111
    iget v2, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->currentBase:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->doc:I

    goto/16 :goto_1

    .line 113
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    goto/16 :goto_0
.end method

.method reset(Lorg/apache/lucene/index/MultiDocsEnum;)Lorg/apache/lucene/codecs/MappingMultiDocsEnum;
    .locals 1
    .param p1, "docsEnum"    # Lorg/apache/lucene/index/MultiDocsEnum;

    .prologue
    .line 49
    invoke-virtual {p1}, Lorg/apache/lucene/index/MultiDocsEnum;->getNumSubs()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->numSubs:I

    .line 50
    invoke-virtual {p1}, Lorg/apache/lucene/index/MultiDocsEnum;->getSubs()[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->upto:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    .line 53
    return-object p0
.end method

.method public setMergeState(Lorg/apache/lucene/index/MergeState;)V
    .locals 0
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;

    .prologue
    .line 59
    iput-object p1, p0, Lorg/apache/lucene/codecs/MappingMultiDocsEnum;->mergeState:Lorg/apache/lucene/index/MergeState;

    .line 60
    return-void
.end method
