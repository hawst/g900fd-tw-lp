.class Lorg/apache/lucene/codecs/compressing/CompressionMode$4;
.super Lorg/apache/lucene/codecs/compressing/Decompressor;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/Decompressor;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/compressing/Decompressor;
    .locals 0

    .prologue
    .line 145
    return-object p0
.end method

.method public decompress(Lorg/apache/lucene/store/DataInput;IIILorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "originalLength"    # I
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    sget-boolean v1, Lorg/apache/lucene/codecs/compressing/CompressionMode;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    add-int v1, p3, p4

    if-le v1, p2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 132
    :cond_0
    iget-object v1, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    add-int/lit8 v2, p2, 0x7

    if-ge v1, v2, :cond_1

    .line 133
    add-int/lit8 v1, p2, 0x7

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 135
    :cond_1
    add-int v1, p3, p4

    iget-object v2, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lorg/apache/lucene/codecs/compressing/LZ4;->decompress(Lorg/apache/lucene/store/DataInput;I[BI)I

    move-result v0

    .line 136
    .local v0, "decompressedLength":I
    if-le v0, p2, :cond_2

    .line 137
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Corrupted: lengths mismatch: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 139
    :cond_2
    iput p3, p5, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 140
    iput p4, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 141
    return-void
.end method
