.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;
.super Lorg/apache/lucene/codecs/FieldsConsumer;
.source "SimpleTextFieldsWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;
    }
.end annotation


# static fields
.field static final DOC:Lorg/apache/lucene/util/BytesRef;

.field static final END:Lorg/apache/lucene/util/BytesRef;

.field static final END_OFFSET:Lorg/apache/lucene/util/BytesRef;

.field static final FIELD:Lorg/apache/lucene/util/BytesRef;

.field static final FREQ:Lorg/apache/lucene/util/BytesRef;

.field static final PAYLOAD:Lorg/apache/lucene/util/BytesRef;

.field static final POS:Lorg/apache/lucene/util/BytesRef;

.field static final START_OFFSET:Lorg/apache/lucene/util/BytesRef;

.field static final TERM:Lorg/apache/lucene/util/BytesRef;


# instance fields
.field private final out:Lorg/apache/lucene/store/IndexOutput;

.field private final scratch:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "END"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    .line 39
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "field "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    .line 40
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  term "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->TERM:Lorg/apache/lucene/util/BytesRef;

    .line 41
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    doc "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    .line 42
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      freq "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->FREQ:Lorg/apache/lucene/util/BytesRef;

    .line 43
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      pos "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->POS:Lorg/apache/lucene/util/BytesRef;

    .line 44
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      startOffset "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    .line 45
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      endOffset "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "        payload "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldsConsumer;-><init>()V

    .line 36
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 49
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v1, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextPostingsFormat;->getPostingsFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "fileName":Ljava/lang/String;
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 51
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    return-void
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V

    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V

    return-void
.end method

.method private newline()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 63
    return-void
.end method

.method private write(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 55
    return-void
.end method

.method private write(Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 59
    return-void
.end method


# virtual methods
.method public addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/codecs/TermsConsumer;
    .locals 1
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 68
    iget-object v0, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V

    .line 70
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextTermsWriter;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    :try_start_0
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 182
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 186
    return-void

    .line 183
    :catchall_0
    move-exception v0

    .line 184
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 185
    throw v0
.end method
