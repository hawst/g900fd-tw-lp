.class public abstract Lorg/apache/lucene/codecs/compressing/CompressionMode;
.super Ljava/lang/Object;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateCompressor;,
        Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;,
        Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4FastCompressor;,
        Lorg/apache/lucene/codecs/compressing/CompressionMode$LZ4HighCompressor;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FAST:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field public static final FAST_DECOMPRESSION:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field public static final HIGH_COMPRESSION:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field private static final LZ4_DECOMPRESSOR:Lorg/apache/lucene/codecs/compressing/Decompressor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;->$assertionsDisabled:Z

    .line 44
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$1;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;->FAST:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 69
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$2;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;->HIGH_COMPRESSION:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 94
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$3;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$3;-><init>()V

    sput-object v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;->FAST_DECOMPRESSION:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 126
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$4;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$4;-><init>()V

    sput-object v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;->LZ4_DECOMPRESSOR:Lorg/apache/lucene/codecs/compressing/Decompressor;

    .line 148
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Lorg/apache/lucene/codecs/compressing/Decompressor;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;->LZ4_DECOMPRESSOR:Lorg/apache/lucene/codecs/compressing/Decompressor;

    return-object v0
.end method


# virtual methods
.method public abstract newCompressor()Lorg/apache/lucene/codecs/compressing/Compressor;
.end method

.method public abstract newDecompressor()Lorg/apache/lucene/codecs/compressing/Decompressor;
.end method
