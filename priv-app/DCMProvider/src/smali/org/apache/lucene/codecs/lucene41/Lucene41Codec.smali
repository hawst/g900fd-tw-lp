.class public Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;
.super Lorg/apache/lucene/codecs/Codec;
.source "Lucene41Codec.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final dvFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

.field private final fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

.field private final infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

.field private final liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

.field private final normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

.field private final postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 79
    const-string v0, "Lucene41"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 59
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec$1;

    const-string v1, "Lucene41StoredFields"

    sget-object v2, Lorg/apache/lucene/codecs/compressing/CompressionMode;->FAST:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    const/16 v3, 0x4000

    invoke-direct {v0, p0, v1, v2, v3}, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec$1;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 65
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    .line 66
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    .line 67
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    .line 68
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    .line 70
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec$2;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 127
    const-string v0, "Lucene41"

    invoke-static {v0}, Lorg/apache/lucene/codecs/PostingsFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 128
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->dvFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 129
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40NormsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40NormsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    .line 80
    return-void
.end method


# virtual methods
.method public docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->dvFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    return-object v0
.end method

.method public getPostingsFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    return-object v0
.end method

.method public normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    return-object v0
.end method

.method public final postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    return-object v0
.end method

.method public storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    return-object v0
.end method

.method public final termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41Codec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    return-object v0
.end method
