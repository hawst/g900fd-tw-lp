.class public Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsFormat;
.super Lorg/apache/lucene/codecs/StoredFieldsFormat;
.source "Lucene40StoredFieldsFormat.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsFormat;-><init>()V

    .line 86
    return-void
.end method


# virtual methods
.method public fieldsReader(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/StoredFieldsReader;
    .locals 1
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;

    invoke-direct {v0, p1, p2, p3, p4}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsReader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method

.method public fieldsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/StoredFieldsWriter;
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;

    iget-object v1, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40StoredFieldsWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method
