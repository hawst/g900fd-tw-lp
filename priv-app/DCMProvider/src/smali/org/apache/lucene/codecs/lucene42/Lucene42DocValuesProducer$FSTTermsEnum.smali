.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "Lucene42DocValuesProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FSTTermsEnum"
.end annotation


# instance fields
.field final bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field final firstArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final fst:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/BytesRefFSTEnum",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final scratchBytes:Lorg/apache/lucene/util/BytesRef;

.field final scratchInts:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/util/fst/FST;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 428
    .local p1, "fst":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Ljava/lang/Long;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 423
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 424
    new-instance v0, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v0}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 425
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchInts:Lorg/apache/lucene/util/IntsRef;

    .line 426
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    .line 429
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    .line 430
    new-instance v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    .line 431
    invoke-virtual {p1}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 432
    return-void
.end method


# virtual methods
.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 498
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 508
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 513
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 446
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 436
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->next()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    .line 437
    .local v0, "io":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Ljava/lang/Long;>;"
    if-nez v0, :cond_0

    .line 438
    const/4 v1, 0x0

    .line 440
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 493
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 451
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    if-nez v0, :cond_0

    .line 452
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 458
    :goto_0
    return-object v0

    .line 453
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 456
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 458
    :cond_1
    sget-object v0, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 9
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 475
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 476
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 477
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->bytesReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchInts:Lorg/apache/lucene/util/IntsRef;

    move-wide v2, p1

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/fst/Util;->getByOutput(Lorg/apache/lucene/util/fst/FST;JLorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v0

    .line 478
    .local v0, "output":Lorg/apache/lucene/util/IntsRef;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    iget v2, v0, Lorg/apache/lucene/util/IntsRef;->length:I

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 479
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    iput v8, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 480
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    iput v8, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 481
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/util/fst/Util;->toBytesRef(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    .line 483
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->scratchBytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    .line 484
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 464
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    if-nez v0, :cond_0

    .line 465
    const/4 v0, 0x0

    .line 467
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 488
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;->in:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->current()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 503
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
