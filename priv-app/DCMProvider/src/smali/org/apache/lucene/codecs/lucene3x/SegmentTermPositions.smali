.class final Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;
.super Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;
.source "SegmentTermPositions.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private lazySkipPointer:J

.field private lazySkipProxCount:I

.field private needToLoadPayload:Z

.field private payload:Lorg/apache/lucene/util/BytesRef;

.field private payloadLength:I

.field private position:I

.field private proxCount:I

.field private proxStream:Lorg/apache/lucene/store/IndexInput;

.field private proxStreamOrig:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 2
    .param p1, "freqStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "proxStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "tis"    # Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    .param p4, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 60
    invoke-direct {p0, p1, p3, p4}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;Lorg/apache/lucene/index/FieldInfos;)V

    .line 49
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipPointer:J

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    .line 61
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStreamOrig:Lorg/apache/lucene/store/IndexInput;

    .line 62
    return-void
.end method

.method private lazySkip()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 173
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStreamOrig:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    .line 180
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->skipPayload()V

    .line 182
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipPointer:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipPointer:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 184
    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipPointer:J

    .line 187
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    if-eqz v0, :cond_2

    .line 188
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->skipPositions(I)V

    .line 189
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    .line 191
    :cond_2
    return-void
.end method

.method private final readDeltaPosition()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 94
    .local v0, "delta":I
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->currentFieldStoresPayloads:Z

    if-eqz v1, :cond_2

    .line 99
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    .line 102
    :cond_0
    ushr-int/lit8 v0, v0, 0x1

    .line 103
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    .line 107
    :cond_1
    :goto_0
    return v0

    .line 104
    :cond_2
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 105
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private skipPayload()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    if-lez v0, :cond_0

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 159
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    .line 160
    return-void
.end method

.method private skipPositions(I)V
    .locals 3
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 149
    :cond_0
    move v0, p1

    .local v0, "f":I
    :goto_0
    if-gtz v0, :cond_1

    .line 153
    return-void

    .line 150
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->readDeltaPosition()I

    .line 151
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->skipPayload()V

    .line 149
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-super {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->close()V

    .line 79
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 80
    :cond_0
    return-void
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    if-gtz v0, :cond_0

    .line 199
    const/4 v0, 0x0

    .line 214
    :goto_0
    return-object v0

    .line 202
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_2

    .line 205
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 210
    :goto_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 211
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    .line 214
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    goto :goto_1
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    return v0
.end method

.method public isPayloadAvailable()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 120
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxCount:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    .line 122
    invoke-super {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->next()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->freq:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxCount:I

    .line 124
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->position:I

    .line 125
    const/4 v0, 0x1

    .line 127
    :cond_0
    return v0
.end method

.method public final nextPosition()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, v1, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    .line 87
    :cond_0
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkip()V

    .line 88
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxCount:I

    .line 89
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->position:I

    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->readDeltaPosition()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->position:I

    goto :goto_0
.end method

.method public final read([I[I)I
    .locals 2
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I

    .prologue
    .line 132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TermPositions does not support processing multiple documents in one call. Use TermDocs instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final seek(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/index/Term;)V
    .locals 3
    .param p1, "ti"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-super {p0, p1, p2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->seek(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/index/Term;)V

    .line 67
    if-eqz p1, :cond_0

    .line 68
    iget-wide v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipPointer:J

    .line 70
    :cond_0
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    .line 71
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxCount:I

    .line 72
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    .line 73
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    .line 74
    return-void
.end method

.method protected skipProx(JI)V
    .locals 1
    .param p1, "proxPointer"    # J
    .param p3, "payloadLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 140
    iput-wide p1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipPointer:J

    .line 141
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    .line 142
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->proxCount:I

    .line 143
    iput p3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->payloadLength:I

    .line 144
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->needToLoadPayload:Z

    .line 145
    return-void
.end method

.method protected final skippingDoc()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->freq:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->lazySkipProxCount:I

    .line 114
    return-void
.end method
