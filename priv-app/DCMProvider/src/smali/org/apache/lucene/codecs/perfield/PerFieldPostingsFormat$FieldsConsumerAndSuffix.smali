.class Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;
.super Ljava/lang/Object;
.source "PerFieldPostingsFormat.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FieldsConsumerAndSuffix"
.end annotation


# instance fields
.field consumer:Lorg/apache/lucene/codecs/FieldsConsumer;

.field suffix:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldPostingsFormat$FieldsConsumerAndSuffix;->consumer:Lorg/apache/lucene/codecs/FieldsConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/FieldsConsumer;->close()V

    .line 86
    return-void
.end method
