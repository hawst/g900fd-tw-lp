.class public final Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;
.super Lorg/apache/lucene/codecs/TermVectorsWriter;
.source "Lucene40TermVectorsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final MAX_RAW_MERGE_DOCS:I = 0x1060


# instance fields
.field private bufferedFreq:I

.field private bufferedIndex:I

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private fieldCount:I

.field private fps:[J

.field private lastFieldName:Ljava/lang/String;

.field lastOffset:I

.field lastPayloadLength:I

.field lastPosition:I

.field private final lastTerm:Lorg/apache/lucene/util/BytesRef;

.field private numVectorFields:I

.field private offsetEndBuffer:[I

.field private offsetStartBuffer:[I

.field private offsets:Z

.field private payloadData:Lorg/apache/lucene/util/BytesRef;

.field private payloads:Z

.field private positions:Z

.field scratch:Lorg/apache/lucene/util/BytesRef;

.field private final segment:Ljava/lang/String;

.field private tvd:Lorg/apache/lucene/store/IndexOutput;

.field private tvf:Lorg/apache/lucene/store/IndexOutput;

.field private tvx:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    .line 343
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 68
    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;-><init>()V

    .line 65
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    .line 102
    new-array v1, v3, [J

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fps:[J

    .line 103
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fieldCount:I

    .line 104
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->numVectorFields:I

    .line 137
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, v3}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    .line 141
    new-array v1, v3, [I

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetStartBuffer:[I

    .line 142
    new-array v1, v3, [I

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetEndBuffer:[I

    .line 143
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1, v3}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    .line 144
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    .line 145
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedFreq:I

    .line 146
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->positions:Z

    .line 147
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    .line 148
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    .line 171
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPosition:I

    .line 172
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastOffset:I

    .line 173
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPayloadLength:I

    .line 175
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 69
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 70
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->segment:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    .line 74
    .local v0, "success":Z
    :try_start_0
    const-string v1, ""

    const-string v2, "tvx"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    .line 75
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    const-string v2, "Lucene40TermVectorsIndex"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 76
    const-string v1, ""

    const-string v2, "tvd"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    const-string v2, "Lucene40TermVectorsDocs"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 78
    const-string v1, ""

    const-string v2, "tvf"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    .line 79
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    const-string v2, "Lucene40TermVectorsFields"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 80
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    sget-wide v2, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_INDEX:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :catchall_0
    move-exception v1

    .line 85
    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->abort()V

    .line 88
    :cond_0
    throw v1

    .line 81
    :cond_1
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    sget-wide v2, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_DOCS:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 82
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    sget-wide v2, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_FIELDS:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    :cond_3
    const/4 v0, 0x1

    .line 85
    if-nez v0, :cond_4

    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->abort()V

    .line 89
    :cond_4
    return-void
.end method

.method private addRawDocuments(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;[I[II)V
    .locals 16
    .param p1, "reader"    # Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
    .param p2, "tvdLengths"    # [I
    .param p3, "tvfLengths"    # [I
    .param p4, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 296
    .local v4, "tvdPosition":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v8

    .line 297
    .local v8, "tvfPosition":J
    move-wide v6, v4

    .line 298
    .local v6, "tvdStart":J
    move-wide v10, v8

    .line 299
    .local v10, "tvfStart":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    move/from16 v0, p4

    if-lt v2, v0, :cond_0

    .line 305
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->getTvdStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v12

    sub-long v14, v4, v6

    invoke-virtual {v3, v12, v14, v15}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 306
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->getTvfStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v12

    sub-long v14, v8, v10

    invoke-virtual {v3, v12, v14, v15}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 307
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v12

    cmp-long v3, v12, v4

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 300
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 301
    aget v3, p2, v2

    int-to-long v12, v3

    add-long/2addr v4, v12

    .line 302
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3, v8, v9}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 303
    aget v3, p3, v2

    int-to-long v12, v3

    add-long/2addr v8, v12

    .line 299
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 308
    :cond_1
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v12

    cmp-long v3, v12, v8

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 309
    :cond_2
    return-void
.end method

.method private copyVectorsNoDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;Lorg/apache/lucene/index/AtomicReader;[I[I)I
    .locals 8
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "matchingVectorsReader"    # Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
    .param p3, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p4, "rawDocLengths"    # [I
    .param p5, "rawDocLengths2"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 404
    invoke-virtual {p3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    .line 405
    .local v3, "maxDoc":I
    if-eqz p2, :cond_2

    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "docCount":I
    :goto_0
    if-lt v0, v3, :cond_1

    .line 424
    .end local v0    # "docCount":I
    :cond_0
    return v3

    .line 409
    .restart local v0    # "docCount":I
    :cond_1
    const/16 v5, 0x1060

    sub-int v6, v3, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 410
    .local v2, "len":I
    invoke-virtual {p2, p4, p5, v0, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->rawDocs([I[III)V

    .line 411
    invoke-direct {p0, p2, p4, p5, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->addRawDocuments(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;[I[II)V

    .line 412
    add-int/2addr v0, v2

    .line 413
    iget-object v5, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    mul-int/lit16 v6, v2, 0x12c

    int-to-double v6, v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_0

    .line 416
    .end local v0    # "docCount":I
    .end local v2    # "len":I
    :cond_2
    const/4 v1, 0x0

    .local v1, "docNum":I
    :goto_1
    if-ge v1, v3, :cond_0

    .line 419
    invoke-virtual {p3, v1}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v4

    .line 420
    .local v4, "vectors":Lorg/apache/lucene/index/Fields;
    invoke-virtual {p0, v4, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V

    .line 421
    iget-object v5, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    const-wide v6, 0x4072c00000000000L    # 300.0

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    .line 416
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private copyVectorsWithDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;Lorg/apache/lucene/index/AtomicReader;[I[I)I
    .locals 10
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "matchingVectorsReader"    # Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
    .param p3, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p4, "rawDocLengths"    # [I
    .param p5, "rawDocLengths2"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 351
    invoke-virtual {p3}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    .line 352
    .local v2, "maxDoc":I
    invoke-virtual {p3}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v1

    .line 353
    .local v1, "liveDocs":Lorg/apache/lucene/util/Bits;
    const/4 v5, 0x0

    .line 354
    .local v5, "totalNumDocs":I
    if-eqz p2, :cond_6

    .line 356
    const/4 v0, 0x0

    .local v0, "docNum":I
    :goto_0
    if-lt v0, v2, :cond_1

    .line 395
    :cond_0
    return v5

    .line 357
    :cond_1
    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v7

    if-nez v7, :cond_2

    .line 359
    add-int/lit8 v0, v0, 0x1

    .line 360
    goto :goto_0

    .line 364
    :cond_2
    move v4, v0

    .local v4, "start":I
    const/4 v3, 0x0

    .line 366
    .local v3, "numDocs":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 367
    add-int/lit8 v3, v3, 0x1

    .line 368
    if-lt v0, v2, :cond_4

    .line 375
    :goto_1
    invoke-virtual {p2, p4, p5, v4, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->rawDocs([I[III)V

    .line 376
    invoke-direct {p0, p2, p4, p5, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->addRawDocuments(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;[I[II)V

    .line 377
    add-int/2addr v5, v3

    .line 378
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    mul-int/lit16 v8, v3, 0x12c

    int-to-double v8, v8

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_0

    .line 369
    :cond_4
    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v7

    if-nez v7, :cond_5

    .line 370
    add-int/lit8 v0, v0, 0x1

    .line 371
    goto :goto_1

    .line 373
    :cond_5
    const/16 v7, 0x1060

    .line 365
    if-lt v3, v7, :cond_3

    goto :goto_1

    .line 381
    .end local v0    # "docNum":I
    .end local v3    # "numDocs":I
    .end local v4    # "start":I
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "docNum":I
    :goto_2
    if-ge v0, v2, :cond_0

    .line 382
    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v7

    if-nez v7, :cond_7

    .line 381
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 389
    :cond_7
    invoke-virtual {p3, v0}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v6

    .line 390
    .local v6, "vectors":Lorg/apache/lucene/index/Fields;
    invoke-virtual {p0, v6, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V

    .line 391
    add-int/lit8 v5, v5, 0x1

    .line 392
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    const-wide v8, 0x4072c00000000000L    # 300.0

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_3
.end method

.method private writePosition(ILorg/apache/lucene/util/BytesRef;)V
    .locals 3
    .param p1, "delta"    # I
    .param p2, "payload"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 256
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    if-eqz v1, :cond_4

    .line 257
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 259
    .local v0, "payloadLength":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPayloadLength:I

    if-eq v0, v1, :cond_1

    .line 260
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPayloadLength:I

    .line 261
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v2, p1, 0x1

    or-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 262
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 266
    :goto_1
    if-lez v0, :cond_3

    .line 267
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v1, v0

    if-gez v1, :cond_2

    .line 270
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "A term cannot have more than Integer.MAX_VALUE bytes of payload data in a single document"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 257
    .end local v0    # "payloadLength":I
    :cond_0
    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0

    .line 264
    .restart local v0    # "payloadLength":I
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    shl-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_1

    .line 272
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/util/BytesRef;->append(Lorg/apache/lucene/util/BytesRef;)V

    .line 277
    .end local v0    # "payloadLength":I
    :cond_3
    :goto_2
    return-void

    .line 275
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    goto :goto_2
.end method


# virtual methods
.method public abort()V
    .locals 6

    .prologue
    .line 282
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "tvx"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 285
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "tvd"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 286
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "tvf"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 284
    invoke-static {v0, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    .line 287
    return-void

    .line 283
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public addPosition(IIILorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "startOffset"    # I
    .param p3, "endOffset"    # I
    .param p4, "payload"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 212
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->positions:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    if-eqz v0, :cond_3

    .line 214
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPosition:I

    sub-int v0, p1, v0

    invoke-direct {p0, v0, p4}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->writePosition(ILorg/apache/lucene/util/BytesRef;)V

    .line 215
    iput p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPosition:I

    .line 218
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetStartBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    aput p2, v0, v1

    .line 220
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetEndBuffer:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    aput p3, v0, v1

    .line 223
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    .line 234
    :cond_2
    :goto_0
    return-void

    .line 224
    :cond_3
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->positions:Z

    if-eqz v0, :cond_4

    .line 226
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPosition:I

    sub-int v0, p1, v0

    invoke-direct {p0, v0, p4}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->writePosition(ILorg/apache/lucene/util/BytesRef;)V

    .line 227
    iput p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPosition:I

    goto :goto_0

    .line 228
    :cond_4
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    if-eqz v0, :cond_2

    .line 230
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastOffset:I

    sub-int v1, p2, v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 231
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    sub-int v1, p3, p2

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 232
    iput p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastOffset:I

    goto :goto_0
.end method

.method public addProx(ILorg/apache/lucene/store/DataInput;Lorg/apache/lucene/store/DataInput;)V
    .locals 7
    .param p1, "numProx"    # I
    .param p2, "positions"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "offsets"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    iget-boolean v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    if-eqz v3, :cond_4

    .line 182
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p1, :cond_2

    .line 194
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 202
    .end local v1    # "i":I
    :cond_0
    if-eqz p3, :cond_1

    .line 203
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-lt v1, p1, :cond_5

    .line 208
    .end local v1    # "i":I
    :cond_1
    return-void

    .line 183
    .restart local v1    # "i":I
    :cond_2
    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 184
    .local v0, "code":I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 185
    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v2

    .line 186
    .local v2, "length":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 187
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    iput v2, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 188
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p2, v3, v4, v5}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 189
    ushr-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->writePosition(ILorg/apache/lucene/util/BytesRef;)V

    .line 182
    .end local v2    # "length":I
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 191
    :cond_3
    ushr-int/lit8 v3, v0, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->writePosition(ILorg/apache/lucene/util/BytesRef;)V

    goto :goto_2

    .line 195
    .end local v0    # "code":I
    .end local v1    # "i":I
    :cond_4
    if-eqz p2, :cond_0

    .line 197
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    if-ge v1, p1, :cond_0

    .line 198
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    ushr-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 197
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 204
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p3}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 205
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p3}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 203
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 445
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 443
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 444
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    return-void
.end method

.method public finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 6
    .param p1, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numDocs"    # I

    .prologue
    .line 429
    sget-wide v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->HEADER_LENGTH_INDEX:J

    int-to-long v2, p2

    const-wide/16 v4, 0x10

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 435
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tvx size mismatch: mergedDocs is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but tvx size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " file="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; now aborting this merge to prevent index corruption"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_0
    return-void
.end method

.method public finishDocument()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fieldCount:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->numVectorFields:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 132
    :cond_0
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fieldCount:I

    if-lt v0, v1, :cond_1

    .line 135
    return-void

    .line 133
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fps:[J

    aget-wide v2, v2, v0

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fps:[J

    add-int/lit8 v5, v0, -0x1

    aget-wide v4, v4, v5

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public finishTerm()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    if-lez v1, :cond_4

    .line 240
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->positions:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 241
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedFreq:I

    if-eq v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 242
    :cond_2
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    if-eqz v1, :cond_3

    .line 243
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 245
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    if-lt v0, v1, :cond_5

    .line 253
    .end local v0    # "i":I
    :cond_4
    return-void

    .line 246
    .restart local v0    # "i":I
    :cond_5
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    if-eqz v1, :cond_6

    .line 247
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetStartBuffer:[I

    aget v2, v2, v0

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastOffset:I

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 248
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetEndBuffer:[I

    aget v2, v2, v0

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetStartBuffer:[I

    aget v3, v3, v0

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 249
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetEndBuffer:[I

    aget v1, v1, v0

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastOffset:I

    .line 245
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 449
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final merge(Lorg/apache/lucene/index/MergeState;)I
    .locals 12
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v0, 0x1060

    .line 314
    new-array v4, v0, [I

    .line 315
    .local v4, "rawDocLengths":[I
    new-array v5, v0, [I

    .line 317
    .local v5, "rawDocLengths2":[I
    const/4 v7, 0x0

    .line 318
    .local v7, "idx":I
    const/4 v10, 0x0

    .line 319
    .local v10, "numDocs":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p1, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v6, v0, :cond_0

    .line 337
    iget-object v0, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v0, v10}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V

    .line 338
    return v10

    .line 320
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/AtomicReader;

    .line 322
    .local v3, "reader":Lorg/apache/lucene/index/AtomicReader;
    iget-object v0, p1, Lorg/apache/lucene/index/MergeState;->matchingSegmentReaders:[Lorg/apache/lucene/index/SegmentReader;

    add-int/lit8 v8, v7, 0x1

    .end local v7    # "idx":I
    .local v8, "idx":I
    aget-object v9, v0, v7

    .line 323
    .local v9, "matchingSegmentReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v2, 0x0

    .line 324
    .local v2, "matchingVectorsReader":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
    if-eqz v9, :cond_1

    .line 325
    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentReader;->getTermVectorsReader()Lorg/apache/lucene/codecs/TermVectorsReader;

    move-result-object v11

    .line 327
    .local v11, "vectorsReader":Lorg/apache/lucene/codecs/TermVectorsReader;
    if-eqz v11, :cond_1

    instance-of v0, v11, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    if-eqz v0, :cond_1

    move-object v2, v11

    .line 328
    check-cast v2, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    .line 331
    .end local v11    # "vectorsReader":Lorg/apache/lucene/codecs/TermVectorsReader;
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p1

    .line 332
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->copyVectorsWithDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;Lorg/apache/lucene/index/AtomicReader;[I[I)I

    move-result v0

    add-int/2addr v10, v0

    .line 319
    :goto_1
    add-int/lit8 v6, v6, 0x1

    move v7, v8

    .end local v8    # "idx":I
    .restart local v7    # "idx":I
    goto :goto_0

    .end local v7    # "idx":I
    .restart local v8    # "idx":I
    :cond_2
    move-object v0, p0

    move-object v1, p1

    .line 334
    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->copyVectorsNoDeletions(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;Lorg/apache/lucene/index/AtomicReader;[I[I)I

    move-result v0

    add-int/2addr v10, v0

    goto :goto_1
.end method

.method public startDocument(I)V
    .locals 4
    .param p1, "numVectorFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastFieldName:Ljava/lang/String;

    .line 94
    iput p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->numVectorFields:I

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 96
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvx:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fieldCount:I

    .line 99
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fps:[J

    invoke-static {v0, p1}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fps:[J

    .line 100
    return-void
.end method

.method public startField(Lorg/apache/lucene/index/FieldInfo;IZZZ)V
    .locals 6
    .param p1, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "numTerms"    # I
    .param p3, "positions"    # Z
    .param p4, "offsets"    # Z
    .param p5, "payloads"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastFieldName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastFieldName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fieldName="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " lastFieldName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastFieldName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 110
    :cond_0
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastFieldName:Ljava/lang/String;

    .line 111
    iput-boolean p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->positions:Z

    .line 112
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    .line 113
    iput-boolean p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloads:Z

    .line 114
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 115
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPayloadLength:I

    .line 116
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fps:[J

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fieldCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->fieldCount:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    aput-wide v4, v1, v2

    .line 117
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvd:Lorg/apache/lucene/store/IndexOutput;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 118
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "bits":B
    if-eqz p3, :cond_1

    .line 121
    const/4 v1, 0x1

    int-to-byte v0, v1

    .line 122
    :cond_1
    if-eqz p4, :cond_2

    .line 123
    or-int/lit8 v1, v0, 0x2

    int-to-byte v0, v1

    .line 124
    :cond_2
    if-eqz p5, :cond_3

    .line 125
    or-int/lit8 v1, v0, 0x4

    int-to-byte v0, v1

    .line 126
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 127
    return-void
.end method

.method public startTerm(Lorg/apache/lucene/util/BytesRef;I)V
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "freq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 152
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2, p1}, Lorg/apache/lucene/util/StringHelper;->bytesDifference(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 153
    .local v0, "prefix":I
    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v1, v2, v0

    .line 154
    .local v1, "suffix":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 155
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 156
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v4, v0

    invoke-virtual {v2, v3, v4, v1}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 157
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->tvf:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2, p2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 158
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 159
    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastOffset:I

    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->lastPosition:I

    .line 161
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsets:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->positions:Z

    if-eqz v2, :cond_0

    .line 163
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetStartBuffer:[I

    invoke-static {v2, p2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetStartBuffer:[I

    .line 164
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetEndBuffer:[I

    invoke-static {v2, p2}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->offsetEndBuffer:[I

    .line 166
    :cond_0
    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedIndex:I

    .line 167
    iput p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->bufferedFreq:I

    .line 168
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsWriter;->payloadData:Lorg/apache/lucene/util/BytesRef;

    iput v5, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 169
    return-void
.end method
