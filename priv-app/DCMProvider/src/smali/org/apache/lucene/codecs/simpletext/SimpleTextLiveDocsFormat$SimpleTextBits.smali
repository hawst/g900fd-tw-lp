.class Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;
.super Ljava/lang/Object;
.source "SimpleTextLiveDocsFormat.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SimpleTextBits"
.end annotation


# instance fields
.field final bits:Ljava/util/BitSet;

.field final size:I


# direct methods
.method constructor <init>(Ljava/util/BitSet;I)V
    .locals 0
    .param p1, "bits"    # Ljava/util/BitSet;
    .param p2, "size"    # I

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->bits:Ljava/util/BitSet;

    .line 154
    iput p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->size:I

    .line 155
    return-void
.end method


# virtual methods
.method public get(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->bits:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->size:I

    return v0
.end method
