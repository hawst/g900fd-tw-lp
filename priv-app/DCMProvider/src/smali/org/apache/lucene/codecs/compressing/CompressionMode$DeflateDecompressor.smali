.class final Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;
.super Lorg/apache/lucene/codecs/compressing/Decompressor;
.source "CompressionMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressionMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DeflateDecompressor"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field compressed:[B

.field final decompressor:Ljava/util/zip/Inflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 182
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressionMode;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/Decompressor;-><init>()V

    .line 188
    new-instance v0, Ljava/util/zip/Inflater;

    invoke-direct {v0}, Ljava/util/zip/Inflater;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->decompressor:Ljava/util/zip/Inflater;

    .line 189
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->compressed:[B

    .line 190
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/compressing/Decompressor;
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;-><init>()V

    return-object v0
.end method

.method public decompress(Lorg/apache/lucene/store/DataInput;IIILorg/apache/lucene/util/BytesRef;)V
    .locals 7
    .param p1, "in"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "originalLength"    # I
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 194
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    add-int v4, p3, p4

    if-le v4, p2, :cond_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 195
    :cond_0
    if-nez p4, :cond_1

    .line 196
    iput v6, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 229
    :goto_0
    return-void

    .line 199
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 200
    .local v0, "compressedLength":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->compressed:[B

    array-length v4, v4

    if-le v0, v4, :cond_2

    .line 201
    const/4 v4, 0x1

    invoke-static {v0, v4}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v4

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->compressed:[B

    .line 203
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->compressed:[B

    invoke-virtual {p1, v4, v6, v0}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 205
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->decompressor:Ljava/util/zip/Inflater;

    invoke-virtual {v4}, Ljava/util/zip/Inflater;->reset()V

    .line 206
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->decompressor:Ljava/util/zip/Inflater;

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->compressed:[B

    invoke-virtual {v4, v5, v6, v0}, Ljava/util/zip/Inflater;->setInput([BII)V

    .line 208
    iput v6, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v6, p5, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 212
    :goto_1
    :try_start_0
    iget-object v4, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v4, v4

    iget v5, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v3, v4, v5

    .line 213
    .local v3, "remaining":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->decompressor:Ljava/util/zip/Inflater;

    iget-object v5, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v6, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v4, v5, v6, v3}, Ljava/util/zip/Inflater;->inflate([BII)I
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 217
    .local v1, "count":I
    iget v4, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v4, v1

    iput v4, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 218
    iget-object v4, p0, Lorg/apache/lucene/codecs/compressing/CompressionMode$DeflateDecompressor;->decompressor:Ljava/util/zip/Inflater;

    invoke-virtual {v4}, Ljava/util/zip/Inflater;->finished()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 224
    iget v4, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eq v4, p2, :cond_4

    .line 225
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Lengths mismatch: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " != "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 214
    .end local v1    # "count":I
    .end local v3    # "remaining":I
    :catch_0
    move-exception v2

    .line 215
    .local v2, "e":Ljava/util/zip/DataFormatException;
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4, v2}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 221
    .end local v2    # "e":Ljava/util/zip/DataFormatException;
    .restart local v1    # "count":I
    .restart local v3    # "remaining":I
    :cond_3
    iget-object v4, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-static {v4}, Lorg/apache/lucene/util/ArrayUtil;->grow([B)[B

    move-result-object v4

    iput-object v4, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    goto :goto_1

    .line 227
    :cond_4
    iput p3, p5, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 228
    iput p4, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0
.end method
