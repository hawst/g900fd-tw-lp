.class final Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;
.super Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;
.source "Lucene41SkipWriter.java"


# instance fields
.field private curDoc:I

.field private curDocPointer:J

.field private curPayPointer:J

.field private curPayloadByteUpto:I

.field private curPosBufferUpto:I

.field private curPosPointer:J

.field private final docOut:Lorg/apache/lucene/store/IndexOutput;

.field private fieldHasOffsets:Z

.field private fieldHasPayloads:Z

.field private fieldHasPositions:Z

.field private lastPayloadByteUpto:[I

.field private lastSkipDoc:[I

.field private lastSkipDocPointer:[J

.field private lastSkipPayPointer:[J

.field private lastSkipPosPointer:[J

.field private final payOut:Lorg/apache/lucene/store/IndexOutput;

.field private final posOut:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method public constructor <init>(IIILorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/IndexOutput;)V
    .locals 1
    .param p1, "maxSkipLevels"    # I
    .param p2, "blockSize"    # I
    .param p3, "docCount"    # I
    .param p4, "docOut"    # Lorg/apache/lucene/store/IndexOutput;
    .param p5, "posOut"    # Lorg/apache/lucene/store/IndexOutput;
    .param p6, "payOut"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 70
    const/16 v0, 0x8

    invoke-direct {p0, p2, v0, p1, p3}, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;-><init>(IIII)V

    .line 71
    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    .line 72
    iput-object p5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    .line 73
    iput-object p6, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    .line 75
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDoc:[I

    .line 76
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDocPointer:[J

    .line 77
    if-eqz p5, :cond_1

    .line 78
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPosPointer:[J

    .line 79
    if-eqz p6, :cond_0

    .line 80
    new-array v0, p1, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPayPointer:[J

    .line 82
    :cond_0
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastPayloadByteUpto:[I

    .line 84
    :cond_1
    return-void
.end method


# virtual methods
.method public bufferSkip(IIJJII)V
    .locals 3
    .param p1, "doc"    # I
    .param p2, "numDocs"    # I
    .param p3, "posFP"    # J
    .param p5, "payFP"    # J
    .param p7, "posBufferUpto"    # I
    .param p8, "payloadByteUpto"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iput p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curDoc:I

    .line 113
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curDocPointer:J

    .line 114
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPosPointer:J

    .line 115
    iput-wide p5, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPayPointer:J

    .line 116
    iput p7, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPosBufferUpto:I

    .line 117
    iput p8, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPayloadByteUpto:I

    .line 118
    invoke-virtual {p0, p2}, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->bufferSkip(I)V

    .line 119
    return-void
.end method

.method public resetSkip()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 94
    invoke-super {p0}, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->resetSkip()V

    .line 95
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDoc:[I

    invoke-static {v0, v4}, Ljava/util/Arrays;->fill([II)V

    .line 96
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDocPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->docOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 97
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPositions:Z

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPosPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->posOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 99
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPayloads:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastPayloadByteUpto:[I

    invoke-static {v0, v4}, Ljava/util/Arrays;->fill([II)V

    .line 102
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasOffsets:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPayloads:Z

    if-eqz v0, :cond_2

    .line 103
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPayPointer:[J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->payOut:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 106
    :cond_2
    return-void
.end method

.method public setField(ZZZ)V
    .locals 0
    .param p1, "fieldHasPositions"    # Z
    .param p2, "fieldHasOffsets"    # Z
    .param p3, "fieldHasPayloads"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPositions:Z

    .line 88
    iput-boolean p2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasOffsets:Z

    .line 89
    iput-boolean p3, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPayloads:Z

    .line 90
    return-void
.end method

.method protected writeSkipData(ILorg/apache/lucene/store/IndexOutput;)V
    .locals 6
    .param p1, "level"    # I
    .param p2, "skipBuffer"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curDoc:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDoc:[I

    aget v2, v2, p1

    sub-int v0, v1, v2

    .line 127
    .local v0, "delta":I
    invoke-virtual {p2, v0}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 128
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDoc:[I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curDoc:I

    aput v2, v1, p1

    .line 130
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curDocPointer:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDocPointer:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipDocPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curDocPointer:J

    aput-wide v2, v1, p1

    .line 133
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPositions:Z

    if-eqz v1, :cond_2

    .line 137
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPosPointer:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPosPointer:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 138
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPosPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPosPointer:J

    aput-wide v2, v1, p1

    .line 139
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPosBufferUpto:I

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 141
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPayloads:Z

    if-eqz v1, :cond_0

    .line 142
    iget v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPayloadByteUpto:I

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 145
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasOffsets:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->fieldHasPayloads:Z

    if-eqz v1, :cond_2

    .line 146
    :cond_1
    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPayPointer:J

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPayPointer:[J

    aget-wide v4, v1, p1

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 147
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->lastSkipPayPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41SkipWriter;->curPayPointer:J

    aput-wide v2, v1, p1

    .line 150
    :cond_2
    return-void
.end method
