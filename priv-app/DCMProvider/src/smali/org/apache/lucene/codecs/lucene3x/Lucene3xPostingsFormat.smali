.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xPostingsFormat;
.super Lorg/apache/lucene/codecs/PostingsFormat;
.source "Lucene3xPostingsFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final FREQ_EXTENSION:Ljava/lang/String; = "frq"

.field public static final PROX_EXTENSION:Ljava/lang/String; = "prx"

.field public static final TERMS_EXTENSION:Ljava/lang/String; = "tis"

.field public static final TERMS_INDEX_EXTENSION:Ljava/lang/String; = "tii"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "Lucene3x"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/PostingsFormat;-><init>(Ljava/lang/String;)V

    .line 53
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/FieldsProducer;
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    iget v5, p1, Lorg/apache/lucene/index/SegmentReadState;->termsIndexDivisor:I

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;I)V

    return-object v0
.end method
