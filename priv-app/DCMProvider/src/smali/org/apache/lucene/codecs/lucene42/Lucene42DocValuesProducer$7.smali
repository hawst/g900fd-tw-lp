.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;
.super Lorg/apache/lucene/index/SortedSetDocValues;
.source "Lucene42DocValuesProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field currentOrd:J

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

.field private final synthetic val$docToOrds:Lorg/apache/lucene/index/BinaryDocValues;

.field private final synthetic val$entry:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

.field private final synthetic val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

.field private final synthetic val$fst:Lorg/apache/lucene/util/fst/FST;

.field private final synthetic val$fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

.field private final synthetic val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private final synthetic val$input:Lorg/apache/lucene/store/ByteArrayDataInput;

.field private final synthetic val$ref:Lorg/apache/lucene/util/BytesRef;

.field private final synthetic val$scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

.field private final synthetic val$scratchInts:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/store/ByteArrayDataInput;Lorg/apache/lucene/index/BinaryDocValues;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/fst/BytesRefFSTEnum;Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->this$0:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$input:Lorg/apache/lucene/store/ByteArrayDataInput;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$docToOrds:Lorg/apache/lucene/index/BinaryDocValues;

    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$ref:Lorg/apache/lucene/util/BytesRef;

    iput-object p5, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    iput-object p6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$fst:Lorg/apache/lucene/util/fst/FST;

    iput-object p7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object p8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object p9, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$scratchInts:Lorg/apache/lucene/util/IntsRef;

    iput-object p10, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    iput-object p11, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$entry:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    .line 327
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public getValueCount()J
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$entry:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->numOrds:J

    return-wide v0
.end method

.method public lookupOrd(JLorg/apache/lucene/util/BytesRef;)V
    .locals 9
    .param p1, "ord"    # J
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 350
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 351
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 352
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$scratchInts:Lorg/apache/lucene/util/IntsRef;

    move-wide v2, p1

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/fst/Util;->getByOutput(Lorg/apache/lucene/util/fst/FST;JLorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v8

    .line 353
    .local v8, "output":Lorg/apache/lucene/util/IntsRef;
    iget v1, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    new-array v1, v1, [B

    iput-object v1, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 354
    const/4 v1, 0x0

    iput v1, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 355
    const/4 v1, 0x0

    iput v1, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 356
    invoke-static {v8, p3}, Lorg/apache/lucene/util/fst/Util;->toBytesRef(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    return-void

    .line 357
    .end local v8    # "output":Lorg/apache/lucene/util/IntsRef;
    :catch_0
    move-exception v0

    .line 358
    .local v0, "bogus":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public lookupTerm(Lorg/apache/lucene/util/BytesRef;)J
    .locals 6
    .param p1, "key"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const-wide/16 v4, 0x1

    .line 365
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v1

    .line 366
    .local v1, "o":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Ljava/lang/Long;>;"
    if-nez v1, :cond_0

    .line 367
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->getValueCount()J

    move-result-wide v2

    neg-long v2, v2

    sub-long/2addr v2, v4

    .line 371
    :goto_0
    return-wide v2

    .line 368
    :cond_0
    iget-object v2, v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 369
    iget-object v2, v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v2

    int-to-long v2, v2

    goto :goto_0

    .line 371
    :cond_1
    iget-object v2, v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    neg-long v2, v2

    sub-long/2addr v2, v4

    goto :goto_0

    .line 373
    .end local v1    # "o":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Ljava/lang/Long;>;"
    :catch_0
    move-exception v0

    .line 374
    .local v0, "bogus":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public nextOrd()J
    .locals 4

    .prologue
    .line 332
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$input:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->eof()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    const-wide/16 v0, -0x1

    .line 336
    :goto_0
    return-wide v0

    .line 335
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->currentOrd:J

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$input:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->currentOrd:J

    .line 336
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->currentOrd:J

    goto :goto_0
.end method

.method public setDocument(I)V
    .locals 4
    .param p1, "docID"    # I

    .prologue
    .line 342
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$docToOrds:Lorg/apache/lucene/index/BinaryDocValues;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$ref:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 343
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$input:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$ref:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$ref:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$ref:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 344
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->currentOrd:J

    .line 345
    return-void
.end method

.method public termsEnum()Lorg/apache/lucene/index/TermsEnum;
    .locals 2

    .prologue
    .line 385
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$7;->val$fst:Lorg/apache/lucene/util/fst/FST;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    return-object v0
.end method
