.class public final Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;
.super Lorg/apache/lucene/codecs/StoredFieldsReader;
.source "CompressingStoredFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final bytes:Lorg/apache/lucene/util/BytesRef;

.field private closed:Z

.field private final compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

.field private final decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fieldsStream:Lorg/apache/lucene/store/IndexInput;

.field private final indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

.field private final numDocs:I

.field private final packedIntsVersion:I


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I
    .locals 3

    .prologue
    .line 61
    sget-object v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->values()[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)V
    .locals 2
    .param p1, "reader"    # Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    .prologue
    .line 74
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 75
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 76
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 77
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->clone()Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    .line 78
    iget v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    .line 79
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 80
    iget-object v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/compressing/Decompressor;->clone()Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    .line 81
    iget v0, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I

    .line 82
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->closed:Z

    .line 84
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;Lorg/apache/lucene/codecs/compressing/CompressionMode;)V
    .locals 12
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "segmentSuffix"    # Ljava/lang/String;
    .param p4, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p5, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p6, "formatName"    # Ljava/lang/String;
    .param p7, "compressionMode"    # Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 89
    move-object/from16 v0, p7

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    .line 90
    iget-object v6, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    .line 91
    .local v6, "segment":Ljava/lang/String;
    const/4 v7, 0x0

    .line 92
    .local v7, "success":Z
    move-object/from16 v0, p4

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 93
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I

    .line 94
    const/4 v4, 0x0

    .line 96
    .local v4, "indexStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    const-string v8, "fdt"

    invoke-static {v6, p3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p5

    invoke-virtual {p1, v8, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 97
    const-string v8, "fdx"

    invoke-static {v6, p3, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 98
    .local v5, "indexStreamFN":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {p1, v5, v0}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 100
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "Index"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 101
    .local v3, "codecNameIdx":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "Data"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, "codecNameDat":Ljava/lang/String;
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v4, v3, v8, v9}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 103
    iget-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v2, v9, v10}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 104
    sget-boolean v8, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    invoke-static {v2}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v8

    int-to-long v8, v8

    iget-object v10, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v10}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    .end local v2    # "codecNameDat":Ljava/lang/String;
    .end local v3    # "codecNameIdx":Ljava/lang/String;
    .end local v5    # "indexStreamFN":Ljava/lang/String;
    :catchall_0
    move-exception v8

    .line 116
    if-nez v7, :cond_0

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 117
    aput-object p0, v9, v10

    const/4 v10, 0x1

    aput-object v4, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 119
    :cond_0
    throw v8

    .line 105
    .restart local v2    # "codecNameDat":Ljava/lang/String;
    .restart local v3    # "codecNameIdx":Ljava/lang/String;
    .restart local v5    # "indexStreamFN":Ljava/lang/String;
    :cond_1
    :try_start_1
    sget-boolean v8, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v8, :cond_2

    invoke-static {v3}, Lorg/apache/lucene/codecs/CodecUtil;->headerLength(Ljava/lang/String;)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 107
    :cond_2
    new-instance v8, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    invoke-direct {v8, v4, p2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/SegmentInfo;)V

    iput-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    .line 108
    const/4 v4, 0x0

    .line 110
    iget-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    .line 111
    invoke-virtual/range {p7 .. p7}, Lorg/apache/lucene/codecs/compressing/CompressionMode;->newDecompressor()Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v8

    iput-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    .line 112
    new-instance v8, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v8}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v8, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    const/4 v7, 0x1

    .line 116
    if-nez v7, :cond_3

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/io/Closeable;

    const/4 v9, 0x0

    .line 117
    aput-object p0, v8, v9

    const/4 v9, 0x1

    aput-object v4, v8, v9

    invoke-static {v8}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 120
    :cond_3
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    return v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/codecs/compressing/Decompressor;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    return-object v0
.end method

.method private ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 126
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->closed:Z

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this FieldsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    return-void
.end method

.method private static readField(Lorg/apache/lucene/store/ByteArrayDataInput;Lorg/apache/lucene/index/StoredFieldVisitor;Lorg/apache/lucene/index/FieldInfo;I)V
    .locals 5
    .param p0, "in"    # Lorg/apache/lucene/store/ByteArrayDataInput;
    .param p1, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .param p2, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "bits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 143
    sget v2, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_MASK:I

    and-int/2addr v2, p3

    packed-switch v2, :pswitch_data_0

    .line 169
    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown type flag: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 145
    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v1

    .line 146
    .local v1, "length":I
    new-array v0, v1, [B

    .line 147
    .local v0, "data":[B
    invoke-virtual {p0, v0, v3, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readBytes([BII)V

    .line 148
    invoke-virtual {p1, p2, v0}, Lorg/apache/lucene/index/StoredFieldVisitor;->binaryField(Lorg/apache/lucene/index/FieldInfo;[B)V

    .line 171
    .end local v0    # "data":[B
    .end local v1    # "length":I
    :goto_0
    return-void

    .line 151
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v1

    .line 152
    .restart local v1    # "length":I
    new-array v0, v1, [B

    .line 153
    .restart local v0    # "data":[B
    invoke-virtual {p0, v0, v3, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readBytes([BII)V

    .line 154
    new-instance v2, Ljava/lang/String;

    sget-object v3, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {p1, p2, v2}, Lorg/apache/lucene/index/StoredFieldVisitor;->stringField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    .end local v0    # "data":[B
    .end local v1    # "length":I
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readInt()I

    move-result v2

    invoke-virtual {p1, p2, v2}, Lorg/apache/lucene/index/StoredFieldVisitor;->intField(Lorg/apache/lucene/index/FieldInfo;I)V

    goto :goto_0

    .line 160
    :pswitch_3
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v2

    invoke-virtual {p1, p2, v2}, Lorg/apache/lucene/index/StoredFieldVisitor;->floatField(Lorg/apache/lucene/index/FieldInfo;F)V

    goto :goto_0

    .line 163
    :pswitch_4
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readLong()J

    move-result-wide v2

    invoke-virtual {p1, p2, v2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->longField(Lorg/apache/lucene/index/FieldInfo;J)V

    goto :goto_0

    .line 166
    :pswitch_5
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    invoke-virtual {p1, p2, v2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->doubleField(Lorg/apache/lucene/index/FieldInfo;D)V

    goto :goto_0

    .line 143
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static skipField(Lorg/apache/lucene/store/ByteArrayDataInput;I)V
    .locals 4
    .param p0, "in"    # Lorg/apache/lucene/store/ByteArrayDataInput;
    .param p1, "bits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    sget v1, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_MASK:I

    and-int/2addr v1, p1

    packed-switch v1, :pswitch_data_0

    .line 189
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type flag: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 177
    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    .line 178
    .local v0, "length":I
    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->skipBytes(I)V

    .line 191
    .end local v0    # "length":I
    :goto_0
    return-void

    .line 182
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readInt()I

    goto :goto_0

    .line 186
    :pswitch_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readLong()J

    goto :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method chunkIterator(I)Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;
    .locals 4
    .param p1, "startDocID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 297
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->ensureOpen()V

    .line 298
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 299
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;)V

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/codecs/StoredFieldsReader;
    .locals 1

    .prologue
    .line 288
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->ensureOpen()V

    .line 289
    new-instance v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)V

    return-object v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 136
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->closed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 137
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    aput-object v1, v0, v3

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 138
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->closed:Z

    .line 140
    :cond_0
    return-void
.end method

.method getCompressionMode()Lorg/apache/lucene/codecs/compressing/CompressionMode;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->compressionMode:Lorg/apache/lucene/codecs/compressing/CompressionMode;

    return-object v0
.end method

.method public visitDocument(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 34
    .param p1, "docID"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v32

    move-wide/from16 v0, v32

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 198
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v17

    .line 199
    .local v17, "docBase":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    .line 200
    .local v7, "chunkDocs":I
    move/from16 v0, p1

    move/from16 v1, v17

    if-lt v0, v1, :cond_0

    .line 201
    add-int v4, v17, v7

    move/from16 v0, p1

    if-ge v0, v4, :cond_0

    .line 202
    add-int v4, v17, v7

    move-object/from16 v0, p0

    iget v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I

    if-le v4, v5, :cond_1

    .line 203
    :cond_0
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Corrupted: docID="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 204
    const-string v6, ", docBase="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", chunkDocs="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 205
    const-string v6, ", numDocs="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 203
    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 209
    :cond_1
    const/4 v4, 0x1

    if-ne v7, v4, :cond_2

    .line 210
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v28

    .line 211
    .local v28, "numStoredFields":I
    const/4 v12, 0x0

    .line 212
    .local v12, "offset":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v13

    .line 213
    .local v13, "length":I
    move v11, v13

    .line 250
    .local v11, "totalLength":I
    :goto_0
    if-nez v13, :cond_9

    const/4 v4, 0x1

    move v5, v4

    :goto_1
    if-nez v28, :cond_a

    const/4 v4, 0x1

    :goto_2
    if-eq v5, v4, :cond_b

    .line 251
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "length="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", numStoredFields="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 215
    .end local v11    # "totalLength":I
    .end local v12    # "offset":I
    .end local v13    # "length":I
    .end local v28    # "numStoredFields":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v16

    .line 216
    .local v16, "bitsPerStoredFields":I
    if-nez v16, :cond_3

    .line 217
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v28

    .line 227
    .restart local v28    # "numStoredFields":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v8

    .line 228
    .local v8, "bitsPerLength":I
    if-nez v8, :cond_5

    .line 229
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v13

    .line 230
    .restart local v13    # "length":I
    sub-int v4, p1, v17

    mul-int v12, v4, v13

    .line 231
    .restart local v12    # "offset":I
    mul-int v11, v7, v13

    .line 232
    .restart local v11    # "totalLength":I
    goto :goto_0

    .line 218
    .end local v8    # "bitsPerLength":I
    .end local v11    # "totalLength":I
    .end local v12    # "offset":I
    .end local v13    # "length":I
    .end local v28    # "numStoredFields":I
    :cond_3
    const/16 v4, 0x1f

    move/from16 v0, v16

    if-le v0, v4, :cond_4

    .line 219
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bitsPerStoredFields="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 221
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v22

    .line 222
    .local v22, "filePointer":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v5, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    move/from16 v0, v16

    invoke-static {v4, v5, v6, v7, v0}, Lorg/apache/lucene/util/packed/PackedInts;->getDirectReaderNoHeader(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/packed/PackedInts$Format;III)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v30

    .line 223
    .local v30, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    sub-int v4, p1, v17

    move-object/from16 v0, v30

    invoke-interface {v0, v4}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v28, v0

    .line 224
    .restart local v28    # "numStoredFields":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v5, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    move/from16 v0, v16

    invoke-virtual {v5, v6, v7, v0}, Lorg/apache/lucene/util/packed/PackedInts$Format;->byteCount(III)J

    move-result-wide v32

    add-long v32, v32, v22

    move-wide/from16 v0, v32

    invoke-virtual {v4, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_3

    .line 232
    .end local v22    # "filePointer":J
    .end local v30    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v8    # "bitsPerLength":I
    :cond_5
    const/16 v4, 0x1f

    move/from16 v0, v16

    if-le v0, v4, :cond_6

    .line 233
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bitsPerLength="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 235
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    sget-object v5, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I

    const/4 v9, 0x1

    invoke-static/range {v4 .. v9}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderIteratorNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;IIII)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;

    move-result-object v25

    .line 236
    .local v25, "it":Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    const/16 v29, 0x0

    .line 237
    .local v29, "off":I
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_4
    sub-int v4, p1, v17

    move/from16 v0, v24

    if-lt v0, v4, :cond_7

    .line 240
    move/from16 v12, v29

    .line 241
    .restart local v12    # "offset":I
    invoke-interface/range {v25 .. v25}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;->next()J

    move-result-wide v4

    long-to-int v13, v4

    .line 242
    .restart local v13    # "length":I
    add-int v29, v29, v13

    .line 243
    sub-int v4, p1, v17

    add-int/lit8 v24, v4, 0x1

    :goto_5
    move/from16 v0, v24

    if-lt v0, v7, :cond_8

    .line 246
    move/from16 v11, v29

    .restart local v11    # "totalLength":I
    goto/16 :goto_0

    .line 238
    .end local v11    # "totalLength":I
    .end local v12    # "offset":I
    .end local v13    # "length":I
    :cond_7
    move/from16 v0, v29

    int-to-long v4, v0

    invoke-interface/range {v25 .. v25}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;->next()J

    move-result-wide v32

    add-long v4, v4, v32

    long-to-int v0, v4

    move/from16 v29, v0

    .line 237
    add-int/lit8 v24, v24, 0x1

    goto :goto_4

    .line 244
    .restart local v12    # "offset":I
    .restart local v13    # "length":I
    :cond_8
    move/from16 v0, v29

    int-to-long v4, v0

    invoke-interface/range {v25 .. v25}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;->next()J

    move-result-wide v32

    add-long v4, v4, v32

    long-to-int v0, v4

    move/from16 v29, v0

    .line 243
    add-int/lit8 v24, v24, 0x1

    goto :goto_5

    .line 250
    .end local v8    # "bitsPerLength":I
    .end local v16    # "bitsPerStoredFields":I
    .end local v24    # "i":I
    .end local v25    # "it":Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    .end local v29    # "off":I
    .restart local v11    # "totalLength":I
    :cond_9
    const/4 v4, 0x0

    move v5, v4

    goto/16 :goto_1

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 253
    :cond_b
    if-nez v28, :cond_d

    .line 284
    :cond_c
    :pswitch_0
    return-void

    .line 258
    :cond_d
    move-object/from16 v0, p0

    iget-object v9, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual/range {v9 .. v14}, Lorg/apache/lucene/codecs/compressing/Decompressor;->decompress(Lorg/apache/lucene/store/DataInput;IIILorg/apache/lucene/util/BytesRef;)V

    .line 259
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eq v4, v13, :cond_e

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 261
    :cond_e
    new-instance v18, Lorg/apache/lucene/store/ByteArrayDataInput;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, v18

    invoke-direct {v0, v4, v5, v6}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>([BII)V

    .line 262
    .local v18, "documentInput":Lorg/apache/lucene/store/ByteArrayDataInput;
    const/16 v19, 0x0

    .local v19, "fieldIDX":I
    :goto_6
    move/from16 v0, v19

    move/from16 v1, v28

    if-lt v0, v1, :cond_f

    .line 283
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    if-eq v4, v5, :cond_c

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 263
    :cond_f
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v26

    .line 264
    .local v26, "infoAndBits":J
    sget v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_BITS:I

    ushr-long v4, v26, v4

    long-to-int v0, v4

    move/from16 v21, v0

    .line 265
    .local v21, "fieldNumber":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v20

    .line 267
    .local v20, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    sget v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsWriter;->TYPE_MASK:I

    int-to-long v4, v4

    and-long v4, v4, v26

    long-to-int v15, v4

    .line 268
    .local v15, "bits":I
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_10

    const/4 v4, 0x5

    if-le v15, v4, :cond_10

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "bits="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v15}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 270
    :cond_10
    invoke-static {}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/StoredFieldVisitor;->needsField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 262
    :cond_11
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_6

    .line 272
    :pswitch_1
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v15}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->readField(Lorg/apache/lucene/store/ByteArrayDataInput;Lorg/apache/lucene/index/StoredFieldVisitor;Lorg/apache/lucene/index/FieldInfo;I)V

    .line 273
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    if-le v4, v5, :cond_11

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 276
    :pswitch_2
    move-object/from16 v0, v18

    invoke-static {v0, v15}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->skipField(Lorg/apache/lucene/store/ByteArrayDataInput;I)V

    .line 277
    sget-boolean v4, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v5, v6

    if-le v4, v5, :cond_11

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
