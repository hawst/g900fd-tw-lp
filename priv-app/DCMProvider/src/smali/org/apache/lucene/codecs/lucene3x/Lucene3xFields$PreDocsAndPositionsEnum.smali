.class final Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "Lucene3xFields.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PreDocsAndPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private docID:I

.field private final pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 999
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1002
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 1001
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    .line 1003
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    iget-object v1, p1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->proxStream:Lorg/apache/lucene/store/IndexInput;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v3

    # getter for: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$1(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;-><init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;Lorg/apache/lucene/index/FieldInfos;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    .line 1004
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1028
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->skipTo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->doc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    .line 1031
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 1068
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    iget v0, v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->df:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 1042
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    return v0
.end method

.method public endOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1058
    const/4 v0, -0x1

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1037
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->freq()I

    move-result v0

    return v0
.end method

.method getFreqStream()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    iget-object v0, v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1063
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1019
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->next()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->doc()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    .line 1022
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1047
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1048
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->nextPosition()I

    move-result v0

    return v0
.end method

.method public reset(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "termEnum"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1011
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->setLiveDocs(Lorg/apache/lucene/util/Bits;)V

    .line 1012
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->pos:Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermPositions;->seek(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)V

    .line 1013
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->docID:I

    .line 1014
    return-object p0
.end method

.method public startOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1053
    const/4 v0, -0x1

    return v0
.end method
