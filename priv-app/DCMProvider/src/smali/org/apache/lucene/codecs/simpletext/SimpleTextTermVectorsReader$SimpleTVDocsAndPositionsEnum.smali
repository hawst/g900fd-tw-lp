.class Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleTVDocsAndPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private didNext:Z

.field private doc:I

.field private endOffsets:[I

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private nextPos:I

.field private payloads:[Lorg/apache/lucene/util/BytesRef;

.field private positions:[I

.field private startOffsets:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 450
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 452
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->doc:I

    .line 450
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;)V
    .locals 0

    .prologue
    .line 450
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;-><init>()V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 538
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 2

    .prologue
    .line 529
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->endOffsets:[I

    if-nez v0, :cond_0

    .line 530
    const/4 v0, -0x1

    .line 532
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->endOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 462
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    array-length v0, v0

    .line 466
    :goto_0
    return v0

    .line 465
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 466
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    array-length v0, v0

    goto :goto_0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->payloads:[Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->payloads:[Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 477
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->didNext:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 478
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->didNext:Z

    .line 479
    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->doc:I

    .line 481
    :goto_0
    return v0

    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->doc:I

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 3

    .prologue
    .line 508
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 509
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    array-length v1, v1

    if-lt v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 510
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    if-eqz v0, :cond_3

    .line 511
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    aget v0, v0, v1

    .line 514
    :goto_0
    return v0

    .line 513
    :cond_3
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    .line 514
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;[I[I[I[Lorg/apache/lucene/util/BytesRef;)V
    .locals 2
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "positions"    # [I
    .param p3, "startOffsets"    # [I
    .param p4, "endOffsets"    # [I
    .param p5, "payloads"    # [Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v1, 0x0

    .line 491
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 492
    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->positions:[I

    .line 493
    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    .line 494
    iput-object p4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->endOffsets:[I

    .line 495
    iput-object p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->payloads:[Lorg/apache/lucene/util/BytesRef;

    .line 496
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->doc:I

    .line 497
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->didNext:Z

    .line 498
    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    .line 499
    return-void
.end method

.method public startOffset()I
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    if-nez v0, :cond_0

    .line 521
    const/4 v0, -0x1

    .line 523
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->startOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVDocsAndPositionsEnum;->nextPos:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0
.end method
