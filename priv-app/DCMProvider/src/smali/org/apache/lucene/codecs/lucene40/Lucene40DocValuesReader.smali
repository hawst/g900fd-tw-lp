.class final Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;
.super Lorg/apache/lucene/codecs/DocValuesProducer;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType:[I = null

.field private static final segmentSuffix:Ljava/lang/String; = "dv"


# instance fields
.field private final binaryInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/BinaryDocValues;",
            ">;"
        }
    .end annotation
.end field

.field private final dir:Lorg/apache/lucene/store/Directory;

.field private final legacyKey:Ljava/lang/String;

.field private final numericInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/NumericDocValues;",
            ">;"
        }
    .end annotation
.end field

.field private final sortedInstances:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/SortedDocValues;",
            ">;"
        }
    .end annotation
.end field

.field private final state:Lorg/apache/lucene/index/SegmentReadState;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType()[I
    .locals 3

    .prologue
    .line 49
    sget-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->$SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->values()[Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_FIXED_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_DEREF:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_SORTED:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->BYTES_VAR_STRAIGHT:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_16:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    :goto_7
    :try_start_7
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_8
    :try_start_8
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_9
    :try_start_9
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FIXED_INTS_8:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_a
    :try_start_a
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FLOAT_32:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_b
    :try_start_b
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->FLOAT_64:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_c
    :try_start_c
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->NONE:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_d
    :try_start_d
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->VAR_INTS:Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_e
    sput-object v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->$SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_e

    :catch_1
    move-exception v1

    goto :goto_d

    :catch_2
    move-exception v1

    goto :goto_c

    :catch_3
    move-exception v1

    goto :goto_b

    :catch_4
    move-exception v1

    goto :goto_a

    :catch_5
    move-exception v1

    goto :goto_9

    :catch_6
    move-exception v1

    goto :goto_8

    :catch_7
    move-exception v1

    goto :goto_7

    :catch_8
    move-exception v1

    goto :goto_6

    :catch_9
    move-exception v1

    goto :goto_5

    :catch_a
    move-exception v1

    goto :goto_4

    :catch_b
    move-exception v1

    goto/16 :goto_3

    :catch_c
    move-exception v1

    goto/16 :goto_2

    :catch_d
    move-exception v1

    goto/16 :goto_1
.end method

.method constructor <init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "legacyKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->numericInstances:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->binaryInstances:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->sortedInstances:Ljava/util/Map;

    .line 64
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    .line 65
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->legacyKey:Ljava/lang/String;

    .line 66
    new-instance v0, Lorg/apache/lucene/store/CompoundFileDirectory;

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p2, v2, v3}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    .line 67
    return-void
.end method

.method private correctBuggyOrds(Lorg/apache/lucene/index/SortedDocValues;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 3
    .param p1, "in"    # Lorg/apache/lucene/index/SortedDocValues;

    .prologue
    .line 592
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 593
    .local v1, "maxDoc":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 600
    new-instance v2, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$15;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/index/SortedDocValues;)V

    move-object p1, v2

    .end local p1    # "in":Lorg/apache/lucene/index/SortedDocValues;
    :cond_0
    return-object p1

    .line 594
    .restart local p1    # "in":Lorg/apache/lucene/index/SortedDocValues;
    :cond_1
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v2

    if-eqz v2, :cond_0

    .line 593
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadByteField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 6
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 156
    const-string v3, "Ints"

    invoke-static {p2, v3, v4, v4}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 159
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v1

    .line 160
    .local v1, "valueSize":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    .line 161
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "invalid valueSize: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 163
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    .line 164
    .local v0, "maxDoc":I
    new-array v2, v0, [B

    .line 165
    .local v2, "values":[B
    array-length v3, v2

    invoke-virtual {p2, v2, v4, v3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 166
    new-instance v3, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$3;

    invoke-direct {v3, p0, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$3;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[B)V

    return-object v3
.end method

.method private loadBytesFixedDeref(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 14
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "dv"

    const-string v12, "dat"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 381
    .local v3, "dataName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "dv"

    const-string v12, "idx"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 382
    .local v6, "indexName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 383
    .local v2, "data":Lorg/apache/lucene/store/IndexInput;
    const/4 v5, 0x0

    .line 384
    .local v5, "index":Lorg/apache/lucene/store/IndexInput;
    const/4 v8, 0x0

    .line 386
    .local v8, "success":Z
    :try_start_0
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v3, v11}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 387
    const-string v10, "FixedDerefBytesDat"

    .line 388
    const/4 v11, 0x0

    .line 389
    const/4 v12, 0x0

    .line 387
    invoke-static {v2, v10, v11, v12}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 390
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v6, v11}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v5

    .line 391
    const-string v10, "FixedDerefBytesIdx"

    .line 392
    const/4 v11, 0x0

    .line 393
    const/4 v12, 0x0

    .line 391
    invoke-static {v5, v10, v11, v12}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 395
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v4

    .line 396
    .local v4, "fixedLength":I
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v9

    .line 397
    .local v9, "valueCount":I
    new-instance v0, Lorg/apache/lucene/util/PagedBytes;

    const/16 v10, 0x10

    invoke-direct {v0, v10}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 398
    .local v0, "bytes":Lorg/apache/lucene/util/PagedBytes;
    int-to-long v10, v4

    int-to-long v12, v9

    mul-long/2addr v10, v12

    invoke-virtual {v0, v2, v10, v11}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 399
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v1

    .line 400
    .local v1, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    invoke-static {v5}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v7

    .line 401
    .local v7, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    .line 402
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "did not read all bytes from file \""

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\": read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " (resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v4    # "fixedLength":I
    .end local v7    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v9    # "valueCount":I
    :catchall_0
    move-exception v10

    .line 416
    if-eqz v8, :cond_3

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 417
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 421
    :goto_0
    throw v10

    .line 404
    .restart local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .restart local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .restart local v4    # "fixedLength":I
    .restart local v7    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v9    # "valueCount":I
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_1

    .line 405
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "did not read all bytes from file \""

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\": read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " (resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 407
    :cond_1
    const/4 v8, 0x1

    .line 408
    new-instance v10, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;

    invoke-direct {v10, p0, v4, v7, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$11;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;ILorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416
    if-eqz v8, :cond_2

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 417
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 408
    :goto_1
    return-object v10

    .line 418
    :cond_2
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 419
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 418
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v4    # "fixedLength":I
    .end local v7    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v9    # "valueCount":I
    :cond_3
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 419
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private loadBytesFixedSorted(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 10
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "data"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "index"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 520
    const-string v0, "FixedSortedBytesDat"

    invoke-static {p2, v0, v1, v1}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 523
    const-string v0, "FixedSortedBytesIdx"

    invoke-static {p3, v0, v1, v1}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 527
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v4

    .line 528
    .local v4, "fixedLength":I
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v5

    .line 530
    .local v5, "valueCount":I
    new-instance v6, Lorg/apache/lucene/util/PagedBytes;

    const/16 v0, 0x10

    invoke-direct {v6, v0}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 531
    .local v6, "bytes":Lorg/apache/lucene/util/PagedBytes;
    int-to-long v0, v4

    int-to-long v8, v5

    mul-long/2addr v0, v8

    invoke-virtual {v6, p2, v0, v1}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 532
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v3

    .line 533
    .local v3, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    invoke-static {p3}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v2

    .line 535
    .local v2, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;II)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->correctBuggyOrds(Lorg/apache/lucene/index/SortedDocValues;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    return-object v0
.end method

.method private loadBytesFixedStraight(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 12
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 305
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "dv"

    const-string v8, "dat"

    invoke-static {v6, v7, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 306
    .local v2, "fileName":Ljava/lang/String;
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v6, v2, v7}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 307
    .local v4, "input":Lorg/apache/lucene/store/IndexInput;
    const/4 v5, 0x0

    .line 309
    .local v5, "success":Z
    :try_start_0
    const-string v6, "FixedStraightBytes"

    .line 310
    const/4 v7, 0x0

    .line 311
    const/4 v8, 0x0

    .line 309
    invoke-static {v4, v6, v7, v8}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 312
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v3

    .line 313
    .local v3, "fixedLength":I
    new-instance v0, Lorg/apache/lucene/util/PagedBytes;

    const/16 v6, 0x10

    invoke-direct {v0, v6}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 314
    .local v0, "bytes":Lorg/apache/lucene/util/PagedBytes;
    int-to-long v6, v3

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v8}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    int-to-long v8, v8

    mul-long/2addr v6, v8

    invoke-virtual {v0, v4, v6, v7}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 315
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v1

    .line 316
    .local v1, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 317
    new-instance v6, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "did not read all bytes from file \""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\": read "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " vs size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (resource: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v3    # "fixedLength":I
    :catchall_0
    move-exception v6

    .line 327
    if-eqz v5, :cond_2

    new-array v7, v11, [Ljava/io/Closeable;

    .line 328
    aput-object v4, v7, v10

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 332
    :goto_0
    throw v6

    .line 319
    .restart local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .restart local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .restart local v3    # "fixedLength":I
    :cond_0
    const/4 v5, 0x1

    .line 320
    :try_start_1
    new-instance v6, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;

    invoke-direct {v6, p0, v1, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$9;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/PagedBytes$Reader;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 327
    if-eqz v5, :cond_1

    new-array v7, v11, [Ljava/io/Closeable;

    .line 328
    aput-object v4, v7, v10

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 320
    :goto_1
    return-object v6

    .line 329
    :cond_1
    new-array v7, v11, [Ljava/io/Closeable;

    .line 330
    aput-object v4, v7, v10

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 329
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v3    # "fixedLength":I
    :cond_2
    new-array v7, v11, [Ljava/io/Closeable;

    .line 330
    aput-object v4, v7, v10

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private loadBytesVarDeref(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 14
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 425
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "dv"

    const-string v12, "dat"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "dataName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "dv"

    const-string v12, "idx"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 427
    .local v5, "indexName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 428
    .local v2, "data":Lorg/apache/lucene/store/IndexInput;
    const/4 v4, 0x0

    .line 429
    .local v4, "index":Lorg/apache/lucene/store/IndexInput;
    const/4 v7, 0x0

    .line 431
    .local v7, "success":Z
    :try_start_0
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v3, v11}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 432
    const-string v10, "VarDerefBytesDat"

    .line 433
    const/4 v11, 0x0

    .line 434
    const/4 v12, 0x0

    .line 432
    invoke-static {v2, v10, v11, v12}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 435
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v5, v11}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 436
    const-string v10, "VarDerefBytesIdx"

    .line 437
    const/4 v11, 0x0

    .line 438
    const/4 v12, 0x0

    .line 436
    invoke-static {v4, v10, v11, v12}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 440
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 441
    .local v8, "totalBytes":J
    new-instance v0, Lorg/apache/lucene/util/PagedBytes;

    const/16 v10, 0x10

    invoke-direct {v0, v10}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 442
    .local v0, "bytes":Lorg/apache/lucene/util/PagedBytes;
    invoke-virtual {v0, v2, v8, v9}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 443
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v1

    .line 444
    .local v1, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v6

    .line 445
    .local v6, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    .line 446
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "did not read all bytes from file \""

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\": read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " (resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 469
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v6    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v8    # "totalBytes":J
    :catchall_0
    move-exception v10

    .line 470
    if-eqz v7, :cond_3

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 471
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 475
    :goto_0
    throw v10

    .line 448
    .restart local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .restart local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .restart local v6    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v8    # "totalBytes":J
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_1

    .line 449
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "did not read all bytes from file \""

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\": read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " (resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 451
    :cond_1
    const/4 v7, 0x1

    .line 452
    new-instance v10, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;

    invoke-direct {v10, p0, v6, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$12;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470
    if-eqz v7, :cond_2

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 471
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 452
    :goto_1
    return-object v10

    .line 472
    :cond_2
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 473
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 472
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v6    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v8    # "totalBytes":J
    :cond_3
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 473
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private loadBytesVarSorted(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 10
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "data"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "index"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 554
    const-string v0, "VarDerefBytesDat"

    invoke-static {p2, v0, v1, v1}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 557
    const-string v0, "VarDerefBytesIdx"

    invoke-static {p3, v0, v1, v1}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 561
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 562
    .local v8, "maxAddress":J
    new-instance v6, Lorg/apache/lucene/util/PagedBytes;

    const/16 v0, 0x10

    invoke-direct {v6, v0}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 563
    .local v6, "bytes":Lorg/apache/lucene/util/PagedBytes;
    invoke-virtual {v6, p2, v8, v9}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 564
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v4

    .line 565
    .local v4, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    invoke-static {p3}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v3

    .line 566
    .local v3, "addressReader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-static {p3}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v2

    .line 568
    .local v2, "ordsReader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-interface {v3}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->size()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    .line 570
    .local v5, "valueCount":I
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$14;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;I)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->correctBuggyOrds(Lorg/apache/lucene/index/SortedDocValues;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    return-object v0
.end method

.method private loadBytesVarStraight(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 14
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "dv"

    const-string v12, "dat"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 337
    .local v3, "dataName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "dv"

    const-string v12, "idx"

    invoke-static {v10, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 338
    .local v5, "indexName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 339
    .local v2, "data":Lorg/apache/lucene/store/IndexInput;
    const/4 v4, 0x0

    .line 340
    .local v4, "index":Lorg/apache/lucene/store/IndexInput;
    const/4 v7, 0x0

    .line 342
    .local v7, "success":Z
    :try_start_0
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v3, v11}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 343
    const-string v10, "VarStraightBytesDat"

    .line 344
    const/4 v11, 0x0

    .line 345
    const/4 v12, 0x0

    .line 343
    invoke-static {v2, v10, v11, v12}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 346
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v11, v11, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v5, v11}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 347
    const-string v10, "VarStraightBytesIdx"

    .line 348
    const/4 v11, 0x0

    .line 349
    const/4 v12, 0x0

    .line 347
    invoke-static {v4, v10, v11, v12}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 350
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v8

    .line 351
    .local v8, "totalBytes":J
    new-instance v0, Lorg/apache/lucene/util/PagedBytes;

    const/16 v10, 0x10

    invoke-direct {v0, v10}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .line 352
    .local v0, "bytes":Lorg/apache/lucene/util/PagedBytes;
    invoke-virtual {v0, v2, v8, v9}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/store/IndexInput;J)V

    .line 353
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Lorg/apache/lucene/util/PagedBytes;->freeze(Z)Lorg/apache/lucene/util/PagedBytes$Reader;

    move-result-object v1

    .line 354
    .local v1, "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    invoke-static {v4}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v6

    .line 355
    .local v6, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    .line 356
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "did not read all bytes from file \""

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\": read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " (resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v6    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v8    # "totalBytes":J
    :catchall_0
    move-exception v10

    .line 371
    if-eqz v7, :cond_3

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 372
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 376
    :goto_0
    throw v10

    .line 358
    .restart local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .restart local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .restart local v6    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .restart local v8    # "totalBytes":J
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-eqz v10, :cond_1

    .line 359
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "did not read all bytes from file \""

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\": read "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " vs size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " (resource: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 361
    :cond_1
    const/4 v7, 0x1

    .line 362
    new-instance v10, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;

    invoke-direct {v10, p0, v6, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 371
    if-eqz v7, :cond_2

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 372
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 362
    :goto_1
    return-object v10

    .line 373
    :cond_2
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 374
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 373
    .end local v0    # "bytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v1    # "bytesReader":Lorg/apache/lucene/util/PagedBytes$Reader;
    .end local v6    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v8    # "totalBytes":J
    :cond_3
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 374
    aput-object v2, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private loadDoubleField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 7
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 259
    const-string v4, "Floats"

    invoke-static {p2, v4, v5, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 262
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .line 263
    .local v2, "valueSize":I
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 264
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid valueSize: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 266
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 267
    .local v1, "maxDoc":I
    new-array v3, v1, [J

    .line 268
    .local v3, "values":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    .line 271
    new-instance v4, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$8;

    invoke-direct {v4, p0, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$8;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[J)V

    return-object v4

    .line 269
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v0

    .line 268
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadFloatField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 7
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 238
    const-string v4, "Floats"

    invoke-static {p2, v4, v5, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 241
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .line 242
    .local v2, "valueSize":I
    const/4 v4, 0x4

    if-eq v2, v4, :cond_0

    .line 243
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid valueSize: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 245
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 246
    .local v1, "maxDoc":I
    new-array v3, v1, [I

    .line 247
    .local v3, "values":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    .line 250
    new-instance v4, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$7;

    invoke-direct {v4, p0, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$7;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[I)V

    return-object v4

    .line 248
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v4

    aput v4, v3, v0

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadIntField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 7
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 196
    const-string v4, "Ints"

    invoke-static {p2, v4, v5, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 199
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .line 200
    .local v2, "valueSize":I
    const/4 v4, 0x4

    if-eq v2, v4, :cond_0

    .line 201
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid valueSize: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 203
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 204
    .local v1, "maxDoc":I
    new-array v3, v1, [I

    .line 205
    .local v3, "values":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    .line 208
    new-instance v4, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$5;

    invoke-direct {v4, p0, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$5;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[I)V

    return-object v4

    .line 206
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v4

    aput v4, v3, v0

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadLongField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 7
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 217
    const-string v4, "Ints"

    invoke-static {p2, v4, v5, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 220
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .line 221
    .local v2, "valueSize":I
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 222
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid valueSize: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 224
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 225
    .local v1, "maxDoc":I
    new-array v3, v1, [J

    .line 226
    .local v3, "values":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    .line 229
    new-instance v4, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$6;

    invoke-direct {v4, p0, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$6;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[J)V

    return-object v4

    .line 227
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    aput-wide v4, v3, v0

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadShortField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 7
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 175
    const-string v4, "Ints"

    invoke-static {p2, v4, v5, v5}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 178
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    .line 179
    .local v2, "valueSize":I
    const/4 v4, 0x2

    if-eq v2, v4, :cond_0

    .line 180
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid valueSize: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 182
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 183
    .local v1, "maxDoc":I
    new-array v3, v1, [S

    .line 184
    .local v3, "values":[S
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    .line 187
    new-instance v4, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$4;

    invoke-direct {v4, p0, v3}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$4;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[S)V

    return-object v4

    .line 185
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readShort()S

    move-result v4

    aput-short v4, v3, v0

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private loadVarIntsField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 16
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    const-string v3, "PackedInts"

    .line 120
    const/4 v4, 0x0

    .line 121
    const/4 v13, 0x0

    .line 119
    move-object/from16 v0, p2

    invoke-static {v0, v3, v4, v13}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 122
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v2

    .line 123
    .local v2, "header":B
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 124
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v11

    .line 125
    .local v11, "maxDoc":I
    new-array v12, v11, [J

    .line 126
    .local v12, "values":[J
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    array-length v3, v12

    if-lt v10, v3, :cond_0

    .line 129
    new-instance v3, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v12}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$1;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;[J)V

    .line 139
    .end local v10    # "i":I
    .end local v11    # "maxDoc":I
    .end local v12    # "values":[J
    :goto_1
    return-object v3

    .line 127
    .restart local v10    # "i":I
    .restart local v11    # "maxDoc":I
    .restart local v12    # "values":[J
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v14

    aput-wide v14, v12, v10

    .line 126
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 135
    .end local v10    # "i":I
    .end local v11    # "maxDoc":I
    .end local v12    # "values":[J
    :cond_1
    if-nez v2, :cond_2

    .line 136
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v8

    .line 137
    .local v8, "minValue":J
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    .line 138
    .local v6, "defaultValue":J
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/util/packed/PackedInts;->getReader(Lorg/apache/lucene/store/DataInput;)Lorg/apache/lucene/util/packed/PackedInts$Reader;

    move-result-object v5

    .line 139
    .local v5, "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    new-instance v3, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v9}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$2;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;JJ)V

    goto :goto_1

    .line 151
    .end local v5    # "reader":Lorg/apache/lucene/util/packed/PackedInts$Reader;
    .end local v6    # "defaultValue":J
    .end local v8    # "minValue":J
    :cond_2
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v13, "invalid VAR_INTS header byte: "

    invoke-direct {v4, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v13, " (resource="

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v13, ")"

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 625
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 626
    return-void
.end method

.method public declared-synchronized getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 3
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->binaryInstances:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/BinaryDocValues;

    .line 282
    .local v0, "instance":Lorg/apache/lucene/index/BinaryDocValues;
    if-nez v0, :cond_0

    .line 283
    invoke-static {}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->$SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType()[I

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->legacyKey:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 297
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    .end local v0    # "instance":Lorg/apache/lucene/index/BinaryDocValues;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 285
    .restart local v0    # "instance":Lorg/apache/lucene/index/BinaryDocValues;
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesFixedStraight(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    .line 299
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->binaryInstances:Ljava/util/Map;

    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 301
    :cond_0
    monitor-exit p0

    return-object v0

    .line 288
    :pswitch_1
    :try_start_2
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesVarStraight(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    .line 289
    goto :goto_0

    .line 291
    :pswitch_2
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesFixedDeref(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    .line 292
    goto :goto_0

    .line 294
    :pswitch_3
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesVarDeref(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 295
    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 8
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->numericInstances:Ljava/util/Map;

    iget v5, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/NumericDocValues;

    .line 72
    .local v2, "instance":Lorg/apache/lucene/index/NumericDocValues;
    if-nez v2, :cond_1

    .line 73
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "dv"

    const-string v6, "dat"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "fileName":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v0, v5}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 75
    .local v1, "input":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 77
    .local v3, "success":Z
    :try_start_1
    invoke-static {}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->$SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType()[I

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->legacyKey:Ljava/lang/String;

    invoke-virtual {p1, v5}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 100
    :pswitch_0
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :catchall_0
    move-exception v4

    .line 107
    if-eqz v3, :cond_2

    const/4 v5, 0x1

    :try_start_2
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 108
    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 112
    :goto_0
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 71
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "input":Lorg/apache/lucene/store/IndexInput;
    .end local v2    # "instance":Lorg/apache/lucene/index/NumericDocValues;
    .end local v3    # "success":Z
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 79
    .restart local v0    # "fileName":Ljava/lang/String;
    .restart local v1    # "input":Lorg/apache/lucene/store/IndexInput;
    .restart local v2    # "instance":Lorg/apache/lucene/index/NumericDocValues;
    .restart local v3    # "success":Z
    :pswitch_1
    :try_start_3
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadVarIntsField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 102
    :goto_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 103
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "did not read all bytes from file \""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\": read "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " vs size "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " (resource: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 82
    :pswitch_2
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadByteField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 83
    goto :goto_1

    .line 85
    :pswitch_3
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadShortField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 86
    goto :goto_1

    .line 88
    :pswitch_4
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadIntField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 89
    goto :goto_1

    .line 91
    :pswitch_5
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadLongField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 92
    goto :goto_1

    .line 94
    :pswitch_6
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadFloatField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v2

    .line 95
    goto :goto_1

    .line 97
    :pswitch_7
    invoke-direct {p0, p1, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadDoubleField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/NumericDocValues;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 98
    goto :goto_1

    .line 105
    :cond_0
    const/4 v3, 0x1

    .line 107
    if-eqz v3, :cond_3

    const/4 v4, 0x1

    :try_start_4
    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 108
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 113
    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->numericInstances:Ljava/util/Map;

    iget v5, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 115
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "input":Lorg/apache/lucene/store/IndexInput;
    .end local v3    # "success":Z
    :cond_1
    monitor-exit p0

    return-object v2

    .line 109
    .restart local v0    # "fileName":Ljava/lang/String;
    .restart local v1    # "input":Lorg/apache/lucene/store/IndexInput;
    .restart local v3    # "success":Z
    :cond_2
    const/4 v5, 0x1

    :try_start_5
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 110
    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 109
    :cond_3
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 110
    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 10
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 480
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->sortedInstances:Ljava/util/Map;

    iget v7, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SortedDocValues;

    .line 481
    .local v4, "instance":Lorg/apache/lucene/index/SortedDocValues;
    if-nez v4, :cond_2

    .line 482
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "dv"

    const-string v8, "dat"

    invoke-static {v6, v7, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "dataName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "dv"

    const-string v8, "idx"

    invoke-static {v6, v7, v8}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 484
    .local v3, "indexName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 485
    .local v0, "data":Lorg/apache/lucene/store/IndexInput;
    const/4 v2, 0x0

    .line 486
    .local v2, "index":Lorg/apache/lucene/store/IndexInput;
    const/4 v5, 0x0

    .line 488
    .local v5, "success":Z
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v6, v1, v7}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 489
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->state:Lorg/apache/lucene/index/SegmentReadState;

    iget-object v7, v7, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v6, v3, v7}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 490
    invoke-static {}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->$SWITCH_TABLE$org$apache$lucene$codecs$lucene40$Lucene40FieldInfosReader$LegacyDocValuesType()[I

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->legacyKey:Ljava/lang/String;

    invoke-virtual {p1, v7}, Lorg/apache/lucene/index/FieldInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader$LegacyDocValuesType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 498
    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 507
    :catchall_0
    move-exception v6

    .line 508
    if-eqz v5, :cond_3

    const/4 v7, 0x2

    :try_start_2
    new-array v7, v7, [Ljava/io/Closeable;

    const/4 v8, 0x0

    .line 509
    aput-object v0, v7, v8

    const/4 v8, 0x1

    aput-object v2, v7, v8

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 513
    :goto_0
    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 480
    .end local v0    # "data":Lorg/apache/lucene/store/IndexInput;
    .end local v1    # "dataName":Ljava/lang/String;
    .end local v2    # "index":Lorg/apache/lucene/store/IndexInput;
    .end local v3    # "indexName":Ljava/lang/String;
    .end local v4    # "instance":Lorg/apache/lucene/index/SortedDocValues;
    .end local v5    # "success":Z
    :catchall_1
    move-exception v6

    monitor-exit p0

    throw v6

    .line 492
    .restart local v0    # "data":Lorg/apache/lucene/store/IndexInput;
    .restart local v1    # "dataName":Ljava/lang/String;
    .restart local v2    # "index":Lorg/apache/lucene/store/IndexInput;
    .restart local v3    # "indexName":Ljava/lang/String;
    .restart local v4    # "instance":Lorg/apache/lucene/index/SortedDocValues;
    .restart local v5    # "success":Z
    :pswitch_0
    :try_start_3
    invoke-direct {p0, p1, v0, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesFixedSorted(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v4

    .line 500
    :goto_1
    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 501
    new-instance v6, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "did not read all bytes from file \""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\": read "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " vs size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (resource: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 495
    :pswitch_1
    invoke-direct {p0, p1, v0, v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesVarSorted(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v4

    .line 496
    goto :goto_1

    .line 503
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    .line 504
    new-instance v6, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "did not read all bytes from file \""

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\": read "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " vs size "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (resource: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 506
    :cond_1
    const/4 v5, 0x1

    .line 508
    if-eqz v5, :cond_4

    const/4 v6, 0x2

    :try_start_4
    new-array v6, v6, [Ljava/io/Closeable;

    const/4 v7, 0x0

    .line 509
    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 514
    :goto_2
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->sortedInstances:Ljava/util/Map;

    iget v7, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 516
    .end local v0    # "data":Lorg/apache/lucene/store/IndexInput;
    .end local v1    # "dataName":Ljava/lang/String;
    .end local v2    # "index":Lorg/apache/lucene/store/IndexInput;
    .end local v3    # "indexName":Ljava/lang/String;
    .end local v5    # "success":Z
    :cond_2
    monitor-exit p0

    return-object v4

    .line 510
    .restart local v0    # "data":Lorg/apache/lucene/store/IndexInput;
    .restart local v1    # "dataName":Ljava/lang/String;
    .restart local v2    # "index":Lorg/apache/lucene/store/IndexInput;
    .restart local v3    # "indexName":Ljava/lang/String;
    .restart local v5    # "success":Z
    :cond_3
    const/4 v7, 0x2

    :try_start_5
    new-array v7, v7, [Ljava/io/Closeable;

    const/4 v8, 0x0

    .line 511
    aput-object v0, v7, v8

    const/4 v8, 0x1

    aput-object v2, v7, v8

    invoke-static {v7}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 510
    :cond_4
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/io/Closeable;

    const/4 v7, 0x0

    .line 511
    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v2, v6, v7

    invoke-static {v6}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 490
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 2
    .param p1, "field"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 620
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Lucene 4.0 does not support SortedSet: how did you pull this off?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
