.class final Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;
.super Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LiveDocsSegmentDocsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 600
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/Bits;)V
    .locals 1
    .param p2, "startFreqIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 602
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    .line 603
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsEnumBase;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/util/Bits;)V

    .line 604
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 605
    :cond_0
    return-void
.end method


# virtual methods
.method protected final linearScan(I)I
    .locals 6
    .param p1, "scanTo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 624
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->docs:[I

    .line 625
    .local v1, "docs":[I
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->count:I

    .line 626
    .local v4, "upTo":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 627
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->start:I

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 635
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->refill()I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->doc:I

    :goto_1
    return v5

    .line 628
    :cond_0
    aget v0, v1, v2

    .line 629
    .local v0, "d":I
    if-gt p1, v0, :cond_1

    invoke-interface {v3, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 630
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->start:I

    .line 631
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freqs:[I

    aget v5, v5, v2

    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freq:I

    .line 632
    aget v5, v1, v2

    iput v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->doc:I

    goto :goto_1

    .line 627
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public final nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 609
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 610
    .local v2, "liveDocs":Lorg/apache/lucene/util/Bits;
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->start:I

    add-int/lit8 v1, v3, 0x1

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->count:I

    if-lt v1, v3, :cond_0

    .line 618
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->count:I

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->start:I

    .line 619
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->refill()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->doc:I

    :goto_1
    return v0

    .line 611
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->docs:[I

    aget v0, v3, v1

    .line 612
    .local v0, "d":I
    invoke-interface {v2, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 613
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->start:I

    .line 614
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freqs:[I

    aget v3, v3, v1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freq:I

    .line 615
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->doc:I

    goto :goto_1

    .line 610
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected final nextUnreadDoc()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 668
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->accum:I

    .line 669
    .local v1, "docAcc":I
    const/4 v3, 0x1

    .line 670
    .local v3, "frq":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 671
    .local v2, "freqIn":Lorg/apache/lucene/store/IndexInput;
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->indexOmitsTF:Z

    .line 672
    .local v7, "omitTF":Z
    iget v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->limit:I

    .line 673
    .local v6, "loopLimit":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 674
    .local v5, "liveDocs":Lorg/apache/lucene/util/Bits;
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->ord:I

    .local v4, "i":I
    :goto_0
    if-lt v4, v6, :cond_0

    .line 688
    iget v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->limit:I

    iput v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->ord:I

    .line 689
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freq:I

    .line 690
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->accum:I

    .line 691
    const v8, 0x7fffffff

    :goto_1
    return v8

    .line 675
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 676
    .local v0, "code":I
    if-eqz v7, :cond_1

    .line 677
    add-int/2addr v1, v0

    .line 682
    :goto_2
    invoke-interface {v5, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 683
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freq:I

    .line 684
    add-int/lit8 v8, v4, 0x1

    iput v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->ord:I

    .line 685
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->accum:I

    move v8, v1

    goto :goto_1

    .line 679
    :cond_1
    ushr-int/lit8 v8, v0, 0x1

    add-int/2addr v1, v8

    .line 680
    invoke-virtual {p0, v2, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->readFreq(Lorg/apache/lucene/store/IndexInput;I)I

    move-result v3

    goto :goto_2

    .line 674
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected scanTo(I)I
    .locals 9
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 640
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->accum:I

    .line 641
    .local v1, "docAcc":I
    const/4 v3, 0x1

    .line 642
    .local v3, "frq":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 643
    .local v2, "freqIn":Lorg/apache/lucene/store/IndexInput;
    iget-boolean v7, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->indexOmitsTF:Z

    .line 644
    .local v7, "omitTF":Z
    iget v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->limit:I

    .line 645
    .local v6, "loopLimit":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 646
    .local v5, "liveDocs":Lorg/apache/lucene/util/Bits;
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->ord:I

    .local v4, "i":I
    :goto_0
    if-lt v4, v6, :cond_0

    .line 660
    iget v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->limit:I

    iput v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->ord:I

    .line 661
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freq:I

    .line 662
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->accum:I

    .line 663
    const v8, 0x7fffffff

    :goto_1
    return v8

    .line 647
    :cond_0
    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 648
    .local v0, "code":I
    if-eqz v7, :cond_1

    .line 649
    add-int/2addr v1, v0

    .line 654
    :goto_2
    if-lt v1, p1, :cond_2

    invoke-interface {v5, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 655
    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->freq:I

    .line 656
    add-int/lit8 v8, v4, 0x1

    iput v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->ord:I

    .line 657
    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->accum:I

    move v8, v1

    goto :goto_1

    .line 651
    :cond_1
    ushr-int/lit8 v8, v0, 0x1

    add-int/2addr v1, v8

    .line 652
    invoke-virtual {p0, v2, v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$LiveDocsSegmentDocsEnum;->readFreq(Lorg/apache/lucene/store/IndexInput;I)I

    move-result v3

    goto :goto_2

    .line 646
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
