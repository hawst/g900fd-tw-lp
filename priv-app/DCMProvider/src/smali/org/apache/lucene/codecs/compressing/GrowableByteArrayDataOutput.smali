.class final Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;
.super Lorg/apache/lucene/store/DataOutput;
.source "GrowableByteArrayDataOutput.java"


# instance fields
.field bytes:[B

.field length:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "cp"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 34
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    .line 36
    return-void
.end method


# virtual methods
.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 41
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    invoke-static {v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    .line 43
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    aput-byte p1, v0, v1

    .line 44
    return-void
.end method

.method public writeBytes([BII)V
    .locals 3
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    add-int v0, v1, p3

    .line 49
    .local v0, "newLength":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 50
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    invoke-static {v1, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    .line 52
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 53
    iput v0, p0, Lorg/apache/lucene/codecs/compressing/GrowableByteArrayDataOutput;->length:I

    .line 54
    return-void
.end method
