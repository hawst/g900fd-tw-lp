.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesFixedSorted(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)Lorg/apache/lucene/index/SortedDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final synthetic val$fixedLength:I

.field private final synthetic val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

.field private final synthetic val$valueCount:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;II)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    iput p4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$fixedLength:I

    iput p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$valueCount:I

    .line 535
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 538
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v0, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getValueCount()I
    .locals 1

    .prologue
    .line 548
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$valueCount:I

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "ord"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 543
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$fixedLength:I

    int-to-long v2, v1

    int-to-long v4, p1

    mul-long/2addr v2, v4

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$13;->val$fixedLength:I

    invoke-virtual {v0, p2, v2, v3, v1}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 544
    return-void
.end method
