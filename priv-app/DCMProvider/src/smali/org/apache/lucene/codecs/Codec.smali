.class public abstract Lorg/apache/lucene/codecs/Codec;
.super Ljava/lang/Object;
.source "Codec.java"

# interfaces
.implements Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;"
    }
.end annotation


# static fields
.field private static defaultCodec:Lorg/apache/lucene/codecs/Codec;

.field private static final loader:Lorg/apache/lucene/util/NamedSPILoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/NamedSPILoader",
            "<",
            "Lorg/apache/lucene/codecs/Codec;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/util/NamedSPILoader;

    const-class v1, Lorg/apache/lucene/codecs/Codec;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/NamedSPILoader;-><init>(Ljava/lang/Class;)V

    .line 41
    sput-object v0, Lorg/apache/lucene/codecs/Codec;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    .line 122
    const-string v0, "Lucene42"

    invoke-static {v0}, Lorg/apache/lucene/codecs/Codec;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/Codec;->defaultCodec:Lorg/apache/lucene/codecs/Codec;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lorg/apache/lucene/util/NamedSPILoader;->checkServiceName(Ljava/lang/String;)V

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/codecs/Codec;->name:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static availableCodecs()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    sget-object v0, Lorg/apache/lucene/codecs/Codec;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You called Codec.availableCodecs() before all Codecs could be initialized. This likely happens if you call it from a Codec\'s ctor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/Codec;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0}, Lorg/apache/lucene/util/NamedSPILoader;->availableServices()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/Codec;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    sget-object v0, Lorg/apache/lucene/codecs/Codec;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You called Codec.forName() before all Codecs could be initialized. This likely happens if you call it from a Codec\'s ctor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/Codec;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/NamedSPILoader;->lookup(Ljava/lang/String;)Lorg/apache/lucene/util/NamedSPILoader$NamedSPI;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/codecs/Codec;

    return-object v0
.end method

.method public static getDefault()Lorg/apache/lucene/codecs/Codec;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lorg/apache/lucene/codecs/Codec;->defaultCodec:Lorg/apache/lucene/codecs/Codec;

    return-object v0
.end method

.method public static reloadCodecs(Ljava/lang/ClassLoader;)V
    .locals 1
    .param p0, "classloader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 119
    sget-object v0, Lorg/apache/lucene/codecs/Codec;->loader:Lorg/apache/lucene/util/NamedSPILoader;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/util/NamedSPILoader;->reload(Ljava/lang/ClassLoader;)V

    .line 120
    return-void
.end method

.method public static setDefault(Lorg/apache/lucene/codecs/Codec;)V
    .locals 0
    .param p0, "codec"    # Lorg/apache/lucene/codecs/Codec;

    .prologue
    .line 136
    sput-object p0, Lorg/apache/lucene/codecs/Codec;->defaultCodec:Lorg/apache/lucene/codecs/Codec;

    .line 137
    return-void
.end method


# virtual methods
.method public abstract docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
.end method

.method public abstract fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lorg/apache/lucene/codecs/Codec;->name:Ljava/lang/String;

    return-object v0
.end method

.method public abstract liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
.end method

.method public abstract normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
.end method

.method public abstract postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
.end method

.method public abstract segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
.end method

.method public abstract storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
.end method

.method public abstract termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lorg/apache/lucene/codecs/Codec;->name:Ljava/lang/String;

    return-object v0
.end method
