.class public Lorg/apache/lucene/codecs/TermStats;
.super Ljava/lang/Object;
.source "TermStats.java"


# instance fields
.field public final docFreq:I

.field public final totalTermFreq:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 0
    .param p1, "docFreq"    # I
    .param p2, "totalTermFreq"    # J

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    .line 40
    iput-wide p2, p0, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    .line 41
    return-void
.end method
