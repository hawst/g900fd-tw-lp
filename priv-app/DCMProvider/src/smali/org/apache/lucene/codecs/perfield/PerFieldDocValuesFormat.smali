.class public abstract Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;
.super Lorg/apache/lucene/codecs/DocValuesFormat;
.source "PerFieldDocValuesFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;,
        Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;,
        Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;
    }
.end annotation


# static fields
.field public static final PER_FIELD_FORMAT_KEY:Ljava/lang/String;

.field public static final PER_FIELD_NAME:Ljava/lang/String; = "PerFieldDV40"

.field public static final PER_FIELD_SUFFIX_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".format"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->PER_FIELD_FORMAT_KEY:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ".suffix"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;->PER_FIELD_SUFFIX_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "PerFieldDV40"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/DocValuesFormat;-><init>(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method static getFullSegmentSuffix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "outerSegmentSuffix"    # Ljava/lang/String;
    .param p2, "segmentSuffix"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 182
    return-object p2

    .line 187
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot embed PerFieldPostingsFormat inside itself (field \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" returned PerFieldPostingsFormat)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static getSuffix(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "formatName"    # Ljava/lang/String;
    .param p1, "suffix"    # Ljava/lang/String;

    .prologue
    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    new-instance v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsWriter;-><init>(Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;Lorg/apache/lucene/index/SegmentWriteState;)V

    return-object v0
.end method

.method public final fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 282
    new-instance v0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$FieldsReader;-><init>(Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;Lorg/apache/lucene/index/SegmentReadState;)V

    return-object v0
.end method

.method public abstract getDocValuesFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;
.end method
