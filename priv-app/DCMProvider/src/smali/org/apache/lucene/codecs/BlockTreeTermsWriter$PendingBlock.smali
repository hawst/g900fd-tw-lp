.class final Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
.super Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
.source "BlockTreeTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PendingBlock"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final floorLeadByte:I

.field public final fp:J

.field public final hasTerms:Z

.field public index:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field public final isFloor:Z

.field public final prefix:Lorg/apache/lucene/util/BytesRef;

.field private final scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

.field public subIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 371
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/BytesRef;JZZILjava/util/List;)V
    .locals 2
    .param p1, "prefix"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "fp"    # J
    .param p4, "hasTerms"    # Z
    .param p5, "isFloor"    # Z
    .param p6, "floorLeadByte"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/BytesRef;",
            "JZZI",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 382
    .local p7, "subIndices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;-><init>(Z)V

    .line 379
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

    .line 383
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    .line 384
    iput-wide p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    .line 385
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->hasTerms:Z

    .line 386
    iput-boolean p5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->isFloor:Z

    .line 387
    iput p6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->floorLeadByte:I

    .line 388
    iput-object p7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    .line 389
    return-void
.end method

.method private append(Lorg/apache/lucene/util/fst/Builder;Lorg/apache/lucene/util/fst/FST;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/Builder",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 468
    .local p1, "builder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/BytesRef;>;"
    .local p2, "subIndex":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-direct {v1, p2}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    .line 470
    .local v1, "subIndexEnum":Lorg/apache/lucene/util/fst/BytesRefFSTEnum;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum<Lorg/apache/lucene/util/BytesRef;>;"
    :goto_0
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->next()Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v0

    .local v0, "indexEnt":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Lorg/apache/lucene/util/BytesRef;>;"
    if-nez v0, :cond_0

    .line 476
    return-void

    .line 474
    :cond_0
    iget-object v2, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

    invoke-static {v2, v3}, Lorg/apache/lucene/util/fst/Util;->toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v3

    iget-object v2, v0, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v2, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v3, v2}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public compileIndex(Ljava/util/List;Lorg/apache/lucene/store/RAMOutputStream;)V
    .locals 18
    .param p2, "scratchBytes"    # Lorg/apache/lucene/store/RAMOutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;",
            ">;",
            "Lorg/apache/lucene/store/RAMOutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 398
    .local p1, "floorBlocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;>;"
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->isFloor:Z

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->isFloor:Z

    if-nez v3, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isFloor="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->isFloor:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " floorBlocks="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 400
    :cond_2
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 405
    :cond_3
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->hasTerms:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->isFloor:Z

    invoke-static {v4, v5, v3, v6}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->encodeOutput(JZZ)J

    move-result-wide v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 406
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->isFloor:Z

    if-eqz v3, :cond_4

    .line 407
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 408
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 419
    :cond_4
    invoke-static {}, Lorg/apache/lucene/util/fst/ByteSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/ByteSequenceOutputs;

    move-result-object v9

    .line 420
    .local v9, "outputs":Lorg/apache/lucene/util/fst/ByteSequenceOutputs;
    new-instance v2, Lorg/apache/lucene/util/fst/Builder;

    sget-object v3, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 421
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const v8, 0x7fffffff

    .line 422
    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 423
    const/4 v12, 0x0

    const/4 v13, 0x1

    const/16 v14, 0xf

    .line 420
    invoke-direct/range {v2 .. v14}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;IIZZILorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Builder$FreezeTail;ZFZI)V

    .line 428
    .local v2, "indexBuilder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v4

    long-to-int v3, v4

    new-array v15, v3, [B

    .line 429
    .local v15, "bytes":[B
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    array-length v3, v15

    if-gtz v3, :cond_9

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 408
    .end local v2    # "indexBuilder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v9    # "outputs":Lorg/apache/lucene/util/fst/ByteSequenceOutputs;
    .end local v15    # "bytes":[B
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    .line 409
    .local v16, "sub":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    move-object/from16 v0, v16

    iget v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->floorLeadByte:I

    const/4 v5, -0x1

    if-ne v3, v5, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 413
    :cond_6
    move-object/from16 v0, v16

    iget v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->floorLeadByte:I

    int-to-byte v3, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/apache/lucene/store/RAMOutputStream;->writeByte(B)V

    .line 414
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->$assertionsDisabled:Z

    if-nez v3, :cond_7

    move-object/from16 v0, v16

    iget-wide v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    cmp-long v3, v6, v10

    if-gtz v3, :cond_7

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 415
    :cond_7
    move-object/from16 v0, v16

    iget-wide v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    sub-long/2addr v6, v10

    const/4 v3, 0x1

    shl-long/2addr v6, v3

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->hasTerms:Z

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_1
    int-to-long v10, v3

    or-long/2addr v6, v10

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_1

    .line 430
    .end local v16    # "sub":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    .restart local v2    # "indexBuilder":Lorg/apache/lucene/util/fst/Builder;, "Lorg/apache/lucene/util/fst/Builder<Lorg/apache/lucene/util/BytesRef;>;"
    .restart local v9    # "outputs":Lorg/apache/lucene/util/fst/ByteSequenceOutputs;
    .restart local v15    # "bytes":[B
    :cond_9
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v15, v3}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo([BI)V

    .line 431
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

    invoke-static {v3, v4}, Lorg/apache/lucene/util/fst/Util;->toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v3

    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    const/4 v5, 0x0

    array-length v6, v15

    invoke-direct {v4, v15, v5, v6}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 432
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 436
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    if-eqz v3, :cond_a

    .line 437
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_c

    .line 442
    :cond_a
    if-eqz p1, :cond_b

    .line 443
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_d

    .line 453
    :cond_b
    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->index:Lorg/apache/lucene/util/fst/FST;

    .line 454
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    .line 462
    return-void

    .line 437
    :cond_c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/util/fst/FST;

    .line 438
    .local v17, "subIndex":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->append(Lorg/apache/lucene/util/fst/Builder;Lorg/apache/lucene/util/fst/FST;)V

    goto :goto_2

    .line 443
    .end local v17    # "subIndex":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_d
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    .line 444
    .restart local v16    # "sub":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    move-object/from16 v0, v16

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    if-eqz v4, :cond_e

    .line 445
    move-object/from16 v0, v16

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_f

    .line 449
    :cond_e
    const/4 v4, 0x0

    move-object/from16 v0, v16

    iput-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->subIndices:Ljava/util/List;

    goto :goto_3

    .line 445
    :cond_f
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/util/fst/FST;

    .line 446
    .restart local v17    # "subIndex":Lorg/apache/lucene/util/fst/FST;, "Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->append(Lorg/apache/lucene/util/fst/Builder;Lorg/apache/lucene/util/fst/FST;)V

    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 393
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BLOCK: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
