.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat$SimpleTextNormsProducer;
.super Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;
.source "SimpleTextNormsFormat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextNormsFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SimpleTextNormsProducer"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentReadState;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    const-string v0, "len"

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;)V

    .line 60
    return-void
.end method


# virtual methods
.method public bridge synthetic close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->close()V

    return-void
.end method

.method public bridge synthetic getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    return-object v0
.end method
