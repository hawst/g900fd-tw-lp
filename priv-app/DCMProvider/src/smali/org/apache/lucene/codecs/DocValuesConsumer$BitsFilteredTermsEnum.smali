.class Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "DocValuesConsumer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BitsFilteredTermsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final liveTerms:Lorg/apache/lucene/util/OpenBitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 608
    const-class v0, Lorg/apache/lucene/codecs/DocValuesConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/OpenBitSet;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "liveTerms"    # Lorg/apache/lucene/util/OpenBitSet;

    .prologue
    .line 612
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Z)V

    .line 613
    sget-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 614
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;->liveTerms:Lorg/apache/lucene/util/OpenBitSet;

    .line 615
    return-void
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 619
    iget-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;->liveTerms:Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$BitsFilteredTermsEnum;->ord()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/OpenBitSet;->get(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 622
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method
