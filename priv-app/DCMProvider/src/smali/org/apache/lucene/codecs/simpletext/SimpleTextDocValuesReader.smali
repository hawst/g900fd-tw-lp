.class Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;
.super Lorg/apache/lucene/codecs/DocValuesProducer;
.source "SimpleTextDocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final data:Lorg/apache/lucene/store/IndexInput;

.field final fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;",
            ">;"
        }
    .end annotation
.end field

.field final maxDoc:I

.field final scratch:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;)V
    .locals 12
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .param p2, "ext"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesProducer;-><init>()V

    .line 68
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 69
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    .line 73
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v5, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    iget-object v6, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentSuffix:Ljava/lang/String;

    invoke-static {v5, v6, p2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lorg/apache/lucene/index/SegmentReadState;->context:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    .line 74
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    iput v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    .line 76
    :goto_0
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 78
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 135
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_11

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 81
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 82
    :cond_1
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "fieldName":Ljava/lang/String;
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentReadState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 85
    .local v2, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    if-nez v2, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 87
    :cond_2
    new-instance v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    invoke-direct {v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;-><init>()V

    .line 88
    .local v1, "field":Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 91
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 93
    :cond_3
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v0

    .line 94
    .local v0, "dvType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    if-nez v0, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 95
    :cond_4
    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v4, :cond_7

    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 97
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MINVALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " field="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ext="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 98
    :cond_5
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MINVALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->minValue:J

    .line 99
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 100
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 101
    :cond_6
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    .line 102
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iput-wide v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    .line 103
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    iget-object v5, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    iget v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    mul-int/2addr v5, v8

    int-to-long v8, v5

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto/16 :goto_0

    .line 104
    :cond_7
    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v4, :cond_a

    .line 105
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 106
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_8

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_8

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 107
    :cond_8
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    .line 108
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 109
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_9

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 110
    :cond_9
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    .line 111
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iput-wide v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    .line 112
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    iget-object v5, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    iget v8, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    add-int/2addr v5, v8

    iget v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    mul-int/2addr v5, v8

    int-to-long v8, v5

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto/16 :goto_0

    .line 113
    :cond_a
    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eq v0, v4, :cond_b

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-ne v0, v4, :cond_10

    .line 114
    :cond_b
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 115
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->NUMVALUES:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_c

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 116
    :cond_c
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->NUMVALUES:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->numValues:J

    .line 117
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 118
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_d

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 119
    :cond_d
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->MAXLENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    .line 120
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 121
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_e

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_e

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 122
    :cond_e
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->PATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    .line 123
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->readLine()V

    .line 124
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_f

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->ORDPATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->startsWith(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_f

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 125
    :cond_f
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->ORDPATTERN:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->ordPattern:Ljava/lang/String;

    .line 126
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    iput-wide v4, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    .line 127
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v6

    iget-object v5, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    iget v8, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    add-int/2addr v5, v8

    int-to-long v8, v5

    iget-wide v10, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->numValues:J

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    iget-object v5, v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->ordPattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    iget v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    mul-int/2addr v5, v8

    int-to-long v8, v5

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto/16 :goto_0

    .line 129
    :cond_10
    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 136
    .end local v0    # "dvType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .end local v1    # "field":Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    .end local v2    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v3    # "fieldName":Ljava/lang/String;
    :cond_11
    return-void
.end method

.method private readLine()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 373
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 375
    return-void
.end method

.method private startsWith(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 1
    .param p1, "prefix"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 379
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, p1}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    return v0
.end method

.method private stripPrefix(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 5
    .param p1, "prefix"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 384
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, v4

    const-string v4, "UTF-8"

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 368
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 369
    return-void
.end method

.method public getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
    .locals 7
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    .line 186
    .local v3, "field":Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 189
    .local v2, "in":Lorg/apache/lucene/store/IndexInput;
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 190
    .local v4, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v5, Ljava/text/DecimalFormat;

    iget-object v0, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    new-instance v1, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v5, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 192
    .local v5, "decoder":Ljava/text/DecimalFormat;
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;)V

    return-object v0
.end method

.method public getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
    .locals 7
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    .line 141
    .local v3, "field":Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 145
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "field="

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " fields="

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 147
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 148
    .local v2, "in":Lorg/apache/lucene/store/IndexInput;
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 149
    .local v4, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v5, Ljava/text/DecimalFormat;

    iget-object v0, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    new-instance v1, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v5, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 151
    .local v5, "decoder":Ljava/text/DecimalFormat;
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Ljava/text/DecimalFormat;->setParseBigDecimal(Z)V

    .line 153
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;)V

    return-object v0
.end method

.method public getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
    .locals 8
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    .line 227
    .local v3, "field":Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 229
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 230
    .local v2, "in":Lorg/apache/lucene/store/IndexInput;
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 231
    .local v4, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v6, Ljava/text/DecimalFormat;

    iget-object v0, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    new-instance v1, Ljava/text/DecimalFormatSymbols;

    sget-object v7, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v6, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 232
    .local v6, "decoder":Ljava/text/DecimalFormat;
    new-instance v5, Ljava/text/DecimalFormat;

    iget-object v0, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->ordPattern:Ljava/lang/String;

    new-instance v1, Ljava/text/DecimalFormatSymbols;

    sget-object v7, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v7}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v5, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 234
    .local v5, "ordDecoder":Ljava/text/DecimalFormat;
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$3;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;Ljava/text/DecimalFormat;)V

    return-object v0
.end method

.method public getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 7
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->fields:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    .line 294
    .local v3, "field":Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 296
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->data:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 297
    .local v2, "in":Lorg/apache/lucene/store/IndexInput;
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 298
    .local v4, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v5, Ljava/text/DecimalFormat;

    iget-object v0, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    new-instance v1, Ljava/text/DecimalFormatSymbols;

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v6}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v5, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 300
    .local v5, "decoder":Ljava/text/DecimalFormat;
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;-><init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;)V

    return-object v0
.end method
