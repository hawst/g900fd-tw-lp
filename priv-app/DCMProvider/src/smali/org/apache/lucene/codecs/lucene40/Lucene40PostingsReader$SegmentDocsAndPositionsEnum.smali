.class final Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "Lucene40PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SegmentDocsAndPositionsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field accum:I

.field doc:I

.field freq:I

.field private final freqIn:Lorg/apache/lucene/store/IndexInput;

.field freqOffset:J

.field private lazyProxPointer:J

.field limit:I

.field liveDocs:Lorg/apache/lucene/util/Bits;

.field ord:I

.field posPendingCount:I

.field position:I

.field private final proxIn:Lorg/apache/lucene/store/IndexInput;

.field proxOffset:J

.field skipOffset:J

.field skipped:Z

.field skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

.field final startFreqIn:Lorg/apache/lucene/store/IndexInput;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 699
    const-class v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V
    .locals 1
    .param p2, "freqIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "proxIn"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 722
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 705
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    .line 723
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->startFreqIn:Lorg/apache/lucene/store/IndexInput;

    .line 724
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    .line 725
    invoke-virtual {p3}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    .line 726
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 11
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 807
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v1, v1, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    sub-int v1, p1, v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->limit:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v2, v2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipMinimum:I

    if-lt v1, v2, :cond_2

    .line 812
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    if-nez v1, :cond_0

    .line 814
    new-instance v1, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v3, v3, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->maxSkipLevels:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;

    iget v4, v4, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader;->skipInterval:I

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    .line 817
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipped:Z

    if-nez v1, :cond_1

    .line 823
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqOffset:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipOffset:J

    add-long/2addr v2, v4

    .line 824
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqOffset:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->proxOffset:J

    .line 825
    iget v8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->limit:I

    move v10, v9

    .line 823
    invoke-virtual/range {v1 .. v10}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->init(JJJIZZ)V

    .line 827
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipped:Z

    .line 830
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->skipTo(I)I

    move-result v0

    .line 832
    .local v0, "newOrd":I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->ord:I

    if-le v0, v1, :cond_2

    .line 834
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->ord:I

    .line 835
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->accum:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    .line 836
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getFreqPointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 837
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipper:Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->getProxPointer()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->lazyProxPointer:J

    .line 838
    iput v9, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    .line 839
    iput v9, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    .line 845
    .end local v0    # "newOrd":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->nextDoc()I

    .line 846
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    .line 844
    if-gt p1, v1, :cond_2

    .line 848
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    return v1
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 897
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->limit:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 794
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 1

    .prologue
    .line 885
    const/4 v0, -0x1

    return v0
.end method

.method public freq()I
    .locals 1

    .prologue
    .line 799
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freq:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 892
    const/4 v0, 0x0

    return-object v0
.end method

.method public nextDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 763
    :cond_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->ord:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->limit:I

    if-ne v1, v2, :cond_1

    .line 765
    const v1, 0x7fffffff

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    .line 789
    :goto_0
    return v1

    .line 768
    :cond_1
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->ord:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->ord:I

    .line 771
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 773
    .local v0, "code":I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->accum:I

    ushr-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->accum:I

    .line 774
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_3

    .line 775
    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freq:I

    .line 779
    :goto_1
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freq:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    .line 781
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->accum:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 786
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    .line 789
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->accum:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    goto :goto_0

    .line 777
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freq:I

    goto :goto_1
.end method

.method public nextPosition()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 854
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->lazyProxPointer:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->lazyProxPointer:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 856
    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->lazyProxPointer:J

    .line 860
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freq:I

    if-le v0, v1, :cond_2

    .line 861
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    .line 862
    :cond_1
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freq:I

    if-ne v0, v1, :cond_3

    .line 869
    :cond_2
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    .line 871
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    .line 873
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    if-gez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "nextPosition() was called too many times (more than freq() times) posPendingCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 863
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->proxIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_1

    .line 864
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    goto :goto_0

    .line 875
    :cond_4
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    return v0
.end method

.method public reset(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;
    .locals 5
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 729
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 730
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 732
    :cond_1
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 737
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqIn:Lorg/apache/lucene/store/IndexInput;

    iget-wide v2, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 738
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->lazyProxPointer:J

    .line 740
    iget v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->docFreq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->limit:I

    .line 741
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->limit:I

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 743
    :cond_2
    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->ord:I

    .line 744
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->doc:I

    .line 745
    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->accum:I

    .line 746
    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->position:I

    .line 748
    iput-boolean v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipped:Z

    .line 749
    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->posPendingCount:I

    .line 751
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->freqOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->freqOffset:J

    .line 752
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->proxOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->proxOffset:J

    .line 753
    iget-wide v0, p2, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$StandardTermState;->skipOffset:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40PostingsReader$SegmentDocsAndPositionsEnum;->skipOffset:J

    .line 756
    return-object p0
.end method

.method public startOffset()I
    .locals 1

    .prologue
    .line 880
    const/4 v0, -0x1

    return v0
.end method
