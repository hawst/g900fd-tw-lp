.class Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;
.super Lorg/apache/lucene/index/SortedDocValues;
.source "Lucene42DocValuesProducer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;->getSorted(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

.field private final synthetic val$docToOrd:Lorg/apache/lucene/index/NumericDocValues;

.field private final synthetic val$entry:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

.field private final synthetic val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

.field private final synthetic val$fst:Lorg/apache/lucene/util/fst/FST;

.field private final synthetic val$fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

.field private final synthetic val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private final synthetic val$scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

.field private final synthetic val$scratchInts:Lorg/apache/lucene/util/IntsRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;Lorg/apache/lucene/index/NumericDocValues;Lorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/fst/BytesRefFSTEnum;Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->this$0:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$docToOrd:Lorg/apache/lucene/index/NumericDocValues;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    iput-object p4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$fst:Lorg/apache/lucene/util/fst/FST;

    iput-object p5, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object p6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object p7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$scratchInts:Lorg/apache/lucene/util/IntsRef;

    iput-object p8, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    iput-object p9, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$entry:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    .line 252
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public getOrd(I)I
    .locals 2
    .param p1, "docID"    # I

    .prologue
    .line 255
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$docToOrd:Lorg/apache/lucene/index/NumericDocValues;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getValueCount()I
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$entry:Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTEntry;->numOrds:J

    long-to-int v0, v0

    return v0
.end method

.method public lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
    .locals 9
    .param p1, "ord"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 261
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/fst/FST$BytesReader;->setPosition(J)V

    .line 262
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$fst:Lorg/apache/lucene/util/fst/FST;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    .line 263
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$fst:Lorg/apache/lucene/util/fst/FST;

    int-to-long v2, p1

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$in:Lorg/apache/lucene/util/fst/FST$BytesReader;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$firstArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$scratchArc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$scratchInts:Lorg/apache/lucene/util/IntsRef;

    invoke-static/range {v1 .. v7}, Lorg/apache/lucene/util/fst/Util;->getByOutput(Lorg/apache/lucene/util/fst/FST;JLorg/apache/lucene/util/fst/FST$BytesReader;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v8

    .line 264
    .local v8, "output":Lorg/apache/lucene/util/IntsRef;
    iget v1, v8, Lorg/apache/lucene/util/IntsRef;->length:I

    new-array v1, v1, [B

    iput-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 265
    const/4 v1, 0x0

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 266
    const/4 v1, 0x0

    iput v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 267
    invoke-static {v8, p2}, Lorg/apache/lucene/util/fst/Util;->toBytesRef(Lorg/apache/lucene/util/IntsRef;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    return-void

    .line 268
    .end local v8    # "output":Lorg/apache/lucene/util/IntsRef;
    :catch_0
    move-exception v0

    .line 269
    .local v0, "bogus":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public lookupTerm(Lorg/apache/lucene/util/BytesRef;)I
    .locals 4
    .param p1, "key"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 276
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$fstEnum:Lorg/apache/lucene/util/fst/BytesRefFSTEnum;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/fst/BytesRefFSTEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;

    move-result-object v1

    .line 277
    .local v1, "o":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Ljava/lang/Long;>;"
    if-nez v1, :cond_0

    .line 278
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->getValueCount()I

    move-result v2

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 282
    :goto_0
    return v2

    .line 279
    :cond_0
    iget-object v2, v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->input:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 280
    iget-object v2, v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->intValue()I

    move-result v2

    goto :goto_0

    .line 282
    :cond_1
    iget-object v2, v1, Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;->output:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    neg-long v2, v2

    long-to-int v2, v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 284
    .end local v1    # "o":Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput;, "Lorg/apache/lucene/util/fst/BytesRefFSTEnum$InputOutput<Ljava/lang/Long;>;"
    :catch_0
    move-exception v0

    .line 285
    .local v0, "bogus":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public termsEnum()Lorg/apache/lucene/index/TermsEnum;
    .locals 2

    .prologue
    .line 296
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$6;->val$fst:Lorg/apache/lucene/util/fst/FST;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer$FSTTermsEnum;-><init>(Lorg/apache/lucene/util/fst/FST;)V

    return-object v0
.end method
