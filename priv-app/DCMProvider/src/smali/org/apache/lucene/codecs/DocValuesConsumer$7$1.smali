.class Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;
.super Ljava/lang/Object;
.source "DocValuesConsumer.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/DocValuesConsumer$7;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field currentLiveDocs:Lorg/apache/lucene/util/Bits;

.field currentReader:Lorg/apache/lucene/index/AtomicReader;

.field docIDUpto:I

.field nextIsSet:Z

.field nextValue:J

.field ordLength:I

.field ordUpto:I

.field ords:[J

.field readerUpto:I

.field final synthetic this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$7;

.field private final synthetic val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

.field private final synthetic val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

.field private final synthetic val$readers:[Lorg/apache/lucene/index/AtomicReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/DocValuesConsumer$7;[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/SortedSetDocValues;Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->this$1:Lorg/apache/lucene/codecs/DocValuesConsumer$7;

    iput-object p2, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

    iput-object p4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    .line 526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 527
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    .line 533
    const/16 v0, 0x8

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ords:[J

    return-void
.end method

.method private setNext()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 560
    :goto_0
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    array-length v6, v6

    if-ne v5, v6, :cond_0

    .line 568
    :goto_1
    return v1

    .line 564
    :cond_0
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordUpto:I

    iget v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    if-ge v5, v6, :cond_1

    .line 565
    iget-object v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ords:[J

    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordUpto:I

    aget-wide v6, v1, v5

    iput-wide v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->nextValue:J

    .line 566
    iget v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordUpto:I

    .line 567
    iput-boolean v4, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->nextIsSet:Z

    move v1, v4

    .line 568
    goto :goto_1

    .line 571
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    if-eqz v5, :cond_2

    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v6

    if-ne v5, v6, :cond_4

    .line 572
    :cond_2
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    .line 573
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    array-length v6, v6

    if-ge v5, v6, :cond_3

    .line 574
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$readers:[Lorg/apache/lucene/index/AtomicReader;

    iget v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    aget-object v5, v5, v6

    iput-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    .line 575
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    .line 577
    :cond_3
    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    goto :goto_0

    .line 581
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentLiveDocs:Lorg/apache/lucene/util/Bits;

    iget v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    invoke-interface {v5, v6}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 582
    :cond_5
    sget-boolean v5, Lorg/apache/lucene/codecs/DocValuesConsumer;->$assertionsDisabled:Z

    if-nez v5, :cond_6

    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->currentReader:Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v6

    if-lt v5, v6, :cond_6

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 583
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$dvs:[Lorg/apache/lucene/index/SortedSetDocValues;

    iget v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    aget-object v0, v5, v6

    .line 584
    .local v0, "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    invoke-virtual {v0, v5}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 585
    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    iput v1, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordUpto:I

    .line 587
    :goto_2
    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v2

    .local v2, "ord":J
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-nez v5, :cond_7

    .line 594
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    goto/16 :goto_0

    .line 588
    :cond_7
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ords:[J

    array-length v6, v6

    if-ne v5, v6, :cond_8

    .line 589
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ords:[J

    iget v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    add-int/lit8 v6, v6, 0x1

    invoke-static {v5, v6}, Lorg/apache/lucene/util/ArrayUtil;->grow([JI)[J

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ords:[J

    .line 591
    :cond_8
    iget-object v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ords:[J

    iget v6, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    iget-object v7, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->val$map:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iget v8, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->readerUpto:I

    invoke-virtual {v7, v8, v2, v3}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getGlobalOrd(IJ)J

    move-result-wide v8

    aput-wide v8, v5, v6

    .line 592
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->ordLength:I

    goto :goto_2

    .line 598
    .end local v0    # "dv":Lorg/apache/lucene/index/SortedSetDocValues;
    .end local v2    # "ord":J
    :cond_9
    iget v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->docIDUpto:I

    goto/16 :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    .prologue
    .line 539
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->nextIsSet:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->setNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 549
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 550
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 552
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/DocValuesConsumer;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->nextIsSet:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 553
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->nextIsSet:Z

    .line 555
    iget-wide v0, p0, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->nextValue:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/DocValuesConsumer$7$1;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 544
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
