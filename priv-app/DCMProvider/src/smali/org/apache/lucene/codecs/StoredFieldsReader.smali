.class public abstract Lorg/apache/lucene/codecs/StoredFieldsReader;
.super Ljava/lang/Object;
.source "StoredFieldsReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;->clone()Lorg/apache/lucene/codecs/StoredFieldsReader;

    move-result-object v0

    return-object v0
.end method

.method public abstract clone()Lorg/apache/lucene/codecs/StoredFieldsReader;
.end method

.method public abstract visitDocument(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
