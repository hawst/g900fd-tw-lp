.class public Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;
.super Lorg/apache/lucene/codecs/MultiLevelSkipListReader;
.source "Lucene40SkipListReader.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private currentFieldStoresOffsets:Z

.field private currentFieldStoresPayloads:Z

.field private freqPointer:[J

.field private lastFreqPointer:J

.field private lastOffsetLength:I

.field private lastPayloadLength:I

.field private lastProxPointer:J

.field private offsetLength:[I

.field private payloadLength:[I

.field private proxPointer:[J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;II)V
    .locals 1
    .param p1, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "maxSkipLevels"    # I
    .param p3, "skipInterval"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    .line 50
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->freqPointer:[J

    .line 51
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->proxPointer:[J

    .line 52
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->payloadLength:[I

    .line 53
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->offsetLength:[I

    .line 54
    return-void
.end method


# virtual methods
.method public getFreqPointer()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastFreqPointer:J

    return-wide v0
.end method

.method public getOffsetLength()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastOffsetLength:I

    return v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastPayloadLength:I

    return v0
.end method

.method public getProxPointer()J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastProxPointer:J

    return-wide v0
.end method

.method public init(JJJIZZ)V
    .locals 3
    .param p1, "skipPointer"    # J
    .param p3, "freqBasePointer"    # J
    .param p5, "proxBasePointer"    # J
    .param p7, "df"    # I
    .param p8, "storesPayloads"    # Z
    .param p9, "storesOffsets"    # Z

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-super {p0, p1, p2, p7}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->init(JI)V

    .line 59
    iput-boolean p8, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->currentFieldStoresPayloads:Z

    .line 60
    iput-boolean p9, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->currentFieldStoresOffsets:Z

    .line 61
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastFreqPointer:J

    .line 62
    iput-wide p5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastProxPointer:J

    .line 64
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->freqPointer:[J

    invoke-static {v0, p3, p4}, Ljava/util/Arrays;->fill([JJ)V

    .line 65
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->proxPointer:[J

    invoke-static {v0, p5, p6}, Ljava/util/Arrays;->fill([JJ)V

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->payloadLength:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 67
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->offsetLength:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 68
    return-void
.end method

.method protected readSkipData(ILorg/apache/lucene/store/IndexInput;)I
    .locals 6
    .param p1, "level"    # I
    .param p2, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->currentFieldStoresPayloads:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->currentFieldStoresOffsets:Z

    if-eqz v1, :cond_3

    .line 124
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 125
    .local v0, "delta":I
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_2

    .line 126
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->currentFieldStoresPayloads:Z

    if-eqz v1, :cond_1

    .line 127
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->payloadLength:[I

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, v1, p1

    .line 129
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->currentFieldStoresOffsets:Z

    if-eqz v1, :cond_2

    .line 130
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->offsetLength:[I

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, v1, p1

    .line 133
    :cond_2
    ushr-int/lit8 v0, v0, 0x1

    .line 138
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->freqPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 139
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->proxPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 141
    return v0

    .line 135
    .end local v0    # "delta":I
    :cond_3
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .restart local v0    # "delta":I
    goto :goto_0
.end method

.method protected seekChild(I)V
    .locals 4
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->seekChild(I)V

    .line 99
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->freqPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastFreqPointer:J

    aput-wide v2, v0, p1

    .line 100
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->proxPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastProxPointer:J

    aput-wide v2, v0, p1

    .line 101
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->payloadLength:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastPayloadLength:I

    aput v1, v0, p1

    .line 102
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->offsetLength:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastOffsetLength:I

    aput v1, v0, p1

    .line 103
    return-void
.end method

.method protected setLastSkipData(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 107
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->setLastSkipData(I)V

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->freqPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastFreqPointer:J

    .line 109
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->proxPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastProxPointer:J

    .line 110
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->payloadLength:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastPayloadLength:I

    .line 111
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->offsetLength:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40SkipListReader;->lastOffsetLength:I

    .line 112
    return-void
.end method
