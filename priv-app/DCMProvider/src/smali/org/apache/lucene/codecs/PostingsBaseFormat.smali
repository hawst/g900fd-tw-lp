.class public abstract Lorg/apache/lucene/codecs/PostingsBaseFormat;
.super Ljava/lang/Object;
.source "PostingsBaseFormat.java"


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/codecs/PostingsBaseFormat;->name:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public abstract postingsReaderBase(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/PostingsReaderBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract postingsWriterBase(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/PostingsWriterBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
