.class public abstract Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;
.super Ljava/lang/Object;
.source "MultiLevelSkipListWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected numberOfSkipLevels:I

.field private skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

.field private skipInterval:I

.field private skipMultiplier:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(III)V
    .locals 0
    .param p1, "skipInterval"    # I
    .param p2, "maxSkipLevels"    # I
    .param p3, "df"    # I

    .prologue
    .line 89
    invoke-direct {p0, p1, p1, p2, p3}, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;-><init>(IIII)V

    .line 90
    return-void
.end method

.method protected constructor <init>(IIII)V
    .locals 2
    .param p1, "skipInterval"    # I
    .param p2, "skipMultiplier"    # I
    .param p3, "maxSkipLevels"    # I
    .param p4, "df"    # I

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipInterval:I

    .line 70
    iput p2, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipMultiplier:I

    .line 73
    if-gt p4, p1, :cond_1

    .line 74
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    .line 80
    :goto_0
    iget v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    if-le v0, p3, :cond_0

    .line 81
    iput p3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    .line 83
    :cond_0
    return-void

    .line 76
    :cond_1
    div-int v0, p4, p1

    int-to-long v0, v0

    invoke-static {v0, v1, p2}, Lorg/apache/lucene/util/MathUtil;->log(JI)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    goto :goto_0
.end method


# virtual methods
.method public bufferSkip(I)V
    .locals 7
    .param p1, "df"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    sget-boolean v6, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipInterval:I

    rem-int v6, p1, v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 129
    :cond_0
    const/4 v3, 0x1

    .line 130
    .local v3, "numLevels":I
    iget v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipInterval:I

    div-int/2addr p1, v6

    .line 133
    :goto_0
    iget v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipMultiplier:I

    rem-int v6, p1, v6

    if-nez v6, :cond_1

    iget v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    if-lt v3, v6, :cond_2

    .line 138
    :cond_1
    const-wide/16 v0, 0x0

    .line 140
    .local v0, "childPointer":J
    const/4 v2, 0x0

    .local v2, "level":I
    :goto_1
    if-lt v2, v3, :cond_3

    .line 153
    return-void

    .line 134
    .end local v0    # "childPointer":J
    .end local v2    # "level":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 135
    iget v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipMultiplier:I

    div-int/2addr p1, v6

    goto :goto_0

    .line 141
    .restart local v0    # "childPointer":J
    .restart local v2    # "level":I
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v6, v6, v2

    invoke-virtual {p0, v2, v6}, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->writeSkipData(ILorg/apache/lucene/store/IndexOutput;)V

    .line 143
    iget-object v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v6, v6, v2

    invoke-virtual {v6}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v4

    .line 145
    .local v4, "newChildPointer":J
    if-eqz v2, :cond_4

    .line 147
    iget-object v6, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v6, v6, v2

    invoke-virtual {v6, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 151
    :cond_4
    move-wide v0, v4

    .line 140
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method protected init()V
    .locals 3

    .prologue
    .line 94
    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    new-array v1, v1, [Lorg/apache/lucene/store/RAMOutputStream;

    iput-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    .line 95
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    if-lt v0, v1, :cond_0

    .line 98
    return-void

    .line 96
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    new-instance v2, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v2}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    aput-object v2, v1, v0

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected resetSkip()V
    .locals 2

    .prologue
    .line 102
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    if-nez v1, :cond_1

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->init()V

    .line 109
    :cond_0
    return-void

    .line 105
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 106
    iget-object v1, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeSkip(Lorg/apache/lucene/store/IndexOutput;)J
    .locals 8
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 164
    .local v4, "skipPointer":J
    iget-object v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    array-length v3, v3

    if-nez v3, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-wide v4

    .line 166
    :cond_1
    iget v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->numberOfSkipLevels:I

    add-int/lit8 v2, v3, -0x1

    .local v2, "level":I
    :goto_1
    if-gtz v2, :cond_2

    .line 173
    iget-object v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    const/4 v6, 0x0

    aget-object v3, v3, v6

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    goto :goto_0

    .line 167
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v0

    .line 168
    .local v0, "length":J
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_3

    .line 169
    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVLong(J)V

    .line 170
    iget-object v3, p0, Lorg/apache/lucene/codecs/MultiLevelSkipListWriter;->skipBuffer:[Lorg/apache/lucene/store/RAMOutputStream;

    aget-object v3, v3, v2

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 166
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method protected abstract writeSkipData(ILorg/apache/lucene/store/IndexOutput;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
