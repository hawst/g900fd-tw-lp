.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "CompressingTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TVDocsEnum"
.end annotation


# instance fields
.field private basePayloadOffset:I

.field private doc:I

.field private i:I

.field private lengths:[I

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private final payload:Lorg/apache/lucene/util/BytesRef;

.field private payloadIndex:[I

.field private positionIndex:I

.field private positions:[I

.field private startOffsets:[I

.field private termFreq:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 921
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 910
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    .line 922
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    .line 923
    return-void
.end method

.method private checkDoc()V
    .locals 2

    .prologue
    .line 943
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 944
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DocsEnum exhausted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 945
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 946
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DocsEnum not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 948
    :cond_1
    return-void
.end method

.method private checkPosition()V
    .locals 2

    .prologue
    .line 951
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->checkDoc()V

    .line 952
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    if-gez v0, :cond_0

    .line 953
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Position enum not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 954
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->termFreq:I

    if-lt v0, v1, :cond_1

    .line 955
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Read past last position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 957
    :cond_1
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1033
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 1038
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 1019
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    return v0
.end method

.method public endOffset()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 993
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->checkPosition()V

    .line 994
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->startOffsets:[I

    if-nez v0, :cond_0

    .line 995
    const/4 v0, -0x1

    .line 997
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->startOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v1, v2

    aget v0, v0, v1

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->lengths:[I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v2, v3

    aget v1, v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1013
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->checkDoc()V

    .line 1014
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->termFreq:I

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1003
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->checkPosition()V

    .line 1004
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payloadIndex:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v0, :cond_1

    .line 1005
    :cond_0
    const/4 v0, 0x0

    .line 1007
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public nextDoc()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1024
    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1025
    :cond_0
    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    .line 1027
    :goto_0
    return v0

    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    goto :goto_0
.end method

.method public nextPosition()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 961
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    if-eqz v0, :cond_0

    .line 962
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 963
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->termFreq:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    .line 964
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Read past last position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 967
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    .line 969
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payloadIndex:[I

    if-eqz v0, :cond_2

    .line 970
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->basePayloadOffset:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payloadIndex:[I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v3, v4

    aget v2, v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 971
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payloadIndex:[I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payloadIndex:[I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v3, v4

    aget v2, v2, v3

    sub-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 974
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positions:[I

    if-nez v0, :cond_3

    .line 975
    const/4 v0, -0x1

    .line 977
    :goto_0
    return v0

    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positions:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v1, v2

    aget v0, v0, v1

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;II[I[I[ILorg/apache/lucene/util/BytesRef;[I)V
    .locals 3
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "freq"    # I
    .param p3, "positionIndex"    # I
    .param p4, "positions"    # [I
    .param p5, "startOffsets"    # [I
    .param p6, "lengths"    # [I
    .param p7, "payloads"    # Lorg/apache/lucene/util/BytesRef;
    .param p8, "payloadIndex"    # [I

    .prologue
    .line 928
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 929
    iput p2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->termFreq:I

    .line 930
    iput p3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    .line 931
    iput-object p4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positions:[I

    .line 932
    iput-object p5, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->startOffsets:[I

    .line 933
    iput-object p6, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->lengths:[I

    .line 934
    iget v0, p7, Lorg/apache/lucene/util/BytesRef;->offset:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->basePayloadOffset:I

    .line 935
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iput-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 936
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payload:Lorg/apache/lucene/util/BytesRef;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    iput v2, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 937
    iput-object p8, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->payloadIndex:[I

    .line 939
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->doc:I

    .line 940
    return-void
.end method

.method public startOffset()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 983
    invoke-direct {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->checkPosition()V

    .line 984
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->startOffsets:[I

    if-nez v0, :cond_0

    .line 985
    const/4 v0, -0x1

    .line 987
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->startOffsets:[I

    iget v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->positionIndex:I

    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVDocsEnum;->i:I

    add-int/2addr v1, v2

    aget v0, v0, v1

    goto :goto_0
.end method
