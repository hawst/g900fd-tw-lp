.class public abstract Lorg/apache/lucene/codecs/PostingsConsumer;
.super Ljava/lang/Object;
.source "PostingsConsumer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/codecs/PostingsConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/PostingsConsumer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method


# virtual methods
.method public abstract addPosition(ILorg/apache/lucene/util/BytesRef;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract finishDoc()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;Lorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/DocsEnum;Lorg/apache/lucene/util/FixedBitSet;)Lorg/apache/lucene/codecs/TermStats;
    .locals 14
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .param p2, "indexOptions"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .param p3, "postings"    # Lorg/apache/lucene/index/DocsEnum;
    .param p4, "visitedDocs"    # Lorg/apache/lucene/util/FixedBitSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    const/4 v2, 0x0

    .line 79
    .local v2, "df":I
    const-wide/16 v10, 0x0

    .line 81
    .local v10, "totTF":J
    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v9, :cond_3

    .line 83
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v3

    .line 84
    .local v3, "doc":I
    const v9, 0x7fffffff

    if-ne v3, v9, :cond_2

    .line 92
    const-wide/16 v10, -0x1

    .line 146
    :cond_0
    new-instance v9, Lorg/apache/lucene/codecs/TermStats;

    sget-object v12, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v12, :cond_1

    const-wide/16 v10, -0x1

    .end local v10    # "totTF":J
    :cond_1
    invoke-direct {v9, v2, v10, v11}, Lorg/apache/lucene/codecs/TermStats;-><init>(IJ)V

    return-object v9

    .line 87
    .restart local v10    # "totTF":J
    :cond_2
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 88
    const/4 v9, -0x1

    invoke-virtual {p0, v3, v9}, Lorg/apache/lucene/codecs/PostingsConsumer;->startDoc(II)V

    .line 89
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/PostingsConsumer;->finishDoc()V

    .line 90
    add-int/lit8 v2, v2, 0x1

    .line 82
    goto :goto_0

    .line 93
    .end local v3    # "doc":I
    :cond_3
    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v9, :cond_4

    .line 95
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v3

    .line 96
    .restart local v3    # "doc":I
    const v9, 0x7fffffff

    if-eq v3, v9, :cond_0

    .line 99
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 100
    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v4

    .line 101
    .local v4, "freq":I
    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/codecs/PostingsConsumer;->startDoc(II)V

    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/PostingsConsumer;->finishDoc()V

    .line 103
    add-int/lit8 v2, v2, 0x1

    .line 104
    int-to-long v12, v4

    add-long/2addr v10, v12

    .line 94
    goto :goto_1

    .line 106
    .end local v3    # "doc":I
    .end local v4    # "freq":I
    :cond_4
    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-ne v0, v9, :cond_6

    move-object/from16 v8, p3

    .line 107
    check-cast v8, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 109
    .local v8, "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :goto_2
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v3

    .line 110
    .restart local v3    # "doc":I
    const v9, 0x7fffffff

    if-eq v3, v9, :cond_0

    .line 113
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 114
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v4

    .line 115
    .restart local v4    # "freq":I
    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/codecs/PostingsConsumer;->startDoc(II)V

    .line 116
    int-to-long v12, v4

    add-long/2addr v10, v12

    .line 117
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    if-lt v5, v4, :cond_5

    .line 122
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/PostingsConsumer;->finishDoc()V

    .line 123
    add-int/lit8 v2, v2, 0x1

    .line 108
    goto :goto_2

    .line 118
    :cond_5
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v7

    .line 119
    .local v7, "position":I
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    .line 120
    .local v6, "payload":Lorg/apache/lucene/util/BytesRef;
    const/4 v9, -0x1

    const/4 v12, -0x1

    invoke-virtual {p0, v7, v6, v9, v12}, Lorg/apache/lucene/codecs/PostingsConsumer;->addPosition(ILorg/apache/lucene/util/BytesRef;II)V

    .line 117
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 126
    .end local v3    # "doc":I
    .end local v4    # "freq":I
    .end local v5    # "i":I
    .end local v6    # "payload":Lorg/apache/lucene/util/BytesRef;
    .end local v7    # "position":I
    .end local v8    # "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :cond_6
    sget-boolean v9, Lorg/apache/lucene/codecs/PostingsConsumer;->$assertionsDisabled:Z

    if-nez v9, :cond_7

    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, p2

    if-eq v0, v9, :cond_7

    new-instance v9, Ljava/lang/AssertionError;

    invoke-direct {v9}, Ljava/lang/AssertionError;-><init>()V

    throw v9

    :cond_7
    move-object/from16 v8, p3

    .line 127
    check-cast v8, Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 129
    .restart local v8    # "postingsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :goto_4
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v3

    .line 130
    .restart local v3    # "doc":I
    const v9, 0x7fffffff

    if-eq v3, v9, :cond_0

    .line 133
    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 134
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v4

    .line 135
    .restart local v4    # "freq":I
    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/codecs/PostingsConsumer;->startDoc(II)V

    .line 136
    int-to-long v12, v4

    add-long/2addr v10, v12

    .line 137
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_5
    if-lt v5, v4, :cond_8

    .line 142
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/PostingsConsumer;->finishDoc()V

    .line 143
    add-int/lit8 v2, v2, 0x1

    .line 128
    goto :goto_4

    .line 138
    :cond_8
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v7

    .line 139
    .restart local v7    # "position":I
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    .line 140
    .restart local v6    # "payload":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v9

    invoke-virtual {v8}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v12

    invoke-virtual {p0, v7, v6, v9, v12}, Lorg/apache/lucene/codecs/PostingsConsumer;->addPosition(ILorg/apache/lucene/util/BytesRef;II)V

    .line 137
    add-int/lit8 v5, v5, 0x1

    goto :goto_5
.end method

.method public abstract startDoc(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
