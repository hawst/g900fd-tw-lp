.class Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;
.super Ljava/lang/Object;
.source "PerFieldDocValuesFormat.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ConsumerAndSuffix"
.end annotation


# instance fields
.field consumer:Lorg/apache/lucene/codecs/DocValuesConsumer;

.field suffix:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/codecs/perfield/PerFieldDocValuesFormat$ConsumerAndSuffix;->consumer:Lorg/apache/lucene/codecs/DocValuesConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/DocValuesConsumer;->close()V

    .line 89
    return-void
.end method
