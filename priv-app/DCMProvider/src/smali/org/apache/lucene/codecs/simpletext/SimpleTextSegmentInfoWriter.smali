.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;
.super Lorg/apache/lucene/codecs/SegmentInfoWriter;
.source "SimpleTextSegmentInfoWriter.java"


# static fields
.field static final SI_ATT_KEY:Lorg/apache/lucene/util/BytesRef;

.field static final SI_ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

.field static final SI_DIAG_KEY:Lorg/apache/lucene/util/BytesRef;

.field static final SI_DIAG_VALUE:Lorg/apache/lucene/util/BytesRef;

.field static final SI_DOCCOUNT:Lorg/apache/lucene/util/BytesRef;

.field static final SI_FILE:Lorg/apache/lucene/util/BytesRef;

.field static final SI_NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

.field static final SI_NUM_DIAG:Lorg/apache/lucene/util/BytesRef;

.field static final SI_NUM_FILES:Lorg/apache/lucene/util/BytesRef;

.field static final SI_USECOMPOUND:Lorg/apache/lucene/util/BytesRef;

.field static final SI_VERSION:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    version "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_VERSION:Lorg/apache/lucene/util/BytesRef;

    .line 43
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    number of documents "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DOCCOUNT:Lorg/apache/lucene/util/BytesRef;

    .line 44
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    uses compound file "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_USECOMPOUND:Lorg/apache/lucene/util/BytesRef;

    .line 45
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    diagnostics "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_DIAG:Lorg/apache/lucene/util/BytesRef;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      key "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_KEY:Lorg/apache/lucene/util/BytesRef;

    .line 47
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      value "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_VALUE:Lorg/apache/lucene/util/BytesRef;

    .line 48
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    attributes "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      key "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      value "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    files "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_FILES:Lorg/apache/lucene/util/BytesRef;

    .line 52
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "      file "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_FILE:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/apache/lucene/codecs/SegmentInfoWriter;-><init>()V

    return-void
.end method


# virtual methods
.method public write(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 18
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "ioContext"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    move-object/from16 v0, p2

    iget-object v15, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v16, ""

    const-string v17, "si"

    invoke-static/range {v15 .. v17}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 58
    .local v13, "segFileName":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lorg/apache/lucene/index/SegmentInfo;->addFile(Ljava/lang/String;)V

    .line 60
    const/4 v14, 0x0

    .line 61
    .local v14, "success":Z
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v13, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v11

    .line 64
    .local v11, "output":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    new-instance v12, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v12}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 66
    .local v12, "scratch":Lorg/apache/lucene/util/BytesRef;
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_VERSION:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 67
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 68
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 70
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DOCCOUNT:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 71
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 72
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 74
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_USECOMPOUND:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 75
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 76
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 78
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDiagnostics()Ljava/util/Map;

    move-result-object v4

    .line 79
    .local v4, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v4, :cond_3

    const/4 v9, 0x0

    .line 80
    .local v9, "numDiagnostics":I
    :goto_0
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_DIAG:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 81
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 82
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 84
    if-lez v9, :cond_0

    .line 85
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_4

    .line 96
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->attributes()Ljava/util/Map;

    move-result-object v2

    .line 97
    .local v2, "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v2, :cond_5

    const/4 v8, 0x0

    .line 98
    .local v8, "numAtts":I
    :goto_2
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_ATTS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 99
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 100
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 102
    if-lez v8, :cond_1

    .line 103
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_6

    .line 114
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v7

    .line 115
    .local v7, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez v7, :cond_7

    const/4 v10, 0x0

    .line 116
    .local v10, "numFiles":I
    :goto_4
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_NUM_FILES:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 117
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 118
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 120
    if-lez v10, :cond_2

    .line 121
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v16

    if-nez v16, :cond_8

    .line 127
    :cond_2
    const/4 v14, 0x1

    .line 129
    if-nez v14, :cond_a

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/io/Closeable;

    const/16 v16, 0x0

    .line 130
    aput-object v11, v15, v16

    invoke-static {v15}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 132
    :try_start_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :goto_6
    return-void

    .line 79
    .end local v2    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8    # "numAtts":I
    .end local v9    # "numDiagnostics":I
    .end local v10    # "numFiles":I
    :cond_3
    :try_start_2
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v9

    goto :goto_0

    .line 85
    .restart local v9    # "numDiagnostics":I
    :cond_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 86
    .local v3, "diagEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_KEY:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 87
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 88
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 90
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_DIAG_VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 91
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 92
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 128
    .end local v3    # "diagEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v9    # "numDiagnostics":I
    .end local v12    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :catchall_0
    move-exception v15

    .line 129
    if-nez v14, :cond_9

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    .line 130
    aput-object v11, v16, v17

    invoke-static/range {v16 .. v16}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 132
    :try_start_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 138
    :goto_7
    throw v15

    .line 97
    .restart local v2    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v9    # "numDiagnostics":I
    .restart local v12    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :cond_5
    :try_start_4
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v8

    goto/16 :goto_2

    .line 103
    .restart local v8    # "numAtts":I
    :cond_6
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 104
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_KEY:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 105
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 106
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 108
    sget-object v15, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_ATT_VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v11, v15}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 109
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v11, v15, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 110
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    goto/16 :goto_3

    .line 115
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_7
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v10

    goto/16 :goto_4

    .line 121
    .restart local v10    # "numFiles":I
    :cond_8
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 122
    .local v6, "fileName":Ljava/lang/String;
    sget-object v16, Lorg/apache/lucene/codecs/simpletext/SimpleTextSegmentInfoWriter;->SI_FILE:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 123
    invoke-static {v11, v6, v12}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 124
    invoke-static {v11}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_5

    .line 136
    .end local v2    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "fileName":Ljava/lang/String;
    .end local v7    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8    # "numAtts":I
    .end local v9    # "numDiagnostics":I
    .end local v10    # "numFiles":I
    .end local v12    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :cond_9
    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexOutput;->close()V

    goto :goto_7

    .restart local v2    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v8    # "numAtts":I
    .restart local v9    # "numDiagnostics":I
    .restart local v10    # "numFiles":I
    .restart local v12    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :cond_a
    invoke-virtual {v11}, Lorg/apache/lucene/store/IndexOutput;->close()V

    goto/16 :goto_6

    .line 133
    .end local v2    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v8    # "numAtts":I
    .end local v9    # "numDiagnostics":I
    .end local v10    # "numFiles":I
    .end local v12    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :catch_0
    move-exception v16

    goto :goto_7

    .restart local v2    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v7    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v8    # "numAtts":I
    .restart local v9    # "numDiagnostics":I
    .restart local v10    # "numFiles":I
    .restart local v12    # "scratch":Lorg/apache/lucene/util/BytesRef;
    :catch_1
    move-exception v15

    goto/16 :goto_6
.end method
