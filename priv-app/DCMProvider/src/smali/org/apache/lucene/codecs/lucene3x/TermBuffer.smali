.class final Lorg/apache/lucene/codecs/lucene3x/TermBuffer;
.super Ljava/lang/Object;
.source "TermBuffer.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final utf8AsUTF16Comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bytes:Lorg/apache/lucene/util/BytesRef;

.field private currentFieldNumber:I

.field private field:Ljava/lang/String;

.field newSuffixStart:I

.field private term:Lorg/apache/lucene/index/Term;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->$assertionsDisabled:Z

    .line 44
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->utf8AsUTF16Comparator:Ljava/util/Comparator;

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 42
    const/4 v0, -0x2

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    .line 33
    return-void
.end method


# virtual methods
.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->clone()Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    move-result-object v0

    return-object v0
.end method

.method protected clone()Lorg/apache/lucene/codecs/lucene3x/TermBuffer;
    .locals 3

    .prologue
    .line 121
    const/4 v1, 0x0

    .line 123
    .local v1, "clone":Lorg/apache/lucene/codecs/lucene3x/TermBuffer;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 126
    return-object v1

    .line 124
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public compareTo(Lorg/apache/lucene/codecs/lucene3x/TermBuffer;)I
    .locals 3
    .param p1, "other"    # Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    .line 51
    sget-object v0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->utf8AsUTF16Comparator:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, p1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 53
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public read(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 6
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 59
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->newSuffixStart:I

    .line 60
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 61
    .local v1, "length":I
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->newSuffixStart:I

    add-int v2, v3, v1

    .line 62
    .local v2, "totalLength":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v3, v3

    if-ge v3, v2, :cond_0

    .line 63
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 65
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    iput v2, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 66
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->newSuffixStart:I

    invoke-virtual {p1, v3, v4, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 67
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 68
    .local v0, "fieldNumber":I
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    if-eq v0, v3, :cond_4

    .line 69
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    .line 71
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 72
    const-string v3, ""

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    .line 80
    :cond_1
    :goto_0
    return-void

    .line 74
    :cond_2
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    invoke-virtual {p2, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v3

    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(I)V

    throw v3

    .line 75
    :cond_3
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    invoke-virtual {p2, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    goto :goto_0

    .line 78
    :cond_4
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "currentFieldNumber="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " field="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " vs "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    const-string v3, "null"

    :goto_1
    invoke-direct {v4, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    :cond_5
    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    goto :goto_1
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 103
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    .line 104
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    .line 106
    return-void
.end method

.method public set(Lorg/apache/lucene/codecs/lucene3x/TermBuffer;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/codecs/lucene3x/TermBuffer;

    .prologue
    .line 94
    iget-object v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    .line 95
    iget v0, p1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 99
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 100
    return-void
.end method

.method public set(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 83
    if-nez p1, :cond_0

    .line 84
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->reset()V

    .line 91
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 88
    invoke-virtual {p1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->currentFieldNumber:I

    .line 90
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    goto :goto_0
.end method

.method public toTerm()Lorg/apache/lucene/index/Term;
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 116
    :goto_0
    return-object v0

    .line 112
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    if-nez v0, :cond_1

    .line 113
    new-instance v0, Lorg/apache/lucene/index/Term;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->field:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    .line 116
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/TermBuffer;->term:Lorg/apache/lucene/index/Term;

    goto :goto_0
.end method
