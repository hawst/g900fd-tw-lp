.class Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;
.super Ljava/lang/Object;
.source "SegmentTermDocs.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected count:I

.field protected currentFieldStoresPayloads:Z

.field protected df:I

.field doc:I

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field freq:I

.field private freqBasePointer:J

.field protected freqStream:Lorg/apache/lucene/store/IndexInput;

.field private haveSkipped:Z

.field protected indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field protected liveDocs:Lorg/apache/lucene/util/Bits;

.field private maxSkipLevels:I

.field private proxBasePointer:J

.field private skipInterval:I

.field private skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

.field private skipPointer:J

.field private final tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "freqStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "tis"    # Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 57
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    .line 58
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    .line 59
    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 60
    invoke-virtual {p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->getSkipInterval()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipInterval:I

    .line 61
    invoke-virtual {p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->getMaxSkipLevels()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->maxSkipLevels:I

    .line 62
    return-void
.end method

.method private final readNoTf([I[II)I
    .locals 3
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    const/4 v0, 0x0

    .line 179
    .local v0, "i":I
    :cond_0
    :goto_0
    if-ge v0, p3, :cond_1

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    if-lt v1, v2, :cond_2

    .line 192
    :cond_1
    return v0

    .line 181
    :cond_2
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 182
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    .line 184
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    invoke-interface {v1, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    :cond_3
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    aput v1, p1, v0

    .line 188
    const/4 v1, 0x1

    aput v1, p2, v0

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 109
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->close()V

    .line 111
    :cond_0
    return-void
.end method

.method public final doc()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    return v0
.end method

.method public final freq()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    return v0
.end method

.method public next()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 123
    :goto_0
    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    if-ne v2, v3, :cond_1

    .line 124
    const/4 v1, 0x0

    .line 146
    :cond_0
    return v1

    .line 125
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 127
    .local v0, "docCode":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v2, v3, :cond_3

    .line 128
    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 139
    :cond_2
    :goto_1
    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    .line 141
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    invoke-interface {v2, v3}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 144
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skippingDoc()V

    goto :goto_0

    .line 130
    :cond_3
    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    ushr-int/lit8 v3, v0, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 131
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_4

    .line 132
    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    goto :goto_1

    .line 134
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    .line 135
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    if-ne v2, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method public read([I[I)I
    .locals 5
    .param p1, "docs"    # [I
    .param p2, "freqs"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    array-length v2, p1

    .line 153
    .local v2, "length":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v4, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v3, v4, :cond_1

    .line 154
    invoke-direct {p0, p1, p2, v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->readNoTf([I[II)I

    move-result v1

    .line 173
    :cond_0
    return v1

    .line 156
    :cond_1
    const/4 v1, 0x0

    .line 157
    .local v1, "i":I
    :cond_2
    :goto_0
    if-ge v1, v2, :cond_0

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    if-ge v3, v4, :cond_0

    .line 159
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 160
    .local v0, "docCode":I
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    ushr-int/lit8 v4, v0, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 161
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_4

    .line 162
    const/4 v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    .line 165
    :goto_1
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    .line 167
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    invoke-interface {v3, v4}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 168
    :cond_3
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    aput v3, p1, v1

    .line 169
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    aput v3, p2, v1

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freq:I

    goto :goto_1
.end method

.method public seek(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;)V
    .locals 4
    .param p1, "segmentTermEnum"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v2, p1, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    if-ne v2, v3, :cond_0

    .line 79
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 80
    .local v0, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->termInfo()Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v1

    .line 86
    .local v1, "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :goto_0
    invoke-virtual {p0, v1, v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->seek(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/index/Term;)V

    .line 87
    return-void

    .line 82
    .end local v0    # "term":Lorg/apache/lucene/index/Term;
    .end local v1    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    :cond_0
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 83
    .restart local v0    # "term":Lorg/apache/lucene/index/Term;
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    invoke-virtual {v2, v0}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v1

    .restart local v1    # "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    goto :goto_0
.end method

.method seek(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/index/Term;)V
    .locals 8
    .param p1, "ti"    # Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    .param p2, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 90
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    .line 91
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 92
    .local v0, "fi":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 93
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v1

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->currentFieldStoresPayloads:Z

    .line 94
    if-nez p1, :cond_2

    .line 95
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    .line 105
    :goto_2
    return-void

    .line 92
    :cond_0
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    goto :goto_0

    :cond_1
    move v1, v2

    .line 93
    goto :goto_1

    .line 97
    :cond_2
    iget v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->docFreq:I

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    .line 98
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 99
    iget-wide v4, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->freqPointer:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqBasePointer:J

    .line 100
    iget-wide v4, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->proxPointer:J

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->proxBasePointer:J

    .line 101
    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqBasePointer:J

    iget v1, p1, Lorg/apache/lucene/codecs/lucene3x/TermInfo;->skipOffset:I

    int-to-long v6, v1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipPointer:J

    .line 102
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqBasePointer:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 103
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->haveSkipped:Z

    goto :goto_2
.end method

.method public seek(Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->tis:Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->get(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    move-result-object v0

    .line 66
    .local v0, "ti":Lorg/apache/lucene/codecs/lucene3x/TermInfo;
    invoke-virtual {p0, v0, p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->seek(Lorg/apache/lucene/codecs/lucene3x/TermInfo;Lorg/apache/lucene/index/Term;)V

    .line 67
    return-void
.end method

.method public setLiveDocs(Lorg/apache/lucene/util/Bits;)V
    .locals 0
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;

    .prologue
    .line 70
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 71
    return-void
.end method

.method protected skipProx(JI)V
    .locals 0
    .param p1, "proxPointer"    # J
    .param p3, "payloadLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 197
    return-void
.end method

.method public skipTo(I)Z
    .locals 11
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 202
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipInterval:I

    sub-int v1, p1, v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipInterval:I

    if-lt v1, v2, :cond_2

    .line 203
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    if-nez v1, :cond_0

    .line 204
    new-instance v1, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->maxSkipLevels:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipInterval:I

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    .line 206
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->haveSkipped:Z

    if-nez v1, :cond_1

    .line 207
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipPointer:J

    iget-wide v4, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqBasePointer:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->proxBasePointer:J

    iget v8, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->df:I

    iget-boolean v9, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->currentFieldStoresPayloads:Z

    invoke-virtual/range {v1 .. v9}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->init(JJJIZ)V

    .line 208
    iput-boolean v10, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->haveSkipped:Z

    .line 211
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->skipTo(I)I

    move-result v0

    .line 212
    .local v0, "newCount":I
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    if-le v0, v1, :cond_2

    .line 213
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->freqStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->getFreqPointer()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 214
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->getProxPointer()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->getPayloadLength()I

    move-result v1

    invoke-virtual {p0, v2, v3, v1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipProx(JI)V

    .line 216
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->skipListReader:Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->getDoc()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 217
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->count:I

    .line 223
    .end local v0    # "newCount":I
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->next()Z

    move-result v1

    if-nez v1, :cond_3

    .line 224
    const/4 v1, 0x0

    .line 226
    :goto_0
    return v1

    .line 225
    :cond_3
    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/SegmentTermDocs;->doc:I

    .line 222
    if-gt p1, v1, :cond_2

    move v1, v10

    .line 226
    goto :goto_0
.end method

.method protected skippingDoc()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    return-void
.end method
