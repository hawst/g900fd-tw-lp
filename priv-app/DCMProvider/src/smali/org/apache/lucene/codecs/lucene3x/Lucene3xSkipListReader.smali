.class final Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;
.super Lorg/apache/lucene/codecs/MultiLevelSkipListReader;
.source "Lucene3xSkipListReader.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private currentFieldStoresPayloads:Z

.field private freqPointer:[J

.field private lastFreqPointer:J

.field private lastPayloadLength:I

.field private lastProxPointer:J

.field private payloadLength:[I

.field private proxPointer:[J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;II)V
    .locals 1
    .param p1, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "maxSkipLevels"    # I
    .param p3, "skipInterval"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;-><init>(Lorg/apache/lucene/store/IndexInput;II)V

    .line 43
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->freqPointer:[J

    .line 44
    new-array v0, p2, [J

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->proxPointer:[J

    .line 45
    new-array v0, p2, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->payloadLength:[I

    .line 46
    return-void
.end method


# virtual methods
.method public getFreqPointer()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastFreqPointer:J

    return-wide v0
.end method

.method public getPayloadLength()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastPayloadLength:I

    return v0
.end method

.method public getProxPointer()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastProxPointer:J

    return-wide v0
.end method

.method public init(JJJIZ)V
    .locals 3
    .param p1, "skipPointer"    # J
    .param p3, "freqBasePointer"    # J
    .param p5, "proxBasePointer"    # J
    .param p7, "df"    # I
    .param p8, "storesPayloads"    # Z

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p7}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->init(JI)V

    .line 50
    iput-boolean p8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->currentFieldStoresPayloads:Z

    .line 51
    iput-wide p3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastFreqPointer:J

    .line 52
    iput-wide p5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastProxPointer:J

    .line 54
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->freqPointer:[J

    invoke-static {v0, p3, p4}, Ljava/util/Arrays;->fill([JJ)V

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->proxPointer:[J

    invoke-static {v0, p5, p6}, Ljava/util/Arrays;->fill([JJ)V

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->payloadLength:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 57
    return-void
.end method

.method protected readSkipData(ILorg/apache/lucene/store/IndexInput;)I
    .locals 6
    .param p1, "level"    # I
    .param p2, "skipStream"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->currentFieldStoresPayloads:Z

    if-eqz v1, :cond_1

    .line 103
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 104
    .local v0, "delta":I
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->payloadLength:[I

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, v1, p1

    .line 107
    :cond_0
    ushr-int/lit8 v0, v0, 0x1

    .line 112
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->freqPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 113
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->proxPointer:[J

    aget-wide v2, v1, p1

    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    aput-wide v2, v1, p1

    .line 115
    return v0

    .line 109
    .end local v0    # "delta":I
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .restart local v0    # "delta":I
    goto :goto_0
.end method

.method protected seekChild(I)V
    .locals 4
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->seekChild(I)V

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->freqPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastFreqPointer:J

    aput-wide v2, v0, p1

    .line 82
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->proxPointer:[J

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastProxPointer:J

    aput-wide v2, v0, p1

    .line 83
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->payloadLength:[I

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastPayloadLength:I

    aput v1, v0, p1

    .line 84
    return-void
.end method

.method protected setLastSkipData(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 88
    invoke-super {p0, p1}, Lorg/apache/lucene/codecs/MultiLevelSkipListReader;->setLastSkipData(I)V

    .line 89
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->freqPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastFreqPointer:J

    .line 90
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->proxPointer:[J

    aget-wide v0, v0, p1

    iput-wide v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastProxPointer:J

    .line 91
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->payloadLength:[I

    aget v0, v0, p1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSkipListReader;->lastPayloadLength:I

    .line 92
    return-void
.end method
