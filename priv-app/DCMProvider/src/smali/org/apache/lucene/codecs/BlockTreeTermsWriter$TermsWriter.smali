.class Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;
.super Lorg/apache/lucene/codecs/TermsConsumer;
.source "BlockTreeTermsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TermsWriter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final blockBuilder:Lorg/apache/lucene/util/fst/Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Builder",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

.field private final bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

.field docCount:I

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field indexStartFP:J

.field private lastBlockIndex:I

.field private final noOutputs:Lorg/apache/lucene/util/fst/NoOutputs;

.field private numTerms:J

.field private final pending:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

.field private subBytes:[I

.field private subSubCounts:[I

.field private subTermCountSums:[I

.field private subTermCounts:[I

.field sumDocFreq:J

.field sumTotalTermFreq:J

.field final synthetic this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 481
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 13
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/16 v1, 0xa

    .line 954
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/TermsConsumer;-><init>()V

    .line 495
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    .line 498
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    .line 502
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    .line 503
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    .line 504
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    .line 505
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    .line 992
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/IntsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

    .line 1049
    new-instance v0, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    .line 1050
    new-instance v0, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    .line 955
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 957
    invoke-static {}, Lorg/apache/lucene/util/fst/NoOutputs;->getSingleton()Lorg/apache/lucene/util/fst/NoOutputs;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->noOutputs:Lorg/apache/lucene/util/fst/NoOutputs;

    .line 962
    new-instance v0, Lorg/apache/lucene/util/fst/Builder;

    sget-object v1, Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;->BYTE1:Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;

    .line 964
    const v6, 0x7fffffff

    .line 965
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->noOutputs:Lorg/apache/lucene/util/fst/NoOutputs;

    .line 966
    new-instance v8, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;

    const/4 v3, 0x0

    invoke-direct {v8, p0, v3}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter$FindBlocks;)V

    .line 967
    const/4 v10, 0x0

    .line 968
    const/16 v12, 0xf

    move v3, v2

    move v5, v4

    move v9, v2

    move v11, v4

    invoke-direct/range {v0 .. v12}, Lorg/apache/lucene/util/fst/Builder;-><init>(Lorg/apache/lucene/util/fst/FST$INPUT_TYPE;IIZZILorg/apache/lucene/util/fst/Outputs;Lorg/apache/lucene/util/fst/Builder$FreezeTail;ZFZI)V

    .line 962
    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->blockBuilder:Lorg/apache/lucene/util/fst/Builder;

    .line 970
    iget-object v0, p1, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/PostingsWriterBase;->setField(Lorg/apache/lucene/index/FieldInfo;)V

    .line 971
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;)Lorg/apache/lucene/util/fst/Builder;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->blockBuilder:Lorg/apache/lucene/util/fst/Builder;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;)Lorg/apache/lucene/codecs/BlockTreeTermsWriter;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    return-object v0
.end method

.method private toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 3
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 777
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 782
    :goto_0
    return-object v1

    .line 778
    :catch_0
    move-exception v0

    .line 782
    .local v0, "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private writeBlock(Lorg/apache/lucene/util/IntsRef;IIIIIZIZ)Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    .locals 24
    .param p1, "prevTerm"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "prefixLength"    # I
    .param p3, "indexPrefixLength"    # I
    .param p4, "startBackwards"    # I
    .param p5, "length"    # I
    .param p6, "futureTermCount"    # I
    .param p7, "isFloor"    # Z
    .param p8, "floorLeadByte"    # I
    .param p9, "isLastInFloor"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 791
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gtz p5, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 793
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int v15, v2, p4

    .line 795
    .local v15, "start":I
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    if-gez v15, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "pending.size()="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " startBackwards="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " length="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 797
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    add-int v6, v15, p5

    invoke-interface {v2, v15, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v14

    .line 799
    .local v14, "slice":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v4

    .line 801
    .local v4, "startFP":J
    new-instance v3, Lorg/apache/lucene/util/BytesRef;

    move/from16 v0, p3

    invoke-direct {v3, v0}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 802
    .local v3, "prefix":Lorg/apache/lucene/util/BytesRef;
    const/4 v13, 0x0

    .local v13, "m":I
    :goto_0
    move/from16 v0, p3

    if-lt v13, v0, :cond_5

    .line 805
    move/from16 v0, p3

    iput v0, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 808
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    shl-int/lit8 v7, p5, 0x1

    if-eqz p9, :cond_6

    const/4 v2, 0x1

    :goto_1
    or-int/2addr v2, v7

    invoke-virtual {v6, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 818
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    if-ge v2, v15, :cond_7

    .line 820
    const/4 v12, 0x1

    .line 842
    .local v12, "isLeafBlock":Z
    :goto_2
    if-eqz v12, :cond_e

    .line 843
    const/4 v9, 0x0

    .line 844
    .local v9, "subIndices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;>;"
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_b

    .line 865
    move/from16 v18, p5

    .line 924
    .local v18, "termCount":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v20

    const/4 v2, 0x1

    shl-long v20, v20, v2

    move-wide/from16 v0, v20

    long-to-int v7, v0

    if-eqz v12, :cond_15

    const/4 v2, 0x1

    :goto_4
    or-int/2addr v2, v7

    invoke-virtual {v6, v2}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 925
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 926
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 929
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v6}, Lorg/apache/lucene/store/RAMOutputStream;->getFilePointer()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 930
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    invoke-virtual {v2, v6}, Lorg/apache/lucene/store/RAMOutputStream;->writeTo(Lorg/apache/lucene/store/IndexOutput;)V

    .line 931
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMOutputStream;->reset()V

    .line 934
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    add-int v6, p6, v18

    move/from16 v0, v18

    invoke-virtual {v2, v6, v0}, Lorg/apache/lucene/codecs/PostingsWriterBase;->flushTermsBlock(II)V

    .line 937
    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 939
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    if-lt v2, v15, :cond_4

    .line 940
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    add-int v6, v15, p5

    if-ge v2, v6, :cond_16

    .line 941
    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    .line 951
    :cond_4
    :goto_5
    new-instance v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    if-eqz v18, :cond_17

    const/4 v6, 0x1

    :goto_6
    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;-><init>(Lorg/apache/lucene/util/BytesRef;JZZILjava/util/List;)V

    return-object v2

    .line 803
    .end local v9    # "subIndices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;>;"
    .end local v12    # "isLeafBlock":Z
    .end local v18    # "termCount":I
    :cond_5
    iget-object v2, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    aget v6, v6, v13

    int-to-byte v6, v6

    aput-byte v6, v2, v13

    .line 802
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 808
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 822
    :cond_7
    if-nez p7, :cond_8

    .line 824
    const/4 v12, 0x0

    .line 826
    .restart local v12    # "isLeafBlock":Z
    goto/16 :goto_2

    .line 828
    .end local v12    # "isLeafBlock":Z
    :cond_8
    const/16 v19, 0x1

    .line 830
    .local v19, "v":Z
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_a

    .line 836
    :goto_7
    move/from16 v12, v19

    .restart local v12    # "isLeafBlock":Z
    goto/16 :goto_2

    .line 830
    .end local v12    # "isLeafBlock":Z
    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;

    .line 831
    .local v11, "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    iget-boolean v6, v11, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;->isTerm:Z

    if-nez v6, :cond_9

    .line 832
    const/16 v19, 0x0

    .line 833
    goto :goto_7

    .line 844
    .end local v11    # "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    .end local v19    # "v":Z
    .restart local v9    # "subIndices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;>;"
    .restart local v12    # "isLeafBlock":Z
    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;

    .line 845
    .restart local v11    # "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    sget-boolean v6, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_c

    iget-boolean v6, v11, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;->isTerm:Z

    if-nez v6, :cond_c

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_c
    move-object/from16 v17, v11

    .line 846
    check-cast v17, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;

    .line 847
    .local v17, "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    move-object/from16 v0, v17

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v16, v6, p2

    .line 855
    .local v16, "suffix":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 856
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v6, v7, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeBytes([BII)V

    .line 859
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v7, v7, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 860
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v6, v7, :cond_2

    .line 861
    sget-boolean v6, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_d

    move-object/from16 v0, v17

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget-wide v6, v6, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    move-object/from16 v0, v17

    iget-object v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v8, v8, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v0, v8

    move-wide/from16 v20, v0

    cmp-long v6, v6, v20

    if-gez v6, :cond_d

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget-wide v0, v7, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " vs "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v7, v7, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 862
    :cond_d
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget-wide v0, v7, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v7, v7, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v0, v7

    move-wide/from16 v22, v0

    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    goto/16 :goto_3

    .line 867
    .end local v9    # "subIndices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;>;"
    .end local v11    # "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    .end local v16    # "suffix":I
    .end local v17    # "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    :cond_e
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 868
    .restart local v9    # "subIndices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/fst/FST<Lorg/apache/lucene/util/BytesRef;>;>;"
    const/16 v18, 0x0

    .line 869
    .restart local v18    # "termCount":I
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_f

    .line 916
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 869
    :cond_f
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;

    .line 870
    .restart local v11    # "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    iget-boolean v6, v11, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;->isTerm:Z

    if-eqz v6, :cond_12

    move-object/from16 v17, v11

    .line 871
    check-cast v17, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;

    .line 872
    .restart local v17    # "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    move-object/from16 v0, v17

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v16, v6, p2

    .line 881
    .restart local v16    # "suffix":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    shl-int/lit8 v7, v16, 0x1

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 882
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v6, v7, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeBytes([BII)V

    .line 885
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v7, v7, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 886
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v6

    sget-object v7, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v6, v7, :cond_11

    .line 887
    sget-boolean v6, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_10

    move-object/from16 v0, v17

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget-wide v6, v6, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    move-object/from16 v0, v17

    iget-object v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v8, v8, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v0, v8

    move-wide/from16 v20, v0

    cmp-long v6, v6, v20

    if-gez v6, :cond_10

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 888
    :cond_10
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter2:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget-wide v0, v7, Lorg/apache/lucene/codecs/TermStats;->totalTermFreq:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->stats:Lorg/apache/lucene/codecs/TermStats;

    iget v7, v7, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    int-to-long v0, v7

    move-wide/from16 v22, v0

    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 891
    :cond_11
    add-int/lit8 v18, v18, 0x1

    .line 892
    goto/16 :goto_8

    .end local v16    # "suffix":I
    .end local v17    # "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    :cond_12
    move-object v10, v11

    .line 893
    check-cast v10, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    .line 894
    .local v10, "block":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    iget-object v6, v10, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v16, v6, p2

    .line 896
    .restart local v16    # "suffix":I
    sget-boolean v6, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_13

    if-gtz v16, :cond_13

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 900
    :cond_13
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    shl-int/lit8 v7, v16, 0x1

    or-int/lit8 v7, v7, 0x1

    invoke-virtual {v6, v7}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 901
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v7, v10, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v6, v7, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeBytes([BII)V

    .line 902
    sget-boolean v6, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_14

    iget-wide v6, v10, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    cmp-long v6, v6, v4

    if-ltz v6, :cond_14

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 911
    :cond_14
    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->bytesWriter:Lorg/apache/lucene/store/RAMOutputStream;

    iget-wide v0, v10, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->fp:J

    move-wide/from16 v20, v0

    sub-long v20, v4, v20

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;->writeVLong(J)V

    .line 912
    iget-object v6, v10, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->index:Lorg/apache/lucene/util/fst/FST;

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 924
    .end local v10    # "block":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    .end local v11    # "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    .end local v16    # "suffix":I
    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 943
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    sub-int v2, v2, p5

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    goto/16 :goto_5

    .line 951
    :cond_17
    const/4 v6, 0x0

    goto/16 :goto_6
.end method


# virtual methods
.method public finish(JJI)V
    .locals 19
    .param p1, "sumTotalTermFreq"    # J
    .param p3, "sumDocFreq"    # J
    .param p5, "docCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1009
    move-object/from16 v0, p0

    iget-wide v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->numTerms:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_5

    .line 1010
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->blockBuilder:Lorg/apache/lucene/util/fst/Builder;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/Builder;->finish()Lorg/apache/lucene/util/fst/FST;

    .line 1013
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;

    iget-boolean v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;->isTerm:Z

    if-eqz v5, :cond_1

    :cond_0
    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "pending.size()="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " pending="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 1014
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    .line 1015
    .local v4, "root":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    iget-object v5, v4, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 1016
    :cond_2
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-object v5, v4, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->index:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v5}, Lorg/apache/lucene/util/fst/FST;->getEmptyOutput()Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 1018
    :cond_3
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->sumTotalTermFreq:J

    .line 1019
    move-wide/from16 v0, p3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->sumDocFreq:J

    .line 1020
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->docCount:I

    .line 1023
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->indexStartFP:J

    .line 1024
    iget-object v5, v4, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->index:Lorg/apache/lucene/util/fst/FST;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->indexOut:Lorg/apache/lucene/store/IndexOutput;
    invoke-static {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/fst/FST;->save(Lorg/apache/lucene/store/DataOutput;)V

    .line 1035
    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->fields:Ljava/util/List;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsWriter;)Ljava/util/List;

    move-result-object v17

    new-instance v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;

    move-object/from16 v0, p0

    iget-object v6, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 1036
    move-object/from16 v0, p0

    iget-object v7, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    iget-object v7, v7, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->index:Lorg/apache/lucene/util/fst/FST;

    invoke-virtual {v7}, Lorg/apache/lucene/util/fst/FST;->getEmptyOutput()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/lucene/util/BytesRef;

    .line 1037
    move-object/from16 v0, p0

    iget-wide v8, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->numTerms:J

    .line 1038
    move-object/from16 v0, p0

    iget-wide v10, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->indexStartFP:J

    move-wide/from16 v12, p1

    move-wide/from16 v14, p3

    move/from16 v16, p5

    .line 1041
    invoke-direct/range {v5 .. v16}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$FieldMetaData;-><init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/BytesRef;JJJJI)V

    .line 1035
    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047
    .end local v4    # "root":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    :cond_4
    return-void

    .line 1043
    :cond_5
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    const-wide/16 v6, 0x0

    cmp-long v5, p1, v6

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v5}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v5

    sget-object v6, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v5, v6, :cond_6

    const-wide/16 v6, -0x1

    cmp-long v5, p1, v6

    if-eqz v5, :cond_7

    :cond_6
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 1044
    :cond_7
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_8

    const-wide/16 v6, 0x0

    cmp-long v5, p3, v6

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 1045
    :cond_8
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    if-eqz p5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5
.end method

.method public finishTerm(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V
    .locals 4
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "stats"    # Lorg/apache/lucene/codecs/TermStats;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 997
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p2, Lorg/apache/lucene/codecs/TermStats;->docFreq:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1000
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->blockBuilder:Lorg/apache/lucene/util/fst/Builder;

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->scratchIntsRef:Lorg/apache/lucene/util/IntsRef;

    invoke-static {p1, v1}, Lorg/apache/lucene/util/fst/Util;->toIntsRef(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/IntsRef;)Lorg/apache/lucene/util/IntsRef;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->noOutputs:Lorg/apache/lucene/util/fst/NoOutputs;

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/NoOutputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/fst/Builder;->add(Lorg/apache/lucene/util/IntsRef;Ljava/lang/Object;)V

    .line 1001
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    new-instance v1, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;

    invoke-static {p1}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;-><init>(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/codecs/TermStats;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1002
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/PostingsWriterBase;->finishTerm(Lorg/apache/lucene/codecs/TermStats;)V

    .line 1003
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->numTerms:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->numTerms:J

    .line 1004
    return-void
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 975
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public startTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;
    .locals 1
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 981
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/PostingsWriterBase;->startTerm()V

    .line 989
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->postingsWriter:Lorg/apache/lucene/codecs/PostingsWriterBase;

    return-object v0
.end method

.method writeBlocks(Lorg/apache/lucene/util/IntsRef;II)V
    .locals 38
    .param p1, "prevTerm"    # Lorg/apache/lucene/util/IntsRef;
    .param p2, "prefixLength"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 563
    if-eqz p2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->maxItemsInBlock:I

    move/from16 v0, p3

    if-gt v0, v2, :cond_1

    .line 568
    :cond_0
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v11, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p3

    invoke-direct/range {v2 .. v11}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->writeBlock(Lorg/apache/lucene/util/IntsRef;IIIIIZIZ)Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    move-result-object v28

    .line 569
    .local v28, "nonFloorBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget-object v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->scratchBytes:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->compileIndex(Ljava/util/List;Lorg/apache/lucene/store/RAMOutputStream;)V

    .line 570
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    move-object/from16 v0, v28

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 770
    .end local v28    # "nonFloorBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->lastBlockIndex:I

    .line 771
    return-void

    .line 592
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int v3, v3, p2

    aget v30, v2, v3

    .line 601
    .local v30, "savLabel":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int v3, v3, p3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v31

    .line 602
    .local v31, "slice":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;>;"
    const/16 v27, -0x1

    .line 603
    .local v27, "lastSuffixLeadLabel":I
    const/16 v37, 0x0

    .line 604
    .local v37, "termCount":I
    const/16 v33, 0x0

    .line 605
    .local v33, "subCount":I
    const/16 v29, 0x0

    .line 607
    .local v29, "numSubs":I
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 657
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    array-length v2, v2

    move/from16 v0, v29

    if-ne v2, v0, :cond_2

    .line 658
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    .line 659
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    .line 660
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    invoke-static {v2}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    .line 663
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    aput v27, v2, v29

    .line 664
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    aput v37, v2, v29

    .line 665
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    aput v33, v2, v29

    .line 666
    add-int/lit8 v29, v29, 0x1

    .line 675
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    array-length v2, v2

    move/from16 v0, v29

    if-ge v2, v0, :cond_3

    .line 676
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    move/from16 v0, v29

    invoke-static {v2, v0}, Lorg/apache/lucene/util/ArrayUtil;->grow([II)[I

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    .line 682
    :cond_3
    const/16 v35, 0x0

    .line 683
    .local v35, "sum":I
    add-int/lit8 v26, v29, -0x1

    .local v26, "idx":I
    :goto_2
    if-gez v26, :cond_e

    .line 695
    const/4 v7, 0x0

    .line 696
    .local v7, "pendingCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    const/4 v3, 0x0

    aget v10, v2, v3

    .line 697
    .local v10, "startLabel":I
    move/from16 v6, p3

    .line 698
    .local v6, "curStart":I
    const/16 v33, 0x0

    .line 700
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 701
    .local v25, "floorBlocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;>;"
    const/16 v23, 0x0

    .line 703
    .local v23, "firstBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    const/16 v32, 0x0

    .local v32, "sub":I
    :goto_3
    move/from16 v0, v32

    move/from16 v1, v29

    if-lt v0, v1, :cond_f

    .line 762
    :cond_4
    :goto_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int v3, v3, p2

    aput v30, v2, v3

    .line 764
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_17

    if-nez v23, :cond_17

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 607
    .end local v6    # "curStart":I
    .end local v7    # "pendingCount":I
    .end local v10    # "startLabel":I
    .end local v23    # "firstBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    .end local v25    # "floorBlocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;>;"
    .end local v26    # "idx":I
    .end local v32    # "sub":I
    .end local v35    # "sum":I
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;

    .line 611
    .local v22, "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    move-object/from16 v0, v22

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;->isTerm:Z

    if-eqz v3, :cond_b

    move-object/from16 v36, v22

    .line 612
    check-cast v36, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;

    .line 613
    .local v36, "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    move-object/from16 v0, v36

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v0, p2

    if-ne v3, v0, :cond_a

    .line 617
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_6

    const/4 v3, -0x1

    move/from16 v0, v27

    if-eq v0, v3, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 618
    :cond_6
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_7

    if-eqz v29, :cond_7

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 619
    :cond_7
    const/16 v34, -0x1

    .line 629
    .end local v36    # "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    .local v34, "suffixLeadLabel":I
    :goto_5
    move/from16 v0, v34

    move/from16 v1, v27

    if-eq v0, v1, :cond_9

    add-int v3, v37, v33

    if-eqz v3, :cond_9

    .line 630
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    array-length v3, v3

    move/from16 v0, v29

    if-ne v3, v0, :cond_8

    .line 631
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    invoke-static {v3}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    .line 632
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    invoke-static {v3}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    .line 633
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    invoke-static {v3}, Lorg/apache/lucene/util/ArrayUtil;->grow([I)[I

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    .line 635
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    aput v27, v3, v29

    .line 636
    move/from16 v27, v34

    .line 637
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    aput v37, v3, v29

    .line 638
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    aput v33, v3, v29

    .line 646
    const/16 v33, 0x0

    move/from16 v37, v33

    .line 647
    add-int/lit8 v29, v29, 0x1

    .line 650
    :cond_9
    move-object/from16 v0, v22

    iget-boolean v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;->isTerm:Z

    if-eqz v3, :cond_d

    .line 651
    add-int/lit8 v37, v37, 0x1

    .line 652
    goto/16 :goto_1

    .line 621
    .end local v34    # "suffixLeadLabel":I
    .restart local v36    # "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    :cond_a
    move-object/from16 v0, v36

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, v36

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;->term:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int v4, v4, p2

    aget-byte v3, v3, v4

    and-int/lit16 v0, v3, 0xff

    move/from16 v34, v0

    .line 623
    .restart local v34    # "suffixLeadLabel":I
    goto :goto_5

    .end local v34    # "suffixLeadLabel":I
    .end local v36    # "term":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingTerm;
    :cond_b
    move-object/from16 v21, v22

    .line 624
    check-cast v21, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    .line 625
    .local v21, "block":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_c

    move-object/from16 v0, v21

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v0, p2

    if-gt v3, v0, :cond_c

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 626
    :cond_c
    move-object/from16 v0, v21

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, v21

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->prefix:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int v4, v4, p2

    aget-byte v3, v3, v4

    and-int/lit16 v0, v3, 0xff

    move/from16 v34, v0

    .restart local v34    # "suffixLeadLabel":I
    goto/16 :goto_5

    .line 653
    .end local v21    # "block":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    :cond_d
    add-int/lit8 v33, v33, 0x1

    goto/16 :goto_1

    .line 684
    .end local v22    # "ent":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingEntry;
    .end local v34    # "suffixLeadLabel":I
    .restart local v26    # "idx":I
    .restart local v35    # "sum":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    aget v2, v2, v26

    add-int v35, v35, v2

    .line 685
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    aput v35, v2, v26

    .line 683
    add-int/lit8 v26, v26, -0x1

    goto/16 :goto_2

    .line 704
    .restart local v6    # "curStart":I
    .restart local v7    # "pendingCount":I
    .restart local v10    # "startLabel":I
    .restart local v23    # "firstBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    .restart local v25    # "floorBlocks":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;>;"
    .restart local v32    # "sub":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCounts:[I

    aget v2, v2, v32

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    aget v3, v3, v32

    add-int/2addr v2, v3

    add-int/2addr v7, v2

    .line 706
    add-int/lit8 v33, v33, 0x1

    .line 710
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->minItemsInBlock:I

    if-lt v7, v2, :cond_16

    .line 712
    const/4 v2, -0x1

    if-ne v10, v2, :cond_10

    .line 713
    move/from16 v5, p2

    .line 720
    .local v5, "curPrefixLength":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    add-int/lit8 v3, v32, 0x1

    aget v8, v2, v3

    const/4 v9, 0x1

    if-ne v6, v7, :cond_11

    const/4 v11, 0x1

    :goto_7
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    invoke-direct/range {v2 .. v11}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->writeBlock(Lorg/apache/lucene/util/IntsRef;IIIIIZIZ)Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    move-result-object v24

    .line 721
    .local v24, "floorBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    if-nez v23, :cond_12

    .line 722
    move-object/from16 v23, v24

    .line 726
    :goto_8
    sub-int/2addr v6, v7

    .line 728
    const/4 v7, 0x0

    .line 730
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->minItemsInBlock:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_13

    const/4 v2, 0x1

    move/from16 v0, v33

    if-gt v0, v2, :cond_13

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "minItemsInBlock="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->minItemsInBlock:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " subCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v33

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sub="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " subTermCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subTermCountSums:[I

    aget v4, v4, v32

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " subSubCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subSubCounts:[I

    aget v4, v4, v32

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " depth="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 715
    .end local v5    # "curPrefixLength":I
    .end local v24    # "floorBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    :cond_10
    add-int/lit8 v5, p2, 0x1

    .line 717
    .restart local v5    # "curPrefixLength":I
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int v3, v3, p2

    aput v10, v2, v3

    goto/16 :goto_6

    .line 720
    :cond_11
    const/4 v11, 0x0

    goto/16 :goto_7

    .line 724
    .restart local v24    # "floorBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    :cond_12
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 731
    :cond_13
    const/16 v33, 0x0

    .line 732
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->subBytes:[I

    add-int/lit8 v3, v32, 0x1

    aget v10, v2, v3

    .line 734
    if-eqz v6, :cond_4

    .line 738
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->maxItemsInBlock:I

    if-gt v6, v2, :cond_16

    .line 743
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_14

    const/4 v2, -0x1

    if-ne v10, v2, :cond_14

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 744
    :cond_14
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_15

    if-nez v23, :cond_15

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 745
    :cond_15
    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/lucene/util/IntsRef;->ints:[I

    move-object/from16 v0, p1

    iget v3, v0, Lorg/apache/lucene/util/IntsRef;->offset:I

    add-int v3, v3, p2

    aput v10, v2, v3

    .line 756
    add-int/lit8 v14, p2, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x1

    const/16 v20, 0x1

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move/from16 v13, p2

    move v15, v6

    move/from16 v16, v6

    move/from16 v19, v10

    invoke-direct/range {v11 .. v20}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->writeBlock(Lorg/apache/lucene/util/IntsRef;IIIIIZIZ)Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 703
    .end local v5    # "curPrefixLength":I
    .end local v24    # "floorBlock":Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;
    :cond_16
    add-int/lit8 v32, v32, 0x1

    goto/16 :goto_3

    .line 765
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsWriter;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsWriter;->scratchBytes:Lorg/apache/lucene/store/RAMOutputStream;

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$PendingBlock;->compileIndex(Ljava/util/List;Lorg/apache/lucene/store/RAMOutputStream;)V

    .line 767
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsWriter$TermsWriter;->pending:Ljava/util/List;

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method
