.class public Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;
.super Lorg/apache/lucene/codecs/FieldInfosFormat;
.source "Lucene40FieldInfosFormat.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final CODEC_NAME:Ljava/lang/String; = "Lucene40FieldInfos"

.field static final FIELD_INFOS_EXTENSION:Ljava/lang/String; = "fnm"

.field static final FORMAT_CURRENT:I = 0x0

.field static final FORMAT_START:I = 0x0

.field static final IS_INDEXED:B = 0x1t

.field static final OMIT_NORMS:B = 0x10t

.field static final OMIT_POSITIONS:B = -0x80t

.field static final OMIT_TERM_FREQ_AND_POSITIONS:B = 0x40t

.field static final STORE_OFFSETS_IN_POSTINGS:B = 0x4t

.field static final STORE_PAYLOADS:B = 0x20t

.field static final STORE_TERMVECTOR:B = 0x2t


# instance fields
.field private final reader:Lorg/apache/lucene/codecs/FieldInfosReader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosFormat;-><init>()V

    .line 99
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    .line 103
    return-void
.end method


# virtual methods
.method public getFieldInfosReader()Lorg/apache/lucene/codecs/FieldInfosReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40FieldInfosFormat;->reader:Lorg/apache/lucene/codecs/FieldInfosReader;

    return-object v0
.end method

.method public getFieldInfosWriter()Lorg/apache/lucene/codecs/FieldInfosWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec can only be used for reading"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
