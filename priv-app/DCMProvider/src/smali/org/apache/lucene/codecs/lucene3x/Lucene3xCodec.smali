.class public Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;
.super Lorg/apache/lucene/codecs/Codec;
.source "Lucene3xCodec.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field static final COMPOUND_FILE_STORE_EXTENSION:Ljava/lang/String; = "cfx"


# instance fields
.field private final docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

.field private final fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

.field private final infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

.field private final liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

.field private final normsFormat:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsFormat;

.field private final postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "Lucene3x"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 51
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xPostingsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xPostingsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 53
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 55
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    .line 57
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFieldInfosFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    .line 59
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    .line 61
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->normsFormat:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsFormat;

    .line 67
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    .line 70
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec$1;

    const-string v1, "Lucene3x"

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec$1;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 49
    return-void
.end method

.method public static getDocStoreFiles(Lorg/apache/lucene/index/SegmentInfo;)Ljava/util/Set;
    .locals 4
    .param p0, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfo;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    invoke-static {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 126
    invoke-static {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "dsName":Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 128
    .local v1, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreIsCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const-string v2, ""

    const-string v3, "cfx"

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 139
    .end local v0    # "dsName":Ljava/lang/String;
    .end local v1    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    return-object v1

    .line 131
    .restart local v0    # "dsName":Ljava/lang/String;
    .restart local v1    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    const-string v2, ""

    const-string v3, "fdx"

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 132
    const-string v2, ""

    const-string v3, "fdt"

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v2, ""

    const-string v3, "tvx"

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v2, ""

    const-string v3, "tvf"

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 135
    const-string v2, ""

    const-string v3, "tvd"

    invoke-static {v0, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    .end local v0    # "dsName":Ljava/lang/String;
    .end local v1    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    return-object v0
.end method

.method public liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    return-object v0
.end method

.method public normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->normsFormat:Lorg/apache/lucene/codecs/lucene3x/Lucene3xNormsFormat;

    return-object v0
.end method

.method public postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    return-object v0
.end method

.method public storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    return-object v0
.end method

.method public termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    return-object v0
.end method
