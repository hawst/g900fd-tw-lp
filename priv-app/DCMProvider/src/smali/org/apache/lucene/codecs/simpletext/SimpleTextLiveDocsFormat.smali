.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;
.super Lorg/apache/lucene/codecs/LiveDocsFormat;
.source "SimpleTextLiveDocsFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;,
        Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final DOC:Lorg/apache/lucene/util/BytesRef;

.field static final END:Lorg/apache/lucene/util/BytesRef;

.field static final LIVEDOCS_EXTENSION:Ljava/lang/String; = "liv"

.field static final SIZE:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->$assertionsDisabled:Z

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "size "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->SIZE:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  doc "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->DOC:Lorg/apache/lucene/util/BytesRef;

    .line 52
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "END"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->END:Lorg/apache/lucene/util/BytesRef;

    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/codecs/LiveDocsFormat;-><init>()V

    return-void
.end method

.method private parseIntAt(Lorg/apache/lucene/util/BytesRef;ILorg/apache/lucene/util/CharsRef;)I
    .locals 3
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "offset"    # I
    .param p3, "scratch"    # Lorg/apache/lucene/util/CharsRef;

    .prologue
    .line 103
    iget-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v1, p2

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v2, p2

    invoke-static {v0, v1, v2, p3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 104
    iget-object v0, p3, Lorg/apache/lucene/util/CharsRef;->chars:[C

    const/4 v1, 0x0

    iget v2, p3, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v0

    return v0
.end method


# virtual methods
.method public files(Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/util/Collection;)V
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    .local p2, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v1, "liv"

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_0
    return-void
.end method

.method public newLiveDocs(I)Lorg/apache/lucene/util/MutableBits;
    .locals 1
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;

    invoke-direct {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;-><init>(I)V

    return-object v0
.end method

.method public newLiveDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/MutableBits;
    .locals 4
    .param p1, "existing"    # Lorg/apache/lucene/util/Bits;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    move-object v0, p1

    check-cast v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;

    .line 62
    .local v0, "bits":Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;
    new-instance v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;

    iget-object v1, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->bits:Ljava/util/BitSet;

    invoke-virtual {v1}, Ljava/util/BitSet;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/BitSet;

    iget v3, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->size:I

    invoke-direct {v2, v1, v3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextMutableBits;-><init>(Ljava/util/BitSet;I)V

    return-object v2
.end method

.method public readLiveDocs(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/util/Bits;
    .locals 12
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    sget-boolean v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z

    move-result v8

    if-nez v8, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 68
    :cond_0
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 69
    .local v4, "scratch":Lorg/apache/lucene/util/BytesRef;
    new-instance v5, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    .line 71
    .local v5, "scratchUTF16":Lorg/apache/lucene/util/CharsRef;
    iget-object v8, p2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v8, v8, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v9, "liv"

    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "fileName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 73
    .local v3, "in":Lorg/apache/lucene/store/IndexInput;
    const/4 v7, 0x0

    .line 75
    .local v7, "success":Z
    :try_start_0
    invoke-virtual {p1, v2, p3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    .line 77
    invoke-static {v3, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 78
    sget-boolean v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->$assertionsDisabled:Z

    if-nez v8, :cond_1

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->SIZE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v8}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v8

    if-nez v8, :cond_1

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :catchall_0
    move-exception v8

    .line 94
    if-eqz v7, :cond_5

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 95
    aput-object v3, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 99
    :goto_0
    throw v8

    .line 79
    :cond_1
    :try_start_1
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->SIZE:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v4, v8, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->parseIntAt(Lorg/apache/lucene/util/BytesRef;ILorg/apache/lucene/util/CharsRef;)I

    move-result v6

    .line 81
    .local v6, "size":I
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0, v6}, Ljava/util/BitSet;-><init>(I)V

    .line 83
    .local v0, "bits":Ljava/util/BitSet;
    invoke-static {v3, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 84
    :goto_1
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v4, v8}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 91
    const/4 v7, 0x1

    .line 92
    new-instance v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;

    invoke-direct {v8, v0, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;-><init>(Ljava/util/BitSet;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    if-eqz v7, :cond_4

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 95
    aput-object v3, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 92
    :goto_2
    return-object v8

    .line 85
    :cond_2
    :try_start_2
    sget-boolean v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->$assertionsDisabled:Z

    if-nez v8, :cond_3

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v8}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v8

    if-nez v8, :cond_3

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 86
    :cond_3
    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->DOC:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v4, v8, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->parseIntAt(Lorg/apache/lucene/util/BytesRef;ILorg/apache/lucene/util/CharsRef;)I

    move-result v1

    .line 87
    .local v1, "docid":I
    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 88
    invoke-static {v3, v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 96
    .end local v1    # "docid":I
    :cond_4
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 97
    aput-object v3, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2

    .line 96
    .end local v0    # "bits":Ljava/util/BitSet;
    .end local v6    # "size":I
    :cond_5
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 97
    aput-object v3, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method public writeLiveDocs(Lorg/apache/lucene/util/MutableBits;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V
    .locals 14
    .param p1, "bits"    # Lorg/apache/lucene/util/MutableBits;
    .param p2, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p4, "newDelCount"    # I
    .param p5, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    move-object v9, p1

    check-cast v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;

    iget-object v6, v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat$SimpleTextBits;->bits:Ljava/util/BitSet;

    .line 110
    .local v6, "set":Ljava/util/BitSet;
    invoke-interface {p1}, Lorg/apache/lucene/util/MutableBits;->length()I

    move-result v7

    .line 111
    .local v7, "size":I
    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 113
    .local v5, "scratch":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p3

    iget-object v9, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v9, v9, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v10, "liv"

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getNextDelGen()J

    move-result-wide v12

    invoke-static {v9, v10, v12, v13}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "fileName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 115
    .local v4, "out":Lorg/apache/lucene/store/IndexOutput;
    const/4 v8, 0x0

    .line 117
    .local v8, "success":Z
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {v0, v2, v1}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v4

    .line 118
    sget-object v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->SIZE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v9}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 119
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 120
    invoke-static {v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 122
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v3

    .local v3, "i":I
    :goto_0
    if-gez v3, :cond_0

    .line 128
    sget-object v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v9}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 129
    invoke-static {v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    const/4 v8, 0x1

    .line 132
    if-eqz v8, :cond_2

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 133
    aput-object v4, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 138
    :goto_1
    return-void

    .line 123
    :cond_0
    :try_start_1
    sget-object v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextLiveDocsFormat;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v9}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 124
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 125
    invoke-static {v4}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 122
    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v6, v9}, Ljava/util/BitSet;->nextSetBit(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    goto :goto_0

    .line 131
    .end local v3    # "i":I
    :catchall_0
    move-exception v9

    .line 132
    if-eqz v8, :cond_1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 133
    aput-object v4, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 137
    :goto_2
    throw v9

    .line 134
    :cond_1
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 135
    aput-object v4, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2

    .line 134
    .restart local v3    # "i":I
    :cond_2
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/io/Closeable;

    const/4 v10, 0x0

    .line 135
    aput-object v4, v9, v10

    invoke-static {v9}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method
