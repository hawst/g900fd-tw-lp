.class Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;
.super Lorg/apache/lucene/index/Terms;
.source "SimpleTextTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleTVTerms"
.end annotation


# instance fields
.field final hasOffsets:Z

.field final hasPayloads:Z

.field final hasPositions:Z

.field final terms:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            "Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVPostings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ZZZ)V
    .locals 1
    .param p1, "hasOffsets"    # Z
    .param p2, "hasPositions"    # Z
    .param p3, "hasPayloads"    # Z

    .prologue
    .line 261
    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 262
    iput-boolean p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->hasOffsets:Z

    .line 263
    iput-boolean p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->hasPositions:Z

    .line 264
    iput-boolean p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->hasPayloads:Z

    .line 265
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->terms:Ljava/util/SortedMap;

    .line 266
    return-void
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 276
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 296
    const/4 v0, 0x1

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->terms:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 286
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->hasOffsets:Z

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 311
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->hasPayloads:Z

    return v0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->hasPositions:Z

    return v0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 2
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->terms:Ljava/util/SortedMap;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTermsEnum;-><init>(Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public size()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextTermVectorsReader$SimpleTVTerms;->terms:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
