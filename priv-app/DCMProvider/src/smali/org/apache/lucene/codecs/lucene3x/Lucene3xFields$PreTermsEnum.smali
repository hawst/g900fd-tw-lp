.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "Lucene3xFields.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreTermsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final UTF8_HIGH_BMP_LEAD:B = -0x12t

.field private static final UTF8_NON_BMP_LEAD:B = -0x10t


# instance fields
.field private current:Lorg/apache/lucene/util/BytesRef;

.field private fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field private internedFieldName:Ljava/lang/String;

.field private newSuffixStart:I

.field private final prevTerm:Lorg/apache/lucene/util/BytesRef;

.field private final scratch:[B

.field private final scratchTerm:Lorg/apache/lucene/util/BytesRef;

.field private seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

.field private skipNext:Z

.field private termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

.field private unicodeSortOrder:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    .line 242
    return-void

    .line 232
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V
    .locals 1

    .prologue
    .line 232
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 257
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    .line 258
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    .line 259
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;)V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V

    return-void
.end method

.method private doContinue()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 354
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v1, v3, -0x1

    .line 356
    .local v1, "downTo":I
    const/4 v0, 0x0

    .line 358
    .local v0, "didSeek":Z
    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 360
    .local v2, "limit":I
    :goto_0
    if-gt v1, v2, :cond_0

    .line 395
    :goto_1
    return v0

    .line 362
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v3, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isHighBMPChar([BI)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 368
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v3, v4, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekToNonBMP(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 370
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 372
    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 373
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 374
    const/4 v0, 0x1

    .line 378
    goto :goto_1

    .line 388
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xc0

    const/16 v4, 0xc0

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_3

    .line 389
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iput v1, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 392
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private doPop()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 409
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v4, v5, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 410
    :cond_0
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v4, v5, :cond_1

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    if-eqz v4, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 412
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    if-le v4, v5, :cond_5

    .line 413
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isNonBMPChar([BI)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 414
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    invoke-direct {p0, v4, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isHighBMPChar([BI)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 417
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    const/4 v6, -0x1

    aput-byte v6, v4, v5

    .line 418
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 426
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    new-instance v6, Lorg/apache/lucene/index/Term;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v6, v7, v8}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {v4, v5, v6, v2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 428
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    .line 432
    .local v1, "t2":Lorg/apache/lucene/index/Term;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-ne v4, v5, :cond_3

    .line 438
    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 439
    .local v0, "b2":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget v3, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 447
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 448
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->setNewSuffixStart(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 461
    .end local v0    # "b2":Lorg/apache/lucene/util/BytesRef;
    .end local v1    # "t2":Lorg/apache/lucene/index/Term;
    :goto_0
    return v2

    .line 451
    .restart local v1    # "t2":Lorg/apache/lucene/index/Term;
    :cond_3
    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    if-nez v4, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eqz v4, :cond_5

    .line 455
    :cond_4
    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 456
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iput v3, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_0

    .end local v1    # "t2":Lorg/apache/lucene/index/Term;
    :cond_5
    move v2, v3

    .line 461
    goto :goto_0
.end method

.method private doPushes()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x0

    const/16 v12, -0x80

    const/4 v11, 0x1

    .line 571
    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 576
    .local v5, "upTo":I
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v5, v6, :cond_0

    .line 671
    return-void

    .line 577
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isNonBMPChar([BI)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 578
    iget v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    if-gt v5, v6, :cond_1

    .line 579
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v5, v6, :cond_1

    .line 580
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isNonBMPChar([BI)Z

    move-result v6

    if-nez v6, :cond_9

    .line 581
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isHighBMPChar([BI)Z

    move-result v6

    if-nez v6, :cond_9

    .line 584
    :cond_1
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v7, v5, 0x4

    if-ge v6, v7, :cond_2

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 586
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v3, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 587
    .local v3, "savLength":I
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v7, v7, v5

    aput-byte v7, v6, v13

    .line 588
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v8, v5, 0x1

    aget-byte v7, v7, v8

    aput-byte v7, v6, v11

    .line 589
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v8, v5, 0x2

    aget-byte v7, v7, v8

    aput-byte v7, v6, v14

    .line 591
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/16 v7, -0x12

    aput-byte v7, v6, v5

    .line 592
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, v5, 0x1

    aput-byte v12, v6, v7

    .line 593
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, v5, 0x2

    aput-byte v12, v6, v7

    .line 594
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    add-int/lit8 v7, v5, 0x3

    iput v7, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 602
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    new-instance v8, Lorg/apache/lucene/index/Term;

    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v9, v9, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v8, v9, v10}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {v6, v7, v8, v11}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 604
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    aget-byte v7, v7, v13

    aput-byte v7, v6, v5

    .line 605
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, v5, 0x1

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    aget-byte v8, v8, v11

    aput-byte v8, v6, v7

    .line 606
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, v5, 0x2

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    aget-byte v8, v8, v14

    aput-byte v8, v6, v7

    .line 607
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iput v3, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 610
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    .line 624
    .local v4, "t2":Lorg/apache/lucene/index/Term;
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-ne v6, v7, :cond_7

    .line 625
    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 626
    .local v0, "b2":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget v6, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v6, :cond_3

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 627
    :cond_3
    iget v6, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v7, v5, 0x3

    if-lt v6, v7, :cond_6

    iget-object v6, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, v5}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isHighBMPChar([BI)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 628
    const/4 v2, 0x1

    .line 629
    .local v2, "matches":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v5, :cond_4

    .line 643
    .end local v0    # "b2":Lorg/apache/lucene/util/BytesRef;
    .end local v1    # "i":I
    :goto_2
    if-eqz v2, :cond_8

    .line 651
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v8}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v11}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 653
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v7}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 657
    add-int/lit8 v5, v5, 0x3

    .line 664
    goto/16 :goto_0

    .line 630
    .restart local v0    # "b2":Lorg/apache/lucene/util/BytesRef;
    .restart local v1    # "i":I
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v6, v6, v1

    iget-object v7, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v7, v7, v1

    if-eq v6, v7, :cond_5

    .line 631
    const/4 v2, 0x0

    .line 632
    goto :goto_2

    .line 629
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 637
    .end local v1    # "i":I
    .end local v2    # "matches":Z
    :cond_6
    const/4 v2, 0x0

    .line 639
    .restart local v2    # "matches":Z
    goto :goto_2

    .line 640
    .end local v0    # "b2":Lorg/apache/lucene/util/BytesRef;
    .end local v2    # "matches":Z
    :cond_7
    const/4 v2, 0x0

    .restart local v2    # "matches":Z
    goto :goto_2

    .line 665
    :cond_8
    add-int/lit8 v5, v5, 0x1

    .line 667
    goto/16 :goto_0

    .line 668
    .end local v2    # "matches":Z
    .end local v3    # "savLength":I
    .end local v4    # "t2":Lorg/apache/lucene/index/Term;
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0
.end method

.method private final isHighBMPChar([BI)Z
    .locals 2
    .param p1, "b"    # [B
    .param p2, "idx"    # I

    .prologue
    .line 247
    aget-byte v0, p1, p2

    and-int/lit8 v0, v0, -0x12

    const/16 v1, -0x12

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private final isNonBMPChar([BI)Z
    .locals 2
    .param p1, "b"    # [B
    .param p2, "idx"    # I

    .prologue
    .line 254
    aget-byte v0, p1, p2

    and-int/lit8 v0, v0, -0x10

    const/16 v1, -0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private seekToNonBMP(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/BytesRef;I)Z
    .locals 12
    .param p1, "te"    # Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;
    .param p2, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "pos"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x2

    const/16 v10, -0x80

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 264
    iget v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 266
    .local v3, "savLength":I
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget v6, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v6, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 270
    :cond_0
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_1

    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, p3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isHighBMPChar([BI)Z

    move-result v6

    if-nez v6, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 280
    :cond_1
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v6, v6

    add-int/lit8 v7, p3, 0x4

    if-ge v6, v7, :cond_2

    .line 281
    add-int/lit8 v6, p3, 0x4

    invoke-virtual {p2, v6}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 284
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    iget-object v7, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v7, v7, p3

    aput-byte v7, v6, v5

    .line 285
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    iget-object v7, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v8, p3, 0x1

    aget-byte v7, v7, v8

    aput-byte v7, v6, v9

    .line 286
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    iget-object v7, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v8, p3, 0x2

    aget-byte v7, v7, v8

    aput-byte v7, v6, v11

    .line 288
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/16 v7, -0x10

    aput-byte v7, v6, p3

    .line 289
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, p3, 0x1

    const/16 v8, -0x70

    aput-byte v8, v6, v7

    .line 290
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, p3, 0x2

    aput-byte v10, v6, v7

    .line 291
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v7, p3, 0x3

    aput-byte v10, v6, v7

    .line 292
    add-int/lit8 v6, p3, 0x4

    iput v6, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 299
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v6

    new-instance v7, Lorg/apache/lucene/index/Term;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v8, v8, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v7, v8, p2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    invoke-virtual {v6, p1, v7, v9}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 303
    invoke-virtual {p1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    .line 308
    .local v4, "t2":Lorg/apache/lucene/index/Term;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-eq v6, v7, :cond_4

    :cond_3
    move v2, v5

    .line 340
    :goto_0
    return v2

    .line 318
    :cond_4
    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 319
    .local v0, "b2":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_5

    iget v6, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v6, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 322
    :cond_5
    iget v6, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v7, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v6, v7, :cond_8

    iget-object v6, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, p3}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isNonBMPChar([BI)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 323
    const/4 v2, 0x1

    .line 324
    .local v2, "matches":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, p3, :cond_6

    .line 335
    .end local v1    # "i":I
    :goto_2
    iput v3, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 336
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    aget-byte v5, v7, v5

    aput-byte v5, v6, p3

    .line 337
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v6, p3, 0x1

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    aget-byte v7, v7, v9

    aput-byte v7, v5, v6

    .line 338
    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v6, p3, 0x2

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratch:[B

    aget-byte v7, v7, v11

    aput-byte v7, v5, v6

    goto :goto_0

    .line 325
    .restart local v1    # "i":I
    :cond_6
    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v6, v6, v1

    iget-object v7, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v7, v7, v1

    if-eq v6, v7, :cond_7

    .line 326
    const/4 v2, 0x0

    .line 327
    goto :goto_2

    .line 324
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 331
    .end local v1    # "i":I
    .end local v2    # "matches":Z
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "matches":Z
    goto :goto_2
.end method

.method private setNewSuffixStart(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "br1"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "br2"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 820
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 821
    .local v2, "limit":I
    const/4 v1, 0x0

    .line 822
    .local v1, "lastStart":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 834
    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 838
    :goto_1
    return-void

    .line 823
    :cond_0
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xc0

    const/16 v4, 0xc0

    if-eq v3, v4, :cond_1

    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_2

    .line 824
    :cond_1
    move v1, v0

    .line 826
    :cond_2
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v5, v0

    aget-byte v4, v4, v5

    if-eq v3, v4, :cond_3

    .line 827
    iput v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    goto :goto_1

    .line 822
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private surrogateDance()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 471
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->unicodeSortOrder:Z

    if-nez v0, :cond_0

    .line 560
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-eq v0, v1, :cond_2

    .line 518
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    const/4 v1, 0x0

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 533
    :goto_1
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 520
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_1

    .line 534
    :cond_3
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 546
    :cond_4
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->doContinue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 559
    :goto_2
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->doPushes()V

    goto :goto_0

    .line 549
    :cond_5
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->doPop()Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_2
.end method


# virtual methods
.method public docFreq()I
    .locals 1

    .prologue
    .line 907
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->docFreq()I

    move-result v0

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 3
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 918
    if-eqz p2, :cond_0

    instance-of v1, p2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;

    if-nez v1, :cond_2

    .line 919
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V

    .line 926
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->reset(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;

    move-result-object v1

    return-object v1

    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
    :cond_2
    move-object v0, p2

    .line 921
    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;

    .line 922
    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;->getFreqStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    iget-object v2, v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    if-eq v1, v2, :cond_1

    .line 923
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;

    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsEnum;
    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 3
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 932
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_0

    .line 933
    const/4 v1, 0x0

    .line 942
    :goto_0
    return-object v1

    .line 934
    :cond_0
    if-eqz p2, :cond_1

    instance-of v1, p2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;

    if-nez v1, :cond_3

    .line 935
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V

    .line 942
    .local v0, "docsPosEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;
    :cond_2
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->reset(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v1

    goto :goto_0

    .end local v0    # "docsPosEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;
    :cond_3
    move-object v0, p2

    .line 937
    check-cast v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;

    .line 938
    .restart local v0    # "docsPosEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;->getFreqStream()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    iget-object v2, v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->freqStream:Lorg/apache/lucene/store/IndexInput;

    if-eq v1, v2, :cond_2

    .line 939
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;

    .end local v0    # "docsPosEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)V

    .restart local v0    # "docsPosEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreDocsAndPositionsEnum;
    goto :goto_1
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 703
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->unicodeSortOrder:Z

    if-eqz v0, :cond_0

    .line 704
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 706
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 845
    iget-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->skipNext:Z

    if-eqz v2, :cond_2

    .line 849
    iput-boolean v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->skipNext:Z

    .line 850
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v2

    if-nez v2, :cond_1

    .line 895
    :cond_0
    :goto_0
    return-object v1

    .line 853
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-ne v2, v3, :cond_0

    .line 856
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 861
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 863
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->next()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-ne v2, v3, :cond_6

    .line 864
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget v2, v2, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->newSuffixStart:I

    iput v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 868
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->surrogateDance()V

    .line 869
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 870
    .local v0, "t":Lorg/apache/lucene/index/Term;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-eq v2, v3, :cond_5

    .line 872
    :cond_3
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 873
    :cond_4
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 877
    :goto_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 875
    :cond_5
    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    goto :goto_1

    .line 885
    .end local v0    # "t":Lorg/apache/lucene/index/Term;
    :cond_6
    iput v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 886
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->surrogateDance()V

    .line 888
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 889
    .restart local v0    # "t":Lorg/apache/lucene/index/Term;
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-eq v2, v3, :cond_8

    .line 891
    :cond_7
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 894
    :cond_8
    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 895
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    goto/16 :goto_0
.end method

.method public ord()J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 717
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method reset(Lorg/apache/lucene/index/FieldInfo;)V
    .locals 6
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 677
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 678
    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    .line 679
    new-instance v1, Lorg/apache/lucene/index/Term;

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    .line 680
    .local v1, "term":Lorg/apache/lucene/index/Term;
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    if-nez v2, :cond_1

    .line 681
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .line 682
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->terms(Lorg/apache/lucene/index/Term;)Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    .line 687
    :goto_0
    iput-boolean v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->skipNext:Z

    .line 689
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->sortTermsByUnicode()Z

    move-result v2

    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->unicodeSortOrder:Z

    .line 691
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v0

    .line 692
    .local v0, "t":Lorg/apache/lucene/index/Term;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-ne v2, v3, :cond_0

    .line 693
    iput v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 694
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    iput v4, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 695
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->surrogateDance()V

    .line 697
    :cond_0
    return-void

    .line 685
    .end local v0    # "t":Lorg/apache/lucene/index/Term;
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v2, v3, v1, v5}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    goto :goto_0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 9
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 725
    const/4 v6, 0x0

    iput-boolean v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->skipNext:Z

    .line 726
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v5

    .line 727
    .local v5, "tis":Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    new-instance v3, Lorg/apache/lucene/index/Term;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v6, v6, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v6, p1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 729
    .local v3, "t0":Lorg/apache/lucene/index/Term;
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 731
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v5, v6, v3, p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 733
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v2

    .line 735
    .local v2, "t":Lorg/apache/lucene/index/Term;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-ne v6, v7, :cond_1

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    invoke-virtual {p1, v6}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 741
    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 742
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 814
    :goto_0
    return-object v6

    .line 743
    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-eq v6, v7, :cond_6

    .line 754
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 756
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v6, :cond_3

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 758
    :cond_3
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_1
    if-gez v1, :cond_4

    .line 785
    iput-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 786
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 759
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-direct {p0, v6, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->isHighBMPChar([BI)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 764
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v6, v7, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekToNonBMP(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 766
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->scratchTerm:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v7}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 767
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    # invokes: Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->getTermsDict()Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;
    invoke-static {v6}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->access$0(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;)Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->seekTermEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v8}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v8

    invoke-virtual {v6, v7, v8, p2}, Lorg/apache/lucene/codecs/lucene3x/TermInfosReader;->seekEnum(Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;Lorg/apache/lucene/index/Term;Z)Lorg/apache/lucene/codecs/lucene3x/TermInfo;

    .line 769
    add-int/lit8 v6, v1, 0x1

    iput v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->newSuffixStart:I

    .line 771
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->doPushes()V

    .line 775
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 776
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 758
    :cond_5
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 792
    .end local v1    # "i":I
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->prevTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 798
    invoke-virtual {v2}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 799
    .local v0, "br":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_7

    iget v6, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    if-eqz v6, :cond_7

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 801
    :cond_7
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->setNewSuffixStart(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 803
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->surrogateDance()V

    .line 805
    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->termEnum:Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/lucene3x/SegmentTermEnum;->term()Lorg/apache/lucene/index/Term;

    move-result-object v4

    .line 806
    .local v4, "t2":Lorg/apache/lucene/index/Term;
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    if-eq v6, v7, :cond_a

    .line 808
    :cond_8
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_9

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->field()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->internedFieldName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 809
    :cond_9
    iput-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 810
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0

    .line 812
    :cond_a
    invoke-virtual {v4}, Lorg/apache/lucene/index/Term;->bytes()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 813
    sget-boolean v6, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->$assertionsDisabled:Z

    if-nez v6, :cond_b

    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->unicodeSortOrder:Z

    if-eqz v6, :cond_b

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v6}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v6

    if-ltz v6, :cond_b

    new-instance v6, Ljava/lang/AssertionError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "term="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/util/UnicodeUtil;->toHexString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " vs current="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v8}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lorg/apache/lucene/util/UnicodeUtil;->toHexString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 814
    :cond_b
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 712
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 912
    const-wide/16 v0, -0x1

    return-wide v0
.end method
