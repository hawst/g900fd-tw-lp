.class final Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;
.super Lorg/apache/lucene/codecs/StoredFieldsReader;
.source "Lucene3xStoredFieldsReader.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I = null

.field static final synthetic $assertionsDisabled:Z

.field public static final FIELDS_EXTENSION:Ljava/lang/String; = "fdt"

.field public static final FIELDS_INDEX_EXTENSION:Ljava/lang/String; = "fdx"

.field public static final FIELD_IS_BINARY:I = 0x2

.field public static final FIELD_IS_NUMERIC_DOUBLE:I = 0x20

.field public static final FIELD_IS_NUMERIC_FLOAT:I = 0x18

.field public static final FIELD_IS_NUMERIC_INT:I = 0x8

.field public static final FIELD_IS_NUMERIC_LONG:I = 0x10

.field static final FIELD_IS_NUMERIC_MASK:I = 0x38

.field public static final FORMAT_CURRENT:I = 0x3

.field static final FORMAT_LUCENE_3_0_NO_COMPRESSED_FIELDS:I = 0x2

.field static final FORMAT_LUCENE_3_2_NUMERIC_FIELDS:I = 0x3

.field static final FORMAT_MINIMUM:I = 0x2

.field private static final FORMAT_SIZE:I = 0x4

.field private static final _NUMERIC_BIT_SHIFT:I = 0x3


# instance fields
.field private closed:Z

.field private docStoreOffset:I

.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private final fieldsStream:Lorg/apache/lucene/store/IndexInput;

.field private final format:I

.field private final indexStream:Lorg/apache/lucene/store/IndexInput;

.field private numTotalDocs:I

.field private size:I

.field private final storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I
    .locals 3

    .prologue
    .line 48
    sget-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->values()[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->$assertionsDisabled:Z

    .line 82
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/index/FieldInfos;IIIILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V
    .locals 1
    .param p1, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numTotalDocs"    # I
    .param p3, "size"    # I
    .param p4, "format"    # I
    .param p5, "docStoreOffset"    # I
    .param p6, "fieldsStream"    # Lorg/apache/lucene/store/IndexInput;
    .param p7, "indexStream"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 128
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 130
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 131
    iput p2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->numTotalDocs:I

    .line 132
    iput p3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->size:I

    .line 133
    iput p4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    .line 134
    iput p5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->docStoreOffset:I

    .line 135
    iput-object p6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 136
    iput-object p7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    .line 138
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 15
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 141
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreSegment(Lorg/apache/lucene/index/SegmentInfo;)Ljava/lang/String;

    move-result-object v7

    .line 142
    .local v7, "segment":Ljava/lang/String;
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreOffset(Lorg/apache/lucene/index/SegmentInfo;)I

    move-result v3

    .line 143
    .local v3, "docStoreOffset":I
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v8

    .line 144
    .local v8, "size":I
    const/4 v9, 0x0

    .line 145
    .local v9, "success":Z
    move-object/from16 v0, p3

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 147
    const/4 v10, -0x1

    if-eq v3, v10, :cond_1

    :try_start_0
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->getDocStoreIsCompoundFile(Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 148
    new-instance v2, Lorg/apache/lucene/store/CompoundFileDirectory;

    move-object/from16 v0, p2

    iget-object v10, v0, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    .line 149
    const-string v11, ""

    const-string v12, "cfx"

    invoke-static {v7, v11, v12}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    move-object/from16 v0, p4

    invoke-direct {v2, v10, v11, v0, v12}, Lorg/apache/lucene/store/CompoundFileDirectory;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V

    .line 148
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    .end local p1    # "d":Lorg/apache/lucene/store/Directory;
    .local v2, "d":Lorg/apache/lucene/store/Directory;
    move-object/from16 p1, v2

    .line 153
    .end local v2    # "d":Lorg/apache/lucene/store/Directory;
    .restart local p1    # "d":Lorg/apache/lucene/store/Directory;
    :goto_0
    const-string v10, ""

    const-string v11, "fdt"

    invoke-static {v7, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v10, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    .line 154
    const-string v10, ""

    const-string v11, "fdx"

    invoke-static {v7, v10, v11}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 155
    .local v6, "indexStreamFN":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-virtual {v0, v6, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    .line 157
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v10}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v10

    iput v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    .line 159
    iget v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    const/4 v11, 0x2

    if-ge v10, v11, :cond_2

    .line 160
    new-instance v10, Lorg/apache/lucene/index/IndexFormatTooOldException;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    iget v12, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    const/4 v13, 0x2

    const/4 v14, 0x3

    invoke-direct {v10, v11, v12, v13, v14}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    .end local v6    # "indexStreamFN":Ljava/lang/String;
    :catchall_0
    move-exception v10

    .line 190
    if-nez v9, :cond_0

    .line 192
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 195
    :cond_0
    :goto_1
    throw v10

    .line 151
    :cond_1
    const/4 v10, 0x0

    :try_start_2
    iput-object v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    goto :goto_0

    .line 161
    .restart local v6    # "indexStreamFN":Ljava/lang/String;
    :cond_2
    iget v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    const/4 v11, 0x3

    if-le v10, v11, :cond_3

    .line 162
    new-instance v10, Lorg/apache/lucene/index/IndexFormatTooNewException;

    iget-object v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    iget v12, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    const/4 v13, 0x2

    const/4 v14, 0x3

    invoke-direct {v10, v11, v12, v13, v14}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v10

    .line 164
    :cond_3
    iget-object v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v10}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x4

    sub-long v4, v10, v12

    .line 166
    .local v4, "indexSize":J
    const/4 v10, -0x1

    if-eq v3, v10, :cond_4

    .line 168
    iput v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->docStoreOffset:I

    .line 169
    iput v8, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->size:I

    .line 173
    sget-boolean v10, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v10, :cond_5

    const-wide/16 v10, 0x8

    div-long v10, v4, v10

    long-to-int v10, v10

    iget v11, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->docStoreOffset:I

    add-int/2addr v11, v8

    if-ge v10, v11, :cond_5

    new-instance v10, Ljava/lang/AssertionError;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "indexSize="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " size="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " docStoreOffset="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v10

    .line 175
    :cond_4
    const/4 v10, 0x0

    iput v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->docStoreOffset:I

    .line 176
    const/4 v10, 0x3

    shr-long v10, v4, v10

    long-to-int v10, v10

    iput v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->size:I

    .line 178
    iget v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->size:I

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v11

    if-eq v10, v11, :cond_5

    .line 179
    new-instance v10, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "doc counts differ for segment "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": fieldsReader shows "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->size:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " but segmentInfo shows "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 182
    :cond_5
    const/4 v10, 0x3

    shr-long v10, v4, v10

    long-to-int v10, v10

    iput v10, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->numTotalDocs:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 183
    const/4 v9, 0x1

    .line 190
    if-nez v9, :cond_6

    .line 192
    :try_start_3
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 196
    :cond_6
    :goto_2
    return-void

    .line 193
    .end local v4    # "indexSize":J
    .end local v6    # "indexStreamFN":Ljava/lang/String;
    :catch_0
    move-exception v11

    goto/16 :goto_1

    .restart local v4    # "indexSize":J
    .restart local v6    # "indexStreamFN":Ljava/lang/String;
    :catch_1
    move-exception v10

    goto :goto_2
.end method

.method public static checkCodeVersion(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 7
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p1, "segment"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 113
    const-string v3, ""

    const-string v4, "fdx"

    invoke-static {p1, v3, v4}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 114
    .local v2, "indexStreamFN":Ljava/lang/String;
    sget-object v3, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {p0, v2, v3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 117
    .local v1, "idxStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 118
    .local v0, "format":I
    if-ge v0, v5, :cond_0

    .line 119
    new-instance v3, Lorg/apache/lucene/index/IndexFormatTooOldException;

    const/4 v4, 0x2

    const/4 v5, 0x3

    invoke-direct {v3, v1, v0, v4, v5}, Lorg/apache/lucene/index/IndexFormatTooOldException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    .end local v0    # "format":I
    :catchall_0
    move-exception v3

    .line 123
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 124
    throw v3

    .line 120
    .restart local v0    # "format":I
    :cond_0
    if-le v0, v6, :cond_1

    .line 121
    :try_start_1
    new-instance v3, Lorg/apache/lucene/index/IndexFormatTooNewException;

    const/4 v4, 0x2

    const/4 v5, 0x3

    invoke-direct {v3, v1, v0, v4, v5}, Lorg/apache/lucene/index/IndexFormatTooNewException;-><init>(Lorg/apache/lucene/store/DataInput;III)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    :cond_1
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 125
    return-void
.end method

.method private ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 202
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->closed:Z

    if-eqz v0, :cond_0

    .line 203
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this FieldsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_0
    return-void
.end method

.method private readField(Lorg/apache/lucene/index/StoredFieldVisitor;Lorg/apache/lucene/index/FieldInfo;I)V
    .locals 7
    .param p1, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .param p2, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "bits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 250
    and-int/lit8 v2, p3, 0x38

    .line 251
    .local v2, "numeric":I
    if-eqz v2, :cond_0

    .line 252
    sparse-switch v2, :sswitch_data_0

    .line 266
    new-instance v3, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid numeric type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 254
    :sswitch_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v3

    invoke-virtual {p1, p2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->intField(Lorg/apache/lucene/index/FieldInfo;I)V

    .line 278
    :goto_0
    return-void

    .line 257
    :sswitch_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    invoke-virtual {p1, p2, v4, v5}, Lorg/apache/lucene/index/StoredFieldVisitor;->longField(Lorg/apache/lucene/index/FieldInfo;J)V

    goto :goto_0

    .line 260
    :sswitch_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v3

    invoke-virtual {p1, p2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->floatField(Lorg/apache/lucene/index/FieldInfo;F)V

    goto :goto_0

    .line 263
    :sswitch_3
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    invoke-virtual {p1, p2, v4, v5}, Lorg/apache/lucene/index/StoredFieldVisitor;->doubleField(Lorg/apache/lucene/index/FieldInfo;D)V

    goto :goto_0

    .line 269
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 270
    .local v1, "length":I
    new-array v0, v1, [B

    .line 271
    .local v0, "bytes":[B
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3, v0, v6, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 272
    and-int/lit8 v3, p3, 0x2

    if-eqz v3, :cond_1

    .line 273
    invoke-virtual {p1, p2, v0}, Lorg/apache/lucene/index/StoredFieldVisitor;->binaryField(Lorg/apache/lucene/index/FieldInfo;[B)V

    goto :goto_0

    .line 275
    :cond_1
    new-instance v3, Ljava/lang/String;

    array-length v4, v0

    sget-object v5, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v0, v6, v4, v5}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-virtual {p1, p2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->stringField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method private seekIndex(I)V
    .locals 8
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    const-wide/16 v2, 0x4

    iget v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->docStoreOffset:I

    add-int/2addr v1, p1

    int-to-long v4, v1

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 222
    return-void
.end method

.method private skipField(I)V
    .locals 8
    .param p1, "bits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    and-int/lit8 v1, p1, 0x38

    .line 282
    .local v1, "numeric":I
    if-eqz v1, :cond_0

    .line 283
    sparse-switch v1, :sswitch_data_0

    .line 293
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid numeric type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 286
    :sswitch_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    .line 299
    :goto_0
    return-void

    .line 290
    :sswitch_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    goto :goto_0

    .line 296
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 297
    .local v0, "length":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    goto :goto_0

    .line 283
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/codecs/StoredFieldsReader;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->clone()Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;
    .locals 8

    .prologue
    .line 107
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->ensureOpen()V

    .line 108
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->numTotalDocs:I

    iget v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->size:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->format:I

    iget v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->docStoreOffset:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v7}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;-><init>(Lorg/apache/lucene/index/FieldInfos;IIIILorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/store/IndexInput;)V

    return-object v0
.end method

.method public final close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 214
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->closed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->storeCFSReader:Lorg/apache/lucene/store/CompoundFileDirectory;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 216
    iput-boolean v3, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->closed:Z

    .line 218
    :cond_0
    return-void
.end method

.method public final visitDocument(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 8
    .param p1, "n"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->seekIndex(I)V

    .line 226
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->indexStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v6}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 228
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    .line 229
    .local v4, "numFields":I
    const/4 v1, 0x0

    .local v1, "fieldIDX":I
    :goto_0
    if-lt v1, v4, :cond_0

    .line 247
    :pswitch_0
    return-void

    .line 230
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v3

    .line 231
    .local v3, "fieldNumber":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5, v3}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v2

    .line 233
    .local v2, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    iget-object v5, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v5

    and-int/lit16 v0, v5, 0xff

    .line 234
    .local v0, "bits":I
    sget-boolean v5, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    const/16 v5, 0x3a

    if-le v0, v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "bits="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 236
    :cond_1
    invoke-static {}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I

    move-result-object v5

    invoke-virtual {p2, v2}, Lorg/apache/lucene/index/StoredFieldVisitor;->needsField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 229
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 238
    :pswitch_1
    invoke-direct {p0, p2, v2, v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->readField(Lorg/apache/lucene/index/StoredFieldVisitor;Lorg/apache/lucene/index/FieldInfo;I)V

    goto :goto_1

    .line 241
    :pswitch_2
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xStoredFieldsReader;->skipField(I)V

    goto :goto_1

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
