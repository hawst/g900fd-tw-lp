.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;
.super Lorg/apache/lucene/codecs/PostingsConsumer;
.source "SimpleTextFieldsWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTextPostingsWriter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field private lastStartOffset:I

.field private term:Lorg/apache/lucene/util/BytesRef;

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

.field private final writeOffsets:Z

.field private final writePositions:Z

.field private wroteTerm:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 4
    .param p2, "field"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-direct {p0}, Lorg/apache/lucene/codecs/PostingsConsumer;-><init>()V

    .line 107
    iput v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->lastStartOffset:I

    .line 110
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 111
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->writePositions:Z

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v3, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->writeOffsets:Z

    .line 115
    return-void

    :cond_0
    move v0, v2

    .line 111
    goto :goto_0

    :cond_1
    move v1, v2

    .line 112
    goto :goto_1
.end method


# virtual methods
.method public addPosition(ILorg/apache/lucene/util/BytesRef;II)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "payload"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "startOffset"    # I
    .param p4, "endOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->writePositions:Z

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->POS:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 149
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 153
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->writeOffsets:Z

    if-eqz v0, :cond_3

    .line 154
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ge p4, p3, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 155
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->lastStartOffset:I

    if-ge p3, v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startOffset="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lastStartOffset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->lastStartOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 156
    :cond_2
    iput p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->lastStartOffset:I

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 158
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 160
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 161
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 165
    :cond_3
    if-eqz p2, :cond_5

    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lez v0, :cond_5

    .line 166
    sget-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 167
    :cond_4
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 168
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 169
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 171
    :cond_5
    return-void
.end method

.method public finishDoc()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public reset(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/codecs/PostingsConsumer;
    .locals 1
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 140
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->term:Lorg/apache/lucene/util/BytesRef;

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->wroteTerm:Z

    .line 142
    return-object p0
.end method

.method public startDoc(II)V
    .locals 2
    .param p1, "docID"    # I
    .param p2, "termDocFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->wroteTerm:Z

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->TERM:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->term:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->wroteTerm:Z

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v0, v1, :cond_1

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->FREQ:Lorg/apache/lucene/util/BytesRef;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Lorg/apache/lucene/util/BytesRef;)V

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->write(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$2(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;

    # invokes: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->newline()V
    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;->access$1(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter;)V

    .line 136
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsWriter$SimpleTextPostingsWriter;->lastStartOffset:I

    .line 137
    return-void
.end method
