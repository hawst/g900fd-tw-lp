.class Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;
.super Lorg/apache/lucene/index/Terms;
.source "Lucene40TermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TVTerms"
.end annotation


# instance fields
.field private final numTerms:I

.field private final storeOffsets:Z

.field private final storePayloads:Z

.field private final storePositions:Z

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

.field private final tvfFPStart:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;J)V
    .locals 4
    .param p2, "tvfFP"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 317
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 318
    # getter for: Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 319
    # getter for: Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->numTerms:I

    .line 320
    # getter for: Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v0

    .line 321
    .local v0, "bits":B
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storePositions:Z

    .line 322
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storeOffsets:Z

    .line 323
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_2

    :goto_2
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storePayloads:Z

    .line 324
    # getter for: Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->tvfFPStart:J

    .line 325
    return-void

    :cond_0
    move v1, v3

    .line 321
    goto :goto_0

    :cond_1
    move v1, v3

    .line 322
    goto :goto_1

    :cond_2
    move v2, v3

    .line 323
    goto :goto_2
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x1

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2

    .prologue
    .line 355
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->numTerms:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2

    .prologue
    .line 349
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storeOffsets:Z

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storePayloads:Z

    return v0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storePositions:Z

    return v0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 7
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    instance-of v1, p1, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 331
    check-cast v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;

    .line 332
    .local v0, "termsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    # getter for: Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->tvf:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;->access$3(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 333
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;

    .end local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)V

    .line 338
    .restart local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
    :cond_0
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->numTerms:I

    iget-wide v2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->tvfFPStart:J

    iget-boolean v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storePositions:Z

    iget-boolean v5, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storeOffsets:Z

    iget-boolean v6, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->storePayloads:Z

    invoke-virtual/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;->reset(IJZZZ)V

    .line 339
    return-object v0

    .line 336
    .end local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;-><init>(Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader;)V

    .restart local v0    # "termsEnum":Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTermsEnum;
    goto :goto_0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 344
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40TermVectorsReader$TVTerms;->numTerms:I

    int-to-long v0, v0

    return-wide v0
.end method
