.class Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;
.super Lorg/apache/lucene/index/NumericDocValues;
.source "SimpleTextDocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getNumeric(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/NumericDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

.field private final synthetic val$decoder:Ljava/text/DecimalFormat;

.field private final synthetic val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

.field private final synthetic val$in:Lorg/apache/lucene/store/IndexInput;

.field private final synthetic val$scratch:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$in:Lorg/apache/lucene/store/IndexInput;

    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iput-object p4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iput-object p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$decoder:Ljava/text/DecimalFormat;

    .line 153
    invoke-direct {p0}, Lorg/apache/lucene/index/NumericDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 10
    .param p1, "docID"    # I

    .prologue
    .line 158
    if-ltz p1, :cond_0

    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iget v4, v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    if-lt p1, v4, :cond_1

    .line 159
    :cond_0
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "docID must be 0 .. "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iget v6, v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; got "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :catch_0
    move-exception v2

    .line 174
    .local v2, "ioe":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 161
    .end local v2    # "ioe":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v6, v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-object v5, v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    mul-int/2addr v5, p1

    int-to-long v8, v5

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 162
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 166
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$decoder:Ljava/text/DecimalFormat;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 172
    .local v0, "bd":Ljava/math/BigDecimal;
    :try_start_3
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$1;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v4, v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->minValue:J

    invoke-static {v4, v5}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigIntegerExact()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v4

    return-wide v4

    .line 167
    .end local v0    # "bd":Ljava/math/BigDecimal;
    :catch_1
    move-exception v3

    .line 168
    .local v3, "pe":Ljava/text/ParseException;
    new-instance v1, Lorg/apache/lucene/index/CorruptIndexException;

    const-string v4, "failed to parse BigDecimal value"

    invoke-direct {v1, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    .line 169
    .local v1, "e":Lorg/apache/lucene/index/CorruptIndexException;
    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/CorruptIndexException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 170
    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
.end method
