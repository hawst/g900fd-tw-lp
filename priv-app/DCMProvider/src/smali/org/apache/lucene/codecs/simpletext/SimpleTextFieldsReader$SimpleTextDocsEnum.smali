.class Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "SimpleTextFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SimpleTextDocsEnum"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private cost:I

.field private docID:I

.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private final inStart:Lorg/apache/lucene/store/IndexInput;

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private omitTF:Z

.field private final scratch:Lorg/apache/lucene/util/BytesRef;

.field private final scratchUTF16:Lorg/apache/lucene/util/CharsRef;

.field private tf:I

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 239
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 232
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    .line 235
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 236
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/CharsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 240
    # getter for: Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->access$0(Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->inStart:Lorg/apache/lucene/store/IndexInput;

    .line 241
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->inStart:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    .line 242
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->slowAdvance(I)I

    move-result v0

    return v0
.end method

.method public canReuse(Lorg/apache/lucene/store/IndexInput;)Z
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->inStart:Lorg/apache/lucene/store/IndexInput;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 323
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->cost:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 265
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->tf:I

    return v0
.end method

.method public nextDoc()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v4, 0x7fffffff

    const/4 v9, 0x0

    .line 270
    iget v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    if-ne v5, v4, :cond_0

    .line 271
    iget v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    .line 310
    :goto_0
    return v4

    .line 273
    :cond_0
    const/4 v0, 0x1

    .line 274
    .local v0, "first":Z
    const/4 v1, 0x0

    .line 276
    .local v1, "termFreq":I
    :cond_1
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    .line 277
    .local v2, "lineStart":J
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 278
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 279
    if-nez v0, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    invoke-interface {v5, v6}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 280
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 281
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->omitTF:Z

    if-nez v4, :cond_3

    .line 282
    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->tf:I

    .line 284
    :cond_3
    iget v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    goto :goto_0

    .line 286
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->DOC:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v5, v6, v7, v8}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 287
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget-object v5, v5, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget v6, v6, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v5, v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    .line 288
    const/4 v1, 0x0

    .line 289
    const/4 v0, 0x0

    .line 290
    goto :goto_1

    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 291
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v6, v6, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FREQ:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v5, v6, v7, v8}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 292
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget-object v5, v5, Lorg/apache/lucene/util/CharsRef;->chars:[C

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget v6, v6, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v5, v9, v6}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v1

    .line 293
    goto/16 :goto_1

    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->POS:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 295
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->START_OFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 297
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END_OFFSET:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 299
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->PAYLOAD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 302
    sget-boolean v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->TERM:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "scratch="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 303
    :cond_7
    if-nez v0, :cond_a

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    invoke-interface {v5, v6}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 304
    :cond_8
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4, v2, v3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 305
    iget-boolean v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->omitTF:Z

    if-nez v4, :cond_9

    .line 306
    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->tf:I

    .line 308
    :cond_9
    iget v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    goto/16 :goto_0

    .line 310
    :cond_a
    iput v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    goto/16 :goto_0
.end method

.method public reset(JLorg/apache/lucene/util/Bits;ZI)Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;
    .locals 1
    .param p1, "fp"    # J
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "omitTF"    # Z
    .param p5, "docFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 250
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 251
    iput-boolean p4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->omitTF:Z

    .line 252
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->docID:I

    .line 253
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->tf:I

    .line 254
    iput p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextFieldsReader$SimpleTextDocsEnum;->cost:I

    .line 255
    return-object p0
.end method
