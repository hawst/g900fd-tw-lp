.class public final Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
.super Lorg/apache/lucene/index/Terms;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FieldReader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;,
        Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final docCount:I

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field private final index:Lorg/apache/lucene/util/fst/FST;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field final indexStartFP:J

.field final numTerms:J

.field final rootBlockFP:J

.field final rootCode:Lorg/apache/lucene/util/BytesRef;

.field final sumDocFreq:J

.field final sumTotalTermFreq:J

.field final synthetic this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader;Lorg/apache/lucene/index/FieldInfo;JLorg/apache/lucene/util/BytesRef;JJIJLorg/apache/lucene/store/IndexInput;)V
    .locals 7
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "numTerms"    # J
    .param p5, "rootCode"    # Lorg/apache/lucene/util/BytesRef;
    .param p6, "sumTotalTermFreq"    # J
    .param p8, "sumDocFreq"    # J
    .param p10, "docCount"    # I
    .param p11, "indexStartFP"    # J
    .param p13, "indexIn"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 461
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 462
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v3, p3, v4

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 463
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 465
    iput-wide p3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->numTerms:J

    .line 466
    iput-wide p6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->sumTotalTermFreq:J

    .line 467
    iput-wide p8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->sumDocFreq:J

    .line 468
    move/from16 v0, p10

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->docCount:I

    .line 469
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->indexStartFP:J

    .line 470
    iput-object p5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootCode:Lorg/apache/lucene/util/BytesRef;

    .line 475
    new-instance v3, Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v4, p5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p5, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v6, p5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>([BII)V

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    const/4 v3, 0x2

    ushr-long/2addr v4, v3

    iput-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootBlockFP:J

    .line 477
    if-eqz p13, :cond_1

    .line 478
    invoke-virtual/range {p13 .. p13}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 480
    .local v2, "clone":Lorg/apache/lucene/store/IndexInput;
    move-wide/from16 v0, p11

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 481
    new-instance v3, Lorg/apache/lucene/util/fst/FST;

    invoke-static {}, Lorg/apache/lucene/util/fst/ByteSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/ByteSequenceOutputs;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lorg/apache/lucene/util/fst/FST;-><init>(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/fst/Outputs;)V

    iput-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;

    .line 495
    .end local v2    # "clone":Lorg/apache/lucene/store/IndexInput;
    :goto_0
    return-void

    .line 493
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    return-object v0
.end method


# virtual methods
.method public computeStats()Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 500
    new-instance v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)V

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->computeBlockStats()Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;

    move-result-object v0

    return-object v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 505
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1

    .prologue
    .line 545
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->docCount:I

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2

    .prologue
    .line 540
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->sumDocFreq:J

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2

    .prologue
    .line 535
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->sumTotalTermFreq:J

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    return v0
.end method

.method public hasPositions()Z
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public intersect(Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;
    .locals 2
    .param p1, "compiled"    # Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    .param p2, "startTerm"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 550
    iget-object v0, p1, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    if-eq v0, v1, :cond_0

    .line 551
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "please use CompiledAutomaton.getTermsEnum instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$IntersectEnum;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)V

    return-object v0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 525
    new-instance v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)V

    return-object v0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 530
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->numTerms:J

    return-wide v0
.end method
