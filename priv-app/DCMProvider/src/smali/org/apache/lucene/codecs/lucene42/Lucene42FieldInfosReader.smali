.class final Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosReader;
.super Lorg/apache/lucene/codecs/FieldInfosReader;
.source "Lucene42FieldInfosReader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/codecs/FieldInfosReader;-><init>()V

    .line 47
    return-void
.end method

.method private static getDocValuesType(Lorg/apache/lucene/store/IndexInput;B)Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 3
    .param p0, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    if-nez p1, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    .line 111
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 112
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    goto :goto_0

    .line 113
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 114
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    goto :goto_0

    .line 115
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 116
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    goto :goto_0

    .line 117
    :cond_3
    const/4 v0, 0x4

    if-ne p1, v0, :cond_4

    .line 118
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    goto :goto_0

    .line 120
    :cond_4
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid docvalues byte: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (resource="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/FieldInfos;
    .locals 30
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentName"    # Ljava/lang/String;
    .param p3, "iocontext"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    const-string v4, ""

    const-string v14, "fnm"

    move-object/from16 v0, p2

    invoke-static {v0, v4, v14}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 52
    .local v18, "fileName":Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v21

    .line 54
    .local v21, "input":Lorg/apache/lucene/store/IndexInput;
    const/16 v23, 0x0

    .line 56
    .local v23, "success":Z
    :try_start_0
    const-string v4, "Lucene42FieldInfos"

    .line 57
    const/4 v14, 0x0

    .line 58
    const/16 v25, 0x0

    .line 56
    move-object/from16 v0, v21

    move/from16 v1, v25

    invoke-static {v0, v4, v14, v1}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 60
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v22

    .line 61
    .local v22, "size":I
    move/from16 v0, v22

    new-array v0, v0, [Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v20, v0

    .line 63
    .local v20, "infos":[Lorg/apache/lucene/index/FieldInfo;
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v22

    if-lt v0, v1, :cond_0

    .line 93
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v26

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v28

    cmp-long v4, v26, v28

    if-eqz v4, :cond_9

    .line 94
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v25, "did not read all bytes from file \""

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v25, "\": read "

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v26

    move-wide/from16 v0, v26

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v25, " vs size "

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v26

    move-wide/from16 v0, v26

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v25, " (resource: "

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v4, v14}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    .end local v19    # "i":I
    .end local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .end local v22    # "size":I
    :catchall_0
    move-exception v4

    .line 100
    if-eqz v23, :cond_b

    .line 101
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 105
    :goto_1
    throw v4

    .line 64
    .restart local v19    # "i":I
    .restart local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .restart local v22    # "size":I
    :cond_0
    :try_start_1
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v5

    .line 65
    .local v5, "name":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    .line 66
    .local v7, "fieldNumber":I
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v16

    .line 67
    .local v16, "bits":B
    and-int/lit8 v4, v16, 0x1

    if-eqz v4, :cond_1

    const/4 v6, 0x1

    .line 68
    .local v6, "isIndexed":Z
    :goto_2
    and-int/lit8 v4, v16, 0x2

    if-eqz v4, :cond_2

    const/4 v8, 0x1

    .line 69
    .local v8, "storeTermVector":Z
    :goto_3
    and-int/lit8 v4, v16, 0x10

    if-eqz v4, :cond_3

    const/4 v9, 0x1

    .line 70
    .local v9, "omitNorms":Z
    :goto_4
    and-int/lit8 v4, v16, 0x20

    if-eqz v4, :cond_4

    const/4 v10, 0x1

    .line 72
    .local v10, "storePayloads":Z
    :goto_5
    if-nez v6, :cond_5

    .line 73
    const/4 v11, 0x0

    .line 85
    .local v11, "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :goto_6
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v24

    .line 86
    .local v24, "val":B
    and-int/lit8 v4, v24, 0xf

    int-to-byte v4, v4

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosReader;->getDocValuesType(Lorg/apache/lucene/store/IndexInput;B)Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v12

    .line 87
    .local v12, "docValuesType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    ushr-int/lit8 v4, v24, 0x4

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosReader;->getDocValuesType(Lorg/apache/lucene/store/IndexInput;B)Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v13

    .line 88
    .local v13, "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v15

    .line 89
    .local v15, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Lorg/apache/lucene/index/FieldInfo;

    .line 90
    invoke-static {v15}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v14

    invoke-direct/range {v4 .. v14}, Lorg/apache/lucene/index/FieldInfo;-><init>(Ljava/lang/String;ZIZZZLorg/apache/lucene/index/FieldInfo$IndexOptions;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Lorg/apache/lucene/index/FieldInfo$DocValuesType;Ljava/util/Map;)V

    .line 89
    aput-object v4, v20, v19

    .line 63
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 67
    .end local v6    # "isIndexed":Z
    .end local v8    # "storeTermVector":Z
    .end local v9    # "omitNorms":Z
    .end local v10    # "storePayloads":Z
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v12    # "docValuesType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .end local v13    # "normsType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .end local v15    # "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v24    # "val":B
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 68
    .restart local v6    # "isIndexed":Z
    :cond_2
    const/4 v8, 0x0

    goto :goto_3

    .line 69
    .restart local v8    # "storeTermVector":Z
    :cond_3
    const/4 v9, 0x0

    goto :goto_4

    .line 70
    .restart local v9    # "omitNorms":Z
    :cond_4
    const/4 v10, 0x0

    goto :goto_5

    .line 74
    .restart local v10    # "storePayloads":Z
    :cond_5
    and-int/lit8 v4, v16, 0x40

    if-eqz v4, :cond_6

    .line 75
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 76
    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_6
    and-int/lit8 v4, v16, -0x80

    if-eqz v4, :cond_7

    .line 77
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 78
    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_7
    and-int/lit8 v4, v16, 0x4

    if-eqz v4, :cond_8

    .line 79
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 80
    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .line 81
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    :cond_8
    sget-object v11, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .restart local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    goto :goto_6

    .line 96
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "isIndexed":Z
    .end local v7    # "fieldNumber":I
    .end local v8    # "storeTermVector":Z
    .end local v9    # "omitNorms":Z
    .end local v10    # "storePayloads":Z
    .end local v11    # "indexOptions":Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .end local v16    # "bits":B
    :cond_9
    new-instance v17, Lorg/apache/lucene/index/FieldInfos;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/FieldInfos;-><init>([Lorg/apache/lucene/index/FieldInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    .local v17, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    const/16 v23, 0x1

    .line 100
    if-eqz v23, :cond_a

    .line 101
    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 98
    :goto_7
    return-object v17

    .line 102
    :cond_a
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v14, 0x0

    .line 103
    aput-object v21, v4, v14

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_7

    .line 102
    .end local v17    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v19    # "i":I
    .end local v20    # "infos":[Lorg/apache/lucene/index/FieldInfo;
    .end local v22    # "size":I
    :cond_b
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/io/Closeable;

    const/16 v25, 0x0

    .line 103
    aput-object v21, v14, v25

    invoke-static {v14}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto/16 :goto_1
.end method
