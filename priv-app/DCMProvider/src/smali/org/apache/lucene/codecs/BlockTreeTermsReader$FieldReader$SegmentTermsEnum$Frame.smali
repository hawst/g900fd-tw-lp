.class final Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
.super Ljava/lang/Object;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Frame"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field arc:Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field entCount:I

.field floorData:[B

.field final floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field fp:J

.field fpEnd:J

.field fpOrig:J

.field hasTerms:Z

.field hasTermsOrig:Z

.field isFloor:Z

.field isLastInFloor:Z

.field isLeafBlock:Z

.field lastSubFP:J

.field metaDataUpto:I

.field nextEnt:I

.field nextFloorLabel:I

.field numFollowFloorBlocks:I

.field final ord:I

.field prefix:I

.field private startBytePos:I

.field statBytes:[B

.field final state:Lorg/apache/lucene/codecs/BlockTermState;

.field final statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field private subCode:J

.field private suffix:I

.field suffixBytes:[B

.field final suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field final synthetic this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2245
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;I)V
    .locals 4
    .param p2, "ord"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2300
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2260
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    .line 2261
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 2263
    const/16 v0, 0x40

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statBytes:[B

    .line 2264
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 2266
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorData:[B

    .line 2267
    new-instance v0, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 2301
    iput p2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    .line 2302
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/PostingsReaderBase;->newTermState()Lorg/apache/lucene/codecs/BlockTermState;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    .line 2303
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    .line 2304
    return-void
.end method

.method private fillTerm()V
    .locals 6

    .prologue
    .line 2929
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    add-int v0, v1, v2

    .line 2930
    .local v0, "termLength":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    add-int/2addr v2, v3

    iput v2, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 2931
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v1, v1

    if-ge v1, v0, :cond_0

    .line 2932
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 2934
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2935
    return-void
.end method

.method private prefixMatches(Lorg/apache/lucene/util/BytesRef;)Z
    .locals 3
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 2617
    const/4 v0, 0x0

    .local v0, "bytePos":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    if-lt v0, v1, :cond_0

    .line 2623
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 2618
    :cond_0
    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v2, v0

    aget-byte v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v2, v2, v0

    if-eq v1, v2, :cond_1

    .line 2619
    const/4 v1, 0x0

    goto :goto_1

    .line 2617
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public decodeMetaData()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2584
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->getTermBlockOrd()I

    move-result v0

    .line 2585
    .local v0, "limit":I
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 2589
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 2592
    :goto_0
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    if-lt v1, v0, :cond_1

    .line 2613
    return-void

    .line 2602
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v2

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    .line 2604
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v1

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_2

    .line 2605
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    int-to-long v2, v2

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    .line 2609
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v1

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v1

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/codecs/PostingsReaderBase;->nextTerm(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V

    .line 2610
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    .line 2611
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    goto :goto_0
.end method

.method public getTermBlockOrd()I
    .locals 1

    .prologue
    .line 2321
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    goto :goto_0
.end method

.method loadBlock()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2348
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->initIndexInput()V

    .line 2350
    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    .line 2409
    :goto_0
    return-void

    .line 2356
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v2, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 2357
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 2358
    .local v0, "code":I
    ushr-int/lit8 v2, v0, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    .line 2359
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-gtz v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 2360
    :cond_1
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    .line 2361
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_2
    move v2, v4

    .line 2360
    goto :goto_1

    .line 2369
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 2370
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_6

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    .line 2371
    ushr-int/lit8 v1, v0, 0x1

    .line 2372
    .local v1, "numBytes":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    array-length v2, v2

    if-ge v2, v1, :cond_4

    .line 2373
    invoke-static {v1, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    .line 2375
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    invoke-virtual {v2, v5, v4, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 2376
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    invoke-virtual {v2, v5, v4, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 2387
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    .line 2388
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statBytes:[B

    array-length v2, v2

    if-ge v2, v1, :cond_5

    .line 2389
    invoke-static {v1, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statBytes:[B

    .line 2391
    :cond_5
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statBytes:[B

    invoke-virtual {v2, v3, v4, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 2392
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statsReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->statBytes:[B

    invoke-virtual {v2, v3, v4, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 2393
    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    .line 2395
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iput v4, v2, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 2396
    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2397
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    .line 2401
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v2

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-result-object v4

    iget-object v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v2, v3, v4, v5}, Lorg/apache/lucene/codecs/PostingsReaderBase;->readTermsBlock(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V

    .line 2405
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpEnd:J

    goto/16 :goto_0

    .end local v1    # "numBytes":I
    :cond_6
    move v2, v4

    .line 2370
    goto/16 :goto_2
.end method

.method loadNextFloorBlock()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2328
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "arc="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isFloor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 2329
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpEnd:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    .line 2330
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2331
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 2332
    return-void
.end method

.method public next()Z
    .locals 1

    .prologue
    .line 2459
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextLeaf()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextNonLeaf()Z

    move-result v0

    goto :goto_0
.end method

.method public nextLeaf()Z
    .locals 4

    .prologue
    .line 2465
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-lt v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "nextEnt="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " entCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 2466
    :cond_1
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2467
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    .line 2468
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    .line 2469
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    add-int/2addr v1, v2

    iput v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 2470
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v0, v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v0, v1, :cond_2

    .line 2471
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 2473
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readBytes([BII)V

    .line 2475
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V

    .line 2476
    const/4 v0, 0x0

    return v0
.end method

.method public nextNonLeaf()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2481
    sget-boolean v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-lt v3, v4, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nextEnt="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " entCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " fp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2482
    :cond_1
    iget v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2483
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    .line 2484
    .local v0, "code":I
    ushr-int/lit8 v3, v0, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    .line 2485
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v3

    iput v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    .line 2486
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    add-int/2addr v4, v5

    iput v4, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 2487
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v3, v3

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v3, v4, :cond_2

    .line 2488
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 2490
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    invoke-virtual {v3, v4, v5, v6}, Lorg/apache/lucene/store/ByteArrayDataInput;->readBytes([BII)V

    .line 2491
    and-int/lit8 v3, v0, 0x1

    if-nez v3, :cond_3

    .line 2493
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    invoke-static {v3, v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V

    .line 2494
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    .line 2495
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v3, v2, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 2505
    :goto_0
    return v1

    .line 2499
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    invoke-static {v3, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V

    .line 2500
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    .line 2501
    iget-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    move v1, v2

    .line 2505
    goto :goto_0
.end method

.method rewind()V
    .locals 2

    .prologue
    .line 2414
    iget-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    iput-wide v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    .line 2415
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2416
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTermsOrig:Z

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    .line 2417
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v0, :cond_0

    .line 2418
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->rewind()V

    .line 2419
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->numFollowFloorBlocks:I

    .line 2420
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextFloorLabel:I

    .line 2456
    :cond_0
    return-void
.end method

.method public scanToFloorFrame(Lorg/apache/lucene/util/BytesRef;)V
    .locals 12
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2514
    iget-boolean v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v5, :cond_0

    iget v5, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    if-gt v5, v8, :cond_1

    .line 2577
    :cond_0
    :goto_0
    return-void

    .line 2521
    :cond_1
    iget-object v5, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    add-int/2addr v8, v9

    aget-byte v5, v5, v8

    and-int/lit16 v4, v5, 0xff

    .line 2527
    .local v4, "targetLabel":I
    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextFloorLabel:I

    if-lt v4, v5, :cond_0

    .line 2534
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->numFollowFloorBlocks:I

    if-nez v5, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 2536
    :cond_2
    iget-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    .line 2538
    .local v2, "newFP":J
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v0

    .line 2539
    .local v0, "code":J
    iget-wide v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    ushr-long v10, v0, v6

    add-long v2, v8, v10

    .line 2540
    const-wide/16 v8, 0x1

    and-long/2addr v8, v0

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-eqz v5, :cond_4

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    .line 2545
    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->numFollowFloorBlocks:I

    if-ne v5, v6, :cond_5

    move v5, v6

    :goto_2
    iput-boolean v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    .line 2546
    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->numFollowFloorBlocks:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->numFollowFloorBlocks:I

    .line 2548
    iget-boolean v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    if-eqz v5, :cond_6

    .line 2549
    const/16 v5, 0x100

    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextFloorLabel:I

    .line 2565
    :goto_3
    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    cmp-long v5, v2, v6

    if-eqz v5, :cond_0

    .line 2570
    const/4 v5, -0x1

    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2571
    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    goto :goto_0

    :cond_4
    move v5, v7

    .line 2540
    goto :goto_1

    :cond_5
    move v5, v7

    .line 2545
    goto :goto_2

    .line 2555
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/ByteArrayDataInput;->readByte()B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextFloorLabel:I

    .line 2556
    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextFloorLabel:I

    if-ge v4, v5, :cond_3

    goto :goto_3
.end method

.method public scanToSubBlock(J)V
    .locals 11
    .param p1, "subFP"    # J

    .prologue
    .line 2630
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 2633
    :cond_0
    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    cmp-long v1, v6, p1

    if-nez v1, :cond_1

    .line 2652
    :goto_0
    return-void

    .line 2637
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    cmp-long v1, p1, v6

    if-ltz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fp="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " subFP="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 2638
    :cond_2
    iget-wide v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    sub-long v4, v6, p1

    .line 2641
    .local v4, "targetSubCode":J
    :cond_3
    :goto_1
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-lt v1, v6, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 2642
    :cond_4
    iget v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2643
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v0

    .line 2644
    .local v0, "code":I
    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    if-eqz v1, :cond_5

    move v1, v0

    :goto_2
    invoke-virtual {v6, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->skipBytes(I)V

    .line 2646
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_6

    .line 2647
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v2

    .line 2649
    .local v2, "subCode":J
    cmp-long v1, v4, v2

    if-nez v1, :cond_3

    .line 2651
    iput-wide p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    goto :goto_0

    .line 2644
    .end local v2    # "subCode":J
    :cond_5
    ushr-int/lit8 v1, v0, 0x1

    goto :goto_2

    .line 2655
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v6, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    goto :goto_1
.end method

.method public scanToTerm(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 1
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "exactOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2662
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLeafBlock:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToTermLeaf(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToTermNonLeaf(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public scanToTermLeaf(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 14
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "exactOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2675
    sget-boolean v8, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    iget v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 2677
    :cond_0
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V

    .line 2678
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    .line 2680
    iget v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-ne v8, v9, :cond_2

    .line 2681
    if-eqz p2, :cond_1

    .line 2682
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2684
    :cond_1
    sget-object v8, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 2792
    :goto_0
    return-object v8

    .line 2687
    :cond_2
    sget-boolean v8, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v8, :cond_3

    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefixMatches(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v8

    if-nez v8, :cond_3

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 2692
    :cond_3
    iget v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2694
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    .line 2704
    iget v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    add-int v7, v8, v9

    .line 2705
    .local v7, "termLen":I
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v8}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v8

    iput v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    .line 2706
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    invoke-virtual {v8, v9}, Lorg/apache/lucene/store/ByteArrayDataInput;->skipBytes(I)V

    .line 2708
    iget v9, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v8, v7, :cond_6

    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    :goto_1
    add-int v4, v9, v8

    .line 2709
    .local v4, "targetLimit":I
    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    add-int v5, v8, v9

    .line 2713
    .local v5, "targetPos":I
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    .local v0, "bytePos":I
    move v1, v0

    .end local v0    # "bytePos":I
    .local v1, "bytePos":I
    move v6, v5

    .line 2717
    .end local v5    # "targetPos":I
    .local v6, "targetPos":I
    :goto_2
    if-ge v6, v4, :cond_7

    .line 2718
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "bytePos":I
    .restart local v0    # "bytePos":I
    aget-byte v8, v8, v1

    and-int/lit16 v8, v8, 0xff

    iget-object v9, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6    # "targetPos":I
    .restart local v5    # "targetPos":I
    aget-byte v9, v9, v6

    and-int/lit16 v9, v9, 0xff

    sub-int v2, v8, v9

    .line 2719
    .local v2, "cmp":I
    const/4 v3, 0x0

    .line 2726
    .local v3, "stop":Z
    :goto_3
    if-gez v2, :cond_9

    .line 2730
    iget v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-ne v8, v9, :cond_3

    .line 2731
    if-eqz p2, :cond_4

    .line 2732
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2785
    :cond_4
    if-eqz p2, :cond_5

    .line 2786
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2792
    :cond_5
    sget-object v8, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .end local v0    # "bytePos":I
    .end local v2    # "cmp":I
    .end local v3    # "stop":Z
    .end local v4    # "targetLimit":I
    .end local v5    # "targetPos":I
    :cond_6
    move v8, v7

    .line 2708
    goto :goto_1

    .line 2721
    .restart local v1    # "bytePos":I
    .restart local v4    # "targetLimit":I
    .restart local v6    # "targetPos":I
    :cond_7
    sget-boolean v8, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v8, :cond_8

    if-eq v6, v4, :cond_8

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 2722
    :cond_8
    iget v8, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v2, v7, v8

    .line 2723
    .restart local v2    # "cmp":I
    const/4 v3, 0x1

    .restart local v3    # "stop":Z
    move v0, v1

    .end local v1    # "bytePos":I
    .restart local v0    # "bytePos":I
    move v5, v6

    .end local v6    # "targetPos":I
    .restart local v5    # "targetPos":I
    goto :goto_3

    .line 2739
    :cond_9
    if-lez v2, :cond_c

    .line 2743
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2745
    if-nez p2, :cond_a

    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z
    invoke-static {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 2750
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    const/4 v10, 0x0

    iget-object v11, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v11

    iget-wide v12, v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    invoke-virtual {v9, v10, v12, v13, v7}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$4(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V

    .line 2751
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 2752
    :goto_4
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->next()Z

    move-result v8

    if-nez v8, :cond_b

    .line 2759
    :cond_a
    sget-object v8, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0

    .line 2753
    :cond_b
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v9, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    const/4 v10, 0x0

    iget-object v11, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v11

    iget-wide v12, v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    iget-object v11, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    iget-object v11, v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v11, v11, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v9, v10, v12, v13, v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$4(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V

    .line 2754
    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    goto :goto_4

    .line 2760
    :cond_c
    if-eqz v3, :cond_e

    .line 2767
    sget-boolean v8, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v8, :cond_d

    iget-object v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z
    invoke-static {v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Z

    move-result v8

    if-nez v8, :cond_d

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 2768
    :cond_d
    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2770
    sget-object v8, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_0

    :cond_e
    move v1, v0

    .end local v0    # "bytePos":I
    .restart local v1    # "bytePos":I
    move v6, v5

    .end local v5    # "targetPos":I
    .restart local v6    # "targetPos":I
    goto/16 :goto_2
.end method

.method public scanToTermNonLeaf(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 18
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "exactOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2801
    sget-boolean v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v11, :cond_0

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v12, -0x1

    if-ne v11, v12, :cond_0

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 2803
    :cond_0
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-ne v11, v12, :cond_3

    .line 2804
    if-eqz p2, :cond_1

    .line 2805
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2806
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    const-wide/16 v16, 0x0

    cmp-long v11, v14, v16

    if-nez v11, :cond_2

    const/4 v11, 0x1

    :goto_0
    invoke-static {v12, v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V

    .line 2808
    :cond_1
    sget-object v11, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 2925
    :goto_1
    return-object v11

    .line 2806
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 2811
    :cond_3
    sget-boolean v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v11, :cond_4

    invoke-direct/range {p0 .. p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefixMatches(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v11

    if-nez v11, :cond_4

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 2816
    :cond_4
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 2818
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v5

    .line 2819
    .local v5, "code":I
    ushr-int/lit8 v11, v5, 0x1

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    .line 2828
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    and-int/lit8 v11, v5, 0x1

    if-nez v11, :cond_7

    const/4 v11, 0x1

    :goto_2
    invoke-static {v12, v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V

    .line 2829
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    add-int v10, v11, v12

    .line 2830
    .local v10, "termLen":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    .line 2831
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffix:I

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/ByteArrayDataInput;->skipBytes(I)V

    .line 2832
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2833
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v12, v11, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    add-int/lit8 v12, v12, 0x1

    iput v12, v11, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 2834
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    .line 2840
    :goto_3
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p1

    iget v11, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge v11, v10, :cond_9

    move-object/from16 v0, p1

    iget v11, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    :goto_4
    add-int v7, v12, v11

    .line 2841
    .local v7, "targetLimit":I
    move-object/from16 v0, p1

    iget v11, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    add-int v8, v11, v12

    .line 2845
    .local v8, "targetPos":I
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->startBytePos:I

    .local v2, "bytePos":I
    move v3, v2

    .end local v2    # "bytePos":I
    .local v3, "bytePos":I
    move v9, v8

    .line 2849
    .end local v8    # "targetPos":I
    .local v9, "targetPos":I
    :goto_5
    if-ge v9, v7, :cond_a

    .line 2850
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixBytes:[B

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "bytePos":I
    .restart local v2    # "bytePos":I
    aget-byte v11, v11, v3

    and-int/lit16 v11, v11, 0xff

    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "targetPos":I
    .restart local v8    # "targetPos":I
    aget-byte v12, v12, v9

    and-int/lit16 v12, v12, 0xff

    sub-int v4, v11, v12

    .line 2851
    .local v4, "cmp":I
    const/4 v6, 0x0

    .line 2858
    .local v6, "stop":Z
    :goto_6
    if-gez v4, :cond_c

    .line 2862
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-ne v11, v12, :cond_4

    .line 2863
    if-eqz p2, :cond_5

    .line 2864
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2918
    :cond_5
    if-eqz p2, :cond_6

    .line 2919
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2925
    :cond_6
    sget-object v11, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_1

    .line 2828
    .end local v2    # "bytePos":I
    .end local v4    # "cmp":I
    .end local v6    # "stop":Z
    .end local v7    # "targetLimit":I
    .end local v8    # "targetPos":I
    .end local v10    # "termLen":I
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 2836
    .restart local v10    # "termLen":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->suffixesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v11}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    .line 2837
    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->subCode:J

    sub-long/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    goto :goto_3

    :cond_9
    move v11, v10

    .line 2840
    goto :goto_4

    .line 2853
    .restart local v3    # "bytePos":I
    .restart local v7    # "targetLimit":I
    .restart local v9    # "targetPos":I
    :cond_a
    sget-boolean v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v11, :cond_b

    if-eq v9, v7, :cond_b

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 2854
    :cond_b
    move-object/from16 v0, p1

    iget v11, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v4, v10, v11

    .line 2855
    .restart local v4    # "cmp":I
    const/4 v6, 0x1

    .restart local v6    # "stop":Z
    move v2, v3

    .end local v3    # "bytePos":I
    .restart local v2    # "bytePos":I
    move v8, v9

    .end local v9    # "targetPos":I
    .restart local v8    # "targetPos":I
    goto :goto_6

    .line 2872
    :cond_c
    if-lez v4, :cond_f

    .line 2876
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2878
    if-nez p2, :cond_d

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 2883
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v14}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v14

    iget-wide v14, v14, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    invoke-virtual {v12, v13, v14, v15, v10}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v12

    invoke-static {v11, v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$4(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V

    .line 2884
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 2885
    :goto_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->next()Z

    move-result v11

    if-nez v11, :cond_e

    .line 2892
    :cond_d
    sget-object v11, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_1

    .line 2886
    :cond_e
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v14}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v14

    iget-wide v14, v14, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v12

    invoke-static {v11, v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$4(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V

    .line 2887
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v11

    invoke-virtual {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    goto :goto_7

    .line 2893
    :cond_f
    if-eqz v6, :cond_11

    .line 2900
    sget-boolean v11, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->$assertionsDisabled:Z

    if-nez v11, :cond_10

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->this$2:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z
    invoke-static {v11}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Z

    move-result v11

    if-nez v11, :cond_10

    new-instance v11, Ljava/lang/AssertionError;

    invoke-direct {v11}, Ljava/lang/AssertionError;-><init>()V

    throw v11

    .line 2901
    :cond_10
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fillTerm()V

    .line 2903
    sget-object v11, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_1

    :cond_11
    move v3, v2

    .end local v2    # "bytePos":I
    .restart local v3    # "bytePos":I
    move v9, v8

    .end local v8    # "targetPos":I
    .restart local v9    # "targetPos":I
    goto/16 :goto_5
.end method

.method public setFloorData(Lorg/apache/lucene/store/ByteArrayDataInput;Lorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "in"    # Lorg/apache/lucene/store/ByteArrayDataInput;
    .param p2, "source"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v4, 0x0

    .line 2307
    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p1}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v2

    iget v3, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sub-int/2addr v2, v3

    sub-int v0, v1, v2

    .line 2308
    .local v0, "numBytes":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorData:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 2309
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorData:[B

    .line 2311
    :cond_0
    iget-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    invoke-virtual {p1}, Lorg/apache/lucene/store/ByteArrayDataInput;->getPosition()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorData:[B

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2312
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorData:[B

    invoke-virtual {v1, v2, v4, v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 2313
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVInt()I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->numFollowFloorBlocks:I

    .line 2314
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->floorDataReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextFloorLabel:I

    .line 2318
    return-void
.end method
