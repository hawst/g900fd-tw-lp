.class public abstract Lorg/apache/lucene/codecs/TermVectorsWriter;
.super Ljava/lang/Object;
.source "TermVectorsWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lorg/apache/lucene/codecs/TermVectorsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    return-void
.end method


# virtual methods
.method public abstract abort()V
.end method

.method protected final addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V
    .locals 30
    .param p1, "vectors"    # Lorg/apache/lucene/index/Fields;
    .param p2, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    if-nez p1, :cond_0

    .line 208
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startDocument(I)V

    .line 209
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishDocument()V

    .line 295
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/Fields;->size()I

    move-result v18

    .line 214
    .local v18, "numFields":I
    const/4 v4, -0x1

    move/from16 v0, v18

    if-ne v0, v4, :cond_1

    .line 216
    const/16 v18, 0x0

    .line 217
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 222
    .end local v16    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startDocument(I)V

    .line 224
    const/16 v17, 0x0

    .line 226
    .local v17, "lastFieldName":Ljava/lang/String;
    const/16 v25, 0x0

    .line 227
    .local v25, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v11, 0x0

    .line 229
    .local v11, "docsAndPositionsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    const/4 v13, 0x0

    .line 230
    .local v13, "fieldCount":I
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_2
    :goto_2
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 293
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_12

    move/from16 v0, v18

    if-eq v13, v0, :cond_12

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 218
    .end local v11    # "docsAndPositionsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v13    # "fieldCount":I
    .end local v17    # "lastFieldName":Ljava/lang/String;
    .end local v25    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .restart local v16    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 219
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 230
    .end local v16    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v11    # "docsAndPositionsEnum":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .restart local v13    # "fieldCount":I
    .restart local v17    # "lastFieldName":Ljava/lang/String;
    .restart local v25    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_4
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 231
    .local v14, "fieldName":Ljava/lang/String;
    add-int/lit8 v13, v13, 0x1

    .line 232
    move-object/from16 v0, p2

    iget-object v4, v0, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v4, v14}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v5

    .line 234
    .local v5, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    if-eqz v17, :cond_5

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-gtz v4, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "lastFieldName="

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " fieldName="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v4, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 235
    :cond_5
    move-object/from16 v17, v14

    .line 237
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v24

    .line 238
    .local v24, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v24, :cond_2

    .line 243
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/Terms;->hasPositions()Z

    move-result v7

    .line 244
    .local v7, "hasPositions":Z
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/Terms;->hasOffsets()Z

    move-result v8

    .line 245
    .local v8, "hasOffsets":Z
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/Terms;->hasPayloads()Z

    move-result v9

    .line 246
    .local v9, "hasPayloads":Z
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    if-eqz v9, :cond_6

    if-nez v7, :cond_6

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 248
    :cond_6
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/Terms;->size()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v6, v0

    .line 249
    .local v6, "numTerms":I
    const/4 v4, -0x1

    if-ne v6, v4, :cond_7

    .line 251
    const/4 v6, 0x0

    .line 252
    invoke-virtual/range {v24 .. v25}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v25

    .line 253
    :goto_3
    invoke-virtual/range {v25 .. v25}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    if-nez v4, :cond_8

    :cond_7
    move-object/from16 v4, p0

    .line 258
    invoke-virtual/range {v4 .. v9}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startField(Lorg/apache/lucene/index/FieldInfo;IZZZ)V

    .line 259
    invoke-virtual/range {v24 .. v25}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v25

    .line 261
    const/16 v23, 0x0

    .line 262
    .local v23, "termCount":I
    :goto_4
    invoke-virtual/range {v25 .. v25}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    if-nez v4, :cond_9

    .line 290
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    move/from16 v0, v23

    if-eq v0, v6, :cond_11

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 254
    .end local v23    # "termCount":I
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 263
    .restart local v23    # "termCount":I
    :cond_9
    add-int/lit8 v23, v23, 0x1

    .line 265
    invoke-virtual/range {v25 .. v25}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v15, v0

    .line 267
    .local v15, "freq":I
    invoke-virtual/range {v25 .. v25}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startTerm(Lorg/apache/lucene/util/BytesRef;I)V

    .line 269
    if-nez v7, :cond_a

    if-eqz v8, :cond_e

    .line 270
    :cond_a
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v11}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v11

    .line 271
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    if-nez v11, :cond_b

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 273
    :cond_b
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v10

    .line 274
    .local v10, "docID":I
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_c

    const v4, 0x7fffffff

    if-ne v10, v4, :cond_c

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 275
    :cond_c
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_d

    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v4

    if-eq v4, v15, :cond_d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 277
    :cond_d
    const/16 v21, 0x0

    .local v21, "posUpto":I
    :goto_5
    move/from16 v0, v21

    if-lt v0, v15, :cond_f

    .line 288
    .end local v10    # "docID":I
    .end local v21    # "posUpto":I
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishTerm()V

    goto :goto_4

    .line 278
    .restart local v10    # "docID":I
    .restart local v21    # "posUpto":I
    :cond_f
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v20

    .line 279
    .local v20, "pos":I
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v22

    .line 280
    .local v22, "startOffset":I
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v12

    .line 282
    .local v12, "endOffset":I
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v19

    .line 284
    .local v19, "payload":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v4, Lorg/apache/lucene/codecs/TermVectorsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_10

    if-eqz v7, :cond_10

    if-gez v20, :cond_10

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 285
    :cond_10
    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v22

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v12, v3}, Lorg/apache/lucene/codecs/TermVectorsWriter;->addPosition(IIILorg/apache/lucene/util/BytesRef;)V

    .line 277
    add-int/lit8 v21, v21, 0x1

    goto :goto_5

    .line 291
    .end local v10    # "docID":I
    .end local v12    # "endOffset":I
    .end local v15    # "freq":I
    .end local v19    # "payload":Lorg/apache/lucene/util/BytesRef;
    .end local v20    # "pos":I
    .end local v21    # "posUpto":I
    .end local v22    # "startOffset":I
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishField()V

    goto/16 :goto_2

    .line 294
    .end local v5    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v6    # "numTerms":I
    .end local v7    # "hasPositions":Z
    .end local v8    # "hasOffsets":Z
    .end local v9    # "hasPayloads":Z
    .end local v14    # "fieldName":Ljava/lang/String;
    .end local v23    # "termCount":I
    .end local v24    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishDocument()V

    goto/16 :goto_0
.end method

.method public abstract addPosition(IIILorg/apache/lucene/util/BytesRef;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public addProx(ILorg/apache/lucene/store/DataInput;Lorg/apache/lucene/store/DataInput;)V
    .locals 11
    .param p1, "numProx"    # I
    .param p2, "positions"    # Lorg/apache/lucene/store/DataInput;
    .param p3, "offsets"    # Lorg/apache/lucene/store/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    const/4 v6, 0x0

    .line 127
    .local v6, "position":I
    const/4 v3, 0x0

    .line 128
    .local v3, "lastOffset":I
    const/4 v4, 0x0

    .line 130
    .local v4, "payload":Lorg/apache/lucene/util/BytesRef;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, p1, :cond_0

    .line 169
    return-void

    .line 135
    :cond_0
    if-nez p2, :cond_1

    .line 136
    const/4 v6, -0x1

    .line 137
    const/4 v8, 0x0

    .line 160
    .local v8, "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    if-nez p3, :cond_5

    .line 161
    const/4 v1, -0x1

    .local v1, "endOffset":I
    move v7, v1

    .line 167
    .local v7, "startOffset":I
    :goto_2
    invoke-virtual {p0, v6, v7, v1, v8}, Lorg/apache/lucene/codecs/TermVectorsWriter;->addPosition(IIILorg/apache/lucene/util/BytesRef;)V

    .line 130
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 139
    .end local v1    # "endOffset":I
    .end local v7    # "startOffset":I
    .end local v8    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v0

    .line 140
    .local v0, "code":I
    ushr-int/lit8 v9, v0, 0x1

    add-int/2addr v6, v9

    .line 141
    and-int/lit8 v9, v0, 0x1

    if-eqz v9, :cond_4

    .line 143
    invoke-virtual {p2}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v5

    .line 145
    .local v5, "payloadLength":I
    if-nez v4, :cond_3

    .line 146
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    .end local v4    # "payload":Lorg/apache/lucene/util/BytesRef;
    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 147
    .restart local v4    # "payload":Lorg/apache/lucene/util/BytesRef;
    new-array v9, v5, [B

    iput-object v9, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 152
    :cond_2
    :goto_3
    iget-object v9, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10, v5}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 153
    iput v5, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 154
    move-object v8, v4

    .line 155
    .restart local v8    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .line 148
    .end local v8    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    iget-object v9, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v9, v9

    if-ge v9, v5, :cond_2

    .line 149
    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    goto :goto_3

    .line 156
    .end local v5    # "payloadLength":I
    :cond_4
    const/4 v8, 0x0

    .restart local v8    # "thisPayload":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .line 163
    .end local v0    # "code":I
    :cond_5
    invoke-virtual {p3}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v9

    add-int v7, v3, v9

    .line 164
    .restart local v7    # "startOffset":I
    invoke-virtual {p3}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v9

    add-int v1, v7, v9

    .line 165
    .restart local v1    # "endOffset":I
    move v3, v1

    goto :goto_2
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public finishDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    return-void
.end method

.method public finishField()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    return-void
.end method

.method public finishTerm()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    return-void
.end method

.method public abstract getComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public merge(Lorg/apache/lucene/index/MergeState;)I
    .locals 10
    .param p1, "mergeState"    # Lorg/apache/lucene/index/MergeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "docCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v2, v7, :cond_0

    .line 200
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {p0, v7, v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V

    .line 201
    return v0

    .line 183
    :cond_0
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->readers:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/index/AtomicReader;

    .line 184
    .local v5, "reader":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v4

    .line 185
    .local v4, "maxDoc":I
    invoke-virtual {v5}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v3

    .line 187
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    const/4 v1, 0x0

    .local v1, "docID":I
    :goto_1
    if-lt v1, v4, :cond_1

    .line 182
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 188
    :cond_1
    if-eqz v3, :cond_2

    invoke-interface {v3, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v7

    if-nez v7, :cond_2

    .line 187
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 194
    :cond_2
    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v6

    .line 195
    .local v6, "vectors":Lorg/apache/lucene/index/Fields;
    invoke-virtual {p0, v6, p1}, Lorg/apache/lucene/codecs/TermVectorsWriter;->addAllDocVectors(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/index/MergeState;)V

    .line 196
    add-int/lit8 v0, v0, 0x1

    .line 197
    iget-object v7, p1, Lorg/apache/lucene/index/MergeState;->checkAbort:Lorg/apache/lucene/index/MergeState$CheckAbort;

    const-wide v8, 0x4072c00000000000L    # 300.0

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/index/MergeState$CheckAbort;->work(D)V

    goto :goto_2
.end method

.method public abstract startDocument(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract startField(Lorg/apache/lucene/index/FieldInfo;IZZZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract startTerm(Lorg/apache/lucene/util/BytesRef;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
