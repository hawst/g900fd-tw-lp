.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;
.super Lorg/apache/lucene/index/Terms;
.source "Lucene3xFields.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreTerms"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    const-class v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 0
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 172
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 173
    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 174
    return-void
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;->sortTermsByUnicode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    .line 190
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUTF16Comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_0
.end method

.method public getDocCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    const/4 v0, -0x1

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 206
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2

    .prologue
    .line 201
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 2

    .prologue
    .line 217
    sget-boolean v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 218
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v0

    return v0
.end method

.method public hasPositions()Z
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 3
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    new-instance v0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;-><init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields;Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;)V

    .line 179
    .local v0, "termsEnum":Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTerms;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xFields$PreTermsEnum;->reset(Lorg/apache/lucene/index/FieldInfo;)V

    .line 180
    return-object v0
.end method

.method public size()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    const-wide/16 v0, -0x1

    return-wide v0
.end method
