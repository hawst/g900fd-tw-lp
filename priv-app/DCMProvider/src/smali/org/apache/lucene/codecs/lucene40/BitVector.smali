.class final Lorg/apache/lucene/codecs/lucene40/BitVector;
.super Ljava/lang/Object;
.source "BitVector.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/lucene/util/MutableBits;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final BYTE_COUNTS:[B

.field private static CODEC:Ljava/lang/String; = null

.field public static final VERSION_CURRENT:I = 0x1

.field public static final VERSION_DGAPS_CLEARED:I = 0x1

.field public static final VERSION_PRE:I = -0x1

.field public static final VERSION_START:I


# instance fields
.field private bits:[B

.field private count:I

.field private size:I

.field private version:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v3, 0x4

    .line 45
    const-class v0, Lorg/apache/lucene/codecs/lucene40/BitVector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    .line 187
    const/16 v0, 0x100

    new-array v0, v0, [B

    const/4 v1, 0x1

    .line 188
    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    aput-byte v1, v0, v6

    aput-byte v6, v0, v4

    const/4 v1, 0x1

    aput-byte v1, v0, v3

    aput-byte v6, v0, v5

    aput-byte v6, v0, v7

    const/4 v1, 0x7

    aput-byte v4, v0, v1

    const/16 v1, 0x8

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/16 v1, 0x9

    aput-byte v6, v0, v1

    const/16 v1, 0xa

    aput-byte v6, v0, v1

    const/16 v1, 0xb

    aput-byte v4, v0, v1

    const/16 v1, 0xc

    aput-byte v6, v0, v1

    const/16 v1, 0xd

    aput-byte v4, v0, v1

    const/16 v1, 0xe

    aput-byte v4, v0, v1

    const/16 v1, 0xf

    aput-byte v3, v0, v1

    const/16 v1, 0x10

    .line 189
    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/16 v1, 0x11

    aput-byte v6, v0, v1

    const/16 v1, 0x12

    aput-byte v6, v0, v1

    const/16 v1, 0x13

    aput-byte v4, v0, v1

    const/16 v1, 0x14

    aput-byte v6, v0, v1

    const/16 v1, 0x15

    aput-byte v4, v0, v1

    const/16 v1, 0x16

    aput-byte v4, v0, v1

    const/16 v1, 0x17

    aput-byte v3, v0, v1

    const/16 v1, 0x18

    aput-byte v6, v0, v1

    const/16 v1, 0x19

    aput-byte v4, v0, v1

    const/16 v1, 0x1a

    aput-byte v4, v0, v1

    const/16 v1, 0x1b

    aput-byte v3, v0, v1

    const/16 v1, 0x1c

    aput-byte v4, v0, v1

    const/16 v1, 0x1d

    aput-byte v3, v0, v1

    const/16 v1, 0x1e

    aput-byte v3, v0, v1

    const/16 v1, 0x1f

    aput-byte v5, v0, v1

    const/16 v1, 0x20

    .line 190
    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/16 v1, 0x21

    aput-byte v6, v0, v1

    const/16 v1, 0x22

    aput-byte v6, v0, v1

    const/16 v1, 0x23

    aput-byte v4, v0, v1

    const/16 v1, 0x24

    aput-byte v6, v0, v1

    const/16 v1, 0x25

    aput-byte v4, v0, v1

    const/16 v1, 0x26

    aput-byte v4, v0, v1

    const/16 v1, 0x27

    aput-byte v3, v0, v1

    const/16 v1, 0x28

    aput-byte v6, v0, v1

    const/16 v1, 0x29

    aput-byte v4, v0, v1

    const/16 v1, 0x2a

    aput-byte v4, v0, v1

    const/16 v1, 0x2b

    aput-byte v3, v0, v1

    const/16 v1, 0x2c

    aput-byte v4, v0, v1

    const/16 v1, 0x2d

    aput-byte v3, v0, v1

    const/16 v1, 0x2e

    aput-byte v3, v0, v1

    const/16 v1, 0x2f

    aput-byte v5, v0, v1

    const/16 v1, 0x30

    .line 191
    aput-byte v6, v0, v1

    const/16 v1, 0x31

    aput-byte v4, v0, v1

    const/16 v1, 0x32

    aput-byte v4, v0, v1

    const/16 v1, 0x33

    aput-byte v3, v0, v1

    const/16 v1, 0x34

    aput-byte v4, v0, v1

    const/16 v1, 0x35

    aput-byte v3, v0, v1

    const/16 v1, 0x36

    aput-byte v3, v0, v1

    const/16 v1, 0x37

    aput-byte v5, v0, v1

    const/16 v1, 0x38

    aput-byte v4, v0, v1

    const/16 v1, 0x39

    aput-byte v3, v0, v1

    const/16 v1, 0x3a

    aput-byte v3, v0, v1

    const/16 v1, 0x3b

    aput-byte v5, v0, v1

    const/16 v1, 0x3c

    aput-byte v3, v0, v1

    const/16 v1, 0x3d

    aput-byte v5, v0, v1

    const/16 v1, 0x3e

    aput-byte v5, v0, v1

    const/16 v1, 0x3f

    aput-byte v7, v0, v1

    const/16 v1, 0x40

    .line 192
    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/16 v1, 0x41

    aput-byte v6, v0, v1

    const/16 v1, 0x42

    aput-byte v6, v0, v1

    const/16 v1, 0x43

    aput-byte v4, v0, v1

    const/16 v1, 0x44

    aput-byte v6, v0, v1

    const/16 v1, 0x45

    aput-byte v4, v0, v1

    const/16 v1, 0x46

    aput-byte v4, v0, v1

    const/16 v1, 0x47

    aput-byte v3, v0, v1

    const/16 v1, 0x48

    aput-byte v6, v0, v1

    const/16 v1, 0x49

    aput-byte v4, v0, v1

    const/16 v1, 0x4a

    aput-byte v4, v0, v1

    const/16 v1, 0x4b

    aput-byte v3, v0, v1

    const/16 v1, 0x4c

    aput-byte v4, v0, v1

    const/16 v1, 0x4d

    aput-byte v3, v0, v1

    const/16 v1, 0x4e

    aput-byte v3, v0, v1

    const/16 v1, 0x4f

    aput-byte v5, v0, v1

    const/16 v1, 0x50

    .line 193
    aput-byte v6, v0, v1

    const/16 v1, 0x51

    aput-byte v4, v0, v1

    const/16 v1, 0x52

    aput-byte v4, v0, v1

    const/16 v1, 0x53

    aput-byte v3, v0, v1

    const/16 v1, 0x54

    aput-byte v4, v0, v1

    const/16 v1, 0x55

    aput-byte v3, v0, v1

    const/16 v1, 0x56

    aput-byte v3, v0, v1

    const/16 v1, 0x57

    aput-byte v5, v0, v1

    const/16 v1, 0x58

    aput-byte v4, v0, v1

    const/16 v1, 0x59

    aput-byte v3, v0, v1

    const/16 v1, 0x5a

    aput-byte v3, v0, v1

    const/16 v1, 0x5b

    aput-byte v5, v0, v1

    const/16 v1, 0x5c

    aput-byte v3, v0, v1

    const/16 v1, 0x5d

    aput-byte v5, v0, v1

    const/16 v1, 0x5e

    aput-byte v5, v0, v1

    const/16 v1, 0x5f

    aput-byte v7, v0, v1

    const/16 v1, 0x60

    .line 194
    aput-byte v6, v0, v1

    const/16 v1, 0x61

    aput-byte v4, v0, v1

    const/16 v1, 0x62

    aput-byte v4, v0, v1

    const/16 v1, 0x63

    aput-byte v3, v0, v1

    const/16 v1, 0x64

    aput-byte v4, v0, v1

    const/16 v1, 0x65

    aput-byte v3, v0, v1

    const/16 v1, 0x66

    aput-byte v3, v0, v1

    const/16 v1, 0x67

    aput-byte v5, v0, v1

    const/16 v1, 0x68

    aput-byte v4, v0, v1

    const/16 v1, 0x69

    aput-byte v3, v0, v1

    const/16 v1, 0x6a

    aput-byte v3, v0, v1

    const/16 v1, 0x6b

    aput-byte v5, v0, v1

    const/16 v1, 0x6c

    aput-byte v3, v0, v1

    const/16 v1, 0x6d

    aput-byte v5, v0, v1

    const/16 v1, 0x6e

    aput-byte v5, v0, v1

    const/16 v1, 0x6f

    aput-byte v7, v0, v1

    const/16 v1, 0x70

    .line 195
    aput-byte v4, v0, v1

    const/16 v1, 0x71

    aput-byte v3, v0, v1

    const/16 v1, 0x72

    aput-byte v3, v0, v1

    const/16 v1, 0x73

    aput-byte v5, v0, v1

    const/16 v1, 0x74

    aput-byte v3, v0, v1

    const/16 v1, 0x75

    aput-byte v5, v0, v1

    const/16 v1, 0x76

    aput-byte v5, v0, v1

    const/16 v1, 0x77

    aput-byte v7, v0, v1

    const/16 v1, 0x78

    aput-byte v3, v0, v1

    const/16 v1, 0x79

    aput-byte v5, v0, v1

    const/16 v1, 0x7a

    aput-byte v5, v0, v1

    const/16 v1, 0x7b

    aput-byte v7, v0, v1

    const/16 v1, 0x7c

    aput-byte v5, v0, v1

    const/16 v1, 0x7d

    aput-byte v7, v0, v1

    const/16 v1, 0x7e

    aput-byte v7, v0, v1

    const/16 v1, 0x7f

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0x80

    .line 196
    const/4 v2, 0x1

    aput-byte v2, v0, v1

    const/16 v1, 0x81

    aput-byte v6, v0, v1

    const/16 v1, 0x82

    aput-byte v6, v0, v1

    const/16 v1, 0x83

    aput-byte v4, v0, v1

    const/16 v1, 0x84

    aput-byte v6, v0, v1

    const/16 v1, 0x85

    aput-byte v4, v0, v1

    const/16 v1, 0x86

    aput-byte v4, v0, v1

    const/16 v1, 0x87

    aput-byte v3, v0, v1

    const/16 v1, 0x88

    aput-byte v6, v0, v1

    const/16 v1, 0x89

    aput-byte v4, v0, v1

    const/16 v1, 0x8a

    aput-byte v4, v0, v1

    const/16 v1, 0x8b

    aput-byte v3, v0, v1

    const/16 v1, 0x8c

    aput-byte v4, v0, v1

    const/16 v1, 0x8d

    aput-byte v3, v0, v1

    const/16 v1, 0x8e

    aput-byte v3, v0, v1

    const/16 v1, 0x8f

    aput-byte v5, v0, v1

    const/16 v1, 0x90

    .line 197
    aput-byte v6, v0, v1

    const/16 v1, 0x91

    aput-byte v4, v0, v1

    const/16 v1, 0x92

    aput-byte v4, v0, v1

    const/16 v1, 0x93

    aput-byte v3, v0, v1

    const/16 v1, 0x94

    aput-byte v4, v0, v1

    const/16 v1, 0x95

    aput-byte v3, v0, v1

    const/16 v1, 0x96

    aput-byte v3, v0, v1

    const/16 v1, 0x97

    aput-byte v5, v0, v1

    const/16 v1, 0x98

    aput-byte v4, v0, v1

    const/16 v1, 0x99

    aput-byte v3, v0, v1

    const/16 v1, 0x9a

    aput-byte v3, v0, v1

    const/16 v1, 0x9b

    aput-byte v5, v0, v1

    const/16 v1, 0x9c

    aput-byte v3, v0, v1

    const/16 v1, 0x9d

    aput-byte v5, v0, v1

    const/16 v1, 0x9e

    aput-byte v5, v0, v1

    const/16 v1, 0x9f

    aput-byte v7, v0, v1

    const/16 v1, 0xa0

    .line 198
    aput-byte v6, v0, v1

    const/16 v1, 0xa1

    aput-byte v4, v0, v1

    const/16 v1, 0xa2

    aput-byte v4, v0, v1

    const/16 v1, 0xa3

    aput-byte v3, v0, v1

    const/16 v1, 0xa4

    aput-byte v4, v0, v1

    const/16 v1, 0xa5

    aput-byte v3, v0, v1

    const/16 v1, 0xa6

    aput-byte v3, v0, v1

    const/16 v1, 0xa7

    aput-byte v5, v0, v1

    const/16 v1, 0xa8

    aput-byte v4, v0, v1

    const/16 v1, 0xa9

    aput-byte v3, v0, v1

    const/16 v1, 0xaa

    aput-byte v3, v0, v1

    const/16 v1, 0xab

    aput-byte v5, v0, v1

    const/16 v1, 0xac

    aput-byte v3, v0, v1

    const/16 v1, 0xad

    aput-byte v5, v0, v1

    const/16 v1, 0xae

    aput-byte v5, v0, v1

    const/16 v1, 0xaf

    aput-byte v7, v0, v1

    const/16 v1, 0xb0

    .line 199
    aput-byte v4, v0, v1

    const/16 v1, 0xb1

    aput-byte v3, v0, v1

    const/16 v1, 0xb2

    aput-byte v3, v0, v1

    const/16 v1, 0xb3

    aput-byte v5, v0, v1

    const/16 v1, 0xb4

    aput-byte v3, v0, v1

    const/16 v1, 0xb5

    aput-byte v5, v0, v1

    const/16 v1, 0xb6

    aput-byte v5, v0, v1

    const/16 v1, 0xb7

    aput-byte v7, v0, v1

    const/16 v1, 0xb8

    aput-byte v3, v0, v1

    const/16 v1, 0xb9

    aput-byte v5, v0, v1

    const/16 v1, 0xba

    aput-byte v5, v0, v1

    const/16 v1, 0xbb

    aput-byte v7, v0, v1

    const/16 v1, 0xbc

    aput-byte v5, v0, v1

    const/16 v1, 0xbd

    aput-byte v7, v0, v1

    const/16 v1, 0xbe

    aput-byte v7, v0, v1

    const/16 v1, 0xbf

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xc0

    .line 200
    aput-byte v6, v0, v1

    const/16 v1, 0xc1

    aput-byte v4, v0, v1

    const/16 v1, 0xc2

    aput-byte v4, v0, v1

    const/16 v1, 0xc3

    aput-byte v3, v0, v1

    const/16 v1, 0xc4

    aput-byte v4, v0, v1

    const/16 v1, 0xc5

    aput-byte v3, v0, v1

    const/16 v1, 0xc6

    aput-byte v3, v0, v1

    const/16 v1, 0xc7

    aput-byte v5, v0, v1

    const/16 v1, 0xc8

    aput-byte v4, v0, v1

    const/16 v1, 0xc9

    aput-byte v3, v0, v1

    const/16 v1, 0xca

    aput-byte v3, v0, v1

    const/16 v1, 0xcb

    aput-byte v5, v0, v1

    const/16 v1, 0xcc

    aput-byte v3, v0, v1

    const/16 v1, 0xcd

    aput-byte v5, v0, v1

    const/16 v1, 0xce

    aput-byte v5, v0, v1

    const/16 v1, 0xcf

    aput-byte v7, v0, v1

    const/16 v1, 0xd0

    .line 201
    aput-byte v4, v0, v1

    const/16 v1, 0xd1

    aput-byte v3, v0, v1

    const/16 v1, 0xd2

    aput-byte v3, v0, v1

    const/16 v1, 0xd3

    aput-byte v5, v0, v1

    const/16 v1, 0xd4

    aput-byte v3, v0, v1

    const/16 v1, 0xd5

    aput-byte v5, v0, v1

    const/16 v1, 0xd6

    aput-byte v5, v0, v1

    const/16 v1, 0xd7

    aput-byte v7, v0, v1

    const/16 v1, 0xd8

    aput-byte v3, v0, v1

    const/16 v1, 0xd9

    aput-byte v5, v0, v1

    const/16 v1, 0xda

    aput-byte v5, v0, v1

    const/16 v1, 0xdb

    aput-byte v7, v0, v1

    const/16 v1, 0xdc

    aput-byte v5, v0, v1

    const/16 v1, 0xdd

    aput-byte v7, v0, v1

    const/16 v1, 0xde

    aput-byte v7, v0, v1

    const/16 v1, 0xdf

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xe0

    .line 202
    aput-byte v4, v0, v1

    const/16 v1, 0xe1

    aput-byte v3, v0, v1

    const/16 v1, 0xe2

    aput-byte v3, v0, v1

    const/16 v1, 0xe3

    aput-byte v5, v0, v1

    const/16 v1, 0xe4

    aput-byte v3, v0, v1

    const/16 v1, 0xe5

    aput-byte v5, v0, v1

    const/16 v1, 0xe6

    aput-byte v5, v0, v1

    const/16 v1, 0xe7

    aput-byte v7, v0, v1

    const/16 v1, 0xe8

    aput-byte v3, v0, v1

    const/16 v1, 0xe9

    aput-byte v5, v0, v1

    const/16 v1, 0xea

    aput-byte v5, v0, v1

    const/16 v1, 0xeb

    aput-byte v7, v0, v1

    const/16 v1, 0xec

    aput-byte v5, v0, v1

    const/16 v1, 0xed

    aput-byte v7, v0, v1

    const/16 v1, 0xee

    aput-byte v7, v0, v1

    const/16 v1, 0xef

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xf0

    .line 203
    aput-byte v3, v0, v1

    const/16 v1, 0xf1

    aput-byte v5, v0, v1

    const/16 v1, 0xf2

    aput-byte v5, v0, v1

    const/16 v1, 0xf3

    aput-byte v7, v0, v1

    const/16 v1, 0xf4

    aput-byte v5, v0, v1

    const/16 v1, 0xf5

    aput-byte v7, v0, v1

    const/16 v1, 0xf6

    aput-byte v7, v0, v1

    const/16 v1, 0xf7

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xf8

    aput-byte v5, v0, v1

    const/16 v1, 0xf9

    aput-byte v7, v0, v1

    const/16 v1, 0xfa

    aput-byte v7, v0, v1

    const/16 v1, 0xfb

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xfc

    aput-byte v7, v0, v1

    const/16 v1, 0xfd

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xfe

    const/4 v2, 0x7

    aput-byte v2, v0, v1

    const/16 v1, 0xff

    const/16 v2, 0x8

    aput-byte v2, v0, v1

    .line 187
    sput-object v0, Lorg/apache/lucene/codecs/lucene40/BitVector;->BYTE_COUNTS:[B

    .line 206
    const-string v0, "BitVector"

    sput-object v0, Lorg/apache/lucene/codecs/lucene40/BitVector;->CODEC:Ljava/lang/String;

    .line 219
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    .line 55
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->getNumBytes(I)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 57
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 7
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    invoke-virtual {p1, p2, p3}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 346
    .local v1, "input":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    .line 348
    .local v0, "firstInt":I
    const/4 v2, -0x2

    if-ne v0, v2, :cond_1

    .line 350
    sget-object v2, Lorg/apache/lucene/codecs/lucene40/BitVector;->CODEC:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->version:I

    .line 351
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    .line 356
    :goto_0
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-ne v2, v6, :cond_3

    .line 357
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->version:I

    if-lt v2, v5, :cond_2

    .line 358
    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->readClearedDgaps(Lorg/apache/lucene/store/IndexInput;)V

    .line 366
    :goto_1
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->version:I

    if-ge v2, v5, :cond_0

    .line 367
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->invertAll()V

    .line 370
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v2, :cond_4

    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->verifyCount()Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    .end local v0    # "firstInt":I
    :catchall_0
    move-exception v2

    .line 372
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 373
    throw v2

    .line 353
    .restart local v0    # "firstInt":I
    :cond_1
    const/4 v2, -0x1

    :try_start_1
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->version:I

    .line 354
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    goto :goto_0

    .line 360
    :cond_2
    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->readSetDgaps(Lorg/apache/lucene/store/IndexInput;)V

    goto :goto_1

    .line 363
    :cond_3
    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/lucene40/BitVector;->readBits(Lorg/apache/lucene/store/IndexInput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 372
    :cond_4
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 374
    return-void
.end method

.method constructor <init>([BI)V
    .locals 1
    .param p1, "bits"    # [B
    .param p2, "size"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    .line 61
    iput p2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 63
    return-void
.end method

.method private clearUnusedBits()V
    .locals 5

    .prologue
    .line 262
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v2, v2

    if-lez v2, :cond_0

    .line 263
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    and-int/lit8 v0, v2, 0x7

    .line 264
    .local v0, "lastNBits":I
    if-eqz v0, :cond_0

    .line 265
    const/4 v2, 0x1

    shl-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x1

    .line 266
    .local v1, "mask":I
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-byte v4, v2, v3

    and-int/2addr v4, v1

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 269
    .end local v0    # "lastNBits":I
    .end local v1    # "mask":I
    :cond_0
    return-void
.end method

.method private getNumBytes(I)I
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 66
    ushr-int/lit8 v0, p1, 0x3

    .line 67
    .local v0, "bytesLength":I
    and-int/lit8 v1, p1, 0x7

    if-eqz v1, :cond_0

    .line 68
    add-int/lit8 v0, v0, 0x1

    .line 70
    :cond_0
    return v0
.end method

.method private isSparse()Z
    .locals 14

    .prologue
    const/4 v8, 0x1

    .line 306
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->size()I

    move-result v9

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v10

    sub-int v2, v9, v10

    .line 307
    .local v2, "clearedCount":I
    if-nez v2, :cond_1

    .line 336
    :cond_0
    :goto_0
    return v8

    .line 311
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v9, v9

    div-int v0, v9, v2

    .line 315
    .local v0, "avgGapLength":I
    const/16 v9, 0x80

    if-gt v0, v9, :cond_2

    .line 316
    const/4 v3, 0x1

    .line 329
    .local v3, "expectedDGapBytes":I
    :goto_1
    add-int/lit8 v1, v3, 0x1

    .line 332
    .local v1, "bytesPerSetBit":I
    mul-int/lit8 v9, v1, 0x8

    mul-int/2addr v9, v2

    add-int/lit8 v9, v9, 0x20

    int-to-long v4, v9

    .line 335
    .local v4, "expectedBits":J
    const-wide/16 v6, 0xa

    .line 336
    .local v6, "factor":J
    const-wide/16 v10, 0xa

    mul-long/2addr v10, v4

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->size()I

    move-result v9

    int-to-long v12, v9

    cmp-long v9, v10, v12

    if-ltz v9, :cond_0

    const/4 v8, 0x0

    goto :goto_0

    .line 317
    .end local v1    # "bytesPerSetBit":I
    .end local v3    # "expectedDGapBytes":I
    .end local v4    # "expectedBits":J
    .end local v6    # "factor":J
    :cond_2
    const/16 v9, 0x4000

    if-gt v0, v9, :cond_3

    .line 318
    const/4 v3, 0x2

    .line 319
    .restart local v3    # "expectedDGapBytes":I
    goto :goto_1

    .end local v3    # "expectedDGapBytes":I
    :cond_3
    const/high16 v9, 0x200000

    if-gt v0, v9, :cond_4

    .line 320
    const/4 v3, 0x3

    .line 321
    .restart local v3    # "expectedDGapBytes":I
    goto :goto_1

    .end local v3    # "expectedDGapBytes":I
    :cond_4
    const/high16 v9, 0x10000000

    if-gt v0, v9, :cond_5

    .line 322
    const/4 v3, 0x4

    .line 323
    .restart local v3    # "expectedDGapBytes":I
    goto :goto_1

    .line 324
    .end local v3    # "expectedDGapBytes":I
    :cond_5
    const/4 v3, 0x5

    .restart local v3    # "expectedDGapBytes":I
    goto :goto_1
.end method

.method private readBits(Lorg/apache/lucene/store/IndexInput;)V
    .locals 3
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 388
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->getNumBytes(I)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    .line 389
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v2, v2

    invoke-virtual {p1, v0, v1, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 390
    return-void
.end method

.method private readClearedDgaps(Lorg/apache/lucene/store/IndexInput;)V
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 409
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    .line 410
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 411
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/lucene40/BitVector;->getNumBytes(I)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    .line 412
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    const/4 v3, -0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 413
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->clearUnusedBits()V

    .line 414
    const/4 v0, 0x0

    .line 415
    .local v0, "last":I
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->size()I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v3

    sub-int v1, v2, v3

    .line 416
    .local v1, "numCleared":I
    :cond_0
    if-gtz v1, :cond_1

    .line 422
    return-void

    .line 417
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    add-int/2addr v0, v2

    .line 418
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    aput-byte v3, v2, v0

    .line 419
    sget-object v2, Lorg/apache/lucene/codecs/lucene40/BitVector;->BYTE_COUNTS:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    aget-byte v2, v2, v3

    rsub-int/lit8 v2, v2, 0x8

    sub-int/2addr v1, v2

    .line 420
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez v1, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_2

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    and-int/lit8 v2, v2, 0x7

    rsub-int/lit8 v2, v2, 0x8

    neg-int v2, v2

    if-eq v1, v2, :cond_0

    :cond_2
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
.end method

.method private readSetDgaps(Lorg/apache/lucene/store/IndexInput;)V
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 394
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    .line 395
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 396
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/lucene40/BitVector;->getNumBytes(I)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    .line 397
    const/4 v0, 0x0

    .line 398
    .local v0, "last":I
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v1

    .line 399
    .local v1, "n":I
    :cond_0
    if-gtz v1, :cond_1

    .line 405
    return-void

    .line 400
    :cond_1
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    add-int/2addr v0, v2

    .line 401
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v3

    aput-byte v3, v2, v0

    .line 402
    sget-object v2, Lorg/apache/lucene/codecs/lucene40/BitVector;->BYTE_COUNTS:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    aget-byte v2, v2, v3

    sub-int/2addr v1, v2

    .line 403
    sget-boolean v2, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    if-gez v1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
.end method

.method private verifyCount()Z
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 378
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 379
    :cond_0
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 380
    .local v0, "countSav":I
    iput v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 381
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v1

    if-eq v0, v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "saved count was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but recomputed count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 382
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method private writeBits(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 2
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 281
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 282
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v1, v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 283
    return-void
.end method

.method private writeClearedDgaps(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 6
    .param p1, "output"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 287
    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 288
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->size()I

    move-result v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 289
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v3

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 290
    const/4 v1, 0x0

    .line 291
    .local v1, "last":I
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->size()I

    move-result v3

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->count()I

    move-result v4

    sub-int v2, v3, v4

    .line 292
    .local v2, "numCleared":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    if-gtz v2, :cond_1

    .line 301
    :cond_0
    return-void

    .line 293
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v3, v3, v0

    if-eq v3, v5, :cond_3

    .line 294
    sub-int v3, v0, v1

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 295
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v3, v3, v0

    invoke-virtual {p1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 296
    move v1, v0

    .line 297
    sget-object v3, Lorg/apache/lucene/codecs/lucene40/BitVector;->BYTE_COUNTS:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v4, v4, v0

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    rsub-int/lit8 v3, v3, 0x8

    sub-int/2addr v2, v3

    .line 298
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    if-gez v2, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_2

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    and-int/lit8 v3, v3, 0x7

    rsub-int/lit8 v3, v3, 0x8

    neg-int v3, v3

    if-eq v2, v3, :cond_3

    :cond_2
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 292
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final clear(I)V
    .locals 5
    .param p1, "bit"    # I

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-lt p1, v0, :cond_0

    .line 116
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v0

    .line 118
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    shr-int/lit8 v1, p1, 0x3

    aget-byte v2, v0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x7

    shl-int/2addr v3, v4

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 120
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->clone()Lorg/apache/lucene/codecs/lucene40/BitVector;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/codecs/lucene40/BitVector;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 75
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v2, v2

    new-array v1, v2, [B

    .line 76
    .local v1, "copyBits":[B
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/BitVector;

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/codecs/lucene40/BitVector;-><init>([BI)V

    .line 78
    .local v0, "clone":Lorg/apache/lucene/codecs/lucene40/BitVector;
    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    iput v2, v0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 79
    return-object v0
.end method

.method public final count()I
    .locals 6

    .prologue
    .line 165
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 166
    const/4 v0, 0x0

    .line 167
    .local v0, "c":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v1, v3

    .line 168
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_1

    .line 171
    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 173
    .end local v0    # "c":I
    .end local v1    # "end":I
    .end local v2    # "i":I
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-le v3, v4, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "count="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 169
    .restart local v0    # "c":I
    .restart local v1    # "end":I
    .restart local v2    # "i":I
    :cond_1
    sget-object v3, Lorg/apache/lucene/codecs/lucene40/BitVector;->BYTE_COUNTS:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v4, v4, v2

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    add-int/2addr v0, v3

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 174
    .end local v0    # "c":I
    .end local v1    # "end":I
    .end local v2    # "i":I
    :cond_2
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    return v3
.end method

.method public final get(I)Z
    .locals 3
    .param p1, "bit"    # I

    .prologue
    const/4 v0, 0x1

    .line 145
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    if-ltz p1, :cond_0

    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-lt p1, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds 0.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 146
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    shr-int/lit8 v2, p1, 0x3

    aget-byte v1, v1, v2

    and-int/lit8 v2, p1, 0x7

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getAndClear(I)Z
    .locals 7
    .param p1, "bit"    # I

    .prologue
    const/4 v3, 0x1

    .line 123
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-lt p1, v4, :cond_0

    .line 124
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v3, p1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(I)V

    throw v3

    .line 126
    :cond_0
    shr-int/lit8 v1, p1, 0x3

    .line 127
    .local v1, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v2, v4, v1

    .line 128
    .local v2, "v":I
    and-int/lit8 v4, p1, 0x7

    shl-int v0, v3, v4

    .line 129
    .local v0, "flag":I
    and-int v4, v0, v2

    if-nez v4, :cond_2

    .line 130
    const/4 v3, 0x0

    .line 137
    :cond_1
    return v3

    .line 132
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v5, v4, v1

    xor-int/lit8 v6, v0, -0x1

    and-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 133
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 134
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 135
    sget-boolean v4, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    if-gez v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
.end method

.method public final getAndSet(I)Z
    .locals 6
    .param p1, "bit"    # I

    .prologue
    const/4 v3, 0x1

    .line 94
    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-lt p1, v4, :cond_0

    .line 95
    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "bit="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 97
    :cond_0
    shr-int/lit8 v1, p1, 0x3

    .line 98
    .local v1, "pos":I
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v2, v4, v1

    .line 99
    .local v2, "v":I
    and-int/lit8 v4, p1, 0x7

    shl-int v0, v3, v4

    .line 100
    .local v0, "flag":I
    and-int v4, v0, v2

    if-eqz v4, :cond_1

    .line 108
    :goto_0
    return v3

    .line 103
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    or-int v4, v2, v0

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    .line 104
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 105
    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 106
    sget-boolean v3, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    iget v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-le v3, v4, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 108
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final getRecomputedCount()I
    .locals 5

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "c":I
    iget-object v3, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v1, v3

    .line 181
    .local v1, "end":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 184
    return v0

    .line 182
    :cond_0
    sget-object v3, Lorg/apache/lucene/codecs/lucene40/BitVector;->BYTE_COUNTS:[B

    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v4, v4, v2

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    add-int/2addr v0, v3

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->version:I

    return v0
.end method

.method public invertAll()V
    .locals 3

    .prologue
    .line 248
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 249
    iget v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 251
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v1, v1

    if-lez v1, :cond_1

    .line 252
    const/4 v0, 0x0

    .local v0, "idx":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 255
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->clearUnusedBits()V

    .line 257
    .end local v0    # "idx":I
    :cond_1
    return-void

    .line 253
    .restart local v0    # "idx":I
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    aget-byte v2, v2, v0

    xor-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    return v0
.end method

.method public final set(I)V
    .locals 5
    .param p1, "bit"    # I

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    if-lt p1, v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bit="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    shr-int/lit8 v1, p1, 0x3

    aget-byte v2, v0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x7

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 89
    return-void
.end method

.method public setAll()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->bits:[B

    const/4 v1, -0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 274
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->clearUnusedBits()V

    .line 275
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->count:I

    .line 276
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lorg/apache/lucene/codecs/lucene40/BitVector;->size:I

    return v0
.end method

.method public final write(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 5
    .param p1, "d"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 229
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    instance-of v1, p1, Lorg/apache/lucene/store/CompoundFileDirectory;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 230
    :cond_0
    invoke-virtual {p1, p2, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    .line 232
    .local v0, "output":Lorg/apache/lucene/store/IndexOutput;
    const/4 v1, -0x2

    :try_start_0
    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 233
    sget-object v1, Lorg/apache/lucene/codecs/lucene40/BitVector;->CODEC:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 234
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->isSparse()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->writeClearedDgaps(Lorg/apache/lucene/store/IndexOutput;)V

    .line 240
    :goto_0
    sget-boolean v1, Lorg/apache/lucene/codecs/lucene40/BitVector;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->verifyCount()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    :catchall_0
    move-exception v1

    new-array v2, v3, [Ljava/io/Closeable;

    .line 242
    aput-object v0, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 243
    throw v1

    .line 238
    :cond_1
    :try_start_1
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/lucene40/BitVector;->writeBits(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 241
    :cond_2
    new-array v1, v3, [Ljava/io/Closeable;

    .line 242
    aput-object v0, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 244
    return-void
.end method
