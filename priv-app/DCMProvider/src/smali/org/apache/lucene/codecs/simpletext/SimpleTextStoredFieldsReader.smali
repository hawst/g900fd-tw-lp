.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;
.super Lorg/apache/lucene/codecs/StoredFieldsReader;
.source "SimpleTextStoredFieldsReader.java"


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field private in:Lorg/apache/lucene/store/IndexInput;

.field private offsets:[J

.field private scratch:Lorg/apache/lucene/util/BytesRef;

.field private scratchUTF16:Lorg/apache/lucene/util/CharsRef;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I
    .locals 3

    .prologue
    .line 47
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->values()[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-virtual {v1}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/store/IOContext;)V
    .locals 4
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p3, "fn"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 50
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v1, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 55
    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 56
    const/4 v0, 0x0

    .line 58
    .local v0, "success":Z
    :try_start_0
    iget-object v1, p2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v2, ""

    const-string v3, "fld"

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    const/4 v0, 0x1

    .line 61
    if-nez v0, :cond_0

    .line 63
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 67
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readIndex(I)V

    .line 68
    return-void

    .line 60
    :catchall_0
    move-exception v1

    .line 61
    if-nez v0, :cond_1

    .line 63
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 66
    :cond_1
    :goto_1
    throw v1

    .line 64
    :catch_0
    move-exception v2

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method constructor <init>([JLorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V
    .locals 1
    .param p1, "offsets"    # [J
    .param p2, "in"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;

    .prologue
    .line 71
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsReader;-><init>()V

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/CharsRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/CharsRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    .line 72
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    .line 73
    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 74
    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 75
    return-void
.end method

.method private equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z
    .locals 5
    .param p1, "a"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "b"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "bOffset"    # I

    .prologue
    .line 192
    iget v0, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v1, p3

    if-ne v0, v1, :cond_0

    .line 193
    iget-object v0, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v1, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget-object v2, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v3, p3

    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v4, p3

    invoke-static {v0, v1, v2, v3, v4}, Lorg/apache/lucene/util/ArrayUtil;->equals([BI[BII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseIntAt(I)I
    .locals 4
    .param p1, "offset"    # I

    .prologue
    .line 187
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v0, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v1, p1

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v2, p1

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 188
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget-object v0, v0, Lorg/apache/lucene/util/CharsRef;->chars:[C

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    iget v2, v2, Lorg/apache/lucene/util/CharsRef;->length:I

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/util/ArrayUtil;->parseInt([CII)I

    move-result v0

    return v0
.end method

.method private readField(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 6
    .param p1, "type"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 142
    sget-boolean v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 143
    :cond_0
    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_STRING:Lorg/apache/lucene/util/BytesRef;

    if-ne p1, v1, :cond_2

    .line 144
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v2, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v4, v5

    const-string v5, "UTF-8"

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {p3, p2, v1}, Lorg/apache/lucene/index/StoredFieldVisitor;->stringField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/String;)V

    .line 162
    :cond_1
    :goto_0
    return-void

    .line 145
    :cond_2
    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_BINARY:Lorg/apache/lucene/util/BytesRef;

    if-ne p1, v1, :cond_3

    .line 146
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v1, v1, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v1, v2

    new-array v0, v1, [B

    .line 147
    .local v0, "copy":[B
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 148
    invoke-virtual {p3, p2, v0}, Lorg/apache/lucene/index/StoredFieldVisitor;->binaryField(Lorg/apache/lucene/index/FieldInfo;[B)V

    goto :goto_0

    .line 149
    .end local v0    # "copy":[B
    :cond_3
    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_INT:Lorg/apache/lucene/util/BytesRef;

    if-ne p1, v1, :cond_4

    .line 150
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v1, v2, v3, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p3, p2, v1}, Lorg/apache/lucene/index/StoredFieldVisitor;->intField(Lorg/apache/lucene/index/FieldInfo;I)V

    goto :goto_0

    .line 152
    :cond_4
    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_LONG:Lorg/apache/lucene/util/BytesRef;

    if-ne p1, v1, :cond_5

    .line 153
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v1, v2, v3, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 154
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p3, p2, v2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->longField(Lorg/apache/lucene/index/FieldInfo;J)V

    goto :goto_0

    .line 155
    :cond_5
    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_FLOAT:Lorg/apache/lucene/util/BytesRef;

    if-ne p1, v1, :cond_6

    .line 156
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v1, v2, v3, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 157
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p3, p2, v1}, Lorg/apache/lucene/index/StoredFieldVisitor;->floatField(Lorg/apache/lucene/index/FieldInfo;F)V

    goto/16 :goto_0

    .line 158
    :cond_6
    sget-object v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_DOUBLE:Lorg/apache/lucene/util/BytesRef;

    if-ne p1, v1, :cond_1

    .line 159
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, v1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v2, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    iget v4, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-static {v1, v2, v3, v4}, Lorg/apache/lucene/util/UnicodeUtil;->UTF8toUTF16([BIILorg/apache/lucene/util/CharsRef;)V

    .line 160
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratchUTF16:Lorg/apache/lucene/util/CharsRef;

    invoke-virtual {v1}, Lorg/apache/lucene/util/CharsRef;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {p3, p2, v2, v3}, Lorg/apache/lucene/index/StoredFieldVisitor;->doubleField(Lorg/apache/lucene/index/FieldInfo;D)V

    goto/16 :goto_0
.end method

.method private readIndex(I)V
    .locals 4
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    new-array v1, p1, [J

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    .line 82
    const/4 v0, 0x0

    .line 83
    .local v0, "upto":I
    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    sget-boolean v1, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    array-length v1, v1

    if-eq v0, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 84
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 85
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v1, v2}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_2
    return-void
.end method

.method private readLine()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 184
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/codecs/StoredFieldsReader;
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this FieldsReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    new-instance v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;-><init>([JLorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfos;)V

    return-object v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 174
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 175
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 178
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    .line 180
    return-void

    .line 176
    :catchall_0
    move-exception v0

    .line 177
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 178
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    .line 179
    throw v0
.end method

.method public visitDocument(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 8
    .param p1, "n"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->offsets:[J

    aget-wide v6, v6, p1

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 97
    sget-boolean v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NUM:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 98
    :cond_0
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NUM:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->parseIntAt(I)I

    move-result v3

    .line 100
    .local v3, "numFields":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 138
    :pswitch_0
    return-void

    .line 101
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 102
    sget-boolean v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 103
    :cond_2
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->parseIntAt(I)I

    move-result v1

    .line 104
    .local v1, "fieldNumber":I
    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v5, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v0

    .line 105
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 106
    sget-boolean v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 107
    :cond_3
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 108
    sget-boolean v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 111
    :cond_4
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_STRING:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 112
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_STRING:Lorg/apache/lucene/util/BytesRef;

    .line 127
    .local v4, "type":Lorg/apache/lucene/util/BytesRef;
    :goto_1
    invoke-static {}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$SWITCH_TABLE$org$apache$lucene$index$StoredFieldVisitor$Status()[I

    move-result-object v5

    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/StoredFieldVisitor;->needsField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 100
    :cond_5
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    .end local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :cond_6
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_BINARY:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 114
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_BINARY:Lorg/apache/lucene/util/BytesRef;

    .line 115
    .restart local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .end local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :cond_7
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_INT:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 116
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_INT:Lorg/apache/lucene/util/BytesRef;

    .line 117
    .restart local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .end local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :cond_8
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_LONG:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 118
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_LONG:Lorg/apache/lucene/util/BytesRef;

    .line 119
    .restart local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .end local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :cond_9
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_FLOAT:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 120
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_FLOAT:Lorg/apache/lucene/util/BytesRef;

    .line 121
    .restart local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .end local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :cond_a
    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_DOUBLE:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v7, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {p0, v5, v6, v7}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->equalsAt(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;I)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 122
    sget-object v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_DOUBLE:Lorg/apache/lucene/util/BytesRef;

    .line 123
    .restart local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    goto :goto_1

    .line 124
    .end local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :cond_b
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "unknown field type"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 129
    .restart local v4    # "type":Lorg/apache/lucene/util/BytesRef;
    :pswitch_1
    invoke-direct {p0, v4, v0, p2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readField(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/StoredFieldVisitor;)V

    goto :goto_2

    .line 132
    :pswitch_2
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->readLine()V

    .line 133
    sget-boolean v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsReader;->scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v5, v6}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v5

    if-nez v5, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
