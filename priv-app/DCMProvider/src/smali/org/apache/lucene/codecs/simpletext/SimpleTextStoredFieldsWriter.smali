.class public Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;
.super Lorg/apache/lucene/codecs/StoredFieldsWriter;
.source "SimpleTextStoredFieldsWriter.java"


# static fields
.field static final DOC:Lorg/apache/lucene/util/BytesRef;

.field static final END:Lorg/apache/lucene/util/BytesRef;

.field static final FIELD:Lorg/apache/lucene/util/BytesRef;

.field static final FIELDS_EXTENSION:Ljava/lang/String; = "fld"

.field static final NAME:Lorg/apache/lucene/util/BytesRef;

.field static final NUM:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE_BINARY:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE_DOUBLE:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE_FLOAT:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE_INT:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE_LONG:Lorg/apache/lucene/util/BytesRef;

.field static final TYPE_STRING:Lorg/apache/lucene/util/BytesRef;

.field static final VALUE:Lorg/apache/lucene/util/BytesRef;


# instance fields
.field private final directory:Lorg/apache/lucene/store/Directory;

.field private numDocsWritten:I

.field private out:Lorg/apache/lucene/store/IndexOutput;

.field private final scratch:Lorg/apache/lucene/util/BytesRef;

.field private final segment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "string"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_STRING:Lorg/apache/lucene/util/BytesRef;

    .line 48
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "binary"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_BINARY:Lorg/apache/lucene/util/BytesRef;

    .line 49
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "int"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_INT:Lorg/apache/lucene/util/BytesRef;

    .line 50
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "long"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_LONG:Lorg/apache/lucene/util/BytesRef;

    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "float"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_FLOAT:Lorg/apache/lucene/util/BytesRef;

    .line 52
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "double"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_DOUBLE:Lorg/apache/lucene/util/BytesRef;

    .line 54
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "END"

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    .line 55
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "doc "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    .line 56
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  numfields "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NUM:Lorg/apache/lucene/util/BytesRef;

    .line 57
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "  field "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    .line 58
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    name "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    .line 59
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    type "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    .line 60
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const-string v1, "    value "

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    sput-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 3
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segment"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Lorg/apache/lucene/codecs/StoredFieldsWriter;-><init>()V

    .line 40
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->numDocsWritten:I

    .line 62
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 65
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 66
    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->segment:Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    .line 69
    .local v0, "success":Z
    :try_start_0
    const-string v1, ""

    const-string v2, "fld"

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, p3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    const/4 v0, 0x1

    .line 72
    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->abort()V

    .line 76
    :cond_0
    return-void

    .line 71
    :catchall_0
    move-exception v1

    .line 72
    if-nez v0, :cond_1

    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->abort()V

    .line 75
    :cond_1
    throw v1
.end method

.method private newLine()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->writeNewline(Lorg/apache/lucene/store/DataOutput;)V

    .line 195
    return-void
.end method

.method private write(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 187
    return-void
.end method

.method private write(Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-static {v0, p1}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->write(Lorg/apache/lucene/store/DataOutput;Lorg/apache/lucene/util/BytesRef;)V

    .line 191
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 6

    .prologue
    .line 161
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->segment:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "fld"

    invoke-static {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    .line 164
    return-void

    .line 162
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 178
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 179
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 183
    return-void

    .line 180
    :catchall_0
    move-exception v0

    .line 181
    iput-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    .line 182
    throw v0
.end method

.method public finish(Lorg/apache/lucene/index/FieldInfos;I)V
    .locals 3
    .param p1, "fis"    # Lorg/apache/lucene/index/FieldInfos;
    .param p2, "numDocs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->numDocsWritten:I

    if-eq v0, p2, :cond_0

    .line 169
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mergeFields produced an invalid result: docCount is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 170
    const-string v2, " but only saw "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->numDocsWritten:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " file="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->out:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; now aborting this merge to prevent index corruption"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 169
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->END:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 173
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 174
    return-void
.end method

.method public startDocument(I)V
    .locals 1
    .param p1, "numStoredFields"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->DOC:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 81
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->numDocsWritten:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 84
    sget-object v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NUM:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 85
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 88
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->numDocsWritten:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->numDocsWritten:I

    .line 89
    return-void
.end method

.method public writeField(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/IndexableField;)V
    .locals 5
    .param p1, "info"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "field"    # Lorg/apache/lucene/index/IndexableField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->FIELD:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 94
    iget v2, p1, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 95
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 97
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->NAME:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 98
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 99
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 101
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 102
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->numericValue()Ljava/lang/Number;

    move-result-object v1

    .line 104
    .local v1, "n":Ljava/lang/Number;
    if-eqz v1, :cond_5

    .line 105
    instance-of v2, v1, Ljava/lang/Byte;

    if-nez v2, :cond_0

    instance-of v2, v1, Ljava/lang/Short;

    if-nez v2, :cond_0

    instance-of v2, v1, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 106
    :cond_0
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_INT:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 107
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 109
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 110
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 111
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 156
    :goto_0
    return-void

    .line 112
    :cond_1
    instance-of v2, v1, Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 113
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_LONG:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 114
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 116
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 117
    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    goto :goto_0

    .line 119
    :cond_2
    instance-of v2, v1, Ljava/lang/Float;

    if-eqz v2, :cond_3

    .line 120
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_FLOAT:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 121
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 123
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 124
    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 125
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    goto :goto_0

    .line 126
    :cond_3
    instance-of v2, v1, Ljava/lang/Double;

    if-eqz v2, :cond_4

    .line 127
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_DOUBLE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 128
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 130
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 131
    invoke-virtual {v1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 132
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    goto :goto_0

    .line 134
    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot store numeric type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 137
    :cond_5
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->binaryValue()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 138
    .local v0, "bytes":Lorg/apache/lucene/util/BytesRef;
    if-eqz v0, :cond_6

    .line 139
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_BINARY:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 140
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 142
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 143
    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 144
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    goto/16 :goto_0

    .line 145
    :cond_6
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    .line 146
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "field "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is stored but does not have binaryValue, stringValue nor numericValue"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 148
    :cond_7
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->TYPE_STRING:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 149
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    .line 151
    sget-object v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->VALUE:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Lorg/apache/lucene/util/BytesRef;)V

    .line 152
    invoke-interface {p2}, Lorg/apache/lucene/index/IndexableField;->stringValue()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->write(Ljava/lang/String;)V

    .line 153
    invoke-direct {p0}, Lorg/apache/lucene/codecs/simpletext/SimpleTextStoredFieldsWriter;->newLine()V

    goto/16 :goto_0
.end method
