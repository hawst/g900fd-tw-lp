.class public final Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesFormat;
.super Lorg/apache/lucene/codecs/DocValuesFormat;
.source "Lucene42DocValuesFormat.java"


# static fields
.field private static final DATA_CODEC:Ljava/lang/String; = "Lucene42DocValuesData"

.field private static final DATA_EXTENSION:Ljava/lang/String; = "dvd"

.field private static final METADATA_CODEC:Ljava/lang/String; = "Lucene42DocValuesMetadata"

.field private static final METADATA_EXTENSION:Ljava/lang/String; = "dvm"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    const-string v0, "Lucene42"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/DocValuesFormat;-><init>(Ljava/lang/String;)V

    .line 123
    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 7
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;

    const-string v2, "Lucene42DocValuesData"

    const-string v3, "dvd"

    const-string v4, "Lucene42DocValuesMetadata"

    const-string v5, "dvm"

    const v6, 0x3e4ccccd    # 0.2f

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesConsumer;-><init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V

    return-object v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 6
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;

    const-string v2, "Lucene42DocValuesData"

    const-string v3, "dvd"

    const-string v4, "Lucene42DocValuesMetadata"

    const-string v5, "dvm"

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/codecs/lucene42/Lucene42DocValuesProducer;-><init>(Lorg/apache/lucene/index/SegmentReadState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
