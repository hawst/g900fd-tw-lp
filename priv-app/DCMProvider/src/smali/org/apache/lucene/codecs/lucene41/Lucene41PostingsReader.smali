.class public final Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;
.super Lorg/apache/lucene/codecs/PostingsReaderBase;
.source "Lucene41PostingsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;,
        Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;,
        Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;,
        Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    }
.end annotation


# instance fields
.field private final docIn:Lorg/apache/lucene/store/IndexInput;

.field private final forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

.field private final payIn:Lorg/apache/lucene/store/IndexInput;

.field private final posIn:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;)V
    .locals 7
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "ioContext"    # Lorg/apache/lucene/store/IOContext;
    .param p5, "segmentSuffix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/codecs/PostingsReaderBase;-><init>()V

    .line 67
    const/4 v3, 0x0

    .line 68
    .local v3, "success":Z
    const/4 v0, 0x0

    .line 69
    .local v0, "docIn":Lorg/apache/lucene/store/IndexInput;
    const/4 v2, 0x0

    .line 70
    .local v2, "posIn":Lorg/apache/lucene/store/IndexInput;
    const/4 v1, 0x0

    .line 72
    .local v1, "payIn":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    iget-object v4, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v5, "doc"

    invoke-static {v4, p5, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 75
    const-string v4, "Lucene41PostingsWriterDoc"

    .line 76
    const/4 v5, 0x0

    .line 77
    const/4 v6, 0x0

    .line 74
    invoke-static {v0, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 78
    new-instance v4, Lorg/apache/lucene/codecs/lucene41/ForUtil;

    invoke-direct {v4, v0}, Lorg/apache/lucene/codecs/lucene41/ForUtil;-><init>(Lorg/apache/lucene/store/DataInput;)V

    iput-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    .line 80
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfos;->hasProx()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 81
    iget-object v4, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v5, "pos"

    invoke-static {v4, p5, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    .line 84
    const-string v4, "Lucene41PostingsWriterPos"

    .line 85
    const/4 v5, 0x0

    .line 86
    const/4 v6, 0x0

    .line 83
    invoke-static {v2, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 88
    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfos;->hasPayloads()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p2}, Lorg/apache/lucene/index/FieldInfos;->hasOffsets()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 89
    :cond_0
    iget-object v4, p3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v5, "pay"

    invoke-static {v4, p5, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 92
    const-string v4, "Lucene41PostingsWriterPay"

    .line 93
    const/4 v5, 0x0

    .line 94
    const/4 v6, 0x0

    .line 91
    invoke-static {v1, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 98
    :cond_1
    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;

    .line 99
    iput-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->posIn:Lorg/apache/lucene/store/IndexInput;

    .line 100
    iput-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->payIn:Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    const/4 v3, 0x1

    .line 103
    if-nez v3, :cond_2

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 104
    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    const/4 v5, 0x2

    aput-object v1, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 107
    :cond_2
    return-void

    .line 102
    :catchall_0
    move-exception v4

    .line 103
    if-nez v3, :cond_3

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 104
    aput-object v0, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    const/4 v6, 0x2

    aput-object v1, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 106
    :cond_3
    throw v4
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/codecs/lucene41/ForUtil;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->forUtil:Lorg/apache/lucene/codecs/lucene41/ForUtil;

    return-object v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->posIn:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->payIn:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static readVIntBlock(Lorg/apache/lucene/store/IndexInput;[I[IIZ)V
    .locals 3
    .param p0, "docIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p1, "docBuffer"    # [I
    .param p2, "freqBuffer"    # [I
    .param p3, "num"    # I
    .param p4, "indexHasFreq"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    if-eqz p4, :cond_3

    .line 128
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, p3, :cond_1

    .line 142
    :cond_0
    return-void

    .line 129
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 130
    .local v0, "code":I
    ushr-int/lit8 v2, v0, 0x1

    aput v2, p1, v1

    .line 131
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_2

    .line 132
    const/4 v2, 0x1

    aput v2, p2, v1

    .line 128
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, p2, v1

    goto :goto_1

    .line 138
    .end local v0    # "code":I
    .end local v1    # "i":I
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p3, :cond_0

    .line 139
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v2

    aput v2, p1, v1

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 197
    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->posIn:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->payIn:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    return-void
.end method

.method public docs(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 2
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p5, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    instance-of v1, p4, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;

    if-eqz v1, :cond_1

    move-object v0, p4

    .line 286
    check-cast v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;

    .line 287
    .local v0, "docsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;
    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 288
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;

    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;
    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V

    .line 293
    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;
    :cond_0
    :goto_0
    check-cast p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-virtual {v0, p3, p2, p5}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;->reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v1

    return-object v1

    .line 291
    .end local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;
    .restart local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    :cond_1
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V

    .restart local v0    # "docsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsEnum;
    goto :goto_0
.end method

.method public docsAndPositions(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 6
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .param p3, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p4, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p5, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v4

    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v4, v5}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-ltz v4, :cond_3

    const/4 v2, 0x1

    .line 304
    .local v2, "indexHasOffsets":Z
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v3

    .line 306
    .local v3, "indexHasPayloads":Z
    if-eqz v2, :cond_0

    and-int/lit8 v4, p5, 0x1

    if-nez v4, :cond_5

    .line 307
    :cond_0
    if-eqz v3, :cond_1

    and-int/lit8 v4, p5, 0x2

    if-nez v4, :cond_5

    .line 309
    :cond_1
    instance-of v4, p4, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;

    if-eqz v4, :cond_4

    move-object v0, p4

    .line 310
    check-cast v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;

    .line 311
    .local v0, "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, v4, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 312
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;

    .end local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V

    .line 317
    .restart local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
    :cond_2
    :goto_1
    check-cast p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-virtual {v0, p3, p2}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;->reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v4

    .line 328
    .end local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
    :goto_2
    return-object v4

    .line 303
    .end local v2    # "indexHasOffsets":Z
    .end local v3    # "indexHasPayloads":Z
    .restart local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 315
    .restart local v2    # "indexHasOffsets":Z
    .restart local v3    # "indexHasPayloads":Z
    :cond_4
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V

    .restart local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
    goto :goto_1

    .line 320
    .end local v0    # "docsAndPositionsEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$BlockDocsAndPositionsEnum;
    :cond_5
    instance-of v4, p4, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;

    if-eqz v4, :cond_7

    move-object v1, p4

    .line 321
    check-cast v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;

    .line 322
    .local v1, "everythingEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->docIn:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1, v4, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->canReuse(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 323
    new-instance v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;

    .end local v1    # "everythingEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
    invoke-direct {v1, p0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V

    .line 328
    .restart local v1    # "everythingEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
    :cond_6
    :goto_3
    check-cast p2, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    .end local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    invoke-virtual {v1, p3, p2, p5}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;->reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;I)Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;

    move-result-object v4

    goto :goto_2

    .line 326
    .end local v1    # "everythingEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
    .restart local p2    # "termState":Lorg/apache/lucene/codecs/BlockTermState;
    :cond_7
    new-instance v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;

    invoke-direct {v1, p0, p1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;Lorg/apache/lucene/index/FieldInfo;)V

    .restart local v1    # "everythingEnum":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$EverythingEnum;
    goto :goto_3
.end method

.method public init(Lorg/apache/lucene/store/IndexInput;)V
    .locals 5
    .param p1, "termsIn"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x80

    const/4 v2, 0x0

    .line 112
    .line 113
    const-string v1, "Lucene41PostingsWriterTerms"

    .line 112
    invoke-static {p1, v1, v2, v2}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 116
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 117
    .local v0, "indexBlockSize":I
    if-eq v0, v4, :cond_0

    .line 118
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "index-time BLOCK_SIZE ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") != read-time BLOCK_SIZE ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 120
    :cond_0
    return-void
.end method

.method public bridge synthetic newTermState()Lorg/apache/lucene/codecs/BlockTermState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader;->newTermState()Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    move-result-object v0

    return-object v0
.end method

.method public newTermState()Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    .locals 2

    .prologue
    .line 192
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;-><init>(Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;)V

    return-object v0
.end method

.method public nextTerm(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V
    .locals 12
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "_termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    move-object v7, p2

    check-cast v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    .line 223
    .local v7, "termState":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    iget v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->termBlockOrd:I

    if-nez v8, :cond_2

    const/4 v6, 0x1

    .line 224
    .local v6, "isFirstTerm":Z
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-ltz v8, :cond_3

    const/4 v4, 0x1

    .line 225
    .local v4, "fieldHasPositions":Z
    :goto_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v8

    sget-object v9, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v8, v9}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v8

    if-ltz v8, :cond_4

    const/4 v2, 0x1

    .line 226
    .local v2, "fieldHasOffsets":Z
    :goto_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v3

    .line 228
    .local v3, "fieldHasPayloads":Z
    iget-object v5, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 229
    .local v5, "in":Lorg/apache/lucene/store/DataInput;
    if-eqz v6, :cond_8

    .line 230
    iget v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docFreq:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    .line 231
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v8

    iput v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    .line 232
    const-wide/16 v8, 0x0

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    .line 237
    :goto_3
    if-eqz v4, :cond_1

    .line 238
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    .line 239
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    const-wide/16 v10, 0x80

    cmp-long v8, v8, v10

    if-lez v8, :cond_6

    .line 240
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    .line 244
    :goto_4
    if-nez v3, :cond_0

    if-eqz v2, :cond_7

    :cond_0
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    const-wide/16 v10, 0x80

    cmp-long v8, v8, v10

    if-ltz v8, :cond_7

    .line 245
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    .line 275
    :cond_1
    :goto_5
    iget v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docFreq:I

    const/16 v9, 0x80

    if-le v8, v9, :cond_d

    .line 276
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    .line 280
    :goto_6
    return-void

    .line 223
    .end local v2    # "fieldHasOffsets":Z
    .end local v3    # "fieldHasPayloads":Z
    .end local v4    # "fieldHasPositions":Z
    .end local v5    # "in":Lorg/apache/lucene/store/DataInput;
    .end local v6    # "isFirstTerm":Z
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 224
    .restart local v6    # "isFirstTerm":Z
    :cond_3
    const/4 v4, 0x0

    goto :goto_1

    .line 225
    .restart local v4    # "fieldHasPositions":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 234
    .restart local v2    # "fieldHasOffsets":Z
    .restart local v3    # "fieldHasPayloads":Z
    .restart local v5    # "in":Lorg/apache/lucene/store/DataInput;
    :cond_5
    const/4 v8, -0x1

    iput v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    .line 235
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    goto :goto_3

    .line 242
    :cond_6
    const-wide/16 v8, -0x1

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    goto :goto_4

    .line 247
    :cond_7
    const-wide/16 v8, -0x1

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    goto :goto_5

    .line 251
    :cond_8
    iget v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docFreq:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 252
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v8

    iput v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    .line 257
    :goto_7
    if-eqz v4, :cond_1

    .line 258
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v10

    add-long/2addr v8, v10

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->posStartFP:J

    .line 259
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    const-wide/16 v10, 0x80

    cmp-long v8, v8, v10

    if-lez v8, :cond_b

    .line 260
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v8

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    .line 264
    :goto_8
    if-nez v3, :cond_9

    if-eqz v2, :cond_1

    :cond_9
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->totalTermFreq:J

    const-wide/16 v10, 0x80

    cmp-long v8, v8, v10

    if-ltz v8, :cond_1

    .line 265
    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v0

    .line 266
    .local v0, "delta":J
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-nez v8, :cond_c

    .line 267
    iput-wide v0, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    goto :goto_5

    .line 254
    .end local v0    # "delta":J
    :cond_a
    const/4 v8, -0x1

    iput v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->singletonDocID:I

    .line 255
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    invoke-virtual {v5}, Lorg/apache/lucene/store/DataInput;->readVLong()J

    move-result-wide v10

    add-long/2addr v8, v10

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->docStartFP:J

    goto :goto_7

    .line 262
    :cond_b
    const-wide/16 v8, -0x1

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->lastPosBlockOffset:J

    goto :goto_8

    .line 269
    .restart local v0    # "delta":J
    :cond_c
    iget-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    add-long/2addr v8, v0

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->payStartFP:J

    goto :goto_5

    .line 278
    .end local v0    # "delta":J
    :cond_d
    const-wide/16 v8, -0x1

    iput-wide v8, v7, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->skipOffset:J

    goto :goto_6
.end method

.method public readTermsBlock(Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;)V
    .locals 5
    .param p1, "termsIn"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "_termState"    # Lorg/apache/lucene/codecs/BlockTermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 204
    move-object v1, p3

    check-cast v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;

    .line 206
    .local v1, "termState":Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 208
    .local v0, "numBytes":I
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytes:[B

    if-nez v2, :cond_1

    .line 209
    invoke-static {v0, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytes:[B

    .line 210
    new-instance v2, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 215
    :cond_0
    :goto_0
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytes:[B

    invoke-virtual {p1, v2, v4, v0}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 216
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytesReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v3, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytes:[B

    invoke-virtual {v2, v3, v4, v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 217
    return-void

    .line 211
    :cond_1
    iget-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytes:[B

    array-length v2, v2

    if-ge v2, v0, :cond_0

    .line 212
    invoke-static {v0, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v1, Lorg/apache/lucene/codecs/lucene41/Lucene41PostingsReader$IntBlockTermState;->bytes:[B

    goto :goto_0
.end method
