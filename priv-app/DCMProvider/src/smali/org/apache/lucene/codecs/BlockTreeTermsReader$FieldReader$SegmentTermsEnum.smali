.class final Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SegmentTermsEnum"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private arcs:[Lorg/apache/lucene/util/fst/FST$Arc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

.field private eof:Z

.field private final fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

.field private in:Lorg/apache/lucene/store/IndexInput;

.field private final scratchReader:Lorg/apache/lucene/store/ByteArrayDataInput;

.field private stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

.field private final staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

.field private targetBeforeCurrentLength:I

.field final term:Lorg/apache/lucene/util/BytesRef;

.field private termExists:Z

.field final synthetic this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

.field private validIndexPrefix:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1246
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1270
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 1256
    new-instance v2, Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-direct {v2}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->scratchReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    .line 1264
    new-instance v2, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v2}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 1268
    const/4 v2, 0x1

    new-array v2, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1272
    new-array v2, v4, [Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1275
    new-instance v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v3, -0x1

    invoke-direct {v2, p0, v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;I)V

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1277
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1278
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    .line 1285
    :goto_0
    const/4 v1, 0x0

    .local v1, "arcIdx":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 1289
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1291
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1292
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 1294
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1280
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v1    # "arcIdx":I
    :cond_0
    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {p1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/util/fst/FST;->getBytesReader()Lorg/apache/lucene/util/fst/FST$BytesReader;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    goto :goto_0

    .line 1286
    .restart local v1    # "arcIdx":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    new-instance v3, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v3}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    aput-object v3, v2, v1

    .line 1285
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1296
    :cond_2
    const/4 v0, 0x0

    .line 1298
    .restart local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1301
    iput v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1309
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 1247
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Z)V
    .locals 0

    .prologue
    .line 1252
    iput-boolean p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    return-void
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Z
    .locals 1

    .prologue
    .line 1252
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    return v0
.end method

.method static synthetic access$3(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .locals 1

    .prologue
    .line 1251
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    return-object v0
.end method

.method static synthetic access$4(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V
    .locals 0

    .prologue
    .line 1251
    iput-object p1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    return-void
.end method

.method static synthetic access$5(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    .locals 1

    .prologue
    .line 1246
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    return-object v0
.end method

.method private clearEOF()Z
    .locals 1

    .prologue
    .line 1491
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    .line 1492
    const/4 v0, 0x1

    return v0
.end method

.method private getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;
    .locals 5
    .param p1, "ord"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1423
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v2, v2

    if-lt p1, v2, :cond_0

    .line 1425
    add-int/lit8 v2, p1, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v1, v2, [Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1426
    .local v1, "next":[Lorg/apache/lucene/util/fst/FST$Arc;
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v3, v3

    invoke-static {v2, v4, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1427
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    array-length v0, v2

    .local v0, "arcOrd":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_1

    .line 1430
    iput-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1432
    .end local v0    # "arcOrd":I
    .end local v1    # "next":[Lorg/apache/lucene/util/fst/FST$Arc;
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v2, v2, p1

    return-object v2

    .line 1428
    .restart local v0    # "arcOrd":I
    .restart local v1    # "next":[Lorg/apache/lucene/util/fst/FST$Arc;
    :cond_1
    new-instance v2, Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-direct {v2}, Lorg/apache/lucene/util/fst/FST$Arc;-><init>()V

    aput-object v2, v1, v0

    .line 1427
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .locals 5
    .param p1, "ord"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1410
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    array-length v2, v2

    if-lt p1, v2, :cond_0

    .line 1411
    add-int/lit8 v2, p1, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v2

    new-array v0, v2, [Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1412
    .local v0, "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    array-length v3, v3

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1413
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    array-length v1, v2

    .local v1, "stackOrd":I
    :goto_0
    array-length v2, v0

    if-lt v1, v2, :cond_1

    .line 1416
    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1418
    .end local v0    # "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .end local v1    # "stackOrd":I
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    aget-object v2, v2, p1

    iget v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    if-eq v2, p1, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1414
    .restart local v0    # "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .restart local v1    # "stackOrd":I
    :cond_1
    new-instance v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-direct {v2, p0, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;I)V

    aput-object v2, v0, v1

    .line 1413
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1419
    .end local v0    # "next":[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .end local v1    # "stackOrd":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    aget-object v2, v2, p1

    return-object v2
.end method

.method private printSeekState(Ljava/io/PrintStream;)V
    .locals 18
    .param p1, "out"    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2017
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    if-ne v12, v13, :cond_1

    .line 2018
    const-string v12, "  no prior seek"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2061
    :cond_0
    return-void

    .line 2020
    :cond_1
    const-string v12, "  prior seek state:"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2021
    const/4 v8, 0x0

    .line 2022
    .local v8, "ord":I
    const/4 v7, 0x1

    .line 2024
    .local v7, "isSeekFrame":Z
    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->getFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v6

    .line 2025
    .local v6, "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    if-nez v6, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 2026
    :cond_2
    new-instance v10, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v13, 0x0

    iget v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    invoke-direct {v10, v12, v13, v14}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    .line 2027
    .local v10, "prefix":Lorg/apache/lucene/util/BytesRef;
    iget v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v13, -0x1

    if-ne v12, v13, :cond_8

    .line 2028
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v12, "    frame "

    invoke-direct {v13, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v7, :cond_3

    const-string v12, "(seek)"

    :goto_1
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ord="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " fp="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v12, :cond_4

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v14, " (fpOrig="

    invoke-direct {v12, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, ")"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_2
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " prefixLen="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " prefix="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v14, -0x1

    if-ne v12, v14, :cond_5

    const-string v12, ""

    :goto_3
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " hasTerms="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " isFloor="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " code="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    const/4 v12, 0x2

    shl-long/2addr v14, v12

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    if-eqz v12, :cond_6

    const/4 v12, 0x2

    :goto_4
    int-to-long v0, v12

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v12, :cond_7

    const/4 v12, 0x1

    :goto_5
    int-to-long v0, v12

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " isLastInFloor="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " mdUpto="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " tbOrd="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->getTermBlockOrd()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2032
    :goto_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    if-eqz v12, :cond_13

    .line 2033
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_e

    if-eqz v7, :cond_e

    iget-object v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    if-nez v12, :cond_e

    new-instance v12, Ljava/lang/AssertionError;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "isSeekFrame="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " f.arc="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 2028
    :cond_3
    const-string v12, "(next)"

    goto/16 :goto_1

    :cond_4
    const-string v12, ""

    goto/16 :goto_2

    :cond_5
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v14, " (of "

    invoke-direct {v12, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, ")"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_3

    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_5

    .line 2030
    :cond_8
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v12, "    frame "

    invoke-direct {v13, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v7, :cond_9

    const-string v12, "(seek, loaded)"

    :goto_7
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ord="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " fp="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v12, :cond_a

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v14, " (fpOrig="

    invoke-direct {v12, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, ")"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    :goto_8
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " prefixLen="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " prefix="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " nextEnt="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    iget v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v14, -0x1

    if-ne v12, v14, :cond_b

    const-string v12, ""

    :goto_9
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " hasTerms="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " isFloor="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " code="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    const/4 v12, 0x2

    shl-long/2addr v14, v12

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    if-eqz v12, :cond_c

    const/4 v12, 0x2

    :goto_a
    int-to-long v0, v12

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v12, :cond_d

    const/4 v12, 0x1

    :goto_b
    int-to-long v0, v12

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " lastSubFP="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " isLastInFloor="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " mdUpto="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " tbOrd="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v6}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->getTermBlockOrd()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_9
    const-string v12, "(next, loaded)"

    goto/16 :goto_7

    :cond_a
    const-string v12, ""

    goto/16 :goto_8

    :cond_b
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v14, " (of "

    invoke-direct {v12, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, ")"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_9

    :cond_c
    const/4 v12, 0x0

    goto :goto_a

    :cond_d
    const/4 v12, 0x0

    goto :goto_b

    .line 2034
    :cond_e
    iget v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    if-lez v12, :cond_f

    if-eqz v7, :cond_f

    iget-object v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v12, v12, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v13, v13, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    add-int/lit8 v14, v14, -0x1

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    if-eq v12, v13, :cond_f

    .line 2035
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "      broken seek state: arc.label="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v13, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    iget v13, v13, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    int-to-char v13, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " vs term byte="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v13, v13, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v14, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    add-int/lit8 v14, v14, -0x1

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    int-to-char v13, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2036
    new-instance v12, Ljava/lang/RuntimeException;

    const-string v13, "seek state is broken"

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 2038
    :cond_f
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    invoke-static {v12, v10}, Lorg/apache/lucene/util/fst/Util;->get(Lorg/apache/lucene/util/fst/FST;Lorg/apache/lucene/util/BytesRef;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/util/BytesRef;

    .line 2039
    .local v9, "output":Lorg/apache/lucene/util/BytesRef;
    if-nez v9, :cond_10

    .line 2040
    const-string v12, "      broken seek state: prefix is not final in index"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2041
    new-instance v12, Ljava/lang/RuntimeException;

    const-string v13, "seek state is broken"

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 2042
    :cond_10
    if-eqz v7, :cond_13

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-nez v12, :cond_13

    .line 2043
    new-instance v11, Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v12, v9, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v13, v9, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v14, v9, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-direct {v11, v12, v13, v14}, Lorg/apache/lucene/store/ByteArrayDataInput;-><init>([BII)V

    .line 2044
    .local v11, "reader":Lorg/apache/lucene/store/ByteArrayDataInput;
    invoke-virtual {v11}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v4

    .line 2045
    .local v4, "codeOrig":J
    iget-wide v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    const/4 v14, 0x2

    shl-long v14, v12, v14

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    if-eqz v12, :cond_11

    const/4 v12, 0x2

    :goto_c
    int-to-long v12, v12

    or-long/2addr v14, v12

    iget-boolean v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v12, :cond_12

    const/4 v12, 0x1

    :goto_d
    int-to-long v12, v12

    or-long v2, v14, v12

    .line 2046
    .local v2, "code":J
    cmp-long v12, v4, v2

    if-eqz v12, :cond_13

    .line 2047
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "      broken seek state: output code="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " doesn\'t match frame code="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2048
    new-instance v12, Ljava/lang/RuntimeException;

    const-string v13, "seek state is broken"

    invoke-direct {v12, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 2045
    .end local v2    # "code":J
    :cond_11
    const/4 v12, 0x0

    goto :goto_c

    :cond_12
    const/4 v12, 0x0

    goto :goto_d

    .line 2052
    .end local v4    # "codeOrig":J
    .end local v9    # "output":Lorg/apache/lucene/util/BytesRef;
    .end local v11    # "reader":Lorg/apache/lucene/store/ByteArrayDataInput;
    :cond_13
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    if-eq v6, v12, :cond_0

    .line 2055
    iget v12, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    if-ne v12, v13, :cond_14

    .line 2056
    const/4 v7, 0x0

    .line 2058
    :cond_14
    add-int/lit8 v8, v8, 0x1

    .line 2023
    goto/16 :goto_0
.end method

.method private setEOF()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1497
    iput-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    .line 1498
    return v0
.end method


# virtual methods
.method public computeBlockStats()Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1322
    new-instance v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v4

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->segment:Ljava/lang/String;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v7, v7, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v7, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v4, v7}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    .local v1, "stats":Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1324
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->getNodeCount()J

    move-result-wide v8

    iput-wide v8, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->indexNodeCount:J

    .line 1325
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->getArcCount()J

    move-result-wide v8

    iput-wide v8, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->indexArcCount:J

    .line 1326
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/FST;->sizeInBytes()J

    move-result-wide v8

    iput-wide v8, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->indexNumBytes:J

    .line 1329
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1331
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1332
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v7, v7, v5

    invoke-virtual {v4, v7}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 1334
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1336
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_1
    const/4 v0, 0x0

    .line 1341
    .restart local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootCode:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v0, v4, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1342
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v8, v7, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    iput-wide v8, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    .line 1343
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1344
    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1346
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-boolean v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    if-eqz v4, :cond_4

    move v4, v5

    :goto_0
    invoke-virtual {v1, v7, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlock(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;Z)V

    .line 1352
    :cond_3
    :goto_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v7, v7, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-eq v4, v7, :cond_5

    .line 1371
    :goto_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->next()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1373
    const/4 v4, 0x0

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v8, v7, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p0, v4, v8, v9, v7}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1374
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v8, v7, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    iput-wide v8, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    .line 1378
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-boolean v5, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    .line 1380
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1381
    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-boolean v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    if-eqz v4, :cond_8

    move v4, v5

    :goto_3
    invoke-virtual {v1, v7, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlock(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;Z)V

    goto :goto_2

    :cond_4
    move v4, v6

    .line 1346
    goto :goto_0

    .line 1353
    :cond_5
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->endBlock(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;)V

    .line 1354
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-boolean v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    if-nez v4, :cond_6

    .line 1355
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadNextFloorBlock()V

    .line 1356
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v1, v4, v6}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->startBlock(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;Z)V

    goto :goto_1

    .line 1358
    :cond_6
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    if-nez v4, :cond_7

    .line 1389
    invoke-virtual {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->finish()V

    .line 1392
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1393
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 1394
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v4

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v6, v6, v5

    invoke-virtual {v4, v6}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 1396
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_b

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 1361
    :cond_7
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v2, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    .line 1362
    .local v2, "lastFP":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v7, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v7, v7, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    add-int/lit8 v7, v7, -0x1

    aget-object v4, v4, v7

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1363
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v8, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    cmp-long v4, v2, v8

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .end local v2    # "lastFP":J
    :cond_8
    move v4, v6

    .line 1381
    goto :goto_3

    .line 1383
    :cond_9
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v1, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->term(Lorg/apache/lucene/util/BytesRef;)V

    goto/16 :goto_1

    .line 1398
    :cond_a
    const/4 v0, 0x0

    .line 1400
    :cond_b
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v4, v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootCode:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v0, v4, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1401
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->rewind()V

    .line 1402
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1403
    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1404
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iput v5, v4, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1406
    return-object v1
.end method

.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2164
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2166
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->decodeMetaData()V

    .line 2168
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTermState;->docFreq:I

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 6
    .param p1, "skipDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2180
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2184
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->decodeMetaData()V

    .line 2188
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/PostingsReaderBase;->docs(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 6
    .param p1, "skipDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2193
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_0

    .line 2195
    const/4 v0, 0x0

    .line 2200
    :goto_0
    return-object v0

    .line 2198
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2199
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->decodeMetaData()V

    .line 2200
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v2, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/PostingsReaderBase;->docsAndPositions(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/codecs/BlockTermState;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    goto :goto_0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1437
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method initIndexInput()V
    .locals 1

    .prologue
    .line 1313
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    if-nez v0, :cond_0

    .line 1314
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v0

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    .line 1316
    :cond_0
    return-void
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 2069
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->in:Lorg/apache/lucene/store/IndexInput;

    if-nez v5, :cond_2

    .line 2072
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2073
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    aget-object v6, v6, v8

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v0

    .line 2075
    .local v0, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 2077
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_0
    const/4 v0, 0x0

    .line 2079
    .restart local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    iget-object v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->rootCode:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v0, v5, v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 2080
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 2083
    .end local v0    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 2085
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v5, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 2091
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    if-ne v5, v6, :cond_5

    .line 2099
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v5, v8}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v1

    .line 2100
    .local v1, "result":Z
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    if-nez v1, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 2105
    .end local v1    # "result":Z
    :cond_4
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-boolean v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isLastInFloor:Z

    if-nez v5, :cond_6

    .line 2106
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadNextFloorBlock()V

    .line 2104
    :cond_5
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v6, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->entCount:I

    if-eq v5, v6, :cond_4

    .line 2139
    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->next()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2142
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v6, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p0, v4, v6, v7, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 2146
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-boolean v8, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    .line 2148
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    goto :goto_1

    .line 2109
    :cond_6
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    if-nez v5, :cond_8

    .line 2111
    sget-boolean v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->setEOF()Z

    move-result v5

    if-nez v5, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 2112
    :cond_7
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iput v8, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 2113
    iput v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 2114
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->rewind()V

    .line 2115
    iput-boolean v8, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    .line 2151
    :goto_2
    return-object v4

    .line 2118
    :cond_8
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v2, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    .line 2119
    .local v2, "lastFP":J
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v6, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    add-int/lit8 v6, v6, -0x1

    aget-object v5, v5, v6

    iput-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 2121
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v5, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_9

    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-wide v6, v5, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    cmp-long v5, v6, v2

    if-eqz v5, :cond_a

    .line 2124
    :cond_9
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5, v6}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToFloorFrame(Lorg/apache/lucene/util/BytesRef;)V

    .line 2125
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 2126
    iget-object v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v5, v2, v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToSubBlock(J)V

    .line 2131
    :cond_a
    iget v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    iget-object v6, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v6, v6, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    goto/16 :goto_0

    .line 2151
    .end local v2    # "lastFP":J
    :cond_b
    iget-object v4, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    goto :goto_2
.end method

.method public ord()J
    .locals 1

    .prologue
    .line 2240
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .locals 6
    .param p2, "fp"    # J
    .param p4, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;JI)",
            "Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    const/4 v4, -0x1

    .line 1460
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->getFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v0

    .line 1461
    .local v0, "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    iput-object p1, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->arc:Lorg/apache/lucene/util/fst/FST$Arc;

    .line 1462
    iget-wide v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    cmp-long v1, v2, p2

    if-nez v1, :cond_1

    iget v1, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    if-eq v1, v4, :cond_1

    .line 1464
    iget v1, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    iget v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    if-le v1, v2, :cond_0

    .line 1465
    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->rewind()V

    .line 1471
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget v1, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    if-eq p4, v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1473
    :cond_1
    iput v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->nextEnt:I

    .line 1474
    iput p4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    .line 1475
    iget-object v1, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    const/4 v2, 0x0

    iput v2, v1, Lorg/apache/lucene/codecs/BlockTermState;->termBlockOrd:I

    .line 1476
    iput-wide p2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fp:J

    iput-wide p2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->fpOrig:J

    .line 1477
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->lastSubFP:J

    .line 1486
    :cond_2
    return-object v0
.end method

.method pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .locals 10
    .param p2, "frameData"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/util/fst/FST$Arc",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;",
            "Lorg/apache/lucene/util/BytesRef;",
            "I)",
            "Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1442
    .local p1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->scratchReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    iget-object v6, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v7, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v8, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v3, v6, v7, v8}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 1443
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->scratchReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v3}, Lorg/apache/lucene/store/ByteArrayDataInput;->readVLong()J

    move-result-wide v0

    .line 1444
    .local v0, "code":J
    const/4 v3, 0x2

    ushr-long v4, v0, v3

    .line 1445
    .local v4, "fpSeek":J
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v3, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->getFrame(I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v2

    .line 1446
    .local v2, "f":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    const-wide/16 v6, 0x2

    and-long/2addr v6, v0

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    .line 1447
    iget-boolean v3, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    iput-boolean v3, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTermsOrig:Z

    .line 1448
    const-wide/16 v6, 0x1

    and-long/2addr v6, v0

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    .line 1449
    iget-boolean v3, v2, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->isFloor:Z

    if-eqz v3, :cond_0

    .line 1450
    iget-object v3, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->scratchReader:Lorg/apache/lucene/store/ByteArrayDataInput;

    invoke-virtual {v2, v3, p2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->setFloorData(Lorg/apache/lucene/store/ByteArrayDataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 1452
    :cond_0
    invoke-virtual {p0, p1, v4, v5, p3}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;JI)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1454
    return-object v2

    .line 1446
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1448
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 16
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1764
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    if-nez v12, :cond_0

    .line 1765
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string v13, "terms index was not loaded"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 1768
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v12, v12

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gt v12, v13, :cond_1

    .line 1769
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v13, v13, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v14, v14, 0x1

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v13

    iput-object v13, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1772
    :cond_1
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->clearEOF()Z

    move-result v12

    if-nez v12, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1783
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 1785
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    if-eq v12, v13, :cond_13

    .line 1798
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    const/4 v13, 0x0

    aget-object v1, v12, v13

    .line 1799
    .local v1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_3

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-nez v12, :cond_3

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1800
    :cond_3
    iget-object v5, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1801
    .local v5, "output":Lorg/apache/lucene/util/BytesRef;
    const/4 v10, 0x0

    .line 1803
    .local v10, "targetUpto":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v13, 0x0

    aget-object v3, v12, v13

    .line 1804
    .local v3, "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v13, v13, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v12, v13, :cond_4

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1806
    :cond_4
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 1808
    .local v8, "targetLimit":I
    const/4 v2, 0x0

    .line 1814
    .local v2, "cmp":I
    :goto_0
    if-lt v10, v8, :cond_b

    .line 1839
    :cond_5
    if-nez v2, :cond_8

    .line 1840
    move v11, v10

    .line 1843
    .local v11, "targetUptoMid":I
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v13, v13, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 1844
    .local v9, "targetLimit2":I
    :goto_1
    if-lt v10, v9, :cond_f

    .line 1855
    :cond_6
    if-nez v2, :cond_7

    .line 1856
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v12, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v2, v12, v13

    .line 1858
    :cond_7
    move v10, v11

    .line 1861
    .end local v9    # "targetLimit2":I
    .end local v11    # "targetUptoMid":I
    :cond_8
    if-gez v2, :cond_10

    .line 1868
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1922
    .end local v2    # "cmp":I
    .end local v3    # "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .end local v8    # "targetLimit":I
    :cond_9
    :goto_2
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v10, v12, :cond_16

    .line 1988
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1990
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToFloorFrame(Lorg/apache/lucene/util/BytesRef;)V

    .line 1992
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1994
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToTerm(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v6

    .line 1996
    .local v6, "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v12, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v6, v12, :cond_a

    .line 1997
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 1998
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    .line 1999
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v12

    if-eqz v12, :cond_1b

    .line 2003
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 2011
    .end local v6    # "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_a
    :goto_3
    return-object v6

    .line 1815
    .restart local v2    # "cmp":I
    .restart local v3    # "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .restart local v8    # "targetLimit":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v12, v12, v10

    and-int/lit16 v12, v12, 0xff

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v14, v10

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    sub-int v2, v12, v13

    .line 1819
    if-nez v2, :cond_5

    .line 1822
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    add-int/lit8 v13, v10, 0x1

    aget-object v1, v12, v13

    .line 1823
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_c

    iget v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v14, v10

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    if-eq v12, v13, :cond_c

    new-instance v12, Ljava/lang/AssertionError;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "arc.label="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, v1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    int-to-char v14, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " targetLabel="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v15, v10

    aget-byte v14, v14, v15

    and-int/lit16 v14, v14, 0xff

    int-to-char v14, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 1829
    :cond_c
    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v13

    iget-object v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->NO_OUTPUT:Lorg/apache/lucene/util/BytesRef;

    if-eq v12, v13, :cond_d

    .line 1830
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1832
    .restart local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    :cond_d
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-eqz v12, :cond_e

    .line 1833
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v13, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    add-int/lit8 v13, v13, 0x1

    aget-object v3, v12, v13

    .line 1835
    :cond_e
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 1845
    .restart local v9    # "targetLimit2":I
    .restart local v11    # "targetUptoMid":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v12, v12, v10

    and-int/lit16 v12, v12, 0xff

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v14, v10

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    sub-int v2, v12, v13

    .line 1849
    if-nez v2, :cond_6

    .line 1852
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1870
    .end local v9    # "targetLimit2":I
    .end local v11    # "targetUptoMid":I
    :cond_10
    if-lez v2, :cond_11

    .line 1875
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 1879
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1880
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->rewind()V

    goto/16 :goto_2

    .line 1883
    :cond_11
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_12

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v12, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eq v12, v13, :cond_12

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1884
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    if-eqz v12, :cond_9

    .line 1888
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_3

    .line 1898
    .end local v1    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v2    # "cmp":I
    .end local v3    # "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .end local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    .end local v8    # "targetLimit":I
    .end local v10    # "targetUpto":I
    :cond_13
    const/4 v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 1899
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    invoke-virtual {v12, v13}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    .line 1902
    .restart local v1    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_14

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-nez v12, :cond_14

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1903
    :cond_14
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_15

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    if-nez v12, :cond_15

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1909
    :cond_15
    iget-object v5, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1911
    .restart local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1914
    const/4 v10, 0x0

    .line 1915
    .restart local v10    # "targetUpto":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v12, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    goto/16 :goto_2

    .line 1924
    :cond_16
    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v13, v10

    aget-byte v12, v12, v13

    and-int/lit16 v7, v12, 0xff

    .line 1926
    .local v7, "targetLabel":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    add-int/lit8 v13, v10, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v12, v7, v1, v13, v14}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    .line 1928
    .local v4, "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    if-nez v4, :cond_18

    .line 1935
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1938
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToFloorFrame(Lorg/apache/lucene/util/BytesRef;)V

    .line 1940
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1942
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToTerm(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v6

    .line 1943
    .restart local v6    # "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v12, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v6, v12, :cond_a

    .line 1944
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 1945
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    .line 1947
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v12

    if-eqz v12, :cond_17

    .line 1951
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_3

    .line 1956
    :cond_17
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_3

    .line 1966
    .end local v6    # "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_18
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v13, v7

    aput-byte v13, v12, v10

    .line 1967
    move-object v1, v4

    .line 1969
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_19

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    if-nez v12, :cond_19

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1970
    :cond_19
    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v13

    iget-object v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->NO_OUTPUT:Lorg/apache/lucene/util/BytesRef;

    if-eq v12, v13, :cond_1a

    .line 1971
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1977
    .restart local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    :cond_1a
    add-int/lit8 v10, v10, 0x1

    .line 1979
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1981
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v12, v10}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    goto/16 :goto_2

    .line 2008
    .end local v4    # "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v7    # "targetLabel":I
    .restart local v6    # "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_1b
    sget-object v6, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_3
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J

    .prologue
    .line 2235
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V
    .locals 2
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "otherState"    # Lorg/apache/lucene/index/TermState;

    .prologue
    .line 2208
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->clearEOF()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2209
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    if-nez v0, :cond_5

    .line 2210
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-eqz p2, :cond_2

    instance-of v0, p2, Lorg/apache/lucene/codecs/BlockTermState;

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2211
    :cond_3
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iput-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 2212
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/codecs/BlockTermState;->copyFrom(Lorg/apache/lucene/index/TermState;)V

    .line 2213
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 2214
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->getTermBlockOrd()I

    move-result v1

    iput v1, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    .line 2215
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->metaDataUpto:I

    if-gtz v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2216
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 2222
    :cond_5
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 16
    .param p1, "target"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1504
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    if-nez v12, :cond_0

    .line 1505
    new-instance v12, Ljava/lang/IllegalStateException;

    const-string v13, "terms index was not loaded"

    invoke-direct {v12, v13}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 1508
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v12, v12

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-gt v12, v13, :cond_1

    .line 1509
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v13, v13, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v14, v14, 0x1

    invoke-static {v13, v14}, Lorg/apache/lucene/util/ArrayUtil;->grow([BI)[B

    move-result-object v13

    iput-object v13, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 1512
    :cond_1
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->clearEOF()Z

    move-result v12

    if-nez v12, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1523
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 1525
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    if-eq v12, v13, :cond_12

    .line 1538
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    const/4 v13, 0x0

    aget-object v1, v12, v13

    .line 1539
    .local v1, "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_3

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-nez v12, :cond_3

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1540
    :cond_3
    iget-object v5, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1541
    .local v5, "output":Lorg/apache/lucene/util/BytesRef;
    const/4 v10, 0x0

    .line 1543
    .local v10, "targetUpto":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v13, 0x0

    aget-object v3, v12, v13

    .line 1544
    .local v3, "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v13, v13, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v12, v13, :cond_4

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1546
    :cond_4
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 1548
    .local v8, "targetLimit":I
    const/4 v2, 0x0

    .line 1554
    .local v2, "cmp":I
    :goto_0
    if-lt v10, v8, :cond_a

    .line 1576
    :cond_5
    if-nez v2, :cond_8

    .line 1577
    move v11, v10

    .line 1583
    .local v11, "targetUptoMid":I
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v13, v13, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 1584
    .local v9, "targetLimit2":I
    :goto_1
    if-lt v10, v9, :cond_e

    .line 1595
    :cond_6
    if-nez v2, :cond_7

    .line 1596
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v12, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v2, v12, v13

    .line 1598
    :cond_7
    move v10, v11

    .line 1601
    .end local v9    # "targetLimit2":I
    .end local v11    # "targetUptoMid":I
    :cond_8
    if-gez v2, :cond_f

    .line 1608
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1665
    .end local v2    # "cmp":I
    .end local v3    # "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .end local v8    # "targetLimit":I
    :cond_9
    :goto_2
    move-object/from16 v0, p1

    iget v12, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v10, v12, :cond_15

    .line 1731
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1733
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToFloorFrame(Lorg/apache/lucene/util/BytesRef;)V

    .line 1736
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-boolean v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    if-nez v12, :cond_1b

    .line 1737
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    .line 1738
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iput v10, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1742
    const/4 v12, 0x0

    .line 1758
    :goto_3
    return v12

    .line 1555
    .restart local v2    # "cmp":I
    .restart local v3    # "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .restart local v8    # "targetLimit":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v12, v12, v10

    and-int/lit16 v12, v12, 0xff

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v14, v10

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    sub-int v2, v12, v13

    .line 1559
    if-nez v2, :cond_5

    .line 1562
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    add-int/lit8 v13, v10, 0x1

    aget-object v1, v12, v13

    .line 1566
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_b

    iget v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v14, v10

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    if-eq v12, v13, :cond_b

    new-instance v12, Ljava/lang/AssertionError;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "arc.label="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v14, v1, Lorg/apache/lucene/util/fst/FST$Arc;->label:I

    int-to-char v14, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " targetLabel="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v15, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v15, v10

    aget-byte v14, v14, v15

    and-int/lit16 v14, v14, 0xff

    int-to-char v14, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v12

    .line 1567
    :cond_b
    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v13

    iget-object v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->NO_OUTPUT:Lorg/apache/lucene/util/BytesRef;

    if-eq v12, v13, :cond_c

    .line 1568
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1570
    .restart local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    :cond_c
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1571
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->stack:[Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v13, v3, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->ord:I

    add-int/lit8 v13, v13, 0x1

    aget-object v3, v12, v13

    .line 1573
    :cond_d
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 1585
    .restart local v9    # "targetLimit2":I
    .restart local v11    # "targetUptoMid":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v12, v12, v10

    and-int/lit16 v12, v12, 0xff

    move-object/from16 v0, p1

    iget-object v13, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v14, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v14, v10

    aget-byte v13, v13, v14

    and-int/lit16 v13, v13, 0xff

    sub-int v2, v12, v13

    .line 1589
    if-nez v2, :cond_6

    .line 1592
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1610
    .end local v9    # "targetLimit2":I
    .end local v11    # "targetUptoMid":I
    :cond_f
    if-lez v2, :cond_10

    .line 1615
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 1619
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1620
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->rewind()V

    goto/16 :goto_2

    .line 1623
    :cond_10
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_11

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v12, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eq v12, v13, :cond_11

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1624
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    if-eqz v12, :cond_9

    .line 1628
    const/4 v12, 0x1

    goto/16 :goto_3

    .line 1641
    .end local v1    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v2    # "cmp":I
    .end local v3    # "lastFrame":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;
    .end local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    .end local v8    # "targetLimit":I
    .end local v10    # "targetUpto":I
    :cond_12
    const/4 v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->targetBeforeCurrentLength:I

    .line 1642
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->arcs:[Lorg/apache/lucene/util/fst/FST$Arc;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    invoke-virtual {v12, v13}, Lorg/apache/lucene/util/fst/FST;->getFirstArc(Lorg/apache/lucene/util/fst/FST$Arc;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v1

    .line 1645
    .restart local v1    # "arc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_13

    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-nez v12, :cond_13

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1646
    :cond_13
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_14

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    if-nez v12, :cond_14

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1652
    :cond_14
    iget-object v5, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1654
    .restart local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->staticFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    .line 1657
    const/4 v10, 0x0

    .line 1658
    .restart local v10    # "targetUpto":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v12, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    goto/16 :goto_2

    .line 1667
    :cond_15
    move-object/from16 v0, p1

    iget-object v12, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    move-object/from16 v0, p1

    iget v13, v0, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v13, v10

    aget-byte v12, v12, v13

    and-int/lit16 v7, v12, 0xff

    .line 1669
    .local v7, "targetLabel":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->index:Lorg/apache/lucene/util/fst/FST;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/util/fst/FST;

    move-result-object v12

    add-int/lit8 v13, v10, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->getArc(I)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->fstReader:Lorg/apache/lucene/util/fst/FST$BytesReader;

    invoke-virtual {v12, v7, v1, v13, v14}, Lorg/apache/lucene/util/fst/FST;->findTargetArc(ILorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/fst/FST$BytesReader;)Lorg/apache/lucene/util/fst/FST$Arc;

    move-result-object v4

    .line 1671
    .local v4, "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    if-nez v4, :cond_18

    .line 1678
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->prefix:I

    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->validIndexPrefix:I

    .line 1681
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToFloorFrame(Lorg/apache/lucene/util/BytesRef;)V

    .line 1683
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-boolean v12, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->hasTerms:Z

    if-nez v12, :cond_16

    .line 1684
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->termExists:Z

    .line 1685
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v13, v7

    aput-byte v13, v12, v10

    .line 1686
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    add-int/lit8 v13, v10, 0x1

    iput v13, v12, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 1690
    const/4 v12, 0x0

    goto/16 :goto_3

    .line 1693
    :cond_16
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1695
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToTerm(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v6

    .line 1696
    .local v6, "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v12, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v6, v12, :cond_17

    .line 1700
    const/4 v12, 0x1

    goto/16 :goto_3

    .line 1705
    :cond_17
    const/4 v12, 0x0

    goto/16 :goto_3

    .line 1709
    .end local v6    # "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_18
    move-object v1, v4

    .line 1710
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget-object v12, v12, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v13, v7

    aput-byte v13, v12, v10

    .line 1712
    sget-boolean v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v12, :cond_19

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    if-nez v12, :cond_19

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 1713
    :cond_19
    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v13

    iget-object v13, v13, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->NO_OUTPUT:Lorg/apache/lucene/util/BytesRef;

    if-eq v12, v13, :cond_1a

    .line 1714
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->output:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    check-cast v5, Lorg/apache/lucene/util/BytesRef;

    .line 1720
    .restart local v5    # "output":Lorg/apache/lucene/util/BytesRef;
    :cond_1a
    add-int/lit8 v10, v10, 0x1

    .line 1722
    invoke-virtual {v1}, Lorg/apache/lucene/util/fst/FST$Arc;->isFinal()Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1724
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->this$1:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    # getter for: Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->this$0:Lorg/apache/lucene/codecs/BlockTreeTermsReader;
    invoke-static {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;)Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    move-result-object v12

    iget-object v13, v12, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    iget-object v12, v1, Lorg/apache/lucene/util/fst/FST$Arc;->nextFinalOutput:Ljava/lang/Object;

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v13, v5, v12}, Lorg/apache/lucene/util/fst/Outputs;->add(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v12, v10}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->pushFrame(Lorg/apache/lucene/util/fst/FST$Arc;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    goto/16 :goto_2

    .line 1745
    .end local v4    # "nextArc":Lorg/apache/lucene/util/fst/FST$Arc;, "Lorg/apache/lucene/util/fst/FST$Arc<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v7    # "targetLabel":I
    :cond_1b
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->loadBlock()V

    .line 1747
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v12, v0, v13}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->scanToTerm(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v6

    .line 1748
    .restart local v6    # "result":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-object v12, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v6, v12, :cond_1c

    .line 1752
    const/4 v12, 0x1

    goto/16 :goto_3

    .line 1758
    :cond_1c
    const/4 v12, 0x0

    goto/16 :goto_3
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 2158
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2159
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2226
    sget-boolean v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 2227
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->decodeMetaData()V

    .line 2228
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v1, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/BlockTermState;->clone()Lorg/apache/lucene/index/TermState;

    move-result-object v0

    .line 2230
    .local v0, "ts":Lorg/apache/lucene/index/TermState;
    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2173
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->eof:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 2174
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->decodeMetaData()V

    .line 2175
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum;->currentFrame:Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader$SegmentTermsEnum$Frame;->state:Lorg/apache/lucene/codecs/BlockTermState;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/BlockTermState;->totalTermFreq:J

    return-wide v0
.end method
