.class Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;
.super Ljava/lang/Object;
.source "CompressingTermVectorsReader.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field i:I

.field final synthetic this$1:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;

    .line 620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 621
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->i:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 624
    iget v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->i:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)[I

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->next()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public next()Ljava/lang/String;
    .locals 5

    .prologue
    .line 628
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 629
    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    .line 631
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNums:[I
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->access$1(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)[I

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->fieldNumOffs:[I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)[I

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->i:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->i:I

    aget v2, v2, v3

    aget v0, v1, v2

    .line 632
    .local v0, "fieldNum":I
    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields$1;->this$1:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;->access$2(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader$TVFields;)Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;

    move-result-object v1

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingTermVectorsReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    return-object v1
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 636
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
