.class Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "Lucene40DocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;->loadBytesVarStraight(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

.field private final synthetic val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

.field private final synthetic val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;Lorg/apache/lucene/util/packed/PackedInts$Reader;Lorg/apache/lucene/util/PagedBytes$Reader;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;->this$0:Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    iput-object p3, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    .line 362
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 8
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 365
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    invoke-interface {v4, p1}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v2

    .line 366
    .local v2, "startAddress":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;->val$reader:Lorg/apache/lucene/util/packed/PackedInts$Reader;

    add-int/lit8 v5, p1, 0x1

    invoke-interface {v4, v5}, Lorg/apache/lucene/util/packed/PackedInts$Reader;->get(I)J

    move-result-wide v0

    .line 367
    .local v0, "endAddress":J
    iget-object v4, p0, Lorg/apache/lucene/codecs/lucene40/Lucene40DocValuesReader$10;->val$bytesReader:Lorg/apache/lucene/util/PagedBytes$Reader;

    sub-long v6, v0, v2

    long-to-int v5, v6

    invoke-virtual {v4, p2, v2, v3, v5}, Lorg/apache/lucene/util/PagedBytes$Reader;->fillSlice(Lorg/apache/lucene/util/BytesRef;JI)V

    .line 368
    return-void
.end method
