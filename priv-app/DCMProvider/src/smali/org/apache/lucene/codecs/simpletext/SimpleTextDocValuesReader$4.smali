.class Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;
.super Lorg/apache/lucene/index/SortedSetDocValues;
.source "SimpleTextDocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getSortedSet(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/SortedSetDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field currentIndex:I

.field currentOrds:[Ljava/lang/String;

.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

.field private final synthetic val$decoder:Ljava/text/DecimalFormat;

.field private final synthetic val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

.field private final synthetic val$in:Lorg/apache/lucene/store/IndexInput;

.field private final synthetic val$scratch:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iput-object p4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iput-object p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$decoder:Ljava/text/DecimalFormat;

    .line 300
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValues;-><init>()V

    .line 301
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentOrds:[Ljava/lang/String;

    .line 302
    iput v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentIndex:I

    return-void
.end method


# virtual methods
.method public getValueCount()J
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v0, v0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->numValues:J

    return-wide v0
.end method

.method public lookupOrd(JLorg/apache/lucene/util/BytesRef;)V
    .locals 11
    .param p1, "ord"    # J
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 336
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_0

    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v4, v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->numValues:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    .line 337
    :cond_0
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ord must be 0 .. "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v6, v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->numValues:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; got "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :catch_0
    move-exception v1

    .line 355
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 339
    .end local v1    # "ioe":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v6, v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-object v5, v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget v8, v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    add-int/2addr v5, v8

    int-to-long v8, v5

    mul-long/2addr v8, p1

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 340
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 341
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v6}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 344
    :cond_2
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$decoder:Ljava/text/DecimalFormat;

    new-instance v5, Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    iget v9, v9, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v8, v9

    const-string v9, "UTF-8"

    invoke-direct {v5, v6, v7, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v2

    .line 350
    .local v2, "len":I
    :try_start_3
    new-array v4, v2, [B

    iput-object v4, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 351
    const/4 v4, 0x0

    iput v4, p3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 352
    iput v2, p3, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 353
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 357
    return-void

    .line 345
    .end local v2    # "len":I
    :catch_1
    move-exception v3

    .line 346
    .local v3, "pe":Ljava/text/ParseException;
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    const-string v4, "failed to parse int length"

    invoke-direct {v0, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    .line 347
    .local v0, "e":Lorg/apache/lucene/index/CorruptIndexException;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/CorruptIndexException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 348
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method public nextOrd()J
    .locals 3

    .prologue
    .line 306
    iget v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentIndex:I

    iget-object v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentOrds:[Ljava/lang/String;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 307
    const-wide/16 v0, -0x1

    .line 309
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentOrds:[Ljava/lang/String;

    iget v1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentIndex:I

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public setDocument(I)V
    .locals 10
    .param p1, "docID"    # I

    .prologue
    .line 315
    if-ltz p1, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iget v2, v2, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    if-lt p1, v2, :cond_1

    .line 316
    :cond_0
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "docID must be 0 .. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iget v4, v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; got "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 319
    :cond_1
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v4, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v6, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->numValues:J

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-object v3, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget v8, v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    add-int/2addr v3, v8

    int-to-long v8, v3

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-object v3, v3, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->ordPattern:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v3, p1

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 320
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v2, v3}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 321
    iget-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 322
    .local v1, "ordList":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 323
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentOrds:[Ljava/lang/String;

    .line 327
    :goto_0
    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentIndex:I

    .line 331
    return-void

    .line 325
    :cond_2
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$4;->currentOrds:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 328
    .end local v1    # "ordList":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 329
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
