.class Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "SimpleTextDocValuesReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->getBinary(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

.field private final synthetic val$decoder:Ljava/text/DecimalFormat;

.field private final synthetic val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

.field private final synthetic val$in:Lorg/apache/lucene/store/IndexInput;

.field private final synthetic val$scratch:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;Lorg/apache/lucene/store/IndexInput;Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;Lorg/apache/lucene/util/BytesRef;Ljava/text/DecimalFormat;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iput-object p2, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$in:Lorg/apache/lucene/store/IndexInput;

    iput-object p3, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iput-object p4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iput-object p5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$decoder:Ljava/text/DecimalFormat;

    .line 192
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 10
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 196
    if-ltz p1, :cond_0

    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iget v4, v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    if-lt p1, v4, :cond_1

    .line 197
    :cond_0
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "docID must be 0 .. "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->this$0:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;

    iget v6, v6, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->maxDoc:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; got "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :catch_0
    move-exception v1

    .line 215
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 199
    .end local v1    # "ioe":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-wide v6, v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->dataStartFilePointer:J

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget-object v5, v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->pattern:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x9

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$field:Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;

    iget v8, v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$OneField;->maxLength:I

    add-int/2addr v5, v8

    mul-int/2addr v5, p1

    int-to-long v8, v5

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 200
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/codecs/simpletext/SimpleTextUtil;->readLine(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/BytesRef;)V

    .line 201
    sget-boolean v4, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader;->$assertionsDisabled:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    sget-object v5, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v4, v5}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 204
    :cond_2
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$decoder:Ljava/text/DecimalFormat;

    new-instance v5, Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v7, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->offset:I

    sget-object v8, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$scratch:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    sget-object v9, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesWriter;->LENGTH:Lorg/apache/lucene/util/BytesRef;

    iget v9, v9, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int/2addr v8, v9

    const-string v9, "UTF-8"

    invoke-direct {v5, v6, v7, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/text/DecimalFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v2

    .line 210
    .local v2, "len":I
    :try_start_3
    new-array v4, v2, [B

    iput-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 211
    const/4 v4, 0x0

    iput v4, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 212
    iput v2, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 213
    iget-object v4, p0, Lorg/apache/lucene/codecs/simpletext/SimpleTextDocValuesReader$2;->val$in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, v2}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 217
    return-void

    .line 205
    .end local v2    # "len":I
    :catch_1
    move-exception v3

    .line 206
    .local v3, "pe":Ljava/text/ParseException;
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    const-string v4, "failed to parse int length"

    invoke-direct {v0, v4}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "e":Lorg/apache/lucene/index/CorruptIndexException;
    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/CorruptIndexException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 208
    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
.end method
