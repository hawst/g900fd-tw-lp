.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec$1;
.super Lorg/apache/lucene/codecs/DocValuesFormat;
.source "Lucene3xCodec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;


# direct methods
.method constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;Ljava/lang/String;)V
    .locals 0
    .param p2, "$anonymous0"    # Ljava/lang/String;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec$1;->this$0:Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;

    .line 70
    invoke-direct {p0, p2}, Lorg/apache/lucene/codecs/DocValuesFormat;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/DocValuesConsumer;
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "this codec cannot write docvalues"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fieldsProducer(Lorg/apache/lucene/index/SegmentReadState;)Lorg/apache/lucene/codecs/DocValuesProducer;
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentReadState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method
