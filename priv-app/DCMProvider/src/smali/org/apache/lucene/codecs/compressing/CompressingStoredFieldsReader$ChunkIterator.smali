.class final Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;
.super Ljava/lang/Object;
.source "CompressingStoredFieldsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ChunkIterator"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field bytes:Lorg/apache/lucene/util/BytesRef;

.field chunkDocs:I

.field docBase:I

.field lengths:[I

.field numStoredFields:[I

.field final synthetic this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 302
    const-class v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 310
    iput-object p1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    .line 312
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 313
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    .line 314
    new-array v0, v1, [I

    iput-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    .line 315
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;)V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0, p1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;-><init>(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)V

    return-void
.end method


# virtual methods
.method chunkSize()I
    .locals 3

    .prologue
    .line 321
    const/4 v1, 0x0

    .line 322
    .local v1, "sum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    if-lt v0, v2, :cond_0

    .line 325
    return v1

    .line 323
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    aget v2, v2, v0

    add-int/2addr v1, v2

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method copyCompressedData(Lorg/apache/lucene/store/DataOutput;)V
    .locals 6
    .param p1, "out"    # Lorg/apache/lucene/store/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 398
    iget v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I
    invoke-static {v3}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$2(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 399
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v0

    .line 401
    .local v0, "chunkEnd":J
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v3}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v4

    sub-long v4, v0, v4

    invoke-virtual {p1, v2, v4, v5}, Lorg/apache/lucene/store/DataOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 402
    return-void

    .line 400
    .end local v0    # "chunkEnd":J
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$1(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-result-object v2

    iget v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    iget v4, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method decompress()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkSize()I

    move-result v2

    .line 388
    .local v2, "chunkSize":I
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->decompressor:Lorg/apache/lucene/codecs/compressing/Decompressor;
    invoke-static {v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$4(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/codecs/compressing/Decompressor;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v1}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v5, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/compressing/Decompressor;->decompress(Lorg/apache/lucene/store/DataInput;IIILorg/apache/lucene/util/BytesRef;)V

    .line 389
    iget-object v0, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eq v0, v2, :cond_0

    .line 390
    new-instance v0, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Corrupted: expected chunk size = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkSize()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", got "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_0
    return-void
.end method

.method next(I)V
    .locals 17
    .param p1, "doc"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    sget-boolean v2, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    add-int/2addr v2, v3

    move/from16 v0, p1

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 333
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->indexReader:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;
    invoke-static {v3}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$1(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsIndexReader;->getStartPointer(I)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v13

    .line 336
    .local v13, "docBase":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v5

    .line 337
    .local v5, "chunkDocs":I
    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    move-object/from16 v0, p0

    iget v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    add-int/2addr v2, v3

    if-lt v13, v2, :cond_1

    .line 338
    add-int v2, v13, v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->numDocs:I
    invoke-static {v3}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$2(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)I

    move-result v3

    if-le v2, v3, :cond_2

    .line 339
    :cond_1
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Corrupted: current docBase="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 340
    const-string v4, ", current numDocs="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", new docBase="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 341
    const-string v4, ", new numDocs="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 339
    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 343
    :cond_2
    move-object/from16 v0, p0

    iput v13, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->docBase:I

    .line 344
    move-object/from16 v0, p0

    iput v5, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->chunkDocs:I

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    array-length v2, v2

    if-le v5, v2, :cond_3

    .line 347
    const/4 v2, 0x4

    invoke-static {v5, v2}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v16

    .line 348
    .local v16, "newLength":I
    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    .line 349
    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    .line 352
    .end local v16    # "newLength":I
    :cond_3
    const/4 v2, 0x1

    if-ne v5, v2, :cond_5

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    aput v4, v2, v3

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    aput v4, v2, v3

    .line 380
    :cond_4
    :goto_0
    return-void

    .line 356
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v6

    .line 357
    .local v6, "bitsPerStoredFields":I
    if-nez v6, :cond_7

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    invoke-static {v2, v3, v5, v4}, Ljava/util/Arrays;->fill([IIII)V

    .line 368
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v11

    .line 369
    .local v11, "bitsPerLength":I
    if-nez v11, :cond_9

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v4

    invoke-static {v2, v3, v5, v4}, Ljava/util/Arrays;->fill([IIII)V

    goto :goto_0

    .line 359
    .end local v11    # "bitsPerLength":I
    :cond_7
    const/16 v2, 0x1f

    if-le v6, v2, :cond_8

    .line 360
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bitsPerStoredFields="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 362
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I
    invoke-static {v4}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$3(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)I

    move-result v4

    const/4 v7, 0x1

    invoke-static/range {v2 .. v7}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderIteratorNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;IIII)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;

    move-result-object v15

    .line 363
    .local v15, "it":Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    if-ge v14, v5, :cond_6

    .line 364
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->numStoredFields:[I

    invoke-interface {v15}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;->next()J

    move-result-wide v8

    long-to-int v3, v8

    aput v3, v2, v14

    .line 363
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 371
    .end local v14    # "i":I
    .end local v15    # "it":Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    .restart local v11    # "bitsPerLength":I
    :cond_9
    const/16 v2, 0x1f

    if-le v11, v2, :cond_a

    .line 372
    new-instance v2, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bitsPerLength="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 374
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->fieldsStream:Lorg/apache/lucene/store/IndexInput;
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$0(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v7

    sget-object v8, Lorg/apache/lucene/util/packed/PackedInts$Format;->PACKED:Lorg/apache/lucene/util/packed/PackedInts$Format;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->this$0:Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;

    # getter for: Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->packedIntsVersion:I
    invoke-static {v2}, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;->access$3(Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader;)I

    move-result v9

    const/4 v12, 0x1

    move v10, v5

    invoke-static/range {v7 .. v12}, Lorg/apache/lucene/util/packed/PackedInts;->getReaderIteratorNoHeader(Lorg/apache/lucene/store/DataInput;Lorg/apache/lucene/util/packed/PackedInts$Format;IIII)Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;

    move-result-object v15

    .line 375
    .restart local v15    # "it":Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_2
    if-ge v14, v5, :cond_4

    .line 376
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/codecs/compressing/CompressingStoredFieldsReader$ChunkIterator;->lengths:[I

    invoke-interface {v15}, Lorg/apache/lucene/util/packed/PackedInts$ReaderIterator;->next()J

    move-result-wide v8

    long-to-int v3, v8

    aput v3, v2, v14

    .line 375
    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method
