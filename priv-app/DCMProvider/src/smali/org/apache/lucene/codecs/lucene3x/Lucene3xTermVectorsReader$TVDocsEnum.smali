.class Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "Lucene3xTermVectorsReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TVDocsEnum"
.end annotation


# instance fields
.field private didNext:Z

.field private doc:I

.field private freq:I

.field private liveDocs:Lorg/apache/lucene/util/Bits;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 549
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 551
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->doc:I

    .line 549
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;)V
    .locals 0

    .prologue
    .line 549
    invoke-direct {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;-><init>()V

    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 577
    iget-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->didNext:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 578
    invoke-virtual {p0}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->nextDoc()I

    move-result v0

    .line 580
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->doc:I

    goto :goto_0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 593
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 557
    iget v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->freq:I

    return v0
.end method

.method public nextDoc()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 567
    iget-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->didNext:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, v0}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 568
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->didNext:Z

    .line 569
    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->doc:I

    .line 571
    :goto_0
    return v0

    :cond_1
    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->doc:I

    goto :goto_0
.end method

.method public reset(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;)V
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "termAndPostings"    # Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;

    .prologue
    .line 585
    iput-object p1, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 586
    iget v0, p2, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TermAndPostings;->freq:I

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->freq:I

    .line 587
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->doc:I

    .line 588
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/codecs/lucene3x/Lucene3xTermVectorsReader$TVDocsEnum;->didNext:Z

    .line 589
    return-void
.end method
