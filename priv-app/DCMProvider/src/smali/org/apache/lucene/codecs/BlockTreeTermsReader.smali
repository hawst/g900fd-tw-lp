.class public Lorg/apache/lucene/codecs/BlockTreeTermsReader;
.super Lorg/apache/lucene/codecs/FieldsProducer;
.source "BlockTreeTermsReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;,
        Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final NO_OUTPUT:Lorg/apache/lucene/util/BytesRef;

.field private dirOffset:J

.field private final fields:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;",
            ">;"
        }
    .end annotation
.end field

.field final fstOutputs:Lorg/apache/lucene/util/fst/Outputs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/fst/Outputs",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final in:Lorg/apache/lucene/store/IndexInput;

.field private indexDirOffset:J

.field private final postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;

.field private segment:Ljava/lang/String;

.field private final version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/FieldInfos;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/codecs/PostingsReaderBase;Lorg/apache/lucene/store/IOContext;Ljava/lang/String;I)V
    .locals 28
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p3, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "postingsReader"    # Lorg/apache/lucene/codecs/PostingsReaderBase;
    .param p5, "ioContext"    # Lorg/apache/lucene/store/IOContext;
    .param p6, "segmentSuffix"    # Ljava/lang/String;
    .param p7, "indexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/codecs/FieldsProducer;-><init>()V

    .line 100
    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    .line 444
    invoke-static {}, Lorg/apache/lucene/util/fst/ByteSequenceOutputs;->getSingleton()Lorg/apache/lucene/util/fst/ByteSequenceOutputs;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    .line 445
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fstOutputs:Lorg/apache/lucene/util/fst/Outputs;

    invoke-virtual {v4}, Lorg/apache/lucene/util/fst/Outputs;->getNoOutput()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->NO_OUTPUT:Lorg/apache/lucene/util/BytesRef;

    .line 118
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;

    .line 120
    move-object/from16 v0, p3

    iget-object v4, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->segment:Ljava/lang/String;

    .line 121
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->segment:Ljava/lang/String;

    const-string v5, "tim"

    move-object/from16 v0, p6

    invoke-static {v4, v0, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v0, v4, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    .line 124
    const/16 v24, 0x0

    .line 125
    .local v24, "success":Z
    const/16 v17, 0x0

    .line 128
    .local v17, "indexIn":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->readHeader(Lorg/apache/lucene/store/IndexInput;)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->version:I

    .line 129
    const/4 v4, -0x1

    move/from16 v0, p7

    if-eq v0, v4, :cond_1

    .line 130
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->segment:Ljava/lang/String;

    const-string v5, "tip"

    move-object/from16 v0, p6

    invoke-static {v4, v0, v5}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    invoke-virtual {v0, v4, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v17

    .line 132
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->readIndexHeader(Lorg/apache/lucene/store/IndexInput;)I

    move-result v20

    .line 133
    .local v20, "indexVersion":I
    move-object/from16 v0, p0

    iget v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->version:I

    move/from16 v0, v20

    if-eq v0, v4, :cond_1

    .line 134
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "mixmatched version files: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->version:I

    move/from16 v25, v0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, ","

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, "="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    .end local v20    # "indexVersion":I
    :catchall_0
    move-exception v4

    .line 186
    if-nez v24, :cond_0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/io/Closeable;

    const/16 v25, 0x0

    .line 188
    aput-object v17, v5, v25

    const/16 v25, 0x1

    aput-object p0, v5, v25

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 190
    :cond_0
    throw v4

    .line 139
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Lorg/apache/lucene/codecs/PostingsReaderBase;->init(Lorg/apache/lucene/store/IndexInput;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->dirOffset:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->seekDir(Lorg/apache/lucene/store/IndexInput;J)V

    .line 143
    const/4 v4, -0x1

    move/from16 v0, p7

    if-eq v0, v4, :cond_2

    .line 144
    move-object/from16 v0, p0

    iget-wide v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->indexDirOffset:J

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4, v5}, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->seekDir(Lorg/apache/lucene/store/IndexInput;J)V

    .line 147
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v22

    .line 148
    .local v22, "numFields":I
    if-gez v22, :cond_3

    .line 149
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "invalid numFields: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " (resource="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 152
    :cond_3
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v22

    if-lt v0, v1, :cond_6

    .line 180
    const/4 v4, -0x1

    move/from16 v0, p7

    if-eq v0, v4, :cond_4

    .line 181
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/store/IndexInput;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 184
    :cond_4
    const/16 v24, 0x1

    .line 186
    if-nez v24, :cond_5

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 188
    aput-object v17, v4, v5

    const/4 v5, 0x1

    aput-object p0, v4, v5

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 191
    :cond_5
    return-void

    .line 153
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v18

    .line 154
    .local v18, "field":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v7

    .line 155
    .local v7, "numTerms":J
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_7

    const-wide/16 v4, 0x0

    cmp-long v4, v7, v4

    if-gez v4, :cond_7

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 156
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v21

    .line 157
    .local v21, "numBytes":I
    new-instance v9, Lorg/apache/lucene/util/BytesRef;

    move/from16 v0, v21

    new-array v4, v0, [B

    invoke-direct {v9, v4}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    .line 158
    .local v9, "rootCode":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    iget-object v5, v9, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    const/16 v25, 0x0

    move/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v4, v5, v0, v1}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 159
    move/from16 v0, v21

    iput v0, v9, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 160
    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v6

    .line 161
    .local v6, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    sget-boolean v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->$assertionsDisabled:Z

    if-nez v4, :cond_8

    if-nez v6, :cond_8

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "field="

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 162
    :cond_8
    invoke-virtual {v6}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v4

    sget-object v5, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-ne v4, v5, :cond_a

    const-wide/16 v10, -0x1

    .line 163
    .local v10, "sumTotalTermFreq":J
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v12

    .line 164
    .local v12, "sumDocFreq":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v14

    .line 165
    .local v14, "docCount":I
    if-ltz v14, :cond_9

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    if-le v14, v4, :cond_b

    .line 166
    :cond_9
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "invalid docCount: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " maxDoc: "

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v25

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " (resource="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 162
    .end local v10    # "sumTotalTermFreq":J
    .end local v12    # "sumDocFreq":J
    .end local v14    # "docCount":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v10

    goto :goto_1

    .line 168
    .restart local v10    # "sumTotalTermFreq":J
    .restart local v12    # "sumDocFreq":J
    .restart local v14    # "docCount":I
    :cond_b
    int-to-long v4, v14

    cmp-long v4, v12, v4

    if-gez v4, :cond_c

    .line 169
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "invalid sumDocFreq: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " docCount: "

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " (resource="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 171
    :cond_c
    const-wide/16 v4, -0x1

    cmp-long v4, v10, v4

    if-eqz v4, :cond_d

    cmp-long v4, v10, v12

    if-gez v4, :cond_d

    .line 172
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "invalid sumTotalTermFreq: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " sumDocFreq: "

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " (resource="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 174
    :cond_d
    const/4 v4, -0x1

    move/from16 v0, p7

    if-eq v0, v4, :cond_e

    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v15

    .line 175
    .local v15, "indexStartFP":J
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    move-object/from16 v25, v0

    iget-object v0, v6, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    new-instance v4, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v17}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;-><init>(Lorg/apache/lucene/codecs/BlockTreeTermsReader;Lorg/apache/lucene/index/FieldInfo;JLorg/apache/lucene/util/BytesRef;JJIJLorg/apache/lucene/store/IndexInput;)V

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    .line 176
    .local v23, "previous":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    if-eqz v23, :cond_f

    .line 177
    new-instance v4, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v25, "duplicate field: "

    move-object/from16 v0, v25

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v6, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, " (resource="

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v25, ")"

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 174
    .end local v15    # "indexStartFP":J
    .end local v23    # "previous":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    :cond_e
    const-wide/16 v15, 0x0

    goto :goto_2

    .line 152
    .restart local v15    # "indexStartFP":J
    .restart local v23    # "previous":Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;
    :cond_f
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/codecs/PostingsReaderBase;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method static synthetic access$2(Lorg/apache/lucene/codecs/BlockTreeTermsReader;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->segment:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method brToString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 3
    .param p1, "b"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 259
    if-nez p1, :cond_0

    .line 260
    const-string v1, "null"

    .line 268
    :goto_0
    return-object v1

    .line 263
    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->utf8ToString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 268
    .local v0, "t":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lorg/apache/lucene/util/BytesRef;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 233
    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->in:Lorg/apache/lucene/store/IndexInput;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->postingsReader:Lorg/apache/lucene/codecs/PostingsReaderBase;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 239
    return-void

    .line 234
    :catchall_0
    move-exception v0

    .line 237
    iget-object v1, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->clear()V

    .line 238
    throw v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method protected readHeader(Lorg/apache/lucene/store/IndexInput;)I
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 195
    const-string v1, "BLOCK_TREE_TERMS_DICT"

    .line 196
    const/4 v2, 0x0

    .line 195
    invoke-static {p1, v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v0

    .line 198
    .local v0, "version":I
    if-ge v0, v3, :cond_0

    .line 199
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->dirOffset:J

    .line 201
    :cond_0
    return v0
.end method

.method protected readIndexHeader(Lorg/apache/lucene/store/IndexInput;)I
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 206
    const-string v1, "BLOCK_TREE_TERMS_INDEX"

    .line 207
    const/4 v2, 0x0

    .line 206
    invoke-static {p1, v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    move-result v0

    .line 209
    .local v0, "version":I
    if-ge v0, v3, :cond_0

    .line 210
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->indexDirOffset:J

    .line 212
    :cond_0
    return v0
.end method

.method protected seekDir(Lorg/apache/lucene/store/IndexInput;J)V
    .locals 4
    .param p1, "input"    # Lorg/apache/lucene/store/IndexInput;
    .param p2, "dirOffset"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    iget v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->version:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 219
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x8

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 220
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide p2

    .line 222
    :cond_0
    invoke-virtual {p1, p2, p3}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 223
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 248
    sget-boolean v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 249
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/codecs/BlockTreeTermsReader;->fields:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Terms;

    return-object v0
.end method
