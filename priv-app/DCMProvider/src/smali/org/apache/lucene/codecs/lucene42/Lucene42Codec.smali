.class public Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;
.super Lorg/apache/lucene/codecs/Codec;
.source "Lucene42Codec.java"


# instance fields
.field private final defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

.field private final fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

.field private final fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

.field private final infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

.field private final liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

.field private final normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

.field private final postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

.field private final vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "Lucene42"

    invoke-direct {p0, v0}, Lorg/apache/lucene/codecs/Codec;-><init>(Ljava/lang/String;)V

    .line 50
    new-instance v0, Lorg/apache/lucene/codecs/lucene41/Lucene41StoredFieldsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene41/Lucene41StoredFieldsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    .line 51
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42TermVectorsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42TermVectorsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    .line 52
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42FieldInfosFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    .line 53
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40SegmentInfoFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    .line 54
    new-instance v0, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene40/Lucene40LiveDocsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    .line 56
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec$1;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 64
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec$2;-><init>(Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;)V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 129
    const-string v0, "Lucene41"

    invoke-static {v0}, Lorg/apache/lucene/codecs/PostingsFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    .line 130
    const-string v0, "Lucene42"

    invoke-static {v0}, Lorg/apache/lucene/codecs/DocValuesFormat;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    .line 132
    new-instance v0, Lorg/apache/lucene/codecs/lucene42/Lucene42NormsFormat;

    invoke-direct {v0}, Lorg/apache/lucene/codecs/lucene42/Lucene42NormsFormat;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    .line 74
    return-void
.end method


# virtual methods
.method public final docValuesFormat()Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->docValuesFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public final fieldInfosFormat()Lorg/apache/lucene/codecs/FieldInfosFormat;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->fieldInfosFormat:Lorg/apache/lucene/codecs/FieldInfosFormat;

    return-object v0
.end method

.method public getDocValuesFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/DocValuesFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->defaultDVFormat:Lorg/apache/lucene/codecs/DocValuesFormat;

    return-object v0
.end method

.method public getPostingsFormatForField(Ljava/lang/String;)Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->defaultFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->liveDocsFormat:Lorg/apache/lucene/codecs/LiveDocsFormat;

    return-object v0
.end method

.method public final normsFormat()Lorg/apache/lucene/codecs/NormsFormat;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->normsFormat:Lorg/apache/lucene/codecs/NormsFormat;

    return-object v0
.end method

.method public final postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->postingsFormat:Lorg/apache/lucene/codecs/PostingsFormat;

    return-object v0
.end method

.method public final segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->infosFormat:Lorg/apache/lucene/codecs/SegmentInfoFormat;

    return-object v0
.end method

.method public final storedFieldsFormat()Lorg/apache/lucene/codecs/StoredFieldsFormat;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->fieldsFormat:Lorg/apache/lucene/codecs/StoredFieldsFormat;

    return-object v0
.end method

.method public final termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/codecs/lucene42/Lucene42Codec;->vectorsFormat:Lorg/apache/lucene/codecs/TermVectorsFormat;

    return-object v0
.end method
