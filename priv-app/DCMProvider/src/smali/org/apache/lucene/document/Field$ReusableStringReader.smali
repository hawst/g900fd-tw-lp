.class final Lorg/apache/lucene/document/Field$ReusableStringReader;
.super Ljava/io/Reader;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ReusableStringReader"
.end annotation


# instance fields
.field private pos:I

.field private s:Ljava/lang/String;

.field private size:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 563
    invoke-direct {p0}, Ljava/io/Reader;-><init>()V

    .line 564
    iput v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    iput v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->size:I

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    .line 563
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->size:I

    iput v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    .line 599
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    .line 600
    return-void
.end method

.method public read()I
    .locals 3

    .prologue
    .line 575
    iget v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    iget v1, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->size:I

    if-ge v0, v1, :cond_0

    .line 576
    iget-object v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    iget v1, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 579
    :goto_0
    return v0

    .line 578
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    .line 579
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public read([CII)I
    .locals 3
    .param p1, "c"    # [C
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 585
    iget v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    iget v1, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->size:I

    if-ge v0, v1, :cond_0

    .line 586
    iget v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->size:I

    iget v1, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 587
    iget-object v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    iget v1, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    iget v2, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    add-int/2addr v2, p3

    invoke-virtual {v0, v1, v2, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    .line 588
    iget v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    move v0, p3

    .line 592
    :goto_0
    return v0

    .line 591
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    .line 592
    const/4 v0, -0x1

    goto :goto_0
.end method

.method setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 568
    iput-object p1, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->s:Ljava/lang/String;

    .line 569
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->size:I

    .line 570
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/document/Field$ReusableStringReader;->pos:I

    .line 571
    return-void
.end method
