.class public Lorg/apache/lucene/document/SortedBytesDocValuesField;
.super Lorg/apache/lucene/document/SortedDocValuesField;
.source "SortedBytesDocValuesField.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final TYPE_FIXED_LEN:Lorg/apache/lucene/document/FieldType;

.field public static final TYPE_VAR_LEN:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lorg/apache/lucene/document/SortedDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    sput-object v0, Lorg/apache/lucene/document/SortedBytesDocValuesField;->TYPE_FIXED_LEN:Lorg/apache/lucene/document/FieldType;

    .line 51
    sget-object v0, Lorg/apache/lucene/document/SortedDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    sput-object v0, Lorg/apache/lucene/document/SortedBytesDocValuesField;->TYPE_VAR_LEN:Lorg/apache/lucene/document/FieldType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/SortedDocValuesField;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Z)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "isFixedLength"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/SortedDocValuesField;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 72
    return-void
.end method
