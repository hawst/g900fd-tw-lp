.class public Lorg/apache/lucene/document/ByteDocValuesField;
.super Lorg/apache/lucene/document/NumericDocValuesField;
.source "ByteDocValuesField.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;B)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # B

    .prologue
    .line 49
    int-to-long v0, p2

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;-><init>(Ljava/lang/String;J)V

    .line 50
    return-void
.end method


# virtual methods
.method public setByteValue(B)V
    .locals 2
    .param p1, "value"    # B

    .prologue
    .line 54
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/document/ByteDocValuesField;->setLongValue(J)V

    .line 55
    return-void
.end method
