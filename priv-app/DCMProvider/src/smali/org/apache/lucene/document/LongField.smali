.class public final Lorg/apache/lucene/document/LongField;
.super Lorg/apache/lucene/document/Field;
.source "LongField.java"


# static fields
.field public static final TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

.field public static final TYPE_STORED:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 131
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    .line 133
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 134
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 135
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 136
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 137
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setNumericType(Lorg/apache/lucene/document/FieldType$NumericType;)V

    .line 138
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 145
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    .line 147
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 148
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 149
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 150
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 151
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setNumericType(Lorg/apache/lucene/document/FieldType$NumericType;)V

    .line 152
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 153
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 154
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLorg/apache/lucene/document/Field$Store;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J
    .param p4, "stored"    # Lorg/apache/lucene/document/Field$Store;

    .prologue
    .line 165
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    if-ne p4, v0, :cond_0

    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 166
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/LongField;->fieldsData:Ljava/lang/Object;

    .line 167
    return-void

    .line 165
    :cond_0
    sget-object v0, Lorg/apache/lucene/document/LongField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;JLorg/apache/lucene/document/FieldType;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J
    .param p4, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 179
    invoke-direct {p0, p1, p4}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 180
    invoke-virtual {p4}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eq v0, v1, :cond_0

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "type.numericType() must be LONG but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/LongField;->fieldsData:Ljava/lang/Object;

    .line 184
    return-void
.end method
