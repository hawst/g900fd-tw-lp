.class public Lorg/apache/lucene/document/NumericDocValuesField;
.super Lorg/apache/lucene/document/Field;
.source "NumericDocValuesField.java"


# static fields
.field public static final TYPE:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/NumericDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    .line 43
    sget-object v0, Lorg/apache/lucene/document/NumericDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setDocValueType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 44
    sget-object v0, Lorg/apache/lucene/document/NumericDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 54
    sget-object v0, Lorg/apache/lucene/document/NumericDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 55
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/NumericDocValuesField;->fieldsData:Ljava/lang/Object;

    .line 56
    return-void
.end method
