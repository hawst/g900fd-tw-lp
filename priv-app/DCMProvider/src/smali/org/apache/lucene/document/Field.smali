.class public Lorg/apache/lucene/document/Field;
.super Ljava/lang/Object;
.source "Field.java"

# interfaces
.implements Lorg/apache/lucene/index/IndexableField;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/document/Field$Index;,
        Lorg/apache/lucene/document/Field$ReusableStringReader;,
        Lorg/apache/lucene/document/Field$Store;,
        Lorg/apache/lucene/document/Field$StringTokenStream;,
        Lorg/apache/lucene/document/Field$TermVector;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$Field$Index:[I

.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$Field$TermVector:[I

.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I


# instance fields
.field protected boost:F

.field protected fieldsData:Ljava/lang/Object;

.field private transient internalReader:Lorg/apache/lucene/document/Field$ReusableStringReader;

.field private transient internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

.field protected final name:Ljava/lang/String;

.field protected tokenStream:Lorg/apache/lucene/analysis/TokenStream;

.field protected final type:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$Field$Index()[I
    .locals 3

    .prologue
    .line 55
    sget-object v0, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$Field$Index:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/Field$Index;->values()[Lorg/apache/lucene/document/Field$Index;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/Field$Index;->ANALYZED:Lorg/apache/lucene/document/Field$Index;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$Index;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$Index;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$Index;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$Index;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$Index;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$Field$Index:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$Field$TermVector()[I
    .locals 3

    .prologue
    .line 55
    sget-object v0, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$Field$TermVector:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/Field$TermVector;->values()[Lorg/apache/lucene/document/Field$TermVector;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$TermVector;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->WITH_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$TermVector;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS:Lorg/apache/lucene/document/Field$TermVector;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$TermVector;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$TermVector;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->YES:Lorg/apache/lucene/document/Field$TermVector;

    invoke-virtual {v1}, Lorg/apache/lucene/document/Field$TermVector;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$Field$TermVector:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I
    .locals 3

    .prologue
    .line 55
    sget-object v0, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/FieldType$NumericType;->values()[Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/Reader;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 996
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 997
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .param p3, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1014
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->ANALYZED:Lorg/apache/lucene/document/Field$Index;

    invoke-static {v0, v1, p3}, Lorg/apache/lucene/document/Field;->translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/FieldType;)V

    .line 1015
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/FieldType;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/Field;->boost:F

    .line 114
    if-nez p1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    if-nez p3, :cond_1

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "type cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    if-nez p2, :cond_2

    .line 121
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "reader cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_2
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->stored()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fields with a Reader value cannot be stored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_3
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->tokenized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "non-tokenized fields must use String values"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_4
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 131
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 132
    iput-object p3, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    .line 133
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "index"    # Lorg/apache/lucene/document/Field$Index;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 956
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-static {p3, p4, v0}, Lorg/apache/lucene/document/Field;->translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 957
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p4, "index"    # Lorg/apache/lucene/document/Field$Index;
    .param p5, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 979
    invoke-static {p3, p4, p5}, Lorg/apache/lucene/document/Field;->translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 980
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/Field;->boost:F

    .line 236
    if-nez p1, :cond_0

    .line 237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_0
    if-nez p2, :cond_1

    .line 240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_1
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->stored()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 243
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "it doesn\'t make sense to have a field that is neither indexed nor stored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_2
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->storeTermVectors()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot store term vector information for a field that is not indexed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_3
    iput-object p3, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    .line 252
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 253
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 254
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1032
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/Field$TermVector;)V

    .line 1033
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/Field$TermVector;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1051
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->NO:Lorg/apache/lucene/document/Field$Store;

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->ANALYZED:Lorg/apache/lucene/document/Field$Index;

    invoke-static {v0, v1, p3}, Lorg/apache/lucene/document/Field;->translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V

    .line 1052
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/Field;->boost:F

    .line 146
    if-nez p1, :cond_0

    .line 147
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_0
    if-nez p2, :cond_1

    .line 150
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "tokenStream cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_1
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->tokenized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 153
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TokenStream fields must be indexed and tokenized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_3
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->stored()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TokenStream fields cannot be stored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_4
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 161
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    .line 162
    iput-object p3, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    .line 163
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/Field;->boost:F

    .line 93
    if-nez p1, :cond_0

    .line 94
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 97
    if-nez p2, :cond_1

    .line 98
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "type cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    .line 101
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/document/FieldType;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/lucene/document/Field;->boost:F

    .line 212
    if-nez p1, :cond_0

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Fields with BytesRef values cannot be indexed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_1
    iput-object p2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 219
    iput-object p3, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    .line 220
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1064
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    sget-object v2, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/document/Field;->translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BLorg/apache/lucene/document/FieldType;)V

    .line 1065
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BII)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1079
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    sget-object v2, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/document/Field;->translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BIILorg/apache/lucene/document/FieldType;)V

    .line 1080
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BIILorg/apache/lucene/document/FieldType;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .param p5, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 196
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p2, p3, p4}, Lorg/apache/lucene/util/BytesRef;-><init>([BII)V

    invoke-direct {p0, p1, v0, p5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/document/FieldType;)V

    .line 197
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BLorg/apache/lucene/document/FieldType;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 178
    const/4 v3, 0x0

    array-length v4, p2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BIILorg/apache/lucene/document/FieldType;)V

    .line 179
    return-void
.end method

.method public static final translateFieldType(Lorg/apache/lucene/document/Field$Store;Lorg/apache/lucene/document/Field$Index;Lorg/apache/lucene/document/Field$TermVector;)Lorg/apache/lucene/document/FieldType;
    .locals 5
    .param p0, "store"    # Lorg/apache/lucene/document/Field$Store;
    .param p1, "index"    # Lorg/apache/lucene/document/Field$Index;
    .param p2, "termVector"    # Lorg/apache/lucene/document/Field$TermVector;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 890
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    .line 892
    .local v0, "ft":Lorg/apache/lucene/document/FieldType;
    sget-object v1, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    if-ne p0, v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 894
    invoke-static {}, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$Field$Index()[I

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/lucene/document/Field$Index;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 915
    :goto_1
    invoke-static {}, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$Field$TermVector()[I

    move-result-object v1

    .line 917
    invoke-virtual {p2}, Lorg/apache/lucene/document/Field$TermVector;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_1

    .line 937
    :goto_2
    :pswitch_0
    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 938
    return-object v0

    :cond_0
    move v1, v3

    .line 892
    goto :goto_0

    .line 896
    :pswitch_1
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 897
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    goto :goto_1

    .line 900
    :pswitch_2
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 901
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 902
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    goto :goto_1

    .line 905
    :pswitch_3
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 906
    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    goto :goto_1

    .line 909
    :pswitch_4
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 910
    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 911
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    goto :goto_1

    .line 921
    :pswitch_5
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectors(Z)V

    goto :goto_2

    .line 924
    :pswitch_6
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectors(Z)V

    .line 925
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectorPositions(Z)V

    goto :goto_2

    .line 928
    :pswitch_7
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectors(Z)V

    .line 929
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectorOffsets(Z)V

    goto :goto_2

    .line 932
    :pswitch_8
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectors(Z)V

    .line 933
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectorPositions(Z)V

    .line 934
    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectorOffsets(Z)V

    goto :goto_2

    .line 894
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch

    .line 917
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public binaryValue()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    check-cast v0, Lorg/apache/lucene/util/BytesRef;

    .line 473
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public boost()F
    .locals 1

    .prologue
    .line 441
    iget v0, p0, Lorg/apache/lucene/document/Field;->boost:F

    return v0
.end method

.method public fieldType()Lorg/apache/lucene/document/FieldType;
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    return-object v0
.end method

.method public bridge synthetic fieldType()Lorg/apache/lucene/index/IndexableFieldType;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->fieldType()Lorg/apache/lucene/document/FieldType;

    move-result-object v0

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    return-object v0
.end method

.method public numericValue()Ljava/lang/Number;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    .line 464
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readerValue()Ljava/io/Reader;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/io/Reader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    check-cast v0, Ljava/io/Reader;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBoost(F)V
    .locals 2
    .param p1, "boost"    # F

    .prologue
    .line 451
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->omitNorms()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 453
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You cannot set an index-time boost on an unindexed field, or one that omits norms"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_1
    iput p1, p0, Lorg/apache/lucene/document/Field;->boost:F

    .line 457
    return-void
.end method

.method public setByteValue(B)V
    .locals 3
    .param p1, "value"    # B

    .prologue
    .line 352
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Byte;

    if-nez v0, :cond_0

    .line 353
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Byte"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_0
    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 356
    return-void
.end method

.method public setBytesValue(Lorg/apache/lucene/util/BytesRef;)V
    .locals 3
    .param p1, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 338
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/lucene/util/BytesRef;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to BytesRef"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot set a BytesRef value on an indexed field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 344
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 345
    return-void
.end method

.method public setBytesValue([B)V
    .locals 1
    .param p1, "value"    # [B

    .prologue
    .line 327
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p1}, Lorg/apache/lucene/util/BytesRef;-><init>([B)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/document/Field;->setBytesValue(Lorg/apache/lucene/util/BytesRef;)V

    .line 328
    return-void
.end method

.method public setDoubleValue(D)V
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 407
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Double;

    if-nez v0, :cond_0

    .line 408
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Double"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 411
    return-void
.end method

.method public setFloatValue(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 396
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Float;

    if-nez v0, :cond_0

    .line 397
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Float"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 399
    :cond_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 400
    return-void
.end method

.method public setIntValue(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 374
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 375
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Integer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 378
    return-void
.end method

.method public setLongValue(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 385
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 386
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Long"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 389
    return-void
.end method

.method public setReaderValue(Ljava/io/Reader;)V
    .locals 3
    .param p1, "value"    # Ljava/io/Reader;

    .prologue
    .line 316
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/io/Reader;

    if-nez v0, :cond_0

    .line 317
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Reader"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 320
    return-void
.end method

.method public setShortValue(S)V
    .locals 3
    .param p1, "value"    # S

    .prologue
    .line 363
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Short;

    if-nez v0, :cond_0

    .line 364
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to Short"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_0
    invoke-static {p1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 367
    return-void
.end method

.method public setStringValue(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 305
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot change value type from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to String"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    .line 309
    return-void
.end method

.method public setTokenStream(Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 2
    .param p1, "tokenStream"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 419
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->tokenized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 420
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TokenStream fields must be indexed and tokenized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 422
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 423
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot set private TokenStream on numeric fields"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_2
    iput-object p1, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    .line 426
    return-void
.end method

.method public stringValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 264
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 480
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 481
    .local v0, "result":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 482
    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 483
    iget-object v1, p0, Lorg/apache/lucene/document/Field;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 486
    iget-object v1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 487
    iget-object v1, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 490
    :cond_0
    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 491
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public tokenStream(Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/TokenStream;
    .locals 6
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 502
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->fieldType()Lorg/apache/lucene/document/FieldType;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 503
    const/4 v3, 0x0

    .line 557
    :goto_0
    return-object v3

    .line 506
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->fieldType()Lorg/apache/lucene/document/FieldType;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v1

    .line 507
    .local v1, "numericType":Lorg/apache/lucene/document/FieldType$NumericType;
    if-eqz v1, :cond_2

    .line 508
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    instance-of v3, v3, Lorg/apache/lucene/analysis/NumericTokenStream;

    if-nez v3, :cond_1

    .line 511
    new-instance v3, Lorg/apache/lucene/analysis/NumericTokenStream;

    iget-object v4, p0, Lorg/apache/lucene/document/Field;->type:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v4}, Lorg/apache/lucene/document/FieldType;->numericPrecisionStep()I

    move-result v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/analysis/NumericTokenStream;-><init>(I)V

    iput-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    .line 513
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    check-cast v0, Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 515
    .local v0, "nts":Lorg/apache/lucene/analysis/NumericTokenStream;
    iget-object v2, p0, Lorg/apache/lucene/document/Field;->fieldsData:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Number;

    .line 516
    .local v2, "val":Ljava/lang/Number;
    invoke-static {}, Lorg/apache/lucene/document/Field;->$SWITCH_TABLE$org$apache$lucene$document$FieldType$NumericType()[I

    move-result-object v3

    invoke-virtual {v1}, Lorg/apache/lucene/document/FieldType$NumericType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 530
    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "Should never get here"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 518
    :pswitch_0
    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/analysis/NumericTokenStream;->setIntValue(I)Lorg/apache/lucene/analysis/NumericTokenStream;

    .line 532
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    goto :goto_0

    .line 521
    :pswitch_1
    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/analysis/NumericTokenStream;->setLongValue(J)Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_1

    .line 524
    :pswitch_2
    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/analysis/NumericTokenStream;->setFloatValue(F)Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_1

    .line 527
    :pswitch_3
    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lorg/apache/lucene/analysis/NumericTokenStream;->setDoubleValue(D)Lorg/apache/lucene/analysis/NumericTokenStream;

    goto :goto_1

    .line 535
    .end local v0    # "nts":Lorg/apache/lucene/analysis/NumericTokenStream;
    .end local v2    # "val":Ljava/lang/Number;
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->fieldType()Lorg/apache/lucene/document/FieldType;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/document/FieldType;->tokenized()Z

    move-result v3

    if-nez v3, :cond_5

    .line 536
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->stringValue()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 537
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Non-Tokenized Fields must have a String value"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 539
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    instance-of v3, v3, Lorg/apache/lucene/document/Field$StringTokenStream;

    if-nez v3, :cond_4

    .line 542
    new-instance v3, Lorg/apache/lucene/document/Field$StringTokenStream;

    invoke-direct {v3}, Lorg/apache/lucene/document/Field$StringTokenStream;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    .line 544
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    check-cast v3, Lorg/apache/lucene/document/Field$StringTokenStream;

    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/document/Field$StringTokenStream;->setValue(Ljava/lang/String;)V

    .line 545
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalTokenStream:Lorg/apache/lucene/analysis/TokenStream;

    goto/16 :goto_0

    .line 548
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    if-eqz v3, :cond_6

    .line 549
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    goto/16 :goto_0

    .line 550
    :cond_6
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->readerValue()Ljava/io/Reader;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 551
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->readerValue()Ljava/io/Reader;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    goto/16 :goto_0

    .line 552
    :cond_7
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->stringValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 553
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalReader:Lorg/apache/lucene/document/Field$ReusableStringReader;

    if-nez v3, :cond_8

    .line 554
    new-instance v3, Lorg/apache/lucene/document/Field$ReusableStringReader;

    invoke-direct {v3}, Lorg/apache/lucene/document/Field$ReusableStringReader;-><init>()V

    iput-object v3, p0, Lorg/apache/lucene/document/Field;->internalReader:Lorg/apache/lucene/document/Field$ReusableStringReader;

    .line 556
    :cond_8
    iget-object v3, p0, Lorg/apache/lucene/document/Field;->internalReader:Lorg/apache/lucene/document/Field$ReusableStringReader;

    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->stringValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/lucene/document/Field$ReusableStringReader;->setValue(Ljava/lang/String;)V

    .line 557
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/document/Field;->internalReader:Lorg/apache/lucene/document/Field$ReusableStringReader;

    invoke-virtual {p1, v3, v4}, Lorg/apache/lucene/analysis/Analyzer;->tokenStream(Ljava/lang/String;Ljava/io/Reader;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v3

    goto/16 :goto_0

    .line 560
    :cond_9
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Field must have either TokenStream, String, Reader or Number value"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 516
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public tokenStreamValue()Lorg/apache/lucene/analysis/TokenStream;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lorg/apache/lucene/document/Field;->tokenStream:Lorg/apache/lucene/analysis/TokenStream;

    return-object v0
.end method
