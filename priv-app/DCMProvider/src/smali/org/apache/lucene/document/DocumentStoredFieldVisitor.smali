.class public Lorg/apache/lucene/document/DocumentStoredFieldVisitor;
.super Lorg/apache/lucene/index/StoredFieldVisitor;
.source "DocumentStoredFieldVisitor.java"


# instance fields
.field private final doc:Lorg/apache/lucene/document/Document;

.field private final fieldsToAdd:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldVisitor;-><init>()V

    .line 38
    new-instance v0, Lorg/apache/lucene/document/Document;

    invoke-direct {v0}, Lorg/apache/lucene/document/Document;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->fieldsToAdd:Ljava/util/Set;

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "fieldsToAdd":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldVisitor;-><init>()V

    .line 38
    new-instance v0, Lorg/apache/lucene/document/Document;

    invoke-direct {v0}, Lorg/apache/lucene/document/Document;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    .line 46
    iput-object p1, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->fieldsToAdd:Ljava/util/Set;

    .line 47
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 4
    .param p1, "fields"    # [Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/index/StoredFieldVisitor;-><init>()V

    .line 38
    new-instance v1, Lorg/apache/lucene/document/Document;

    invoke-direct {v1}, Lorg/apache/lucene/document/Document;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    .line 51
    new-instance v1, Ljava/util/HashSet;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->fieldsToAdd:Ljava/util/Set;

    .line 52
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 55
    return-void

    .line 52
    :cond_0
    aget-object v0, p1, v1

    .line 53
    .local v0, "field":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->fieldsToAdd:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public binaryField(Lorg/apache/lucene/index/FieldInfo;[B)V
    .locals 3
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    new-instance v1, Lorg/apache/lucene/document/StoredField;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 65
    return-void
.end method

.method public doubleField(Lorg/apache/lucene/index/FieldInfo;D)V
    .locals 4
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # D

    .prologue
    .line 94
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    new-instance v1, Lorg/apache/lucene/document/StoredField;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, p2, p3}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;D)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 95
    return-void
.end method

.method public floatField(Lorg/apache/lucene/index/FieldInfo;F)V
    .locals 3
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # F

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    new-instance v1, Lorg/apache/lucene/document/StoredField;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;F)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 90
    return-void
.end method

.method public getDocument()Lorg/apache/lucene/document/Document;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    return-object v0
.end method

.method public intField(Lorg/apache/lucene/index/FieldInfo;I)V
    .locals 3
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    new-instance v1, Lorg/apache/lucene/document/StoredField;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, p2}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 80
    return-void
.end method

.method public longField(Lorg/apache/lucene/index/FieldInfo;J)V
    .locals 4
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # J

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    new-instance v1, Lorg/apache/lucene/document/StoredField;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, p2, p3}, Lorg/apache/lucene/document/StoredField;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 85
    return-void
.end method

.method public needsField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;
    .locals 2
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->fieldsToAdd:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->fieldsToAdd:Ljava/util/Set;

    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    goto :goto_0
.end method

.method public stringField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/String;)V
    .locals 4
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0, v1}, Lorg/apache/lucene/document/FieldType;-><init>(Lorg/apache/lucene/document/FieldType;)V

    .line 70
    .local v0, "ft":Lorg/apache/lucene/document/FieldType;
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setStoreTermVectors(Z)V

    .line 71
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 72
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 73
    invoke-virtual {p1}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->doc:Lorg/apache/lucene/document/Document;

    new-instance v2, Lorg/apache/lucene/document/Field;

    iget-object v3, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    invoke-virtual {v1, v2}, Lorg/apache/lucene/document/Document;->add(Lorg/apache/lucene/index/IndexableField;)V

    .line 75
    return-void
.end method
