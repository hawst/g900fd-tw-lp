.class public Lorg/apache/lucene/document/DoubleDocValuesField;
.super Lorg/apache/lucene/document/NumericDocValuesField;
.source "DoubleDocValuesField.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;D)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # D

    .prologue
    .line 43
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;-><init>(Ljava/lang/String;J)V

    .line 44
    return-void
.end method


# virtual methods
.method public setDoubleValue(D)V
    .locals 3
    .param p1, "value"    # D

    .prologue
    .line 48
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;->setLongValue(J)V

    .line 49
    return-void
.end method

.method public setLongValue(J)V
    .locals 2
    .param p1, "value"    # J

    .prologue
    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot change value type from Double to Long"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
