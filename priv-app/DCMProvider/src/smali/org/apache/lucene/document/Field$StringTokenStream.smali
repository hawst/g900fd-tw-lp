.class final Lorg/apache/lucene/document/Field$StringTokenStream;
.super Lorg/apache/lucene/analysis/TokenStream;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "StringTokenStream"
.end annotation


# instance fields
.field private final offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

.field private final termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

.field private used:Z

.field private value:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 613
    invoke-direct {p0}, Lorg/apache/lucene/analysis/TokenStream;-><init>()V

    .line 604
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/document/Field$StringTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iput-object v0, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 605
    const-class v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/document/Field$StringTokenStream;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iput-object v0, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 606
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->used:Z

    .line 607
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->value:Ljava/lang/String;

    .line 614
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 646
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->value:Ljava/lang/String;

    .line 647
    return-void
.end method

.method public end()V
    .locals 2

    .prologue
    .line 635
    iget-object v1, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 636
    .local v0, "finalOffset":I
    iget-object v1, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-interface {v1, v0, v0}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 637
    return-void
.end method

.method public incrementToken()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 623
    iget-boolean v2, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->used:Z

    if-eqz v2, :cond_0

    .line 630
    :goto_0
    return v0

    .line 626
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/document/Field$StringTokenStream;->clearAttributes()V

    .line 627
    iget-object v2, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->termAttribute:Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    iget-object v3, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->value:Ljava/lang/String;

    invoke-interface {v2, v3}, Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;->append(Ljava/lang/String;)Lorg/apache/lucene/analysis/tokenattributes/CharTermAttribute;

    .line 628
    iget-object v2, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->offsetAttribute:Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    iget-object v3, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->value:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-interface {v2, v0, v3}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->setOffset(II)V

    .line 629
    iput-boolean v1, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->used:Z

    move v0, v1

    .line 630
    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 641
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->used:Z

    .line 642
    return-void
.end method

.method setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 618
    iput-object p1, p0, Lorg/apache/lucene/document/Field$StringTokenStream;->value:Ljava/lang/String;

    .line 619
    return-void
.end method
