.class public final Lorg/apache/lucene/document/TextField;
.super Lorg/apache/lucene/document/Field;
.source "TextField.java"


# static fields
.field public static final TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

.field public static final TYPE_STORED:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 31
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    .line 34
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    .line 37
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 38
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 39
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 41
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 42
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 43
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 44
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/Reader;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "reader"    # Ljava/io/Reader;

    .prologue
    .line 56
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/document/FieldType;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "store"    # Lorg/apache/lucene/document/Field$Store;

    .prologue
    .line 66
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    if-ne p3, v0, :cond_0

    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 67
    return-void

    .line 66
    :cond_0
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "stream"    # Lorg/apache/lucene/analysis/TokenStream;

    .prologue
    .line 76
    sget-object v0, Lorg/apache/lucene/document/TextField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/document/FieldType;)V

    .line 77
    return-void
.end method
