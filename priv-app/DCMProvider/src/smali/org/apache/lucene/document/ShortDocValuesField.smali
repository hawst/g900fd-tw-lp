.class public Lorg/apache/lucene/document/ShortDocValuesField;
.super Lorg/apache/lucene/document/NumericDocValuesField;
.source "ShortDocValuesField.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;S)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # S

    .prologue
    .line 48
    int-to-long v0, p2

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;-><init>(Ljava/lang/String;J)V

    .line 49
    return-void
.end method


# virtual methods
.method public setShortValue(S)V
    .locals 2
    .param p1, "value"    # S

    .prologue
    .line 53
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/document/ShortDocValuesField;->setLongValue(J)V

    .line 54
    return-void
.end method
