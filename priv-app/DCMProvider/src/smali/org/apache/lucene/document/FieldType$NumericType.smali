.class public final enum Lorg/apache/lucene/document/FieldType$NumericType;
.super Ljava/lang/Enum;
.source "FieldType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/FieldType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NumericType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/FieldType$NumericType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/document/FieldType$NumericType;

.field public static final enum FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

.field public static final enum INT:Lorg/apache/lucene/document/FieldType$NumericType;

.field public static final enum LONG:Lorg/apache/lucene/document/FieldType$NumericType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lorg/apache/lucene/document/FieldType$NumericType;

    const-string v1, "INT"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/FieldType$NumericType;-><init>(Ljava/lang/String;I)V

    .line 37
    sput-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 38
    new-instance v0, Lorg/apache/lucene/document/FieldType$NumericType;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/document/FieldType$NumericType;-><init>(Ljava/lang/String;I)V

    .line 39
    sput-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 40
    new-instance v0, Lorg/apache/lucene/document/FieldType$NumericType;

    const-string v1, "FLOAT"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/document/FieldType$NumericType;-><init>(Ljava/lang/String;I)V

    .line 41
    sput-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 42
    new-instance v0, Lorg/apache/lucene/document/FieldType$NumericType;

    const-string v1, "DOUBLE"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/document/FieldType$NumericType;-><init>(Ljava/lang/String;I)V

    .line 43
    sput-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/document/FieldType$NumericType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->INT:Lorg/apache/lucene/document/FieldType$NumericType;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->LONG:Lorg/apache/lucene/document/FieldType$NumericType;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->DOUBLE:Lorg/apache/lucene/document/FieldType$NumericType;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->ENUM$VALUES:[Lorg/apache/lucene/document/FieldType$NumericType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/FieldType$NumericType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/FieldType$NumericType;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/document/FieldType$NumericType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/document/FieldType$NumericType;->ENUM$VALUES:[Lorg/apache/lucene/document/FieldType$NumericType;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
