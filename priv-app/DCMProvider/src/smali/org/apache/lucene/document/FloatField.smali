.class public final Lorg/apache/lucene/document/FloatField;
.super Lorg/apache/lucene/document/Field;
.source "FloatField.java"


# static fields
.field public static final TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

.field public static final TYPE_STORED:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 121
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    .line 123
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 124
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 125
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 126
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 127
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setNumericType(Lorg/apache/lucene/document/FieldType$NumericType;)V

    .line 128
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 135
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    .line 137
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 138
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 139
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 140
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 141
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setNumericType(Lorg/apache/lucene/document/FieldType$NumericType;)V

    .line 142
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 143
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 144
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;FLorg/apache/lucene/document/Field$Store;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # F
    .param p3, "stored"    # Lorg/apache/lucene/document/Field$Store;

    .prologue
    .line 155
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    if-ne p3, v0, :cond_0

    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 156
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/FloatField;->fieldsData:Ljava/lang/Object;

    .line 157
    return-void

    .line 155
    :cond_0
    sget-object v0, Lorg/apache/lucene/document/FloatField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;FLorg/apache/lucene/document/FieldType;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # F
    .param p3, "type"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 169
    invoke-direct {p0, p1, p3}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 170
    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    sget-object v1, Lorg/apache/lucene/document/FieldType$NumericType;->FLOAT:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eq v0, v1, :cond_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "type.numericType() must be FLOAT but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_0
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/FloatField;->fieldsData:Ljava/lang/Object;

    .line 174
    return-void
.end method
