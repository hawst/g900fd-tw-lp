.class public final Lorg/apache/lucene/document/StringField;
.super Lorg/apache/lucene/document/Field;
.source "StringField.java"


# static fields
.field public static final TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

.field public static final TYPE_STORED:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 32
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    .line 36
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    .line 39
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 40
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 41
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 42
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 43
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 45
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setIndexed(Z)V

    .line 46
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setOmitNorms(Z)V

    .line 47
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V

    .line 48
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v2}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 49
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/document/FieldType;->setTokenized(Z)V

    .line 50
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/Field$Store;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "stored"    # Lorg/apache/lucene/document/Field$Store;

    .prologue
    .line 60
    sget-object v0, Lorg/apache/lucene/document/Field$Store;->YES:Lorg/apache/lucene/document/Field$Store;

    if-ne p3, v0, :cond_0

    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_STORED:Lorg/apache/lucene/document/FieldType;

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 61
    return-void

    .line 60
    :cond_0
    sget-object v0, Lorg/apache/lucene/document/StringField;->TYPE_NOT_STORED:Lorg/apache/lucene/document/FieldType;

    goto :goto_0
.end method
