.class public Lorg/apache/lucene/document/FieldType;
.super Ljava/lang/Object;
.source "FieldType.java"

# interfaces
.implements Lorg/apache/lucene/index/IndexableFieldType;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/document/FieldType$NumericType;
    }
.end annotation


# instance fields
.field private docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

.field private frozen:Z

.field private indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

.field private indexed:Z

.field private numericPrecisionStep:I

.field private numericType:Lorg/apache/lucene/document/FieldType$NumericType;

.field private omitNorms:Z

.field private storeTermVectorOffsets:Z

.field private storeTermVectorPayloads:Z

.field private storeTermVectorPositions:Z

.field private storeTermVectors:Z

.field private stored:Z

.field private tokenized:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->tokenized:Z

    .line 54
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 57
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/lucene/document/FieldType;->numericPrecisionStep:I

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/document/FieldType;)V
    .locals 1
    .param p1, "ref"    # Lorg/apache/lucene/document/FieldType;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->tokenized:Z

    .line 54
    sget-object v0, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    iput-object v0, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 57
    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/lucene/document/FieldType;->numericPrecisionStep:I

    .line 64
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->indexed:Z

    .line 65
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->stored()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->stored:Z

    .line 66
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->tokenized()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->tokenized:Z

    .line 67
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->storeTermVectors()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectors:Z

    .line 68
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->storeTermVectorOffsets()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorOffsets:Z

    .line 69
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->storeTermVectorPositions()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorPositions:Z

    .line 70
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->storeTermVectorPayloads()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorPayloads:Z

    .line 71
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->omitNorms()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->omitNorms:Z

    .line 72
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->indexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 73
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->docValueType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/FieldType;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 74
    invoke-virtual {p1}, Lorg/apache/lucene/document/FieldType;->numericType()Lorg/apache/lucene/document/FieldType$NumericType;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/FieldType;->numericType:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 76
    return-void
.end method

.method private checkIfFrozen()V
    .locals 2

    .prologue
    .line 85
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->frozen:Z

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this FieldType is already frozen and cannot be changed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_0
    return-void
.end method


# virtual methods
.method public docValueType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lorg/apache/lucene/document/FieldType;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    return-object v0
.end method

.method public freeze()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->frozen:Z

    .line 97
    return-void
.end method

.method public indexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    return-object v0
.end method

.method public indexed()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->indexed:Z

    return v0
.end method

.method public numericPrecisionStep()I
    .locals 1

    .prologue
    .line 360
    iget v0, p0, Lorg/apache/lucene/document/FieldType;->numericPrecisionStep:I

    return v0
.end method

.method public numericType()Lorg/apache/lucene/document/FieldType$NumericType;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lorg/apache/lucene/document/FieldType;->numericType:Lorg/apache/lucene/document/FieldType$NumericType;

    return-object v0
.end method

.method public omitNorms()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->omitNorms:Z

    return v0
.end method

.method public setDocValueType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V
    .locals 0
    .param p1, "type"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 432
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 433
    iput-object p1, p0, Lorg/apache/lucene/document/FieldType;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 434
    return-void
.end method

.method public setIndexOptions(Lorg/apache/lucene/index/FieldInfo$IndexOptions;)V
    .locals 0
    .param p1, "value"    # Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .prologue
    .line 307
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 308
    iput-object p1, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    .line 309
    return-void
.end method

.method public setIndexed(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 118
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 119
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->indexed:Z

    .line 120
    return-void
.end method

.method public setNumericPrecisionStep(I)V
    .locals 3
    .param p1, "precisionStep"    # I

    .prologue
    .line 344
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 345
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 346
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "precisionStep must be >= 1 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 348
    :cond_0
    iput p1, p0, Lorg/apache/lucene/document/FieldType;->numericPrecisionStep:I

    .line 349
    return-void
.end method

.method public setNumericType(Lorg/apache/lucene/document/FieldType$NumericType;)V
    .locals 0
    .param p1, "type"    # Lorg/apache/lucene/document/FieldType$NumericType;

    .prologue
    .line 319
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 320
    iput-object p1, p0, Lorg/apache/lucene/document/FieldType;->numericType:Lorg/apache/lucene/document/FieldType$NumericType;

    .line 321
    return-void
.end method

.method public setOmitNorms(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 284
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 285
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->omitNorms:Z

    .line 286
    return-void
.end method

.method public setStoreTermVectorOffsets(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 213
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 214
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorOffsets:Z

    .line 215
    return-void
.end method

.method public setStoreTermVectorPayloads(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 261
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 262
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorPayloads:Z

    .line 263
    return-void
.end method

.method public setStoreTermVectorPositions(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 237
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 238
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorPositions:Z

    .line 239
    return-void
.end method

.method public setStoreTermVectors(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 189
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 190
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectors:Z

    .line 191
    return-void
.end method

.method public setStored(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 141
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 142
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->stored:Z

    .line 143
    return-void
.end method

.method public setTokenized(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 165
    invoke-direct {p0}, Lorg/apache/lucene/document/FieldType;->checkIfFrozen()V

    .line 166
    iput-boolean p1, p0, Lorg/apache/lucene/document/FieldType;->tokenized:Z

    .line 167
    return-void
.end method

.method public storeTermVectorOffsets()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorOffsets:Z

    return v0
.end method

.method public storeTermVectorPayloads()Z
    .locals 1

    .prologue
    .line 249
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorPayloads:Z

    return v0
.end method

.method public storeTermVectorPositions()Z
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectorPositions:Z

    return v0
.end method

.method public storeTermVectors()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->storeTermVectors:Z

    return v0
.end method

.method public stored()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->stored:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 367
    .local v0, "result":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->stored()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    const-string v1, "stored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->indexed()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 371
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 372
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    :cond_1
    const-string v1, "indexed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->tokenized()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 375
    const-string v1, ",tokenized"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 377
    :cond_2
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->storeTermVectors()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 378
    const-string v1, ",termVector"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->storeTermVectorOffsets()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 381
    const-string v1, ",termVectorOffsets"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    :cond_4
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->storeTermVectorPositions()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 384
    const-string v1, ",termVectorPosition"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->storeTermVectorPayloads()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 386
    const-string v1, ",termVectorPayloads"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/document/FieldType;->omitNorms()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 390
    const-string v1, ",omitNorms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    :cond_6
    iget-object v1, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    sget-object v2, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v1, v2, :cond_7

    .line 393
    const-string v1, ",indexOptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    iget-object v1, p0, Lorg/apache/lucene/document/FieldType;->indexOptions:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 396
    :cond_7
    iget-object v1, p0, Lorg/apache/lucene/document/FieldType;->numericType:Lorg/apache/lucene/document/FieldType$NumericType;

    if-eqz v1, :cond_8

    .line 397
    const-string v1, ",numericType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    iget-object v1, p0, Lorg/apache/lucene/document/FieldType;->numericType:Lorg/apache/lucene/document/FieldType$NumericType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 399
    const-string v1, ",numericPrecisionStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 400
    iget v1, p0, Lorg/apache/lucene/document/FieldType;->numericPrecisionStep:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 403
    :cond_8
    iget-object v1, p0, Lorg/apache/lucene/document/FieldType;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    if-eqz v1, :cond_a

    .line 404
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 405
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    :cond_9
    const-string v1, "docValueType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    iget-object v1, p0, Lorg/apache/lucene/document/FieldType;->docValueType:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 410
    :cond_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public tokenized()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lorg/apache/lucene/document/FieldType;->tokenized:Z

    return v0
.end method
