.class public Lorg/apache/lucene/document/DateTools;
.super Ljava/lang/Object;
.source "DateTools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/document/DateTools$Resolution;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$document$DateTools$Resolution:[I

.field static final GMT:Ljava/util/TimeZone;

.field private static final TL_CAL:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field private static final TL_FORMATS:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[",
            "Ljava/text/SimpleDateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$document$DateTools$Resolution()[I
    .locals 3

    .prologue
    .line 51
    sget-object v0, Lorg/apache/lucene/document/DateTools;->$SWITCH_TABLE$org$apache$lucene$document$DateTools$Resolution:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/document/DateTools$Resolution;->values()[Lorg/apache/lucene/document/DateTools$Resolution;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->DAY:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->HOUR:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->MILLISECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->MINUTE:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->MONTH:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->SECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->YEAR:Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-virtual {v1}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lorg/apache/lucene/document/DateTools;->$SWITCH_TABLE$org$apache$lucene$document$DateTools$Resolution:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/document/DateTools;->GMT:Ljava/util/TimeZone;

    .line 55
    new-instance v0, Lorg/apache/lucene/document/DateTools$1;

    invoke-direct {v0}, Lorg/apache/lucene/document/DateTools$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/DateTools;->TL_CAL:Ljava/lang/ThreadLocal;

    .line 63
    new-instance v0, Lorg/apache/lucene/document/DateTools$2;

    invoke-direct {v0}, Lorg/apache/lucene/document/DateTools$2;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/DateTools;->TL_FORMATS:Ljava/lang/ThreadLocal;

    .line 72
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dateToString(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "resolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 87
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lorg/apache/lucene/document/DateTools;->timeToString(JLorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static round(JLorg/apache/lucene/document/DateTools$Resolution;)J
    .locals 4
    .param p0, "time"    # J
    .param p2, "resolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    const/4 v3, 0x0

    .line 161
    sget-object v1, Lorg/apache/lucene/document/DateTools;->TL_CAL:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 162
    .local v0, "calInstance":Ljava/util/Calendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 164
    invoke-static {}, Lorg/apache/lucene/document/DateTools;->$SWITCH_TABLE$org$apache$lucene$document$DateTools$Resolution()[I

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/lucene/document/DateTools$Resolution;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 182
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown resolution "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 167
    :pswitch_0
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 169
    :pswitch_1
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 171
    :pswitch_2
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 173
    :pswitch_3
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 175
    :pswitch_4
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 177
    :pswitch_5
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 184
    :pswitch_6
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static round(Ljava/util/Date;Lorg/apache/lucene/document/DateTools$Resolution;)Ljava/util/Date;
    .locals 4
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "resolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 146
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3, p1}, Lorg/apache/lucene/document/DateTools;->round(JLorg/apache/lucene/document/DateTools$Resolution;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public static stringToDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p0, "dateString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 130
    :try_start_0
    sget-object v1, Lorg/apache/lucene/document/DateTools;->TL_FORMATS:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/text/ParseException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input is not a valid date string: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1
.end method

.method public static stringToTime(Ljava/lang/String;)J
    .locals 2
    .param p0, "dateString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 115
    invoke-static {p0}, Lorg/apache/lucene/document/DateTools;->stringToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static timeToString(JLorg/apache/lucene/document/DateTools$Resolution;)Ljava/lang/String;
    .locals 4
    .param p0, "time"    # J
    .param p2, "resolution"    # Lorg/apache/lucene/document/DateTools$Resolution;

    .prologue
    .line 100
    new-instance v0, Ljava/util/Date;

    invoke-static {p0, p1, p2}, Lorg/apache/lucene/document/DateTools;->round(JLorg/apache/lucene/document/DateTools$Resolution;)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 101
    .local v0, "date":Ljava/util/Date;
    sget-object v1, Lorg/apache/lucene/document/DateTools;->TL_FORMATS:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/text/SimpleDateFormat;

    iget v2, p2, Lorg/apache/lucene/document/DateTools$Resolution;->formatLen:I

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
