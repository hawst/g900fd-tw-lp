.class public final Lorg/apache/lucene/document/StoredField;
.super Lorg/apache/lucene/document/Field;
.source "StoredField.java"


# static fields
.field public static final TYPE:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    .line 35
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setStored(Z)V

    .line 36
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;D)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # D

    .prologue
    .line 128
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 129
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/StoredField;->fieldsData:Ljava/lang/Object;

    .line 130
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;F)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # F

    .prologue
    .line 106
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 107
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/StoredField;->fieldsData:Ljava/lang/Object;

    .line 108
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 95
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 96
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/StoredField;->fieldsData:Ljava/lang/Object;

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 117
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 118
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/document/StoredField;->fieldsData:Ljava/lang/Object;

    .line 119
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 84
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 74
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/document/FieldType;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 48
    sget-object v0, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BLorg/apache/lucene/document/FieldType;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[BII)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "offset"    # I
    .param p4, "length"    # I

    .prologue
    .line 62
    sget-object v5, Lorg/apache/lucene/document/StoredField;->TYPE:Lorg/apache/lucene/document/FieldType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;[BIILorg/apache/lucene/document/FieldType;)V

    .line 63
    return-void
.end method
