.class public abstract enum Lorg/apache/lucene/document/Field$Index;
.super Ljava/lang/Enum;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "Index"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/Field$Index;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final enum ANALYZED:Lorg/apache/lucene/document/Field$Index;

.field public static final enum ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/document/Field$Index;

.field public static final enum NO:Lorg/apache/lucene/document/Field$Index;

.field public static final enum NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

.field public static final enum NOT_ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 675
    new-instance v0, Lorg/apache/lucene/document/Field$Index$1;

    const-string v1, "NO"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/Field$Index$1;-><init>(Ljava/lang/String;I)V

    .line 678
    sput-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    .line 687
    new-instance v0, Lorg/apache/lucene/document/Field$Index$2;

    const-string v1, "ANALYZED"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/document/Field$Index$2;-><init>(Ljava/lang/String;I)V

    .line 690
    sput-object v0, Lorg/apache/lucene/document/Field$Index;->ANALYZED:Lorg/apache/lucene/document/Field$Index;

    .line 699
    new-instance v0, Lorg/apache/lucene/document/Field$Index$3;

    const-string v1, "NOT_ANALYZED"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/document/Field$Index$3;-><init>(Ljava/lang/String;I)V

    .line 703
    sput-object v0, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

    .line 712
    new-instance v0, Lorg/apache/lucene/document/Field$Index$4;

    const-string v1, "NOT_ANALYZED_NO_NORMS"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/document/Field$Index$4;-><init>(Ljava/lang/String;I)V

    .line 726
    sput-object v0, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    .line 735
    new-instance v0, Lorg/apache/lucene/document/Field$Index$5;

    const-string v1, "ANALYZED_NO_NORMS"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/document/Field$Index$5;-><init>(Ljava/lang/String;I)V

    .line 740
    sput-object v0, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    .line 673
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/lucene/document/Field$Index;

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->ANALYZED:Lorg/apache/lucene/document/Field$Index;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/lucene/document/Field$Index;->ENUM$VALUES:[Lorg/apache/lucene/document/Field$Index;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$Index;)V
    .locals 0

    .prologue
    .line 673
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/Field$Index;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static toIndex(ZZ)Lorg/apache/lucene/document/Field$Index;
    .locals 1
    .param p0, "indexed"    # Z
    .param p1, "analyzed"    # Z

    .prologue
    .line 751
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/document/Field$Index;->toIndex(ZZZ)Lorg/apache/lucene/document/Field$Index;

    move-result-object v0

    return-object v0
.end method

.method public static toIndex(ZZZ)Lorg/apache/lucene/document/Field$Index;
    .locals 1
    .param p0, "indexed"    # Z
    .param p1, "analyzed"    # Z
    .param p2, "omitNorms"    # Z

    .prologue
    .line 758
    if-nez p0, :cond_0

    .line 759
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NO:Lorg/apache/lucene/document/Field$Index;

    .line 774
    :goto_0
    return-object v0

    .line 763
    :cond_0
    if-nez p2, :cond_2

    .line 764
    if-eqz p1, :cond_1

    .line 765
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->ANALYZED:Lorg/apache/lucene/document/Field$Index;

    goto :goto_0

    .line 767
    :cond_1
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED:Lorg/apache/lucene/document/Field$Index;

    goto :goto_0

    .line 771
    :cond_2
    if-eqz p1, :cond_3

    .line 772
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    goto :goto_0

    .line 774
    :cond_3
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->NOT_ANALYZED_NO_NORMS:Lorg/apache/lucene/document/Field$Index;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/Field$Index;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/document/Field$Index;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Field$Index;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/document/Field$Index;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/document/Field$Index;->ENUM$VALUES:[Lorg/apache/lucene/document/Field$Index;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/document/Field$Index;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public abstract isAnalyzed()Z
.end method

.method public abstract isIndexed()Z
.end method

.method public abstract omitNorms()Z
.end method
