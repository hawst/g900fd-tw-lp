.class public Lorg/apache/lucene/document/SortedSetDocValuesField;
.super Lorg/apache/lucene/document/Field;
.source "SortedSetDocValuesField.java"


# static fields
.field public static final TYPE:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lorg/apache/lucene/document/FieldType;

    invoke-direct {v0}, Lorg/apache/lucene/document/FieldType;-><init>()V

    sput-object v0, Lorg/apache/lucene/document/SortedSetDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    .line 47
    sget-object v0, Lorg/apache/lucene/document/SortedSetDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/document/FieldType;->setDocValueType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 48
    sget-object v0, Lorg/apache/lucene/document/SortedSetDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-virtual {v0}, Lorg/apache/lucene/document/FieldType;->freeze()V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 58
    sget-object v0, Lorg/apache/lucene/document/SortedSetDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/document/Field;-><init>(Ljava/lang/String;Lorg/apache/lucene/document/FieldType;)V

    .line 59
    iput-object p2, p0, Lorg/apache/lucene/document/SortedSetDocValuesField;->fieldsData:Ljava/lang/Object;

    .line 60
    return-void
.end method
