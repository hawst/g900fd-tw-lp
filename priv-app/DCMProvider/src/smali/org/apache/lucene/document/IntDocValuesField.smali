.class public Lorg/apache/lucene/document/IntDocValuesField;
.super Lorg/apache/lucene/document/NumericDocValuesField;
.source "IntDocValuesField.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 47
    int-to-long v0, p2

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;-><init>(Ljava/lang/String;J)V

    .line 48
    return-void
.end method


# virtual methods
.method public setIntValue(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 52
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/document/IntDocValuesField;->setLongValue(J)V

    .line 53
    return-void
.end method
