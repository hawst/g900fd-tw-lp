.class public Lorg/apache/lucene/document/DerefBytesDocValuesField;
.super Lorg/apache/lucene/document/BinaryDocValuesField;
.source "DerefBytesDocValuesField.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final TYPE_FIXED_LEN:Lorg/apache/lucene/document/FieldType;

.field public static final TYPE_VAR_LEN:Lorg/apache/lucene/document/FieldType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lorg/apache/lucene/document/BinaryDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    sput-object v0, Lorg/apache/lucene/document/DerefBytesDocValuesField;->TYPE_FIXED_LEN:Lorg/apache/lucene/document/FieldType;

    .line 50
    sget-object v0, Lorg/apache/lucene/document/BinaryDocValuesField;->TYPE:Lorg/apache/lucene/document/FieldType;

    sput-object v0, Lorg/apache/lucene/document/DerefBytesDocValuesField;->TYPE_VAR_LEN:Lorg/apache/lucene/document/FieldType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/BinaryDocValuesField;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Z)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;
    .param p3, "isFixedLength"    # Z

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/BinaryDocValuesField;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 72
    return-void
.end method
