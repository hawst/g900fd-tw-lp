.class public abstract enum Lorg/apache/lucene/document/Field$TermVector;
.super Ljava/lang/Enum;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/Field;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "TermVector"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/Field$TermVector;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/document/Field$TermVector;

.field public static final enum NO:Lorg/apache/lucene/document/Field$TermVector;

.field public static final enum WITH_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

.field public static final enum WITH_POSITIONS:Lorg/apache/lucene/document/Field$TermVector;

.field public static final enum WITH_POSITIONS_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

.field public static final enum YES:Lorg/apache/lucene/document/Field$TermVector;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 789
    new-instance v0, Lorg/apache/lucene/document/Field$TermVector$1;

    const-string v1, "NO"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/document/Field$TermVector$1;-><init>(Ljava/lang/String;I)V

    .line 791
    sput-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    .line 800
    new-instance v0, Lorg/apache/lucene/document/Field$TermVector$2;

    const-string v1, "YES"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/document/Field$TermVector$2;-><init>(Ljava/lang/String;I)V

    .line 802
    sput-object v0, Lorg/apache/lucene/document/Field$TermVector;->YES:Lorg/apache/lucene/document/Field$TermVector;

    .line 811
    new-instance v0, Lorg/apache/lucene/document/Field$TermVector$3;

    const-string v1, "WITH_POSITIONS"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/document/Field$TermVector$3;-><init>(Ljava/lang/String;I)V

    .line 816
    sput-object v0, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS:Lorg/apache/lucene/document/Field$TermVector;

    .line 825
    new-instance v0, Lorg/apache/lucene/document/Field$TermVector$4;

    const-string v1, "WITH_OFFSETS"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/document/Field$TermVector$4;-><init>(Ljava/lang/String;I)V

    .line 830
    sput-object v0, Lorg/apache/lucene/document/Field$TermVector;->WITH_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    .line 839
    new-instance v0, Lorg/apache/lucene/document/Field$TermVector$5;

    const-string v1, "WITH_POSITIONS_OFFSETS"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/document/Field$TermVector$5;-><init>(Ljava/lang/String;I)V

    .line 846
    sput-object v0, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    .line 787
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/lucene/document/Field$TermVector;

    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->YES:Lorg/apache/lucene/document/Field$TermVector;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS:Lorg/apache/lucene/document/Field$TermVector;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->WITH_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/lucene/document/Field$TermVector;->ENUM$VALUES:[Lorg/apache/lucene/document/Field$TermVector;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 787
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILorg/apache/lucene/document/Field$TermVector;)V
    .locals 0

    .prologue
    .line 787
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/document/Field$TermVector;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static toTermVector(ZZZ)Lorg/apache/lucene/document/Field$TermVector;
    .locals 1
    .param p0, "stored"    # Z
    .param p1, "withOffsets"    # Z
    .param p2, "withPositions"    # Z

    .prologue
    .line 859
    if-nez p0, :cond_0

    .line 860
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->NO:Lorg/apache/lucene/document/Field$TermVector;

    .line 873
    :goto_0
    return-object v0

    .line 863
    :cond_0
    if-eqz p1, :cond_2

    .line 864
    if-eqz p2, :cond_1

    .line 865
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    goto :goto_0

    .line 867
    :cond_1
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->WITH_OFFSETS:Lorg/apache/lucene/document/Field$TermVector;

    goto :goto_0

    .line 870
    :cond_2
    if-eqz p2, :cond_3

    .line 871
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->WITH_POSITIONS:Lorg/apache/lucene/document/Field$TermVector;

    goto :goto_0

    .line 873
    :cond_3
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->YES:Lorg/apache/lucene/document/Field$TermVector;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/Field$TermVector;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/document/Field$TermVector;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/Field$TermVector;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/document/Field$TermVector;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/document/Field$TermVector;->ENUM$VALUES:[Lorg/apache/lucene/document/Field$TermVector;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/document/Field$TermVector;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public abstract isStored()Z
.end method

.method public abstract withOffsets()Z
.end method

.method public abstract withPositions()Z
.end method
