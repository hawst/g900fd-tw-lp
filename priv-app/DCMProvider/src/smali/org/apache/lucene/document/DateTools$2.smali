.class Lorg/apache/lucene/document/DateTools$2;
.super Ljava/lang/ThreadLocal;
.source "DateTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/DateTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ThreadLocal",
        "<[",
        "Ljava/text/SimpleDateFormat;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/ThreadLocal;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method protected bridge synthetic initialValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/document/DateTools$2;->initialValue()[Ljava/text/SimpleDateFormat;

    move-result-object v0

    return-object v0
.end method

.method protected initialValue()[Ljava/text/SimpleDateFormat;
    .locals 7

    .prologue
    .line 66
    sget-object v2, Lorg/apache/lucene/document/DateTools$Resolution;->MILLISECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    iget v2, v2, Lorg/apache/lucene/document/DateTools$Resolution;->formatLen:I

    add-int/lit8 v2, v2, 0x1

    new-array v0, v2, [Ljava/text/SimpleDateFormat;

    .line 67
    .local v0, "arr":[Ljava/text/SimpleDateFormat;
    invoke-static {}, Lorg/apache/lucene/document/DateTools$Resolution;->values()[Lorg/apache/lucene/document/DateTools$Resolution;

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_0

    .line 70
    return-object v0

    .line 67
    :cond_0
    aget-object v1, v4, v3

    .line 68
    .local v1, "resolution":Lorg/apache/lucene/document/DateTools$Resolution;
    iget v6, v1, Lorg/apache/lucene/document/DateTools$Resolution;->formatLen:I

    iget-object v2, v1, Lorg/apache/lucene/document/DateTools$Resolution;->format:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/text/SimpleDateFormat;

    aput-object v2, v0, v6

    .line 67
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0
.end method
