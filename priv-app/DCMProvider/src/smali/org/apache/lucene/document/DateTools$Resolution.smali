.class public final enum Lorg/apache/lucene/document/DateTools$Resolution;
.super Ljava/lang/Enum;
.source "DateTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/document/DateTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Resolution"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/document/DateTools$Resolution;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DAY:Lorg/apache/lucene/document/DateTools$Resolution;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/document/DateTools$Resolution;

.field public static final enum HOUR:Lorg/apache/lucene/document/DateTools$Resolution;

.field public static final enum MILLISECOND:Lorg/apache/lucene/document/DateTools$Resolution;

.field public static final enum MINUTE:Lorg/apache/lucene/document/DateTools$Resolution;

.field public static final enum MONTH:Lorg/apache/lucene/document/DateTools$Resolution;

.field public static final enum SECOND:Lorg/apache/lucene/document/DateTools$Resolution;

.field public static final enum YEAR:Lorg/apache/lucene/document/DateTools$Resolution;


# instance fields
.field final format:Ljava/text/SimpleDateFormat;

.field final formatLen:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x6

    const/4 v4, 0x4

    .line 190
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "YEAR"

    .line 191
    invoke-direct {v0, v1, v6, v4}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->YEAR:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 192
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "MONTH"

    .line 193
    invoke-direct {v0, v1, v7, v5}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->MONTH:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 194
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "DAY"

    .line 195
    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->DAY:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 196
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "HOUR"

    const/4 v2, 0x3

    .line 197
    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->HOUR:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 198
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "MINUTE"

    .line 199
    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->MINUTE:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 200
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "SECOND"

    const/4 v2, 0x5

    .line 201
    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->SECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 202
    new-instance v0, Lorg/apache/lucene/document/DateTools$Resolution;

    const-string v1, "MILLISECOND"

    .line 203
    const/16 v2, 0x11

    invoke-direct {v0, v1, v5, v2}, Lorg/apache/lucene/document/DateTools$Resolution;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->MILLISECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    .line 188
    const/4 v0, 0x7

    new-array v0, v0, [Lorg/apache/lucene/document/DateTools$Resolution;

    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->YEAR:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v1, v0, v6

    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->MONTH:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v1, v0, v7

    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->DAY:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v1, v0, v8

    const/4 v1, 0x3

    sget-object v2, Lorg/apache/lucene/document/DateTools$Resolution;->HOUR:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v2, v0, v1

    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->MINUTE:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v1, v0, v4

    const/4 v1, 0x5

    sget-object v2, Lorg/apache/lucene/document/DateTools$Resolution;->SECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v2, v0, v1

    sget-object v1, Lorg/apache/lucene/document/DateTools$Resolution;->MILLISECOND:Lorg/apache/lucene/document/DateTools$Resolution;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->ENUM$VALUES:[Lorg/apache/lucene/document/DateTools$Resolution;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 3
    .param p3, "formatLen"    # I

    .prologue
    .line 208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 209
    iput p3, p0, Lorg/apache/lucene/document/DateTools$Resolution;->formatLen:I

    .line 212
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMddHHmmssSSS"

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lorg/apache/lucene/document/DateTools$Resolution;->format:Ljava/text/SimpleDateFormat;

    .line 213
    iget-object v0, p0, Lorg/apache/lucene/document/DateTools$Resolution;->format:Ljava/text/SimpleDateFormat;

    sget-object v1, Lorg/apache/lucene/document/DateTools;->GMT:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 214
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/document/DateTools$Resolution;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/document/DateTools$Resolution;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/document/DateTools$Resolution;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/document/DateTools$Resolution;->ENUM$VALUES:[Lorg/apache/lucene/document/DateTools$Resolution;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/document/DateTools$Resolution;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 220
    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
