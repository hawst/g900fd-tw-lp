.class public Lorg/apache/lucene/document/FloatDocValuesField;
.super Lorg/apache/lucene/document/NumericDocValuesField;
.source "FloatDocValuesField.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # F

    .prologue
    .line 43
    invoke-static {p2}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;-><init>(Ljava/lang/String;J)V

    .line 44
    return-void
.end method


# virtual methods
.method public setFloatValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 48
    invoke-static {p1}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    int-to-long v0, v0

    invoke-super {p0, v0, v1}, Lorg/apache/lucene/document/NumericDocValuesField;->setLongValue(J)V

    .line 49
    return-void
.end method

.method public setLongValue(J)V
    .locals 2
    .param p1, "value"    # J

    .prologue
    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot change value type from Float to Long"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
