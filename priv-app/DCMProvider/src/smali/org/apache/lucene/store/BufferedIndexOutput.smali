.class public abstract Lorg/apache/lucene/store/BufferedIndexOutput;
.super Lorg/apache/lucene/store/IndexOutput;
.source "BufferedIndexOutput.java"


# static fields
.field public static final DEFAULT_BUFFER_SIZE:I = 0x4000


# instance fields
.field private final buffer:[B

.field private bufferPosition:I

.field private final bufferSize:I

.field private bufferStart:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0x4000

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/BufferedIndexOutput;-><init>(I)V

    .line 38
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "bufferSize"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 29
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 46
    if-gtz p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bufferSize must be greater than 0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    iput p1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    .line 50
    new-array v0, p1, [B

    iput-object v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    .line 51
    return-void
.end method

.method private flushBuffer([BI)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BII)V

    .line 114
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 127
    return-void
.end method

.method public flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BI)V

    .line 103
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 105
    return-void
.end method

.method protected abstract flushBuffer([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getBufferSize()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    return v0
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 131
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public abstract length()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public seek(J)V
    .locals 1
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 137
    iput-wide p1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    .line 138
    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    if-lt v0, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 57
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    aput-byte p1, v0, v1

    .line 58
    return-void
.end method

.method public writeBytes([BII)V
    .locals 8
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    sub-int v0, v3, v4

    .line 64
    .local v0, "bytesLeft":I
    if-lt v0, p3, :cond_1

    .line 66
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    invoke-static {p1, p2, v3, v4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 69
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-nez v3, :cond_0

    .line 70
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    if-le p3, v3, :cond_3

    .line 75
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    if-lez v3, :cond_2

    .line 76
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 78
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BII)V

    .line 79
    iget-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferStart:J

    goto :goto_0

    .line 82
    :cond_3
    const/4 v2, 0x0

    .line 84
    .local v2, "pos":I
    :cond_4
    :goto_1
    if-ge v2, p3, :cond_0

    .line 85
    sub-int v3, p3, v2

    if-ge v3, v0, :cond_5

    sub-int v1, p3, v2

    .line 86
    .local v1, "pieceLength":I
    :goto_2
    add-int v3, v2, p2

    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->buffer:[B

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    invoke-static {p1, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 87
    add-int/2addr v2, v1

    .line 88
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    add-int/2addr v3, v1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    .line 90
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferPosition:I

    sub-int v0, v3, v4

    .line 91
    if-nez v0, :cond_4

    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V

    .line 93
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexOutput;->bufferSize:I

    goto :goto_1

    .end local v1    # "pieceLength":I
    :cond_5
    move v1, v0

    .line 85
    goto :goto_2
.end method
