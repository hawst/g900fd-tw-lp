.class Lorg/apache/lucene/store/CompoundFileDirectory$1;
.super Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.source "CompoundFileDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/CompoundFileDirectory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/CompoundFileDirectory;

.field private final synthetic val$entry:Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/CompoundFileDirectory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/store/CompoundFileDirectory$1;->this$0:Lorg/apache/lucene/store/CompoundFileDirectory;

    iput-object p3, p0, Lorg/apache/lucene/store/CompoundFileDirectory$1;->val$entry:Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .line 357
    invoke-direct {p0, p2}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 360
    return-void
.end method

.method public openFullSlice()Lorg/apache/lucene/store/IndexInput;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 369
    const-string v1, "full-slice"

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory$1;->val$entry:Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    iget-wide v4, v0, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->length:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/CompoundFileDirectory$1;->openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    .locals 6
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "offset"    # J
    .param p4, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 364
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory$1;->this$0:Lorg/apache/lucene/store/CompoundFileDirectory;

    # getter for: Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    invoke-static {v0}, Lorg/apache/lucene/store/CompoundFileDirectory;->access$0(Lorg/apache/lucene/store/CompoundFileDirectory;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory$1;->val$entry:Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    iget-wide v2, v1, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->offset:J

    add-long/2addr v2, p2

    move-object v1, p1

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;->openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method
