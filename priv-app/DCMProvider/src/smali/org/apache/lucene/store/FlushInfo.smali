.class public Lorg/apache/lucene/store/FlushInfo;
.super Ljava/lang/Object;
.source "FlushInfo.java"


# instance fields
.field public final estimatedSegmentSize:J

.field public final numDocs:I


# direct methods
.method public constructor <init>(IJ)V
    .locals 0
    .param p1, "numDocs"    # I
    .param p2, "estimatedSegmentSize"    # J

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lorg/apache/lucene/store/FlushInfo;->numDocs:I

    .line 42
    iput-wide p2, p0, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p0, p1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 60
    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 62
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 63
    check-cast v0, Lorg/apache/lucene/store/FlushInfo;

    .line 64
    .local v0, "other":Lorg/apache/lucene/store/FlushInfo;
    iget-wide v4, p0, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    iget-wide v6, v0, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 65
    goto :goto_0

    .line 66
    :cond_4
    iget v3, p0, Lorg/apache/lucene/store/FlushInfo;->numDocs:I

    iget v4, v0, Lorg/apache/lucene/store/FlushInfo;->numDocs:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 67
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 47
    const/16 v0, 0x1f

    .line 48
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 50
    .local v1, "result":I
    iget-wide v2, p0, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    iget-wide v4, p0, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    .line 49
    add-int/lit8 v1, v2, 0x1f

    .line 51
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/store/FlushInfo;->numDocs:I

    add-int v1, v2, v3

    .line 52
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FlushInfo [numDocs="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/store/FlushInfo;->numDocs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", estimatedSegmentSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 74
    iget-wide v2, p0, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
