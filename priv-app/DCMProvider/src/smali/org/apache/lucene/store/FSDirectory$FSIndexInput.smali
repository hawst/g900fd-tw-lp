.class public abstract Lorg/apache/lucene/store/FSDirectory$FSIndexInput;
.super Lorg/apache/lucene/store/BufferedIndexInput;
.source "FSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/FSDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "FSIndexInput"
.end annotation


# instance fields
.field protected final chunkSize:I

.field protected final end:J

.field protected final file:Ljava/io/RandomAccessFile;

.field isClone:Z

.field protected final off:J


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V
    .locals 2
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/io/File;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p4, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 409
    invoke-direct {p0, p1, p3}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    .line 399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->isClone:Z

    .line 410
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->file:Ljava/io/RandomAccessFile;

    .line 411
    iput p4, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->chunkSize:I

    .line 412
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->off:J

    .line 413
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->end:J

    .line 414
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/io/RandomAccessFile;JJII)V
    .locals 3
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/RandomAccessFile;
    .param p3, "off"    # J
    .param p5, "length"    # J
    .param p7, "bufferSize"    # I
    .param p8, "chunkSize"    # I

    .prologue
    .line 418
    invoke-direct {p0, p1, p7}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 399
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->isClone:Z

    .line 419
    iput-object p2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->file:Ljava/io/RandomAccessFile;

    .line 420
    iput p8, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->chunkSize:I

    .line 421
    iput-wide p3, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->off:J

    .line 422
    add-long v0, p3, p5

    iput-wide v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->end:J

    .line 423
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->isClone:Z

    .line 424
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/store/BufferedIndexInput;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->clone()Lorg/apache/lucene/store/FSDirectory$FSIndexInput;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/store/FSDirectory$FSIndexInput;
    .locals 2

    .prologue
    .line 436
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->clone()Lorg/apache/lucene/store/BufferedIndexInput;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;

    .line 437
    .local v0, "clone":Lorg/apache/lucene/store/FSDirectory$FSIndexInput;
    const/4 v1, 0x1

    iput-boolean v1, v0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->isClone:Z

    .line 438
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    iget-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->isClone:Z

    if-nez v0, :cond_0

    .line 430
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 432
    :cond_0
    return-void
.end method

.method isFDValid()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 450
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->valid()Z

    move-result v0

    return v0
.end method

.method public final length()J
    .locals 4

    .prologue
    .line 443
    iget-wide v0, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->end:J

    iget-wide v2, p0, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->off:J

    sub-long/2addr v0, v2

    return-wide v0
.end method
