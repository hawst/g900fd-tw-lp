.class public Lorg/apache/lucene/store/NIOFSDirectory;
.super Lorg/apache/lucene/store/FSDirectory;
.source "NIOFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 0
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 65
    return-void
.end method


# virtual methods
.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->ensureOpen()V

    .line 87
    new-instance v4, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->getDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {v4, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 88
    .local v4, "path":Ljava/io/File;
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v3, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 89
    .local v3, "descriptor":Ljava/io/RandomAccessFile;
    new-instance v0, Lorg/apache/lucene/store/NIOFSDirectory$1;

    move-object v1, p0

    move-object v2, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/NIOFSDirectory$1;-><init>(Lorg/apache/lucene/store/NIOFSDirectory;Lorg/apache/lucene/store/Directory;Ljava/io/RandomAccessFile;Ljava/io/File;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method

.method public openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->ensureOpen()V

    .line 80
    new-instance v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->getDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/store/NIOFSDirectory;->getReadChunkSize()I

    move-result v2

    invoke-direct {v0, v1, p2, v2}, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;-><init>(Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V

    return-object v0
.end method
