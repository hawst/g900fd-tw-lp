.class public Lorg/apache/lucene/store/RAMInputStream;
.super Lorg/apache/lucene/store/IndexInput;
.source "RAMInputStream.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final BUFFER_SIZE:I = 0x400


# instance fields
.field private bufferLength:I

.field private bufferPosition:I

.field private bufferStart:J

.field private currentBuffer:[B

.field private currentBufferIndex:I

.field private file:Lorg/apache/lucene/store/RAMFile;

.field private length:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/RAMFile;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "f"    # Lorg/apache/lucene/store/RAMFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RAMInputStream(name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 41
    iput-object p2, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    .line 42
    iget-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v0, v0, Lorg/apache/lucene/store/RAMFile;->length:J

    iput-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    .line 43
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 44
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RAMInputStream too large length="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    .line 51
    return-void
.end method

.method private final switchCurrentBuffer(Z)V
    .locals 10
    .param p1, "enforceEOF"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x400

    const/16 v2, 0x400

    .line 90
    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    int-to-long v4, v3

    mul-long/2addr v4, v8

    iput-wide v4, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    .line 91
    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    iget-object v4, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v4}, Lorg/apache/lucene/store/RAMFile;->numBuffers()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 93
    if-eqz p1, :cond_0

    .line 94
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "read past EOF: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    :cond_0
    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 98
    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 106
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/RAMInputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget v4, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    .line 102
    const/4 v3, 0x0

    iput v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 103
    iget-wide v4, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    iget-wide v6, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    sub-long v0, v4, v6

    .line 104
    .local v0, "buflen":J
    cmp-long v3, v0, v8

    if-lez v3, :cond_2

    :goto_1
    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    goto :goto_0

    :cond_2
    long-to-int v2, v0

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 110
    iget v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->length:J

    return-wide v0
.end method

.method public readByte()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    if-lt v0, v1, :cond_0

    .line 66
    iget v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 69
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    iget v1, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    :goto_0
    if-gtz p3, :cond_0

    .line 87
    return-void

    .line 75
    :cond_0
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    if-lt v2, v3, :cond_1

    .line 76
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 77
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 80
    :cond_1
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferLength:I

    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    sub-int v1, v2, v3

    .line 81
    .local v1, "remainInBuffer":I
    if-ge p3, v1, :cond_2

    move v0, p3

    .line 82
    .local v0, "bytesToCopy":I
    :goto_1
    iget-object v2, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    iget v3, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 83
    add-int/2addr p2, v0

    .line 84
    sub-int/2addr p3, v0

    .line 85
    iget v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    goto :goto_0

    .end local v0    # "bytesToCopy":I
    :cond_2
    move v0, v1

    .line 81
    goto :goto_1
.end method

.method public seek(J)V
    .locals 5
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x400

    .line 115
    iget-object v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBuffer:[B

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferStart:J

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 116
    :cond_0
    div-long v0, p1, v2

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->currentBufferIndex:I

    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/RAMInputStream;->switchCurrentBuffer(Z)V

    .line 119
    :cond_1
    rem-long v0, p1, v2

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMInputStream;->bufferPosition:I

    .line 120
    return-void
.end method
