.class final Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;
.super Lorg/apache/lucene/store/IndexOutput;
.source "CompoundFileWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/CompoundFileWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DirectCFSIndexOutput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private closed:Z

.field private final delegate:Lorg/apache/lucene/store/IndexOutput;

.field private entry:Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

.field private final isSeparate:Z

.field private final offset:J

.field final synthetic this$0:Lorg/apache/lucene/store/CompoundFileWriter;

.field private writtenBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 289
    const-class v0, Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/CompoundFileWriter;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;Z)V
    .locals 2
    .param p2, "delegate"    # Lorg/apache/lucene/store/IndexOutput;
    .param p3, "entry"    # Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    .param p4, "isSeparate"    # Z

    .prologue
    .line 298
    iput-object p1, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->this$0:Lorg/apache/lucene/store/CompoundFileWriter;

    .line 299
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 300
    iput-object p2, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    .line 301
    iput-object p3, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->entry:Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    .line 302
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->offset:J

    iput-wide v0, p3, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->offset:J

    .line 303
    iput-boolean p4, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->isSeparate:Z

    .line 305
    return-void
.end method


# virtual methods
.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 314
    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->closed:Z

    if-nez v0, :cond_0

    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->closed:Z

    .line 316
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->entry:Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    iget-wide v2, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->writtenBytes:J

    iput-wide v2, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->length:J

    .line 317
    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->isSeparate:Z

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 320
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->this$0:Lorg/apache/lucene/store/CompoundFileWriter;

    # getter for: Lorg/apache/lucene/store/CompoundFileWriter;->pendingEntries:Ljava/util/Queue;
    invoke-static {v0}, Lorg/apache/lucene/store/CompoundFileWriter;->access$0(Lorg/apache/lucene/store/CompoundFileWriter;)Ljava/util/Queue;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->entry:Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 326
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->this$0:Lorg/apache/lucene/store/CompoundFileWriter;

    # invokes: Lorg/apache/lucene/store/CompoundFileWriter;->prunePendingEntries()V
    invoke-static {v0}, Lorg/apache/lucene/store/CompoundFileWriter;->access$1(Lorg/apache/lucene/store/CompoundFileWriter;)V

    .line 328
    :cond_0
    return-void

    .line 323
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->this$0:Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/store/CompoundFileWriter;->releaseOutputLock()V

    goto :goto_0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 310
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 332
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->offset:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public length()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    sget-boolean v0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 344
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->length()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->offset:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public seek(J)V
    .locals 5
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 337
    sget-boolean v0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 338
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    iget-wide v2, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->offset:J

    add-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 339
    return-void
.end method

.method public writeByte(B)V
    .locals 4
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 349
    sget-boolean v0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 350
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->writtenBytes:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->writtenBytes:J

    .line 351
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 352
    return-void
.end method

.method public writeBytes([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    sget-boolean v0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->closed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 357
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->writtenBytes:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->writtenBytes:J

    .line 358
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 359
    return-void
.end method
