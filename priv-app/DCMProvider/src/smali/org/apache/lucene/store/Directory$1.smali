.class Lorg/apache/lucene/store/Directory$1;
.super Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.source "Directory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/Directory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final base:Lorg/apache/lucene/store/IndexInput;

.field final synthetic this$0:Lorg/apache/lucene/store/Directory;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    iput-object p2, p0, Lorg/apache/lucene/store/Directory$1;->this$0:Lorg/apache/lucene/store/Directory;

    .line 234
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 235
    invoke-virtual {p2, p3, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/Directory$1;->base:Lorg/apache/lucene/store/IndexInput;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lorg/apache/lucene/store/Directory$1;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 243
    return-void
.end method

.method public openFullSlice()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lorg/apache/lucene/store/Directory$1;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    .locals 8
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "offset"    # J
    .param p4, "length"    # J

    .prologue
    .line 238
    new-instance v1, Lorg/apache/lucene/store/Directory$SlicedIndexInput;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SlicedIndexInput("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/lucene/store/Directory$1;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/store/Directory$1;->base:Lorg/apache/lucene/store/IndexInput;

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/store/Directory$SlicedIndexInput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;JJ)V

    return-object v1
.end method
