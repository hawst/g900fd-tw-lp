.class abstract Lorg/apache/lucene/store/ByteBufferIndexInput;
.super Lorg/apache/lucene/store/IndexInput;
.source "ByteBufferIndexInput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffers:[Ljava/nio/ByteBuffer;

.field private final chunkSizeMask:J

.field private final chunkSizePower:I

.field private final clones:Lorg/apache/lucene/util/WeakIdentityMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/util/WeakIdentityMap",
            "<",
            "Lorg/apache/lucene/store/ByteBufferIndexInput;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private curBuf:Ljava/nio/ByteBuffer;

.field private curBufIndex:I

.field private isClone:Z

.field private length:J

.field private offset:I

.field private sliceDescription:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lorg/apache/lucene/store/ByteBufferIndexInput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/ByteBufferIndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;[Ljava/nio/ByteBuffer;JIZ)V
    .locals 5
    .param p1, "resourceDescription"    # Ljava/lang/String;
    .param p2, "buffers"    # [Ljava/nio/ByteBuffer;
    .param p3, "length"    # J
    .param p5, "chunkSizePower"    # I
    .param p6, "trackClones"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x1

    .line 57
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->isClone:Z

    .line 58
    iput-object p2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 59
    iput-wide p3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->length:J

    .line 60
    iput p5, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizePower:I

    .line 61
    shl-long v0, v2, p5

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizeMask:J

    .line 62
    if-eqz p6, :cond_1

    invoke-static {}, Lorg/apache/lucene/util/WeakIdentityMap;->newConcurrentHashMap()Lorg/apache/lucene/util/WeakIdentityMap;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    .line 64
    sget-boolean v0, Lorg/apache/lucene/store/ByteBufferIndexInput;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-ltz p5, :cond_0

    const/16 v0, 0x1e

    if-le p5, v0, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 65
    :cond_2
    sget-boolean v0, Lorg/apache/lucene/store/ByteBufferIndexInput;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    ushr-long v0, p3, p5

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 67
    :cond_3
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/store/ByteBufferIndexInput;->seek(J)V

    .line 68
    return-void
.end method

.method private buildSlice(JJ)Lorg/apache/lucene/store/ByteBufferIndexInput;
    .locals 7
    .param p1, "offset"    # J
    .param p3, "length"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 216
    iget-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 217
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already closed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_0
    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    cmp-long v0, p3, v2

    if-ltz v0, :cond_1

    add-long v0, p1, p3

    iget-wide v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->length:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 220
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "slice() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->sliceDescription:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " out of bounds: offset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",fileLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->length:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_2
    iget v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->offset:I

    int-to-long v0, v0

    add-long/2addr p1, v0

    .line 226
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/store/ByteBufferIndexInput;

    .line 227
    .local v6, "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    const/4 v0, 0x1

    iput-boolean v0, v6, Lorg/apache/lucene/store/ByteBufferIndexInput;->isClone:Z

    .line 229
    sget-boolean v0, Lorg/apache/lucene/store/ByteBufferIndexInput;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    iget-object v0, v6, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    iget-object v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    if-eq v0, v1, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 230
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/ByteBufferIndexInput;->buildSlice([Ljava/nio/ByteBuffer;JJ)[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, v6, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 231
    iget-wide v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizeMask:J

    and-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, v6, Lorg/apache/lucene/store/ByteBufferIndexInput;->offset:I

    .line 232
    iput-wide p3, v6, Lorg/apache/lucene/store/ByteBufferIndexInput;->length:J

    .line 235
    iget-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    if-eqz v0, :cond_4

    .line 236
    iget-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v6, v1}, Lorg/apache/lucene/util/WeakIdentityMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    :cond_4
    return-object v6
.end method

.method private buildSlice([Ljava/nio/ByteBuffer;JJ)[Ljava/nio/ByteBuffer;
    .locals 10
    .param p1, "buffers"    # [Ljava/nio/ByteBuffer;
    .param p2, "offset"    # J
    .param p4, "length"    # J

    .prologue
    .line 246
    add-long v2, p2, p4

    .line 248
    .local v2, "sliceEnd":J
    iget v6, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizePower:I

    ushr-long v6, p2, v6

    long-to-int v5, v6

    .line 249
    .local v5, "startIndex":I
    iget v6, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizePower:I

    ushr-long v6, v2, v6

    long-to-int v0, v6

    .line 252
    .local v0, "endIndex":I
    sub-int v6, v0, v5

    add-int/lit8 v6, v6, 0x1

    new-array v4, v6, [Ljava/nio/ByteBuffer;

    .line 254
    .local v4, "slices":[Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v4

    if-lt v1, v6, :cond_0

    .line 259
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v4, v6

    iget-wide v8, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizeMask:J

    and-long/2addr v8, v2

    long-to-int v7, v8

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 261
    return-object v4

    .line 255
    :cond_0
    add-int v6, v5, v1

    aget-object v6, p1, v6

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v6

    aput-object v6, v4, v1

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private unsetBuffers()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 265
    iput-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 266
    iput-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    .line 267
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    .line 268
    return-void
.end method


# virtual methods
.method public final clone()Lorg/apache/lucene/store/ByteBufferIndexInput;
    .locals 6

    .prologue
    .line 187
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->length:J

    invoke-direct {p0, v2, v3, v4, v5}, Lorg/apache/lucene/store/ByteBufferIndexInput;->buildSlice(JJ)Lorg/apache/lucene/store/ByteBufferIndexInput;

    move-result-object v0

    .line 189
    .local v0, "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->getFilePointer()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/ByteBufferIndexInput;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    return-object v0

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Should never happen: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->clone()Lorg/apache/lucene/store/ByteBufferIndexInput;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 273
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_0

    .line 298
    invoke-direct {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->unsetBuffers()V

    .line 300
    :goto_0
    return-void

    .line 276
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    .line 277
    .local v1, "bufs":[Ljava/nio/ByteBuffer;
    invoke-direct {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->unsetBuffers()V

    .line 278
    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    if-eqz v4, :cond_1

    .line 279
    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v4, p0}, Lorg/apache/lucene/util/WeakIdentityMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    :cond_1
    iget-boolean v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->isClone:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_2

    .line 298
    invoke-direct {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->unsetBuffers()V

    goto :goto_0

    .line 285
    :cond_2
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    if-eqz v4, :cond_3

    .line 286
    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v4}, Lorg/apache/lucene/util/WeakIdentityMap;->keyIterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/store/ByteBufferIndexInput;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    .line 291
    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->clones:Lorg/apache/lucene/util/WeakIdentityMap;

    invoke-virtual {v4}, Lorg/apache/lucene/util/WeakIdentityMap;->clear()V

    .line 294
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/store/ByteBufferIndexInput;>;"
    :cond_3
    array-length v5, v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x0

    :goto_2
    if-lt v4, v5, :cond_6

    .line 298
    invoke-direct {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->unsetBuffers()V

    goto :goto_0

    .line 287
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/store/ByteBufferIndexInput;>;"
    :cond_4
    :try_start_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/store/ByteBufferIndexInput;

    .line 288
    .local v2, "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    sget-boolean v4, Lorg/apache/lucene/store/ByteBufferIndexInput;->$assertionsDisabled:Z

    if-nez v4, :cond_5

    iget-boolean v4, v2, Lorg/apache/lucene/store/ByteBufferIndexInput;->isClone:Z

    if-nez v4, :cond_5

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 297
    .end local v1    # "bufs":[Ljava/nio/ByteBuffer;
    .end local v2    # "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/store/ByteBufferIndexInput;>;"
    :catchall_0
    move-exception v4

    .line 298
    invoke-direct {p0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->unsetBuffers()V

    .line 299
    throw v4

    .line 289
    .restart local v1    # "bufs":[Ljava/nio/ByteBuffer;
    .restart local v2    # "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/store/ByteBufferIndexInput;>;"
    :cond_5
    :try_start_4
    invoke-direct {v2}, Lorg/apache/lucene/store/ByteBufferIndexInput;->unsetBuffers()V

    goto :goto_1

    .line 294
    .end local v2    # "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/store/ByteBufferIndexInput;>;"
    :cond_6
    aget-object v0, v1, v4

    .line 295
    .local v0, "b":Ljava/nio/ByteBuffer;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/ByteBufferIndexInput;->freeBuffer(Ljava/nio/ByteBuffer;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 294
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method protected abstract freeBuffer(Ljava/nio/ByteBuffer;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getFilePointer()J
    .locals 6

    .prologue
    .line 149
    :try_start_0
    iget v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    int-to-long v2, v1

    iget v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizePower:I

    shl-long/2addr v2, v1

    iget-object v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iget v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->offset:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v4, v1

    sub-long/2addr v2, v4

    return-wide v2

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "npe":Ljava/lang/NullPointerException;
    new-instance v1, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already closed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final length()J
    .locals 2

    .prologue
    .line 182
    iget-wide v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->length:J

    return-wide v0
.end method

.method public final readByte()B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 83
    :goto_0
    return v2

    .line 74
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    :cond_0
    iget v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    .line 77
    iget v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 78
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "read past EOF: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 80
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    iget v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    aget-object v2, v2, v3

    iput-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    .line 81
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 82
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    goto :goto_0

    .line 84
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 85
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Already closed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final readBytes([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, p2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/nio/BufferUnderflowException;
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 95
    .local v0, "curAvail":I
    :goto_1
    if-gt p3, v0, :cond_0

    .line 107
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, p2, p3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 96
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, p2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 97
    sub-int/2addr p3, v0

    .line 98
    add-int/2addr p2, v0

    .line 99
    iget v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    .line 100
    iget v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    iget-object v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    array-length v4, v4

    if-lt v3, v4, :cond_1

    .line 101
    new-instance v3, Ljava/io/EOFException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "read past EOF: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 103
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    iget v4, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    aget-object v3, v3, v4

    iput-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    .line 104
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 105
    iget-object v3, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    goto :goto_1

    .line 108
    .end local v0    # "curAvail":I
    .end local v1    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v2

    .line 109
    .local v2, "npe":Ljava/lang/NullPointerException;
    new-instance v3, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Already closed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public final readInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 129
    :goto_0
    return v2

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v2

    goto :goto_0

    .line 130
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 131
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Already closed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final readLong()J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getLong()J
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v2

    .line 140
    :goto_0
    return-wide v2

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    goto :goto_0

    .line 141
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 142
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Already closed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final readShort()S
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getShort()S
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 118
    :goto_0
    return v2

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/nio/BufferUnderflowException;
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readShort()S

    move-result v2

    goto :goto_0

    .line 119
    .end local v0    # "e":Ljava/nio/BufferUnderflowException;
    :catch_1
    move-exception v1

    .line 120
    .local v1, "npe":Ljava/lang/NullPointerException;
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Already closed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final seek(J)V
    .locals 9
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    const-wide/16 v6, 0x0

    cmp-long v5, p1, v6

    if-gez v5, :cond_0

    .line 159
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Seeking to negative position: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 161
    :cond_0
    iget v5, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->offset:I

    int-to-long v6, v5

    add-long/2addr p1, v6

    .line 164
    iget v5, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizePower:I

    shr-long v6, p1, v5

    long-to-int v2, v6

    .line 166
    .local v2, "bi":I
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->buffers:[Ljava/nio/ByteBuffer;

    aget-object v1, v5, v2

    .line 167
    .local v1, "b":Ljava/nio/ByteBuffer;
    iget-wide v6, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->chunkSizeMask:J

    and-long/2addr v6, p1

    long-to-int v5, v6

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 169
    iput v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBufIndex:I

    .line 170
    iput-object v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->curBuf:Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 178
    return-void

    .line 171
    .end local v1    # "b":Ljava/nio/ByteBuffer;
    :catch_0
    move-exception v0

    .line 172
    .local v0, "aioobe":Ljava/lang/ArrayIndexOutOfBoundsException;
    new-instance v5, Ljava/io/EOFException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "seek past EOF: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 173
    .end local v0    # "aioobe":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_1
    move-exception v3

    .line 174
    .local v3, "iae":Ljava/lang/IllegalArgumentException;
    new-instance v5, Ljava/io/EOFException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "seek past EOF: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 175
    .end local v3    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v4

    .line 176
    .local v4, "npe":Ljava/lang/NullPointerException;
    new-instance v5, Lorg/apache/lucene/store/AlreadyClosedException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Already closed: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public final slice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/ByteBufferIndexInput;
    .locals 6
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "offset"    # J
    .param p4, "length"    # J

    .prologue
    .line 201
    iget-boolean v2, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->isClone:Z

    if-eqz v2, :cond_0

    .line 202
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cannot slice() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from a cloned IndexInput: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 204
    :cond_0
    invoke-direct {p0, p2, p3, p4, p5}, Lorg/apache/lucene/store/ByteBufferIndexInput;->buildSlice(JJ)Lorg/apache/lucene/store/ByteBufferIndexInput;

    move-result-object v0

    .line 205
    .local v0, "clone":Lorg/apache/lucene/store/ByteBufferIndexInput;
    iput-object p1, v0, Lorg/apache/lucene/store/ByteBufferIndexInput;->sliceDescription:Ljava/lang/String;

    .line 207
    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/ByteBufferIndexInput;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    return-object v0

    .line 208
    :catch_0
    move-exception v1

    .line 209
    .local v1, "ioe":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Should never happen: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->sliceDescription:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " [slice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/ByteBufferIndexInput;->sliceDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
