.class public abstract Lorg/apache/lucene/store/BufferedIndexInput;
.super Lorg/apache/lucene/store/IndexInput;
.source "BufferedIndexInput.java"


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$store$IOContext$Context:[I = null

.field static final synthetic $assertionsDisabled:Z

.field public static final BUFFER_SIZE:I = 0x400

.field public static final MERGE_BUFFER_SIZE:I = 0x1000


# instance fields
.field protected buffer:[B

.field private bufferLength:I

.field private bufferPosition:I

.field private bufferSize:I

.field private bufferStart:J


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$store$IOContext$Context()[I
    .locals 3

    .prologue
    .line 24
    sget-object v0, Lorg/apache/lucene/store/BufferedIndexInput;->$SWITCH_TABLE$org$apache$lucene$store$IOContext$Context:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/store/IOContext$Context;->values()[Lorg/apache/lucene/store/IOContext$Context;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->DEFAULT:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->FLUSH:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->MERGE:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->READ:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/store/BufferedIndexInput;->$SWITCH_TABLE$org$apache$lucene$store$IOContext$Context:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lorg/apache/lucene/store/BufferedIndexInput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/BufferedIndexInput;->$assertionsDisabled:Z

    .line 38
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "resourceDesc"    # Ljava/lang/String;

    .prologue
    .line 56
    const/16 v0, 0x400

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 40
    const/16 v0, 0x400

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    .line 44
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 45
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 46
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 66
    invoke-direct {p0, p2}, Lorg/apache/lucene/store/BufferedIndexInput;->checkBufferSize(I)V

    .line 67
    iput p2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 1
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;

    .prologue
    .line 60
    invoke-static {p2}, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize(Lorg/apache/lucene/store/IOContext;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 61
    return-void
.end method

.method public static bufferSize(Lorg/apache/lucene/store/IOContext;)I
    .locals 2
    .param p0, "context"    # Lorg/apache/lucene/store/IOContext;

    .prologue
    .line 345
    invoke-static {}, Lorg/apache/lucene/store/BufferedIndexInput;->$SWITCH_TABLE$org$apache$lucene$store$IOContext$Context()[I

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 349
    const/16 v0, 0x400

    :goto_0
    return v0

    .line 347
    :pswitch_0
    const/16 v0, 0x1000

    goto :goto_0

    .line 345
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private checkBufferSize(I)V
    .locals 3
    .param p1, "bufferSize"    # I

    .prologue
    .line 107
    if-gtz p1, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bufferSize must be greater than 0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    return-void
.end method

.method private refill()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 260
    iget-wide v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v8, v3

    add-long v4, v6, v8

    .line 261
    .local v4, "start":J
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    int-to-long v6, v3

    add-long v0, v4, v6

    .line 262
    .local v0, "end":J
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->length()J

    move-result-wide v6

    cmp-long v3, v0, v6

    if-lez v3, :cond_0

    .line 263
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->length()J

    move-result-wide v0

    .line 264
    :cond_0
    sub-long v6, v0, v4

    long-to-int v2, v6

    .line 265
    .local v2, "newLength":I
    if-gtz v2, :cond_1

    .line 266
    new-instance v3, Ljava/io/EOFException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "read past EOF: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 268
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-nez v3, :cond_2

    .line 269
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    new-array v3, v3, [B

    invoke-virtual {p0, v3}, Lorg/apache/lucene/store/BufferedIndexInput;->newBuffer([B)V

    .line 270
    iget-wide v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    invoke-virtual {p0, v6, v7}, Lorg/apache/lucene/store/BufferedIndexInput;->seekInternal(J)V

    .line 272
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    invoke-virtual {p0, v3, v10, v2}, Lorg/apache/lucene/store/BufferedIndexInput;->readInternal([BII)V

    .line 273
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 274
    iput-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 275
    iput v10, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 276
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/store/BufferedIndexInput;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 310
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/BufferedIndexInput;

    .line 312
    .local v0, "clone":Lorg/apache/lucene/store/BufferedIndexInput;
    const/4 v1, 0x0

    iput-object v1, v0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    .line 313
    iput v2, v0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 314
    iput v2, v0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 315
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->getFilePointer()J

    move-result-wide v2

    iput-wide v2, v0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 317
    return-object v0
.end method

.method public bridge synthetic clone()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->clone()Lorg/apache/lucene/store/BufferedIndexInput;

    move-result-object v0

    return-object v0
.end method

.method protected final flushBuffer(Lorg/apache/lucene/store/IndexOutput;J)I
    .locals 4
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int v0, v1, v2

    .line 331
    .local v0, "toCopy":I
    int-to-long v2, v0

    cmp-long v1, v2, p2

    if-lez v1, :cond_0

    .line 332
    long-to-int v0, p2

    .line 334
    :cond_0
    if-lez v0, :cond_1

    .line 335
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-virtual {p1, v1, v2, v0}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 336
    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 338
    :cond_1
    return v0
.end method

.method public final getBufferSize()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    return v0
.end method

.method public final getFilePointer()J
    .locals 4

    .prologue
    .line 288
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method protected newBuffer([B)V
    .locals 0
    .param p1, "newBuffer"    # [B

    .prologue
    .line 98
    iput-object p1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    .line 99
    return-void
.end method

.method public final readByte()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    iget v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    if-lt v0, v1, :cond_0

    .line 51
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->refill()V

    .line 52
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public final readBytes([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/lucene/store/BufferedIndexInput;->readBytes([BIIZ)V

    .line 114
    return-void
.end method

.method public final readBytes([BIIZ)V
    .locals 9
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "useBuffer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 119
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-gt p3, v3, :cond_1

    .line 121
    if-lez p3, :cond_0

    .line 122
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-static {v3, v4, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    :cond_0
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/2addr v3, p3

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 164
    :goto_0
    return-void

    .line 126
    :cond_1
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int v2, v3, v4

    .line 127
    .local v2, "available":I
    if-lez v2, :cond_2

    .line 128
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-static {v3, v4, p1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    add-int/2addr p2, v2

    .line 130
    sub-int/2addr p3, v2

    .line 131
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/2addr v3, v2

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 134
    :cond_2
    if-eqz p4, :cond_4

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    if-ge p3, v3, :cond_4

    .line 138
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->refill()V

    .line 139
    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    if-ge v3, p3, :cond_3

    .line 141
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    invoke-static {v3, v8, p1, p2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    new-instance v3, Ljava/io/EOFException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "read past EOF: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 144
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    invoke-static {v3, v8, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 145
    iput p3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    goto :goto_0

    .line 155
    :cond_4
    iget-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    int-to-long v6, p3

    add-long v0, v4, v6

    .line 156
    .local v0, "after":J
    invoke-virtual {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->length()J

    move-result-wide v4

    cmp-long v3, v0, v4

    if-lez v3, :cond_5

    .line 157
    new-instance v3, Ljava/io/EOFException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "read past EOF: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 158
    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/BufferedIndexInput;->readInternal([BII)V

    .line 159
    iput-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 160
    iput v8, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 161
    iput v8, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    goto :goto_0
.end method

.method public final readInt()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    const/4 v0, 0x4

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 179
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    .line 178
    or-int/2addr v0, v1

    .line 179
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 178
    or-int/2addr v0, v1

    .line 181
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readInt()I

    move-result v0

    goto :goto_0
.end method

.method protected abstract readInternal([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final readLong()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    const/16 v2, 0x8

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-gt v2, v3, :cond_0

    .line 188
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    .line 189
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    .line 188
    or-int/2addr v2, v3

    .line 189
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 188
    or-int v0, v2, v3

    .line 190
    .local v0, "i1":I
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    .line 191
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    .line 190
    or-int/2addr v2, v3

    .line 191
    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 190
    or-int v1, v2, v3

    .line 192
    .local v1, "i2":I
    int-to-long v2, v0

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    int-to-long v4, v1

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    .line 194
    .end local v0    # "i1":I
    .end local v1    # "i2":I
    :goto_0
    return-wide v2

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v2

    goto :goto_0
.end method

.method public final readShort()S
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 168
    const/4 v0, 0x2

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 169
    iget-object v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    .line 171
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readShort()S

    move-result v0

    goto :goto_0
.end method

.method public final readVInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    const/4 v2, 0x5

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v3, v4

    if-gt v2, v3, :cond_2

    .line 201
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 202
    .local v0, "b":B
    if-ltz v0, :cond_1

    move v1, v0

    .line 219
    .end local v0    # "b":B
    :cond_0
    :goto_0
    return v1

    .line 203
    .restart local v0    # "b":B
    :cond_1
    and-int/lit8 v1, v0, 0x7f

    .line 204
    .local v1, "i":I
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 205
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x7

    or-int/2addr v1, v2

    .line 206
    if-gez v0, :cond_0

    .line 207
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 208
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v1, v2

    .line 209
    if-gez v0, :cond_0

    .line 210
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 211
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x15

    or-int/2addr v1, v2

    .line 212
    if-gez v0, :cond_0

    .line 213
    iget-object v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v2, v3

    .line 215
    and-int/lit8 v2, v0, 0xf

    shl-int/lit8 v2, v2, 0x1c

    or-int/2addr v1, v2

    .line 216
    and-int/lit16 v2, v0, 0xf0

    if-eqz v2, :cond_0

    .line 217
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Invalid vInt detected (too many bits)"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 219
    .end local v0    # "b":B
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v1

    goto :goto_0
.end method

.method public final readVLong()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x7f

    .line 225
    const/16 v1, 0x9

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int/2addr v4, v5

    if-gt v1, v4, :cond_2

    .line 226
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 227
    .local v0, "b":B
    if-ltz v0, :cond_1

    int-to-long v2, v0

    .line 255
    .end local v0    # "b":B
    :cond_0
    :goto_0
    return-wide v2

    .line 228
    .restart local v0    # "b":B
    :cond_1
    int-to-long v4, v0

    and-long v2, v4, v6

    .line 229
    .local v2, "i":J
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 230
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/4 v1, 0x7

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 231
    if-gez v0, :cond_0

    .line 232
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 233
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0xe

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 234
    if-gez v0, :cond_0

    .line 235
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 236
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x15

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 237
    if-gez v0, :cond_0

    .line 238
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 239
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x1c

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 240
    if-gez v0, :cond_0

    .line 241
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 242
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x23

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 243
    if-gez v0, :cond_0

    .line 244
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 245
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x2a

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 246
    if-gez v0, :cond_0

    .line 247
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 248
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x31

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 249
    if-gez v0, :cond_0

    .line 250
    iget-object v1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    aget-byte v0, v1, v4

    .line 251
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x38

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 252
    if-gez v0, :cond_0

    .line 253
    new-instance v1, Ljava/io/IOException;

    const-string v4, "Invalid vLong detected (negative values disallowed)"

    invoke-direct {v1, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 255
    .end local v0    # "b":B
    .end local v2    # "i":J
    :cond_2
    invoke-super {p0}, Lorg/apache/lucene/store/IndexInput;->readVLong()J

    move-result-wide v2

    goto/16 :goto_0
.end method

.method public final seek(J)V
    .locals 5
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 292
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 293
    iget-wide v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 300
    :goto_0
    return-void

    .line 295
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 296
    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 297
    iput v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 298
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/store/BufferedIndexInput;->seekInternal(J)V

    goto :goto_0
.end method

.method protected abstract seekInternal(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final setBufferSize(I)V
    .locals 8
    .param p1, "newSize"    # I

    .prologue
    const/4 v3, 0x0

    .line 72
    sget-boolean v4, Lorg/apache/lucene/store/BufferedIndexInput;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-eqz v4, :cond_1

    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    iget-object v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    array-length v5, v5

    if-eq v4, v5, :cond_1

    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "buffer="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bufferSize="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " buffer.length="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-eqz v6, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    array-length v3, v3

    :cond_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4

    .line 73
    :cond_1
    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    if-eq p1, v4, :cond_2

    .line 74
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/BufferedIndexInput;->checkBufferSize(I)V

    .line 75
    iput p1, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize:I

    .line 76
    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    if-eqz v4, :cond_2

    .line 80
    new-array v1, p1, [B

    .line 81
    .local v1, "newBuffer":[B
    iget v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    sub-int v0, v4, v5

    .line 83
    .local v0, "leftInBuffer":I
    if-le v0, p1, :cond_3

    .line 84
    move v2, p1

    .line 87
    .local v2, "numToCopy":I
    :goto_0
    iget-object v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->buffer:[B

    iget v5, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    invoke-static {v4, v5, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iget-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    iget v6, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferStart:J

    .line 89
    iput v3, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferPosition:I

    .line 90
    iput v2, p0, Lorg/apache/lucene/store/BufferedIndexInput;->bufferLength:I

    .line 91
    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/BufferedIndexInput;->newBuffer([B)V

    .line 94
    .end local v0    # "leftInBuffer":I
    .end local v1    # "newBuffer":[B
    .end local v2    # "numToCopy":I
    :cond_2
    return-void

    .line 86
    .restart local v0    # "leftInBuffer":I
    .restart local v1    # "newBuffer":[B
    :cond_3
    move v2, v0

    .restart local v2    # "numToCopy":I
    goto :goto_0
.end method
