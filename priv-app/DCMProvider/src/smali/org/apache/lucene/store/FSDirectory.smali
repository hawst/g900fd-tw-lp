.class public abstract Lorg/apache/lucene/store/FSDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "FSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/FSDirectory$FSIndexInput;,
        Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;
    }
.end annotation


# static fields
.field public static final DEFAULT_READ_CHUNK_SIZE:I


# instance fields
.field private chunkSize:I

.field protected final directory:Ljava/io/File;

.field protected final staleFiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    sput v0, Lorg/apache/lucene/store/FSDirectory;->DEFAULT_READ_CHUNK_SIZE:I

    return-void

    :cond_0
    const/high16 v0, 0x6400000

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 3
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 123
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    .line 124
    sget v0, Lorg/apache/lucene/store/FSDirectory;->DEFAULT_READ_CHUNK_SIZE:I

    iput v0, p0, Lorg/apache/lucene/store/FSDirectory;->chunkSize:I

    .line 139
    if-nez p2, :cond_0

    .line 140
    new-instance p2, Lorg/apache/lucene/store/NativeFSLockFactory;

    .end local p2    # "lockFactory":Lorg/apache/lucene/store/LockFactory;
    invoke-direct {p2}, Lorg/apache/lucene/store/NativeFSLockFactory;-><init>()V

    .line 142
    .restart local p2    # "lockFactory":Lorg/apache/lucene/store/LockFactory;
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/store/FSDirectory;->getCanonicalPath(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    .line 144
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    new-instance v0, Lorg/apache/lucene/store/NoSuchDirectoryException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "file \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' exists but is not a directory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/NoSuchDirectoryException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 147
    :cond_1
    invoke-virtual {p0, p2}, Lorg/apache/lucene/store/FSDirectory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 149
    return-void
.end method

.method public static fileModified(Ljava/io/File;Ljava/lang/String;)J
    .locals 4
    .param p0, "directory"    # Ljava/io/File;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 255
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 256
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    return-wide v2
.end method

.method private static getCanonicalPath(Ljava/io/File;)Ljava/io/File;
    .locals 2
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static listAll(Ljava/io/File;)[Ljava/lang/String;
    .locals 4
    .param p0, "dir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    new-instance v1, Lorg/apache/lucene/store/NoSuchDirectoryException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "directory \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' does not exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/NoSuchDirectoryException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 219
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    new-instance v1, Lorg/apache/lucene/store/NoSuchDirectoryException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' exists but is not a directory"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/lucene/store/NoSuchDirectoryException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 223
    :cond_1
    new-instance v1, Lorg/apache/lucene/store/FSDirectory$1;

    invoke-direct {v1}, Lorg/apache/lucene/store/FSDirectory$1;-><init>()V

    invoke-virtual {p0, v1}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "result":[Ljava/lang/String;
    if-nez v0, :cond_2

    .line 231
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "directory \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' exists and is a directory, but cannot be listed: list() returned null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 233
    :cond_2
    return-object v0
.end method

.method public static open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;
    .locals 1
    .param p0, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v0

    return-object v0
.end method

.method public static open(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)Lorg/apache/lucene/store/FSDirectory;
    .locals 1
    .param p0, "path"    # Ljava/io/File;
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->WINDOWS:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/apache/lucene/util/Constants;->SUN_OS:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/apache/lucene/util/Constants;->LINUX:Z

    if-eqz v0, :cond_1

    .line 179
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    if-eqz v0, :cond_1

    .line 180
    new-instance v0, Lorg/apache/lucene/store/MMapDirectory;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/MMapDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 184
    :goto_0
    return-object v0

    .line 181
    :cond_1
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->WINDOWS:Z

    if-eqz v0, :cond_2

    .line 182
    new-instance v0, Lorg/apache/lucene/store/SimpleFSDirectory;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/SimpleFSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    goto :goto_0

    .line 184
    :cond_2
    new-instance v0, Lorg/apache/lucene/store/NIOFSDirectory;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/NIOFSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 338
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lorg/apache/lucene/store/FSDirectory;->isOpen:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    monitor-exit p0

    return-void

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 285
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 287
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/FSDirectory;->ensureCanWrite(Ljava/lang/String;)V

    .line 288
    new-instance v0, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;-><init>(Lorg/apache/lucene/store/FSDirectory;Ljava/lang/String;)V

    return-object v0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 276
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 277
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_0

    .line 278
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot delete "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 279
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 280
    return-void
.end method

.method protected ensureCanWrite(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 292
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 293
    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 294
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot create directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 296
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 297
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    .line 298
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot overwrite: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 299
    :cond_1
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 248
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 249
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 250
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 263
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 264
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 265
    .local v2, "len":J
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 266
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 268
    :cond_0
    return-wide v2
.end method

.method protected fsync(Ljava/lang/String;)V
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 520
    new-instance v3, Ljava/io/File;

    iget-object v8, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-direct {v3, v8, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 521
    .local v3, "fullFile":Ljava/io/File;
    const/4 v7, 0x0

    .line 522
    .local v7, "success":Z
    const/4 v6, 0x0

    .line 523
    .local v6, "retryCount":I
    const/4 v0, 0x0

    .line 524
    .local v0, "exc":Ljava/io/IOException;
    :cond_0
    :goto_0
    if-nez v7, :cond_1

    const/4 v8, 0x5

    if-lt v6, v8, :cond_2

    .line 547
    :cond_1
    if-nez v7, :cond_5

    .line 549
    throw v0

    .line 525
    :cond_2
    add-int/lit8 v6, v6, 0x1

    .line 526
    const/4 v1, 0x0

    .line 529
    .local v1, "file":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v8, "rw"

    invoke-direct {v2, v3, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    .local v2, "file":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 531
    const/4 v7, 0x1

    .line 533
    if-eqz v2, :cond_0

    .line 534
    :try_start_2
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 536
    :catch_0
    move-exception v5

    move-object v1, v2

    .line 537
    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    .local v5, "ioe":Ljava/io/IOException;
    :goto_1
    if-nez v0, :cond_3

    .line 538
    move-object v0, v5

    .line 541
    :cond_3
    const-wide/16 v8, 0x5

    :try_start_3
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 542
    :catch_1
    move-exception v4

    .line 543
    .local v4, "ie":Ljava/lang/InterruptedException;
    new-instance v8, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v8, v4}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v8

    .line 532
    .end local v4    # "ie":Ljava/lang/InterruptedException;
    .end local v5    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 533
    :goto_2
    if-eqz v1, :cond_4

    .line 534
    :try_start_4
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 535
    :cond_4
    throw v8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 536
    :catch_2
    move-exception v5

    goto :goto_1

    .line 550
    .end local v1    # "file":Ljava/io/RandomAccessFile;
    :cond_5
    return-void

    .line 532
    .restart local v2    # "file":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "file":Ljava/io/RandomAccessFile;
    .restart local v1    # "file":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method public getDirectory()Ljava/io/File;
    .locals 1

    .prologue
    .line 343
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 344
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 7

    .prologue
    .line 319
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 322
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 327
    .local v3, "dirName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 328
    .local v2, "digest":I
    const/4 v1, 0x0

    .local v1, "charIDX":I
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v1, v5, :cond_0

    .line 332
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "lucene-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 323
    .end local v1    # "charIDX":I
    .end local v2    # "digest":I
    .end local v3    # "dirName":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 324
    .local v4, "e":Ljava/io/IOException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 329
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v1    # "charIDX":I
    .restart local v2    # "digest":I
    .restart local v3    # "dirName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 330
    .local v0, "ch":C
    mul-int/lit8 v5, v2, 0x1f

    add-int v2, v5, v0

    .line 328
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final getReadChunkSize()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lorg/apache/lucene/store/FSDirectory;->chunkSize:I

    return v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 242
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-static {v0}, Lorg/apache/lucene/store/FSDirectory;->listAll(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onIndexOutputClosed(Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;)V
    .locals 2
    .param p1, "io"    # Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;

    .prologue
    .line 302
    iget-object v0, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    # getter for: Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->name:Ljava/lang/String;
    invoke-static {p1}, Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;->access$0(Lorg/apache/lucene/store/FSDirectory$FSIndexOutput;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 303
    return-void
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 5
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 190
    invoke-super {p0, p1}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 194
    instance-of v2, p1, Lorg/apache/lucene/store/FSLockFactory;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 195
    check-cast v1, Lorg/apache/lucene/store/FSLockFactory;

    .line 196
    .local v1, "lf":Lorg/apache/lucene/store/FSLockFactory;
    invoke-virtual {v1}, Lorg/apache/lucene/store/FSLockFactory;->getLockDir()Ljava/io/File;

    move-result-object v0

    .line 198
    .local v0, "dir":Ljava/io/File;
    if-nez v0, :cond_1

    .line 199
    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/store/FSLockFactory;->setLockDir(Ljava/io/File;)V

    .line 200
    invoke-virtual {v1, v4}, Lorg/apache/lucene/store/FSLockFactory;->setLockPrefix(Ljava/lang/String;)V

    .line 206
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "lf":Lorg/apache/lucene/store/FSLockFactory;
    :cond_0
    :goto_0
    return-void

    .line 201
    .restart local v0    # "dir":Ljava/io/File;
    .restart local v1    # "lf":Lorg/apache/lucene/store/FSLockFactory;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 202
    invoke-virtual {v1, v4}, Lorg/apache/lucene/store/FSLockFactory;->setLockPrefix(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final setReadChunkSize(I)V
    .locals 2
    .param p1, "chunkSize"    # I

    .prologue
    .line 377
    if-gtz p1, :cond_0

    .line 378
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "chunkSize must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-nez v0, :cond_1

    .line 381
    iput p1, p0, Lorg/apache/lucene/store/FSDirectory;->chunkSize:I

    .line 383
    :cond_1
    return-void
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 307
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->ensureOpen()V

    .line 308
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 309
    .local v1, "toSync":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 311
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 314
    iget-object v2, p0, Lorg/apache/lucene/store/FSDirectory;->staleFiles:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 315
    return-void

    .line 311
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 312
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/FSDirectory;->fsync(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 350
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/FSDirectory;->directory:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lockFactory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/store/FSDirectory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
