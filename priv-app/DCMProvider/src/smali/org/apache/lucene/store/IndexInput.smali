.class public abstract Lorg/apache/lucene/store/IndexInput;
.super Lorg/apache/lucene/store/DataInput;
.source "IndexInput.java"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Cloneable;


# instance fields
.field private final resourceDescription:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "resourceDescription"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 46
    if-nez p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "resourceDescription must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/store/IndexInput;->resourceDescription:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/store/DataInput;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/store/IndexInput;
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lorg/apache/lucene/store/DataInput;->clone()Lorg/apache/lucene/store/DataInput;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IndexInput;

    return-object v0
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getFilePointer()J
.end method

.method public abstract length()J
.end method

.method public abstract seek(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/store/IndexInput;->resourceDescription:Ljava/lang/String;

    return-object v0
.end method
