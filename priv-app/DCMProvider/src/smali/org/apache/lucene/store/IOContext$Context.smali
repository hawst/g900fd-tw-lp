.class public final enum Lorg/apache/lucene/store/IOContext$Context;
.super Ljava/lang/Enum;
.source "IOContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/IOContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Context"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/store/IOContext$Context;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DEFAULT:Lorg/apache/lucene/store/IOContext$Context;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/store/IOContext$Context;

.field public static final enum FLUSH:Lorg/apache/lucene/store/IOContext$Context;

.field public static final enum MERGE:Lorg/apache/lucene/store/IOContext$Context;

.field public static final enum READ:Lorg/apache/lucene/store/IOContext$Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lorg/apache/lucene/store/IOContext$Context;

    const-string v1, "MERGE"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/store/IOContext$Context;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/store/IOContext$Context;->MERGE:Lorg/apache/lucene/store/IOContext$Context;

    new-instance v0, Lorg/apache/lucene/store/IOContext$Context;

    const-string v1, "READ"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/store/IOContext$Context;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/store/IOContext$Context;->READ:Lorg/apache/lucene/store/IOContext$Context;

    new-instance v0, Lorg/apache/lucene/store/IOContext$Context;

    const-string v1, "FLUSH"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/store/IOContext$Context;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/store/IOContext$Context;->FLUSH:Lorg/apache/lucene/store/IOContext$Context;

    new-instance v0, Lorg/apache/lucene/store/IOContext$Context;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/store/IOContext$Context;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/lucene/store/IOContext$Context;->DEFAULT:Lorg/apache/lucene/store/IOContext$Context;

    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/store/IOContext$Context;

    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->MERGE:Lorg/apache/lucene/store/IOContext$Context;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->READ:Lorg/apache/lucene/store/IOContext$Context;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->FLUSH:Lorg/apache/lucene/store/IOContext$Context;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/store/IOContext$Context;->DEFAULT:Lorg/apache/lucene/store/IOContext$Context;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/store/IOContext$Context;->ENUM$VALUES:[Lorg/apache/lucene/store/IOContext$Context;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/store/IOContext$Context;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/store/IOContext$Context;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/IOContext$Context;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/store/IOContext$Context;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/store/IOContext$Context;->ENUM$VALUES:[Lorg/apache/lucene/store/IOContext$Context;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/store/IOContext$Context;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
