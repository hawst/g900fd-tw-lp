.class public Lorg/apache/lucene/store/FileSwitchDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "FileSwitchDirectory.java"


# instance fields
.field private doClose:Z

.field private final primaryDir:Lorg/apache/lucene/store/Directory;

.field private final primaryExtensions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final secondaryDir:Lorg/apache/lucene/store/Directory;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;Z)V
    .locals 1
    .param p2, "primaryDir"    # Lorg/apache/lucene/store/Directory;
    .param p3, "secondaryDir"    # Lorg/apache/lucene/store/Directory;
    .param p4, "doClose"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/store/Directory;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "primaryExtensions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 49
    iput-object p1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryExtensions:Ljava/util/Set;

    .line 50
    iput-object p2, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    .line 51
    iput-object p3, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    .line 52
    iput-boolean p4, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->doClose:Z

    .line 53
    invoke-virtual {p2}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    .line 54
    return-void
.end method

.method private getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-static {p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "ext":Ljava/lang/String;
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryExtensions:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    .line 131
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    goto :goto_0
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 119
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 120
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 121
    const-string v1, ""

    .line 123
    :goto_0
    return-object v1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-boolean v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->doClose:Z

    if-eqz v0, :cond_0

    .line 70
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->doClose:Z

    .line 76
    :cond_0
    return-void

    .line 71
    :catchall_0
    move-exception v0

    .line 72
    iget-object v1, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Directory;->close()V

    .line 73
    throw v0
.end method

.method public createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    return-object v0
.end method

.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    move-result-object v0

    return-object v0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPrimaryDir()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getSecondaryDir()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 80
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 85
    .local v3, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 87
    .local v1, "exc":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :try_start_0
    iget-object v5, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    move v5, v4

    :goto_0
    if-lt v5, v7, :cond_1

    .line 94
    :goto_1
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5
    :try_end_1
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    if-lt v4, v6, :cond_2

    .line 111
    :cond_0
    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 112
    throw v1

    .line 87
    :cond_1
    :try_start_2
    aget-object v2, v6, v5

    .line 88
    .local v2, "f":Ljava/lang/String;
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_2 .. :try_end_2} :catch_0

    .line 87
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 90
    .end local v2    # "f":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    move-object v1, v0

    goto :goto_1

    .line 94
    .end local v0    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :cond_2
    :try_start_3
    aget-object v2, v5, v4

    .line 95
    .restart local v2    # "f":Ljava/lang/String;
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_3 .. :try_end_3} :catch_1

    .line 94
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 97
    .end local v2    # "f":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 100
    .restart local v0    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    if-eqz v1, :cond_3

    .line 101
    throw v1

    .line 105
    :cond_3
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 106
    throw v0

    .line 114
    .end local v0    # "e":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :cond_4
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    return-object v4
.end method

.method public openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/FileSwitchDirectory;->getDirectory(Ljava/lang/String;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 158
    .local v1, "primaryNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v2, "secondaryNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 166
    iget-object v3, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v1}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 167
    iget-object v3, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->secondaryDir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 168
    return-void

    .line 160
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 161
    .local v0, "name":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/store/FileSwitchDirectory;->primaryExtensions:Ljava/util/Set;

    invoke-static {v0}, Lorg/apache/lucene/store/FileSwitchDirectory;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 162
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    :cond_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
