.class public Lorg/apache/lucene/store/InputStreamDataInput;
.super Lorg/apache/lucene/store/DataInput;
.source "InputStreamDataInput.java"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final is:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/apache/lucene/store/InputStreamDataInput;->is:Ljava/io/InputStream;

    .line 32
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/store/InputStreamDataInput;->is:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 57
    return-void
.end method

.method public readByte()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v1, p0, Lorg/apache/lucene/store/InputStreamDataInput;->is:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 37
    .local v0, "v":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/io/EOFException;

    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    throw v1

    .line 38
    :cond_0
    int-to-byte v1, v0

    return v1
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    :goto_0
    if-gtz p3, :cond_0

    .line 52
    return-void

    .line 44
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/InputStreamDataInput;->is:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 45
    .local v0, "cnt":I
    if-gez v0, :cond_1

    .line 47
    new-instance v1, Ljava/io/EOFException;

    invoke-direct {v1}, Ljava/io/EOFException;-><init>()V

    throw v1

    .line 49
    :cond_1
    sub-int/2addr p3, v0

    .line 50
    add-int/2addr p2, v0

    goto :goto_0
.end method
