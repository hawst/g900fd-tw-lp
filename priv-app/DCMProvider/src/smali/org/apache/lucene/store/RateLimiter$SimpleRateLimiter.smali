.class public Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;
.super Lorg/apache/lucene/store/RateLimiter;
.source "RateLimiter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/RateLimiter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SimpleRateLimiter"
.end annotation


# instance fields
.field private volatile lastNS:J

.field private volatile mbPerSec:D

.field private volatile nsPerByte:D


# direct methods
.method public constructor <init>(D)V
    .locals 1
    .param p1, "mbPerSec"    # D

    .prologue
    .line 60
    invoke-direct {p0}, Lorg/apache/lucene/store/RateLimiter;-><init>()V

    .line 61
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->setMbPerSec(D)V

    .line 62
    return-void
.end method


# virtual methods
.method public getMbPerSec()D
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->mbPerSec:D

    return-wide v0
.end method

.method public pause(J)J
    .locals 19
    .param p1, "bytes"    # J

    .prologue
    .line 93
    const-wide/16 v12, 0x1

    cmp-long v5, p1, v12

    if-nez v5, :cond_0

    .line 94
    const-wide/16 v12, 0x0

    .line 121
    :goto_0
    return-wide v12

    .line 99
    :cond_0
    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->lastNS:J

    move-wide/from16 v0, p1

    long-to-double v14, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->nsPerByte:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    double-to-long v14, v14

    add-long v10, v12, v14

    move-object/from16 v0, p0

    iput-wide v10, v0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->lastNS:J

    .line 101
    .local v10, "targetNS":J
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    .local v8, "startNS":J
    move-wide v2, v8

    .line 102
    .local v2, "curNS":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->lastNS:J

    cmp-long v5, v12, v2

    if-gez v5, :cond_1

    .line 103
    move-object/from16 v0, p0

    iput-wide v2, v0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->lastNS:J

    .line 109
    :cond_1
    :goto_1
    sub-long v6, v10, v2

    .line 110
    .local v6, "pauseNS":J
    const-wide/16 v12, 0x0

    cmp-long v5, v6, v12

    if-lez v5, :cond_2

    .line 112
    const-wide/32 v12, 0xf4240

    :try_start_0
    div-long v12, v6, v12

    long-to-int v5, v12

    int-to-long v12, v5

    const-wide/32 v14, 0xf4240

    rem-long v14, v6, v14

    long-to-int v5, v14

    invoke-static {v12, v13, v5}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 108
    goto :goto_1

    .line 113
    :catch_0
    move-exception v4

    .line 114
    .local v4, "ie":Ljava/lang/InterruptedException;
    new-instance v5, Lorg/apache/lucene/util/ThreadInterruptedException;

    invoke-direct {v5, v4}, Lorg/apache/lucene/util/ThreadInterruptedException;-><init>(Ljava/lang/InterruptedException;)V

    throw v5

    .line 121
    .end local v4    # "ie":Ljava/lang/InterruptedException;
    :cond_2
    sub-long v12, v2, v8

    goto :goto_0
.end method

.method public setMbPerSec(D)V
    .locals 5
    .param p1, "mbPerSec"    # D

    .prologue
    .line 69
    iput-wide p1, p0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->mbPerSec:D

    .line 70
    const-wide v0, 0x41cdcd6500000000L    # 1.0E9

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    mul-double/2addr v2, p1

    div-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;->nsPerByte:D

    .line 72
    return-void
.end method
