.class public Lorg/apache/lucene/store/NRTCachingDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "NRTCachingDirectory.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final VERBOSE:Z


# instance fields
.field private final cache:Lorg/apache/lucene/store/RAMDirectory;

.field private final delegate:Lorg/apache/lucene/store/Directory;

.field private final maxCachedBytes:J

.field private final maxMergeSizeBytes:J

.field private final uncacheLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lorg/apache/lucene/store/NRTCachingDirectory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/NRTCachingDirectory;->$assertionsDisabled:Z

    .line 74
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;DD)V
    .locals 4
    .param p1, "delegate"    # Lorg/apache/lucene/store/Directory;
    .param p2, "maxMergeSizeMB"    # D
    .param p4, "maxCachedMB"    # D

    .prologue
    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    .line 81
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 67
    new-instance v0, Lorg/apache/lucene/store/RAMDirectory;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMDirectory;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    .line 283
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->uncacheLock:Ljava/lang/Object;

    .line 82
    iput-object p1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    .line 83
    mul-double v0, p2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxMergeSizeBytes:J

    .line 84
    mul-double v0, p4, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxCachedBytes:J

    .line 85
    return-void
.end method

.method private unCache(Ljava/lang/String;)V
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iget-object v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->uncacheLock:Ljava/lang/Object;

    monitor-enter v4

    .line 292
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 294
    monitor-exit v4

    .line 316
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 297
    new-instance v3, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cannot uncache file=\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\": it was separately also created in the delegate directory"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 288
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 299
    :cond_1
    :try_start_1
    sget-object v0, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    .line 300
    .local v0, "context":Lorg/apache/lucene/store/IOContext;
    iget-object v3, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, p1, v0}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 301
    .local v2, "out":Lorg/apache/lucene/store/IndexOutput;
    const/4 v1, 0x0

    .line 303
    .local v1, "in":Lorg/apache/lucene/store/IndexInput;
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v3, p1, v0}, Lorg/apache/lucene/store/RAMDirectory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 304
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    invoke-virtual {v2, v1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 305
    const/4 v3, 0x2

    :try_start_3
    new-array v3, v3, [Ljava/io/Closeable;

    const/4 v5, 0x0

    .line 306
    aput-object v1, v3, v5

    const/4 v5, 0x1

    aput-object v2, v3, v5

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 310
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 313
    :try_start_4
    iget-object v3, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/store/RAMDirectory;->deleteFile(Ljava/lang/String;)V

    .line 310
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 288
    :try_start_5
    monitor-exit v4

    goto :goto_0

    .line 305
    :catchall_1
    move-exception v3

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    .line 306
    aput-object v1, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-static {v5}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 307
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 310
    :catchall_2
    move-exception v3

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->clearLock(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMDirectory;->listAll()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_0

    .line 264
    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMDirectory;->close()V

    .line 265
    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Directory;->close()V

    .line 266
    return-void

    .line 261
    :cond_0
    aget-object v0, v2, v1

    .line 262
    .local v0, "fileName":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/apache/lucene/store/NRTCachingDirectory;->unCache(Ljava/lang/String;)V

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/store/NRTCachingDirectory;->doCacheWrite(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 199
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/RAMDirectory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    .line 206
    :goto_1
    return-object v0

    .line 202
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 206
    :goto_2
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    goto :goto_1

    .line 203
    :catch_0
    move-exception v0

    goto :goto_2

    .line 196
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/NRTCachingDirectory;->ensureOpen()V

    .line 242
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/RAMDirectory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 248
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    sget-boolean v0, Lorg/apache/lucene/store/NRTCachingDirectory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "name="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 166
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 170
    :goto_0
    monitor-exit p0

    return-void

    .line 168
    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected doCacheWrite(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Z
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;

    .prologue
    .line 273
    const-wide/16 v0, 0x0

    .line 274
    .local v0, "bytes":J
    iget-object v2, p2, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    if-eqz v2, :cond_1

    .line 275
    iget-object v2, p2, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    iget-wide v0, v2, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    .line 280
    :cond_0
    :goto_0
    const-string v2, "segments.gen"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-wide v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxMergeSizeBytes:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v2}, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes()J

    move-result-wide v2

    add-long/2addr v2, v0

    iget-wide v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxCachedBytes:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 276
    :cond_1
    iget-object v2, p2, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    if-eqz v2, :cond_0

    .line 277
    iget-object v2, p2, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    iget-wide v0, v2, Lorg/apache/lucene/store/FlushInfo;->estimatedSegmentSize:J

    goto :goto_0

    .line 280
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public declared-synchronized fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized fileLength(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileLength(Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 177
    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDelegate()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getLockFactory()Lorg/apache/lucene/store/LockFactory;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v0

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized listAll()[Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 123
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 124
    .local v2, "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v4}, Lorg/apache/lucene/store/RAMDirectory;->listAll()[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v4, v3

    :goto_0
    if-lt v4, v6, :cond_1

    .line 132
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4
    :try_end_1
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-lt v3, v5, :cond_2

    .line 145
    :cond_0
    :try_start_2
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v3

    .line 124
    :cond_1
    :try_start_3
    aget-object v1, v5, v4

    .line 125
    .local v1, "f":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 132
    .end local v1    # "f":Ljava/lang/String;
    :cond_2
    :try_start_4
    aget-object v1, v4, v3

    .line 136
    .restart local v1    # "f":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 132
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 138
    .end local v1    # "f":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 141
    .local v0, "ex":Lorg/apache/lucene/store/NoSuchDirectoryException;
    :try_start_5
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 142
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 123
    .end local v0    # "ex":Lorg/apache/lucene/store/NoSuchDirectoryException;
    .end local v2    # "files":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public listCachedFiles()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMDirectory;->listAll()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/RAMDirectory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/RAMDirectory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 232
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "lf"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 99
    return-void
.end method

.method public sizeInBytes()J
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->cache:Lorg/apache/lucene/store/RAMDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "fileNames":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 218
    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 219
    return-void

    .line 215
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 216
    .local v0, "fileName":Ljava/lang/String;
    invoke-direct {p0, v0}, Lorg/apache/lucene/store/NRTCachingDirectory;->unCache(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NRTCachingDirectory("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; maxCacheMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxCachedBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " maxMergeSizeMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/store/NRTCachingDirectory;->maxMergeSizeBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
