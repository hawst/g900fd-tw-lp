.class public Lorg/apache/lucene/store/MergeInfo;
.super Ljava/lang/Object;
.source "MergeInfo.java"


# instance fields
.field public final estimatedMergeBytes:J

.field public final isExternal:Z

.field public final mergeMaxNumSegments:I

.field public final totalDocCount:I


# direct methods
.method public constructor <init>(IJZI)V
    .locals 0
    .param p1, "totalDocCount"    # I
    .param p2, "estimatedMergeBytes"    # J
    .param p4, "isExternal"    # Z
    .param p5, "mergeMaxNumSegments"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lorg/apache/lucene/store/MergeInfo;->totalDocCount:I

    .line 45
    iput-wide p2, p0, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    .line 46
    iput-boolean p4, p0, Lorg/apache/lucene/store/MergeInfo;->isExternal:Z

    .line 47
    iput p5, p0, Lorg/apache/lucene/store/MergeInfo;->mergeMaxNumSegments:I

    .line 48
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 65
    if-ne p0, p1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 68
    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 70
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 71
    check-cast v0, Lorg/apache/lucene/store/MergeInfo;

    .line 72
    .local v0, "other":Lorg/apache/lucene/store/MergeInfo;
    iget-wide v4, p0, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    iget-wide v6, v0, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 73
    goto :goto_0

    .line 74
    :cond_4
    iget-boolean v3, p0, Lorg/apache/lucene/store/MergeInfo;->isExternal:Z

    iget-boolean v4, v0, Lorg/apache/lucene/store/MergeInfo;->isExternal:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 75
    goto :goto_0

    .line 76
    :cond_5
    iget v3, p0, Lorg/apache/lucene/store/MergeInfo;->mergeMaxNumSegments:I

    iget v4, v0, Lorg/apache/lucene/store/MergeInfo;->mergeMaxNumSegments:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 77
    goto :goto_0

    .line 78
    :cond_6
    iget v3, p0, Lorg/apache/lucene/store/MergeInfo;->totalDocCount:I

    iget v4, v0, Lorg/apache/lucene/store/MergeInfo;->totalDocCount:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 79
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 53
    const/16 v0, 0x1f

    .line 54
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 56
    .local v1, "result":I
    iget-wide v2, p0, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    iget-wide v4, p0, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    .line 55
    add-int/lit8 v1, v2, 0x1f

    .line 57
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/store/MergeInfo;->isExternal:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x4cf

    :goto_0
    add-int v1, v3, v2

    .line 58
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/store/MergeInfo;->mergeMaxNumSegments:I

    add-int v1, v2, v3

    .line 59
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lorg/apache/lucene/store/MergeInfo;->totalDocCount:I

    add-int v1, v2, v3

    .line 60
    return v1

    .line 57
    :cond_0
    const/16 v2, 0x4d5

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MergeInfo [totalDocCount="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/lucene/store/MergeInfo;->totalDocCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 86
    const-string v1, ", estimatedMergeBytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/apache/lucene/store/MergeInfo;->estimatedMergeBytes:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isExternal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 87
    iget-boolean v1, p0, Lorg/apache/lucene/store/MergeInfo;->isExternal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mergeMaxNumSegments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/store/MergeInfo;->mergeMaxNumSegments:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
