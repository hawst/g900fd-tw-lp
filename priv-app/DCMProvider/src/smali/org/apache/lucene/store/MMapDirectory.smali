.class public Lorg/apache/lucene/store/MMapDirectory;
.super Lorg/apache/lucene/store/FSDirectory;
.source "MMapDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_MAX_BUFF:I

.field public static final UNMAP_SUPPORTED:Z


# instance fields
.field final chunkSizePower:I

.field private useUnmapHack:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 79
    const-class v3, Lorg/apache/lucene/store/MMapDirectory;

    invoke-virtual {v3}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    sput-boolean v2, Lorg/apache/lucene/store/MMapDirectory;->$assertionsDisabled:Z

    .line 85
    sget-boolean v2, Lorg/apache/lucene/util/Constants;->JRE_IS_64BIT:Z

    if-eqz v2, :cond_1

    const/high16 v2, 0x40000000    # 2.0f

    :goto_0
    sput v2, Lorg/apache/lucene/store/MMapDirectory;->DEFAULT_MAX_BUFF:I

    .line 144
    :try_start_0
    const-string v2, "sun.misc.Cleaner"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    .line 145
    const-string v2, "java.nio.DirectByteBuffer"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 146
    const-string v3, "cleaner"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    const/4 v1, 0x1

    .line 151
    .local v1, "v":Z
    :goto_1
    sput-boolean v1, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    .line 152
    return-void

    .line 85
    .end local v1    # "v":Z
    :cond_1
    const/high16 v2, 0x10000000

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .restart local v1    # "v":Z
    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/MMapDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    sget v0, Lorg/apache/lucene/store/MMapDirectory;->DEFAULT_MAX_BUFF:I

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/lucene/store/MMapDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;I)V

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;I)V
    .locals 2
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .param p3, "maxChunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 80
    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    iput-boolean v0, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    .line 130
    if-gtz p3, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Maximum chunk size for mmap must be >0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    invoke-static {p3}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1f

    iput v0, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    .line 134
    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    if-ltz v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_2
    return-void
.end method


# virtual methods
.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/store/MMapDirectory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    .line 204
    .local v0, "full":Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
    new-instance v1, Lorg/apache/lucene/store/MMapDirectory$1;

    invoke-direct {v1, p0, p0, v0}, Lorg/apache/lucene/store/MMapDirectory$1;-><init>(Lorg/apache/lucene/store/MMapDirectory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;)V

    return-object v1
.end method

.method public final getMaxChunkSize()I
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x1

    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    shl-int/2addr v0, v1

    return v0
.end method

.method public getUseUnmap()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    return v0
.end method

.method map(Ljava/io/RandomAccessFile;JJ)[Ljava/nio/ByteBuffer;
    .locals 14
    .param p1, "raf"    # Ljava/io/RandomAccessFile;
    .param p2, "offset"    # J
    .param p4, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    ushr-long v2, p4, v1

    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 267
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RandomAccessFile too big for chunk size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 269
    :cond_0
    const-wide/16 v2, 0x1

    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    shl-long v12, v2, v1

    .line 272
    .local v12, "chunkSize":J
    iget v1, p0, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    ushr-long v2, p4, v1

    long-to-int v1, v2

    add-int/lit8 v11, v1, 0x1

    .line 274
    .local v11, "nrBuffers":I
    new-array v10, v11, [Ljava/nio/ByteBuffer;

    .line 276
    .local v10, "buffers":[Ljava/nio/ByteBuffer;
    const-wide/16 v8, 0x0

    .line 277
    .local v8, "bufferStart":J
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 278
    .local v0, "rafc":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .local v6, "bufNr":I
    :goto_0
    if-lt v6, v11, :cond_1

    .line 287
    return-object v10

    .line 279
    :cond_1
    add-long v2, v8, v12

    cmp-long v1, p4, v2

    if-lez v1, :cond_2

    move-wide v2, v12

    :goto_1
    long-to-int v7, v2

    .line 283
    .local v7, "bufSize":I
    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_ONLY:Ljava/nio/channels/FileChannel$MapMode;

    add-long v2, p2, v8

    int-to-long v4, v7

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v1

    aput-object v1, v10, v6

    .line 284
    int-to-long v2, v7

    add-long/2addr v8, v2

    .line 278
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 281
    .end local v7    # "bufSize":I
    :cond_2
    sub-long v2, p4, v8

    goto :goto_1
.end method

.method public openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-virtual {p0}, Lorg/apache/lucene/store/MMapDirectory;->ensureOpen()V

    .line 192
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/lucene/store/MMapDirectory;->getDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 193
    .local v0, "f":Ljava/io/File;
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 195
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v2, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MMapIndexInput(path=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3, v1}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;-><init>(Lorg/apache/lucene/store/MMapDirectory;Ljava/lang/String;Ljava/io/RandomAccessFile;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 195
    return-object v2

    .line 196
    :catchall_0
    move-exception v2

    .line 197
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    .line 198
    throw v2
.end method

.method public setUseUnmap(Z)V
    .locals 2
    .param p1, "useUnmapHack"    # Z

    .prologue
    .line 167
    if-eqz p1, :cond_0

    sget-boolean v0, Lorg/apache/lucene/store/MMapDirectory;->UNMAP_SUPPORTED:Z

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unmap hack not supported on this platform!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iput-boolean p1, p0, Lorg/apache/lucene/store/MMapDirectory;->useUnmapHack:Z

    .line 170
    return-void
.end method
