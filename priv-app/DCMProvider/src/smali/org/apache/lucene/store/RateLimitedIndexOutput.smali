.class final Lorg/apache/lucene/store/RateLimitedIndexOutput;
.super Lorg/apache/lucene/store/BufferedIndexOutput;
.source "RateLimitedIndexOutput.java"


# instance fields
.field private final bufferedDelegate:Lorg/apache/lucene/store/BufferedIndexOutput;

.field private final delegate:Lorg/apache/lucene/store/IndexOutput;

.field private final rateLimiter:Lorg/apache/lucene/store/RateLimiter;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/RateLimiter;Lorg/apache/lucene/store/IndexOutput;)V
    .locals 1
    .param p1, "rateLimiter"    # Lorg/apache/lucene/store/RateLimiter;
    .param p2, "delegate"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;-><init>()V

    .line 33
    instance-of v0, p2, Lorg/apache/lucene/store/BufferedIndexOutput;

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 34
    check-cast v0, Lorg/apache/lucene/store/BufferedIndexOutput;

    iput-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->bufferedDelegate:Lorg/apache/lucene/store/BufferedIndexOutput;

    .line 35
    iput-object p2, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    .line 40
    :goto_0
    iput-object p1, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->rateLimiter:Lorg/apache/lucene/store/RateLimiter;

    .line 41
    return-void

    .line 37
    :cond_0
    iput-object p2, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->bufferedDelegate:Lorg/apache/lucene/store/BufferedIndexOutput;

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    :try_start_0
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 82
    return-void

    .line 79
    :catchall_0
    move-exception v0

    .line 80
    iget-object v1, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 81
    throw v0
.end method

.method public flush()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    :try_start_0
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexOutput;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 73
    return-void

    .line 70
    :catchall_0
    move-exception v0

    .line 71
    iget-object v1, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 72
    throw v0
.end method

.method protected flushBuffer([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->rateLimiter:Lorg/apache/lucene/store/RateLimiter;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/RateLimiter;->pause(J)J

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->bufferedDelegate:Lorg/apache/lucene/store/BufferedIndexOutput;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->bufferedDelegate:Lorg/apache/lucene/store/BufferedIndexOutput;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/store/BufferedIndexOutput;->flushBuffer([BII)V

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    goto :goto_0
.end method

.method public length()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public seek(J)V
    .locals 1
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedIndexOutput;->flush()V

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedIndexOutput;->delegate:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/IndexOutput;->seek(J)V

    .line 63
    return-void
.end method
