.class Lorg/apache/lucene/store/SimpleFSDirectory$1;
.super Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.source "SimpleFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/SimpleFSDirectory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/SimpleFSDirectory;

.field private final synthetic val$context:Lorg/apache/lucene/store/IOContext;

.field private final synthetic val$descriptor:Ljava/io/RandomAccessFile;

.field private final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/SimpleFSDirectory;Lorg/apache/lucene/store/Directory;Ljava/io/RandomAccessFile;Ljava/io/File;Lorg/apache/lucene/store/IOContext;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->this$0:Lorg/apache/lucene/store/SimpleFSDirectory;

    iput-object p3, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    iput-object p4, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$file:Ljava/io/File;

    iput-object p5, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$context:Lorg/apache/lucene/store/IOContext;

    .line 67
    invoke-direct {p0, p2}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 72
    return-void
.end method

.method public openFullSlice()Lorg/apache/lucene/store/IndexInput;
    .locals 7

    .prologue
    .line 83
    :try_start_0
    const-string v1, "full-slice"

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/SimpleFSDirectory$1;->openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 84
    :catch_0
    move-exception v6

    .line 85
    .local v6, "ex":Ljava/io/IOException;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    .locals 10
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "offset"    # J
    .param p4, "length"    # J

    .prologue
    .line 76
    new-instance v1, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "SimpleFSIndexInput("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in path=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\" slice="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-long v2, p2, p4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    .line 77
    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->val$context:Lorg/apache/lucene/store/IOContext;

    invoke-static {v0}, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize(Lorg/apache/lucene/store/IOContext;)I

    move-result v8

    iget-object v0, p0, Lorg/apache/lucene/store/SimpleFSDirectory$1;->this$0:Lorg/apache/lucene/store/SimpleFSDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/SimpleFSDirectory;->getReadChunkSize()I

    move-result v9

    move-wide v4, p2

    move-wide v6, p4

    .line 76
    invoke-direct/range {v1 .. v9}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;-><init>(Ljava/lang/String;Ljava/io/RandomAccessFile;JJII)V

    return-object v1
.end method
