.class public abstract Lorg/apache/lucene/store/LockFactory;
.super Ljava/lang/Object;
.source "LockFactory.java"


# instance fields
.field protected lockPrefix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/LockFactory;->lockPrefix:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public abstract clearLock(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getLockPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lorg/apache/lucene/store/LockFactory;->lockPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public abstract makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
.end method

.method public setLockPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1, "lockPrefix"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lorg/apache/lucene/store/LockFactory;->lockPrefix:Ljava/lang/String;

    .line 52
    return-void
.end method
