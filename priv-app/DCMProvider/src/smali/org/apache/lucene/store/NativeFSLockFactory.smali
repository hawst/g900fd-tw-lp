.class public Lorg/apache/lucene/store/NativeFSLockFactory;
.super Lorg/apache/lucene/store/FSLockFactory;
.source "NativeFSLockFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/NativeFSLockFactory;-><init>(Ljava/io/File;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 0
    .param p1, "lockDir"    # Ljava/io/File;

    .prologue
    .line 86
    invoke-direct {p0}, Lorg/apache/lucene/store/FSLockFactory;-><init>()V

    .line 87
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/NativeFSLockFactory;->setLockDir(Ljava/io/File;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "lockDirName"    # Ljava/lang/String;

    .prologue
    .line 77
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/NativeFSLockFactory;-><init>(Ljava/io/File;)V

    .line 78
    return-void
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 2
    .param p1, "lockName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/NativeFSLockFactory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->release()V

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockPrefix:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockPrefix:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 117
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 119
    :cond_1
    return-void
.end method

.method public declared-synchronized makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 2
    .param p1, "lockName"    # Ljava/lang/String;

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockPrefix:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockPrefix:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 94
    :cond_0
    new-instance v0, Lorg/apache/lucene/store/NativeFSLock;

    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLockFactory;->lockDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/store/NativeFSLock;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
