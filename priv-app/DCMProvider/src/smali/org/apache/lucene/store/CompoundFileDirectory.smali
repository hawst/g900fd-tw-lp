.class public final Lorg/apache/lucene/store/CompoundFileDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "CompoundFileDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final CODEC_MAGIC_BYTE1:B = 0x3ft

.field private static final CODEC_MAGIC_BYTE2:B = -0x29t

.field private static final CODEC_MAGIC_BYTE3:B = 0x6ct

.field private static final CODEC_MAGIC_BYTE4:B = 0x17t

.field private static final SENTINEL:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final directory:Lorg/apache/lucene/store/Directory;

.field private final entries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final fileName:Ljava/lang/String;

.field private final handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

.field private final openForWrite:Z

.field protected final readBufferSize:I

.field private final writer:Lorg/apache/lucene/store/CompoundFileWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lorg/apache/lucene/store/CompoundFileDirectory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/CompoundFileDirectory;->$assertionsDisabled:Z

    .line 87
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/store/CompoundFileDirectory;->SENTINEL:Ljava/util/Map;

    .line 125
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;Z)V
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p4, "openForWrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 95
    iput-object p1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->directory:Lorg/apache/lucene/store/Directory;

    .line 96
    iput-object p2, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->fileName:Ljava/lang/String;

    .line 97
    invoke-static {p3}, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize(Lorg/apache/lucene/store/IOContext;)I

    move-result v1

    iput v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->readBufferSize:I

    .line 98
    iput-boolean v4, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->isOpen:Z

    .line 99
    iput-boolean p4, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->openForWrite:Z

    .line 100
    if-nez p4, :cond_2

    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "success":Z
    invoke-virtual {p1, p2, p3}, Lorg/apache/lucene/store/Directory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    .line 104
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    invoke-static {v1, p1, p2}, Lorg/apache/lucene/store/CompoundFileDirectory;->readEntries(Lorg/apache/lucene/store/Directory$IndexInputSlicer;Lorg/apache/lucene/store/Directory;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    const/4 v0, 0x1

    .line 107
    if-nez v0, :cond_0

    new-array v1, v3, [Ljava/io/Closeable;

    .line 108
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    aput-object v2, v1, v4

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 111
    :cond_0
    iput-boolean v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->isOpen:Z

    .line 112
    iput-object v5, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    .line 120
    .end local v0    # "success":Z
    :goto_0
    return-void

    .line 106
    .restart local v0    # "success":Z
    :catchall_0
    move-exception v1

    .line 107
    if-nez v0, :cond_1

    new-array v2, v3, [Ljava/io/Closeable;

    .line 108
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    aput-object v3, v2, v4

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 110
    :cond_1
    throw v1

    .line 114
    .end local v0    # "success":Z
    :cond_2
    sget-boolean v1, Lorg/apache/lucene/store/CompoundFileDirectory;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    instance-of v1, p1, Lorg/apache/lucene/store/CompoundFileDirectory;

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "compound file inside of compound file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 115
    :cond_3
    sget-object v1, Lorg/apache/lucene/store/CompoundFileDirectory;->SENTINEL:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    .line 116
    iput-boolean v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->isOpen:Z

    .line 117
    new-instance v1, Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-direct {v1, p1, p2}, Lorg/apache/lucene/store/CompoundFileWriter;-><init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    .line 118
    iput-object v5, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    goto :goto_0
.end method

.method static synthetic access$0(Lorg/apache/lucene/store/CompoundFileDirectory;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    return-object v0
.end method

.method private static final readEntries(Lorg/apache/lucene/store/Directory$IndexInputSlicer;Lorg/apache/lucene/store/Directory;Ljava/lang/String;)Ljava/util/Map;
    .locals 22
    .param p0, "handle"    # Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory$IndexInputSlicer;",
            "Lorg/apache/lucene/store/Directory;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    const/4 v15, 0x0

    .line 131
    .local v15, "priorE":Ljava/io/IOException;
    const/16 v17, 0x0

    .local v17, "stream":Lorg/apache/lucene/store/IndexInput;
    const/4 v5, 0x0

    .line 136
    .local v5, "entriesStream":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;->openFullSlice()Lorg/apache/lucene/store/IndexInput;

    move-result-object v17

    .line 137
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v7

    .line 140
    .local v7, "firstInt":I
    const/16 v19, 0x3f

    move/from16 v0, v19

    if-ne v7, v0, :cond_4

    .line 141
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v16

    .line 142
    .local v16, "secondByte":B
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v18

    .line 143
    .local v18, "thirdByte":B
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v8

    .line 144
    .local v8, "fourthByte":B
    const/16 v19, -0x29

    move/from16 v0, v16

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 145
    const/16 v19, 0x6c

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 146
    const/16 v19, 0x17

    move/from16 v0, v19

    if-eq v8, v0, :cond_1

    .line 147
    :cond_0
    new-instance v19, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Illegal/impossible header for CFS file: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 148
    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ","

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 147
    invoke-direct/range {v19 .. v20}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    .end local v7    # "firstInt":I
    .end local v8    # "fourthByte":B
    .end local v16    # "secondByte":B
    .end local v18    # "thirdByte":B
    :catch_0
    move-exception v11

    .line 175
    .local v11, "ioe":Ljava/io/IOException;
    move-object v15, v11

    .line 177
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v17, v19, v20

    const/16 v20, 0x1

    aput-object v5, v19, v20

    move-object/from16 v0, v19

    invoke-static {v15, v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 180
    new-instance v19, Ljava/lang/AssertionError;

    const-string v20, "impossible to get here"

    invoke-direct/range {v19 .. v20}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v19

    .line 150
    .end local v11    # "ioe":Ljava/io/IOException;
    .restart local v7    # "firstInt":I
    .restart local v8    # "fourthByte":B
    .restart local v16    # "secondByte":B
    .restart local v18    # "thirdByte":B
    :cond_1
    :try_start_1
    const-string v19, "CompoundFileWriterData"

    .line 151
    const/16 v20, 0x0

    const/16 v21, 0x0

    .line 150
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeaderNoMagic(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 153
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/index/IndexFileNames;->stripExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, ""

    .line 154
    const-string v21, "cfe"

    .line 152
    invoke-static/range {v19 .. v21}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 155
    .local v4, "entriesFileName":Ljava/lang/String;
    sget-object v19, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v5

    .line 156
    const-string v19, "CompoundFileWriterEntries"

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v5, v0, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 157
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v13

    .line 158
    .local v13, "numEntries":I
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12, v13}, Ljava/util/HashMap;-><init>(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    .local v12, "mapping":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-lt v9, v13, :cond_2

    .line 177
    .end local v4    # "entriesFileName":Ljava/lang/String;
    .end local v8    # "fourthByte":B
    .end local v9    # "i":I
    .end local v13    # "numEntries":I
    .end local v16    # "secondByte":B
    .end local v18    # "thirdByte":B
    :goto_1
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v17, v19, v20

    const/16 v20, 0x1

    aput-object v5, v19, v20

    move-object/from16 v0, v19

    invoke-static {v15, v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 173
    return-object v12

    .line 160
    .restart local v4    # "entriesFileName":Ljava/lang/String;
    .restart local v8    # "fourthByte":B
    .restart local v9    # "i":I
    .restart local v13    # "numEntries":I
    .restart local v16    # "secondByte":B
    .restart local v18    # "thirdByte":B
    :cond_2
    :try_start_2
    new-instance v6, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    invoke-direct {v6}, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;-><init>()V

    .line 161
    .local v6, "fileEntry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v10

    .line 162
    .local v10, "id":Ljava/lang/String;
    invoke-interface {v12, v10, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .line 163
    .local v14, "previous":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    if-eqz v14, :cond_3

    .line 164
    new-instance v19, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Duplicate cfs entry id="

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " in CFS: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 176
    .end local v4    # "entriesFileName":Ljava/lang/String;
    .end local v6    # "fileEntry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .end local v7    # "firstInt":I
    .end local v8    # "fourthByte":B
    .end local v9    # "i":I
    .end local v10    # "id":Ljava/lang/String;
    .end local v12    # "mapping":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;>;"
    .end local v13    # "numEntries":I
    .end local v14    # "previous":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .end local v16    # "secondByte":B
    .end local v18    # "thirdByte":B
    :catchall_0
    move-exception v19

    .line 177
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v17, v20, v21

    const/16 v21, 0x1

    aput-object v5, v20, v21

    move-object/from16 v0, v20

    invoke-static {v15, v0}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 178
    throw v19

    .line 166
    .restart local v4    # "entriesFileName":Ljava/lang/String;
    .restart local v6    # "fileEntry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .restart local v7    # "firstInt":I
    .restart local v8    # "fourthByte":B
    .restart local v9    # "i":I
    .restart local v10    # "id":Ljava/lang/String;
    .restart local v12    # "mapping":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;>;"
    .restart local v13    # "numEntries":I
    .restart local v14    # "previous":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .restart local v16    # "secondByte":B
    .restart local v18    # "thirdByte":B
    :cond_3
    :try_start_3
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v20

    move-wide/from16 v0, v20

    iput-wide v0, v6, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->offset:J

    .line 167
    invoke-virtual {v5}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v20

    move-wide/from16 v0, v20

    iput-wide v0, v6, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->length:J

    .line 159
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 171
    .end local v4    # "entriesFileName":Ljava/lang/String;
    .end local v6    # "fileEntry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .end local v8    # "fourthByte":B
    .end local v9    # "i":I
    .end local v10    # "id":Ljava/lang/String;
    .end local v12    # "mapping":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;>;"
    .end local v13    # "numEntries":I
    .end local v14    # "previous":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .end local v16    # "secondByte":B
    .end local v18    # "thirdByte":B
    :cond_4
    move-object/from16 v0, v17

    invoke-static {v0, v7}, Lorg/apache/lucene/store/CompoundFileDirectory;->readLegacyEntries(Lorg/apache/lucene/store/IndexInput;I)Ljava/util/Map;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    .restart local v12    # "mapping":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;>;"
    goto :goto_1
.end method

.method private static readLegacyEntries(Lorg/apache/lucene/store/IndexInput;I)Ljava/util/Map;
    .locals 14
    .param p0, "stream"    # Lorg/apache/lucene/store/IndexInput;
    .param p1, "firstInt"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/IndexInput;",
            "I)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/CorruptIndexException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 188
    .local v1, "entries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;>;"
    if-gez p1, :cond_2

    .line 189
    const/4 v11, -0x1

    if-ge p1, v11, :cond_0

    .line 190
    new-instance v11, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Incompatible format version: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " expected >= "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, -0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " (resource: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 190
    invoke-direct {v11, v12}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 194
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readVInt()I

    move-result v0

    .line 195
    .local v0, "count":I
    const/4 v10, 0x0

    .line 202
    .local v10, "stripSegmentName":Z
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v8

    .line 203
    .local v8, "streamLength":J
    const/4 v2, 0x0

    .line 204
    .local v2, "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-lt v3, v0, :cond_3

    .line 232
    if-eqz v2, :cond_1

    .line 233
    iget-wide v12, v2, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->offset:J

    sub-long v12, v8, v12

    iput-wide v12, v2, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->length:J

    .line 236
    :cond_1
    return-object v1

    .line 197
    .end local v0    # "count":I
    .end local v2    # "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .end local v3    # "i":I
    .end local v8    # "streamLength":J
    .end local v10    # "stripSegmentName":Z
    :cond_2
    move v0, p1

    .line 198
    .restart local v0    # "count":I
    const/4 v10, 0x1

    .restart local v10    # "stripSegmentName":Z
    goto :goto_0

    .line 205
    .restart local v2    # "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    .restart local v3    # "i":I
    .restart local v8    # "streamLength":J
    :cond_3
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readLong()J

    move-result-wide v6

    .line 206
    .local v6, "offset":J
    const-wide/16 v12, 0x0

    cmp-long v11, v6, v12

    if-ltz v11, :cond_4

    cmp-long v11, v6, v8

    if-lez v11, :cond_5

    .line 207
    :cond_4
    new-instance v11, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Invalid CFS entry offset: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " (resource: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 209
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/store/IndexInput;->readString()Ljava/lang/String;

    move-result-object v4

    .line 211
    .local v4, "id":Ljava/lang/String;
    if-eqz v10, :cond_6

    .line 214
    invoke-static {v4}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 217
    :cond_6
    if-eqz v2, :cond_7

    .line 219
    iget-wide v12, v2, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->offset:J

    sub-long v12, v6, v12

    iput-wide v12, v2, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->length:J

    .line 222
    :cond_7
    new-instance v2, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .end local v2    # "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    invoke-direct {v2}, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;-><init>()V

    .line 223
    .restart local v2    # "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    iput-wide v6, v2, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->offset:J

    .line 225
    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .line 226
    .local v5, "previous":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    if-eqz v5, :cond_8

    .line 227
    new-instance v11, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Duplicate cfs entry id="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in CFS: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 204
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 249
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->isOpen:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 260
    :goto_0
    monitor-exit p0

    return-void

    .line 253
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->isOpen:Z

    .line 254
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    if-eqz v0, :cond_2

    .line 255
    sget-boolean v0, Lorg/apache/lucene/store/CompoundFileDirectory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->openForWrite:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 256
    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/store/CompoundFileWriter;->close()V

    goto :goto_0

    .line 257
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 258
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    aput-object v2, v0, v1

    invoke-static {v0}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 331
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileDirectory;->ensureOpen()V

    .line 332
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/CompoundFileWriter;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v0

    return-object v0
.end method

.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileDirectory;->ensureOpen()V

    .line 351
    sget-boolean v2, Lorg/apache/lucene/store/CompoundFileDirectory;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->openForWrite:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 352
    :cond_0
    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    .local v1, "id":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .line 354
    .local v0, "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    if-nez v0, :cond_1

    .line 355
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No sub-file with id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " found (fileName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " files: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 357
    :cond_1
    new-instance v2, Lorg/apache/lucene/store/CompoundFileDirectory$1;

    invoke-direct {v2, p0, p0, v0}, Lorg/apache/lucene/store/CompoundFileDirectory$1;-><init>(Lorg/apache/lucene/store/CompoundFileDirectory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;)V

    return-object v2
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 306
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 295
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileDirectory;->ensureOpen()V

    .line 296
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/CompoundFileWriter;->fileExists(Ljava/lang/String;)Z

    move-result v0

    .line 299
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileDirectory;->ensureOpen()V

    .line 320
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    if-eqz v1, :cond_0

    .line 321
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/store/CompoundFileWriter;->fileLength(Ljava/lang/String;)J

    move-result-wide v2

    .line 326
    :goto_0
    return-wide v2

    .line 323
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .line 324
    .local v0, "e":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    if-nez v0, :cond_1

    .line 325
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 326
    :cond_1
    iget-wide v2, v0, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->length:J

    goto :goto_0
.end method

.method public getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 277
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileDirectory;->ensureOpen()V

    .line 279
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    if-eqz v3, :cond_1

    .line 280
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->writer:Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/store/CompoundFileWriter;->listAll()[Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "res":[Ljava/lang/String;
    :cond_0
    return-object v1

    .line 282
    .end local v1    # "res":[Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 284
    .restart local v1    # "res":[Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->fileName:Ljava/lang/String;

    invoke-static {v3}, Lorg/apache/lucene/index/IndexFileNames;->parseSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 285
    .local v2, "seg":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 286
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 344
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileDirectory;->ensureOpen()V

    .line 265
    sget-boolean v0, Lorg/apache/lucene/store/CompoundFileDirectory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->openForWrite:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 266
    :cond_0
    :try_start_1
    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 267
    .local v7, "id":Ljava/lang/String;
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;

    .line 268
    .local v6, "entry":Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;
    if-nez v6, :cond_1

    .line 269
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No sub-file with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found (fileName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " files: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->entries:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->handle:Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    iget-wide v2, v6, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->offset:J

    iget-wide v4, v6, Lorg/apache/lucene/store/CompoundFileDirectory$FileEntry;->length:J

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;->openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 312
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 337
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CompoundFileDirectory(file=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->fileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" in dir="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileDirectory;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
