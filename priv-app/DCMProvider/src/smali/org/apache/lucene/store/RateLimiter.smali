.class public abstract Lorg/apache/lucene/store/RateLimiter;
.super Ljava/lang/Object;
.source "RateLimiter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getMbPerSec()D
.end method

.method public abstract pause(J)J
.end method

.method public abstract setMbPerSec(D)V
.end method
