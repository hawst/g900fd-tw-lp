.class Lorg/apache/lucene/store/NIOFSDirectory$1;
.super Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.source "NIOFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/NIOFSDirectory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/NIOFSDirectory;

.field private final synthetic val$context:Lorg/apache/lucene/store/IOContext;

.field private final synthetic val$descriptor:Ljava/io/RandomAccessFile;

.field private final synthetic val$path:Ljava/io/File;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/NIOFSDirectory;Lorg/apache/lucene/store/Directory;Ljava/io/RandomAccessFile;Ljava/io/File;Lorg/apache/lucene/store/IOContext;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->this$0:Lorg/apache/lucene/store/NIOFSDirectory;

    iput-object p3, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    iput-object p4, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$path:Ljava/io/File;

    iput-object p5, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$context:Lorg/apache/lucene/store/IOContext;

    .line 89
    invoke-direct {p0, p2}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 94
    return-void
.end method

.method public openFullSlice()Lorg/apache/lucene/store/IndexInput;
    .locals 7

    .prologue
    .line 105
    :try_start_0
    const-string v1, "full-slice"

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/NIOFSDirectory$1;->openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 106
    :catch_0
    move-exception v6

    .line 107
    .local v6, "ex":Ljava/io/IOException;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    .locals 12
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "offset"    # J
    .param p4, "length"    # J

    .prologue
    .line 98
    new-instance v1, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;

    iget-object v3, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$path:Ljava/io/File;

    iget-object v4, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$descriptor:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 99
    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->val$context:Lorg/apache/lucene/store/IOContext;

    invoke-static {v0}, Lorg/apache/lucene/store/BufferedIndexInput;->bufferSize(Lorg/apache/lucene/store/IOContext;)I

    move-result v10

    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$1;->this$0:Lorg/apache/lucene/store/NIOFSDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/NIOFSDirectory;->getReadChunkSize()I

    move-result v11

    move-object v2, p1

    move-wide v6, p2

    move-wide/from16 v8, p4

    .line 98
    invoke-direct/range {v1 .. v11}, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;Ljava/io/RandomAccessFile;Ljava/nio/channels/FileChannel;JJII)V

    return-object v1
.end method
