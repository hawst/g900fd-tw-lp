.class public Lorg/apache/lucene/store/RAMOutputStream;
.super Lorg/apache/lucene/store/IndexOutput;
.source "RAMOutputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BUFFER_SIZE:I = 0x400


# instance fields
.field private bufferLength:I

.field private bufferPosition:I

.field private bufferStart:J

.field private currentBuffer:[B

.field private currentBufferIndex:I

.field private file:Lorg/apache/lucene/store/RAMFile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/RAMOutputStream;->$assertionsDisabled:Z

    .line 28
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMFile;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/RAMFile;)V
    .locals 1
    .param p1, "f"    # Lorg/apache/lucene/store/RAMFile;

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    .line 51
    return-void
.end method

.method private setFileLength()V
    .locals 6

    .prologue
    .line 162
    iget-wide v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    iget v4, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 163
    .local v0, "pointer":J
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v2, v2, Lorg/apache/lucene/store/RAMFile;->length:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 164
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v2, v0, v1}, Lorg/apache/lucene/store/RAMFile;->setLength(J)V

    .line 166
    :cond_0
    return-void
.end method

.method private final switchCurrentBuffer()V
    .locals 4

    .prologue
    .line 151
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    iget-object v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMFile;->numBuffers()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 152
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/RAMFile;->addBuffer(I)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    .line 156
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 157
    const-wide/16 v0, 0x400

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    .line 158
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    array-length v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    .line 159
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMOutputStream;->flush()V

    .line 103
    return-void
.end method

.method public flush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->setFileLength()V

    .line 171
    return-void
.end method

.method public getFilePointer()J
    .locals 4

    .prologue
    .line 175
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    if-gez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v0, v0, Lorg/apache/lucene/store/RAMFile;->length:J

    return-wide v0
.end method

.method public reset()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 94
    iput v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 95
    iput-wide v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    .line 96
    iput v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    .line 97
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/RAMFile;->setLength(J)V

    .line 98
    return-void
.end method

.method public seek(J)V
    .locals 7
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x400

    .line 109
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->setFileLength()V

    .line 110
    iget-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferStart:J

    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    .line 111
    :cond_0
    div-long v0, p1, v4

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 112
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 115
    :cond_1
    rem-long v0, p1, v4

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 116
    return-void
.end method

.method public sizeInBytes()J
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMFile;->numBuffers()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x400

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    if-ne v0, v1, :cond_0

    .line 126
    iget v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 127
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 129
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    iget v1, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    aput-byte p1, v0, v1

    .line 130
    return-void
.end method

.method public writeBytes([BII)V
    .locals 4
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    sget-boolean v2, Lorg/apache/lucene/store/RAMOutputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-nez p1, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 136
    :cond_0
    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    iget v3, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferLength:I

    if-ne v2, v3, :cond_1

    .line 137
    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBufferIndex:I

    .line 138
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMOutputStream;->switchCurrentBuffer()V

    .line 141
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    array-length v2, v2

    iget v3, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    sub-int v1, v2, v3

    .line 142
    .local v1, "remainInBuffer":I
    if-ge p3, v1, :cond_3

    move v0, p3

    .line 143
    .local v0, "bytesToCopy":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->currentBuffer:[B

    iget v3, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    invoke-static {p1, p2, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    add-int/2addr p2, v0

    .line 145
    sub-int/2addr p3, v0

    .line 146
    iget v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/apache/lucene/store/RAMOutputStream;->bufferPosition:I

    .line 135
    .end local v0    # "bytesToCopy":I
    .end local v1    # "remainInBuffer":I
    :cond_2
    if-gtz p3, :cond_0

    .line 148
    return-void

    .restart local v1    # "remainInBuffer":I
    :cond_3
    move v0, v1

    .line 142
    goto :goto_0
.end method

.method public writeTo(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 12
    .param p1, "out"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMOutputStream;->flush()V

    .line 56
    iget-object v5, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v2, v5, Lorg/apache/lucene/store/RAMFile;->length:J

    .line 57
    .local v2, "end":J
    const-wide/16 v8, 0x0

    .line 58
    .local v8, "pos":J
    const/4 v0, 0x0

    .local v0, "buffer":I
    move v1, v0

    .line 59
    .end local v0    # "buffer":I
    .local v1, "buffer":I
    :goto_0
    cmp-long v5, v8, v2

    if-ltz v5, :cond_0

    .line 68
    return-void

    .line 60
    :cond_0
    const/16 v4, 0x400

    .line 61
    .local v4, "length":I
    int-to-long v10, v4

    add-long v6, v8, v10

    .line 62
    .local v6, "nextPos":J
    cmp-long v5, v6, v2

    if-lez v5, :cond_1

    .line 63
    sub-long v10, v2, v8

    long-to-int v4, v10

    .line 65
    :cond_1
    iget-object v5, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "buffer":I
    .restart local v0    # "buffer":I
    invoke-virtual {v5, v1}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v5

    invoke-virtual {p1, v5, v4}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BI)V

    .line 66
    move-wide v8, v6

    move v1, v0

    .end local v0    # "buffer":I
    .restart local v1    # "buffer":I
    goto :goto_0
.end method

.method public writeTo([BI)V
    .locals 12
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMOutputStream;->flush()V

    .line 74
    iget-object v10, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    iget-wide v4, v10, Lorg/apache/lucene/store/RAMFile;->length:J

    .line 75
    .local v4, "end":J
    const-wide/16 v8, 0x0

    .line 76
    .local v8, "pos":J
    const/4 v0, 0x0

    .line 77
    .local v0, "buffer":I
    move v2, p2

    .local v2, "bytesUpto":I
    move v1, v0

    .line 78
    .end local v0    # "buffer":I
    .local v1, "buffer":I
    :goto_0
    cmp-long v10, v8, v4

    if-ltz v10, :cond_0

    .line 88
    return-void

    .line 79
    :cond_0
    const/16 v3, 0x400

    .line 80
    .local v3, "length":I
    int-to-long v10, v3

    add-long v6, v8, v10

    .line 81
    .local v6, "nextPos":J
    cmp-long v10, v6, v4

    if-lez v10, :cond_1

    .line 82
    sub-long v10, v4, v8

    long-to-int v3, v10

    .line 84
    :cond_1
    iget-object v10, p0, Lorg/apache/lucene/store/RAMOutputStream;->file:Lorg/apache/lucene/store/RAMFile;

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "buffer":I
    .restart local v0    # "buffer":I
    invoke-virtual {v10, v1}, Lorg/apache/lucene/store/RAMFile;->getBuffer(I)[B

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    add-int/2addr v2, v3

    .line 86
    move-wide v8, v6

    move v1, v0

    .end local v0    # "buffer":I
    .restart local v1    # "buffer":I
    goto :goto_0
.end method
