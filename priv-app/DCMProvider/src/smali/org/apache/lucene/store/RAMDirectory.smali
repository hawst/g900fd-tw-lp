.class public Lorg/apache/lucene/store/RAMDirectory;
.super Lorg/apache/lucene/store/Directory;
.source "RAMDirectory.java"


# instance fields
.field protected final fileMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/RAMFile;",
            ">;"
        }
    .end annotation
.end field

.field protected final sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 49
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    .line 59
    :try_start_0
    new-instance v0, Lorg/apache/lucene/store/SingleInstanceLockFactory;

    invoke-direct {v0}, Lorg/apache/lucene/store/SingleInstanceLockFactory;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/RAMDirectory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IOContext;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/lucene/store/RAMDirectory;-><init>(Lorg/apache/lucene/store/Directory;ZLorg/apache/lucene/store/IOContext;)V

    .line 93
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/store/Directory;ZLorg/apache/lucene/store/IOContext;)V
    .locals 4
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "closeDir"    # Z
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0}, Lorg/apache/lucene/store/RAMDirectory;-><init>()V

    .line 97
    invoke-virtual {p1}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_1

    .line 100
    if-eqz p2, :cond_0

    .line 101
    invoke-virtual {p1}, Lorg/apache/lucene/store/Directory;->close()V

    .line 103
    :cond_0
    return-void

    .line 97
    :cond_1
    aget-object v0, v2, v1

    .line 98
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {p1, p0, v0, v0, p3}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    .line 97
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/RAMDirectory;->isOpen:Z

    .line 202
    iget-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 203
    return-void
.end method

.method public createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 164
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->newRAMFile()Lorg/apache/lucene/store/RAMFile;

    move-result-object v1

    .line 165
    .local v1, "file":Lorg/apache/lucene/store/RAMFile;
    iget-object v2, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 166
    .local v0, "existing":Lorg/apache/lucene/store/RAMFile;
    if-eqz v0, :cond_0

    .line 167
    iget-object v2, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    iget-wide v4, v0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J

    neg-long v4, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 168
    const/4 v2, 0x0

    iput-object v2, v0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    .line 170
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    new-instance v2, Lorg/apache/lucene/store/RAMOutputStream;

    invoke-direct {v2, v1}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    return-object v2
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 151
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 152
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-eqz v0, :cond_0

    .line 153
    const/4 v1, 0x0

    iput-object v1, v0, Lorg/apache/lucene/store/RAMFile;->directory:Lorg/apache/lucene/store/RAMDirectory;

    .line 154
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    iget-wide v2, v0, Lorg/apache/lucene/store/RAMFile;->sizeInBytes:J

    neg-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 158
    return-void

    .line 156
    :cond_0
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 120
    iget-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final fileLength(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 129
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 130
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-nez v0, :cond_0

    .line 131
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 133
    :cond_0
    invoke-virtual {v0}, Lorg/apache/lucene/store/RAMFile;->getLength()J

    move-result-wide v2

    return-wide v2
.end method

.method public final listAll()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 110
    iget-object v3, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 111
    .local v0, "fileNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 112
    .local v2, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 113
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    return-object v3

    .line 112
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .local v1, "name":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected newRAMFile()Lorg/apache/lucene/store/RAMFile;
    .locals 1

    .prologue
    .line 180
    new-instance v0, Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v0, p0}, Lorg/apache/lucene/store/RAMFile;-><init>(Lorg/apache/lucene/store/RAMDirectory;)V

    return-object v0
.end method

.method public openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 191
    iget-object v1, p0, Lorg/apache/lucene/store/RAMDirectory;->fileMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/RAMFile;

    .line 192
    .local v0, "file":Lorg/apache/lucene/store/RAMFile;
    if-nez v0, :cond_0

    .line 193
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1, p1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_0
    new-instance v1, Lorg/apache/lucene/store/RAMInputStream;

    invoke-direct {v1, p1, v0}, Lorg/apache/lucene/store/RAMInputStream;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/RAMFile;)V

    return-object v1
.end method

.method public final sizeInBytes()J
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lorg/apache/lucene/store/RAMDirectory;->ensureOpen()V

    .line 142
    iget-object v0, p0, Lorg/apache/lucene/store/RAMDirectory;->sizeInBytes:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    return-void
.end method
