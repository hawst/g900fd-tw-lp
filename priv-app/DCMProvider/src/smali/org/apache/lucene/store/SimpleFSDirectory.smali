.class public Lorg/apache/lucene/store/SimpleFSDirectory;
.super Lorg/apache/lucene/store/FSDirectory;
.source "SimpleFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1, "path"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V
    .locals 0
    .param p1, "path"    # Ljava/io/File;
    .param p2, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/store/FSDirectory;-><init>(Ljava/io/File;Lorg/apache/lucene/store/LockFactory;)V

    .line 42
    return-void
.end method


# virtual methods
.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory;->ensureOpen()V

    .line 65
    new-instance v4, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory;->getDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {v4, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 66
    .local v4, "file":Ljava/io/File;
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v3, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 67
    .local v3, "descriptor":Ljava/io/RandomAccessFile;
    new-instance v0, Lorg/apache/lucene/store/SimpleFSDirectory$1;

    move-object v1, p0

    move-object v2, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/store/SimpleFSDirectory$1;-><init>(Lorg/apache/lucene/store/SimpleFSDirectory;Lorg/apache/lucene/store/Directory;Ljava/io/RandomAccessFile;Ljava/io/File;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method

.method public openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory;->ensureOpen()V

    .line 57
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lorg/apache/lucene/store/SimpleFSDirectory;->directory:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 58
    .local v0, "path":Ljava/io/File;
    new-instance v1, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SimpleFSIndexInput(path=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/store/SimpleFSDirectory;->getReadChunkSize()I

    move-result v3

    invoke-direct {v1, v2, v0, p2, v3}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V

    return-object v1
.end method
