.class public abstract Lorg/apache/lucene/store/DataInput;
.super Ljava/lang/Object;
.source "DataInput.java"

# interfaces
.implements Ljava/lang/Cloneable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->clone()Lorg/apache/lucene/store/DataInput;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/store/DataInput;
    .locals 3

    .prologue
    .line 206
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/DataInput;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/Error;

    const-string v2, "This cannot happen: Failing to clone DataInput"

    invoke-direct {v1, v2}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public abstract readByte()B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract readBytes([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public readBytes([BIIZ)V
    .locals 0
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .param p4, "useBuffer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 71
    return-void
.end method

.method public readInt()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 85
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    .line 84
    or-int/2addr v0, v1

    .line 85
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 84
    or-int/2addr v0, v1

    return v0
.end method

.method public readLong()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public readShort()S
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public readString()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 188
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readVInt()I

    move-result v1

    .line 189
    .local v1, "length":I
    new-array v0, v1, [B

    .line 190
    .local v0, "bytes":[B
    invoke-virtual {p0, v0, v4, v1}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 191
    new-instance v2, Ljava/lang/String;

    sget-object v3, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, v0, v4, v1, v3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v2
.end method

.method public readStringSet()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 230
    .local v2, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v0

    .line 231
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 235
    return-object v2

    .line 232
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public readStringStringMap()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 215
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 216
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readInt()I

    move-result v0

    .line 217
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 223
    return-object v3

    .line 218
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v2

    .line 219
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readString()Ljava/lang/String;

    move-result-object v4

    .line 220
    .local v4, "val":Ljava/lang/String;
    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public readVInt()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 109
    .local v0, "b":B
    if-ltz v0, :cond_1

    move v1, v0

    .line 123
    :cond_0
    return v1

    .line 110
    :cond_1
    and-int/lit8 v1, v0, 0x7f

    .line 111
    .local v1, "i":I
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 112
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x7

    or-int/2addr v1, v2

    .line 113
    if-gez v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 115
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v1, v2

    .line 116
    if-gez v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 118
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x15

    or-int/2addr v1, v2

    .line 119
    if-gez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 122
    and-int/lit8 v2, v0, 0xf

    shl-int/lit8 v2, v2, 0x1c

    or-int/2addr v1, v2

    .line 123
    and-int/lit16 v2, v0, 0xf0

    if-eqz v2, :cond_0

    .line 124
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Invalid vInt detected (too many bits)"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readVLong()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x7f

    .line 154
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 155
    .local v0, "b":B
    if-ltz v0, :cond_1

    int-to-long v2, v0

    .line 180
    :cond_0
    return-wide v2

    .line 156
    :cond_1
    int-to-long v4, v0

    and-long v2, v4, v6

    .line 157
    .local v2, "i":J
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 158
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/4 v1, 0x7

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 159
    if-gez v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 161
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0xe

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 162
    if-gez v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 164
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x15

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 165
    if-gez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 167
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x1c

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 168
    if-gez v0, :cond_0

    .line 169
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 170
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x23

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 171
    if-gez v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 173
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x2a

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 174
    if-gez v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 176
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x31

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 177
    if-gez v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lorg/apache/lucene/store/DataInput;->readByte()B

    move-result v0

    .line 179
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x38

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 180
    if-gez v0, :cond_0

    .line 181
    new-instance v1, Ljava/io/IOException;

    const-string v4, "Invalid vLong detected (negative values disallowed)"

    invoke-direct {v1, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
