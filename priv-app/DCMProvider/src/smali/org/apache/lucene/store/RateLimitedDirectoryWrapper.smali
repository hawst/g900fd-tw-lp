.class public final Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;
.super Lorg/apache/lucene/store/Directory;
.source "RateLimitedDirectoryWrapper.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private volatile contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

.field private final delegate:Lorg/apache/lucene/store/Directory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p1, "wrapped"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/store/Directory;-><init>()V

    .line 37
    invoke-static {}, Lorg/apache/lucene/store/IOContext$Context;->values()[Lorg/apache/lucene/store/IOContext$Context;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lorg/apache/lucene/store/RateLimiter;

    iput-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    .line 40
    iput-object p1, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    .line 41
    return-void
.end method

.method private getRateLimiter(Lorg/apache/lucene/store/IOContext$Context;)Lorg/apache/lucene/store/RateLimiter;
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext$Context;

    .prologue
    .line 151
    sget-boolean v0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 152
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    invoke-virtual {p1}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 118
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->clearLock(Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->isOpen:Z

    .line 99
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->close()V

    .line 100
    return-void
.end method

.method public copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 1
    .param p1, "to"    # Lorg/apache/lucene/store/Directory;
    .param p2, "src"    # Ljava/lang/String;
    .param p3, "dest"    # Ljava/lang/String;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 147
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/apache/lucene/store/Directory;->copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    .line 148
    return-void
.end method

.method public createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 75
    iget-object v2, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, p1, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    .line 76
    .local v1, "output":Lorg/apache/lucene/store/IndexOutput;
    iget-object v2, p2, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    invoke-direct {p0, v2}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->getRateLimiter(Lorg/apache/lucene/store/IOContext$Context;)Lorg/apache/lucene/store/RateLimiter;

    move-result-object v0

    .line 77
    .local v0, "limiter":Lorg/apache/lucene/store/RateLimiter;
    if-eqz v0, :cond_0

    .line 78
    new-instance v2, Lorg/apache/lucene/store/RateLimitedIndexOutput;

    invoke-direct {v2, v0, v1}, Lorg/apache/lucene/store/RateLimitedIndexOutput;-><init>(Lorg/apache/lucene/store/RateLimiter;Lorg/apache/lucene/store/IndexOutput;)V

    move-object v1, v2

    .line 80
    .end local v1    # "output":Lorg/apache/lucene/store/IndexOutput;
    :cond_0
    return-object v1
.end method

.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 106
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;

    move-result-object v0

    return-object v0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 62
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public fileLength(Ljava/lang/String;)J
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDelegate()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method public getLockFactory()Lorg/apache/lucene/store/LockFactory;
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 130
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v0

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 136
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->getLockID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxWriteMBPerSec(Lorg/apache/lucene/store/IOContext$Context;)Ljava/lang/Double;
    .locals 4
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext$Context;

    .prologue
    .line 228
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 229
    if-nez p1, :cond_0

    .line 230
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Context must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 232
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->getRateLimiter(Lorg/apache/lucene/store/IOContext$Context;)Lorg/apache/lucene/store/RateLimiter;

    move-result-object v0

    .line 233
    .local v0, "limiter":Lorg/apache/lucene/store/RateLimiter;
    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/store/RateLimiter;->getMbPerSec()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    goto :goto_0
.end method

.method public listAll()[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 50
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 112
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    return-object v0
.end method

.method public openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    return-object v0
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->setLockFactory(Lorg/apache/lucene/store/LockFactory;)V

    .line 125
    return-void
.end method

.method public setMaxWriteMBPerSec(Ljava/lang/Double;Lorg/apache/lucene/store/IOContext$Context;)V
    .locals 6
    .param p1, "mbPerSec"    # Ljava/lang/Double;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext$Context;

    .prologue
    .line 175
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 176
    if-nez p2, :cond_0

    .line 177
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Context must not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 179
    :cond_0
    invoke-virtual {p2}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    .line 180
    .local v1, "ord":I
    iget-object v2, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    aget-object v0, v2, v1

    .line 181
    .local v0, "limiter":Lorg/apache/lucene/store/RateLimiter;
    if-nez p1, :cond_2

    .line 182
    if-eqz v0, :cond_1

    .line 183
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/RateLimiter;->setMbPerSec(D)V

    .line 184
    iget-object v2, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 192
    :cond_1
    :goto_0
    return-void

    .line 186
    :cond_2
    if-eqz v0, :cond_3

    .line 187
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/RateLimiter;->setMbPerSec(D)V

    .line 188
    iget-object v2, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    aput-object v0, v2, v1

    goto :goto_0

    .line 190
    :cond_3
    iget-object v2, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    new-instance v3, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lorg/apache/lucene/store/RateLimiter$SimpleRateLimiter;-><init>(D)V

    aput-object v3, v2, v1

    goto :goto_0
.end method

.method public setRateLimiter(Lorg/apache/lucene/store/RateLimiter;Lorg/apache/lucene/store/IOContext$Context;)V
    .locals 2
    .param p1, "mergeWriteRateLimiter"    # Lorg/apache/lucene/store/RateLimiter;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext$Context;

    .prologue
    .line 212
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 213
    if-nez p2, :cond_0

    .line 214
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->contextRateLimiters:[Lorg/apache/lucene/store/RateLimiter;

    invoke-virtual {p2}, Lorg/apache/lucene/store/IOContext$Context;->ordinal()I

    move-result v1

    aput-object p1, v0, v1

    .line 217
    return-void
.end method

.method public sync(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "names":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->ensureOpen()V

    .line 86
    iget-object v0, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 87
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RateLimitedDirectoryWrapper("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/store/RateLimitedDirectoryWrapper;->delegate:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/store/Directory;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
