.class final Lorg/apache/lucene/store/CompoundFileWriter;
.super Ljava/lang/Object;
.source "CompoundFileWriter.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;,
        Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final DATA_CODEC:Ljava/lang/String; = "CompoundFileWriterData"

.field static final ENTRY_CODEC:Ljava/lang/String; = "CompoundFileWriterEntries"

.field static final FORMAT_NO_SEGMENT_PREFIX:I = -0x1

.field static final FORMAT_PRE_VERSION:I

.field static final VERSION_CURRENT:I

.field static final VERSION_START:I


# instance fields
.field private closed:Z

.field final dataFileName:Ljava/lang/String;

.field private dataOut:Lorg/apache/lucene/store/IndexOutput;

.field private final directory:Lorg/apache/lucene/store/Directory;

.field private final entries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field final entryTableName:Ljava/lang/String;

.field private final outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final pendingEntries:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final seenIDs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lorg/apache/lucene/store/CompoundFileWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    .line 66
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 3
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->seenIDs:Ljava/util/Set;

    .line 72
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->pendingEntries:Ljava/util/Queue;

    .line 73
    iput-boolean v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->closed:Z

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 87
    if-nez p1, :cond_0

    .line 88
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "directory cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    if-nez p2, :cond_1

    .line 90
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "name cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 93
    invoke-static {p2}, Lorg/apache/lucene/index/IndexFileNames;->stripExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    .line 94
    const-string v2, "cfe"

    .line 92
    invoke-static {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entryTableName:Ljava/lang/String;

    .line 95
    iput-object p2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataFileName:Ljava/lang/String;

    .line 97
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/store/CompoundFileWriter;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->pendingEntries:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/store/CompoundFileWriter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 257
    invoke-direct {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->prunePendingEntries()V

    return-void
.end method

.method private final copyFileEntry(Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;)J
    .locals 13
    .param p1, "dataOut"    # Lorg/apache/lucene/store/IndexOutput;
    .param p2, "fileEntry"    # Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v10, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    sget-object v12, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v10, v11, v12}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v4

    .line 177
    .local v4, "is":Lorg/apache/lucene/store/IndexInput;
    const/4 v5, 0x0

    .line 179
    .local v5, "success":Z
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v8

    .line 180
    .local v8, "startPtr":J
    iget-wide v6, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->length:J

    .line 181
    .local v6, "length":J
    invoke-virtual {p1, v4, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V

    .line 183
    invoke-virtual {p1}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v2

    .line 184
    .local v2, "endPtr":J
    sub-long v0, v2, v8

    .line 185
    .local v0, "diff":J
    cmp-long v10, v0, v6

    if-eqz v10, :cond_0

    .line 186
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Difference in the output file offsets "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 187
    const-string v12, " does not match the original file length "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 186
    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    .end local v0    # "diff":J
    .end local v2    # "endPtr":J
    .end local v6    # "length":J
    .end local v8    # "startPtr":J
    :catchall_0
    move-exception v10

    .line 192
    if-eqz v5, :cond_2

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 193
    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 195
    iget-object v11, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v12, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 199
    :goto_0
    throw v10

    .line 188
    .restart local v0    # "diff":J
    .restart local v2    # "endPtr":J
    .restart local v6    # "length":J
    .restart local v8    # "startPtr":J
    :cond_0
    :try_start_1
    iput-wide v8, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->offset:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    const/4 v5, 0x1

    .line 192
    if-eqz v5, :cond_1

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 193
    aput-object v4, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 195
    iget-object v10, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v11, p2, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V

    .line 190
    :goto_1
    return-wide v6

    .line 196
    :cond_1
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/io/Closeable;

    const/4 v11, 0x0

    .line 197
    aput-object v4, v10, v11

    invoke-static {v10}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1

    .line 196
    .end local v0    # "diff":J
    .end local v2    # "endPtr":J
    .end local v6    # "length":J
    .end local v8    # "startPtr":J
    :cond_2
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/io/Closeable;

    const/4 v12, 0x0

    .line 197
    aput-object v4, v11, v12

    invoke-static {v11}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_0
.end method

.method private final ensureOpen()V
    .locals 2

    .prologue
    .line 165
    iget-boolean v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->closed:Z

    if-eqz v0, :cond_0

    .line 166
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "CFS Directory is already closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    return-void
.end method

.method private declared-synchronized getOutput()Lorg/apache/lucene/store/IndexOutput;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 103
    .local v0, "success":Z
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataFileName:Ljava/lang/String;

    sget-object v3, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    .line 104
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    const-string v2, "CompoundFileWriterData"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    const/4 v0, 0x1

    .line 107
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :try_start_2
    new-array v1, v1, [Ljava/io/Closeable;

    const/4 v2, 0x0

    .line 108
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v3, v1, v2

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 112
    .end local v0    # "success":Z
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object v1

    .line 106
    .restart local v0    # "success":Z
    :catchall_0
    move-exception v1

    .line 107
    if-nez v0, :cond_1

    const/4 v2, 0x1

    :try_start_3
    new-array v2, v2, [Ljava/io/Closeable;

    const/4 v3, 0x0

    .line 108
    iget-object v4, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v4, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 110
    :cond_1
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 100
    .end local v0    # "success":Z
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private final prunePendingEntries()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 259
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 261
    :goto_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->pendingEntries:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    .line 268
    .local v0, "compareAndSet":Z
    sget-boolean v2, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 262
    .end local v0    # "compareAndSet":Z
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->pendingEntries:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    .line 263
    .local v1, "entry":Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    invoke-direct {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->getOutput()Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lorg/apache/lucene/store/CompoundFileWriter;->copyFileEntry(Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;)J

    .line 264
    iget-object v2, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    iget-object v3, v1, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 266
    .end local v1    # "entry":Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    :catchall_0
    move-exception v2

    .line 267
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    .line 268
    .restart local v0    # "compareAndSet":Z
    sget-boolean v3, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 269
    :cond_1
    throw v2

    .line 271
    .end local v0    # "compareAndSet":Z
    :cond_2
    return-void
.end method


# virtual methods
.method public close()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 134
    iget-boolean v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->closed:Z

    if-eqz v3, :cond_0

    .line 162
    :goto_0
    return-void

    .line 137
    :cond_0
    const/4 v2, 0x0

    .line 138
    .local v2, "priorException":Ljava/io/IOException;
    const/4 v1, 0x0

    .line 142
    .local v1, "entryTableOut":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->pendingEntries:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 143
    :cond_1
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "CFS has pending open files"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/io/IOException;
    move-object v2, v0

    .line 152
    new-array v3, v6, [Ljava/io/Closeable;

    iget-object v4, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 155
    .end local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    iget-object v4, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entryTableName:Ljava/lang/String;

    sget-object v5, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    .line 156
    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {p0, v3, v1}, Lorg/apache/lucene/store/CompoundFileWriter;->writeEntryTable(Ljava/util/Collection;Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 160
    new-array v3, v6, [Ljava/io/Closeable;

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    goto :goto_0

    .line 145
    :cond_2
    const/4 v3, 0x1

    :try_start_2
    iput-boolean v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->closed:Z

    .line 147
    invoke-direct {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->getOutput()Lorg/apache/lucene/store/IndexOutput;

    .line 148
    sget-boolean v3, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151
    :catchall_0
    move-exception v3

    .line 152
    new-array v4, v6, [Ljava/io/Closeable;

    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v5, v4, v7

    invoke-static {v2, v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 153
    throw v3

    .line 152
    :cond_3
    new-array v3, v6, [Ljava/io/Closeable;

    iget-object v4, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataOut:Lorg/apache/lucene/store/IndexOutput;

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    goto :goto_1

    .line 157
    :catch_1
    move-exception v0

    .line 158
    .restart local v0    # "e":Ljava/io/IOException;
    move-object v2, v0

    .line 160
    new-array v3, v6, [Ljava/io/Closeable;

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    goto :goto_0

    .line 159
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v3

    .line 160
    new-array v4, v6, [Ljava/io/Closeable;

    aput-object v1, v4, v7

    invoke-static {v2, v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 161
    throw v3
.end method

.method createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    invoke-direct {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->ensureOpen()V

    .line 215
    const/4 v4, 0x0

    .line 216
    .local v4, "success":Z
    const/4 v3, 0x0

    .line 218
    .local v3, "outputLocked":Z
    :try_start_0
    sget-boolean v5, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-nez p1, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    const-string v6, "name must not be null"

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    :catchall_0
    move-exception v5

    .line 243
    if-nez v4, :cond_8

    .line 244
    iget-object v6, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    if-eqz v3, :cond_8

    .line 246
    sget-boolean v6, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_7

    iget-object v6, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v6

    if-nez v6, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 219
    :cond_0
    :try_start_1
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 220
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "File "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " already exists"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 222
    :cond_1
    new-instance v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    const/4 v5, 0x0

    invoke-direct {v0, v5}, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;-><init>(Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;)V

    .line 223
    .local v0, "entry":Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    iput-object p1, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    .line 224
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v5, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    invoke-static {p1}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 226
    .local v1, "id":Ljava/lang/String;
    sget-boolean v5, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->seenIDs:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "file=\""

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\" maps to id=\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\", which was already written"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 227
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->seenIDs:Ljava/util/Set;

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 231
    new-instance v2, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;

    invoke-direct {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->getOutput()Lorg/apache/lucene/store/IndexOutput;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v2, p0, v5, v0, v6}, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;-><init>(Lorg/apache/lucene/store/CompoundFileWriter;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    .local v2, "out":Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;
    :goto_0
    const/4 v4, 0x1

    .line 243
    if-nez v4, :cond_6

    .line 244
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    if-eqz v3, :cond_6

    .line 246
    sget-boolean v5, Lorg/apache/lucene/store/CompoundFileWriter;->$assertionsDisabled:Z

    if-nez v5, :cond_5

    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-nez v5, :cond_5

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 233
    .end local v2    # "out":Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;
    :cond_3
    :try_start_2
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    iput-object v5, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->dir:Lorg/apache/lucene/store/Directory;

    .line 234
    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, p1}, Lorg/apache/lucene/store/Directory;->fileExists(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 235
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "File "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " already exists"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 237
    :cond_4
    new-instance v2, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;

    iget-object v5, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v5

    .line 238
    const/4 v6, 0x1

    .line 237
    invoke-direct {v2, p0, v5, v0, v6}, Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;-><init>(Lorg/apache/lucene/store/CompoundFileWriter;Lorg/apache/lucene/store/IndexOutput;Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v2    # "out":Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;
    goto :goto_0

    .line 247
    :cond_5
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->releaseOutputLock()V

    .line 241
    :cond_6
    return-object v2

    .line 247
    .end local v0    # "entry":Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "out":Lorg/apache/lucene/store/CompoundFileWriter$DirectCFSIndexOutput;
    :cond_7
    invoke-virtual {p0}, Lorg/apache/lucene/store/CompoundFileWriter;->releaseOutputLock()V

    .line 250
    :cond_8
    throw v5
.end method

.method fileExists(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 282
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method fileLength(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    iget-object v1, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    .line 275
    .local v0, "fileEntry":Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    if-nez v0, :cond_0

    .line 276
    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " does not exist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 278
    :cond_0
    iget-wide v2, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->length:J

    return-wide v2
.end method

.method getDirectory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->dataFileName:Ljava/lang/String;

    return-object v0
.end method

.method listAll()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->entries:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method final releaseOutputLock()V
    .locals 3

    .prologue
    .line 254
    iget-object v0, p0, Lorg/apache/lucene/store/CompoundFileWriter;->outputTaken:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 255
    return-void
.end method

.method protected writeEntryTable(Ljava/util/Collection;Lorg/apache/lucene/store/IndexOutput;)V
    .locals 4
    .param p2, "entryOut"    # Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;",
            ">;",
            "Lorg/apache/lucene/store/IndexOutput;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "entries":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;>;"
    const-string v1, "CompoundFileWriterEntries"

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 205
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p2, v1}, Lorg/apache/lucene/store/IndexOutput;->writeVInt(I)V

    .line 206
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 211
    return-void

    .line 206
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;

    .line 207
    .local v0, "fe":Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;
    iget-object v2, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->file:Ljava/lang/String;

    invoke-static {v2}, Lorg/apache/lucene/index/IndexFileNames;->stripSegmentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 208
    iget-wide v2, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->offset:J

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 209
    iget-wide v2, v0, Lorg/apache/lucene/store/CompoundFileWriter$FileEntry;->length:J

    invoke-virtual {p2, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    goto :goto_0
.end method
