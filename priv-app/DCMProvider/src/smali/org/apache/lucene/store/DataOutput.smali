.class public abstract Lorg/apache/lucene/store/DataOutput;
.super Ljava/lang/Object;
.source "DataOutput.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static COPY_BUFFER_SIZE:I


# instance fields
.field private copyBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/store/DataOutput;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/DataOutput;->$assertionsDisabled:Z

    .line 239
    const/16 v0, 0x4000

    sput v0, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public copyBytes(Lorg/apache/lucene/store/DataInput;J)V
    .locals 10
    .param p1, "input"    # Lorg/apache/lucene/store/DataInput;
    .param p2, "numBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 244
    sget-boolean v3, Lorg/apache/lucene/store/DataOutput;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    cmp-long v3, p2, v8

    if-gez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "numBytes="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 245
    :cond_0
    move-wide v0, p2

    .line 246
    .local v0, "left":J
    iget-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    if-nez v3, :cond_1

    .line 247
    sget v3, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    .line 248
    :cond_1
    :goto_0
    cmp-long v3, v0, v8

    if-gtz v3, :cond_2

    .line 258
    return-void

    .line 250
    :cond_2
    sget v3, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    int-to-long v4, v3

    cmp-long v3, v0, v4

    if-lez v3, :cond_3

    .line 251
    sget v2, Lorg/apache/lucene/store/DataOutput;->COPY_BUFFER_SIZE:I

    .line 254
    .local v2, "toCopy":I
    :goto_1
    iget-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    invoke-virtual {p1, v3, v6, v2}, Lorg/apache/lucene/store/DataInput;->readBytes([BII)V

    .line 255
    iget-object v3, p0, Lorg/apache/lucene/store/DataOutput;->copyBuffer:[B

    invoke-virtual {p0, v3, v6, v2}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 256
    int-to-long v4, v2

    sub-long/2addr v0, v4

    goto :goto_0

    .line 253
    .end local v2    # "toCopy":I
    :cond_3
    long-to-int v2, v0

    .restart local v2    # "toCopy":I
    goto :goto_1
.end method

.method public abstract writeByte(B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public writeBytes([BI)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 53
    return-void
.end method

.method public abstract writeBytes([BII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public writeInt(I)V
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    shr-int/lit8 v0, p1, 0x18

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 71
    shr-int/lit8 v0, p1, 0x10

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 72
    shr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 73
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 74
    return-void
.end method

.method public writeLong(J)V
    .locals 3
    .param p1, "i"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    const/16 v0, 0x20

    shr-long v0, p1, v0

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 206
    long-to-int v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 207
    return-void
.end method

.method public writeShort(S)V
    .locals 1
    .param p1, "i"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    shr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 81
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 82
    return-void
.end method

.method public writeString(Ljava/lang/String;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 233
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    .line 234
    .local v0, "utf8Result":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {p1, v3, v1, v0}, Lorg/apache/lucene/util/UnicodeUtil;->UTF16toUTF8(Ljava/lang/CharSequence;IILorg/apache/lucene/util/BytesRef;)V

    .line 235
    iget v1, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeVInt(I)V

    .line 236
    iget-object v1, v0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {p0, v1, v3, v2}, Lorg/apache/lucene/store/DataOutput;->writeBytes([BII)V

    .line 237
    return-void
.end method

.method public writeStringSet(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 292
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 299
    :cond_0
    return-void

    .line 294
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 295
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 296
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeStringStringMap(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_1

    .line 271
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 279
    :cond_0
    return-void

    .line 273
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeInt(I)V

    .line 274
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 275
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    .line 276
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/store/DataOutput;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final writeVInt(I)V
    .locals 1
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 195
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 196
    return-void

    .line 192
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 193
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method public final writeVLong(J)V
    .locals 7
    .param p1, "i"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 217
    sget-boolean v0, Lorg/apache/lucene/store/DataOutput;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    cmp-long v0, p1, v4

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 219
    :cond_0
    const-wide/16 v0, 0x7f

    and-long/2addr v0, p1

    const-wide/16 v2, 0x80

    or-long/2addr v0, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 220
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    .line 218
    :cond_1
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 222
    long-to-int v0, p1

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/DataOutput;->writeByte(B)V

    .line 223
    return-void
.end method
