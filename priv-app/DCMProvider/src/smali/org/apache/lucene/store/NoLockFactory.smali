.class public Lorg/apache/lucene/store/NoLockFactory;
.super Lorg/apache/lucene/store/LockFactory;
.source "NoLockFactory.java"


# static fields
.field private static singleton:Lorg/apache/lucene/store/NoLockFactory;

.field private static singletonLock:Lorg/apache/lucene/store/NoLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lorg/apache/lucene/store/NoLock;

    invoke-direct {v0}, Lorg/apache/lucene/store/NoLock;-><init>()V

    sput-object v0, Lorg/apache/lucene/store/NoLockFactory;->singletonLock:Lorg/apache/lucene/store/NoLock;

    .line 34
    new-instance v0, Lorg/apache/lucene/store/NoLockFactory;

    invoke-direct {v0}, Lorg/apache/lucene/store/NoLockFactory;-><init>()V

    sput-object v0, Lorg/apache/lucene/store/NoLockFactory;->singleton:Lorg/apache/lucene/store/NoLockFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lorg/apache/lucene/store/LockFactory;-><init>()V

    return-void
.end method

.method public static getNoLockFactory()Lorg/apache/lucene/store/NoLockFactory;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lorg/apache/lucene/store/NoLockFactory;->singleton:Lorg/apache/lucene/store/NoLockFactory;

    return-object v0
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 0
    .param p1, "lockName"    # Ljava/lang/String;

    .prologue
    .line 48
    return-void
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "lockName"    # Ljava/lang/String;

    .prologue
    .line 44
    sget-object v0, Lorg/apache/lucene/store/NoLockFactory;->singletonLock:Lorg/apache/lucene/store/NoLock;

    return-object v0
.end method
