.class public abstract Lorg/apache/lucene/store/Directory;
.super Ljava/lang/Object;
.source "Directory.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/Directory$IndexInputSlicer;,
        Lorg/apache/lucene/store/Directory$SlicedIndexInput;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected volatile isOpen:Z

.field protected lockFactory:Lorg/apache/lucene/store/LockFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/Directory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/store/Directory;->isOpen:Z

    .line 44
    return-void
.end method


# virtual methods
.method public clearLock(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/LockFactory;->clearLock(Ljava/lang/String;)V

    .line 128
    :cond_0
    return-void
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public copy(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V
    .locals 8
    .param p1, "to"    # Lorg/apache/lucene/store/Directory;
    .param p2, "src"    # Ljava/lang/String;
    .param p3, "dest"    # Ljava/lang/String;
    .param p4, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    const/4 v2, 0x0

    .line 196
    .local v2, "os":Lorg/apache/lucene/store/IndexOutput;
    const/4 v1, 0x0

    .line 197
    .local v1, "is":Lorg/apache/lucene/store/IndexInput;
    const/4 v3, 0x0

    .line 199
    .local v3, "priorException":Ljava/io/IOException;
    :try_start_0
    invoke-virtual {p1, p3, p4}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v2

    .line 200
    invoke-virtual {p0, p2, p4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v6

    invoke-virtual {v2, v1, v6, v7}, Lorg/apache/lucene/store/IndexOutput;->copyBytes(Lorg/apache/lucene/store/DataInput;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 205
    const/4 v4, 0x0

    .line 207
    .local v4, "success":Z
    const/4 v5, 0x2

    :try_start_1
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v3, v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 208
    const/4 v4, 0x1

    .line 210
    if-nez v4, :cond_0

    .line 212
    :try_start_2
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_6

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 202
    .end local v4    # "success":Z
    :catch_0
    move-exception v0

    .line 203
    .local v0, "ioe":Ljava/io/IOException;
    move-object v3, v0

    .line 205
    const/4 v4, 0x0

    .line 207
    .restart local v4    # "success":Z
    const/4 v5, 0x2

    :try_start_3
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v3, v5}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 208
    const/4 v4, 0x1

    .line 210
    if-nez v4, :cond_0

    .line 212
    :try_start_4
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 213
    :catch_1
    move-exception v5

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v5

    .line 210
    if-nez v4, :cond_1

    .line 212
    :try_start_5
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    .line 216
    :cond_1
    :goto_1
    throw v5

    .line 204
    .end local v0    # "ioe":Ljava/io/IOException;
    .end local v4    # "success":Z
    :catchall_1
    move-exception v5

    .line 205
    const/4 v4, 0x0

    .line 207
    .restart local v4    # "success":Z
    const/4 v6, 0x2

    :try_start_6
    new-array v6, v6, [Ljava/io/Closeable;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    invoke-static {v3, v6}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 208
    const/4 v4, 0x1

    .line 210
    if-nez v4, :cond_2

    .line 212
    :try_start_7
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4

    .line 217
    :cond_2
    :goto_2
    throw v5

    .line 209
    :catchall_2
    move-exception v5

    .line 210
    if-nez v4, :cond_3

    .line 212
    :try_start_8
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    .line 216
    :cond_3
    :goto_3
    throw v5

    .line 209
    :catchall_3
    move-exception v5

    .line 210
    if-nez v4, :cond_4

    .line 212
    :try_start_9
    invoke-virtual {p1, p3}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    .line 216
    :cond_4
    :goto_4
    throw v5

    .line 213
    .restart local v0    # "ioe":Ljava/io/IOException;
    :catch_2
    move-exception v6

    goto :goto_1

    .end local v0    # "ioe":Ljava/io/IOException;
    :catch_3
    move-exception v6

    goto :goto_3

    :catch_4
    move-exception v6

    goto :goto_2

    :catch_5
    move-exception v6

    goto :goto_4

    :catch_6
    move-exception v5

    goto :goto_0
.end method

.method public abstract createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->ensureOpen()V

    .line 234
    new-instance v0, Lorg/apache/lucene/store/Directory$1;

    invoke-direct {v0, p0, p0, p1, p2}, Lorg/apache/lucene/store/Directory$1;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)V

    return-object v0
.end method

.method public abstract deleteFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 255
    iget-boolean v0, p0, Lorg/apache/lucene/store/Directory;->isOpen:Z

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this Directory is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    return-void
.end method

.method public abstract fileExists(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract fileLength(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getLockFactory()Lorg/apache/lucene/store/LockFactory;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    return-object v0
.end method

.method public getLockID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract listAll()[Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 116
    iget-object v0, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/LockFactory;->makeLock(Ljava/lang/String;)Lorg/apache/lucene/store/Lock;

    move-result-object v0

    return-object v0
.end method

.method public abstract openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setLockFactory(Lorg/apache/lucene/store/LockFactory;)V
    .locals 1
    .param p1, "lockFactory"    # Lorg/apache/lucene/store/LockFactory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    sget-boolean v0, Lorg/apache/lucene/store/Directory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 146
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/store/Directory;->lockFactory:Lorg/apache/lucene/store/LockFactory;

    .line 147
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->getLockID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/LockFactory;->setLockPrefix(Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public abstract sync(Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " lockFactory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->getLockFactory()Lorg/apache/lucene/store/LockFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
