.class public Lorg/apache/lucene/store/ChecksumIndexOutput;
.super Lorg/apache/lucene/store/IndexOutput;
.source "ChecksumIndexOutput.java"


# instance fields
.field digest:Ljava/util/zip/Checksum;

.field main:Lorg/apache/lucene/store/IndexOutput;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/IndexOutput;)V
    .locals 1
    .param p1, "main"    # Lorg/apache/lucene/store/IndexOutput;

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/apache/lucene/store/IndexOutput;-><init>()V

    .line 34
    iput-object p1, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    .line 35
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->digest:Ljava/util/zip/Checksum;

    .line 36
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 62
    return-void
.end method

.method public finishCommit()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {p0}, Lorg/apache/lucene/store/ChecksumIndexOutput;->getChecksum()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 77
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->flush()V

    .line 57
    return-void
.end method

.method public getChecksum()J
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->digest:Ljava/util/zip/Checksum;

    invoke-interface {v0}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFilePointer()J
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->getFilePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public length()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexOutput;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public seek(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 71
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public writeByte(B)V
    .locals 1
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->digest:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1}, Ljava/util/zip/Checksum;->update(I)V

    .line 41
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 42
    return-void
.end method

.method public writeBytes([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->digest:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/zip/Checksum;->update([BII)V

    .line 47
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexOutput;->main:Lorg/apache/lucene/store/IndexOutput;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/store/IndexOutput;->writeBytes([BII)V

    .line 48
    return-void
.end method
