.class public abstract Lorg/apache/lucene/store/Lock$With;
.super Ljava/lang/Object;
.source "Lock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/Lock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "With"
.end annotation


# instance fields
.field private lock:Lorg/apache/lucene/store/Lock;

.field private lockWaitTimeout:J


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/Lock;J)V
    .locals 0
    .param p1, "lock"    # Lorg/apache/lucene/store/Lock;
    .param p2, "lockWaitTimeout"    # J

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lorg/apache/lucene/store/Lock$With;->lock:Lorg/apache/lucene/store/Lock;

    .line 117
    iput-wide p2, p0, Lorg/apache/lucene/store/Lock$With;->lockWaitTimeout:J

    .line 118
    return-void
.end method


# virtual methods
.method protected abstract doBody()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public run()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 134
    .local v0, "locked":Z
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/Lock$With;->lock:Lorg/apache/lucene/store/Lock;

    iget-wide v2, p0, Lorg/apache/lucene/store/Lock$With;->lockWaitTimeout:J

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/store/Lock;->obtain(J)Z

    move-result v0

    .line 135
    invoke-virtual {p0}, Lorg/apache/lucene/store/Lock$With;->doBody()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 137
    if-eqz v0, :cond_0

    .line 138
    iget-object v2, p0, Lorg/apache/lucene/store/Lock$With;->lock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v2}, Lorg/apache/lucene/store/Lock;->release()V

    .line 135
    :cond_0
    return-object v1

    .line 136
    :catchall_0
    move-exception v1

    .line 137
    if-eqz v0, :cond_1

    .line 138
    iget-object v2, p0, Lorg/apache/lucene/store/Lock$With;->lock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v2}, Lorg/apache/lucene/store/Lock;->release()V

    .line 139
    :cond_1
    throw v1
.end method
