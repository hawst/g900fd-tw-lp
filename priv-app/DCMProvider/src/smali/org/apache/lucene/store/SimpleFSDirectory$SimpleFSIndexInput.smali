.class public Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;
.super Lorg/apache/lucene/store/FSDirectory$FSIndexInput;
.source "SimpleFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/SimpleFSDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "SimpleFSIndexInput"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V
    .locals 0
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/io/File;
    .param p3, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p4, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V

    .line 99
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/RandomAccessFile;JJII)V
    .locals 1
    .param p1, "resourceDesc"    # Ljava/lang/String;
    .param p2, "file"    # Ljava/io/RandomAccessFile;
    .param p3, "off"    # J
    .param p5, "length"    # J
    .param p7, "bufferSize"    # I
    .param p8, "chunkSize"    # I

    .prologue
    .line 102
    invoke-direct/range {p0 .. p8}, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;-><init>(Ljava/lang/String;Ljava/io/RandomAccessFile;JJII)V

    .line 103
    return-void
.end method


# virtual methods
.method protected readInternal([BII)V
    .locals 16
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Ljava/io/RandomAccessFile;

    monitor-enter v11

    .line 110
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->off:J

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->getFilePointer()J

    move-result-wide v14

    add-long v6, v12, v14

    .line 111
    .local v6, "position":J
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v10, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 112
    const/4 v9, 0x0

    .line 114
    .local v9, "total":I
    move/from16 v0, p3

    int-to-long v12, v0

    add-long/2addr v12, v6

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->end:J

    cmp-long v10, v12, v14

    if-lez v10, :cond_0

    .line 115
    new-instance v10, Ljava/io/EOFException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "read past EOF: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 109
    .end local v6    # "position":J
    .end local v9    # "total":I
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 121
    .restart local v6    # "position":J
    .restart local v9    # "total":I
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget v10, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I

    add-int/2addr v10, v9

    move/from16 v0, p3

    if-le v10, v0, :cond_1

    .line 122
    sub-int v8, p3, v9

    .line 127
    .local v8, "readLength":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->file:Ljava/io/RandomAccessFile;

    add-int v12, p2, v9

    move-object/from16 v0, p1

    invoke-virtual {v10, v0, v12, v8}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    .line 128
    .local v3, "i":I
    add-int/2addr v9, v3

    .line 119
    move/from16 v0, p3

    if-lt v9, v0, :cond_0

    .line 109
    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 143
    return-void

    .line 125
    .end local v3    # "i":I
    .end local v8    # "readLength":I
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .restart local v8    # "readLength":I
    goto :goto_0

    .line 130
    .end local v8    # "readLength":I
    :catch_0
    move-exception v2

    .line 133
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    :try_start_4
    new-instance v5, Ljava/lang/OutOfMemoryError;

    .line 134
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v12, "OutOfMemoryError likely caused by the Sun VM Bug described in https://issues.apache.org/jira/browse/LUCENE-1566; try calling FSDirectory.setReadChunkSize with a value smaller than the current chunk size ("

    invoke-direct {v10, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/store/SimpleFSDirectory$SimpleFSIndexInput;->chunkSize:I

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, ")"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 134
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 133
    invoke-direct {v5, v10}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    .line 137
    .local v5, "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v5, v2}, Ljava/lang/OutOfMemoryError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 138
    throw v5

    .line 139
    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    .end local v5    # "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v4

    .line 140
    .local v4, "ioe":Ljava/io/IOException;
    new-instance v10, Ljava/io/IOException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected seekInternal(J)V
    .locals 0
    .param p1, "position"    # J

    .prologue
    .line 147
    return-void
.end method
