.class public Lorg/apache/lucene/store/IOContext;
.super Ljava/lang/Object;
.source "IOContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/store/IOContext$Context;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT:Lorg/apache/lucene/store/IOContext;

.field public static final READ:Lorg/apache/lucene/store/IOContext;

.field public static final READONCE:Lorg/apache/lucene/store/IOContext;


# instance fields
.field public final context:Lorg/apache/lucene/store/IOContext$Context;

.field public final flushInfo:Lorg/apache/lucene/store/FlushInfo;

.field public final mergeInfo:Lorg/apache/lucene/store/MergeInfo;

.field public final readOnce:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    const-class v0, Lorg/apache/lucene/store/IOContext;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/IOContext;->$assertionsDisabled:Z

    .line 47
    new-instance v0, Lorg/apache/lucene/store/IOContext;

    sget-object v3, Lorg/apache/lucene/store/IOContext$Context;->DEFAULT:Lorg/apache/lucene/store/IOContext$Context;

    invoke-direct {v0, v3}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/IOContext$Context;)V

    sput-object v0, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    .line 49
    new-instance v0, Lorg/apache/lucene/store/IOContext;

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/IOContext;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    .line 51
    new-instance v0, Lorg/apache/lucene/store/IOContext;

    invoke-direct {v0, v2}, Lorg/apache/lucene/store/IOContext;-><init>(Z)V

    sput-object v0, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    return-void

    :cond_0
    move v0, v2

    .line 26
    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IOContext;-><init>(Z)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/FlushInfo;)V
    .locals 1
    .param p1, "flushInfo"    # Lorg/apache/lucene/store/FlushInfo;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-boolean v0, Lorg/apache/lucene/store/IOContext;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 59
    :cond_0
    sget-object v0, Lorg/apache/lucene/store/IOContext$Context;->FLUSH:Lorg/apache/lucene/store/IOContext$Context;

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    .line 62
    iput-object p1, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    .line 63
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IOContext$Context;)V
    .locals 1
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext$Context;

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/IOContext$Context;Lorg/apache/lucene/store/MergeInfo;)V

    .line 67
    return-void
.end method

.method private constructor <init>(Lorg/apache/lucene/store/IOContext$Context;Lorg/apache/lucene/store/MergeInfo;)V
    .locals 2
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext$Context;
    .param p2, "mergeInfo"    # Lorg/apache/lucene/store/MergeInfo;

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    sget-boolean v0, Lorg/apache/lucene/store/IOContext;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/lucene/store/IOContext$Context;->MERGE:Lorg/apache/lucene/store/IOContext$Context;

    if-ne p1, v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "MergeInfo must not be null if context is MERGE"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 82
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/store/IOContext;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/lucene/store/IOContext$Context;->FLUSH:Lorg/apache/lucene/store/IOContext$Context;

    if-ne p1, v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Use IOContext(FlushInfo) to create a FLUSH IOContext"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 83
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    .line 85
    iput-object p2, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    .line 87
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/IOContext;Z)V
    .locals 1
    .param p1, "ctxt"    # Lorg/apache/lucene/store/IOContext;
    .param p2, "readOnce"    # Z

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iget-object v0, p1, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    .line 96
    iget-object v0, p1, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    .line 97
    iget-object v0, p1, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    .line 98
    iput-boolean p2, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    .line 99
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/MergeInfo;)V
    .locals 1
    .param p1, "mergeInfo"    # Lorg/apache/lucene/store/MergeInfo;

    .prologue
    .line 77
    sget-object v0, Lorg/apache/lucene/store/IOContext$Context;->MERGE:Lorg/apache/lucene/store/IOContext$Context;

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/IOContext$Context;Lorg/apache/lucene/store/MergeInfo;)V

    .line 78
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1, "readOnce"    # Z

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    sget-object v0, Lorg/apache/lucene/store/IOContext$Context;->READ:Lorg/apache/lucene/store/IOContext$Context;

    iput-object v0, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    .line 71
    iput-object v1, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    .line 72
    iput-boolean p1, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    .line 73
    iput-object v1, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    .line 74
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    if-ne p0, p1, :cond_1

    .line 135
    :cond_0
    :goto_0
    return v1

    .line 116
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 117
    goto :goto_0

    .line 118
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 119
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 120
    check-cast v0, Lorg/apache/lucene/store/IOContext;

    .line 121
    .local v0, "other":Lorg/apache/lucene/store/IOContext;
    iget-object v3, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    iget-object v4, v0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 122
    goto :goto_0

    .line 123
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    if-nez v3, :cond_5

    .line 124
    iget-object v3, v0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    if-eqz v3, :cond_6

    move v1, v2

    .line 125
    goto :goto_0

    .line 126
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    iget-object v4, v0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/FlushInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 127
    goto :goto_0

    .line 128
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    if-nez v3, :cond_7

    .line 129
    iget-object v3, v0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    if-eqz v3, :cond_8

    move v1, v2

    .line 130
    goto :goto_0

    .line 131
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    iget-object v4, v0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/MergeInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 132
    goto :goto_0

    .line 133
    :cond_8
    iget-boolean v3, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    iget-boolean v4, v0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 134
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 103
    const/16 v0, 0x1f

    .line 104
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 105
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 106
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 107
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 108
    mul-int/lit8 v3, v1, 0x1f

    iget-boolean v2, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    if-eqz v2, :cond_3

    const/16 v2, 0x4cf

    :goto_3
    add-int v1, v3, v2

    .line 109
    return v1

    .line 105
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v2}, Lorg/apache/lucene/store/IOContext$Context;->hashCode()I

    move-result v2

    goto :goto_0

    .line 106
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/store/FlushInfo;->hashCode()I

    move-result v2

    goto :goto_1

    .line 107
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/store/MergeInfo;->hashCode()I

    move-result v3

    goto :goto_2

    .line 108
    :cond_3
    const/16 v2, 0x4d5

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IOContext [context="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/store/IOContext;->context:Lorg/apache/lucene/store/IOContext$Context;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mergeInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/IOContext;->mergeInfo:Lorg/apache/lucene/store/MergeInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 141
    const-string v1, ", flushInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/store/IOContext;->flushInfo:Lorg/apache/lucene/store/FlushInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", readOnce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/apache/lucene/store/IOContext;->readOnce:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
