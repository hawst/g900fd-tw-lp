.class public Lorg/apache/lucene/store/LockVerifyServer;
.super Ljava/lang/Object;
.source "LockVerifyServer.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getTime(J)Ljava/lang/String;
    .locals 6
    .param p0, "startTime"    # J

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p0

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 14
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    array-length v9, p0

    const/4 v12, 0x1

    if-eq v9, v12, :cond_0

    .line 45
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "\nUsage: java org.apache.lucene.store.LockVerifyServer port\n"

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 46
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    .line 49
    :cond_0
    const/4 v9, 0x0

    aget-object v9, p0, v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 51
    .local v7, "port":I
    new-instance v8, Ljava/net/ServerSocket;

    invoke-direct {v8, v7}, Ljava/net/ServerSocket;-><init>(I)V

    .line 52
    .local v8, "s":Ljava/net/ServerSocket;
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    .line 53
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "\nReady on port "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "..."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 55
    const/4 v5, 0x0

    .line 56
    .local v5, "lockedID":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 59
    .local v10, "startTime":J
    :goto_0
    invoke-virtual {v8}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    .line 60
    .local v1, "cs":Ljava/net/Socket;
    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    .line 61
    .local v6, "out":Ljava/io/OutputStream;
    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 63
    .local v4, "in":Ljava/io/InputStream;
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 64
    .local v3, "id":I
    invoke-virtual {v4}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 66
    .local v0, "command":I
    const/4 v2, 0x0

    .line 68
    .local v2, "err":Z
    const/4 v9, 0x1

    if-ne v0, v9, :cond_2

    .line 70
    if-eqz v5, :cond_1

    .line 71
    const/4 v2, 0x1

    .line 72
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v10, v11}, Lorg/apache/lucene/store/LockVerifyServer;->getTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, " ERROR: id "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " got lock, but "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " already holds the lock"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 74
    :cond_1
    move v5, v3

    .line 84
    :goto_1
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "."

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 86
    if-eqz v2, :cond_5

    .line 87
    const/4 v9, 0x1

    invoke-virtual {v6, v9}, Ljava/io/OutputStream;->write(I)V

    .line 91
    :goto_2
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 92
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 93
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    goto :goto_0

    .line 75
    :cond_2
    if-nez v0, :cond_4

    .line 76
    if-eq v5, v3, :cond_3

    .line 77
    const/4 v2, 0x1

    .line 78
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v10, v11}, Lorg/apache/lucene/store/LockVerifyServer;->getTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, " ERROR: id "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " released the lock, but "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is the one holding the lock"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 80
    :cond_3
    const/4 v5, 0x0

    .line 81
    goto :goto_1

    .line 82
    :cond_4
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "unrecognized command "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 89
    :cond_5
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Ljava/io/OutputStream;->write(I)V

    goto :goto_2
.end method
