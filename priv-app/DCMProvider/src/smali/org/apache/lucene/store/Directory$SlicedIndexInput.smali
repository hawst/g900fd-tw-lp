.class final Lorg/apache/lucene/store/Directory$SlicedIndexInput;
.super Lorg/apache/lucene/store/BufferedIndexInput;
.source "Directory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/Directory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SlicedIndexInput"
.end annotation


# instance fields
.field base:Lorg/apache/lucene/store/IndexInput;

.field fileOffset:J

.field length:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;JJ)V
    .locals 9
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "base"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "fileOffset"    # J
    .param p5, "length"    # J

    .prologue
    .line 291
    const/16 v8, 0x400

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v8}, Lorg/apache/lucene/store/Directory$SlicedIndexInput;-><init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;JJI)V

    .line 292
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lorg/apache/lucene/store/IndexInput;JJI)V
    .locals 5
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "base"    # Lorg/apache/lucene/store/IndexInput;
    .param p3, "fileOffset"    # J
    .param p5, "length"    # J
    .param p7, "readBufferSize"    # I

    .prologue
    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SlicedIndexInput("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " slice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-long v2, p3, p5

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p7}, Lorg/apache/lucene/store/BufferedIndexInput;-><init>(Ljava/lang/String;I)V

    .line 296
    invoke-virtual {p2}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    .line 297
    iput-wide p3, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->fileOffset:J

    .line 298
    iput-wide p5, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->length:J

    .line 299
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/store/BufferedIndexInput;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->clone()Lorg/apache/lucene/store/Directory$SlicedIndexInput;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/store/Directory$SlicedIndexInput;
    .locals 4

    .prologue
    .line 303
    invoke-super {p0}, Lorg/apache/lucene/store/BufferedIndexInput;->clone()Lorg/apache/lucene/store/BufferedIndexInput;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;

    .line 304
    .local v0, "clone":Lorg/apache/lucene/store/Directory$SlicedIndexInput;
    iget-object v1, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->clone()Lorg/apache/lucene/store/IndexInput;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    .line 305
    iget-wide v2, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->fileOffset:J

    iput-wide v2, v0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->fileOffset:J

    .line 306
    iget-wide v2, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->length:J

    iput-wide v2, v0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->length:J

    .line 307
    return-object v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    iget-object v0, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 336
    return-void
.end method

.method public length()J
    .locals 2

    .prologue
    .line 340
    iget-wide v0, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->length:J

    return-wide v0
.end method

.method protected readInternal([BII)V
    .locals 6
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->getFilePointer()J

    move-result-wide v0

    .line 319
    .local v0, "start":J
    int-to-long v2, p3

    add-long/2addr v2, v0

    iget-wide v4, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->length:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 320
    new-instance v2, Ljava/io/EOFException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "read past EOF: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 321
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    iget-wide v4, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->fileOffset:J

    add-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/store/IndexInput;->seek(J)V

    .line 322
    iget-object v2, p0, Lorg/apache/lucene/store/Directory$SlicedIndexInput;->base:Lorg/apache/lucene/store/IndexInput;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, p3, v3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BIIZ)V

    .line 323
    return-void
.end method

.method protected seekInternal(J)V
    .locals 0
    .param p1, "pos"    # J

    .prologue
    .line 330
    return-void
.end method
