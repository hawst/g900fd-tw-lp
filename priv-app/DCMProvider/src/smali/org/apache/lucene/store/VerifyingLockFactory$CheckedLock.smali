.class Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;
.super Lorg/apache/lucene/store/Lock;
.source "VerifyingLockFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/VerifyingLockFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckedLock"
.end annotation


# instance fields
.field private lock:Lorg/apache/lucene/store/Lock;

.field final synthetic this$0:Lorg/apache/lucene/store/VerifyingLockFactory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/VerifyingLockFactory;Lorg/apache/lucene/store/Lock;)V
    .locals 0
    .param p2, "lock"    # Lorg/apache/lucene/store/Lock;

    .prologue
    .line 49
    iput-object p1, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->this$0:Lorg/apache/lucene/store/VerifyingLockFactory;

    invoke-direct {p0}, Lorg/apache/lucene/store/Lock;-><init>()V

    .line 50
    iput-object p2, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->lock:Lorg/apache/lucene/store/Lock;

    .line 51
    return-void
.end method

.method private verify(B)V
    .locals 7
    .param p1, "message"    # B

    .prologue
    .line 55
    :try_start_0
    new-instance v4, Ljava/net/Socket;

    iget-object v5, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->this$0:Lorg/apache/lucene/store/VerifyingLockFactory;

    iget-object v5, v5, Lorg/apache/lucene/store/VerifyingLockFactory;->host:Ljava/lang/String;

    iget-object v6, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->this$0:Lorg/apache/lucene/store/VerifyingLockFactory;

    iget v6, v6, Lorg/apache/lucene/store/VerifyingLockFactory;->port:I

    invoke-direct {v4, v5, v6}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    .line 56
    .local v4, "s":Ljava/net/Socket;
    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 57
    .local v2, "out":Ljava/io/OutputStream;
    iget-object v5, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->this$0:Lorg/apache/lucene/store/VerifyingLockFactory;

    iget-byte v5, v5, Lorg/apache/lucene/store/VerifyingLockFactory;->id:B

    invoke-virtual {v2, v5}, Ljava/io/OutputStream;->write(I)V

    .line 58
    invoke-virtual {v2, p1}, Ljava/io/OutputStream;->write(I)V

    .line 59
    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 60
    .local v1, "in":Ljava/io/InputStream;
    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 61
    .local v3, "result":I
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 62
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 63
    invoke-virtual {v4}, Ljava/net/Socket;->close()V

    .line 64
    if-eqz v3, :cond_0

    .line 65
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "lock was double acquired"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    .end local v1    # "in":Ljava/io/InputStream;
    .end local v2    # "out":Ljava/io/OutputStream;
    .end local v3    # "result":I
    .end local v4    # "s":Ljava/net/Socket;
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 69
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "in":Ljava/io/InputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "result":I
    .restart local v4    # "s":Ljava/net/Socket;
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized isLocked()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->lock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->isLocked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized obtain()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->lock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->obtain()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized obtain(J)Z
    .locals 3
    .param p1, "lockWaitTimeout"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->lock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/store/Lock;->obtain(J)Z

    move-result v0

    .line 74
    .local v0, "obtained":Z
    if-eqz v0, :cond_0

    .line 75
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->verify(B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    monitor-exit p0

    return v0

    .line 73
    .end local v0    # "obtained":Z
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->isLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->verify(B)V

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/store/VerifyingLockFactory$CheckedLock;->lock:Lorg/apache/lucene/store/Lock;

    invoke-virtual {v0}, Lorg/apache/lucene/store/Lock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :cond_0
    monitor-exit p0

    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
