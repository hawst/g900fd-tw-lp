.class public abstract Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.super Ljava/lang/Object;
.source "Directory.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/Directory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "IndexInputSlicer"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/Directory;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lorg/apache/lucene/store/Directory$IndexInputSlicer;->this$0:Lorg/apache/lucene/store/Directory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract openFullSlice()Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
