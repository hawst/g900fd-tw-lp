.class public final Lorg/apache/lucene/store/ByteArrayDataInput;
.super Lorg/apache/lucene/store/DataInput;
.source "ByteArrayDataInput.java"


# instance fields
.field private bytes:[B

.field private limit:I

.field private pos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 43
    sget-object v0, Lorg/apache/lucene/util/BytesRef;->EMPTY_BYTES:[B

    invoke-virtual {p0, v0}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([B)V

    .line 44
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 35
    invoke-virtual {p0, p1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([B)V

    .line 36
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 0
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/store/DataInput;-><init>()V

    .line 39
    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 40
    return-void
.end method


# virtual methods
.method public eof()Z
    .locals 2

    .prologue
    .line 75
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->limit:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->limit:I

    return v0
.end method

.method public readByte()B
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v0, v1

    return v0
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 166
    return-void
.end method

.method public readInt()I
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 90
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    .line 89
    or-int/2addr v0, v1

    .line 90
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 89
    or-int/2addr v0, v1

    return v0
.end method

.method public readLong()J
    .locals 8

    .prologue
    .line 95
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    .line 96
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    .line 95
    or-int/2addr v2, v3

    .line 96
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 95
    or-int v0, v2, v3

    .line 97
    .local v0, "i1":I
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    .line 98
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    .line 97
    or-int/2addr v2, v3

    .line 98
    iget-object v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 97
    or-int v1, v2, v3

    .line 99
    .local v1, "i2":I
    int-to-long v2, v0

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    int-to-long v4, v1

    const-wide v6, 0xffffffffL

    and-long/2addr v4, v6

    or-long/2addr v2, v4

    return-wide v2
.end method

.method public readShort()S
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public readVInt()I
    .locals 5

    .prologue
    .line 104
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v2, v3

    .line 105
    .local v0, "b":B
    if-ltz v0, :cond_1

    move v1, v0

    .line 119
    :cond_0
    return v1

    .line 106
    :cond_1
    and-int/lit8 v1, v0, 0x7f

    .line 107
    .local v1, "i":I
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v2, v3

    .line 108
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x7

    or-int/2addr v1, v2

    .line 109
    if-gez v0, :cond_0

    .line 110
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v2, v3

    .line 111
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0xe

    or-int/2addr v1, v2

    .line 112
    if-gez v0, :cond_0

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v2, v3

    .line 114
    and-int/lit8 v2, v0, 0x7f

    shl-int/lit8 v2, v2, 0x15

    or-int/2addr v1, v2

    .line 115
    if-gez v0, :cond_0

    .line 116
    iget-object v2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v2, v3

    .line 118
    and-int/lit8 v2, v0, 0xf

    shl-int/lit8 v2, v2, 0x1c

    or-int/2addr v1, v2

    .line 119
    and-int/lit16 v2, v0, 0xf0

    if-eqz v2, :cond_0

    .line 120
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Invalid vInt detected (too many bits)"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public readVLong()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x7f

    .line 125
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 126
    .local v0, "b":B
    if-ltz v0, :cond_1

    int-to-long v2, v0

    .line 151
    :cond_0
    return-wide v2

    .line 127
    :cond_1
    int-to-long v4, v0

    and-long v2, v4, v6

    .line 128
    .local v2, "i":J
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 129
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/4 v1, 0x7

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 130
    if-gez v0, :cond_0

    .line 131
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 132
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0xe

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 133
    if-gez v0, :cond_0

    .line 134
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 135
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x15

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 136
    if-gez v0, :cond_0

    .line 137
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 138
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x1c

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 139
    if-gez v0, :cond_0

    .line 140
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 141
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x23

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 142
    if-gez v0, :cond_0

    .line 143
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 144
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x2a

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 145
    if-gez v0, :cond_0

    .line 146
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 147
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x31

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 148
    if-gez v0, :cond_0

    .line 149
    iget-object v1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    iget v4, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    aget-byte v0, v1, v4

    .line 150
    int-to-long v4, v0

    and-long/2addr v4, v6

    const/16 v1, 0x38

    shl-long/2addr v4, v1

    or-long/2addr v2, v4

    .line 151
    if-gez v0, :cond_0

    .line 152
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v4, "Invalid vLong detected (negative values disallowed)"

    invoke-direct {v1, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public reset([B)V
    .locals 2
    .param p1, "bytes"    # [B

    .prologue
    .line 47
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/lucene/store/ByteArrayDataInput;->reset([BII)V

    .line 48
    return-void
.end method

.method public reset([BII)V
    .locals 1
    .param p1, "bytes"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 65
    iput-object p1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->bytes:[B

    .line 66
    iput p2, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 67
    add-int v0, p2, p3

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->limit:I

    .line 68
    return-void
.end method

.method public rewind()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 54
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 61
    iput p1, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 62
    return-void
.end method

.method public skipBytes(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 79
    iget v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/lucene/store/ByteArrayDataInput;->pos:I

    .line 80
    return-void
.end method
