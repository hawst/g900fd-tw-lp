.class final Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;
.super Lorg/apache/lucene/store/ByteBufferIndexInput;
.source "MMapDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/MMapDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MMapIndexInput"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/MMapDirectory;

.field private final useUnmapHack:Z


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/MMapDirectory;Ljava/lang/String;Ljava/io/RandomAccessFile;)V
    .locals 8
    .param p2, "resourceDescription"    # Ljava/lang/String;
    .param p3, "raf"    # Ljava/io/RandomAccessFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 227
    iput-object p1, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->this$0:Lorg/apache/lucene/store/MMapDirectory;

    .line 228
    const-wide/16 v2, 0x0

    invoke-virtual {p3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    move-object v0, p1

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/MMapDirectory;->map(Ljava/io/RandomAccessFile;JJ)[Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {p3}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    iget v6, p1, Lorg/apache/lucene/store/MMapDirectory;->chunkSizePower:I

    invoke-virtual {p1}, Lorg/apache/lucene/store/MMapDirectory;->getUseUnmap()Z

    move-result v7

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/store/ByteBufferIndexInput;-><init>(Ljava/lang/String;[Ljava/nio/ByteBuffer;JIZ)V

    .line 229
    invoke-virtual {p1}, Lorg/apache/lucene/store/MMapDirectory;->getUseUnmap()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->useUnmapHack:Z

    .line 230
    return-void
.end method


# virtual methods
.method protected freeBuffer(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 239
    iget-boolean v2, p0, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->useUnmapHack:Z

    if-eqz v2, :cond_0

    .line 241
    :try_start_0
    new-instance v2, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput$1;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput$1;-><init>(Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;Ljava/nio/ByteBuffer;)V

    invoke-static {v2}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/security/PrivilegedActionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :cond_0
    return-void

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Ljava/security/PrivilegedActionException;
    new-instance v1, Ljava/io/IOException;

    const-string v2, "unable to unmap the mapped buffer"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 257
    .local v1, "ioe":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/security/PrivilegedActionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 258
    throw v1
.end method
