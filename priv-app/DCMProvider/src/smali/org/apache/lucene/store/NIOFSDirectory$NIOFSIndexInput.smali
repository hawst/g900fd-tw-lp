.class public Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;
.super Lorg/apache/lucene/store/FSDirectory$FSIndexInput;
.source "NIOFSDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/store/NIOFSDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "NIOFSIndexInput"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private byteBuf:Ljava/nio/ByteBuffer;

.field final channel:Ljava/nio/channels/FileChannel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    const-class v0, Lorg/apache/lucene/store/NIOFSDirectory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V
    .locals 2
    .param p1, "path"    # Ljava/io/File;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .param p3, "chunkSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NIOFSIndexInput(path=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2, p3}, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;-><init>(Ljava/lang/String;Ljava/io/File;Lorg/apache/lucene/store/IOContext;I)V

    .line 124
    iget-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->file:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->channel:Ljava/nio/channels/FileChannel;

    .line 125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Ljava/io/RandomAccessFile;Ljava/nio/channels/FileChannel;JJII)V
    .locals 13
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/io/File;
    .param p3, "file"    # Ljava/io/RandomAccessFile;
    .param p4, "fc"    # Ljava/nio/channels/FileChannel;
    .param p5, "off"    # J
    .param p7, "length"    # J
    .param p9, "bufferSize"    # I
    .param p10, "chunkSize"    # I

    .prologue
    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NIOFSIndexInput("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in path=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" slice="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-long v4, p5, p7

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, p0

    move-object/from16 v5, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v3 .. v11}, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;-><init>(Ljava/lang/String;Ljava/io/RandomAccessFile;JJII)V

    .line 129
    move-object/from16 v0, p4

    iput-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->channel:Ljava/nio/channels/FileChannel;

    .line 130
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->isClone:Z

    .line 131
    return-void
.end method


# virtual methods
.method protected newBuffer([B)V
    .locals 1
    .param p1, "newBuffer"    # [B

    .prologue
    .line 135
    invoke-super {p0, p1}, Lorg/apache/lucene/store/FSDirectory$FSIndexInput;->newBuffer([B)V

    .line 136
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    .line 137
    return-void
.end method

.method protected readInternal([BII)V
    .locals 16
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->buffer:[B

    move-object/from16 v0, p1

    if-ne v0, v12, :cond_1

    if-nez p2, :cond_1

    .line 147
    sget-boolean v12, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->$assertionsDisabled:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    if-nez v12, :cond_0

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 148
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    invoke-virtual {v12}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 149
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->byteBuf:Ljava/nio/ByteBuffer;

    .line 155
    .local v2, "bb":Ljava/nio/ByteBuffer;
    :goto_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v11

    .line 156
    .local v11, "readOffset":I
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v12

    sub-int v10, v12, v11

    .line 157
    .local v10, "readLength":I
    sget-boolean v12, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->$assertionsDisabled:Z

    if-nez v12, :cond_2

    move/from16 v0, p3

    if-eq v10, v0, :cond_2

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 152
    .end local v2    # "bb":Ljava/nio/ByteBuffer;
    .end local v10    # "readLength":I
    .end local v11    # "readOffset":I
    :cond_1
    invoke-static/range {p1 .. p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    .restart local v2    # "bb":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 159
    .restart local v10    # "readLength":I
    .restart local v11    # "readOffset":I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->getFilePointer()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->off:J

    add-long v8, v12, v14

    .line 161
    .local v8, "pos":J
    move/from16 v0, p3

    int-to-long v12, v0

    add-long/2addr v12, v8

    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->end:J

    cmp-long v12, v12, v14

    if-lez v12, :cond_4

    .line 162
    new-instance v12, Ljava/io/EOFException;

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "read past EOF: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 168
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->chunkSize:I

    if-le v10, v12, :cond_5

    .line 171
    move-object/from16 v0, p0

    iget v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->chunkSize:I

    add-int v6, v11, v12

    .line 175
    .local v6, "limit":I
    :goto_1
    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 176
    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v12, v2, v8, v9}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;J)I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    .line 177
    .local v4, "i":I
    int-to-long v12, v4

    add-long/2addr v8, v12

    .line 178
    add-int/2addr v11, v4

    .line 179
    sub-int/2addr v10, v4

    .line 166
    .end local v4    # "i":I
    .end local v6    # "limit":I
    :cond_4
    if-gtz v10, :cond_3

    .line 193
    return-void

    .line 173
    :cond_5
    add-int v6, v11, v10

    .restart local v6    # "limit":I
    goto :goto_1

    .line 181
    .end local v6    # "limit":I
    :catch_0
    move-exception v3

    .line 184
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    new-instance v7, Ljava/lang/OutOfMemoryError;

    .line 185
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "OutOfMemoryError likely caused by the Sun VM Bug described in https://issues.apache.org/jira/browse/LUCENE-1566; try calling FSDirectory.setReadChunkSize with a value smaller than the current chunk size ("

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 187
    move-object/from16 v0, p0

    iget v13, v0, Lorg/apache/lucene/store/NIOFSDirectory$NIOFSIndexInput;->chunkSize:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 185
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 184
    invoke-direct {v7, v12}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    .line 188
    .local v7, "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v7, v3}, Ljava/lang/OutOfMemoryError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 189
    throw v7

    .line 190
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    .end local v7    # "outOfMemoryError":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v5

    .line 191
    .local v5, "ioe":Ljava/io/IOException;
    new-instance v12, Ljava/io/IOException;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12
.end method

.method protected seekInternal(J)V
    .locals 0
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    return-void
.end method
