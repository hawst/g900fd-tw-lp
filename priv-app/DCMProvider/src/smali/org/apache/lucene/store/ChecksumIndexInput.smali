.class public Lorg/apache/lucene/store/ChecksumIndexInput;
.super Lorg/apache/lucene/store/IndexInput;
.source "ChecksumIndexInput.java"


# instance fields
.field digest:Ljava/util/zip/Checksum;

.field main:Lorg/apache/lucene/store/IndexInput;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/IndexInput;)V
    .locals 2
    .param p1, "main"    # Lorg/apache/lucene/store/IndexInput;

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChecksumIndexInput("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/store/IndexInput;-><init>(Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->main:Lorg/apache/lucene/store/IndexInput;

    .line 36
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->digest:Ljava/util/zip/Checksum;

    .line 37
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->main:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 61
    return-void
.end method

.method public getChecksum()J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->digest:Ljava/util/zip/Checksum;

    invoke-interface {v0}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFilePointer()J
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->main:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->getFilePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->main:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/IndexInput;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public readByte()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v1, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->main:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexInput;->readByte()B

    move-result v0

    .line 42
    .local v0, "b":B
    iget-object v1, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->digest:Ljava/util/zip/Checksum;

    invoke-interface {v1, v0}, Ljava/util/zip/Checksum;->update(I)V

    .line 43
    return v0
.end method

.method public readBytes([BII)V
    .locals 1
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->main:Lorg/apache/lucene/store/IndexInput;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/store/IndexInput;->readBytes([BII)V

    .line 50
    iget-object v0, p0, Lorg/apache/lucene/store/ChecksumIndexInput;->digest:Ljava/util/zip/Checksum;

    invoke-interface {v0, p1, p2, p3}, Ljava/util/zip/Checksum;->update([BII)V

    .line 51
    return-void
.end method

.method public seek(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 70
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
