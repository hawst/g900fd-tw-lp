.class Lorg/apache/lucene/store/NativeFSLock;
.super Lorg/apache/lucene/store/Lock;
.source "NativeFSLockFactory.java"


# static fields
.field private static LOCK_HELD:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private channel:Ljava/nio/channels/FileChannel;

.field private f:Ljava/io/RandomAccessFile;

.field private lock:Ljava/nio/channels/FileLock;

.field private lockDir:Ljava/io/File;

.field private path:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .param p1, "lockDir"    # Ljava/io/File;
    .param p2, "lockFileName"    # Ljava/lang/String;

    .prologue
    .line 151
    invoke-direct {p0}, Lorg/apache/lucene/store/Lock;-><init>()V

    .line 152
    iput-object p1, p0, Lorg/apache/lucene/store/NativeFSLock;->lockDir:Ljava/io/File;

    .line 153
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    .line 154
    return-void
.end method

.method private declared-synchronized lockExists()Z
    .locals 1

    .prologue
    .line 157
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized isLocked()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 312
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    .line 323
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 315
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 319
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/lucene/store/NativeFSLock;->obtain()Z

    move-result v1

    .line 320
    .local v1, "obtained":Z
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/lucene/store/NativeFSLock;->release()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 321
    :cond_2
    if-nez v1, :cond_0

    move v2, v3

    goto :goto_0

    .line 322
    .end local v1    # "obtained":Z
    :catch_0
    move-exception v0

    .line 323
    .local v0, "ioe":Ljava/io/IOException;
    goto :goto_0

    .line 312
    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized obtain()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 163
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    .line 258
    :cond_0
    :goto_0
    monitor-exit p0

    return v3

    .line 169
    :cond_1
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->lockDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 170
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->lockDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v4

    if-nez v4, :cond_3

    .line 171
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot create directory: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 172
    iget-object v5, p0, Lorg/apache/lucene/store/NativeFSLock;->lockDir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 171
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 173
    :cond_2
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->lockDir:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_3

    .line 175
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found regular file where directory expected: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 176
    iget-object v5, p0, Lorg/apache/lucene/store/NativeFSLock;->lockDir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 175
    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 179
    :cond_3
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 181
    .local v0, "canonicalPath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 188
    .local v2, "markedHeld":Z
    :try_start_3
    sget-object v4, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 189
    :try_start_4
    sget-object v5, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 191
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 250
    if-eqz v2, :cond_0

    :try_start_5
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 251
    sget-object v4, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 252
    :try_start_6
    sget-object v5, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 253
    sget-object v5, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 251
    :cond_4
    monitor-exit v4

    goto :goto_0

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 197
    :cond_5
    :try_start_8
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 198
    const/4 v2, 0x1

    .line 188
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 203
    :try_start_9
    new-instance v3, Ljava/io/RandomAccessFile;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 213
    :goto_1
    :try_start_a
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    if-eqz v3, :cond_7

    .line 215
    :try_start_b
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    .line 217
    :try_start_c
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->tryLock()Ljava/nio/channels/FileLock;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 230
    :try_start_d
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    if-nez v3, :cond_6

    .line 232
    :try_start_e
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    .line 234
    const/4 v3, 0x0

    :try_start_f
    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 239
    :cond_6
    :goto_2
    :try_start_10
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    if-nez v3, :cond_7

    .line 241
    :try_start_11
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_a

    .line 243
    const/4 v3, 0x0

    :try_start_12
    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    .line 250
    :cond_7
    if-eqz v2, :cond_9

    :try_start_13
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z

    move-result v3

    if-nez v3, :cond_9

    .line 251
    sget-object v4, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v4
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 252
    :try_start_14
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 253
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 251
    :cond_8
    monitor-exit v4
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_c

    .line 258
    :cond_9
    :try_start_15
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    move-result v3

    goto/16 :goto_0

    .line 188
    :catchall_2
    move-exception v3

    :try_start_16
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    :try_start_17
    throw v3
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    .line 249
    :catchall_3
    move-exception v3

    .line 250
    if-eqz v2, :cond_b

    :try_start_18
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z

    move-result v4

    if-nez v4, :cond_b

    .line 251
    sget-object v4, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v4
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 252
    :try_start_19
    sget-object v5, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 253
    sget-object v5, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 251
    :cond_a
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_b

    .line 257
    :cond_b
    :try_start_1a
    throw v3
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    .line 204
    :catch_0
    move-exception v1

    .line 209
    .local v1, "e":Ljava/io/IOException;
    :try_start_1b
    iput-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->failureReason:Ljava/lang/Throwable;

    .line 210
    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    goto :goto_1

    .line 218
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 228
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_1c
    iput-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->failureReason:Ljava/lang/Throwable;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_6

    .line 230
    :try_start_1d
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_4

    if-nez v3, :cond_6

    .line 232
    :try_start_1e
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_5

    .line 234
    const/4 v3, 0x0

    :try_start_1f
    iput-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_4

    goto :goto_2

    .line 238
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_4
    move-exception v3

    .line 239
    :try_start_20
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    if-nez v4, :cond_c

    .line 241
    :try_start_21
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_9

    .line 243
    const/4 v4, 0x0

    :try_start_22
    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 246
    :cond_c
    throw v3
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    .line 233
    .restart local v1    # "e":Ljava/io/IOException;
    :catchall_5
    move-exception v3

    .line 234
    const/4 v4, 0x0

    :try_start_23
    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    .line 235
    throw v3

    .line 229
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_6
    move-exception v3

    .line 230
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_4

    if-nez v4, :cond_d

    .line 232
    :try_start_24
    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_7

    .line 234
    const/4 v4, 0x0

    :try_start_25
    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    .line 237
    :cond_d
    throw v3

    .line 233
    :catchall_7
    move-exception v3

    .line 234
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    .line 235
    throw v3

    .line 233
    :catchall_8
    move-exception v3

    .line 234
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    .line 235
    throw v3
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_4

    .line 242
    :catchall_9
    move-exception v3

    .line 243
    const/4 v4, 0x0

    :try_start_26
    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 244
    throw v3

    .line 242
    :catchall_a
    move-exception v3

    .line 243
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 244
    throw v3
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_3

    .line 251
    :catchall_b
    move-exception v3

    :try_start_27
    monitor-exit v4
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_b

    :try_start_28
    throw v3
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_0

    :catchall_c
    move-exception v3

    :try_start_29
    monitor-exit v4
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_c

    :try_start_2a
    throw v3
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_0
.end method

.method public declared-synchronized release()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/store/NativeFSLock;->lockExists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-eqz v1, :cond_1

    .line 265
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 269
    :try_start_3
    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_9

    .line 271
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 273
    :try_start_5
    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_d

    .line 275
    const/4 v1, 0x0

    :try_start_6
    iput-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 277
    :try_start_7
    sget-object v1, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_f

    .line 285
    :try_start_8
    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 305
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 266
    :catchall_0
    move-exception v1

    .line 267
    const/4 v2, 0x0

    :try_start_9
    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->lock:Ljava/nio/channels/FileLock;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 269
    :try_start_a
    iget-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 271
    const/4 v2, 0x0

    :try_start_b
    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 273
    :try_start_c
    iget-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 275
    const/4 v2, 0x0

    :try_start_d
    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 277
    :try_start_e
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_8

    .line 281
    :try_start_f
    throw v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 263
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 270
    :catchall_2
    move-exception v1

    .line 271
    const/4 v2, 0x0

    :try_start_10
    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 273
    :try_start_11
    iget-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 275
    const/4 v2, 0x0

    :try_start_12
    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 277
    :try_start_13
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    .line 280
    :try_start_14
    throw v1

    .line 274
    :catchall_3
    move-exception v1

    .line 275
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 277
    :try_start_15
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    .line 279
    :try_start_16
    throw v1
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 276
    :catchall_4
    move-exception v1

    :try_start_17
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    :try_start_18
    throw v1
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    :catchall_5
    move-exception v1

    :try_start_19
    monitor-exit v2
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_5

    :try_start_1a
    throw v1

    .line 274
    :catchall_6
    move-exception v1

    .line 275
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 277
    :try_start_1b
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_7

    .line 279
    :try_start_1c
    throw v1
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    .line 276
    :catchall_7
    move-exception v1

    :try_start_1d
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_7

    :try_start_1e
    throw v1
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    :catchall_8
    move-exception v1

    :try_start_1f
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_8

    :try_start_20
    throw v1

    .line 270
    :catchall_9
    move-exception v1

    .line 271
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->channel:Ljava/nio/channels/FileChannel;
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    .line 273
    :try_start_21
    iget-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_a

    .line 275
    const/4 v2, 0x0

    :try_start_22
    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    .line 277
    :try_start_23
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_c

    .line 280
    :try_start_24
    throw v1

    .line 274
    :catchall_a
    move-exception v1

    .line 275
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_1

    .line 277
    :try_start_25
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_b

    .line 279
    :try_start_26
    throw v1
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    .line 276
    :catchall_b
    move-exception v1

    :try_start_27
    monitor-exit v2
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_b

    :try_start_28
    throw v1
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    :catchall_c
    move-exception v1

    :try_start_29
    monitor-exit v2
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_c

    :try_start_2a
    throw v1

    .line 274
    :catchall_d
    move-exception v1

    .line 275
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/store/NativeFSLock;->f:Ljava/io/RandomAccessFile;

    .line 276
    sget-object v2, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    monitor-enter v2
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_1

    .line 277
    :try_start_2b
    sget-object v3, Lorg/apache/lucene/store/NativeFSLock;->LOCK_HELD:Ljava/util/HashSet;

    iget-object v4, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 276
    monitor-exit v2
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_e

    .line 279
    :try_start_2c
    throw v1
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_1

    .line 276
    :catchall_e
    move-exception v1

    :try_start_2d
    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_e

    :try_start_2e
    throw v1
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_1

    :catchall_f
    move-exception v1

    :try_start_2f
    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_f

    :try_start_30
    throw v1
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_1

    .line 292
    :cond_1
    const/4 v0, 0x0

    .line 294
    .local v0, "obtained":Z
    :try_start_31
    invoke-virtual {p0}, Lorg/apache/lucene/store/NativeFSLock;->obtain()Z

    move-result v0

    if-nez v0, :cond_3

    .line 295
    new-instance v1, Lorg/apache/lucene/store/LockReleaseFailedException;

    .line 296
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot forcefully unlock a NativeFSLock which is held by another indexer component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 297
    iget-object v3, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 296
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 295
    invoke-direct {v1, v2}, Lorg/apache/lucene/store/LockReleaseFailedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_10

    .line 299
    :catchall_10
    move-exception v1

    .line 300
    if-eqz v0, :cond_2

    .line 301
    :try_start_32
    invoke-virtual {p0}, Lorg/apache/lucene/store/NativeFSLock;->release()V

    .line 303
    :cond_2
    throw v1

    .line 300
    :cond_3
    if-eqz v0, :cond_0

    .line 301
    invoke-virtual {p0}, Lorg/apache/lucene/store/NativeFSLock;->release()V
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_1

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NativeFSLock@"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/store/NativeFSLock;->path:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
