.class Lorg/apache/lucene/store/MMapDirectory$1;
.super Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.source "MMapDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/store/MMapDirectory;->createSlicer(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/Directory$IndexInputSlicer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/store/MMapDirectory;

.field private final synthetic val$full:Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/MMapDirectory;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/store/MMapDirectory$1;->this$0:Lorg/apache/lucene/store/MMapDirectory;

    iput-object p3, p0, Lorg/apache/lucene/store/MMapDirectory$1;->val$full:Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    .line 204
    invoke-direct {p0, p2}, Lorg/apache/lucene/store/Directory$IndexInputSlicer;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$1;->val$full:Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->close()V

    .line 220
    return-void
.end method

.method public openFullSlice()Lorg/apache/lucene/store/IndexInput;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$1;->this$0:Lorg/apache/lucene/store/MMapDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/MMapDirectory;->ensureOpen()V

    .line 214
    iget-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$1;->val$full:Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    invoke-virtual {v0}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->clone()Lorg/apache/lucene/store/ByteBufferIndexInput;

    move-result-object v0

    return-object v0
.end method

.method public openSlice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/IndexInput;
    .locals 6
    .param p1, "sliceDescription"    # Ljava/lang/String;
    .param p2, "offset"    # J
    .param p4, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$1;->this$0:Lorg/apache/lucene/store/MMapDirectory;

    invoke-virtual {v0}, Lorg/apache/lucene/store/MMapDirectory;->ensureOpen()V

    .line 208
    iget-object v0, p0, Lorg/apache/lucene/store/MMapDirectory$1;->val$full:Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/store/MMapDirectory$MMapIndexInput;->slice(Ljava/lang/String;JJ)Lorg/apache/lucene/store/ByteBufferIndexInput;

    move-result-object v0

    return-object v0
.end method
