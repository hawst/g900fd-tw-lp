.class public abstract Lorg/apache/lucene/index/Fields;
.super Ljava/lang/Object;
.source "Fields.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field public static final EMPTY_ARRAY:[Lorg/apache/lucene/index/Fields;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/index/Fields;

    sput-object v0, Lorg/apache/lucene/index/Fields;->EMPTY_ARRAY:[Lorg/apache/lucene/index/Fields;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public getUniqueTermCount()J
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 58
    const-wide/16 v2, 0x0

    .line 59
    .local v2, "numTerms":J
    invoke-virtual {p0}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 70
    .end local v2    # "numTerms":J
    :goto_1
    return-wide v2

    .line 59
    .restart local v2    # "numTerms":J
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 60
    .local v0, "field":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 61
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {v1}, Lorg/apache/lucene/index/Terms;->size()J

    move-result-wide v4

    .line 63
    .local v4, "termCount":J
    cmp-long v9, v4, v6

    if-nez v9, :cond_2

    move-wide v2, v6

    .line 64
    goto :goto_1

    .line 67
    :cond_2
    add-long/2addr v2, v4

    goto :goto_0
.end method

.method public abstract iterator()Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract size()I
.end method

.method public abstract terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
