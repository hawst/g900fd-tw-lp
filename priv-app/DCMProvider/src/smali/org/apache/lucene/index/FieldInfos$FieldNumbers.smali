.class final Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
.super Ljava/lang/Object;
.source "FieldInfos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FieldInfos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FieldNumbers"
.end annotation


# instance fields
.field private final docValuesType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/FieldInfo$DocValuesType;",
            ">;"
        }
    .end annotation
.end field

.field private lowestUnassignedFieldNumber:I

.field private final nameToNumber:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final numberToName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->lowestUnassignedFieldNumber:I

    .line 177
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->nameToNumber:Ljava/util/Map;

    .line 178
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->numberToName:Ljava/util/Map;

    .line 179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->docValuesType:Ljava/util/Map;

    .line 180
    return-void
.end method


# virtual methods
.method declared-synchronized addOrGet(Ljava/lang/String;ILorg/apache/lucene/index/FieldInfo$DocValuesType;)I
    .locals 6
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "preferredFieldNumber"    # I
    .param p3, "dvType"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 189
    monitor-enter p0

    if-eqz p3, :cond_0

    .line 190
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->docValuesType:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .line 191
    .local v0, "currentDVType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    if-nez v0, :cond_2

    .line 192
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->docValuesType:Ljava/util/Map;

    invoke-interface {v3, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    .end local v0    # "currentDVType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->nameToNumber:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 198
    .local v1, "fieldNumber":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 199
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 201
    .local v2, "preferredBoxed":Ljava/lang/Integer;
    const/4 v3, -0x1

    if-eq p2, v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->numberToName:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 203
    move-object v1, v2

    .line 212
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->numberToName:Ljava/util/Map;

    invoke-interface {v3, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->nameToNumber:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    .end local v2    # "preferredBoxed":Ljava/lang/Integer;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    monitor-exit p0

    return v3

    .line 193
    .end local v1    # "fieldNumber":Ljava/lang/Integer;
    .restart local v0    # "currentDVType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p3, :cond_0

    .line 194
    :try_start_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cannot change DocValues type from "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for field \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    .end local v0    # "currentDVType":Lorg/apache/lucene/index/FieldInfo$DocValuesType;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 206
    .restart local v1    # "fieldNumber":Ljava/lang/Integer;
    .restart local v2    # "preferredBoxed":Ljava/lang/Integer;
    :cond_3
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->numberToName:Ljava/util/Map;

    iget v4, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->lowestUnassignedFieldNumber:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->lowestUnassignedFieldNumber:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 209
    iget v3, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->lowestUnassignedFieldNumber:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_0
.end method

.method declared-synchronized clear()V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->numberToName:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 228
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->nameToNumber:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 229
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->docValuesType:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized containsConsistent(Ljava/lang/Integer;Ljava/lang/String;Lorg/apache/lucene/index/FieldInfo$DocValuesType;)Z
    .locals 1
    .param p1, "number"    # Ljava/lang/Integer;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "dvType"    # Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->numberToName:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->nameToNumber:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    if-eqz p3, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->docValuesType:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos$FieldNumbers;->docValuesType:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-ne p3, v0, :cond_1

    .line 221
    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
