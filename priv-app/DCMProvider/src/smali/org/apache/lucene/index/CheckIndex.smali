.class public Lorg/apache/lucene/index/CheckIndex;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/CheckIndex$Status;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$index$FieldInfo$DocValuesType:[I

.field static final synthetic $assertionsDisabled:Z

.field private static assertsOn:Z


# instance fields
.field private crossCheckTermVectors:Z

.field private dir:Lorg/apache/lucene/store/Directory;

.field private infoStream:Ljava/io/PrintStream;

.field private verbose:Z


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$index$FieldInfo$DocValuesType()[I
    .locals 3

    .prologue
    .line 61
    sget-object v0, Lorg/apache/lucene/index/CheckIndex;->$SWITCH_TABLE$org$apache$lucene$index$FieldInfo$DocValuesType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->values()[Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->BINARY:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->SORTED_SET:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lorg/apache/lucene/index/CheckIndex;->$SWITCH_TABLE$org$apache$lucene$index$FieldInfo$DocValuesType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lorg/apache/lucene/index/CheckIndex;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    .line 1703
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 313
    iput-object p1, p0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    .line 314
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    .line 315
    return-void
.end method

.method private static assertsOn()Z
    .locals 1

    .prologue
    .line 1711
    sget-boolean v0, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/lucene/index/CheckIndex;->testAsserts()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1712
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/CheckIndex;->assertsOn:Z

    return v0
.end method

.method private static checkBinaryDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/BinaryDocValues;)V
    .locals 3
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "dv"    # Lorg/apache/lucene/index/BinaryDocValues;

    .prologue
    .line 1314
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 1315
    .local v1, "scratch":Lorg/apache/lucene/util/BytesRef;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 1319
    return-void

    .line 1316
    :cond_0
    invoke-virtual {p2, v0, v1}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 1317
    sget-boolean v2, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lorg/apache/lucene/util/BytesRef;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1315
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static checkDocValues(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)V
    .locals 3
    .param p0, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "infoStream"    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1402
    invoke-static {}, Lorg/apache/lucene/index/CheckIndex;->$SWITCH_TABLE$org$apache$lucene$index$FieldInfo$DocValuesType()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1436
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1404
    :pswitch_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/index/CheckIndex;->checkSortedDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/SortedDocValues;)V

    .line 1405
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1406
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1407
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1408
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " returns multiple docvalues types!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1412
    :pswitch_1
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/index/CheckIndex;->checkSortedSetDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/SortedSetDocValues;)V

    .line 1413
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1414
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1415
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1416
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " returns multiple docvalues types!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1420
    :pswitch_2
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/index/CheckIndex;->checkBinaryDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/BinaryDocValues;)V

    .line 1421
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1422
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1423
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1424
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " returns multiple docvalues types!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1428
    :pswitch_3
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/index/CheckIndex;->checkNumericDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 1429
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1430
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1431
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1432
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " returns multiple docvalues types!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1438
    :cond_4
    return-void

    .line 1402
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static checkFields(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/util/Bits;ILorg/apache/lucene/index/FieldInfos;ZZLjava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    .locals 86
    .param p0, "fields"    # Lorg/apache/lucene/index/Fields;
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "maxDoc"    # I
    .param p3, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p4, "doPrint"    # Z
    .param p5, "isVectors"    # Z
    .param p6, "infoStream"    # Ljava/io/PrintStream;
    .param p7, "verbose"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 715
    new-instance v50, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    invoke-direct/range {v50 .. v50}, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;-><init>()V

    .line 716
    .local v50, "status":Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    const/4 v4, 0x0

    .line 718
    .local v4, "computedFieldCount":I
    if-nez p0, :cond_1

    .line 719
    const-string v79, "OK [no fields/terms]"

    move-object/from16 v0, p6

    move-object/from16 v1, v79

    invoke-static {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1185
    :cond_0
    return-object v50

    .line 723
    :cond_1
    const/4 v10, 0x0

    .line 724
    .local v10, "docs":Lorg/apache/lucene/index/DocsEnum;
    const/4 v12, 0x0

    .line 725
    .local v12, "docsAndFreqs":Lorg/apache/lucene/index/DocsEnum;
    const/16 v44, 0x0

    .line 727
    .local v44, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    const/16 v31, 0x0

    .line 728
    .local v31, "lastField":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v80

    :cond_2
    invoke-interface/range {v80 .. v80}, Ljava/util/Iterator;->hasNext()Z

    move-result v79

    if-nez v79, :cond_3

    .line 1154
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/Fields;->size()I

    move-result v18

    .line 1156
    .local v18, "fieldCount":I
    const/16 v79, -0x1

    move/from16 v0, v18

    move/from16 v1, v79

    if-eq v0, v1, :cond_4a

    .line 1157
    if-gez v18, :cond_49

    .line 1158
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "invalid fieldCount: "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 728
    .end local v18    # "fieldCount":I
    :cond_3
    invoke-interface/range {v80 .. v80}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 730
    .local v17, "field":Ljava/lang/String;
    if-eqz v31, :cond_4

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v79

    if-gtz v79, :cond_4

    .line 731
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "fields out of order: lastField="

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " field="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 733
    :cond_4
    move-object/from16 v31, v17

    .line 737
    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v19

    .line 738
    .local v19, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    if-nez v19, :cond_5

    .line 739
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "fieldsEnum inconsistent with fieldInfos, no fieldInfos for: "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 741
    :cond_5
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v79

    if-nez v79, :cond_6

    .line 742
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "fieldsEnum inconsistent with fieldInfos, isIndexed == false for: "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 749
    :cond_6
    add-int/lit8 v4, v4, 0x1

    .line 751
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v57

    .line 752
    .local v57, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v57, :cond_2

    .line 756
    invoke-virtual/range {v57 .. v57}, Lorg/apache/lucene/index/Terms;->hasPositions()Z

    move-result v25

    .line 757
    .local v25, "hasPositions":Z
    invoke-virtual/range {v57 .. v57}, Lorg/apache/lucene/index/Terms;->hasOffsets()Z

    move-result v23

    .line 759
    .local v23, "hasOffsets":Z
    if-nez p5, :cond_8

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v79

    sget-object v81, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, v79

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v79

    if-gez v79, :cond_8

    const/16 v22, 0x0

    .line 761
    .local v22, "hasFreqs":Z
    :goto_0
    const/16 v79, 0x0

    move-object/from16 v0, v57

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v62

    .line 763
    .local v62, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/16 v24, 0x1

    .line 764
    .local v24, "hasOrd":Z
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v84, v0

    add-long v60, v82, v84

    .line 766
    .local v60, "termCountStart":J
    const/16 v35, 0x0

    .line 768
    .local v35, "lastTerm":Lorg/apache/lucene/util/BytesRef;
    invoke-virtual/range {v57 .. v57}, Lorg/apache/lucene/index/Terms;->getComparator()Ljava/util/Comparator;

    move-result-object v56

    .line 770
    .local v56, "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    const-wide/16 v54, 0x0

    .line 771
    .local v54, "sumTotalTermFreq":J
    const-wide/16 v52, 0x0

    .line 772
    .local v52, "sumDocFreq":J
    new-instance v78, Lorg/apache/lucene/util/FixedBitSet;

    move-object/from16 v0, v78

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 775
    .local v78, "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    :cond_7
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v51

    .line 776
    .local v51, "term":Lorg/apache/lucene/util/BytesRef;
    if-nez v51, :cond_9

    .line 1027
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v20

    .line 1028
    .local v20, "fieldTerms":Lorg/apache/lucene/index/Terms;
    if-eqz v20, :cond_2

    .line 1036
    move-object/from16 v0, v20

    instance-of v0, v0, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    move/from16 v79, v0

    if-eqz v79, :cond_37

    move-object/from16 v79, v20

    .line 1037
    check-cast v79, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;

    invoke-virtual/range {v79 .. v79}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$FieldReader;->computeStats()Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;

    move-result-object v49

    .line 1038
    .local v49, "stats":Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    sget-boolean v79, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v79, :cond_35

    if-nez v49, :cond_35

    new-instance v79, Ljava/lang/AssertionError;

    invoke-direct/range {v79 .. v79}, Ljava/lang/AssertionError;-><init>()V

    throw v79

    .line 759
    .end local v20    # "fieldTerms":Lorg/apache/lucene/index/Terms;
    .end local v22    # "hasFreqs":Z
    .end local v24    # "hasOrd":Z
    .end local v35    # "lastTerm":Lorg/apache/lucene/util/BytesRef;
    .end local v49    # "stats":Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    .end local v51    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v52    # "sumDocFreq":J
    .end local v54    # "sumTotalTermFreq":J
    .end local v56    # "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v60    # "termCountStart":J
    .end local v62    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v78    # "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    :cond_8
    const/16 v22, 0x1

    goto :goto_0

    .line 780
    .restart local v22    # "hasFreqs":Z
    .restart local v24    # "hasOrd":Z
    .restart local v35    # "lastTerm":Lorg/apache/lucene/util/BytesRef;
    .restart local v51    # "term":Lorg/apache/lucene/util/BytesRef;
    .restart local v52    # "sumDocFreq":J
    .restart local v54    # "sumTotalTermFreq":J
    .restart local v56    # "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    .restart local v60    # "termCountStart":J
    .restart local v62    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .restart local v78    # "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    :cond_9
    sget-boolean v79, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v79, :cond_a

    invoke-virtual/range {v51 .. v51}, Lorg/apache/lucene/util/BytesRef;->isValid()Z

    move-result v79

    if-nez v79, :cond_a

    new-instance v79, Ljava/lang/AssertionError;

    invoke-direct/range {v79 .. v79}, Ljava/lang/AssertionError;-><init>()V

    throw v79

    .line 784
    :cond_a
    if-nez v35, :cond_b

    .line 785
    invoke-static/range {v51 .. v51}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v35

    .line 793
    :goto_1
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v8

    .line 794
    .local v8, "docFreq":I
    if-gtz v8, :cond_d

    .line 795
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "docfreq: "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 787
    .end local v8    # "docFreq":I
    :cond_b
    move-object/from16 v0, v56

    move-object/from16 v1, v35

    move-object/from16 v2, v51

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v79

    if-ltz v79, :cond_c

    .line 788
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "terms out of order: lastTerm="

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " term="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 790
    :cond_c
    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_1

    .line 797
    .restart local v8    # "docFreq":I
    :cond_d
    int-to-long v0, v8

    move-wide/from16 v82, v0

    add-long v52, v52, v82

    .line 799
    move-object/from16 v0, v62

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v10

    .line 800
    move-object/from16 v0, v62

    move-object/from16 v1, p1

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v44

    .line 802
    if-eqz v24, :cond_e

    .line 803
    const-wide/16 v38, -0x1

    .line 805
    .local v38, "ord":J
    :try_start_0
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->ord()J
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v38

    .line 810
    :goto_2
    if-eqz v24, :cond_e

    .line 811
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v84, v0

    add-long v82, v82, v84

    sub-long v40, v82, v60

    .line 812
    .local v40, "ordExpected":J
    cmp-long v79, v38, v40

    if-eqz v79, :cond_e

    .line 813
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "ord mismatch: TermsEnum has ord="

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " vs actual="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 806
    .end local v40    # "ordExpected":J
    :catch_0
    move-exception v63

    .line 807
    .local v63, "uoe":Ljava/lang/UnsupportedOperationException;
    const/16 v24, 0x0

    goto :goto_2

    .line 819
    .end local v38    # "ord":J
    .end local v63    # "uoe":Ljava/lang/UnsupportedOperationException;
    :cond_e
    if-eqz v44, :cond_11

    .line 820
    move-object/from16 v11, v44

    .line 825
    .local v11, "docs2":Lorg/apache/lucene/index/DocsEnum;
    :goto_3
    const/16 v30, -0x1

    .line 826
    .local v30, "lastDoc":I
    const/4 v7, 0x0

    .line 827
    .local v7, "docCount":I
    const-wide/16 v70, 0x0

    .line 829
    .local v70, "totalTermFreq":J
    :cond_f
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v6

    .line 830
    .local v6, "doc":I
    const v79, 0x7fffffff

    move/from16 v0, v79

    if-ne v6, v0, :cond_12

    .line 900
    if-eqz v7, :cond_20

    .line 901
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v82, v0

    const-wide/16 v84, 0x1

    add-long v82, v82, v84

    move-wide/from16 v0, v82

    move-object/from16 v2, v50

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    .line 906
    :goto_4
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v72

    .line 907
    .local v72, "totalTermFreq2":J
    if-eqz v22, :cond_21

    const-wide/16 v82, -0x1

    cmp-long v79, v72, v82

    if-eqz v79, :cond_21

    const/16 v26, 0x1

    .line 910
    .local v26, "hasTotalTermFreq":Z
    :goto_5
    if-eqz p1, :cond_10

    .line 911
    if-eqz v22, :cond_23

    .line 912
    const/16 v79, 0x0

    move-object/from16 v0, v62

    move-object/from16 v1, v79

    invoke-virtual {v0, v1, v12}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v13

    .line 913
    .local v13, "docsNoDel":Lorg/apache/lucene/index/DocsEnum;
    const/4 v7, 0x0

    .line 914
    const-wide/16 v70, 0x0

    .line 915
    :goto_6
    invoke-virtual {v13}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v79

    const v81, 0x7fffffff

    move/from16 v0, v79

    move/from16 v1, v81

    if-ne v0, v1, :cond_22

    .line 931
    .end local v13    # "docsNoDel":Lorg/apache/lucene/index/DocsEnum;
    :cond_10
    if-eq v7, v8, :cond_24

    .line 932
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " docFreq="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != tot docs w/o deletions "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 822
    .end local v6    # "doc":I
    .end local v7    # "docCount":I
    .end local v11    # "docs2":Lorg/apache/lucene/index/DocsEnum;
    .end local v26    # "hasTotalTermFreq":Z
    .end local v30    # "lastDoc":I
    .end local v70    # "totalTermFreq":J
    .end local v72    # "totalTermFreq2":J
    :cond_11
    move-object v11, v10

    .restart local v11    # "docs2":Lorg/apache/lucene/index/DocsEnum;
    goto/16 :goto_3

    .line 833
    .restart local v6    # "doc":I
    .restart local v7    # "docCount":I
    .restart local v30    # "lastDoc":I
    .restart local v70    # "totalTermFreq":J
    :cond_12
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    move-wide/from16 v82, v0

    const-wide/16 v84, 0x1

    add-long v82, v82, v84

    move-wide/from16 v0, v82

    move-object/from16 v2, v50

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    .line 834
    move-object/from16 v0, v78

    invoke-virtual {v0, v6}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 835
    const/16 v21, -0x1

    .line 836
    .local v21, "freq":I
    if-eqz v22, :cond_14

    .line 837
    invoke-virtual {v11}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v21

    .line 838
    if-gtz v21, :cond_13

    .line 839
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": freq "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 841
    :cond_13
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    move-wide/from16 v82, v0

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v84, v0

    add-long v82, v82, v84

    move-wide/from16 v0, v82

    move-object/from16 v2, v50

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    .line 842
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v82, v0

    add-long v70, v70, v82

    .line 844
    :cond_14
    add-int/lit8 v7, v7, 0x1

    .line 846
    move/from16 v0, v30

    if-gt v6, v0, :cond_15

    .line 847
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " <= lastDoc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 849
    :cond_15
    move/from16 v0, p2

    if-lt v6, v0, :cond_16

    .line 850
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " >= maxDoc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 853
    :cond_16
    move/from16 v30, v6

    .line 855
    const/16 v33, -0x1

    .line 856
    .local v33, "lastPos":I
    const/16 v32, 0x0

    .line 857
    .local v32, "lastOffset":I
    if-eqz v25, :cond_f

    .line 858
    const/16 v29, 0x0

    .local v29, "j":I
    :goto_7
    move/from16 v0, v29

    move/from16 v1, v21

    if-ge v0, v1, :cond_f

    .line 859
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v42

    .line 861
    .local v42, "pos":I
    if-gez v42, :cond_17

    .line 862
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 864
    :cond_17
    move/from16 v0, v42

    move/from16 v1, v33

    if-ge v0, v1, :cond_18

    .line 865
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " < lastPos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 867
    :cond_18
    move/from16 v33, v42

    .line 868
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v37

    .line 869
    .local v37, "payload":Lorg/apache/lucene/util/BytesRef;
    if-eqz v37, :cond_19

    .line 870
    sget-boolean v79, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v79, :cond_19

    invoke-virtual/range {v37 .. v37}, Lorg/apache/lucene/util/BytesRef;->isValid()Z

    move-result v79

    if-nez v79, :cond_19

    new-instance v79, Ljava/lang/AssertionError;

    invoke-direct/range {v79 .. v79}, Ljava/lang/AssertionError;-><init>()V

    throw v79

    .line 872
    :cond_19
    if-eqz v37, :cond_1a

    move-object/from16 v0, v37

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v79, v0

    const/16 v81, 0x1

    move/from16 v0, v79

    move/from16 v1, v81

    if-ge v0, v1, :cond_1a

    .line 873
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " payload length is out of bounds "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v37

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v81, v0

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 875
    :cond_1a
    if-eqz v23, :cond_1f

    .line 876
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v48

    .line 877
    .local v48, "startOffset":I
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v14

    .line 880
    .local v14, "endOffset":I
    if-nez p5, :cond_1e

    .line 881
    if-gez v48, :cond_1b

    .line 882
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": startOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 884
    :cond_1b
    move/from16 v0, v48

    move/from16 v1, v32

    if-ge v0, v1, :cond_1c

    .line 885
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": startOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " < lastStartOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 887
    :cond_1c
    if-gez v14, :cond_1d

    .line 888
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": endOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 890
    :cond_1d
    move/from16 v0, v48

    if-ge v14, v0, :cond_1e

    .line 891
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": endOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " < startOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 894
    :cond_1e
    move/from16 v32, v48

    .line 858
    .end local v14    # "endOffset":I
    .end local v48    # "startOffset":I
    :cond_1f
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_7

    .line 903
    .end local v21    # "freq":I
    .end local v29    # "j":I
    .end local v32    # "lastOffset":I
    .end local v33    # "lastPos":I
    .end local v37    # "payload":Lorg/apache/lucene/util/BytesRef;
    .end local v42    # "pos":I
    :cond_20
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    const-wide/16 v84, 0x1

    add-long v82, v82, v84

    move-wide/from16 v0, v82

    move-object/from16 v2, v50

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    goto/16 :goto_4

    .line 907
    .restart local v72    # "totalTermFreq2":J
    :cond_21
    const/16 v26, 0x0

    goto/16 :goto_5

    .line 916
    .restart local v13    # "docsNoDel":Lorg/apache/lucene/index/DocsEnum;
    .restart local v26    # "hasTotalTermFreq":Z
    :cond_22
    invoke-virtual {v13}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v79

    invoke-virtual/range {v78 .. v79}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 917
    add-int/lit8 v7, v7, 0x1

    .line 918
    invoke-virtual {v13}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v79

    move/from16 v0, v79

    int-to-long v0, v0

    move-wide/from16 v82, v0

    add-long v70, v70, v82

    goto/16 :goto_6

    .line 921
    .end local v13    # "docsNoDel":Lorg/apache/lucene/index/DocsEnum;
    :cond_23
    const/16 v79, 0x0

    const/16 v81, 0x0

    move-object/from16 v0, v62

    move-object/from16 v1, v79

    move/from16 v2, v81

    invoke-virtual {v0, v1, v10, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v13

    .line 922
    .restart local v13    # "docsNoDel":Lorg/apache/lucene/index/DocsEnum;
    const/4 v7, 0x0

    .line 923
    const-wide/16 v70, -0x1

    .line 924
    :goto_8
    invoke-virtual {v13}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v79

    const v81, 0x7fffffff

    move/from16 v0, v79

    move/from16 v1, v81

    if-eq v0, v1, :cond_10

    .line 925
    invoke-virtual {v13}, Lorg/apache/lucene/index/DocsEnum;->docID()I

    move-result v79

    invoke-virtual/range {v78 .. v79}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 926
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 934
    .end local v13    # "docsNoDel":Lorg/apache/lucene/index/DocsEnum;
    :cond_24
    if-eqz v26, :cond_26

    .line 935
    const-wide/16 v82, 0x0

    cmp-long v79, v72, v82

    if-gtz v79, :cond_25

    .line 936
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "totalTermFreq: "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-wide/from16 v1, v72

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 938
    :cond_25
    add-long v54, v54, v70

    .line 939
    cmp-long v79, v70, v72

    if-eqz v79, :cond_26

    .line 940
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " totalTermFreq="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v72

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != recomputed totalTermFreq="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v70

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 945
    :cond_26
    if-eqz v25, :cond_32

    .line 946
    const/16 v28, 0x0

    .local v28, "idx":I
    :goto_9
    const/16 v79, 0x7

    move/from16 v0, v28

    move/from16 v1, v79

    if-ge v0, v1, :cond_7

    .line 947
    add-int/lit8 v79, v28, 0x1

    move/from16 v0, v79

    int-to-long v0, v0

    move-wide/from16 v82, v0

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v84, v0

    mul-long v82, v82, v84

    const-wide/16 v84, 0x8

    div-long v82, v82, v84

    move-wide/from16 v0, v82

    long-to-int v0, v0

    move/from16 v47, v0

    .line 948
    .local v47, "skipDocID":I
    move-object/from16 v0, v62

    move-object/from16 v1, p1

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v44

    .line 949
    move-object/from16 v0, v44

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v9

    .line 950
    .local v9, "docID":I
    const v79, 0x7fffffff

    move/from16 v0, v79

    if-eq v9, v0, :cond_7

    .line 953
    move/from16 v0, v47

    if-ge v9, v0, :cond_27

    .line 954
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": advance(docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ") returned docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 956
    :cond_27
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v21

    .line 957
    .restart local v21    # "freq":I
    if-gtz v21, :cond_28

    .line 958
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "termFreq "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 960
    :cond_28
    const/16 v34, -0x1

    .line 961
    .local v34, "lastPosition":I
    const/16 v32, 0x0

    .line 962
    .restart local v32    # "lastOffset":I
    const/16 v43, 0x0

    .local v43, "posUpto":I
    :goto_a
    move/from16 v0, v43

    move/from16 v1, v21

    if-lt v0, v1, :cond_29

    .line 995
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v36

    .line 996
    .local v36, "nextDocID":I
    const v79, 0x7fffffff

    move/from16 v0, v36

    move/from16 v1, v79

    if-eq v0, v1, :cond_7

    .line 999
    move/from16 v0, v36

    if-gt v0, v9, :cond_31

    .line 1000
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": advance(docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, "), then .next() returned docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " vs prev docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 963
    .end local v36    # "nextDocID":I
    :cond_29
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v42

    .line 965
    .restart local v42    # "pos":I
    if-gez v42, :cond_2a

    .line 966
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "position "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 968
    :cond_2a
    move/from16 v0, v42

    move/from16 v1, v34

    if-ge v0, v1, :cond_2b

    .line 969
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "position "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is < lastPosition "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 971
    :cond_2b
    move/from16 v34, v42

    .line 972
    if-eqz v23, :cond_30

    .line 973
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v48

    .line 974
    .restart local v48    # "startOffset":I
    invoke-virtual/range {v44 .. v44}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v14

    .line 977
    .restart local v14    # "endOffset":I
    if-nez p5, :cond_2f

    .line 978
    if-gez v48, :cond_2c

    .line 979
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": startOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 981
    :cond_2c
    move/from16 v0, v48

    move/from16 v1, v32

    if-ge v0, v1, :cond_2d

    .line 982
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": startOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " < lastStartOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 984
    :cond_2d
    if-gez v14, :cond_2e

    .line 985
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": endOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " is out of bounds"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 987
    :cond_2e
    move/from16 v0, v48

    if-ge v14, v0, :cond_2f

    .line 988
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": doc "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": pos "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": endOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " < startOffset "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 991
    :cond_2f
    move/from16 v32, v48

    .line 962
    .end local v14    # "endOffset":I
    .end local v48    # "startOffset":I
    :cond_30
    add-int/lit8 v43, v43, 0x1

    goto/16 :goto_a

    .line 946
    .end local v42    # "pos":I
    .restart local v36    # "nextDocID":I
    :cond_31
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_9

    .line 1005
    .end local v9    # "docID":I
    .end local v21    # "freq":I
    .end local v28    # "idx":I
    .end local v32    # "lastOffset":I
    .end local v34    # "lastPosition":I
    .end local v36    # "nextDocID":I
    .end local v43    # "posUpto":I
    .end local v47    # "skipDocID":I
    :cond_32
    const/16 v28, 0x0

    .restart local v28    # "idx":I
    :goto_b
    const/16 v79, 0x7

    move/from16 v0, v28

    move/from16 v1, v79

    if-ge v0, v1, :cond_7

    .line 1006
    add-int/lit8 v79, v28, 0x1

    move/from16 v0, v79

    int-to-long v0, v0

    move-wide/from16 v82, v0

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v84, v0

    mul-long v82, v82, v84

    const-wide/16 v84, 0x8

    div-long v82, v82, v84

    move-wide/from16 v0, v82

    long-to-int v0, v0

    move/from16 v47, v0

    .line 1007
    .restart local v47    # "skipDocID":I
    const/16 v79, 0x0

    move-object/from16 v0, v62

    move-object/from16 v1, p1

    move/from16 v2, v79

    invoke-virtual {v0, v1, v10, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v10

    .line 1008
    move/from16 v0, v47

    invoke-virtual {v10, v0}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v9

    .line 1009
    .restart local v9    # "docID":I
    const v79, 0x7fffffff

    move/from16 v0, v79

    if-eq v9, v0, :cond_7

    .line 1012
    move/from16 v0, v47

    if-ge v9, v0, :cond_33

    .line 1013
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": advance(docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ") returned docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1015
    :cond_33
    invoke-virtual {v10}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v36

    .line 1016
    .restart local v36    # "nextDocID":I
    const v79, 0x7fffffff

    move/from16 v0, v36

    move/from16 v1, v79

    if-eq v0, v1, :cond_7

    .line 1019
    move/from16 v0, v36

    if-gt v0, v9, :cond_34

    .line 1020
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, ": advance(docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, "), then .next() returned docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " vs prev docID="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1005
    :cond_34
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_b

    .line 1039
    .end local v6    # "doc":I
    .end local v7    # "docCount":I
    .end local v8    # "docFreq":I
    .end local v9    # "docID":I
    .end local v11    # "docs2":Lorg/apache/lucene/index/DocsEnum;
    .end local v26    # "hasTotalTermFreq":Z
    .end local v28    # "idx":I
    .end local v30    # "lastDoc":I
    .end local v36    # "nextDocID":I
    .end local v47    # "skipDocID":I
    .end local v70    # "totalTermFreq":J
    .end local v72    # "totalTermFreq2":J
    .restart local v20    # "fieldTerms":Lorg/apache/lucene/index/Terms;
    .restart local v49    # "stats":Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    :cond_35
    move-object/from16 v0, v50

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->blockTreeStats:Ljava/util/Map;

    move-object/from16 v79, v0

    if-nez v79, :cond_36

    .line 1040
    new-instance v79, Ljava/util/HashMap;

    invoke-direct/range {v79 .. v79}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v79

    move-object/from16 v1, v50

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->blockTreeStats:Ljava/util/Map;

    .line 1042
    :cond_36
    move-object/from16 v0, v50

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->blockTreeStats:Ljava/util/Map;

    move-object/from16 v79, v0

    move-object/from16 v0, v79

    move-object/from16 v1, v17

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1045
    .end local v49    # "stats":Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;
    :cond_37
    const-wide/16 v82, 0x0

    cmp-long v79, v54, v82

    if-eqz v79, :cond_38

    .line 1046
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v79

    invoke-virtual/range {v79 .. v79}, Lorg/apache/lucene/index/Terms;->getSumTotalTermFreq()J

    move-result-wide v76

    .line 1047
    .local v76, "v":J
    const-wide/16 v82, -0x1

    cmp-long v79, v76, v82

    if-eqz v79, :cond_38

    cmp-long v79, v54, v76

    if-eqz v79, :cond_38

    .line 1048
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "sumTotalTermFreq for field "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, "="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v76

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != recomputed sumTotalTermFreq="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v54

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1052
    .end local v76    # "v":J
    :cond_38
    const-wide/16 v82, 0x0

    cmp-long v79, v52, v82

    if-eqz v79, :cond_39

    .line 1053
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v79

    invoke-virtual/range {v79 .. v79}, Lorg/apache/lucene/index/Terms;->getSumDocFreq()J

    move-result-wide v76

    .line 1054
    .restart local v76    # "v":J
    const-wide/16 v82, -0x1

    cmp-long v79, v76, v82

    if-eqz v79, :cond_39

    cmp-long v79, v52, v76

    if-eqz v79, :cond_39

    .line 1055
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "sumDocFreq for field "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, "="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v76

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != recomputed sumDocFreq="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1059
    .end local v76    # "v":J
    :cond_39
    if-eqz v20, :cond_3a

    .line 1060
    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v76

    .line 1061
    .local v76, "v":I
    const/16 v79, -0x1

    move/from16 v0, v76

    move/from16 v1, v79

    if-eq v0, v1, :cond_3a

    invoke-virtual/range {v78 .. v78}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v79

    move/from16 v0, v79

    move/from16 v1, v76

    if-eq v0, v1, :cond_3a

    .line 1062
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "docCount for field "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, "="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v76

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != recomputed docCount="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v78 .. v78}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v81

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1067
    .end local v76    # "v":I
    :cond_3a
    if-eqz v35, :cond_3d

    .line 1068
    move-object/from16 v0, v62

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v79

    sget-object v81, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-object/from16 v0, v79

    move-object/from16 v1, v81

    if-eq v0, v1, :cond_3b

    .line 1069
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "seek to last term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " failed"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1072
    :cond_3b
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v16

    .line 1073
    .local v16, "expectedDocFreq":I
    const/16 v79, 0x0

    const/16 v81, 0x0

    const/16 v82, 0x0

    move-object/from16 v0, v62

    move-object/from16 v1, v79

    move-object/from16 v2, v81

    move/from16 v3, v82

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v5

    .line 1074
    .local v5, "d":Lorg/apache/lucene/index/DocsEnum;
    const/4 v8, 0x0

    .line 1075
    .restart local v8    # "docFreq":I
    :goto_c
    invoke-virtual {v5}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v79

    const v81, 0x7fffffff

    move/from16 v0, v79

    move/from16 v1, v81

    if-ne v0, v1, :cond_3c

    .line 1078
    move/from16 v0, v16

    if-eq v8, v0, :cond_3d

    .line 1079
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "docFreq for last term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, "="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != recomputed docFreq="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1076
    :cond_3c
    add-int/lit8 v8, v8, 0x1

    goto :goto_c

    .line 1084
    .end local v5    # "d":Lorg/apache/lucene/index/DocsEnum;
    .end local v8    # "docFreq":I
    .end local v16    # "expectedDocFreq":I
    :cond_3d
    const-wide/16 v58, -0x1

    .line 1086
    .local v58, "termCount":J
    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v84, v0

    add-long v82, v82, v84

    sub-long v82, v82, v60

    const-wide/16 v84, 0x0

    cmp-long v79, v82, v84

    if-lez v79, :cond_3e

    .line 1087
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v79

    invoke-virtual/range {v79 .. v79}, Lorg/apache/lucene/index/Terms;->size()J

    move-result-wide v58

    .line 1089
    const-wide/16 v82, -0x1

    cmp-long v79, v58, v82

    if-eqz v79, :cond_3e

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v84, v0

    add-long v82, v82, v84

    sub-long v82, v82, v60

    cmp-long v79, v58, v82

    if-eqz v79, :cond_3e

    .line 1090
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "termCount mismatch "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    add-long v82, v82, v58

    move-object/from16 v0, v80

    move-wide/from16 v1, v82

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " vs "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v82, v0

    sub-long v82, v82, v60

    move-object/from16 v0, v80

    move-wide/from16 v1, v82

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1095
    :cond_3e
    if-eqz v24, :cond_2

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v82, v0

    sub-long v82, v82, v60

    const-wide/16 v84, 0x0

    cmp-long v79, v82, v84

    if-lez v79, :cond_2

    .line 1096
    const-wide/16 v82, 0x2710

    move-wide/from16 v0, v82

    move-wide/from16 v2, v58

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v82

    move-wide/from16 v0, v82

    long-to-int v0, v0

    move/from16 v45, v0

    .line 1097
    .local v45, "seekCount":I
    if-lez v45, :cond_2

    .line 1098
    move/from16 v0, v45

    new-array v0, v0, [Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v46, v0

    .line 1101
    .local v46, "seekTerms":[Lorg/apache/lucene/util/BytesRef;
    add-int/lit8 v27, v45, -0x1

    .local v27, "i":I
    :goto_d
    if-gez v27, :cond_3f

    .line 1108
    const-wide/16 v64, 0x0

    .line 1109
    .local v64, "totDocCount":J
    add-int/lit8 v27, v45, -0x1

    :goto_e
    if-gez v27, :cond_40

    .line 1124
    const-wide/16 v66, 0x0

    .line 1125
    .local v66, "totDocCountNoDeletes":J
    const-wide/16 v68, 0x0

    .line 1126
    .local v68, "totDocFreq":J
    const/16 v27, 0x0

    :goto_f
    move/from16 v0, v27

    move/from16 v1, v45

    if-lt v0, v1, :cond_44

    .line 1142
    cmp-long v79, v64, v66

    if-lez v79, :cond_48

    .line 1143
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "more postings with deletes="

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-wide/from16 v1, v64

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " than without="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v66

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1102
    .end local v64    # "totDocCount":J
    .end local v66    # "totDocCountNoDeletes":J
    .end local v68    # "totDocFreq":J
    :cond_3f
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v82, v0

    move/from16 v0, v45

    int-to-long v0, v0

    move-wide/from16 v84, v0

    div-long v84, v58, v84

    mul-long v38, v82, v84

    .line 1103
    .restart local v38    # "ord":J
    move-object/from16 v0, v62

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(J)V

    .line 1104
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v79

    invoke-static/range {v79 .. v79}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v79

    aput-object v79, v46, v27

    .line 1101
    add-int/lit8 v27, v27, -0x1

    goto :goto_d

    .line 1110
    .end local v38    # "ord":J
    .restart local v64    # "totDocCount":J
    :cond_40
    aget-object v79, v46, v27

    move-object/from16 v0, v62

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v79

    sget-object v81, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-object/from16 v0, v79

    move-object/from16 v1, v81

    if-eq v0, v1, :cond_41

    .line 1111
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "seek to existing term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v81, v46, v27

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " failed"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1114
    :cond_41
    const/16 v79, 0x0

    move-object/from16 v0, v62

    move-object/from16 v1, p1

    move/from16 v2, v79

    invoke-virtual {v0, v1, v10, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v10

    .line 1115
    if-nez v10, :cond_43

    .line 1116
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "null DocsEnum from to existing term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v81, v46, v27

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1120
    :cond_42
    const-wide/16 v82, 0x1

    add-long v64, v64, v82

    .line 1119
    :cond_43
    invoke-virtual {v10}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v79

    const v81, 0x7fffffff

    move/from16 v0, v79

    move/from16 v1, v81

    if-ne v0, v1, :cond_42

    .line 1109
    add-int/lit8 v27, v27, -0x1

    goto/16 :goto_e

    .line 1127
    .restart local v66    # "totDocCountNoDeletes":J
    .restart local v68    # "totDocFreq":J
    :cond_44
    aget-object v79, v46, v27

    const/16 v81, 0x1

    move-object/from16 v0, v62

    move-object/from16 v1, v79

    move/from16 v2, v81

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v79

    if-nez v79, :cond_45

    .line 1128
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "seek to existing term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v81, v46, v27

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " failed"

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1131
    :cond_45
    invoke-virtual/range {v62 .. v62}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v79

    move/from16 v0, v79

    int-to-long v0, v0

    move-wide/from16 v82, v0

    add-long v68, v68, v82

    .line 1132
    const/16 v79, 0x0

    const/16 v81, 0x0

    move-object/from16 v0, v62

    move-object/from16 v1, v79

    move/from16 v2, v81

    invoke-virtual {v0, v1, v10, v2}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v10

    .line 1133
    if-nez v10, :cond_47

    .line 1134
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "null DocsEnum from to existing term "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v81, v46, v27

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1138
    :cond_46
    const-wide/16 v82, 0x1

    add-long v66, v66, v82

    .line 1137
    :cond_47
    invoke-virtual {v10}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v79

    const v81, 0x7fffffff

    move/from16 v0, v79

    move/from16 v1, v81

    if-ne v0, v1, :cond_46

    .line 1126
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_f

    .line 1146
    :cond_48
    cmp-long v79, v66, v68

    if-eqz v79, :cond_2

    .line 1147
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "docfreqs="

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-wide/from16 v1, v68

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " != recomputed docfreqs="

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    move-wide/from16 v1, v66

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1160
    .end local v17    # "field":Ljava/lang/String;
    .end local v19    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v20    # "fieldTerms":Lorg/apache/lucene/index/Terms;
    .end local v22    # "hasFreqs":Z
    .end local v23    # "hasOffsets":Z
    .end local v24    # "hasOrd":Z
    .end local v25    # "hasPositions":Z
    .end local v27    # "i":I
    .end local v35    # "lastTerm":Lorg/apache/lucene/util/BytesRef;
    .end local v45    # "seekCount":I
    .end local v46    # "seekTerms":[Lorg/apache/lucene/util/BytesRef;
    .end local v51    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v52    # "sumDocFreq":J
    .end local v54    # "sumTotalTermFreq":J
    .end local v56    # "termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v57    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v58    # "termCount":J
    .end local v60    # "termCountStart":J
    .end local v62    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v64    # "totDocCount":J
    .end local v66    # "totDocCountNoDeletes":J
    .end local v68    # "totDocFreq":J
    .end local v78    # "visitedDocs":Lorg/apache/lucene/util/FixedBitSet;
    .restart local v18    # "fieldCount":I
    :cond_49
    move/from16 v0, v18

    if-eq v0, v4, :cond_4a

    .line 1161
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "fieldCount mismatch "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " vs recomputed field count "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v80

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1168
    :cond_4a
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/Fields;->getUniqueTermCount()J

    move-result-wide v74

    .line 1170
    .local v74, "uniqueTermCountAllFields":J
    const-wide/16 v80, -0x1

    cmp-long v79, v74, v80

    if-eqz v79, :cond_4b

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v80, v0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v82, v0

    add-long v80, v80, v82

    cmp-long v79, v80, v74

    if-eqz v79, :cond_4b

    .line 1171
    new-instance v79, Ljava/lang/RuntimeException;

    new-instance v80, Ljava/lang/StringBuilder;

    const-string v81, "termCount mismatch "

    invoke-direct/range {v80 .. v81}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v80

    move-wide/from16 v1, v74

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    const-string v81, " vs "

    invoke-virtual/range {v80 .. v81}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v80

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v82, v0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    move-wide/from16 v84, v0

    add-long v82, v82, v84

    move-object/from16 v0, v80

    move-wide/from16 v1, v82

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v80

    invoke-virtual/range {v80 .. v80}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v80

    invoke-direct/range {v79 .. v80}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v79

    .line 1174
    :cond_4b
    if-eqz p4, :cond_4c

    .line 1175
    new-instance v79, Ljava/lang/StringBuilder;

    const-string v80, "OK ["

    invoke-direct/range {v79 .. v80}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v80, v0

    invoke-virtual/range {v79 .. v81}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v79

    const-string v80, " terms; "

    invoke-virtual/range {v79 .. v80}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v79

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    move-wide/from16 v80, v0

    invoke-virtual/range {v79 .. v81}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v79

    const-string v80, " terms/docs pairs; "

    invoke-virtual/range {v79 .. v80}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v79

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    move-wide/from16 v80, v0

    invoke-virtual/range {v79 .. v81}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v79

    const-string v80, " tokens]"

    invoke-virtual/range {v79 .. v80}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v79

    invoke-virtual/range {v79 .. v79}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v79

    move-object/from16 v0, p6

    move-object/from16 v1, v79

    invoke-static {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1178
    :cond_4c
    if-eqz p7, :cond_0

    move-object/from16 v0, v50

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->blockTreeStats:Ljava/util/Map;

    move-object/from16 v79, v0

    if-eqz v79, :cond_0

    if-eqz p6, :cond_0

    move-object/from16 v0, v50

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    move-wide/from16 v80, v0

    const-wide/16 v82, 0x0

    cmp-long v79, v80, v82

    if-lez v79, :cond_0

    .line 1179
    move-object/from16 v0, v50

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->blockTreeStats:Ljava/util/Map;

    move-object/from16 v79, v0

    invoke-interface/range {v79 .. v79}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v79

    invoke-interface/range {v79 .. v79}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v80

    :goto_10
    invoke-interface/range {v80 .. v80}, Ljava/util/Iterator;->hasNext()Z

    move-result v79

    if-eqz v79, :cond_0

    invoke-interface/range {v80 .. v80}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Map$Entry;

    .line 1180
    .local v15, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;>;"
    new-instance v81, Ljava/lang/StringBuilder;

    const-string v79, "      field \""

    move-object/from16 v0, v81

    move-object/from16 v1, v79

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v15}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v79

    check-cast v79, Ljava/lang/String;

    move-object/from16 v0, v81

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v79

    const-string v81, "\":"

    move-object/from16 v0, v79

    move-object/from16 v1, v81

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v79

    invoke-virtual/range {v79 .. v79}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v79

    move-object/from16 v0, p6

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1181
    new-instance v81, Ljava/lang/StringBuilder;

    const-string v79, "      "

    move-object/from16 v0, v81

    move-object/from16 v1, v79

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v15}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v79

    check-cast v79, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;

    invoke-virtual/range {v79 .. v79}, Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;->toString()Ljava/lang/String;

    move-result-object v79

    const-string v82, "\n"

    const-string v83, "\n      "

    move-object/from16 v0, v79

    move-object/from16 v1, v82

    move-object/from16 v2, v83

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v79

    move-object/from16 v0, v81

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v79

    invoke-virtual/range {v79 .. v79}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v79

    move-object/from16 v0, p6

    move-object/from16 v1, v79

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_10
.end method

.method private static checkNorms(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)V
    .locals 3
    .param p0, "fi"    # Lorg/apache/lucene/index/FieldInfo;
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "infoStream"    # Ljava/io/PrintStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1441
    invoke-static {}, Lorg/apache/lucene/index/CheckIndex;->$SWITCH_TABLE$org$apache$lucene$index$FieldInfo$DocValuesType()[I

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1446
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "wtf: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/FieldInfo;->getNormType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 1443
    :pswitch_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lorg/apache/lucene/index/CheckIndex;->checkNumericDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/NumericDocValues;)V

    .line 1448
    return-void

    .line 1441
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static checkNumericDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 2
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "ndv"    # Lorg/apache/lucene/index/NumericDocValues;

    .prologue
    .line 1396
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 1399
    return-void

    .line 1397
    :cond_0
    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    .line 1396
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static checkSortedDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/SortedDocValues;)V
    .locals 10
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "dv"    # Lorg/apache/lucene/index/SortedDocValues;

    .prologue
    .line 1322
    invoke-static {p0, p1, p2}, Lorg/apache/lucene/index/CheckIndex;->checkBinaryDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/BinaryDocValues;)V

    .line 1323
    invoke-virtual {p2}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v7

    add-int/lit8 v2, v7, -0x1

    .line 1324
    .local v2, "maxOrd":I
    new-instance v6, Lorg/apache/lucene/util/FixedBitSet;

    invoke-virtual {p2}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v7

    invoke-direct {v6, v7}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 1325
    .local v6, "seenOrds":Lorg/apache/lucene/util/FixedBitSet;
    const/4 v3, -0x1

    .line 1326
    .local v3, "maxOrd2":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v7

    if-lt v0, v7, :cond_0

    .line 1334
    if-eq v2, v3, :cond_3

    .line 1335
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "dv for field: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " reports wrong maxOrd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " but this is not the case: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1327
    :cond_0
    invoke-virtual {p2, v0}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v4

    .line 1328
    .local v4, "ord":I
    if-ltz v4, :cond_1

    if-le v4, v2, :cond_2

    .line 1329
    :cond_1
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ord out of bounds: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1331
    :cond_2
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1332
    invoke-virtual {v6, v4}, Lorg/apache/lucene/util/FixedBitSet;->set(I)V

    .line 1326
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1337
    .end local v4    # "ord":I
    :cond_3
    invoke-virtual {v6}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v7

    invoke-virtual {p2}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v8

    if-eq v7, v8, :cond_4

    .line 1338
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "dv for field: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has holes in its ords, valueCount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " but only used: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lorg/apache/lucene/util/FixedBitSet;->cardinality()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1340
    :cond_4
    const/4 v1, 0x0

    .line 1341
    .local v1, "lastValue":Lorg/apache/lucene/util/BytesRef;
    new-instance v5, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v5}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 1342
    .local v5, "scratch":Lorg/apache/lucene/util/BytesRef;
    const/4 v0, 0x0

    :goto_1
    if-le v0, v2, :cond_5

    .line 1352
    return-void

    .line 1343
    :cond_5
    invoke-virtual {p2, v0, v5}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 1344
    sget-boolean v7, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    invoke-virtual {v5}, Lorg/apache/lucene/util/BytesRef;->isValid()Z

    move-result v7

    if-nez v7, :cond_6

    new-instance v7, Ljava/lang/AssertionError;

    invoke-direct {v7}, Ljava/lang/AssertionError;-><init>()V

    throw v7

    .line 1345
    :cond_6
    if-eqz v1, :cond_7

    .line 1346
    invoke-virtual {v5, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v7

    if-gtz v7, :cond_7

    .line 1347
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "dv for field: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has ords out of order: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " >="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1350
    :cond_7
    invoke-static {v5}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v1

    .line 1342
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static checkSortedSetDocValues(Ljava/lang/String;Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/SortedSetDocValues;)V
    .locals 22
    .param p0, "fieldName"    # Ljava/lang/String;
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "dv"    # Lorg/apache/lucene/index/SortedSetDocValues;

    .prologue
    .line 1355
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v18

    const-wide/16 v20, 0x1

    sub-long v10, v18, v20

    .line 1356
    .local v10, "maxOrd":J
    new-instance v16, Lorg/apache/lucene/util/OpenBitSet;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v18

    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/util/OpenBitSet;-><init>(J)V

    .line 1357
    .local v16, "seenOrds":Lorg/apache/lucene/util/OpenBitSet;
    const-wide/16 v12, -0x1

    .line 1358
    .local v12, "maxOrd2":J
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v17

    move/from16 v0, v17

    if-lt v4, v0, :cond_0

    .line 1374
    cmp-long v17, v10, v12

    if-eqz v17, :cond_5

    .line 1375
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "dv for field: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " reports wrong maxOrd="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " but this is not the case: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1359
    :cond_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 1360
    const-wide/16 v6, -0x1

    .line 1362
    .local v6, "lastOrd":J
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v14

    .local v14, "ord":J
    const-wide/16 v18, -0x1

    cmp-long v17, v14, v18

    if-nez v17, :cond_1

    .line 1358
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1363
    :cond_1
    cmp-long v17, v14, v6

    if-gtz v17, :cond_2

    .line 1364
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "ords out of order: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " <= "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " for doc: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1366
    :cond_2
    const-wide/16 v18, 0x0

    cmp-long v17, v14, v18

    if-ltz v17, :cond_3

    cmp-long v17, v14, v10

    if-lez v17, :cond_4

    .line 1367
    :cond_3
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "ord out of bounds: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1369
    :cond_4
    move-wide v6, v14

    .line 1370
    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    .line 1371
    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v15}, Lorg/apache/lucene/util/OpenBitSet;->set(J)V

    goto :goto_1

    .line 1377
    .end local v6    # "lastOrd":J
    .end local v14    # "ord":J
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v18

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v20

    cmp-long v17, v18, v20

    if-eqz v17, :cond_6

    .line 1378
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "dv for field: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " has holes in its ords, valueCount="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " but only used: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v16 .. v16}, Lorg/apache/lucene/util/OpenBitSet;->cardinality()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1381
    :cond_6
    const/4 v8, 0x0

    .line 1382
    .local v8, "lastValue":Lorg/apache/lucene/util/BytesRef;
    new-instance v9, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v9}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 1383
    .local v9, "scratch":Lorg/apache/lucene/util/BytesRef;
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_2
    cmp-long v17, v4, v10

    if-lez v17, :cond_7

    .line 1393
    return-void

    .line 1384
    :cond_7
    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5, v9}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 1385
    sget-boolean v17, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v17, :cond_8

    invoke-virtual {v9}, Lorg/apache/lucene/util/BytesRef;->isValid()Z

    move-result v17

    if-nez v17, :cond_8

    new-instance v17, Ljava/lang/AssertionError;

    invoke-direct/range {v17 .. v17}, Ljava/lang/AssertionError;-><init>()V

    throw v17

    .line 1386
    :cond_8
    if-eqz v8, :cond_9

    .line 1387
    invoke-virtual {v9, v8}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v17

    if-gtz v17, :cond_9

    .line 1388
    new-instance v17, Ljava/lang/RuntimeException;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "dv for field: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " has ords out of order: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " >="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 1391
    :cond_9
    invoke-static {v9}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v8

    .line 1383
    const-wide/16 v18, 0x1

    add-long v4, v4, v18

    goto :goto_2
.end method

.method public static main([Ljava/lang/String;)V
    .locals 20
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1749
    const/4 v8, 0x0

    .line 1750
    .local v8, "doFix":Z
    const/4 v7, 0x0

    .line 1751
    .local v7, "doCrossCheckTermVectors":Z
    invoke-static {}, Lorg/apache/lucene/codecs/Codec;->getDefault()Lorg/apache/lucene/codecs/Codec;

    move-result-object v4

    .line 1752
    .local v4, "codec":Lorg/apache/lucene/codecs/Codec;
    const/16 v16, 0x0

    .line 1753
    .local v16, "verbose":Z
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1754
    .local v12, "onlySegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .line 1755
    .local v11, "indexPath":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1756
    .local v6, "dirImpl":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1757
    .local v10, "i":I
    :goto_0
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v10, v0, :cond_5

    .line 1796
    if-nez v11, :cond_0

    .line 1797
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "\nERROR: index path not specified"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1798
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "\nUsage: java org.apache.lucene.index.CheckIndex pathToIndex [-fix] [-crossCheckTermVectors] [-segment X] [-segment Y] [-dir-impl X]\n\n  -fix: actually write a new segments_N file, removing any problematic segments\n  -crossCheckTermVectors: verifies that term vectors match postings; THIS IS VERY SLOW!\n  -codec X: when fixing, codec to write the new segments_N file with\n  -verbose: print additional details\n  -segment X: only check the specified segments.  This can be specified multiple\n              times, to check more than one segment, eg \'-segment _2 -segment _a\'.\n              You can\'t use this with the -fix option\n  -dir-impl X: use a specific "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1807
    const-class v19, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " implementation. "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1808
    const-string v19, "If no package is specified the "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-class v19, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " package will be used.\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1809
    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1810
    const-string v19, "**WARNING**: -fix should only be used on an emergency basis as it will cause\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1811
    const-string v19, "documents (perhaps many) to be permanently removed from the index.  Always make\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1812
    const-string v19, "a backup copy of your index before running this!  Do not run this tool on an index\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1813
    const-string v19, "that is actively being written to.  You have been warned!\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1814
    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1815
    const-string v19, "Run without -fix, this tool will open the index, report version information\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1816
    const-string v19, "and report any exceptions it hits and what action it would take if -fix were\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1817
    const-string v19, "specified.  With -fix, this tool will remove any segments that have issues and\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1818
    const-string/jumbo v19, "write a new segments_N file.  This means all documents contained in the affected\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1819
    const-string v19, "segments will be removed.\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1820
    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1821
    const-string v19, "This tool exits with exit code 1 if the index cannot be opened or has any\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1822
    const-string v19, "corruption, else 0.\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1798
    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1823
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    .line 1826
    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/CheckIndex;->assertsOn()Z

    move-result v17

    if-nez v17, :cond_1

    .line 1827
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "\nNOTE: testing will be more thorough if you run java with \'-ea:org.apache.lucene...\', so assertions are enabled"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1829
    :cond_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v17

    if-nez v17, :cond_10

    .line 1830
    const/4 v12, 0x0

    .line 1836
    :cond_2
    :goto_1
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "\nOpening index @ "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1837
    const/4 v5, 0x0

    .line 1839
    .local v5, "dir":Lorg/apache/lucene/store/Directory;
    if-nez v6, :cond_11

    .line 1840
    :try_start_0
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v17}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1850
    :goto_2
    new-instance v3, Lorg/apache/lucene/index/CheckIndex;

    invoke-direct {v3, v5}, Lorg/apache/lucene/index/CheckIndex;-><init>(Lorg/apache/lucene/store/Directory;)V

    .line 1851
    .local v3, "checker":Lorg/apache/lucene/index/CheckIndex;
    invoke-virtual {v3, v7}, Lorg/apache/lucene/index/CheckIndex;->setCrossCheckTermVectors(Z)V

    .line 1852
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v3, v0, v1}, Lorg/apache/lucene/index/CheckIndex;->setInfoStream(Ljava/io/PrintStream;Z)V

    .line 1854
    invoke-virtual {v3, v12}, Lorg/apache/lucene/index/CheckIndex;->checkIndex(Ljava/util/List;)Lorg/apache/lucene/index/CheckIndex$Status;

    move-result-object v13

    .line 1855
    .local v13, "result":Lorg/apache/lucene/index/CheckIndex$Status;
    iget-boolean v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->missingSegments:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 1856
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    .line 1859
    :cond_3
    iget-boolean v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    move/from16 v17, v0

    if-nez v17, :cond_4

    .line 1860
    if-nez v8, :cond_12

    .line 1861
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "WARNING: would write new segments file, and "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " documents would be lost, if -fix were specified\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1875
    :cond_4
    :goto_3
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1878
    iget-boolean v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    move/from16 v17, v0

    if-eqz v17, :cond_14

    .line 1879
    const/4 v9, 0x0

    .line 1882
    .local v9, "exitCode":I
    :goto_4
    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    .line 1883
    return-void

    .line 1758
    .end local v3    # "checker":Lorg/apache/lucene/index/CheckIndex;
    .end local v5    # "dir":Lorg/apache/lucene/store/Directory;
    .end local v9    # "exitCode":I
    .end local v13    # "result":Lorg/apache/lucene/index/CheckIndex$Status;
    :cond_5
    aget-object v2, p0, v10

    .line 1759
    .local v2, "arg":Ljava/lang/String;
    const-string v17, "-fix"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1760
    const/4 v8, 0x1

    .line 1793
    :goto_5
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 1761
    :cond_6
    const-string v17, "-crossCheckTermVectors"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1762
    const/4 v7, 0x1

    .line 1763
    goto :goto_5

    :cond_7
    const-string v17, "-codec"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1764
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ne v10, v0, :cond_8

    .line 1765
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "ERROR: missing name for -codec option"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1766
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    .line 1768
    :cond_8
    add-int/lit8 v10, v10, 0x1

    .line 1769
    aget-object v17, p0, v10

    invoke-static/range {v17 .. v17}, Lorg/apache/lucene/codecs/Codec;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/Codec;

    move-result-object v4

    .line 1770
    goto :goto_5

    :cond_9
    const-string v17, "-verbose"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1771
    const/16 v16, 0x1

    .line 1772
    goto :goto_5

    :cond_a
    const-string v17, "-segment"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 1773
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ne v10, v0, :cond_b

    .line 1774
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "ERROR: missing name for -segment option"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1775
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    .line 1777
    :cond_b
    add-int/lit8 v10, v10, 0x1

    .line 1778
    aget-object v17, p0, v10

    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1779
    :cond_c
    const-string v17, "-dir-impl"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 1780
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ne v10, v0, :cond_d

    .line 1781
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "ERROR: missing value for -dir-impl option"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1782
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    .line 1784
    :cond_d
    add-int/lit8 v10, v10, 0x1

    .line 1785
    aget-object v6, p0, v10

    .line 1786
    goto/16 :goto_5

    .line 1787
    :cond_e
    if-eqz v11, :cond_f

    .line 1788
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "ERROR: unexpected extra argument \'"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v19, p0, v10

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\'"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1789
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    .line 1791
    :cond_f
    aget-object v11, p0, v10

    goto/16 :goto_5

    .line 1831
    .end local v2    # "arg":Ljava/lang/String;
    :cond_10
    if-eqz v8, :cond_2

    .line 1832
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "ERROR: cannot specify both -fix and -segment"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1833
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_1

    .line 1842
    .restart local v5    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_11
    :try_start_1
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-static {v6, v0}, Lorg/apache/lucene/util/CommandLineUtil;->newFSDirectory(Ljava/lang/String;Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto/16 :goto_2

    .line 1844
    :catch_0
    move-exception v15

    .line 1845
    .local v15, "t":Ljava/lang/Throwable;
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "ERROR: could not open directory \""

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\"; exiting"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1846
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 1847
    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_2

    .line 1863
    .end local v15    # "t":Ljava/lang/Throwable;
    .restart local v3    # "checker":Lorg/apache/lucene/index/CheckIndex;
    .restart local v13    # "result":Lorg/apache/lucene/index/CheckIndex$Status;
    :cond_12
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "WARNING: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " documents will be lost\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1864
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "NOTE: will write new segments file in 5 seconds; this will remove "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " docs from the index. THIS IS YOUR LAST CHANCE TO CTRL+C!"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1865
    const/4 v14, 0x0

    .local v14, "s":I
    :goto_6
    const/16 v17, 0x5

    move/from16 v0, v17

    if-lt v14, v0, :cond_13

    .line 1869
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "Writing..."

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1870
    invoke-virtual {v3, v13, v4}, Lorg/apache/lucene/index/CheckIndex;->fixIndex(Lorg/apache/lucene/index/CheckIndex$Status;Lorg/apache/lucene/codecs/Codec;)V

    .line 1871
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v18, "OK"

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1872
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Wrote new segments file \""

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v13, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1866
    :cond_13
    const-wide/16 v18, 0x3e8

    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V

    .line 1867
    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "  "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rsub-int/lit8 v19, v14, 0x5

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "..."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1865
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 1881
    .end local v14    # "s":I
    :cond_14
    const/4 v9, 0x1

    .restart local v9    # "exitCode":I
    goto/16 :goto_4
.end method

.method private static msg(Ljava/io/PrintStream;Ljava/lang/String;)V
    .locals 0
    .param p0, "out"    # Ljava/io/PrintStream;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 347
    if-eqz p0, :cond_0

    .line 348
    invoke-virtual {p0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 349
    :cond_0
    return-void
.end method

.method private static testAsserts()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1706
    sput-boolean v0, Lorg/apache/lucene/index/CheckIndex;->assertsOn:Z

    .line 1707
    return v0
.end method

.method public static testDocValues(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;
    .locals 8
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 1283
    new-instance v2, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;

    invoke-direct {v2}, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;-><init>()V

    .line 1285
    .local v2, "status":Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;
    if-eqz p1, :cond_0

    .line 1286
    :try_start_0
    const-string v3, "    test: docvalues..........."

    invoke-virtual {p1, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1288
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1302
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OK ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;->docCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " total doc count; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;->totalValueFields:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " docvalues fields]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1310
    :cond_2
    :goto_1
    return-object v2

    .line 1288
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FieldInfo;

    .line 1289
    .local v1, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1290
    iget-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;->totalValueFields:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;->totalValueFields:J

    .line 1291
    invoke-static {v1, p0, p1}, Lorg/apache/lucene/index/CheckIndex;->checkDocValues(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1303
    .end local v1    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    :catch_0
    move-exception v0

    .line 1304
    .local v0, "e":Ljava/lang/Throwable;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ERROR ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1305
    iput-object v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;->error:Ljava/lang/Throwable;

    .line 1306
    if-eqz p1, :cond_2

    .line 1307
    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_1

    .line 1293
    .end local v0    # "e":Ljava/lang/Throwable;
    .restart local v1    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    :cond_4
    :try_start_1
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->getBinaryDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;

    move-result-object v4

    if-nez v4, :cond_5

    .line 1294
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->getNumericDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v4

    if-nez v4, :cond_5

    .line 1295
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->getSortedDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedDocValues;

    move-result-object v4

    if-nez v4, :cond_5

    .line 1296
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->getSortedSetDocValues(Ljava/lang/String;)Lorg/apache/lucene/index/SortedSetDocValues;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1297
    :cond_5
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "field: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has docvalues but should omit them!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public static testFieldNorms(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;
    .locals 8
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 676
    new-instance v2, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;-><init>(Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;)V

    .line 680
    .local v2, "status":Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;
    if-eqz p1, :cond_0

    .line 681
    :try_start_0
    const-string v3, "    test: field norms........."

    invoke-virtual {p1, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 683
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 696
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OK ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->totFields:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fields]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 705
    :cond_2
    :goto_1
    return-object v2

    .line 683
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/FieldInfo;

    .line 684
    .local v1, "info":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v1}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 685
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->hasNorms(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 697
    .end local v1    # "info":Lorg/apache/lucene/index/FieldInfo;
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Ljava/lang/Throwable;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ERROR ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 699
    iput-object v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->error:Ljava/lang/Throwable;

    .line 700
    if-eqz p1, :cond_2

    .line 701
    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_1

    .line 686
    .end local v0    # "e":Ljava/lang/Throwable;
    .restart local v1    # "info":Lorg/apache/lucene/index/FieldInfo;
    :cond_4
    :try_start_1
    invoke-static {v1, p0, p1}, Lorg/apache/lucene/index/CheckIndex;->checkNorms(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)V

    .line 687
    iget-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->totFields:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->totFields:J

    goto :goto_0

    .line 689
    :cond_5
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_6

    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->hasNorms(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 690
    :cond_6
    iget-object v4, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/AtomicReader;->getNormValues(Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 691
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "field: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " should omit norms but has them!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public static testPostings(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    .locals 1
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 1193
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/lucene/index/CheckIndex;->testPostings(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    move-result-object v0

    return-object v0
.end method

.method public static testPostings(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    .locals 14
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;
    .param p2, "verbose"    # Z

    .prologue
    .line 1206
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v2

    .line 1207
    .local v2, "maxDoc":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v1

    .line 1210
    .local v1, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-eqz p1, :cond_0

    .line 1211
    :try_start_0
    const-string v4, "    test: terms, freq, prox..."

    invoke-virtual {p1, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1214
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 1215
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    .line 1216
    .local v3, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v6, p1

    move/from16 v7, p2

    invoke-static/range {v0 .. v7}, Lorg/apache/lucene/index/CheckIndex;->checkFields(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/util/Bits;ILorg/apache/lucene/index/FieldInfos;ZZLjava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    move-result-object v13

    .line 1217
    .local v13, "status":Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    if-eqz v1, :cond_2

    .line 1218
    if-eqz p1, :cond_1

    .line 1219
    const-string v4, "    test (ignoring deletes): terms, freq, prox..."

    invoke-virtual {p1, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1221
    :cond_1
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v4, v0

    move v6, v2

    move-object v7, v3

    move-object v10, p1

    move/from16 v11, p2

    invoke-static/range {v4 .. v11}, Lorg/apache/lucene/index/CheckIndex;->checkFields(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/util/Bits;ILorg/apache/lucene/index/FieldInfos;ZZLjava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1232
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    .end local v3    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    :cond_2
    :goto_0
    return-object v13

    .line 1223
    .end local v13    # "status":Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    :catch_0
    move-exception v12

    .line 1224
    .local v12, "e":Ljava/lang/Throwable;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ERROR: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1225
    new-instance v13, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    invoke-direct {v13}, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;-><init>()V

    .line 1226
    .restart local v13    # "status":Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
    iput-object v12, v13, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->error:Ljava/lang/Throwable;

    .line 1227
    if-eqz p1, :cond_2

    .line 1228
    invoke-virtual {v12, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method

.method public static testStoredFields(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;
    .locals 10
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 1240
    new-instance v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    invoke-direct {v4}, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;-><init>()V

    .line 1243
    .local v4, "status":Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;
    if-eqz p1, :cond_0

    .line 1244
    :try_start_0
    const-string v5, "    test: stored fields......."

    invoke-virtual {p1, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1248
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v3

    .line 1249
    .local v3, "liveDocs":Lorg/apache/lucene/util/Bits;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v5

    if-lt v2, v5, :cond_2

    .line 1260
    iget v5, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    invoke-virtual {p0}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v6

    if-eq v5, v6, :cond_5

    .line 1261
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "docCount="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " but saw "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " undeleted docs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1266
    .end local v2    # "j":I
    .end local v3    # "liveDocs":Lorg/apache/lucene/util/Bits;
    :catch_0
    move-exception v1

    .line 1267
    .local v1, "e":Ljava/lang/Throwable;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ERROR ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1268
    iput-object v1, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->error:Ljava/lang/Throwable;

    .line 1269
    if-eqz p1, :cond_1

    .line 1270
    invoke-virtual {v1, p1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 1274
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_1
    :goto_1
    return-object v4

    .line 1252
    .restart local v2    # "j":I
    .restart local v3    # "liveDocs":Lorg/apache/lucene/util/Bits;
    :cond_2
    :try_start_1
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/AtomicReader;->document(I)Lorg/apache/lucene/document/Document;

    move-result-object v0

    .line 1253
    .local v0, "doc":Lorg/apache/lucene/document/Document;
    if-eqz v3, :cond_3

    invoke-interface {v3, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1254
    :cond_3
    iget v5, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    .line 1255
    iget-wide v6, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    invoke-virtual {v0}, Lorg/apache/lucene/document/Document;->getFields()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    .line 1249
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1264
    .end local v0    # "doc":Lorg/apache/lucene/document/Document;
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "OK ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " total field count; avg "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 1265
    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v6}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v6

    iget-wide v8, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->totFields:J

    long-to-float v7, v8

    iget v8, v4, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->docCount:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    float-to-double v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fields per doc]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1264
    invoke-static {p1, v5}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static testTermVectors(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
    .locals 1
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    const/4 v0, 0x0

    .line 1455
    invoke-static {p0, p1, v0, v0}, Lorg/apache/lucene/index/CheckIndex;->testTermVectors(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;ZZ)Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    move-result-object v0

    return-object v0
.end method

.method public static testTermVectors(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;ZZ)Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
    .locals 47
    .param p0, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p1, "infoStream"    # Ljava/io/PrintStream;
    .param p2, "verbose"    # Z
    .param p3, "crossCheckTermVectors"    # Z

    .prologue
    .line 1463
    new-instance v40, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    invoke-direct/range {v40 .. v40}, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;-><init>()V

    .line 1464
    .local v40, "status":Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v5

    .line 1465
    .local v5, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    new-instance v23, Lorg/apache/lucene/util/FixedBitSet;

    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-direct {v0, v3}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 1468
    .local v23, "onlyDocIsDeleted":Lorg/apache/lucene/util/Bits;
    if-eqz p1, :cond_0

    .line 1469
    :try_start_0
    const-string v3, "    test: term vectors........"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1472
    :cond_0
    const/4 v13, 0x0

    .line 1473
    .local v13, "docs":Lorg/apache/lucene/index/DocsEnum;
    const/16 v26, 0x0

    .line 1476
    .local v26, "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    const/16 v27, 0x0

    .line 1477
    .local v27, "postingsDocs":Lorg/apache/lucene/index/DocsEnum;
    const/16 v35, 0x0

    .line 1479
    .local v35, "postingsPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v22

    .line 1483
    .local v22, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-eqz p3, :cond_2

    .line 1484
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v30

    .line 1489
    .local v30, "postingsFields":Lorg/apache/lucene/index/Fields;
    :goto_0
    const/16 v43, 0x0

    .line 1490
    .local v43, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/16 v38, 0x0

    .line 1492
    .local v38, "postingsTermsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v3

    move/from16 v0, v21

    if-lt v0, v3, :cond_3

    .line 1669
    move-object/from16 v0, v40

    iget v3, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    if-nez v3, :cond_23

    const/16 v45, 0x0

    .line 1670
    .local v45, "vectorAvg":F
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OK ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    iget-wide v6, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " total vector count; avg "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1671
    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v4}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    move/from16 v0, v45

    float-to-double v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " term/freq vector fields per doc]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1670
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1680
    .end local v13    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v21    # "j":I
    .end local v22    # "liveDocs":Lorg/apache/lucene/util/Bits;
    .end local v26    # "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v27    # "postingsDocs":Lorg/apache/lucene/index/DocsEnum;
    .end local v30    # "postingsFields":Lorg/apache/lucene/index/Fields;
    .end local v35    # "postingsPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v38    # "postingsTermsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v43    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v45    # "vectorAvg":F
    :cond_1
    :goto_3
    return-object v40

    .line 1486
    .restart local v13    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v22    # "liveDocs":Lorg/apache/lucene/util/Bits;
    .restart local v26    # "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .restart local v27    # "postingsDocs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v35    # "postingsPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :cond_2
    const/16 v30, 0x0

    .restart local v30    # "postingsFields":Lorg/apache/lucene/index/Fields;
    goto :goto_0

    .line 1496
    .restart local v21    # "j":I
    .restart local v38    # "postingsTermsEnum":Lorg/apache/lucene/index/TermsEnum;
    .restart local v43    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_3
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/AtomicReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v2

    .line 1501
    .local v2, "tfv":Lorg/apache/lucene/index/Fields;
    if-eqz v2, :cond_6

    .line 1503
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v8, p1

    move/from16 v9, p2

    invoke-static/range {v2 .. v9}, Lorg/apache/lucene/index/CheckIndex;->checkFields(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/util/Bits;ILorg/apache/lucene/index/FieldInfos;ZZLjava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    .line 1506
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object/from16 v3, v23

    move-object/from16 v8, p1

    move/from16 v9, p2

    invoke-static/range {v2 .. v9}, Lorg/apache/lucene/index/CheckIndex;->checkFields(Lorg/apache/lucene/index/Fields;Lorg/apache/lucene/util/Bits;ILorg/apache/lucene/index/FieldInfos;ZZLjava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    .line 1509
    if-eqz v22, :cond_7

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-interface {v0, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v11, 0x0

    .line 1510
    .local v11, "doStats":Z
    :goto_4
    if-eqz v11, :cond_4

    .line 1511
    move-object/from16 v0, v40

    iget v3, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v40

    iput v3, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I

    .line 1514
    :cond_4
    invoke-virtual {v2}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_8

    .line 1492
    .end local v11    # "doStats":Z
    :cond_6
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    .line 1509
    :cond_7
    const/4 v11, 0x1

    goto :goto_4

    .line 1514
    .restart local v11    # "doStats":Z
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 1515
    .local v17, "field":Ljava/lang/String;
    if-eqz v11, :cond_9

    .line 1516
    move-object/from16 v0, v40

    iget-wide v6, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    move-object/from16 v0, v40

    iput-wide v6, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    .line 1520
    :cond_9
    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v18

    .line 1521
    .local v18, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v4

    if-nez v4, :cond_a

    .line 1522
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "docID="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " has term vectors for field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " but FieldInfo has storeTermVector=false"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1672
    .end local v2    # "tfv":Lorg/apache/lucene/index/Fields;
    .end local v11    # "doStats":Z
    .end local v13    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .end local v17    # "field":Ljava/lang/String;
    .end local v18    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v21    # "j":I
    .end local v22    # "liveDocs":Lorg/apache/lucene/util/Bits;
    .end local v26    # "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v27    # "postingsDocs":Lorg/apache/lucene/index/DocsEnum;
    .end local v30    # "postingsFields":Lorg/apache/lucene/index/Fields;
    .end local v35    # "postingsPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .end local v38    # "postingsTermsEnum":Lorg/apache/lucene/index/TermsEnum;
    .end local v43    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :catch_0
    move-exception v15

    .line 1673
    .local v15, "e":Ljava/lang/Throwable;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ERROR ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 1674
    move-object/from16 v0, v40

    iput-object v15, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->error:Ljava/lang/Throwable;

    .line 1675
    if-eqz p1, :cond_1

    .line 1676
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto/16 :goto_3

    .line 1525
    .end local v15    # "e":Ljava/lang/Throwable;
    .restart local v2    # "tfv":Lorg/apache/lucene/index/Fields;
    .restart local v11    # "doStats":Z
    .restart local v13    # "docs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v17    # "field":Ljava/lang/String;
    .restart local v18    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .restart local v21    # "j":I
    .restart local v22    # "liveDocs":Lorg/apache/lucene/util/Bits;
    .restart local v26    # "postings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .restart local v27    # "postingsDocs":Lorg/apache/lucene/index/DocsEnum;
    .restart local v30    # "postingsFields":Lorg/apache/lucene/index/Fields;
    .restart local v35    # "postingsPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .restart local v38    # "postingsTermsEnum":Lorg/apache/lucene/index/TermsEnum;
    .restart local v43    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_a
    if-eqz p3, :cond_5

    .line 1526
    :try_start_1
    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v42

    .line 1527
    .local v42, "terms":Lorg/apache/lucene/index/Terms;
    invoke-virtual/range {v42 .. v43}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v43

    .line 1528
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v4

    sget-object v6, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v4, v6}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v4

    if-ltz v4, :cond_b

    const/16 v31, 0x1

    .line 1529
    .local v31, "postingsHasFreq":Z
    :goto_5
    invoke-virtual/range {v18 .. v18}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v32

    .line 1530
    .local v32, "postingsHasPayload":Z
    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/Terms;->hasPayloads()Z

    move-result v46

    .line 1532
    .local v46, "vectorsHasPayload":Z
    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v37

    .line 1533
    .local v37, "postingsTerms":Lorg/apache/lucene/index/Terms;
    if-nez v37, :cond_c

    .line 1534
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector field="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " does not exist in postings; doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1528
    .end local v31    # "postingsHasFreq":Z
    .end local v32    # "postingsHasPayload":Z
    .end local v37    # "postingsTerms":Lorg/apache/lucene/index/Terms;
    .end local v46    # "vectorsHasPayload":Z
    :cond_b
    const/16 v31, 0x0

    goto :goto_5

    .line 1536
    .restart local v31    # "postingsHasFreq":Z
    .restart local v32    # "postingsHasPayload":Z
    .restart local v37    # "postingsTerms":Lorg/apache/lucene/index/Terms;
    .restart local v46    # "vectorsHasPayload":Z
    :cond_c
    invoke-virtual/range {v37 .. v38}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v38

    .line 1538
    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/Terms;->hasOffsets()Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/Terms;->hasPositions()Z

    move-result v4

    if-nez v4, :cond_e

    const/16 v19, 0x0

    .line 1539
    .local v19, "hasProx":Z
    :goto_6
    const/16 v41, 0x0

    .line 1540
    .local v41, "term":Lorg/apache/lucene/util/BytesRef;
    :cond_d
    invoke-virtual/range {v43 .. v43}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v41

    if-eqz v41, :cond_5

    .line 1542
    if-eqz v19, :cond_10

    .line 1543
    const/4 v4, 0x0

    move-object/from16 v0, v43

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v26

    .line 1544
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_f

    if-nez v26, :cond_f

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1538
    .end local v19    # "hasProx":Z
    .end local v41    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_e
    const/16 v19, 0x1

    goto :goto_6

    .line 1545
    .restart local v19    # "hasProx":Z
    .restart local v41    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_f
    const/4 v13, 0x0

    .line 1553
    :goto_7
    if-eqz v19, :cond_13

    .line 1554
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_12

    if-nez v26, :cond_12

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1547
    :cond_10
    const/4 v4, 0x0

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v13}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v13

    .line 1548
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_11

    if-nez v13, :cond_11

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1549
    :cond_11
    const/16 v26, 0x0

    goto :goto_7

    .line 1555
    :cond_12
    move-object/from16 v14, v26

    .line 1562
    .local v14, "docs2":Lorg/apache/lucene/index/DocsEnum;
    :goto_8
    const/4 v4, 0x1

    move-object/from16 v0, v38

    move-object/from16 v1, v41

    invoke-virtual {v0, v1, v4}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    if-nez v4, :cond_15

    .line 1563
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " does not exist in postings; doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1557
    .end local v14    # "docs2":Lorg/apache/lucene/index/DocsEnum;
    :cond_13
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_14

    if-nez v13, :cond_14

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1558
    :cond_14
    move-object v14, v13

    .restart local v14    # "docs2":Lorg/apache/lucene/index/DocsEnum;
    goto :goto_8

    .line 1565
    :cond_15
    const/4 v4, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v35

    invoke-virtual {v0, v4, v1}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v35

    .line 1566
    if-nez v35, :cond_16

    .line 1568
    const/4 v4, 0x0

    move-object/from16 v0, v38

    move-object/from16 v1, v27

    invoke-virtual {v0, v4, v1}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v27

    .line 1569
    if-nez v27, :cond_16

    .line 1570
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " does not exist in postings; doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1574
    :cond_16
    if-eqz v35, :cond_17

    .line 1575
    move-object/from16 v28, v35

    .line 1580
    .local v28, "postingsDocs2":Lorg/apache/lucene/index/DocsEnum;
    :goto_9
    move-object/from16 v0, v28

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v10

    .line 1581
    .local v10, "advanceDoc":I
    move/from16 v0, v21

    if-eq v10, v0, :cond_18

    .line 1582
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " was not found in postings (got: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1577
    .end local v10    # "advanceDoc":I
    .end local v28    # "postingsDocs2":Lorg/apache/lucene/index/DocsEnum;
    :cond_17
    move-object/from16 v28, v27

    .restart local v28    # "postingsDocs2":Lorg/apache/lucene/index/DocsEnum;
    goto :goto_9

    .line 1585
    .restart local v10    # "advanceDoc":I
    :cond_18
    invoke-virtual {v14}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v12

    .line 1587
    .local v12, "doc":I
    if-eqz v12, :cond_19

    .line 1588
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector for doc "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " didn\'t return docID=0: got docID="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1591
    :cond_19
    if-eqz v31, :cond_d

    .line 1592
    invoke-virtual {v14}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v44

    .line 1593
    .local v44, "tf":I
    if-eqz v31, :cond_1a

    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v4

    move/from16 v0, v44

    if-eq v4, v0, :cond_1a

    .line 1594
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": freq="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v44

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " differs from postings freq="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v28 .. v28}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1597
    :cond_1a
    if-eqz v19, :cond_d

    .line 1598
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_a
    move/from16 v0, v20

    move/from16 v1, v44

    if-ge v0, v1, :cond_d

    .line 1599
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v25

    .line 1600
    .local v25, "pos":I
    if-eqz v35, :cond_1b

    .line 1601
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v34

    .line 1602
    .local v34, "postingsPos":I
    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/Terms;->hasPositions()Z

    move-result v4

    if-eqz v4, :cond_1b

    move/from16 v0, v25

    move/from16 v1, v34

    if-eq v0, v1, :cond_1b

    .line 1603
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": pos="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " differs from postings pos="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1609
    .end local v34    # "postingsPos":I
    :cond_1b
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v39

    .line 1610
    .local v39, "startOffset":I
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v16

    .line 1622
    .local v16, "endOffset":I
    if-eqz v35, :cond_1d

    .line 1623
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v36

    .line 1625
    .local v36, "postingsStartOffset":I
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v29

    .line 1626
    .local v29, "postingsEndOffset":I
    const/4 v4, -0x1

    move/from16 v0, v39

    if-eq v0, v4, :cond_1c

    const/4 v4, -0x1

    move/from16 v0, v36

    if-eq v0, v4, :cond_1c

    move/from16 v0, v39

    move/from16 v1, v36

    if-eq v0, v1, :cond_1c

    .line 1627
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": startOffset="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v39

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " differs from postings startOffset="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1629
    :cond_1c
    const/4 v4, -0x1

    move/from16 v0, v16

    if-eq v0, v4, :cond_1d

    const/4 v4, -0x1

    move/from16 v0, v29

    if-eq v0, v4, :cond_1d

    move/from16 v0, v16

    move/from16 v1, v29

    if-eq v0, v1, :cond_1d

    .line 1630
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": endOffset="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " differs from postings endOffset="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1634
    .end local v29    # "postingsEndOffset":I
    .end local v36    # "postingsStartOffset":I
    :cond_1d
    invoke-virtual/range {v26 .. v26}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v24

    .line 1636
    .local v24, "payload":Lorg/apache/lucene/util/BytesRef;
    if-eqz v24, :cond_1e

    .line 1637
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_1e

    if-nez v46, :cond_1e

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1640
    :cond_1e
    if-eqz v32, :cond_22

    if-eqz v46, :cond_22

    .line 1641
    sget-boolean v4, Lorg/apache/lucene/index/CheckIndex;->$assertionsDisabled:Z

    if-nez v4, :cond_1f

    if-nez v35, :cond_1f

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 1643
    :cond_1f
    if-nez v24, :cond_20

    .line 1646
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    if-eqz v4, :cond_22

    .line 1647
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " has no payload but postings does: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1652
    :cond_20
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v4

    if-nez v4, :cond_21

    .line 1653
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " has payload="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " but postings does not."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1655
    :cond_21
    invoke-virtual/range {v35 .. v35}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v33

    .line 1656
    .local v33, "postingsPayload":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_22

    .line 1657
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "vector term="

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " field="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " doc="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " has payload="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " but differs from postings payload="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1598
    .end local v33    # "postingsPayload":Lorg/apache/lucene/util/BytesRef;
    :cond_22
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_a

    .line 1669
    .end local v2    # "tfv":Lorg/apache/lucene/index/Fields;
    .end local v10    # "advanceDoc":I
    .end local v11    # "doStats":Z
    .end local v12    # "doc":I
    .end local v14    # "docs2":Lorg/apache/lucene/index/DocsEnum;
    .end local v16    # "endOffset":I
    .end local v17    # "field":Ljava/lang/String;
    .end local v18    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v19    # "hasProx":Z
    .end local v20    # "i":I
    .end local v24    # "payload":Lorg/apache/lucene/util/BytesRef;
    .end local v25    # "pos":I
    .end local v28    # "postingsDocs2":Lorg/apache/lucene/index/DocsEnum;
    .end local v31    # "postingsHasFreq":Z
    .end local v32    # "postingsHasPayload":Z
    .end local v37    # "postingsTerms":Lorg/apache/lucene/index/Terms;
    .end local v39    # "startOffset":I
    .end local v41    # "term":Lorg/apache/lucene/util/BytesRef;
    .end local v42    # "terms":Lorg/apache/lucene/index/Terms;
    .end local v44    # "tf":I
    .end local v46    # "vectorsHasPayload":Z
    :cond_23
    move-object/from16 v0, v40

    iget-wide v6, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->totVectors:J

    long-to-float v3, v6

    move-object/from16 v0, v40

    iget v4, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->docCount:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    int-to-float v4, v4

    div-float v45, v3, v4

    goto/16 :goto_2
.end method


# virtual methods
.method public checkIndex()Lorg/apache/lucene/index/CheckIndex$Status;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 361
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/CheckIndex;->checkIndex(Ljava/util/List;)Lorg/apache/lucene/index/CheckIndex$Status;

    move-result-object v0

    return-object v0
.end method

.method public checkIndex(Ljava/util/List;)Lorg/apache/lucene/index/CheckIndex$Status;
    .locals 44
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/index/CheckIndex$Status;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "onlySegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v40, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static/range {v40 .. v40}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v17

    .line 378
    .local v17, "nf":Ljava/text/NumberFormat;
    new-instance v32, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 379
    .local v32, "sis":Lorg/apache/lucene/index/SegmentInfos;
    new-instance v25, Lorg/apache/lucene/index/CheckIndex$Status;

    invoke-direct/range {v25 .. v25}, Lorg/apache/lucene/index/CheckIndex$Status;-><init>()V

    .line 380
    .local v25, "result":Lorg/apache/lucene/index/CheckIndex$Status;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v25

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->dir:Lorg/apache/lucene/store/Directory;

    .line 382
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v40, v0

    move-object/from16 v0, v32

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    const v40, 0x7fffffff

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    .local v22, "oldest":Ljava/lang/String;
    const/high16 v40, -0x80000000

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    .line 393
    .local v16, "newest":Ljava/lang/String;
    const/16 v21, 0x0

    .line 394
    .local v21, "oldSegs":Ljava/lang/String;
    const/4 v10, 0x0

    .line 395
    .local v10, "foundNonNullVersion":Z
    invoke-static {}, Lorg/apache/lucene/util/StringHelper;->getVersionComparator()Ljava/util/Comparator;

    move-result-object v38

    .line 396
    .local v38, "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_0
    :goto_0
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_6

    .line 412
    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v20

    .line 413
    .local v20, "numSegments":I
    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v30

    .line 415
    .local v30, "segmentsFileName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 417
    .local v13, "input":Lorg/apache/lucene/store/IndexInput;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v40, v0

    sget-object v41, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v40

    move-object/from16 v1, v30

    move-object/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    .line 425
    const/4 v9, 0x0

    .line 427
    .local v9, "format":I
    :try_start_2
    invoke-virtual {v13}, Lorg/apache/lucene/store/IndexInput;->readInt()I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v9

    .line 435
    if-eqz v13, :cond_1

    .line 436
    invoke-virtual {v13}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 439
    :cond_1
    const-string v27, ""

    .line 440
    .local v27, "sFormat":Ljava/lang/String;
    const/16 v33, 0x0

    .line 442
    .local v33, "skip":Z
    move-object/from16 v0, v30

    move-object/from16 v1, v25

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->segmentsFileName:Ljava/lang/String;

    .line 443
    move/from16 v0, v20

    move-object/from16 v1, v25

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->numSegments:I

    .line 444
    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v25

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->userData:Ljava/util/Map;

    .line 446
    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Ljava/util/Map;->size()I

    move-result v40

    if-lez v40, :cond_c

    .line 447
    new-instance v40, Ljava/lang/StringBuilder;

    const-string v41, " userData="

    invoke-direct/range {v40 .. v41}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->getUserData()Ljava/util/Map;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    .line 452
    .local v36, "userDataString":Ljava/lang/String;
    :goto_1
    const/16 v39, 0x0

    .line 453
    .local v39, "versionString":Ljava/lang/String;
    if-eqz v21, :cond_e

    .line 454
    if-eqz v10, :cond_d

    .line 455
    new-instance v40, Ljava/lang/StringBuilder;

    const-string/jumbo v41, "versions=["

    invoke-direct/range {v40 .. v41}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, " .. "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "]"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 463
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "Segments file="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " numSegments="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    .line 464
    const-string v42, " "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " format="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 463
    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 466
    if-eqz p1, :cond_4

    .line 467
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->partial:Z

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "\nChecking only these segments:"

    invoke-virtual/range {v40 .. v41}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 470
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_3
    :goto_3
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v41

    if-nez v41, :cond_10

    .line 474
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->segmentsChecked:Ljava/util/List;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, ":"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 478
    :cond_4
    if-eqz v33, :cond_11

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "\nERROR: this index appears to be created by a newer version of Lucene than this tool was compiled on; please re-compile this tool on the matching version of Lucene; exiting"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 480
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->toolOutOfDate:Z

    .line 668
    .end local v9    # "format":I
    .end local v10    # "foundNonNullVersion":Z
    .end local v13    # "input":Lorg/apache/lucene/store/IndexInput;
    .end local v16    # "newest":Ljava/lang/String;
    .end local v20    # "numSegments":I
    .end local v21    # "oldSegs":Ljava/lang/String;
    .end local v22    # "oldest":Ljava/lang/String;
    .end local v27    # "sFormat":Ljava/lang/String;
    .end local v30    # "segmentsFileName":Ljava/lang/String;
    .end local v33    # "skip":Z
    .end local v36    # "userDataString":Ljava/lang/String;
    .end local v38    # "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    .end local v39    # "versionString":Ljava/lang/String;
    :cond_5
    :goto_4
    return-object v25

    .line 383
    :catch_0
    move-exception v34

    .line 384
    .local v34, "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "ERROR: could not read any segments file in directory"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 385
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->missingSegments:Z

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_5

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_4

    .line 396
    .end local v34    # "t":Ljava/lang/Throwable;
    .restart local v10    # "foundNonNullVersion":Z
    .restart local v16    # "newest":Ljava/lang/String;
    .restart local v21    # "oldSegs":Ljava/lang/String;
    .restart local v22    # "oldest":Ljava/lang/String;
    .restart local v38    # "versionComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/String;>;"
    :cond_6
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 397
    .local v31, "si":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, v31

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v37

    .line 398
    .local v37, "version":Ljava/lang/String;
    if-nez v37, :cond_7

    .line 400
    const-string v21, "pre-3.1"

    .line 401
    goto/16 :goto_0

    .line 402
    :cond_7
    const/4 v10, 0x1

    .line 403
    move-object/from16 v0, v38

    move-object/from16 v1, v37

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v41

    if-gez v41, :cond_8

    .line 404
    move-object/from16 v22, v37

    .line 406
    :cond_8
    move-object/from16 v0, v38

    move-object/from16 v1, v37

    move-object/from16 v2, v16

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v41

    if-lez v41, :cond_0

    .line 407
    move-object/from16 v16, v37

    goto/16 :goto_0

    .line 418
    .end local v31    # "si":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v37    # "version":Ljava/lang/String;
    .restart local v13    # "input":Lorg/apache/lucene/store/IndexInput;
    .restart local v20    # "numSegments":I
    .restart local v30    # "segmentsFileName":Ljava/lang/String;
    :catch_1
    move-exception v34

    .line 419
    .restart local v34    # "t":Ljava/lang/Throwable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "ERROR: could not open segments file in directory"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_9

    .line 421
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 422
    :cond_9
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->cantOpenSegments:Z

    goto/16 :goto_4

    .line 428
    .end local v34    # "t":Ljava/lang/Throwable;
    .restart local v9    # "format":I
    :catch_2
    move-exception v34

    .line 429
    .restart local v34    # "t":Ljava/lang/Throwable;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "ERROR: could not read segment file version in directory"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_a

    .line 431
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 432
    :cond_a
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->missingSegmentVersion:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 435
    if-eqz v13, :cond_5

    .line 436
    invoke-virtual {v13}, Lorg/apache/lucene/store/IndexInput;->close()V

    goto/16 :goto_4

    .line 434
    .end local v34    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v40

    .line 435
    if-eqz v13, :cond_b

    .line 436
    invoke-virtual {v13}, Lorg/apache/lucene/store/IndexInput;->close()V

    .line 437
    :cond_b
    throw v40

    .line 449
    .restart local v27    # "sFormat":Ljava/lang/String;
    .restart local v33    # "skip":Z
    :cond_c
    const-string v36, ""

    .restart local v36    # "userDataString":Ljava/lang/String;
    goto/16 :goto_1

    .line 457
    .restart local v39    # "versionString":Ljava/lang/String;
    :cond_d
    new-instance v40, Ljava/lang/StringBuilder;

    const-string/jumbo v41, "version="

    invoke-direct/range {v40 .. v41}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    .line 459
    goto/16 :goto_2

    .line 460
    :cond_e
    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_f

    new-instance v40, Ljava/lang/StringBuilder;

    const-string/jumbo v41, "version="

    invoke-direct/range {v40 .. v41}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    :goto_5
    goto/16 :goto_2

    :cond_f
    new-instance v40, Ljava/lang/StringBuilder;

    const-string/jumbo v41, "versions=["

    invoke-direct/range {v40 .. v41}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, " .. "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "]"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    goto :goto_5

    .line 470
    :cond_10
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    .line 471
    .local v26, "s":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v41, v0

    if-eqz v41, :cond_3

    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v41, v0

    new-instance v42, Ljava/lang/StringBuilder;

    const-string v43, " "

    invoke-direct/range {v42 .. v43}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v42

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 485
    .end local v26    # "s":Ljava/lang/String;
    :cond_11
    invoke-virtual/range {v32 .. v32}, Lorg/apache/lucene/index/SegmentInfos;->clone()Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v25

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    .line 486
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 487
    const/16 v40, -0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    .line 489
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_6
    move/from16 v0, v20

    if-lt v11, v0, :cond_13

    .line 653
    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I

    move/from16 v40, v0

    if-nez v40, :cond_32

    .line 654
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    .line 658
    :goto_7
    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v40, v0

    move-object/from16 v0, v32

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    move/from16 v41, v0

    move/from16 v0, v40

    move/from16 v1, v41

    if-ge v0, v1, :cond_33

    const/16 v40, 0x1

    :goto_8
    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->validCounter:Z

    if-nez v40, :cond_12

    .line 659
    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    .line 660
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v40, v0

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v41, v0

    add-int/lit8 v41, v41, 0x1

    move/from16 v0, v41

    move-object/from16 v1, v40

    iput v0, v1, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    .line 661
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "ERROR: Next segment name counter "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    iget v0, v0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " is not greater than max segment name "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 664
    :cond_12
    move-object/from16 v0, v25

    iget-boolean v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->clean:Z

    move/from16 v40, v0

    if-eqz v40, :cond_5

    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "No problems were detected with this index.\n"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 490
    :cond_13
    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v12

    .line 491
    .local v12, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x1

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v40

    const/16 v41, 0x24

    invoke-static/range {v40 .. v41}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v29

    .line 492
    .local v29, "segmentName":I
    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    move/from16 v40, v0

    move/from16 v0, v29

    move/from16 v1, v40

    if-le v0, v1, :cond_14

    .line 493
    move/from16 v0, v29

    move-object/from16 v1, v25

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->maxSegmentName:I

    .line 495
    :cond_14
    if-eqz p1, :cond_16

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_16

    .line 489
    :cond_15
    :goto_9
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_6

    .line 498
    :cond_16
    new-instance v28, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;

    invoke-direct/range {v28 .. v28}, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;-><init>()V

    .line 499
    .local v28, "segInfoStat":Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->segmentInfos:Ljava/util/List;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "  "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v42, v11, 0x1

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " of "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, ": name="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " docCount="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 501
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->name:Ljava/lang/String;

    .line 502
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docCount:I

    .line 504
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v35

    .line 506
    .local v35, "toLoseDocCount":I
    const/16 v23, 0x0

    .line 509
    .local v23, "reader":Lorg/apache/lucene/index/AtomicReader;
    :try_start_4
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v5

    .line 510
    .local v5, "codec":Lorg/apache/lucene/codecs/Codec;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    codec="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 511
    move-object/from16 v0, v28

    iput-object v5, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    compound="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 513
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->compound:Z

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    numFiles="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v42

    invoke-interface/range {v42 .. v42}, Ljava/util/Collection;->size()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 515
    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Ljava/util/Collection;->size()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numFiles:I

    .line 516
    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v40

    move-wide/from16 v0, v40

    long-to-double v0, v0

    move-wide/from16 v40, v0

    const-wide/high16 v42, 0x4130000000000000L    # 1048576.0

    div-double v40, v40, v42

    move-wide/from16 v0, v40

    move-object/from16 v2, v28

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->sizeMB:D

    .line 517
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    sget-object v41, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoFormat;->DS_OFFSET_KEY:Ljava/lang/String;

    invoke-virtual/range {v40 .. v41}, Lorg/apache/lucene/index/SegmentInfo;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    if-nez v40, :cond_17

    .line 519
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    size (MB)="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    iget-wide v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->sizeMB:D

    move-wide/from16 v42, v0

    move-object/from16 v0, v17

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 521
    :cond_17
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getDiagnostics()Ljava/util/Map;

    move-result-object v7

    .line 522
    .local v7, "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v28

    iput-object v7, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->diagnostics:Ljava/util/Map;

    .line 523
    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v40

    if-lez v40, :cond_18

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    diagnostics = "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 527
    :cond_18
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->attributes()Ljava/util/Map;

    move-result-object v4

    .line 528
    .local v4, "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v4, :cond_19

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v40

    if-nez v40, :cond_19

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    attributes = "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 532
    :cond_19
    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z

    move-result v40

    if-nez v40, :cond_1c

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "    no deletions"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 534
    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->hasDeletions:Z

    .line 541
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_1a

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "    test: open reader........."

    invoke-virtual/range {v40 .. v41}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 543
    :cond_1a
    new-instance v24, Lorg/apache/lucene/index/SegmentReader;

    const/16 v40, 0x1

    sget-object v41, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v24

    move/from16 v1, v40

    move-object/from16 v2, v41

    invoke-direct {v0, v12, v1, v2}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 545
    .end local v23    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .local v24, "reader":Lorg/apache/lucene/index/AtomicReader;
    const/16 v40, 0x1

    :try_start_5
    move/from16 v0, v40

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->openReaderPassed:Z

    .line 547
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v18

    .line 548
    .local v18, "numDocs":I
    move/from16 v35, v18

    .line 549
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->hasDeletions()Z

    move-result v40

    if-eqz v40, :cond_25

    .line 550
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v40

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v41

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v42

    sub-int v41, v41, v42

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_1d

    .line 551
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "delete count mismatch: info="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v42

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v43

    sub-int v42, v42, v43

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " vs reader="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 633
    .end local v18    # "numDocs":I
    :catch_3
    move-exception v34

    move-object/from16 v23, v24

    .line 634
    .end local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .end local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v24    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v23    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v34    # "t":Ljava/lang/Throwable;
    :goto_b
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "FAILED"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 636
    const-string v6, "fixIndex() would remove reference to this segment"

    .line 637
    .local v6, "comment":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    WARNING: "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "; full exception:"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_1b

    .line 639
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintStream;)V

    .line 640
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, ""

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 641
    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v40, v0

    add-int v40, v40, v35

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    .line 642
    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I

    move/from16 v40, v0

    add-int/lit8 v40, v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v25

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 645
    if-eqz v23, :cond_15

    .line 646
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/AtomicReader;->close()V

    goto/16 :goto_9

    .line 537
    .end local v6    # "comment":Ljava/lang/String;
    .end local v34    # "t":Ljava/lang/Throwable;
    .restart local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .restart local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1c
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "    has deletions [delGen="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v42

    invoke-virtual/range {v41 .. v43}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "]"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 538
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput-boolean v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->hasDeletions:Z

    .line 539
    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v40

    move-wide/from16 v0, v40

    move-object/from16 v2, v28

    iput-wide v0, v2, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->deletionsGen:J
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto/16 :goto_a

    .line 633
    .end local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .end local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_4
    move-exception v34

    goto/16 :goto_b

    .line 553
    .end local v23    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .restart local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v18    # "numDocs":I
    .restart local v24    # "reader":Lorg/apache/lucene/index/AtomicReader;
    :cond_1d
    :try_start_8
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v40

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v41

    sub-int v40, v40, v41

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v41

    move/from16 v0, v40

    move/from16 v1, v41

    if-le v0, v1, :cond_1f

    .line 554
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "too many deleted docs: maxDoc()="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " vs del count="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v42

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->numDocs()I

    move-result v43

    sub-int v42, v42, v43

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 644
    .end local v18    # "numDocs":I
    :catchall_1
    move-exception v40

    move-object/from16 v23, v24

    .line 645
    .end local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .end local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v24    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v23    # "reader":Lorg/apache/lucene/index/AtomicReader;
    :goto_c
    if-eqz v23, :cond_1e

    .line 646
    invoke-virtual/range {v23 .. v23}, Lorg/apache/lucene/index/AtomicReader;->close()V

    .line 647
    :cond_1e
    throw v40

    .line 556
    .end local v23    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .restart local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v18    # "numDocs":I
    .restart local v24    # "reader":Lorg/apache/lucene/index/AtomicReader;
    :cond_1f
    :try_start_9
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v40

    sub-int v40, v40, v18

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v41

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_20

    .line 557
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "delete count mismatch: info="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " vs reader="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v42

    sub-int v42, v42, v18

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 559
    :cond_20
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v15

    .line 560
    .local v15, "liveDocs":Lorg/apache/lucene/util/Bits;
    if-nez v15, :cond_21

    .line 561
    new-instance v40, Ljava/lang/RuntimeException;

    const-string v41, "segment should have deletions, but liveDocs is null"

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 563
    :cond_21
    const/16 v19, 0x0

    .line 564
    .local v19, "numLive":I
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_d
    invoke-interface {v15}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v40

    move/from16 v0, v40

    if-lt v14, v0, :cond_22

    .line 569
    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_24

    .line 570
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "liveDocs count mismatch: info="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, ", vs bits="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 565
    :cond_22
    invoke-interface {v15, v14}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v40

    if-eqz v40, :cond_23

    .line 566
    add-int/lit8 v19, v19, 0x1

    .line 564
    :cond_23
    add-int/lit8 v14, v14, 0x1

    goto :goto_d

    .line 574
    :cond_24
    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v40

    sub-int v40, v40, v18

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numDeleted:I

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "OK ["

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numDeleted:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " deleted docs]"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 591
    .end local v14    # "j":I
    .end local v19    # "numLive":I
    :goto_e
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v40

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v41

    move/from16 v0, v40

    move/from16 v1, v41

    if-eq v0, v1, :cond_2a

    .line 592
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "SegmentReader.maxDoc() "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " != SegmentInfos.docCount "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 577
    .end local v15    # "liveDocs":Lorg/apache/lucene/util/Bits;
    :cond_25
    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v40

    if-eqz v40, :cond_26

    .line 578
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "delete count mismatch: info="

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " vs reader="

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    iget-object v0, v12, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v42

    sub-int v42, v42, v18

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 580
    :cond_26
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v15

    .line 581
    .restart local v15    # "liveDocs":Lorg/apache/lucene/util/Bits;
    if-eqz v15, :cond_27

    .line 583
    const/4 v14, 0x0

    .restart local v14    # "j":I
    :goto_f
    invoke-interface {v15}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v40

    move/from16 v0, v40

    if-lt v14, v0, :cond_28

    .line 589
    .end local v14    # "j":I
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "OK"

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 584
    .restart local v14    # "j":I
    :cond_28
    invoke-interface {v15, v14}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v40

    if-nez v40, :cond_29

    .line 585
    new-instance v40, Ljava/lang/RuntimeException;

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "liveDocs mismatch: info says no deletions but doc "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v41

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " is deleted."

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 583
    :cond_29
    add-int/lit8 v14, v14, 0x1

    goto :goto_f

    .line 596
    .end local v14    # "j":I
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2b

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, "    test: fields.............."

    invoke-virtual/range {v40 .. v41}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 599
    :cond_2b
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v8

    .line 600
    .local v8, "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "OK ["

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " fields]"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    .line 601
    invoke-virtual {v8}, Lorg/apache/lucene/index/FieldInfos;->size()I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, v28

    iput v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->numFields:I

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->testFieldNorms(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->fieldNormStatus:Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/CheckIndex;->verbose:Z

    move/from16 v41, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Lorg/apache/lucene/index/CheckIndex;->testPostings(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;Z)Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termIndexStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->testStoredFields(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->storedFieldStatus:Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/CheckIndex;->verbose:Z

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/lucene/index/CheckIndex;->crossCheckTermVectors:Z

    move/from16 v42, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    move/from16 v2, v41

    move/from16 v3, v42

    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/CheckIndex;->testTermVectors(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;ZZ)Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termVectorStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    .line 615
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v40

    invoke-static {v0, v1}, Lorg/apache/lucene/index/CheckIndex;->testDocValues(Lorg/apache/lucene/index/AtomicReader;Ljava/io/PrintStream;)Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v28

    iput-object v0, v1, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docValuesStatus:Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;

    .line 619
    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->fieldNormStatus:Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2c

    .line 620
    new-instance v40, Ljava/lang/RuntimeException;

    const-string v41, "Field Norm test failed"

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 621
    :cond_2c
    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termIndexStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2d

    .line 622
    new-instance v40, Ljava/lang/RuntimeException;

    const-string v41, "Term Index test failed"

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 623
    :cond_2d
    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->storedFieldStatus:Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2e

    .line 624
    new-instance v40, Ljava/lang/RuntimeException;

    const-string v41, "Stored Field test failed"

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 625
    :cond_2e
    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->termVectorStatus:Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v40, v0

    if-eqz v40, :cond_2f

    .line 626
    new-instance v40, Ljava/lang/RuntimeException;

    const-string v41, "Term Vector test failed"

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 627
    :cond_2f
    move-object/from16 v0, v28

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;->docValuesStatus:Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;->error:Ljava/lang/Throwable;

    move-object/from16 v40, v0

    if-eqz v40, :cond_30

    .line 628
    new-instance v40, Ljava/lang/RuntimeException;

    const-string v41, "DocValues test failed"

    invoke-direct/range {v40 .. v41}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v40

    .line 631
    :cond_30
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    const-string v41, ""

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 645
    if-eqz v24, :cond_31

    .line 646
    invoke-virtual/range {v24 .. v24}, Lorg/apache/lucene/index/AtomicReader;->close()V

    .line 650
    :cond_31
    move-object/from16 v0, v25

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    move-object/from16 v40, v0

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->clone()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    goto/16 :goto_9

    .line 656
    .end local v4    # "atts":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "codec":Lorg/apache/lucene/codecs/Codec;
    .end local v7    # "diagnostics":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v8    # "fieldInfos":Lorg/apache/lucene/index/FieldInfos;
    .end local v12    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v15    # "liveDocs":Lorg/apache/lucene/util/Bits;
    .end local v18    # "numDocs":I
    .end local v24    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .end local v28    # "segInfoStat":Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;
    .end local v29    # "segmentName":I
    .end local v35    # "toLoseDocCount":I
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    move-object/from16 v40, v0

    new-instance v41, Ljava/lang/StringBuilder;

    const-string v42, "WARNING: "

    invoke-direct/range {v41 .. v42}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->numBadSegments:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " broken segments (containing "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v25

    iget v0, v0, Lorg/apache/lucene/index/CheckIndex$Status;->totLoseDocCount:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, " documents) detected"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Lorg/apache/lucene/index/CheckIndex;->msg(Ljava/io/PrintStream;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 658
    :cond_33
    const/16 v40, 0x0

    goto/16 :goto_8

    .line 644
    .restart local v12    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v23    # "reader":Lorg/apache/lucene/index/AtomicReader;
    .restart local v28    # "segInfoStat":Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;
    .restart local v29    # "segmentName":I
    .restart local v35    # "toLoseDocCount":I
    :catchall_2
    move-exception v40

    goto/16 :goto_c
.end method

.method public fixIndex(Lorg/apache/lucene/index/CheckIndex$Status;Lorg/apache/lucene/codecs/Codec;)V
    .locals 2
    .param p1, "result"    # Lorg/apache/lucene/index/CheckIndex$Status;
    .param p2, "codec"    # Lorg/apache/lucene/codecs/Codec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1697
    iget-boolean v0, p1, Lorg/apache/lucene/index/CheckIndex$Status;->partial:Z

    if-eqz v0, :cond_0

    .line 1698
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "can only fix an index that was fully checked (this status checked a subset of segments)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1699
    :cond_0
    iget-object v0, p1, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->changed()V

    .line 1700
    iget-object v0, p1, Lorg/apache/lucene/index/CheckIndex$Status;->newSegments:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v1, p1, Lorg/apache/lucene/index/CheckIndex$Status;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->commit(Lorg/apache/lucene/store/Directory;)V

    .line 1701
    return-void
.end method

.method public getCrossCheckTermVectors()Z
    .locals 1

    .prologue
    .line 328
    iget-boolean v0, p0, Lorg/apache/lucene/index/CheckIndex;->crossCheckTermVectors:Z

    return v0
.end method

.method public setCrossCheckTermVectors(Z)V
    .locals 0
    .param p1, "v"    # Z

    .prologue
    .line 323
    iput-boolean p1, p0, Lorg/apache/lucene/index/CheckIndex;->crossCheckTermVectors:Z

    .line 324
    return-void
.end method

.method public setInfoStream(Ljava/io/PrintStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 343
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/CheckIndex;->setInfoStream(Ljava/io/PrintStream;Z)V

    .line 344
    return-void
.end method

.method public setInfoStream(Ljava/io/PrintStream;Z)V
    .locals 0
    .param p1, "out"    # Ljava/io/PrintStream;
    .param p2, "verbose"    # Z

    .prologue
    .line 337
    iput-object p1, p0, Lorg/apache/lucene/index/CheckIndex;->infoStream:Ljava/io/PrintStream;

    .line 338
    iput-boolean p2, p0, Lorg/apache/lucene/index/CheckIndex;->verbose:Z

    .line 339
    return-void
.end method
