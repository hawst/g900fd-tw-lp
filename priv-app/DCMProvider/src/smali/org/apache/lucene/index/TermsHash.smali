.class final Lorg/apache/lucene/index/TermsHash;
.super Lorg/apache/lucene/index/InvertedDocConsumer;
.source "TermsHash.java"


# instance fields
.field final bytePool:Lorg/apache/lucene/util/ByteBlockPool;

.field final bytesUsed:Lorg/apache/lucene/util/Counter;

.field final consumer:Lorg/apache/lucene/index/TermsHashConsumer;

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final intPool:Lorg/apache/lucene/util/IntBlockPool;

.field final nextTermsHash:Lorg/apache/lucene/index/TermsHash;

.field final primary:Z

.field termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

.field final termBytesRef:Lorg/apache/lucene/util/BytesRef;

.field final tr1:Lorg/apache/lucene/util/BytesRef;

.field final tr2:Lorg/apache/lucene/util/BytesRef;

.field final trackAllocations:Z


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;Lorg/apache/lucene/index/TermsHashConsumer;ZLorg/apache/lucene/index/TermsHash;)V
    .locals 2
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .param p2, "consumer"    # Lorg/apache/lucene/index/TermsHashConsumer;
    .param p3, "trackAllocations"    # Z
    .param p4, "nextTermsHash"    # Lorg/apache/lucene/index/TermsHash;

    .prologue
    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocConsumer;-><init>()V

    .line 51
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->tr1:Lorg/apache/lucene/util/BytesRef;

    .line 52
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->tr2:Lorg/apache/lucene/util/BytesRef;

    .line 55
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->termBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 60
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 61
    iput-object p2, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    .line 62
    iput-boolean p3, p0, Lorg/apache/lucene/index/TermsHash;->trackAllocations:Z

    .line 63
    iput-object p4, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    .line 64
    if-eqz p3, :cond_0

    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    :goto_0
    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 65
    new-instance v0, Lorg/apache/lucene/util/IntBlockPool;

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->intBlockAllocator:Lorg/apache/lucene/util/IntBlockPool$Allocator;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntBlockPool;-><init>(Lorg/apache/lucene/util/IntBlockPool$Allocator;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    .line 66
    new-instance v0, Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->byteBlockAllocator:Lorg/apache/lucene/util/ByteBlockPool$Allocator;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 68
    if-eqz p4, :cond_1

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHash;->primary:Z

    .line 71
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iput-object v0, p0, Lorg/apache/lucene/index/TermsHash;->termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 72
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    iput-object v0, p4, Lorg/apache/lucene/index/TermsHash;->termBytePool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 76
    :goto_1
    return-void

    .line 64
    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/Counter;->newCounter()Lorg/apache/lucene/util/Counter;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/TermsHash;->primary:Z

    goto :goto_1
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    .line 80
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermsHash;->reset()V

    .line 82
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumer;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHash;->abort()V

    .line 88
    :cond_0
    return-void

    .line 83
    :catchall_0
    move-exception v0

    .line 84
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v1, :cond_1

    .line 85
    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsHash;->abort()V

    .line 87
    :cond_1
    throw v0
.end method

.method addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocConsumerPerField;
    .locals 2
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 125
    new-instance v0, Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-direct {v0, p1, p0, v1, p2}, Lorg/apache/lucene/index/TermsHashPerField;-><init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/TermsHash;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method finishDocument()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/TermsHashConsumer;->finishDocument(Lorg/apache/lucene/index/TermsHash;)V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsHashConsumer;->finishDocument(Lorg/apache/lucene/index/TermsHash;)V

    .line 134
    :cond_0
    return-void
.end method

.method flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 7
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/InvertedDocConsumerPerField;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "fieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 102
    .local v0, "childFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v4, :cond_2

    .line 103
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 108
    .local v2, "nextChildFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    :goto_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 116
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v4, v0, p2}, Lorg/apache/lucene/index/TermsHashConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 118
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v4, :cond_1

    .line 119
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    invoke-virtual {v4, v2, p2}, Lorg/apache/lucene/index/TermsHash;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 121
    :cond_1
    return-void

    .line 105
    .end local v2    # "nextChildFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "nextChildFields":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    goto :goto_0

    .line 108
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 109
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/TermsHashPerField;

    .line 110
    .local v3, "perField":Lorg/apache/lucene/index/TermsHashPerField;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, v3, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    invoke-interface {v0, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v4, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v4, :cond_0

    .line 112
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, v3, Lorg/apache/lucene/index/TermsHashPerField;->nextPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-interface {v2, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->intPool:Lorg/apache/lucene/util/IntBlockPool;

    invoke-virtual {v0, v1, v1}, Lorg/apache/lucene/util/IntBlockPool;->reset(ZZ)V

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->bytePool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v0, v1, v1}, Lorg/apache/lucene/util/ByteBlockPool;->reset(ZZ)V

    .line 95
    return-void
.end method

.method startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumer;->startDocument()V

    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHash;->nextTermsHash:Lorg/apache/lucene/index/TermsHash;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHash;->consumer:Lorg/apache/lucene/index/TermsHashConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsHashConsumer;->startDocument()V

    .line 142
    :cond_0
    return-void
.end method
