.class public Lorg/apache/lucene/index/SegmentWriteState;
.super Ljava/lang/Object;
.source "SegmentWriteState.java"


# instance fields
.field public final context:Lorg/apache/lucene/store/IOContext;

.field public delCountOnFlush:I

.field public final directory:Lorg/apache/lucene/store/Directory;

.field public final fieldInfos:Lorg/apache/lucene/index/FieldInfos;

.field public final infoStream:Lorg/apache/lucene/util/InfoStream;

.field public liveDocs:Lorg/apache/lucene/util/MutableBits;

.field public final segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

.field public final segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

.field public final segmentSuffix:Ljava/lang/String;

.field public termIndexInterval:I


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentWriteState;Ljava/lang/String;)V
    .locals 1
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "segmentSuffix"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 99
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    .line 100
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 101
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 102
    iget v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->termIndexInterval:I

    iput v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->termIndexInterval:I

    .line 103
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    .line 104
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    .line 105
    iget-object v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 106
    iget v0, p1, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    iput v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->delCountOnFlush:I

    .line 107
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/util/InfoStream;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/index/FieldInfos;ILorg/apache/lucene/index/BufferedDeletes;Lorg/apache/lucene/store/IOContext;)V
    .locals 1
    .param p1, "infoStream"    # Lorg/apache/lucene/util/InfoStream;
    .param p2, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p3, "segmentInfo"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p4, "fieldInfos"    # Lorg/apache/lucene/index/FieldInfos;
    .param p5, "termIndexInterval"    # I
    .param p6, "segDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p7, "context"    # Lorg/apache/lucene/store/IOContext;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentWriteState;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 85
    iput-object p6, p0, Lorg/apache/lucene/index/SegmentWriteState;->segDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 86
    iput-object p2, p0, Lorg/apache/lucene/index/SegmentWriteState;->directory:Lorg/apache/lucene/store/Directory;

    .line 87
    iput-object p3, p0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    .line 88
    iput-object p4, p0, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    .line 89
    iput p5, p0, Lorg/apache/lucene/index/SegmentWriteState;->termIndexInterval:I

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentWriteState;->segmentSuffix:Ljava/lang/String;

    .line 91
    iput-object p7, p0, Lorg/apache/lucene/index/SegmentWriteState;->context:Lorg/apache/lucene/store/IOContext;

    .line 92
    return-void
.end method
