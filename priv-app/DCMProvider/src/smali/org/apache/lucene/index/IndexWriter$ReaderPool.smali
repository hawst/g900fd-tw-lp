.class Lorg/apache/lucene/index/IndexWriter$ReaderPool;
.super Ljava/lang/Object;
.source "IndexWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/IndexWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReaderPool"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final readerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Lorg/apache/lucene/index/ReadersAndLiveDocs;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 419
    const-class v0, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/IndexWriter;)V
    .locals 1

    .prologue
    .line 419
    iput-object p1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public declared-synchronized commit(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 6
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 517
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 530
    monitor-exit p0

    return-void

    .line 517
    :cond_1
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 518
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;

    .line 519
    .local v1, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    if-eqz v1, :cond_0

    .line 520
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eq v3, v0, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 517
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v1    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 521
    .restart local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v1    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_2
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$1(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writeLiveDocs(Lorg/apache/lucene/store/Directory;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 523
    sget-boolean v3, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 526
    :cond_3
    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v3, v3, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v4, v4, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized drop(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    .locals 2
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 432
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;

    .line 433
    .local v0, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    if-eqz v0, :cond_1

    .line 434
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eq p1, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    .end local v0    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 435
    .restart local v0    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    invoke-virtual {v0}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->dropReaders()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 438
    :cond_1
    monitor-exit p0

    return-void
.end method

.method declared-synchronized dropAll(Z)V
    .locals 7
    .param p1, "doSave"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 467
    monitor-enter p0

    const/4 v1, 0x0

    .line 468
    .local v1, "priorE":Ljava/lang/Throwable;
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 469
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/ReadersAndLiveDocs;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 504
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/ReadersAndLiveDocs;>;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 470
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/ReadersAndLiveDocs;>;>;"
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 473
    .local v2, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    if-eqz p1, :cond_2

    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v4}, Lorg/apache/lucene/index/IndexWriter;->access$1(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writeLiveDocs(Lorg/apache/lucene/store/Directory;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 475
    sget-boolean v4, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget-object v4, v2, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {p0, v4}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 480
    :catch_0
    move-exception v3

    .line 481
    .local v3, "t":Ljava/lang/Throwable;
    if-eqz v1, :cond_2

    .line 482
    move-object v1, v3

    .line 490
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_2
    :goto_1
    :try_start_3
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 497
    :try_start_4
    invoke-virtual {v2}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->dropReaders()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 498
    :catch_1
    move-exception v3

    .line 499
    .restart local v3    # "t":Ljava/lang/Throwable;
    if-eqz v1, :cond_0

    .line 500
    move-object v1, v3

    goto :goto_0

    .line 478
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_3
    :try_start_5
    iget-object v4, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v4, v4, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v5, v5, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 505
    .end local v2    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :cond_4
    if-eqz v1, :cond_5

    .line 506
    :try_start_6
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 508
    :cond_5
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "create"    # Z

    .prologue
    .line 539
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget-object v2, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v2}, Lorg/apache/lucene/index/IndexWriter;->access$1(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "info.dir="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v3}, Lorg/apache/lucene/index/IndexWriter;->access$1(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 541
    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 542
    .local v0, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    if-nez v0, :cond_4

    .line 543
    if-nez p2, :cond_1

    .line 544
    const/4 v1, 0x0

    .line 558
    :goto_0
    monitor-exit p0

    return-object v1

    .line 546
    :cond_1
    :try_start_2
    new-instance v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;

    .end local v0    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;-><init>(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 548
    .restart local v0    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    :cond_2
    if-eqz p2, :cond_3

    .line 555
    invoke-virtual {v0}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->incRef()V

    :cond_3
    move-object v1, v0

    .line 558
    goto :goto_0

    .line 550
    :cond_4
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-eq v1, p1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "rld.info="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " info="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isLive?="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public declared-synchronized infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 4
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 425
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/SegmentInfos;->indexOf(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v0

    .line 426
    .local v0, "idx":I
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "info="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isn\'t live"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 425
    .end local v0    # "idx":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 427
    .restart local v0    # "idx":I
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v1

    if-eq v1, p1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "info="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t match live info in segmentInfos"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 428
    :cond_1
    const/4 v1, 0x1

    monitor-exit p0

    return v1
.end method

.method public declared-synchronized release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V
    .locals 3
    .param p1, "rld"    # Lorg/apache/lucene/index/ReadersAndLiveDocs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 443
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->decRef()V

    .line 446
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->refCount()I

    move-result v0

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->poolReaders:Z
    invoke-static {v0}, Lorg/apache/lucene/index/IndexWriter;->access$0(Lorg/apache/lucene/index/IndexWriter;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->refCount()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 451
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    # getter for: Lorg/apache/lucene/index/IndexWriter;->directory:Lorg/apache/lucene/store/Directory;
    invoke-static {v0}, Lorg/apache/lucene/index/IndexWriter;->access$1(Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writeLiveDocs(Lorg/apache/lucene/store/Directory;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 453
    sget-boolean v0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->infoIsLive(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 456
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->deleter:Lorg/apache/lucene/index/IndexFileDeleter;

    iget-object v1, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->this$0:Lorg/apache/lucene/index/IndexWriter;

    iget-object v1, v1, Lorg/apache/lucene/index/IndexWriter;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/IndexFileDeleter;->checkpoint(Lorg/apache/lucene/index/SegmentInfos;Z)V

    .line 459
    :cond_2
    invoke-virtual {p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->dropReaders()V

    .line 460
    iget-object v0, p0, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->readerMap:Ljava/util/Map;

    iget-object v1, p1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462
    :cond_3
    monitor-exit p0

    return-void
.end method
