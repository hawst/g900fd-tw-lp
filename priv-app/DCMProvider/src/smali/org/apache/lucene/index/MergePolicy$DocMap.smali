.class public abstract Lorg/apache/lucene/index/MergePolicy$DocMap;
.super Ljava/lang/Object;
.source "MergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DocMap"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lorg/apache/lucene/index/MergePolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MergePolicy$DocMap;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method isConsistent(I)Z
    .locals 6
    .param p1, "maxDoc"    # I

    .prologue
    const/4 v3, 0x0

    .line 73
    new-instance v2, Lorg/apache/lucene/util/FixedBitSet;

    invoke-direct {v2, p1}, Lorg/apache/lucene/util/FixedBitSet;-><init>(I)V

    .line 74
    .local v2, "targets":Lorg/apache/lucene/util/FixedBitSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_1

    .line 84
    const/4 v3, 0x1

    :cond_0
    return v3

    .line 75
    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/MergePolicy$DocMap;->map(I)I

    move-result v1

    .line 76
    .local v1, "target":I
    if-ltz v1, :cond_2

    if-lt v1, p1, :cond_3

    .line 77
    :cond_2
    sget-boolean v4, Lorg/apache/lucene/index/MergePolicy$DocMap;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "out of range: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not in [0-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 79
    :cond_3
    invoke-virtual {v2, v1}, Lorg/apache/lucene/util/FixedBitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 80
    sget-boolean v4, Lorg/apache/lucene/index/MergePolicy$DocMap;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " is already taken ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 74
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public abstract map(I)I
.end method
