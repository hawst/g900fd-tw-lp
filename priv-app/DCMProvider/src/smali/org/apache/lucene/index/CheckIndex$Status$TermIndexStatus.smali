.class public final Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex$Status;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TermIndexStatus"
.end annotation


# instance fields
.field public blockTreeStats:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/codecs/BlockTreeTermsReader$Stats;",
            ">;"
        }
    .end annotation
.end field

.field public delTermCount:J

.field public error:Ljava/lang/Throwable;

.field public termCount:J

.field public totFreq:J

.field public totPos:J


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->termCount:J

    .line 240
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->delTermCount:J

    .line 243
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totFreq:J

    .line 246
    iput-wide v0, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->totPos:J

    .line 249
    iput-object v2, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->error:Ljava/lang/Throwable;

    .line 255
    iput-object v2, p0, Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;->blockTreeStats:Ljava/util/Map;

    .line 234
    return-void
.end method
