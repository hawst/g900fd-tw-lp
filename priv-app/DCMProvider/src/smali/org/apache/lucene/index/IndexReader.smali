.class public abstract Lorg/apache/lucene/index/IndexReader;
.super Ljava/lang/Object;
.source "IndexReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;
    }
.end annotation


# instance fields
.field private closed:Z

.field private closedByChild:Z

.field private final parentReaders:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/IndexReader;",
            ">;"
        }
    .end annotation
.end field

.field private final readerClosedListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final refCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closed:Z

    .line 77
    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closedByChild:Z

    .line 78
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 97
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    .line 100
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/IndexReader;->parentReaders:Ljava/util/Set;

    .line 81
    instance-of v0, p0, Lorg/apache/lucene/index/CompositeReader;

    if-nez v0, :cond_0

    instance-of v0, p0, Lorg/apache/lucene/index/AtomicReader;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/Error;

    const-string v1, "IndexReader should never be directly extended, subclass AtomicReader or CompositeReader instead."

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    return-void
.end method

.method private notifyReaderClosedListeners()V
    .locals 4

    .prologue
    .line 131
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    monitor-enter v2

    .line 132
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 131
    monitor-exit v2

    .line 136
    return-void

    .line 132
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .line 133
    .local v0, "listener":Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;
    invoke-interface {v0, p0}, Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;->onClose(Lorg/apache/lucene/index/IndexReader;)V

    goto :goto_0

    .line 131
    .end local v0    # "listener":Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 347
    invoke-static {p0}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 369
    invoke-static {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 336
    invoke-static {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 291
    invoke-static {p0}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;I)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 312
    invoke-static {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method private reportCloseToParentReaders()V
    .locals 5

    .prologue
    .line 139
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->parentReaders:Ljava/util/Set;

    monitor-enter v2

    .line 140
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexReader;->parentReaders:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    monitor-exit v2

    .line 148
    return-void

    .line 140
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexReader;

    .line 141
    .local v0, "parent":Lorg/apache/lucene/index/IndexReader;
    const/4 v3, 0x1

    iput-boolean v3, v0, Lorg/apache/lucene/index/IndexReader;->closedByChild:Z

    .line 143
    iget-object v3, v0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 145
    invoke-direct {v0}, Lorg/apache/lucene/index/IndexReader;->reportCloseToParentReaders()V

    goto :goto_0

    .line 139
    .end local v0    # "parent":Lorg/apache/lucene/index/IndexReader;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public final addReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .prologue
    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public final declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closed:Z

    if-nez v0, :cond_0

    .line 467
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->decRef()V

    .line 468
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    :cond_0
    monitor-exit p0

    return-void

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final decRef()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-gtz v2, :cond_0

    .line 224
    new-instance v2, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v3, "this IndexReader is closed"

    invoke-direct {v2, v3}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 227
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 228
    .local v0, "rc":I
    if-nez v0, :cond_4

    .line 229
    const/4 v1, 0x0

    .line 231
    .local v1, "success":Z
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->doClose()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    const/4 v1, 0x1

    .line 234
    if-nez v1, :cond_1

    .line 236
    iget-object v2, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 239
    :cond_1
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;->reportCloseToParentReaders()V

    .line 240
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexReader;->notifyReaderClosedListeners()V

    .line 244
    .end local v1    # "success":Z
    :cond_2
    return-void

    .line 233
    .restart local v1    # "success":Z
    :catchall_0
    move-exception v2

    .line 234
    if-nez v1, :cond_3

    .line 236
    iget-object v3, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 238
    :cond_3
    throw v2

    .line 241
    .end local v1    # "success":Z
    :cond_4
    if-gez v0, :cond_2

    .line 242
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "too many decRef calls: refCount is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " after decrement"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected abstract doClose()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract docFreq(Lorg/apache/lucene/index/Term;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final document(I)Lorg/apache/lucene/document/Document;
    .locals 2
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 435
    new-instance v0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;

    invoke-direct {v0}, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;-><init>()V

    .line 436
    .local v0, "visitor":Lorg/apache/lucene/document/DocumentStoredFieldVisitor;
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 437
    invoke-virtual {v0}, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->getDocument()Lorg/apache/lucene/document/Document;

    move-result-object v1

    return-object v1
.end method

.method public final document(ILjava/util/Set;)Lorg/apache/lucene/document/Document;
    .locals 2
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/lucene/document/Document;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 446
    .local p2, "fieldsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;

    invoke-direct {v0, p2}, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;-><init>(Ljava/util/Set;)V

    .line 447
    .local v0, "visitor":Lorg/apache/lucene/document/DocumentStoredFieldVisitor;
    invoke-virtual {p0, p1, v0}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 448
    invoke-virtual {v0}, Lorg/apache/lucene/document/DocumentStoredFieldVisitor;->getDocument()Lorg/apache/lucene/document/Document;

    move-result-object v1

    return-object v1
.end method

.method public abstract document(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gtz v0, :cond_0

    .line 252
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this IndexReader is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/IndexReader;->closedByChild:Z

    if-eqz v0, :cond_1

    .line 257
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this IndexReader cannot be used anymore as one of its child readers was closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_1
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 269
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCombinedCoreAndDeletesKey()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 520
    return-object p0
.end method

.method public abstract getContext()Lorg/apache/lucene/index/IndexReaderContext;
.end method

.method public getCoreCacheKey()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 511
    return-object p0
.end method

.method public abstract getDocCount(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getRefCount()I
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public abstract getSumDocFreq(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSumTotalTermFreq(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final getTermVector(ILjava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 2
    .param p1, "docID"    # I
    .param p2, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 386
    .local v0, "vectors":Lorg/apache/lucene/index/Fields;
    if-nez v0, :cond_0

    .line 387
    const/4 v1, 0x0

    .line 389
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    goto :goto_0
.end method

.method public abstract getTermVectors(I)Lorg/apache/lucene/index/Fields;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public hasDeletions()Z
    .locals 1

    .prologue
    .line 455
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->numDeletedDocs()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 280
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final incRef()V
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 174
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 175
    return-void
.end method

.method public final leaves()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->getContext()Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexReaderContext;->leaves()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public abstract maxDoc()I
.end method

.method public final numDeletedDocs()I
    .locals 2

    .prologue
    .line 403
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract numDocs()I
.end method

.method public final registerParentReader(Lorg/apache/lucene/index/IndexReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 126
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->parentReaders:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 128
    return-void
.end method

.method public final removeReaderClosedListener(Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;)V
    .locals 1
    .param p1, "listener"    # Lorg/apache/lucene/index/IndexReader$ReaderClosedListener;

    .prologue
    .line 115
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->ensureOpen()V

    .line 116
    iget-object v0, p0, Lorg/apache/lucene/index/IndexReader;->readerClosedListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public abstract totalTermFreq(Lorg/apache/lucene/index/Term;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final tryIncRef()Z
    .locals 3

    .prologue
    .line 202
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .local v0, "count":I
    if-gtz v0, :cond_1

    .line 207
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 203
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/IndexReader;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    const/4 v1, 0x1

    goto :goto_0
.end method
