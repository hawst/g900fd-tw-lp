.class public final Lorg/apache/lucene/index/NoDeletionPolicy;
.super Lorg/apache/lucene/index/IndexDeletionPolicy;
.source "NoDeletionPolicy.java"


# static fields
.field public static final INSTANCE:Lorg/apache/lucene/index/IndexDeletionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lorg/apache/lucene/index/NoDeletionPolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/NoDeletionPolicy;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/NoDeletionPolicy;->INSTANCE:Lorg/apache/lucene/index/IndexDeletionPolicy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexDeletionPolicy;-><init>()V

    .line 34
    return-void
.end method


# virtual methods
.method public clone()Lorg/apache/lucene/index/IndexDeletionPolicy;
    .locals 0

    .prologue
    .line 44
    return-object p0
.end method

.method public onCommit(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    return-void
.end method

.method public onInit(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    return-void
.end method
