.class Lorg/apache/lucene/index/StandardDirectoryReader$1;
.super Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
.source "StandardDirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$termInfosIndexDivisor:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/store/Directory;I)V
    .locals 0
    .param p1, "$anonymous0"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 1
    iput p2, p0, Lorg/apache/lucene/index/StandardDirectoryReader$1;->val$termInfosIndexDivisor:I

    .line 52
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method protected doBody(Ljava/lang/String;)Ljava/lang/Object;
    .locals 11
    .param p1, "segmentFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v4, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v4}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 56
    .local v4, "sis":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v0, p0, Lorg/apache/lucene/index/StandardDirectoryReader$1;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0, p1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 57
    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    new-array v2, v0, [Lorg/apache/lucene/index/SegmentReader;

    .line 58
    .local v2, "readers":[Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v0

    add-int/lit8 v8, v0, -0x1

    .local v8, "i":I
    :goto_0
    if-gez v8, :cond_0

    .line 71
    new-instance v0, Lorg/apache/lucene/index/StandardDirectoryReader;

    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader$1;->directory:Lorg/apache/lucene/store/Directory;

    const/4 v3, 0x0

    iget v5, p0, Lorg/apache/lucene/index/StandardDirectoryReader$1;->val$termInfosIndexDivisor:I

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/StandardDirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V

    return-object v0

    .line 59
    :cond_0
    const/4 v9, 0x0

    .line 60
    .local v9, "prior":Ljava/io/IOException;
    const/4 v10, 0x0

    .line 62
    .local v10, "success":Z
    :try_start_0
    new-instance v0, Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v4, v8}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v1

    iget v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader$1;->val$termInfosIndexDivisor:I

    sget-object v5, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    invoke-direct {v0, v1, v3, v5}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V

    aput-object v0, v2, v8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    const/4 v10, 0x1

    .line 67
    if-nez v10, :cond_1

    .line 68
    invoke-static {v9, v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 58
    :cond_1
    :goto_1
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 64
    :catch_0
    move-exception v7

    .line 65
    .local v7, "ex":Ljava/io/IOException;
    move-object v9, v7

    .line 67
    if-nez v10, :cond_1

    .line 68
    invoke-static {v9, v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    goto :goto_1

    .line 66
    .end local v7    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    .line 67
    if-nez v10, :cond_2

    .line 68
    invoke-static {v9, v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;[Ljava/io/Closeable;)V

    .line 69
    :cond_2
    throw v0
.end method
