.class Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;
.super Ljava/lang/Object;
.source "NumericDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/NumericDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NumericIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field final iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

.field final maxDoc:I

.field final size:I

.field final synthetic this$0:Lorg/apache/lucene/index/NumericDocValuesWriter;

.field upto:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/NumericDocValuesWriter;I)V
    .locals 2
    .param p2, "maxDoc"    # I

    .prologue
    .line 97
    iput-object p1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->this$0:Lorg/apache/lucene/index/NumericDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    # getter for: Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/NumericDocValuesWriter;->access$0(Lorg/apache/lucene/index/NumericDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    .line 93
    # getter for: Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/NumericDocValuesWriter;->access$0(Lorg/apache/lucene/index/NumericDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->size:I

    .line 98
    iput p2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->maxDoc:I

    .line 99
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 103
    iget v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->maxDoc:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 4

    .prologue
    .line 108
    invoke-virtual {p0}, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    new-instance v2, Ljava/util/NoSuchElementException;

    invoke-direct {v2}, Ljava/util/NoSuchElementException;-><init>()V

    throw v2

    .line 112
    :cond_0
    iget v2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->size:I

    if-ge v2, v3, :cond_1

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->next()J

    move-result-wide v0

    .line 117
    .local v0, "value":J
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->upto:I

    .line 119
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    return-object v2

    .line 115
    .end local v0    # "value":J
    :cond_1
    const-wide/16 v0, 0x0

    .restart local v0    # "value":J
    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
