.class Lorg/apache/lucene/index/MultiDocValues$2;
.super Lorg/apache/lucene/index/NumericDocValues;
.source "MultiDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/MultiDocValues;->getNumericValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/NumericDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$starts:[I

.field private final synthetic val$values:[Lorg/apache/lucene/index/NumericDocValues;


# direct methods
.method constructor <init>([I[Lorg/apache/lucene/index/NumericDocValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/MultiDocValues$2;->val$starts:[I

    iput-object p2, p0, Lorg/apache/lucene/index/MultiDocValues$2;->val$values:[Lorg/apache/lucene/index/NumericDocValues;

    .line 128
    invoke-direct {p0}, Lorg/apache/lucene/index/NumericDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)J
    .locals 4
    .param p1, "docID"    # I

    .prologue
    .line 131
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocValues$2;->val$starts:[I

    invoke-static {p1, v1}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v0

    .line 132
    .local v0, "subIndex":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocValues$2;->val$values:[Lorg/apache/lucene/index/NumericDocValues;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$2;->val$starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/NumericDocValues;->get(I)J

    move-result-wide v2

    return-wide v2
.end method
