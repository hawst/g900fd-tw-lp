.class public Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;
.super Lorg/apache/lucene/index/Terms;
.source "FilterAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterTerms"
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/Terms;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/Terms;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/Terms;

    .prologue
    .line 93
    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 94
    iput-object p1, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    .line 95
    return-void
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public getDocCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v0

    return v0
.end method

.method public getSumDocFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getSumDocFreq()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSumTotalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->getSumTotalTermFreq()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->hasOffsets()Z

    move-result v0

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->hasPayloads()Z

    move-result v0

    return v0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->hasPositions()Z

    move-result v0

    return v0
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 1
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v0

    return-object v0
.end method

.method public size()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterTerms;->in:Lorg/apache/lucene/index/Terms;

    invoke-virtual {v0}, Lorg/apache/lucene/index/Terms;->size()J

    move-result-wide v0

    return-wide v0
.end method
