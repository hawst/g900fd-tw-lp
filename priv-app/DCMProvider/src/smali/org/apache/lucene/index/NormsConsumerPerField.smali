.class final Lorg/apache/lucene/index/NormsConsumerPerField;
.super Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;
.source "NormsConsumerPerField.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/NormsConsumerPerField;",
        ">;"
    }
.end annotation


# instance fields
.field private consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

.field private final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field private final fieldState:Lorg/apache/lucene/index/FieldInvertState;

.field private final similarity:Lorg/apache/lucene/search/similarities/Similarity;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/index/NormsConsumer;)V
    .locals 1
    .param p1, "docInverterPerField"    # Lorg/apache/lucene/index/DocInverterPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p3, "parent"    # Lorg/apache/lucene/index/NormsConsumer;

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;-><init>()V

    .line 31
    iput-object p2, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 32
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 33
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    iput-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 34
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iput-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 35
    return-void
.end method


# virtual methods
.method abort()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/NormsConsumerPerField;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/NormsConsumerPerField;->compareTo(Lorg/apache/lucene/index/NormsConsumerPerField;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/NormsConsumerPerField;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/NormsConsumerPerField;

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method finish()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->omitsNorms()Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    sget-object v1, Lorg/apache/lucene/index/FieldInfo$DocValuesType;->NUMERIC:Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/FieldInfo;->setNormValueType(Lorg/apache/lucene/index/FieldInfo$DocValuesType;)V

    .line 47
    new-instance v0, Lorg/apache/lucene/index/NumericDocValuesWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/NumericDocValuesWriter;-><init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V

    iput-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    .line 49
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    iget-object v1, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    iget-object v2, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    iget-object v3, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/search/similarities/Similarity;->computeNorm(Lorg/apache/lucene/index/FieldInvertState;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/NumericDocValuesWriter;->addValue(IJ)V

    .line 51
    :cond_1
    return-void
.end method

.method flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
    .locals 2
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "normsWriter"    # Lorg/apache/lucene/codecs/DocValuesConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    .line 55
    .local v0, "docCount":I
    iget-object v1, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    if-nez v1, :cond_0

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/NumericDocValuesWriter;->finish(I)V

    .line 61
    iget-object v1, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    invoke-virtual {v1, p1, p2}, Lorg/apache/lucene/index/NumericDocValuesWriter;->flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V

    goto :goto_0
.end method

.method isEmpty()Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/apache/lucene/index/NormsConsumerPerField;->consumer:Lorg/apache/lucene/index/NumericDocValuesWriter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
