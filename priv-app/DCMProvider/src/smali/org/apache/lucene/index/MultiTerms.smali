.class public final Lorg/apache/lucene/index/MultiTerms;
.super Lorg/apache/lucene/index/Terms;
.source "MultiTerms.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final hasOffsets:Z

.field private final hasPayloads:Z

.field private final hasPositions:Z

.field private final subSlices:[Lorg/apache/lucene/index/ReaderSlice;

.field private final subs:[Lorg/apache/lucene/index/Terms;

.field private final termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lorg/apache/lucene/index/MultiTerms;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiTerms;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/index/Terms;[Lorg/apache/lucene/index/ReaderSlice;)V
    .locals 8
    .param p1, "subs"    # [Lorg/apache/lucene/index/Terms;
    .param p2, "subSlices"    # [Lorg/apache/lucene/index/ReaderSlice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/index/Terms;-><init>()V

    .line 51
    iput-object p1, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    .line 52
    iput-object p2, p0, Lorg/apache/lucene/index/MultiTerms;->subSlices:[Lorg/apache/lucene/index/ReaderSlice;

    .line 54
    const/4 v3, 0x0

    .line 55
    .local v3, "_termComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    sget-boolean v6, Lorg/apache/lucene/index/MultiTerms;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    array-length v6, p1

    if-gtz v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "inefficient: don\'t use MultiTerms over one sub"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 56
    :cond_0
    const/4 v0, 0x1

    .line 57
    .local v0, "_hasOffsets":Z
    const/4 v2, 0x1

    .line 58
    .local v2, "_hasPositions":Z
    const/4 v1, 0x0

    .line 59
    .local v1, "_hasPayloads":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v6, p1

    if-lt v4, v6, :cond_1

    .line 75
    iput-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->termComp:Ljava/util/Comparator;

    .line 76
    iput-boolean v0, p0, Lorg/apache/lucene/index/MultiTerms;->hasOffsets:Z

    .line 77
    iput-boolean v2, p0, Lorg/apache/lucene/index/MultiTerms;->hasPositions:Z

    .line 78
    iget-boolean v6, p0, Lorg/apache/lucene/index/MultiTerms;->hasPositions:Z

    if-eqz v6, :cond_4

    if-eqz v1, :cond_4

    const/4 v6, 0x1

    :goto_1
    iput-boolean v6, p0, Lorg/apache/lucene/index/MultiTerms;->hasPayloads:Z

    .line 79
    return-void

    .line 60
    :cond_1
    if-nez v3, :cond_3

    .line 61
    aget-object v6, p1, v4

    invoke-virtual {v6}, Lorg/apache/lucene/index/Terms;->getComparator()Ljava/util/Comparator;

    move-result-object v3

    .line 70
    :cond_2
    aget-object v6, p1, v4

    invoke-virtual {v6}, Lorg/apache/lucene/index/Terms;->hasOffsets()Z

    move-result v6

    and-int/2addr v0, v6

    .line 71
    aget-object v6, p1, v4

    invoke-virtual {v6}, Lorg/apache/lucene/index/Terms;->hasPositions()Z

    move-result v6

    and-int/2addr v2, v6

    .line 72
    aget-object v6, p1, v4

    invoke-virtual {v6}, Lorg/apache/lucene/index/Terms;->hasPayloads()Z

    move-result v6

    or-int/2addr v1, v6

    .line 59
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 65
    :cond_3
    aget-object v6, p1, v4

    invoke-virtual {v6}, Lorg/apache/lucene/index/Terms;->getComparator()Ljava/util/Comparator;

    move-result-object v5

    .line 66
    .local v5, "subTermComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    if-eqz v5, :cond_2

    invoke-interface {v5, v3}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 67
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "sub-readers have different BytesRef.Comparators; cannot merge"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 78
    .end local v5    # "subTermComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    :cond_4
    const/4 v6, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTerms;->termComp:Ljava/util/Comparator;

    return-object v0
.end method

.method public getDocCount()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 149
    const/4 v0, 0x0

    .line 150
    .local v0, "sum":I
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_0

    .line 157
    .end local v0    # "sum":I
    :goto_1
    return v0

    .line 150
    .restart local v0    # "sum":I
    :cond_0
    aget-object v1, v5, v4

    .line 151
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    invoke-virtual {v1}, Lorg/apache/lucene/index/Terms;->getDocCount()I

    move-result v2

    .line 152
    .local v2, "v":I
    if-ne v2, v3, :cond_1

    move v0, v3

    .line 153
    goto :goto_1

    .line 155
    :cond_1
    add-int/2addr v0, v2

    .line 150
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public getSumDocFreq()J
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 136
    const-wide/16 v0, 0x0

    .line 137
    .local v0, "sum":J
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    array-length v9, v8

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v9, :cond_0

    .line 144
    .end local v0    # "sum":J
    :goto_1
    return-wide v0

    .line 137
    .restart local v0    # "sum":J
    :cond_0
    aget-object v2, v8, v3

    .line 138
    .local v2, "terms":Lorg/apache/lucene/index/Terms;
    invoke-virtual {v2}, Lorg/apache/lucene/index/Terms;->getSumDocFreq()J

    move-result-wide v4

    .line 139
    .local v4, "v":J
    cmp-long v10, v4, v6

    if-nez v10, :cond_1

    move-wide v0, v6

    .line 140
    goto :goto_1

    .line 142
    :cond_1
    add-long/2addr v0, v4

    .line 137
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public getSumTotalTermFreq()J
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, -0x1

    .line 123
    const-wide/16 v0, 0x0

    .line 124
    .local v0, "sum":J
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    array-length v9, v8

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v9, :cond_0

    .line 131
    .end local v0    # "sum":J
    :goto_1
    return-wide v0

    .line 124
    .restart local v0    # "sum":J
    :cond_0
    aget-object v2, v8, v3

    .line 125
    .local v2, "terms":Lorg/apache/lucene/index/Terms;
    invoke-virtual {v2}, Lorg/apache/lucene/index/Terms;->getSumTotalTermFreq()J

    move-result-wide v4

    .line 126
    .local v4, "v":J
    cmp-long v10, v4, v6

    if-nez v10, :cond_1

    move-wide v0, v6

    .line 127
    goto :goto_1

    .line 129
    :cond_1
    add-long/2addr v0, v4

    .line 124
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lorg/apache/lucene/index/MultiTerms;->hasOffsets:Z

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lorg/apache/lucene/index/MultiTerms;->hasPayloads:Z

    return v0
.end method

.method public hasPositions()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lorg/apache/lucene/index/MultiTerms;->hasPositions:Z

    return v0
.end method

.method public intersect(Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;
    .locals 5
    .param p1, "compiled"    # Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    .param p2, "startTerm"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v2, "termsEnums":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    .line 91
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 92
    new-instance v4, Lorg/apache/lucene/index/MultiTermsEnum;

    iget-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->subSlices:[Lorg/apache/lucene/index/ReaderSlice;

    invoke-direct {v4, v3}, Lorg/apache/lucene/index/MultiTermsEnum;-><init>([Lorg/apache/lucene/index/ReaderSlice;)V

    sget-object v3, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->EMPTY_ARRAY:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/index/MultiTermsEnum;->reset([Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v3

    .line 94
    :goto_1
    return-object v3

    .line 85
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/index/Terms;->intersect(Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 86
    .local v1, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    if-eqz v1, :cond_1

    .line 87
    new-instance v3, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    invoke-direct {v3, v1, v0}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;-><init>(Lorg/apache/lucene/index/TermsEnum;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    .end local v1    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    sget-object v3, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    goto :goto_1
.end method

.method public iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .locals 5
    .param p1, "reuse"    # Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v2, "termsEnums":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    .line 109
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 110
    new-instance v4, Lorg/apache/lucene/index/MultiTermsEnum;

    iget-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->subSlices:[Lorg/apache/lucene/index/ReaderSlice;

    invoke-direct {v4, v3}, Lorg/apache/lucene/index/MultiTermsEnum;-><init>([Lorg/apache/lucene/index/ReaderSlice;)V

    sget-object v3, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->EMPTY_ARRAY:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    invoke-virtual {v4, v3}, Lorg/apache/lucene/index/MultiTermsEnum;->reset([Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v3

    .line 112
    :goto_1
    return-object v3

    .line 103
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiTerms;->subs:[Lorg/apache/lucene/index/Terms;

    aget-object v3, v3, v0

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 104
    .local v1, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    if-eqz v1, :cond_1

    .line 105
    new-instance v3, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;

    invoke-direct {v3, v1, v0}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;-><init>(Lorg/apache/lucene/index/TermsEnum;I)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    .end local v1    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    sget-object v3, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    goto :goto_1
.end method

.method public size()J
    .locals 2

    .prologue
    .line 118
    const-wide/16 v0, -0x1

    return-wide v0
.end method
