.class public abstract Lorg/apache/lucene/index/DirectoryReader;
.super Lorg/apache/lucene/index/BaseCompositeReader;
.source "DirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/BaseCompositeReader",
        "<",
        "Lorg/apache/lucene/index/AtomicReader;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DEFAULT_TERMS_INDEX_DIVISOR:I = 0x1


# instance fields
.field protected final directory:Lorg/apache/lucene/store/Directory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    .line 55
    return-void

    .line 52
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentReaders"    # [Lorg/apache/lucene/index/AtomicReader;

    .prologue
    .line 368
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/BaseCompositeReader;-><init>([Lorg/apache/lucene/index/IndexReader;)V

    .line 369
    iput-object p1, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    .line 370
    return-void
.end method

.method public static indexExists(Lorg/apache/lucene/store/Directory;)Z
    .locals 8
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 339
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 347
    .local v1, "files":[Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 348
    const-string v3, "segments_"

    .line 349
    .local v3, "prefix":Ljava/lang/String;
    array-length v6, v1

    move v5, v4

    :goto_0
    if-lt v5, v6, :cond_1

    .line 355
    .end local v1    # "files":[Ljava/lang/String;
    .end local v3    # "prefix":Ljava/lang/String;
    :cond_0
    :goto_1
    return v4

    .line 340
    :catch_0
    move-exception v2

    .line 342
    .local v2, "nsde":Lorg/apache/lucene/store/NoSuchDirectoryException;
    goto :goto_1

    .line 349
    .end local v2    # "nsde":Lorg/apache/lucene/store/NoSuchDirectoryException;
    .restart local v1    # "files":[Ljava/lang/String;
    .restart local v3    # "prefix":Ljava/lang/String;
    :cond_1
    aget-object v0, v1, v5

    .line 350
    .local v0, "file":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "segments.gen"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 351
    :cond_2
    const/4 v4, 0x1

    goto :goto_1

    .line 349
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public static listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/List;
    .locals 12
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v4

    .line 273
    .local v4, "files":[Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 275
    .local v0, "commits":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexCommit;>;"
    new-instance v7, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v7}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 276
    .local v7, "latest":Lorg/apache/lucene/index/SegmentInfos;
    invoke-virtual {v7, p0}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 277
    invoke-virtual {v7}, Lorg/apache/lucene/index/SegmentInfos;->getGeneration()J

    move-result-wide v2

    .line 279
    .local v2, "currentGen":J
    new-instance v9, Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;

    invoke-direct {v9, v7, p0}, Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v9, v4

    if-lt v6, v9, :cond_0

    .line 311
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 313
    return-object v0

    .line 283
    :cond_0
    aget-object v1, v4, v6

    .line 285
    .local v1, "fileName":Ljava/lang/String;
    const-string v9, "segments"

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 286
    const-string v9, "segments.gen"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 287
    invoke-static {v1}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v10

    cmp-long v9, v10, v2

    if-gez v9, :cond_1

    .line 289
    new-instance v8, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v8}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 293
    .local v8, "sis":Lorg/apache/lucene/index/SegmentInfos;
    :try_start_0
    invoke-virtual {v8, p0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :goto_1
    if-eqz v8, :cond_1

    .line 306
    new-instance v9, Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;

    invoke-direct {v9, v8, p0}, Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    .end local v8    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 294
    .restart local v8    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_0
    move-exception v5

    .line 302
    .local v5, "fnfe":Ljava/io/FileNotFoundException;
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 2
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p1, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 2
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static open(Lorg/apache/lucene/store/Directory;I)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/DirectoryReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged()Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 171
    .local v0, "newReader":Lorg/apache/lucene/index/DirectoryReader;
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 172
    :cond_0
    return-object v0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/DirectoryReader;
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 184
    .local v0, "newReader":Lorg/apache/lucene/index/DirectoryReader;
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 185
    :cond_0
    return-object v0
.end method

.method public static openIfChanged(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 2
    .param p0, "oldReader"    # Lorg/apache/lucene/index/DirectoryReader;
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 250
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 251
    .local v0, "newReader":Lorg/apache/lucene/index/DirectoryReader;
    sget-boolean v1, Lorg/apache/lucene/index/DirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 252
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final directory()Lorg/apache/lucene/store/Directory;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lorg/apache/lucene/index/DirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    return-object v0
.end method

.method protected abstract doOpenIfChanged()Lorg/apache/lucene/index/DirectoryReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getVersion()J
.end method

.method public abstract isCurrent()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
