.class public Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;
.super Lorg/apache/lucene/index/SortedSetDocValues;
.source "MultiDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MultiSortedSetDocValues"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field currentSubIndex:I

.field public final docStarts:[I

.field public final mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

.field public final values:[Lorg/apache/lucene/index/SortedSetDocValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 410
    const-class v0, Lorg/apache/lucene/index/MultiDocValues;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>([Lorg/apache/lucene/index/SortedSetDocValues;[ILorg/apache/lucene/index/MultiDocValues$OrdinalMap;)V
    .locals 2
    .param p1, "values"    # [Lorg/apache/lucene/index/SortedSetDocValues;
    .param p2, "docStarts"    # [I
    .param p3, "mapping"    # Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 420
    invoke-direct {p0}, Lorg/apache/lucene/index/SortedSetDocValues;-><init>()V

    .line 421
    sget-boolean v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p1

    iget-object v1, p3, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->ordDeltas:[Lorg/apache/lucene/util/packed/MonotonicAppendingLongBuffer;

    array-length v1, v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 422
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    array-length v0, p2

    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 423
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->values:[Lorg/apache/lucene/index/SortedSetDocValues;

    .line 424
    iput-object p2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->docStarts:[I

    .line 425
    iput-object p3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    .line 426
    return-void
.end method


# virtual methods
.method public getValueCount()J
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getValueCount()J

    move-result-wide v0

    return-wide v0
.end method

.method public lookupOrd(JLorg/apache/lucene/util/BytesRef;)V
    .locals 5
    .param p1, "ord"    # J
    .param p3, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 446
    iget-object v3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {v3, p1, p2}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentNumber(J)I

    move-result v2

    .line 447
    .local v2, "subIndex":I
    iget-object v3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    invoke-virtual {v3, v2, p1, p2}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getSegmentOrd(IJ)J

    move-result-wide v0

    .line 448
    .local v0, "segmentOrd":J
    iget-object v3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->values:[Lorg/apache/lucene/index/SortedSetDocValues;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0, v1, p3}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 449
    return-void
.end method

.method public nextOrd()J
    .locals 4

    .prologue
    .line 430
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->values:[Lorg/apache/lucene/index/SortedSetDocValues;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->currentSubIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lorg/apache/lucene/index/SortedSetDocValues;->nextOrd()J

    move-result-wide v0

    .line 431
    .local v0, "segmentOrd":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 434
    .end local v0    # "segmentOrd":J
    :goto_0
    return-wide v0

    .restart local v0    # "segmentOrd":J
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->mapping:Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->currentSubIndex:I

    invoke-virtual {v2, v3, v0, v1}, Lorg/apache/lucene/index/MultiDocValues$OrdinalMap;->getGlobalOrd(IJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public setDocument(I)V
    .locals 3
    .param p1, "docID"    # I

    .prologue
    .line 440
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->docStarts:[I

    invoke-static {p1, v0}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->currentSubIndex:I

    .line 441
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->values:[Lorg/apache/lucene/index/SortedSetDocValues;

    iget v1, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->currentSubIndex:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->docStarts:[I

    iget v2, p0, Lorg/apache/lucene/index/MultiDocValues$MultiSortedSetDocValues;->currentSubIndex:I

    aget v1, v1, v2

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SortedSetDocValues;->setDocument(I)V

    .line 442
    return-void
.end method
