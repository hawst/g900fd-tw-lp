.class public final enum Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
.super Ljava/lang/Enum;
.source "FilteredTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilteredTermsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "AcceptStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

.field public static final enum NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

.field public static final enum NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

.field public static final enum YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

.field public static final enum YES_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    new-instance v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    const-string v1, "YES"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;-><init>(Ljava/lang/String;I)V

    .line 53
    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 54
    new-instance v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    const-string v1, "YES_AND_SEEK"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;-><init>(Ljava/lang/String;I)V

    .line 56
    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 57
    new-instance v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    const-string v1, "NO"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;-><init>(Ljava/lang/String;I)V

    .line 58
    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 59
    new-instance v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    const-string v1, "NO_AND_SEEK"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;-><init>(Ljava/lang/String;I)V

    .line 61
    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 62
    new-instance v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    const-string v1, "END"

    invoke-direct {v0, v1, v6}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;-><init>(Ljava/lang/String;I)V

    .line 63
    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 51
    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    aput-object v1, v0, v5

    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    aput-object v1, v0, v6

    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ENUM$VALUES:[Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ENUM$VALUES:[Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
