.class final Lorg/apache/lucene/index/ByteSliceWriter;
.super Lorg/apache/lucene/store/DataOutput;
.source "ByteSliceWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field offset0:I

.field private final pool:Lorg/apache/lucene/util/ByteBlockPool;

.field private slice:[B

.field private upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/index/ByteSliceWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/util/ByteBlockPool;)V
    .locals 0
    .param p1, "pool"    # Lorg/apache/lucene/util/ByteBlockPool;

    .prologue
    .line 38
    invoke-direct {p0}, Lorg/apache/lucene/store/DataOutput;-><init>()V

    .line 39
    iput-object p1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 40
    return-void
.end method


# virtual methods
.method public getAddress()I
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->offset0:I

    and-int/lit16 v1, v1, -0x8000

    add-int/2addr v0, v1

    return v0
.end method

.method public init(I)V
    .locals 2
    .param p1, "address"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v0, v0, Lorg/apache/lucene/util/ByteBlockPool;->buffers:[[B

    shr-int/lit8 v1, p1, 0xf

    aget-object v0, v0, v1

    iput-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    .line 47
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_0
    and-int/lit16 v0, p1, 0x7fff

    iput v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    .line 49
    iput p1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->offset0:I

    .line 50
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    iget-object v1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    array-length v1, v1

    if-lt v0, v1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_1
    return-void
.end method

.method public writeByte(B)V
    .locals 3
    .param p1, "b"    # B

    .prologue
    .line 56
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 57
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    aget-byte v0, v0, v1

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/ByteBlockPool;->allocSlice([BI)I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v0, v0, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iput-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v0, v0, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    iput v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->offset0:I

    .line 61
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 63
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    iget v1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    aput-byte p1, v0, v1

    .line 64
    sget-boolean v0, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    iget-object v1, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    array-length v1, v1

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 65
    :cond_2
    return-void
.end method

.method public writeBytes([BII)V
    .locals 5
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 69
    add-int v1, p2, p3

    .local v1, "offsetEnd":I
    move v0, p2

    .line 70
    .end local p2    # "offset":I
    .local v0, "offset":I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 81
    return-void

    .line 71
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    aget-byte v2, v2, v3

    if-eqz v2, :cond_1

    .line 73
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v3, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    iget v4, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/ByteBlockPool;->allocSlice([BI)I

    move-result v2

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    .line 74
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget-object v2, v2, Lorg/apache/lucene/util/ByteBlockPool;->buffer:[B

    iput-object v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    .line 75
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    iget v2, v2, Lorg/apache/lucene/util/ByteBlockPool;->byteOffset:I

    iput v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->offset0:I

    .line 78
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    iget v3, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    add-int/lit8 p2, v0, 0x1

    .end local v0    # "offset":I
    .restart local p2    # "offset":I
    aget-byte v4, p1, v0

    aput-byte v4, v2, v3

    .line 79
    sget-boolean v2, Lorg/apache/lucene/index/ByteSliceWriter;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    iget v2, p0, Lorg/apache/lucene/index/ByteSliceWriter;->upto:I

    iget-object v3, p0, Lorg/apache/lucene/index/ByteSliceWriter;->slice:[B

    array-length v3, v3

    if-ne v2, v3, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_2
    move v0, p2

    .end local p2    # "offset":I
    .restart local v0    # "offset":I
    goto :goto_0
.end method
