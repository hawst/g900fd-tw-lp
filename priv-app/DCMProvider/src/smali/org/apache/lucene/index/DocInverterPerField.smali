.class final Lorg/apache/lucene/index/DocInverterPerField;
.super Lorg/apache/lucene/index/DocFieldConsumerPerField;
.source "DocInverterPerField.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field final fieldState:Lorg/apache/lucene/index/FieldInvertState;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocInverter;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 2
    .param p1, "parent"    # Lorg/apache/lucene/index/DocInverter;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/apache/lucene/index/DocFieldConsumerPerField;-><init>()V

    .line 46
    iput-object p2, p0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 47
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverter;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 48
    new-instance v0, Lorg/apache/lucene/index/FieldInvertState;

    iget-object v1, p2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/FieldInvertState;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    .line 49
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0, p0, p2}, Lorg/apache/lucene/index/InvertedDocConsumer;->addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    .line 50
    iget-object v0, p1, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0, p0, p2}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->addField(Lorg/apache/lucene/index/DocInverterPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    .line 51
    return-void
.end method


# virtual methods
.method abort()V
    .locals 2

    .prologue
    .line 56
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;->abort()V

    .line 60
    return-void

    .line 57
    :catchall_0
    move-exception v0

    .line 58
    iget-object v1, p0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    invoke-virtual {v1}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;->abort()V

    .line 59
    throw v0
.end method

.method getFieldInfo()Lorg/apache/lucene/index/FieldInfo;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    return-object v0
.end method

.method public processFields([Lorg/apache/lucene/index/IndexableField;I)V
    .locals 24
    .param p1, "fields"    # [Lorg/apache/lucene/index/IndexableField;
    .param p2, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/FieldInvertState;->reset()V

    .line 68
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->start([Lorg/apache/lucene/index/IndexableField;I)Z

    move-result v5

    .line 70
    .local v5, "doInvert":Z
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, p2

    if-lt v10, v0, :cond_0

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->finish()V

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;->finish()V

    .line 200
    return-void

    .line 72
    :cond_0
    aget-object v7, p1, v10

    .line 73
    .local v7, "field":Lorg/apache/lucene/index/IndexableField;
    invoke-interface {v7}, Lorg/apache/lucene/index/IndexableField;->fieldType()Lorg/apache/lucene/index/IndexableFieldType;

    move-result-object v8

    .line 78
    .local v8, "fieldType":Lorg/apache/lucene/index/IndexableFieldType;
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableFieldType;->indexed()Z

    move-result v20

    if-eqz v20, :cond_12

    if-eqz v5, :cond_12

    .line 79
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableFieldType;->tokenized()Z

    move-result v20

    if-eqz v20, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1

    const/4 v3, 0x1

    .line 82
    .local v3, "analyzed":Z
    :goto_1
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableFieldType;->omitNorms()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface {v7}, Lorg/apache/lucene/index/IndexableField;->boost()F

    move-result v20

    const/high16 v21, 0x3f800000    # 1.0f

    cmpl-float v20, v20, v21

    if-eqz v20, :cond_2

    .line 83
    new-instance v20, Ljava/lang/UnsupportedOperationException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "You cannot set an index-time boost: norms are omitted for field \'"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\'"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 79
    .end local v3    # "analyzed":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 88
    .restart local v3    # "analyzed":Z
    :cond_2
    invoke-interface {v8}, Lorg/apache/lucene/index/IndexableFieldType;->indexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v20

    sget-object v21, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_5

    const/4 v4, 0x1

    .line 89
    .local v4, "checkOffsets":Z
    :goto_2
    const/4 v11, 0x0

    .line 91
    .local v11, "lastStartOffset":I
    if-lez v10, :cond_3

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v22, v0

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/Analyzer;->getPositionIncrementGap(Ljava/lang/String;)I

    move-result v20

    :goto_3
    add-int v20, v20, v22

    move/from16 v0, v20

    move-object/from16 v1, v21

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 95
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v7, v0}, Lorg/apache/lucene/index/IndexableField;->tokenStream(Lorg/apache/lucene/analysis/Analyzer;)Lorg/apache/lucene/analysis/TokenStream;

    move-result-object v17

    .line 97
    .local v17, "stream":Lorg/apache/lucene/analysis/TokenStream;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->reset()V

    .line 99
    const/16 v19, 0x0

    .line 102
    .local v19, "success2":Z
    :try_start_0
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v9

    .line 104
    .local v9, "hasMoreTokens":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    iput-object v0, v1, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v20, v0

    const-class v21, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;

    .line 107
    .local v12, "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInvertState;->attributeSource:Lorg/apache/lucene/util/AttributeSource;

    move-object/from16 v20, v0

    const-class v21, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    invoke-virtual/range {v20 .. v21}, Lorg/apache/lucene/util/AttributeSource;->addAttribute(Ljava/lang/Class;)Lorg/apache/lucene/util/Attribute;

    move-result-object v14

    check-cast v14, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;

    .line 109
    .local v14, "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    if-eqz v9, :cond_11

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->start(Lorg/apache/lucene/index/IndexableField;)V

    .line 120
    :cond_4
    invoke-interface {v14}, Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;->getPositionIncrement()I

    move-result v13

    .line 121
    .local v13, "posIncr":I
    if-gez v13, :cond_7

    .line 122
    new-instance v20, Ljava/lang/IllegalArgumentException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "position increment must be >=0 (got "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    .end local v9    # "hasMoreTokens":Z
    .end local v12    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v13    # "posIncr":I
    .end local v14    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    :catchall_0
    move-exception v20

    .line 182
    if-nez v19, :cond_14

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    .line 183
    aput-object v17, v21, v22

    invoke-static/range {v21 .. v21}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 187
    :goto_4
    throw v20

    .line 88
    .end local v4    # "checkOffsets":Z
    .end local v11    # "lastStartOffset":I
    .end local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .end local v19    # "success2":Z
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 92
    .restart local v4    # "checkOffsets":Z
    .restart local v11    # "lastStartOffset":I
    :cond_6
    const/16 v20, 0x0

    goto/16 :goto_3

    .line 124
    .restart local v9    # "hasMoreTokens":Z
    .restart local v12    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .restart local v13    # "posIncr":I
    .restart local v14    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v19    # "success2":Z
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v20, v0

    if-nez v20, :cond_8

    if-nez v13, :cond_8

    .line 125
    new-instance v20, Ljava/lang/IllegalArgumentException;

    const-string v21, "first position increment must be > 0 (got 0)"

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 127
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v20, v0

    add-int v15, v20, v13

    .line 128
    .local v15, "position":I
    if-lez v15, :cond_c

    .line 131
    add-int/lit8 v15, v15, -0x1

    .line 138
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput v15, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 140
    if-nez v13, :cond_a

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->numOverlap:I

    .line 143
    :cond_a
    if-eqz v4, :cond_f

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v20, v0

    invoke-interface {v12}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->startOffset()I

    move-result v21

    add-int v16, v20, v21

    .line 145
    .local v16, "startOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v20, v0

    invoke-interface {v12}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v21

    add-int v6, v20, v21

    .line 146
    .local v6, "endOffset":I
    if-ltz v16, :cond_b

    move/from16 v0, v16

    if-ge v6, v0, :cond_d

    .line 147
    :cond_b
    new-instance v20, Ljava/lang/IllegalArgumentException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "startOffset must be non-negative, and endOffset must be >= startOffset, startOffset="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 148
    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",endOffset="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 147
    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 132
    .end local v6    # "endOffset":I
    .end local v16    # "startOffset":I
    :cond_c
    if-gez v15, :cond_9

    .line 133
    new-instance v20, Ljava/lang/IllegalArgumentException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "position overflow for field \'"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/lucene/index/IndexableField;->name()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\'"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 150
    .restart local v6    # "endOffset":I
    .restart local v16    # "startOffset":I
    :cond_d
    move/from16 v0, v16

    if-ge v0, v11, :cond_e

    .line 151
    new-instance v20, Ljava/lang/IllegalArgumentException;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "offsets must not go backwards startOffset="

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 152
    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " is < lastStartOffset="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 151
    invoke-direct/range {v20 .. v21}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    :cond_e
    move/from16 v11, v16

    .line 157
    .end local v6    # "endOffset":I
    .end local v16    # "startOffset":I
    :cond_f
    const/16 v18, 0x0

    .line 165
    .local v18, "success":Z
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/InvertedDocConsumerPerField;->add()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166
    const/16 v18, 0x1

    .line 168
    if-nez v18, :cond_10

    .line 169
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->setAborting()V

    .line 172
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->length:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->length:I

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->position:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->position:I

    .line 174
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->incrementToken()Z

    move-result v20

    if-nez v20, :cond_4

    .line 177
    .end local v13    # "posIncr":I
    .end local v15    # "position":I
    .end local v18    # "success":Z
    :cond_11
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->end()V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v21, v0

    invoke-interface {v12}, Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;->endOffset()I

    move-result v22

    add-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 180
    const/16 v19, 0x1

    .line 182
    if-nez v19, :cond_15

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    .line 183
    aput-object v17, v20, v21

    invoke-static/range {v20 .. v20}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 189
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    move/from16 v22, v0

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/apache/lucene/analysis/Analyzer;->getOffsetGap(Ljava/lang/String;)I

    move-result v20

    :goto_6
    add-int v20, v20, v22

    move/from16 v0, v20

    move-object/from16 v1, v21

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->offset:I

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->fieldState:Lorg/apache/lucene/index/FieldInvertState;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    move/from16 v21, v0

    invoke-interface {v7}, Lorg/apache/lucene/index/IndexableField;->boost()F

    move-result v22

    mul-float v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lorg/apache/lucene/index/FieldInvertState;->boost:F

    .line 195
    .end local v3    # "analyzed":Z
    .end local v4    # "checkOffsets":Z
    .end local v9    # "hasMoreTokens":Z
    .end local v11    # "lastStartOffset":I
    .end local v12    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v14    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .end local v19    # "success2":Z
    :cond_12
    const/16 v20, 0x0

    aput-object v20, p1, v10

    .line 70
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 167
    .restart local v3    # "analyzed":Z
    .restart local v4    # "checkOffsets":Z
    .restart local v9    # "hasMoreTokens":Z
    .restart local v11    # "lastStartOffset":I
    .restart local v12    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .restart local v13    # "posIncr":I
    .restart local v14    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .restart local v15    # "position":I
    .restart local v17    # "stream":Lorg/apache/lucene/analysis/TokenStream;
    .restart local v18    # "success":Z
    .restart local v19    # "success2":Z
    :catchall_1
    move-exception v20

    .line 168
    if-nez v18, :cond_13

    .line 169
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocInverterPerField;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->setAborting()V

    .line 171
    :cond_13
    throw v20
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 185
    .end local v9    # "hasMoreTokens":Z
    .end local v12    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .end local v13    # "posIncr":I
    .end local v14    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    .end local v15    # "position":I
    .end local v18    # "success":Z
    :cond_14
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    goto/16 :goto_4

    .restart local v9    # "hasMoreTokens":Z
    .restart local v12    # "offsetAttribute":Lorg/apache/lucene/analysis/tokenattributes/OffsetAttribute;
    .restart local v14    # "posIncrAttribute":Lorg/apache/lucene/analysis/tokenattributes/PositionIncrementAttribute;
    :cond_15
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/analysis/TokenStream;->close()V

    goto :goto_5

    .line 189
    :cond_16
    const/16 v20, 0x0

    goto :goto_6
.end method
