.class Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
.super Ljava/lang/Object;
.source "DocumentsWriterDeleteQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Node"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final nextUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final item:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field volatile next:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 324
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    const-class v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    const-string v2, "next"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    .line 323
    sput-object v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->nextUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 324
    return-void
.end method

.method constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p0, "this":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<TT;>;"
    .local p1, "item":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->item:Ljava/lang/Object;

    .line 320
    return-void
.end method


# virtual methods
.method apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    .locals 2
    .param p1, "bufferedDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "docIDUpto"    # I

    .prologue
    .line 327
    .local p0, "this":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<TT;>;"
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sentinel item must never be applied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method casNext(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 331
    .local p0, "this":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<TT;>;"
    .local p1, "cmp":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    .local p2, "val":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    sget-object v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->nextUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v0, p0, p1, p2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
