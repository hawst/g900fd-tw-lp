.class final Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermArrayNode;
.super Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
.source "DocumentsWriterDeleteQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TermArrayNode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
        "<[",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>([Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "term"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;-><init>(Ljava/lang/Object;)V

    .line 368
    return-void
.end method


# virtual methods
.method apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    .locals 4
    .param p1, "bufferedDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "docIDUpto"    # I

    .prologue
    .line 372
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermArrayNode;->item:Ljava/lang/Object;

    check-cast v1, [Lorg/apache/lucene/index/Term;

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 375
    return-void

    .line 372
    :cond_0
    aget-object v0, v1, v2

    .line 373
    .local v0, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {p1, v0, p2}, Lorg/apache/lucene/index/BufferedDeletes;->addTerm(Lorg/apache/lucene/index/Term;I)V

    .line 372
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "dels="

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermArrayNode;->item:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
