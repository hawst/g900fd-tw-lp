.class public final enum Lorg/apache/lucene/index/StoredFieldVisitor$Status;
.super Ljava/lang/Enum;
.source "StoredFieldVisitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/StoredFieldVisitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/StoredFieldVisitor$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

.field public static final enum NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

.field public static final enum STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

.field public static final enum YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    const-string v1, "YES"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;-><init>(Ljava/lang/String;I)V

    .line 90
    sput-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    .line 91
    new-instance v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    const-string v1, "NO"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;-><init>(Ljava/lang/String;I)V

    .line 92
    sput-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    .line 93
    new-instance v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/StoredFieldVisitor$Status;-><init>(Ljava/lang/String;I)V

    .line 94
    sput-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    .line 88
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->YES:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->NO:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->STOP:Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    aput-object v1, v0, v4

    sput-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ENUM$VALUES:[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/StoredFieldVisitor$Status;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/index/StoredFieldVisitor$Status;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/index/StoredFieldVisitor$Status;->ENUM$VALUES:[Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/index/StoredFieldVisitor$Status;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
