.class public final Lorg/apache/lucene/index/Term;
.super Ljava/lang/Object;
.source "Term.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# instance fields
.field bytes:Lorg/apache/lucene/util/BytesRef;

.field field:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "fld"    # Ljava/lang/String;

    .prologue
    .line 68
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "fld"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 58
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, p2}, Lorg/apache/lucene/util/BytesRef;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "fld"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 52
    return-void
.end method

.method public static final toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;
    .locals 5
    .param p0, "termText"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 86
    sget-object v2, Lorg/apache/lucene/util/IOUtils;->CHARSET_UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v2

    .line 87
    sget-object v3, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v2, v3}, Ljava/nio/charset/CharsetDecoder;->onMalformedInput(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v2

    .line 88
    sget-object v3, Ljava/nio/charset/CodingErrorAction;->REPORT:Ljava/nio/charset/CodingErrorAction;

    invoke-virtual {v2, v3}, Ljava/nio/charset/CharsetDecoder;->onUnmappableCharacter(Ljava/nio/charset/CodingErrorAction;)Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 90
    .local v0, "decoder":Ljava/nio/charset/CharsetDecoder;
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v3, p0, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v4, p0, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v2, v3, v4}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 92
    :goto_0
    return-object v2

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/nio/charset/CharacterCodingException;
    invoke-virtual {p0}, Lorg/apache/lucene/util/BytesRef;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public final bytes()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/Term;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v0

    return v0
.end method

.method public final compareTo(Lorg/apache/lucene/index/Term;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 140
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v1, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    if-ne p0, p1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v1

    .line 103
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 104
    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 106
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 107
    check-cast v0, Lorg/apache/lucene/index/Term;

    .line 108
    .local v0, "other":Lorg/apache/lucene/index/Term;
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 109
    iget-object v3, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 110
    goto :goto_0

    .line 111
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v4, v0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 112
    goto :goto_0

    .line 113
    :cond_5
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_6

    .line 114
    iget-object v3, v0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    if-eqz v3, :cond_0

    move v1, v2

    .line 115
    goto :goto_0

    .line 116
    :cond_6
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 117
    goto :goto_0
.end method

.method public final field()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 123
    const/16 v0, 0x1f

    .line 124
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 125
    .local v1, "result":I
    iget-object v2, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 126
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    if-nez v4, :cond_1

    :goto_1
    add-int v1, v2, v3

    .line 127
    return v1

    .line 125
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 126
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3}, Lorg/apache/lucene/util/BytesRef;->hashCode()I

    move-result v3

    goto :goto_1
.end method

.method final set(Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "fld"    # Ljava/lang/String;
    .param p2, "bytes"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 152
    iput-object p1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 153
    iput-object p2, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    .line 154
    return-void
.end method

.method public final text()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-static {v0}, Lorg/apache/lucene/index/Term;->toString(Lorg/apache/lucene/util/BytesRef;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/lucene/index/Term;->text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
