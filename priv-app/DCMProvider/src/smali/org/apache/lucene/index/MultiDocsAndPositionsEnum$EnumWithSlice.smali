.class public final Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;
.super Ljava/lang/Object;
.source "MultiDocsAndPositionsEnum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnumWithSlice"
.end annotation


# instance fields
.field public docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

.field public slice:Lorg/apache/lucene/index/ReaderSlice;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ReaderSlice;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
