.class Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "SortedSetDocValuesTermsEnum.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private currentOrd:J

.field private final term:Lorg/apache/lucene/util/BytesRef;

.field private final values:Lorg/apache/lucene/index/SortedSetDocValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SortedSetDocValues;)V
    .locals 2
    .param p1, "values"    # Lorg/apache/lucene/index/SortedSetDocValues;

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    .line 32
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 37
    return-void
.end method


# virtual methods
.method public docFreq()I
    .locals 1

    .prologue
    .line 109
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    invoke-static {}, Lorg/apache/lucene/util/BytesRef;->getUTF8SortedAsUnicodeComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iget-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    .line 90
    iget-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    .line 93
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    iget-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 94
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    iget-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 6
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v0

    .line 42
    .local v0, "ord":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 43
    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    .line 44
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    const/4 v3, 0x0

    iput v3, v2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 48
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v3, v3, [B

    iput-object v3, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 49
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 50
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 58
    :goto_0
    return-object v2

    .line 52
    :cond_0
    neg-long v2, v0

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    .line 53
    iget-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    iget-object v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 54
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0

    .line 57
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    iget-wide v4, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    iget-object v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v4, v5, v3}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 58
    sget-object v2, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto :goto_0
.end method

.method public seekExact(J)V
    .locals 5
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    sget-boolean v0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SortedSetDocValues;->getValueCount()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_1
    long-to-int v0, p1

    int-to-long v0, v0

    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    .line 84
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    iget-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupOrd(JLorg/apache/lucene/util/BytesRef;)V

    .line 85
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "state"    # Lorg/apache/lucene/index/TermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    sget-boolean v0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    instance-of v0, p2, Lorg/apache/lucene/index/OrdTermState;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 135
    :cond_1
    check-cast p2, Lorg/apache/lucene/index/OrdTermState;

    .end local p2    # "state":Lorg/apache/lucene/index/TermState;
    iget-wide v0, p2, Lorg/apache/lucene/index/OrdTermState;->ord:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->seekExact(J)V

    .line 136
    return-void
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 6
    .param p1, "text"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 65
    iget-object v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->values:Lorg/apache/lucene/index/SortedSetDocValues;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/SortedSetDocValues;->lookupTerm(Lorg/apache/lucene/util/BytesRef;)J

    move-result-wide v0

    .line 66
    .local v0, "ord":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    .line 67
    iget-object v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iput v2, v3, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 71
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    new-array v3, v3, [B

    iput-object v3, v2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 72
    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 73
    iput-wide v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    .line 74
    const/4 v2, 0x1

    .line 76
    :cond_0
    return v2
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->term:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    new-instance v0, Lorg/apache/lucene/index/OrdTermState;

    invoke-direct {v0}, Lorg/apache/lucene/index/OrdTermState;-><init>()V

    .line 141
    .local v0, "state":Lorg/apache/lucene/index/OrdTermState;
    iget-wide v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesTermsEnum;->currentOrd:J

    iput-wide v2, v0, Lorg/apache/lucene/index/OrdTermState;->ord:J

    .line 142
    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2

    .prologue
    .line 114
    const-wide/16 v0, -0x1

    return-wide v0
.end method
