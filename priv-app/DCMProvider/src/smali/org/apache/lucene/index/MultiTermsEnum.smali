.class public final Lorg/apache/lucene/index/MultiTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "MultiTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;,
        Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;,
        Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private current:Lorg/apache/lucene/util/BytesRef;

.field private final currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

.field private lastSeek:Lorg/apache/lucene/util/BytesRef;

.field private lastSeekExact:Z

.field private final lastSeekScratch:Lorg/apache/lucene/util/BytesRef;

.field private numSubs:I

.field private numTop:I

.field private final queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

.field private final subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

.field private final subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

.field private final subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

.field private termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lorg/apache/lucene/index/MultiTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/index/ReaderSlice;)V
    .locals 4
    .param p1, "slices"    # [Lorg/apache/lucene/index/ReaderSlice;

    .prologue
    .line 77
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 45
    new-instance v1, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v1}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekScratch:Lorg/apache/lucene/util/BytesRef;

    .line 78
    new-instance v1, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    array-length v2, p1

    invoke-direct {v1, v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;-><init>(I)V

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    .line 79
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    .line 80
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    .line 81
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    .line 82
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    .line 83
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 90
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    .line 91
    return-void

    .line 84
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    new-instance v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v3, p1, v0

    invoke-direct {v2, v0, v3}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;-><init>(ILorg/apache/lucene/index/ReaderSlice;)V

    aput-object v2, v1, v0

    .line 85
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    new-instance v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    invoke-direct {v2}, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;-><init>()V

    aput-object v2, v1, v0

    .line 86
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iput-object v2, v1, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    .line 87
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    new-instance v2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    invoke-direct {v2}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;-><init>()V

    aput-object v2, v1, v0

    .line 88
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iput-object v2, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private pullTop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 279
    sget-boolean v0, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 281
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iget v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aput-object v0, v1, v2

    .line 282
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iget-object v0, v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v1, v1, v3

    iget-object v1, v1, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/BytesRef;->bytesEquals(Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v0, v0, v3

    iget-object v0, v0, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 287
    return-void
.end method

.method private pushTop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 291
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lt v0, v1, :cond_0

    .line 299
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    .line 300
    return-void

    .line 292
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v2, v2, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    .line 293
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    if-eqz v1, :cond_1

    .line 294
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    iget-object v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public docFreq()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    const/4 v1, 0x0

    .line 333
    .local v1, "sum":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lt v0, v2, :cond_0

    .line 336
    return v1

    .line 334
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v2, v2, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v2

    add-int/2addr v1, v2

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 11
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 356
    if-eqz p2, :cond_1

    instance-of v8, p2, Lorg/apache/lucene/index/MultiDocsEnum;

    if-eqz v8, :cond_1

    move-object v1, p2

    .line 357
    check-cast v1, Lorg/apache/lucene/index/MultiDocsEnum;

    .line 359
    .local v1, "docsEnum":Lorg/apache/lucene/index/MultiDocsEnum;
    invoke-virtual {v1, p0}, Lorg/apache/lucene/index/MultiDocsEnum;->canReuse(Lorg/apache/lucene/index/MultiTermsEnum;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 360
    new-instance v1, Lorg/apache/lucene/index/MultiDocsEnum;

    .end local v1    # "docsEnum":Lorg/apache/lucene/index/MultiDocsEnum;
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v8, v8

    invoke-direct {v1, p0, v8}, Lorg/apache/lucene/index/MultiDocsEnum;-><init>(Lorg/apache/lucene/index/MultiTermsEnum;I)V

    .line 367
    .restart local v1    # "docsEnum":Lorg/apache/lucene/index/MultiDocsEnum;
    :cond_0
    :goto_0
    instance-of v8, p1, Lorg/apache/lucene/index/MultiBits;

    if-eqz v8, :cond_2

    move-object v4, p1

    .line 368
    check-cast v4, Lorg/apache/lucene/index/MultiBits;

    .line 373
    .local v4, "multiLiveDocs":Lorg/apache/lucene/index/MultiBits;
    :goto_1
    const/4 v7, 0x0

    .line 375
    .local v7, "upto":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lt v3, v8, :cond_3

    .line 415
    if-nez v7, :cond_a

    .line 416
    const/4 v8, 0x0

    .line 418
    :goto_3
    return-object v8

    .line 363
    .end local v1    # "docsEnum":Lorg/apache/lucene/index/MultiDocsEnum;
    .end local v3    # "i":I
    .end local v4    # "multiLiveDocs":Lorg/apache/lucene/index/MultiBits;
    .end local v7    # "upto":I
    :cond_1
    new-instance v1, Lorg/apache/lucene/index/MultiDocsEnum;

    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v8, v8

    invoke-direct {v1, p0, v8}, Lorg/apache/lucene/index/MultiDocsEnum;-><init>(Lorg/apache/lucene/index/MultiTermsEnum;I)V

    .restart local v1    # "docsEnum":Lorg/apache/lucene/index/MultiDocsEnum;
    goto :goto_0

    .line 370
    :cond_2
    const/4 v4, 0x0

    .restart local v4    # "multiLiveDocs":Lorg/apache/lucene/index/MultiBits;
    goto :goto_1

    .line 377
    .restart local v3    # "i":I
    .restart local v7    # "upto":I
    :cond_3
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v2, v8, v3

    .line 381
    .local v2, "entry":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    if-eqz v4, :cond_5

    .line 387
    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v8

    invoke-virtual {v4, v8}, Lorg/apache/lucene/index/MultiBits;->getMatchingSub(Lorg/apache/lucene/index/ReaderSlice;)Lorg/apache/lucene/index/MultiBits$SubResult;

    move-result-object v5

    .line 388
    .local v5, "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    iget-boolean v8, v5, Lorg/apache/lucene/index/MultiBits$SubResult;->matches:Z

    if-eqz v8, :cond_4

    .line 389
    iget-object v0, v5, Lorg/apache/lucene/index/MultiBits$SubResult;->result:Lorg/apache/lucene/util/Bits;

    .line 402
    .end local v5    # "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    .local v0, "b":Lorg/apache/lucene/util/Bits;
    :goto_4
    sget-boolean v8, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v8, :cond_7

    iget v8, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    iget-object v9, v1, Lorg/apache/lucene/index/MultiDocsEnum;->subDocsEnum:[Lorg/apache/lucene/index/DocsEnum;

    array-length v9, v9

    if-lt v8, v9, :cond_7

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    iget v10, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " vs "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v1, Lorg/apache/lucene/index/MultiDocsEnum;->subDocsEnum:[Lorg/apache/lucene/index/DocsEnum;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 393
    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    .restart local v5    # "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    :cond_4
    new-instance v0, Lorg/apache/lucene/index/BitsSlice;

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v8

    invoke-direct {v0, p1, v8}, Lorg/apache/lucene/index/BitsSlice;-><init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/ReaderSlice;)V

    .line 395
    .restart local v0    # "b":Lorg/apache/lucene/util/Bits;
    goto :goto_4

    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    .end local v5    # "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    :cond_5
    if-eqz p1, :cond_6

    .line 396
    new-instance v0, Lorg/apache/lucene/index/BitsSlice;

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v8

    invoke-direct {v0, p1, v8}, Lorg/apache/lucene/index/BitsSlice;-><init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/ReaderSlice;)V

    .line 397
    .restart local v0    # "b":Lorg/apache/lucene/util/Bits;
    goto :goto_4

    .line 399
    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "b":Lorg/apache/lucene/util/Bits;
    goto :goto_4

    .line 403
    :cond_7
    iget-object v8, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    iget-object v9, v1, Lorg/apache/lucene/index/MultiDocsEnum;->subDocsEnum:[Lorg/apache/lucene/index/DocsEnum;

    iget v10, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v0, v9, p3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v6

    .line 404
    .local v6, "subDocsEnum":Lorg/apache/lucene/index/DocsEnum;
    if-eqz v6, :cond_9

    .line 405
    iget-object v8, v1, Lorg/apache/lucene/index/MultiDocsEnum;->subDocsEnum:[Lorg/apache/lucene/index/DocsEnum;

    iget v9, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    aput-object v6, v8, v9

    .line 406
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    aget-object v8, v8, v7

    iput-object v6, v8, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 407
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    aget-object v8, v8, v7

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v9

    iput-object v9, v8, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    .line 408
    add-int/lit8 v7, v7, 0x1

    .line 375
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 411
    :cond_9
    sget-boolean v8, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v8, :cond_8

    new-instance v8, Ljava/lang/AssertionError;

    const-string v9, "One of our subs cannot provide a docsenum"

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 418
    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    .end local v2    # "entry":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    .end local v6    # "subDocsEnum":Lorg/apache/lucene/index/DocsEnum;
    :cond_a
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    invoke-virtual {v1, v8, v7}, Lorg/apache/lucene/index/MultiDocsEnum;->reset([Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;I)Lorg/apache/lucene/index/MultiDocsEnum;

    move-result-object v8

    goto/16 :goto_3
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 12
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 426
    if-eqz p2, :cond_1

    instance-of v9, p2, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    if-eqz v9, :cond_1

    move-object v1, p2

    .line 427
    check-cast v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    .line 429
    .local v1, "docsAndPositionsEnum":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    invoke-virtual {v1, p0}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->canReuse(Lorg/apache/lucene/index/MultiTermsEnum;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 430
    new-instance v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    .end local v1    # "docsAndPositionsEnum":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v9, v9

    invoke-direct {v1, p0, v9}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/index/MultiTermsEnum;I)V

    .line 437
    .restart local v1    # "docsAndPositionsEnum":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    :cond_0
    :goto_0
    instance-of v9, p1, Lorg/apache/lucene/index/MultiBits;

    if-eqz v9, :cond_2

    move-object v4, p1

    .line 438
    check-cast v4, Lorg/apache/lucene/index/MultiBits;

    .line 443
    .local v4, "multiLiveDocs":Lorg/apache/lucene/index/MultiBits;
    :goto_1
    const/4 v7, 0x0

    .line 445
    .local v7, "upto":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lt v3, v9, :cond_3

    .line 491
    if-nez v7, :cond_a

    .line 494
    :goto_3
    return-object v8

    .line 433
    .end local v1    # "docsAndPositionsEnum":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    .end local v3    # "i":I
    .end local v4    # "multiLiveDocs":Lorg/apache/lucene/index/MultiBits;
    .end local v7    # "upto":I
    :cond_1
    new-instance v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v9, v9

    invoke-direct {v1, p0, v9}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;-><init>(Lorg/apache/lucene/index/MultiTermsEnum;I)V

    .restart local v1    # "docsAndPositionsEnum":Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;
    goto :goto_0

    .line 440
    :cond_2
    const/4 v4, 0x0

    .restart local v4    # "multiLiveDocs":Lorg/apache/lucene/index/MultiBits;
    goto :goto_1

    .line 447
    .restart local v3    # "i":I
    .restart local v7    # "upto":I
    :cond_3
    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v2, v9, v3

    .line 451
    .local v2, "entry":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    if-eqz v4, :cond_5

    .line 457
    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v9, v9, v3

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v9}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v9

    invoke-virtual {v4, v9}, Lorg/apache/lucene/index/MultiBits;->getMatchingSub(Lorg/apache/lucene/index/ReaderSlice;)Lorg/apache/lucene/index/MultiBits$SubResult;

    move-result-object v5

    .line 458
    .local v5, "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    iget-boolean v9, v5, Lorg/apache/lucene/index/MultiBits$SubResult;->matches:Z

    if-eqz v9, :cond_4

    .line 459
    iget-object v0, v5, Lorg/apache/lucene/index/MultiBits$SubResult;->result:Lorg/apache/lucene/util/Bits;

    .line 473
    .end local v5    # "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    .local v0, "b":Lorg/apache/lucene/util/Bits;
    :goto_4
    sget-boolean v9, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v9, :cond_7

    iget v9, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    iget-object v10, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subDocsAndPositionsEnum:[Lorg/apache/lucene/index/DocsAndPositionsEnum;

    array-length v10, v10

    if-lt v9, v10, :cond_7

    new-instance v8, Ljava/lang/AssertionError;

    new-instance v9, Ljava/lang/StringBuilder;

    iget v10, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, " vs "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subDocsAndPositionsEnum:[Lorg/apache/lucene/index/DocsAndPositionsEnum;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v8

    .line 464
    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    .restart local v5    # "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    :cond_4
    new-instance v0, Lorg/apache/lucene/index/BitsSlice;

    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v9, v9, v3

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v9}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v9

    invoke-direct {v0, p1, v9}, Lorg/apache/lucene/index/BitsSlice;-><init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/ReaderSlice;)V

    .line 466
    .restart local v0    # "b":Lorg/apache/lucene/util/Bits;
    goto :goto_4

    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    .end local v5    # "sub":Lorg/apache/lucene/index/MultiBits$SubResult;
    :cond_5
    if-eqz p1, :cond_6

    .line 467
    new-instance v0, Lorg/apache/lucene/index/BitsSlice;

    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v9, v9, v3

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v9}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v9

    invoke-direct {v0, p1, v9}, Lorg/apache/lucene/index/BitsSlice;-><init>(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/ReaderSlice;)V

    .line 468
    .restart local v0    # "b":Lorg/apache/lucene/util/Bits;
    goto :goto_4

    .line 470
    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "b":Lorg/apache/lucene/util/Bits;
    goto :goto_4

    .line 474
    :cond_7
    iget-object v9, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    iget-object v10, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subDocsAndPositionsEnum:[Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iget v11, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v0, v10, p3}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v6

    .line 476
    .local v6, "subPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    if-eqz v6, :cond_9

    .line 477
    iget-object v9, v1, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->subDocsAndPositionsEnum:[Lorg/apache/lucene/index/DocsAndPositionsEnum;

    iget v10, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->index:I

    aput-object v6, v9, v10

    .line 478
    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    aget-object v9, v9, v7

    iput-object v6, v9, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->docsAndPositionsEnum:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 479
    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    aget-object v9, v9, v7

    # getter for: Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->subSlice:Lorg/apache/lucene/index/ReaderSlice;
    invoke-static {v2}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->access$0(Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;)Lorg/apache/lucene/index/ReaderSlice;

    move-result-object v10

    iput-object v10, v9, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    .line 480
    add-int/lit8 v7, v7, 0x1

    .line 445
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 482
    :cond_9
    iget-object v9, v2, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v8, v10}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v9

    if-eqz v9, :cond_8

    goto/16 :goto_3

    .line 494
    .end local v0    # "b":Lorg/apache/lucene/util/Bits;
    .end local v2    # "entry":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    .end local v6    # "subPostings":Lorg/apache/lucene/index/DocsAndPositionsEnum;
    :cond_a
    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subDocsAndPositions:[Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;

    invoke-virtual {v1, v8, v7}, Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;->reset([Lorg/apache/lucene/index/MultiDocsAndPositionsEnum$EnumWithSlice;I)Lorg/apache/lucene/index/MultiDocsAndPositionsEnum;

    move-result-object v8

    goto/16 :goto_3
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    return-object v0
.end method

.method public getMatchArray()[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    return-object v0
.end method

.method public getMatchCount()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    return v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-boolean v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekExact:Z

    if-eqz v1, :cond_1

    .line 311
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/MultiTermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v0

    .line 312
    .local v0, "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    sget-boolean v1, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    sget-object v1, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 313
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekExact:Z

    .line 315
    .end local v0    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_1
    iput-object v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    .line 318
    invoke-direct {p0}, Lorg/apache/lucene/index/MultiTermsEnum;->pushTop()V

    .line 321
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v1}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 322
    invoke-direct {p0}, Lorg/apache/lucene/index/MultiTermsEnum;->pullTop()V

    .line 327
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    return-object v1

    .line 324
    :cond_2
    iput-object v2, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0
.end method

.method public ord()J
    .locals 1

    .prologue
    .line 273
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public reset([Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;)Lorg/apache/lucene/index/TermsEnum;
    .locals 8
    .param p1, "termsEnumsIndex"    # [Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 106
    sget-boolean v5, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    array-length v5, p1

    iget-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    array-length v6, v6

    if-le v5, v6, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 107
    :cond_0
    iput v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numSubs:I

    .line 108
    iput v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    .line 109
    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    .line 110
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->clear()V

    .line 111
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p1

    if-lt v1, v5, :cond_2

    .line 139
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 140
    sget-object p0, Lorg/apache/lucene/index/TermsEnum;->EMPTY:Lorg/apache/lucene/index/TermsEnum;

    .line 142
    .end local p0    # "this":Lorg/apache/lucene/index/MultiTermsEnum;
    :cond_1
    return-object p0

    .line 113
    .restart local p0    # "this":Lorg/apache/lucene/index/MultiTermsEnum;
    :cond_2
    aget-object v4, p1, v1

    .line 114
    .local v4, "termsEnumIndex":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;
    sget-boolean v5, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_3

    if-nez v4, :cond_3

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 117
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    if-nez v5, :cond_6

    .line 118
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    iget-object v6, v4, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    iput-object v6, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->termComp:Ljava/util/Comparator;

    .line 128
    :cond_4
    iget-object v5, v4, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v3

    .line 129
    .local v3, "term":Lorg/apache/lucene/util/BytesRef;
    if-eqz v3, :cond_5

    .line 130
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iget v6, v4, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->subIndex:I

    aget-object v0, v5, v6

    .line 131
    .local v0, "entry":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    iget-object v5, v4, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, v5, v3}, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->reset(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/BytesRef;)V

    .line 132
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v5, v0}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iget v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numSubs:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numSubs:I

    aput-object v0, v5, v6

    .line 111
    .end local v0    # "entry":Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    .end local v3    # "term":Lorg/apache/lucene/util/BytesRef;
    :cond_6
    iget-object v5, v4, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumIndex;->termsEnum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v5}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v2

    .line 123
    .local v2, "subTermComp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lorg/apache/lucene/util/BytesRef;>;"
    if-eqz v2, :cond_4

    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    invoke-interface {v2, v5}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 124
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sub-readers have different BytesRef.Comparators: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " vs "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; cannot merge"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 8
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 199
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->clear()V

    .line 200
    iput v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    .line 201
    iput-boolean v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekExact:Z

    .line 203
    const/4 v3, 0x0

    .line 204
    .local v3, "seekOpt":Z
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v5, v6, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-gtz v5, :cond_0

    .line 205
    const/4 v3, 0x1

    .line 208
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekScratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v5, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 209
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekScratch:Lorg/apache/lucene/util/BytesRef;

    iput-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    .line 211
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numSubs:I

    if-lt v2, v5, :cond_1

    .line 252
    iget v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lez v5, :cond_9

    .line 254
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 262
    :goto_1
    return-object v5

    .line 219
    :cond_1
    if-eqz v3, :cond_5

    .line 220
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    iget-object v1, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    .line 221
    .local v1, "curTerm":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_4

    .line 222
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    invoke-interface {v5, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 223
    .local v0, "cmp":I
    if-nez v0, :cond_2

    .line 224
    sget-object v4, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 237
    .end local v0    # "cmp":I
    .end local v1    # "curTerm":Lorg/apache/lucene/util/BytesRef;
    .local v4, "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :goto_2
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v4, v5, :cond_6

    .line 238
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iget v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v7, v7, v2

    aput-object v7, v5, v6

    .line 239
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    iget-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v6, v6, v2

    iget-object v6, v6, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    iput-object v6, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    iput-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 211
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 225
    .end local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .restart local v0    # "cmp":I
    .restart local v1    # "curTerm":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    if-gez v0, :cond_3

    .line 226
    sget-object v4, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 227
    .restart local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    goto :goto_2

    .line 228
    .end local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    iget-object v5, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v4

    .line 230
    .restart local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    goto :goto_2

    .line 231
    .end local v0    # "cmp":I
    .end local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_4
    sget-object v4, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    .line 233
    .restart local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    goto :goto_2

    .line 234
    .end local v1    # "curTerm":Lorg/apache/lucene/util/BytesRef;
    .end local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    iget-object v5, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v5, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v4

    .restart local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    goto :goto_2

    .line 241
    :cond_6
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v4, v5, :cond_8

    .line 242
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    iget-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v6, v6, v2

    iget-object v6, v6, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v6}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v6

    iput-object v6, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    .line 243
    sget-boolean v5, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    iget-object v5, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    if-nez v5, :cond_7

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 244
    :cond_7
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    iget-object v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v6, v6, v2

    invoke-virtual {v5, v6}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->add(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 247
    :cond_8
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v5, v5, v2

    const/4 v6, 0x0

    iput-object v6, v5, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    goto :goto_3

    .line 255
    .end local v4    # "status":Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    :cond_9
    iget-object v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v5}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->size()I

    move-result v5

    if-lez v5, :cond_a

    .line 259
    invoke-direct {p0}, Lorg/apache/lucene/index/MultiTermsEnum;->pullTop()V

    .line 260
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->NOT_FOUND:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_1

    .line 262
    :cond_a
    sget-object v5, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    goto/16 :goto_1
.end method

.method public seekExact(J)V
    .locals 1
    .param p1, "ord"    # J

    .prologue
    .line 268
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 10
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 148
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->queue:Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;

    invoke-virtual {v7}, Lorg/apache/lucene/index/MultiTermsEnum$TermMergeQueue;->clear()V

    .line 149
    iput v6, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    .line 151
    const/4 v3, 0x0

    .line 152
    .local v3, "seekOpt":Z
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v7, v8, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v7

    if-gtz v7, :cond_0

    .line 153
    const/4 v3, 0x1

    .line 156
    :cond_0
    const/4 v7, 0x0

    iput-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeek:Lorg/apache/lucene/util/BytesRef;

    .line 157
    iput-boolean v5, p0, Lorg/apache/lucene/index/MultiTermsEnum;->lastSeekExact:Z

    .line 159
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numSubs:I

    if-lt v2, v7, :cond_1

    .line 194
    iget v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lez v7, :cond_7

    :goto_1
    return v5

    .line 167
    :cond_1
    if-eqz v3, :cond_5

    .line 168
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v7, v7, v2

    iget-object v1, v7, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    .line 169
    .local v1, "curTerm":Lorg/apache/lucene/util/BytesRef;
    if-eqz v1, :cond_4

    .line 170
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->termComp:Ljava/util/Comparator;

    invoke-interface {v7, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 171
    .local v0, "cmp":I
    if-nez v0, :cond_2

    .line 172
    const/4 v4, 0x1

    .line 185
    .end local v0    # "cmp":I
    .end local v1    # "curTerm":Lorg/apache/lucene/util/BytesRef;
    .local v4, "status":Z
    :goto_2
    if-eqz v4, :cond_6

    .line 186
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    iget v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    iget-object v9, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v9, v9, v2

    aput-object v9, v7, v8

    .line 187
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v7, v7, v2

    iget-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v8, v8, v2

    iget-object v8, v8, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v8}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v8

    iput-object v8, v7, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    iput-object v8, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    .line 188
    sget-boolean v7, Lorg/apache/lucene/index/MultiTermsEnum;->$assertionsDisabled:Z

    if-nez v7, :cond_6

    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v7, v7, v2

    iget-object v7, v7, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->current:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p1, v7}, Lorg/apache/lucene/util/BytesRef;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 173
    .end local v4    # "status":Z
    .restart local v0    # "cmp":I
    .restart local v1    # "curTerm":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    if-gez v0, :cond_3

    .line 174
    const/4 v4, 0x0

    .line 175
    .restart local v4    # "status":Z
    goto :goto_2

    .line 176
    .end local v4    # "status":Z
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v7, v7, v2

    iget-object v7, v7, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v7, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    .line 178
    .restart local v4    # "status":Z
    goto :goto_2

    .line 179
    .end local v0    # "cmp":I
    .end local v4    # "status":Z
    :cond_4
    const/4 v4, 0x0

    .line 181
    .restart local v4    # "status":Z
    goto :goto_2

    .line 182
    .end local v1    # "curTerm":Lorg/apache/lucene/util/BytesRef;
    .end local v4    # "status":Z
    :cond_5
    iget-object v7, p0, Lorg/apache/lucene/index/MultiTermsEnum;->currentSubs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v7, v7, v2

    iget-object v7, v7, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v7, p1, p2}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v4

    .restart local v4    # "status":Z
    goto :goto_2

    .line 159
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v4    # "status":Z
    :cond_7
    move v5, v6

    .line 194
    goto :goto_1
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lorg/apache/lucene/index/MultiTermsEnum;->current:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MultiTermsEnum("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->subs:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 341
    const-wide/16 v2, 0x0

    .line 342
    .local v2, "sum":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->numTop:I

    if-lt v0, v1, :cond_1

    move-wide v4, v2

    .line 349
    :cond_0
    return-wide v4

    .line 343
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/MultiTermsEnum;->top:[Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorg/apache/lucene/index/MultiTermsEnum$TermsEnumWithSlice;->terms:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v4

    .line 344
    .local v4, "v":J
    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 347
    add-long/2addr v2, v4

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
