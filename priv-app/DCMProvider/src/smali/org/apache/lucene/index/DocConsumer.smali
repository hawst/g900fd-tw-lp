.class abstract Lorg/apache/lucene/index/DocConsumer;
.super Ljava/lang/Object;
.source "DocConsumer.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract abort()V
.end method

.method abstract doAfterFlush()V
.end method

.method abstract finishDocument()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract flush(Lorg/apache/lucene/index/SegmentWriteState;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract processDocument(Lorg/apache/lucene/index/FieldInfos$Builder;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
