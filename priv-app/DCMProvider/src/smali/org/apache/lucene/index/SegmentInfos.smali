.class public final Lorg/apache/lucene/index/SegmentInfos;
.super Ljava/lang/Object;
.source "SegmentInfos.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final FORMAT_SEGMENTS_GEN_CURRENT:I = -0x2

.field private static final SEGMENT_INFO_UPGRADE_CODEC:Ljava/lang/String; = "SegmentInfo3xUpgrade"

.field private static final SEGMENT_INFO_UPGRADE_VERSION:I

.field public static final VERSION_40:I

.field private static defaultGenLookaheadCount:I

.field private static infoStream:Ljava/io/PrintStream;


# instance fields
.field public counter:I

.field private generation:J

.field private lastGeneration:J

.field pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

.field private segments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation
.end field

.field public userData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public version:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    const-class v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    .line 148
    const/4 v0, 0x0

    sput-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    .line 564
    const/16 v0, 0xa

    sput v0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    .line 156
    return-void
.end method

.method static synthetic access$1()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method static synthetic access$2(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 605
    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->message(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3()I
    .locals 1

    .prologue
    .line 564
    sget v0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    return v0
.end method

.method public static generationFromSegmentsFileName(Ljava/lang/String;)J
    .locals 3
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 239
    const-string v0, "segments"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-wide/16 v0, 0x0

    .line 242
    :goto_0
    return-wide v0

    .line 241
    :cond_0
    const-string v0, "segments"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    const-string v0, "segments"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 243
    const/16 v1, 0x24

    .line 242
    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    goto :goto_0

    .line 245
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fileName \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is not a segments file"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getDefaultGenLookahedCount()I
    .locals 1

    .prologue
    .line 587
    sget v0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    return v0
.end method

.method public static getInfoStream()Ljava/io/PrintStream;
    .locals 1

    .prologue
    .line 596
    sget-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method public static getLastCommitGeneration(Lorg/apache/lucene/store/Directory;)J
    .locals 4
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/store/Directory;->listAll()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration([Ljava/lang/String;)J
    :try_end_0
    .catch Lorg/apache/lucene/store/NoSuchDirectoryException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 196
    :goto_0
    return-wide v2

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "nsde":Lorg/apache/lucene/store/NoSuchDirectoryException;
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public static getLastCommitGeneration([Ljava/lang/String;)J
    .locals 8
    .param p0, "files"    # [Ljava/lang/String;

    .prologue
    .line 171
    if-nez p0, :cond_1

    .line 172
    const-wide/16 v4, -0x1

    .line 183
    :cond_0
    return-wide v4

    .line 174
    :cond_1
    const-wide/16 v4, -0x1

    .line 175
    .local v4, "max":J
    array-length v6, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_0

    aget-object v0, p0, v1

    .line 176
    .local v0, "file":Ljava/lang/String;
    const-string v7, "segments"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "segments.gen"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 177
    invoke-static {v0}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v2

    .line 178
    .local v2, "gen":J
    cmp-long v7, v2, v4

    if-lez v7, :cond_2

    .line 179
    move-wide v4, v2

    .line 175
    .end local v2    # "gen":J
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getLastCommitSegmentsFileName(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;
    .locals 4
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    const-string v0, "segments"

    .line 221
    const-string v1, ""

    .line 222
    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration(Lorg/apache/lucene/store/Directory;)J

    move-result-wide v2

    .line 220
    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastCommitSegmentsFileName([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "files"    # [Ljava/lang/String;

    .prologue
    .line 208
    const-string v0, "segments"

    .line 209
    const-string v1, ""

    .line 210
    invoke-static {p0}, Lorg/apache/lucene/index/SegmentInfos;->getLastCommitGeneration([Ljava/lang/String;)J

    move-result-wide v2

    .line 208
    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static message(Ljava/lang/String;)V
    .locals 3
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 606
    sget-object v0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SIS ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 607
    return-void
.end method

.method private static segmentWasUpgraded(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;)Z
    .locals 7
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 453
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v5, "upgraded"

    const-string v6, "si"

    invoke-static {v4, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 454
    .local v1, "markerFileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 456
    .local v0, "in":Lorg/apache/lucene/store/IndexInput;
    :try_start_0
    sget-object v4, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {p0, v1, v4}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v0

    .line 457
    const-string v4, "SegmentInfo3xUpgrade"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v0, v4, v5, v6}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeader(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_3

    .line 464
    if-eqz v0, :cond_0

    new-array v4, v2, [Ljava/io/Closeable;

    .line 465
    aput-object v0, v4, v3

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 468
    :cond_0
    :goto_0
    return v2

    .line 460
    :catch_0
    move-exception v4

    .line 464
    if-eqz v0, :cond_1

    new-array v2, v2, [Ljava/io/Closeable;

    .line 465
    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    :cond_1
    :goto_1
    move v2, v3

    .line 468
    goto :goto_0

    .line 463
    :catchall_0
    move-exception v4

    .line 464
    if-eqz v0, :cond_2

    new-array v2, v2, [Ljava/io/Closeable;

    .line 465
    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 467
    :cond_2
    throw v4

    .line 464
    :cond_3
    if-eqz v0, :cond_1

    new-array v2, v2, [Ljava/io/Closeable;

    .line 465
    aput-object v0, v2, v3

    invoke-static {v2}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method public static setDefaultGenLookaheadCount(I)V
    .locals 0
    .param p0, "count"    # I

    .prologue
    .line 576
    sput p0, Lorg/apache/lucene/index/SegmentInfos;->defaultGenLookaheadCount:I

    .line 577
    return-void
.end method

.method public static setInfoStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p0, "infoStream"    # Ljava/io/PrintStream;

    .prologue
    .line 559
    sput-object p0, Lorg/apache/lucene/index/SegmentInfos;->infoStream:Ljava/io/PrintStream;

    .line 560
    return-void
.end method

.method private write(Lorg/apache/lucene/store/Directory;)V
    .locals 18
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 362
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->getNextSegmentFileName()Ljava/lang/String;

    move-result-object v6

    .line 365
    .local v6, "segmentsFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v16, -0x1

    cmp-long v14, v14, v16

    if-nez v14, :cond_2

    .line 366
    const-wide/16 v14, 0x1

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 371
    :goto_0
    const/4 v7, 0x0

    .line 372
    .local v7, "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    const/4 v11, 0x0

    .line 374
    .local v11, "success":Z
    new-instance v12, Ljava/util/HashSet;

    invoke-direct {v12}, Ljava/util/HashSet;-><init>()V

    .line 377
    .local v12, "upgradedSIFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :try_start_0
    new-instance v8, Lorg/apache/lucene/store/ChecksumIndexOutput;

    sget-object v14, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v14

    invoke-direct {v8, v14}, Lorg/apache/lucene/store/ChecksumIndexOutput;-><init>(Lorg/apache/lucene/store/IndexOutput;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 378
    .end local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .local v8, "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :try_start_1
    const-string v14, "segments"

    const/4 v15, 0x0

    invoke-static {v8, v14, v15}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 379
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    invoke-virtual {v8, v14, v15}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeLong(J)V

    .line 380
    move-object/from16 v0, p0

    iget v14, v0, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    invoke-virtual {v8, v14}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeInt(I)V

    .line 381
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v14

    invoke-virtual {v8, v14}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeInt(I)V

    .line 382
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_3

    .line 423
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    invoke-virtual {v8, v14}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 424
    move-object/from16 v0, p0

    iput-object v8, v0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 425
    const/4 v11, 0x1

    .line 427
    if-nez v11, :cond_1

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/io/Closeable;

    const/4 v15, 0x0

    .line 430
    aput-object v8, v14, v15

    invoke-static {v14}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 432
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_9

    .line 443
    :try_start_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    .line 449
    :cond_1
    :goto_3
    return-void

    .line 368
    .end local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .end local v11    # "success":Z
    .end local v12    # "upgradedSIFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v16, 0x1

    add-long v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    goto :goto_0

    .line 382
    .restart local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v11    # "success":Z
    .restart local v12    # "upgradedSIFiles":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    :try_start_3
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 383
    .local v10, "siPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v9, v10, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    .line 384
    .local v9, "si":Lorg/apache/lucene/index/SegmentInfo;
    iget-object v15, v9, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v15}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeString(Ljava/lang/String;)V

    .line 385
    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v15

    invoke-virtual {v15}, Lorg/apache/lucene/codecs/Codec;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeString(Ljava/lang/String;)V

    .line 386
    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeLong(J)V

    .line 387
    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v15

    invoke-virtual {v8, v15}, Lorg/apache/lucene/store/ChecksumIndexOutput;->writeInt(I)V

    .line 388
    sget-boolean v15, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v15, :cond_5

    iget-object v15, v9, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    move-object/from16 v0, p1

    if-eq v15, v0, :cond_5

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 426
    .end local v9    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v10    # "siPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catchall_0
    move-exception v14

    move-object v7, v8

    .line 427
    .end local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :goto_4
    if-nez v11, :cond_4

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/io/Closeable;

    const/16 v16, 0x0

    .line 430
    aput-object v7, v15, v16

    invoke-static {v15}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 432
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-nez v16, :cond_8

    .line 443
    :try_start_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    .line 448
    :cond_4
    :goto_6
    throw v14

    .line 390
    .end local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v9    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .restart local v10    # "siPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_5
    :try_start_5
    sget-boolean v15, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v15, :cond_6

    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v15

    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v16

    move/from16 v0, v16

    if-le v15, v0, :cond_6

    new-instance v14, Ljava/lang/AssertionError;

    invoke-direct {v14}, Ljava/lang/AssertionError;-><init>()V

    throw v14

    .line 394
    :cond_6
    invoke-virtual {v9}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v13

    .line 395
    .local v13, "version":Ljava/lang/String;
    if-eqz v13, :cond_7

    invoke-static {}, Lorg/apache/lucene/util/StringHelper;->getVersionComparator()Ljava/util/Comparator;

    move-result-object v15

    const-string v16, "4.0"

    move-object/from16 v0, v16

    invoke-interface {v15, v13, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v15

    if-gez v15, :cond_0

    .line 397
    :cond_7
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lorg/apache/lucene/index/SegmentInfos;->segmentWasUpgraded(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 399
    iget-object v15, v9, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v16, "upgraded"

    const-string v17, "si"

    invoke-static/range {v15 .. v17}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 400
    .local v3, "markerFileName":Ljava/lang/String;
    invoke-virtual {v9, v3}, Lorg/apache/lucene/index/SegmentInfo;->addFile(Ljava/lang/String;)V

    .line 402
    sget-object v15, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, p1

    invoke-static {v0, v9, v15}, Lorg/apache/lucene/index/SegmentInfos;->write3xInfo(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Ljava/lang/String;

    move-result-object v5

    .line 403
    .local v5, "segmentFileName":Ljava/lang/String;
    invoke-interface {v12, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 411
    invoke-virtual {v9, v3}, Lorg/apache/lucene/index/SegmentInfo;->addFile(Ljava/lang/String;)V

    .line 412
    sget-object v15, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v4

    .line 414
    .local v4, "out":Lorg/apache/lucene/store/IndexOutput;
    :try_start_6
    const-string v15, "SegmentInfo3xUpgrade"

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v4, v15, v0}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 416
    :try_start_7
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 418
    invoke-interface {v12, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 419
    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    goto/16 :goto_1

    .line 415
    :catchall_1
    move-exception v14

    .line 416
    invoke-virtual {v4}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 417
    throw v14
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 432
    .end local v3    # "markerFileName":Ljava/lang/String;
    .end local v4    # "out":Lorg/apache/lucene/store/IndexOutput;
    .end local v5    # "segmentFileName":Ljava/lang/String;
    .end local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .end local v9    # "si":Lorg/apache/lucene/index/SegmentInfo;
    .end local v10    # "siPerCommit":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v13    # "version":Ljava/lang/String;
    .restart local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :cond_8
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 434
    .local v2, "fileName":Ljava/lang/String;
    :try_start_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_5

    .line 435
    :catch_0
    move-exception v16

    goto/16 :goto_5

    .line 432
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :cond_9
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 434
    .restart local v2    # "fileName":Ljava/lang/String;
    :try_start_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1

    goto/16 :goto_2

    .line 435
    :catch_1
    move-exception v15

    goto/16 :goto_2

    .line 444
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :catch_2
    move-exception v15

    goto/16 :goto_6

    .end local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :catch_3
    move-exception v14

    goto/16 :goto_3

    .line 426
    .end local v8    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    .restart local v7    # "segnOutput":Lorg/apache/lucene/store/ChecksumIndexOutput;
    :catchall_2
    move-exception v14

    goto/16 :goto_4
.end method

.method public static write3xInfo(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Ljava/lang/String;
    .locals 8
    .param p0, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 476
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    const-string v5, ""

    const-string v6, "si"

    invoke-static {v3, v5, v6}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 477
    .local v0, "fileName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/SegmentInfo;->addFile(Ljava/lang/String;)V

    .line 480
    const/4 v2, 0x0

    .line 481
    .local v2, "success":Z
    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;

    move-result-object v1

    .line 485
    .local v1, "output":Lorg/apache/lucene/store/IndexOutput;
    :try_start_0
    sget-boolean v3, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/lucene/codecs/lucene3x/Lucene3xCodec;

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    const-string v5, "broken test, trying to mix preflex with other codecs"

    invoke-direct {v3, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    :catchall_0
    move-exception v3

    .line 502
    if-nez v2, :cond_0

    new-array v4, v4, [Ljava/io/Closeable;

    .line 503
    aput-object v1, v4, v7

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 505
    :try_start_1
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 510
    :cond_0
    :goto_0
    throw v3

    .line 486
    :cond_1
    :try_start_2
    const-string v3, "Lucene3xSegmentInfo"

    .line 487
    const/4 v5, 0x0

    .line 486
    invoke-static {v1, v3, v5}, Lorg/apache/lucene/codecs/CodecUtil;->writeHeader(Lorg/apache/lucene/store/DataOutput;Ljava/lang/String;I)V

    .line 489
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeString(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 492
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->attributes()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 494
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_1
    int-to-byte v3, v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeByte(B)V

    .line 495
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->getDiagnostics()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeStringStringMap(Ljava/util/Map;)V

    .line 496
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/lucene/store/IndexOutput;->writeStringSet(Ljava/util/Set;)V

    .line 498
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 500
    const/4 v2, 0x1

    .line 502
    if-nez v2, :cond_2

    new-array v3, v4, [Ljava/io/Closeable;

    .line 503
    aput-object v1, v3, v7

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 505
    :try_start_3
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v3, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 512
    :cond_2
    :goto_2
    return-object v0

    .line 494
    :cond_3
    const/4 v3, -0x1

    goto :goto_1

    .line 506
    :catch_0
    move-exception v3

    goto :goto_2

    :catch_1
    move-exception v4

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 1123
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1124
    return-void
.end method

.method public addAll(Ljava/lang/Iterable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1128
    .local p1, "sis":Ljava/lang/Iterable;, "Ljava/lang/Iterable<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1131
    return-void

    .line 1128
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1129
    .local v0, "si":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    goto :goto_0
.end method

.method applyMergeChanges(Lorg/apache/lucene/index/MergePolicy$OneMerge;Z)V
    .locals 9
    .param p1, "merge"    # Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .param p2, "dropSegment"    # Z

    .prologue
    .line 1059
    new-instance v3, Ljava/util/HashSet;

    iget-object v6, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-direct {v3, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1060
    .local v3, "mergedAway":Ljava/util/Set;, "Ljava/util/Set<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/4 v2, 0x0

    .line 1061
    .local v2, "inserted":Z
    const/4 v4, 0x0

    .line 1062
    .local v4, "newSegIdx":I
    const/4 v5, 0x0

    .local v5, "segIdx":I
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .local v0, "cnt":I
    :goto_0
    if-lt v5, v0, :cond_1

    .line 1078
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    iget-object v7, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v6, v4, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 1085
    if-nez v2, :cond_0

    if-nez p2, :cond_0

    .line 1086
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    const/4 v7, 0x0

    iget-object v8, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-interface {v6, v7, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1088
    :cond_0
    return-void

    .line 1063
    :cond_1
    sget-boolean v6, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v6, :cond_2

    if-ge v5, v4, :cond_2

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .line 1064
    :cond_2
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1065
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1066
    if-nez v2, :cond_3

    if-nez p2, :cond_3

    .line 1067
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    iget-object v7, p1, Lorg/apache/lucene/index/MergePolicy$OneMerge;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-interface {v6, v5, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1068
    const/4 v2, 0x1

    .line 1069
    add-int/lit8 v4, v4, 0x1

    .line 1062
    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1072
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v6, v4, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1073
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public asList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1113
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public changed()V
    .locals 4

    .prologue
    .line 1054
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 1055
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 1135
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1136
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->clone()Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/SegmentInfos;
    .locals 5

    .prologue
    .line 523
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfos;

    .line 525
    .local v2, "sis":Lorg/apache/lucene/index/SegmentInfos;
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, v2, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    .line 526
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 531
    new-instance v3, Ljava/util/HashMap;

    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v3, v2, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 532
    return-object v2

    .line 526
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 527
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    sget-boolean v4, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v4, :cond_1

    iget-object v4, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    .end local v1    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v2    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "should not happen"

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 529
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v1    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v2    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->clone()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method final commit(Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 997
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->prepareCommit(Lorg/apache/lucene/store/Directory;)V

    .line 998
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->finishCommit(Lorg/apache/lucene/store/Directory;)V

    .line 999
    return-void
.end method

.method contains(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 1158
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method createBackupSegmentInfos()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1091
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1092
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1096
    return-object v1

    .line 1092
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1093
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    sget-boolean v3, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    iget-object v3, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 1094
    :cond_1
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->clone()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public files(Lorg/apache/lucene/store/Directory;Z)Ljava/util/Collection;
    .locals 6
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "includeSegmentsFile"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Z)",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 890
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 891
    .local v0, "files":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 892
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v3

    .line 893
    .local v3, "segmentFileName":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 898
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 901
    .end local v3    # "segmentFileName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v4

    .line 902
    .local v4, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 909
    return-object v0

    .line 903
    :cond_1
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    .line 904
    .local v2, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    sget-boolean v5, Lorg/apache/lucene/index/SegmentInfos;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    iget-object v5, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-eq v5, p1, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 905
    :cond_2
    iget-object v5, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-ne v5, p1, :cond_3

    .line 906
    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 902
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method final finishCommit(Lorg/apache/lucene/store/Directory;)V
    .locals 8
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 913
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    if-nez v4, :cond_0

    .line 914
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "prepareCommit was not called"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 916
    :cond_0
    const/4 v2, 0x0

    .line 918
    .local v2, "success":Z
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ChecksumIndexOutput;->finishCommit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 919
    const/4 v2, 0x1

    .line 921
    if-nez v2, :cond_5

    .line 923
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 950
    :goto_0
    const-string v4, "segments"

    const-string v5, ""

    iget-wide v6, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-static {v4, v5, v6, v7}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 951
    .local v0, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 953
    :try_start_1
    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 954
    const/4 v2, 0x1

    .line 956
    if-nez v2, :cond_1

    .line 958
    :try_start_2
    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    .line 965
    :cond_1
    :goto_1
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    iput-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 968
    :try_start_3
    const-string v4, "segments.gen"

    sget-object v5, Lorg/apache/lucene/store/IOContext;->READONCE:Lorg/apache/lucene/store/IOContext;

    invoke-virtual {p1, v4, v5}, Lorg/apache/lucene/store/Directory;->createOutput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexOutput;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v1

    .line 970
    .local v1, "genOutput":Lorg/apache/lucene/store/IndexOutput;
    const/4 v4, -0x2

    :try_start_4
    invoke-virtual {v1, v4}, Lorg/apache/lucene/store/IndexOutput;->writeInt(I)V

    .line 971
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V

    .line 972
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    invoke-virtual {v1, v4, v5}, Lorg/apache/lucene/store/IndexOutput;->writeLong(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 974
    :try_start_5
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 975
    const-string v4, "segments.gen"

    invoke-static {v4}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 987
    .end local v1    # "genOutput":Lorg/apache/lucene/store/IndexOutput;
    :goto_2
    return-void

    .line 920
    .end local v0    # "fileName":Ljava/lang/String;
    :catchall_0
    move-exception v4

    .line 921
    if-nez v2, :cond_2

    .line 923
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 938
    :goto_3
    throw v4

    .line 925
    :cond_2
    const/4 v2, 0x0

    .line 927
    :try_start_6
    iget-object v5, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {v5}, Lorg/apache/lucene/store/ChecksumIndexOutput;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 928
    const/4 v2, 0x1

    .line 930
    if-nez v2, :cond_4

    .line 932
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    goto :goto_3

    .line 929
    :catchall_1
    move-exception v4

    .line 930
    if-nez v2, :cond_3

    .line 932
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 936
    :goto_4
    throw v4

    .line 934
    :cond_3
    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    goto :goto_4

    :cond_4
    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    goto :goto_3

    .line 925
    :cond_5
    const/4 v2, 0x0

    .line 927
    :try_start_7
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    invoke-virtual {v4}, Lorg/apache/lucene/store/ChecksumIndexOutput;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 928
    const/4 v2, 0x1

    .line 930
    if-nez v2, :cond_7

    .line 932
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    goto :goto_0

    .line 929
    :catchall_2
    move-exception v4

    .line 930
    if-nez v2, :cond_6

    .line 932
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->rollbackCommit(Lorg/apache/lucene/store/Directory;)V

    .line 936
    :goto_5
    throw v4

    .line 934
    :cond_6
    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    goto :goto_5

    :cond_7
    iput-object v6, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    goto :goto_0

    .line 955
    .restart local v0    # "fileName":Ljava/lang/String;
    :catchall_3
    move-exception v4

    .line 956
    if-nez v2, :cond_8

    .line 958
    :try_start_8
    invoke-virtual {p1, v0}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2

    .line 963
    :cond_8
    :goto_6
    throw v4

    .line 973
    .restart local v1    # "genOutput":Lorg/apache/lucene/store/IndexOutput;
    :catchall_4
    move-exception v4

    .line 974
    :try_start_9
    invoke-virtual {v1}, Lorg/apache/lucene/store/IndexOutput;->close()V

    .line 975
    const-string v5, "segments.gen"

    invoke-static {v5}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/apache/lucene/store/Directory;->sync(Ljava/util/Collection;)V

    .line 976
    throw v4
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0

    .line 977
    .end local v1    # "genOutput":Lorg/apache/lucene/store/IndexOutput;
    :catch_0
    move-exception v3

    .line 981
    .local v3, "t":Ljava/lang/Throwable;
    :try_start_a
    const-string v4, "segments.gen"

    invoke-virtual {p1, v4}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_1

    goto :goto_2

    .line 982
    :catch_1
    move-exception v4

    goto :goto_2

    .line 959
    .end local v3    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v5

    goto :goto_6

    :catch_3
    move-exception v4

    goto/16 :goto_1
.end method

.method public getGeneration()J
    .locals 2

    .prologue
    .line 547
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    return-wide v0
.end method

.method public getLastGeneration()J
    .locals 2

    .prologue
    .line 552
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    return-wide v0
.end method

.method public getNextSegmentFileName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 256
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 257
    const-wide/16 v0, 0x1

    .line 261
    .local v0, "nextGeneration":J
    :goto_0
    const-string v2, "segments"

    .line 262
    const-string v3, ""

    .line 261
    invoke-static {v2, v3, v0, v1}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 259
    .end local v0    # "nextGeneration":J
    :cond_0
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    .restart local v0    # "nextGeneration":J
    goto :goto_0
.end method

.method public getSegmentsFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 229
    const-string v0, "segments"

    .line 230
    const-string v1, ""

    .line 231
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 229
    invoke-static {v0, v1, v2, v3}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserData()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1021
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 542
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->version:J

    return-wide v0
.end method

.method indexOf(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 1166
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1108
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method final prepareCommit(Lorg/apache/lucene/store/Directory;)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 878
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    if-eqz v0, :cond_0

    .line 879
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "prepareCommit was already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 881
    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->write(Lorg/apache/lucene/store/Directory;)V

    .line 882
    return-void
.end method

.method public final read(Lorg/apache/lucene/store/Directory;)V
    .locals 2
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 341
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 343
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos$1;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/SegmentInfos$1;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    .line 350
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos$1;->run()Ljava/lang/Object;

    .line 351
    return-void
.end method

.method public final read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V
    .locals 22
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "segmentFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 276
    const/16 v18, 0x0

    .line 279
    .local v18, "success":Z
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 281
    invoke-static/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->generationFromSegmentsFileName(Ljava/lang/String;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 283
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 285
    new-instance v14, Lorg/apache/lucene/store/ChecksumIndexInput;

    sget-object v19, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/store/Directory;->openInput(Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/store/IndexInput;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Lorg/apache/lucene/store/ChecksumIndexInput;-><init>(Lorg/apache/lucene/store/IndexInput;)V

    .line 287
    .local v14, "input":Lorg/apache/lucene/store/ChecksumIndexInput;
    :try_start_0
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v12

    .line 288
    .local v12, "format":I
    const v19, 0x3fd76c17

    move/from16 v0, v19

    if-ne v12, v0, :cond_5

    .line 290
    const-string v19, "segments"

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v14, v0, v1, v2}, Lorg/apache/lucene/codecs/CodecUtil;->checkHeaderNoMagic(Lorg/apache/lucene/store/DataInput;Ljava/lang/String;II)I

    .line 291
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readLong()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/SegmentInfos;->version:J

    .line 292
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/SegmentInfos;->counter:I

    .line 293
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v15

    .line 294
    .local v15, "numSegments":I
    if-gez v15, :cond_0

    .line 295
    new-instance v19, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "invalid segment count: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (resource: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 326
    .end local v12    # "format":I
    .end local v15    # "numSegments":I
    :catchall_0
    move-exception v19

    .line 327
    if-nez v18, :cond_7

    .line 330
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    .line 331
    aput-object v14, v20, v21

    invoke-static/range {v20 .. v20}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 335
    :goto_0
    throw v19

    .line 297
    .restart local v12    # "format":I
    .restart local v15    # "numSegments":I
    :cond_0
    const/16 v16, 0x0

    .local v16, "seg":I
    :goto_1
    move/from16 v0, v16

    if-lt v0, v15, :cond_2

    .line 310
    :try_start_1
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readStringStringMap()Ljava/util/Map;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 319
    .end local v15    # "numSegments":I
    .end local v16    # "seg":I
    :cond_1
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->getChecksum()J

    move-result-wide v4

    .line 320
    .local v4, "checksumNow":J
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readLong()J

    move-result-wide v6

    .line 321
    .local v6, "checksumThen":J
    cmp-long v19, v4, v6

    if-eqz v19, :cond_6

    .line 322
    new-instance v19, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "checksum mismatch in segments file (resource: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 298
    .end local v4    # "checksumNow":J
    .end local v6    # "checksumThen":J
    .restart local v15    # "numSegments":I
    .restart local v16    # "seg":I
    :cond_2
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readString()Ljava/lang/String;

    move-result-object v17

    .line 299
    .local v17, "segName":Ljava/lang/String;
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/codecs/Codec;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/Codec;

    move-result-object v8

    .line 301
    .local v8, "codec":Lorg/apache/lucene/codecs/Codec;
    invoke-virtual {v8}, Lorg/apache/lucene/codecs/Codec;->segmentInfoFormat()Lorg/apache/lucene/codecs/SegmentInfoFormat;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/codecs/SegmentInfoFormat;->getSegmentInfoReader()Lorg/apache/lucene/codecs/SegmentInfoReader;

    move-result-object v19

    sget-object v20, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/codecs/SegmentInfoReader;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v13

    .line 302
    .local v13, "info":Lorg/apache/lucene/index/SegmentInfo;
    invoke-virtual {v13, v8}, Lorg/apache/lucene/index/SegmentInfo;->setCodec(Lorg/apache/lucene/codecs/Codec;)V

    .line 303
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readLong()J

    move-result-wide v10

    .line 304
    .local v10, "delGen":J
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->readInt()I

    move-result v9

    .line 305
    .local v9, "delCount":I
    if-ltz v9, :cond_3

    invoke-virtual {v13}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v19

    move/from16 v0, v19

    if-le v9, v0, :cond_4

    .line 306
    :cond_3
    new-instance v19, Lorg/apache/lucene/index/CorruptIndexException;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "invalid deletion count: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (resource: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/apache/lucene/index/CorruptIndexException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 308
    :cond_4
    new-instance v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v0, v19

    invoke-direct {v0, v13, v9, v10, v11}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->add(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V

    .line 297
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 312
    .end local v8    # "codec":Lorg/apache/lucene/codecs/Codec;
    .end local v9    # "delCount":I
    .end local v10    # "delGen":J
    .end local v13    # "info":Lorg/apache/lucene/index/SegmentInfo;
    .end local v15    # "numSegments":I
    .end local v16    # "seg":I
    .end local v17    # "segName":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14, v12}, Lorg/apache/lucene/codecs/lucene3x/Lucene3xSegmentInfoReader;->readLegacyInfos(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/store/IndexInput;I)V

    .line 313
    const-string v19, "Lucene3x"

    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/codecs/Codec;->forName(Ljava/lang/String;)Lorg/apache/lucene/codecs/Codec;

    move-result-object v8

    .line 314
    .restart local v8    # "codec":Lorg/apache/lucene/codecs/Codec;
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 315
    .local v13, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v0, v13, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lorg/apache/lucene/index/SegmentInfo;->setCodec(Lorg/apache/lucene/codecs/Codec;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 325
    .end local v8    # "codec":Lorg/apache/lucene/codecs/Codec;
    .end local v13    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .restart local v4    # "checksumNow":J
    .restart local v6    # "checksumThen":J
    :cond_6
    const/16 v18, 0x1

    .line 327
    if-nez v18, :cond_8

    .line 330
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/io/Closeable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    .line 331
    aput-object v14, v19, v20

    invoke-static/range {v19 .. v19}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 336
    :goto_3
    return-void

    .line 333
    .end local v4    # "checksumNow":J
    .end local v6    # "checksumThen":J
    .end local v12    # "format":I
    :cond_7
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->close()V

    goto/16 :goto_0

    .restart local v4    # "checksumNow":J
    .restart local v6    # "checksumThen":J
    .restart local v12    # "format":I
    :cond_8
    invoke-virtual {v14}, Lorg/apache/lucene/store/ChecksumIndexInput;->close()V

    goto :goto_3
.end method

.method remove(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1150
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1151
    return-void
.end method

.method public remove(Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    .locals 1
    .param p1, "si"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    .line 1142
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1143
    return-void
.end method

.method replace(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 1037
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/SegmentInfos;->rollbackSegmentInfos(Ljava/util/List;)V

    .line 1038
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 1039
    return-void
.end method

.method final rollbackCommit(Lorg/apache/lucene/store/Directory;)V
    .locals 7
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 850
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    if-eqz v1, :cond_0

    new-array v1, v6, [Ljava/io/Closeable;

    .line 853
    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    aput-object v2, v1, v3

    invoke-static {v1}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    .line 854
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/SegmentInfos;->pendingSegnOutput:Lorg/apache/lucene/store/ChecksumIndexOutput;

    .line 858
    const-string v1, "segments"

    .line 859
    const-string v2, ""

    .line 860
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 858
    invoke-static {v1, v2, v4, v5}, Lorg/apache/lucene/index/IndexFileNames;->fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 863
    .local v0, "segmentFileName":Ljava/lang/String;
    new-array v1, v6, [Ljava/lang/String;

    aput-object v0, v1, v3

    invoke-static {p1, v1}, Lorg/apache/lucene/util/IOUtils;->deleteFilesIgnoringExceptions(Lorg/apache/lucene/store/Directory;[Ljava/lang/String;)V

    .line 865
    .end local v0    # "segmentFileName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method rollbackSegmentInfos(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1100
    .local p1, "infos":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->clear()V

    .line 1101
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SegmentInfos;->addAll(Ljava/lang/Iterable;)V

    .line 1102
    return-void
.end method

.method setUserData(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1025
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1026
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    .line 1030
    :goto_0
    return-void

    .line 1028
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfos;->userData:Ljava/util/Map;

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1118
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfos;->segments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;
    .locals 6
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 1003
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1004
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1005
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v1

    .line 1006
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 1013
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 1007
    :cond_0
    if-lez v2, :cond_1

    .line 1008
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1010
    :cond_1
    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    .line 1011
    .local v3, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1006
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public totalDocCount()I
    .locals 4

    .prologue
    .line 1044
    const/4 v0, 0x0

    .line 1045
    .local v0, "count":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1048
    return v0

    .line 1045
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 1046
    .local v1, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-object v3, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method updateGeneration(Lorg/apache/lucene/index/SegmentInfos;)V
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/SegmentInfos;

    .prologue
    .line 845
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->lastGeneration:J

    .line 846
    iget-wide v0, p1, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfos;->generation:J

    .line 847
    return-void
.end method
