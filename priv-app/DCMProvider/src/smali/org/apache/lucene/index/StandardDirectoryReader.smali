.class final Lorg/apache/lucene/index/StandardDirectoryReader;
.super Lorg/apache/lucene/index/DirectoryReader;
.source "StandardDirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final applyAllDeletes:Z

.field private final segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

.field private final termInfosIndexDivisor:I

.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lorg/apache/lucene/index/StandardDirectoryReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/StandardDirectoryReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V
    .locals 0
    .param p1, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p2, "readers"    # [Lorg/apache/lucene/index/AtomicReader;
    .param p3, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p4, "sis"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p5, "termInfosIndexDivisor"    # I
    .param p6, "applyAllDeletes"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;)V

    .line 43
    iput-object p3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 44
    iput-object p4, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    .line 45
    iput p5, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->termInfosIndexDivisor:I

    .line 46
    iput-boolean p6, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->applyAllDeletes:Z

    .line 47
    return-void
.end method

.method private doOpenFromCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 2
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 300
    new-instance v0, Lorg/apache/lucene/index/StandardDirectoryReader$2;

    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v0, p0, v1}, Lorg/apache/lucene/index/StandardDirectoryReader$2;-><init>(Lorg/apache/lucene/index/StandardDirectoryReader;Lorg/apache/lucene/store/Directory;)V

    .line 307
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader$2;->run(Lorg/apache/lucene/index/IndexCommit;)Ljava/lang/Object;

    move-result-object v0

    .line 300
    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;

    return-object v0
.end method

.method private doOpenFromWriter(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 6
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 262
    if-eqz p1, :cond_1

    .line 263
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenFromCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 278
    :cond_0
    :goto_0
    return-object v0

    .line 266
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->nrtIsCurrent(Lorg/apache/lucene/index/SegmentInfos;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 267
    goto :goto_0

    .line 270
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-boolean v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->applyAllDeletes:Z

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 273
    .local v0, "reader":Lorg/apache/lucene/index/DirectoryReader;
    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->getVersion()J

    move-result-wide v2

    iget-object v4, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 274
    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->decRef()V

    move-object v0, v1

    .line 275
    goto :goto_0
.end method

.method private doOpenNoWriter(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 3
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 283
    if-nez p1, :cond_1

    .line 284
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->isCurrent()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 296
    :cond_0
    :goto_0
    return-object v0

    .line 288
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexCommit;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 289
    new-instance v0, Ljava/io/IOException;

    const-string v1, "the specified commit does not match the specified Directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexCommit;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 296
    :cond_3
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenFromCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    goto :goto_0
.end method

.method static open(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 18
    .param p0, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v12

    .line 83
    .local v12, "numSegments":I
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v15, "readers":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentReader;>;"
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v2

    .line 86
    .local v2, "dir":Lorg/apache/lucene/store/Directory;
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->clone()Lorg/apache/lucene/index/SegmentInfos;

    move-result-object v5

    .line 87
    .local v5, "segmentInfos":Lorg/apache/lucene/index/SegmentInfos;
    const/4 v11, 0x0

    .line 88
    .local v11, "infosUpto":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-lt v9, v12, :cond_0

    .line 117
    new-instance v1, Lorg/apache/lucene/index/StandardDirectoryReader;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/lucene/index/SegmentReader;

    invoke-interface {v15, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/AtomicReader;

    .line 118
    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v6

    move-object/from16 v4, p0

    move/from16 v7, p2

    .line 117
    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/index/StandardDirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V

    return-object v1

    .line 89
    :cond_0
    const/4 v13, 0x0

    .line 90
    .local v13, "prior":Ljava/io/IOException;
    const/16 v17, 0x0

    .line 92
    .local v17, "success":Z
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v10

    .line 93
    .local v10, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    sget-boolean v1, Lorg/apache/lucene/index/StandardDirectoryReader;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, v10, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-eq v1, v2, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 109
    .end local v10    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :catch_0
    move-exception v8

    .line 110
    .local v8, "ex":Ljava/io/IOException;
    move-object v13, v8

    .line 112
    if-nez v17, :cond_1

    .line 113
    invoke-static {v13, v15}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V

    .line 88
    .end local v8    # "ex":Ljava/io/IOException;
    :cond_1
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 94
    .restart local v10    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    const/4 v3, 0x1

    invoke-virtual {v1, v10, v3}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->get(Lorg/apache/lucene/index/SegmentInfoPerCommit;Z)Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v16

    .line 96
    .local v16, "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :try_start_2
    sget-object v1, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getReadOnlyClone(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v14

    .line 97
    .local v14, "reader":Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual {v14}, Lorg/apache/lucene/index/SegmentReader;->numDocs()I

    move-result v1

    if-gtz v1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lorg/apache/lucene/index/IndexWriter;->getKeepFullyDeletedSegments()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 99
    :cond_3
    invoke-interface {v15, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 100
    add-int/lit8 v11, v11, 0x1

    .line 106
    :goto_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 108
    const/16 v17, 0x1

    .line 112
    if-nez v17, :cond_1

    .line 113
    invoke-static {v13, v15}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V

    goto :goto_1

    .line 102
    :cond_4
    :try_start_4
    invoke-virtual {v14}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 103
    invoke-virtual {v5, v11}, Lorg/apache/lucene/index/SegmentInfos;->remove(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 105
    .end local v14    # "reader":Lorg/apache/lucene/index/SegmentReader;
    :catchall_0
    move-exception v1

    .line 106
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/lucene/index/IndexWriter;->readerPool:Lorg/apache/lucene/index/IndexWriter$ReaderPool;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lorg/apache/lucene/index/IndexWriter$ReaderPool;->release(Lorg/apache/lucene/index/ReadersAndLiveDocs;)V

    .line 107
    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 111
    .end local v10    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v16    # "rld":Lorg/apache/lucene/index/ReadersAndLiveDocs;
    :catchall_1
    move-exception v1

    .line 112
    if-nez v17, :cond_5

    .line 113
    invoke-static {v13, v15}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException(Ljava/lang/Exception;Ljava/lang/Iterable;)V

    .line 115
    :cond_5
    throw v1
.end method

.method static open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexCommit;I)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .param p2, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lorg/apache/lucene/index/StandardDirectoryReader$1;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/index/StandardDirectoryReader$1;-><init>(Lorg/apache/lucene/store/Directory;I)V

    .line 73
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader$1;->run(Lorg/apache/lucene/index/IndexCommit;)Ljava/lang/Object;

    move-result-object v0

    .line 52
    check-cast v0, Lorg/apache/lucene/index/DirectoryReader;

    return-object v0
.end method

.method private static open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;Ljava/util/List;I)Lorg/apache/lucene/index/DirectoryReader;
    .locals 20
    .param p0, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p4, "termInfosIndexDivisor"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/store/Directory;",
            "Lorg/apache/lucene/index/IndexWriter;",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;I)",
            "Lorg/apache/lucene/index/DirectoryReader;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    .local p3, "oldReaders":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/AtomicReader;>;"
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 128
    .local v16, "segmentReaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz p3, :cond_0

    .line 130
    const/4 v11, 0x0

    .local v11, "i":I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v9

    .local v9, "c":I
    :goto_0
    if-lt v11, v9, :cond_1

    .line 136
    .end local v9    # "c":I
    .end local v11    # "i":I
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v2

    new-array v4, v2, [Lorg/apache/lucene/index/SegmentReader;

    .line 140
    .local v4, "newReaders":[Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v2

    new-array v15, v2, [Z

    .line 142
    .local v15, "readerShared":[Z
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v2

    add-int/lit8 v11, v2, -0x1

    .restart local v11    # "i":I
    :goto_1
    if-gez v11, :cond_2

    .line 210
    new-instance v2, Lorg/apache/lucene/index/StandardDirectoryReader;

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v7, p4

    invoke-direct/range {v2 .. v8}, Lorg/apache/lucene/index/StandardDirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;IZ)V

    return-object v2

    .line 131
    .end local v4    # "newReaders":[Lorg/apache/lucene/index/SegmentReader;
    .end local v15    # "readerShared":[Z
    .restart local v9    # "c":I
    :cond_1
    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/lucene/index/SegmentReader;

    .line 132
    .local v17, "sr":Lorg/apache/lucene/index/SegmentReader;
    invoke-virtual/range {v17 .. v17}, Lorg/apache/lucene/index/SegmentReader;->getSegmentName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 144
    .end local v9    # "c":I
    .end local v17    # "sr":Lorg/apache/lucene/index/SegmentReader;
    .restart local v4    # "newReaders":[Lorg/apache/lucene/index/SegmentReader;
    .restart local v15    # "readerShared":[Z
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 145
    .local v13, "oldReaderIndex":Ljava/lang/Integer;
    if-nez v13, :cond_5

    .line 147
    const/4 v2, 0x0

    aput-object v2, v4, v11

    .line 153
    :goto_2
    const/16 v18, 0x0

    .line 154
    .local v18, "success":Z
    const/4 v14, 0x0

    .line 157
    .local v14, "prior":Ljava/lang/Throwable;
    :try_start_0
    aget-object v2, v4, v11

    if-eqz v2, :cond_3

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v2

    aget-object v3, v4, v11

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v3

    if-eq v2, v3, :cond_6

    .line 160
    :cond_3
    new-instance v12, Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    move/from16 v0, p4

    invoke-direct {v12, v2, v0, v3}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V

    .line 161
    .local v12, "newReader":Lorg/apache/lucene/index/SegmentReader;
    const/4 v2, 0x0

    aput-boolean v2, v15, v11

    .line 162
    aput-object v12, v4, v11
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    .end local v12    # "newReader":Lorg/apache/lucene/index/SegmentReader;
    :goto_3
    const/16 v18, 0x1

    .line 182
    if-nez v18, :cond_4

    .line 183
    add-int/lit8 v11, v11, 0x1

    :goto_4
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v2

    if-lt v11, v2, :cond_19

    .line 202
    :cond_4
    if-eqz v14, :cond_1f

    .line 203
    instance-of v2, v14, Ljava/io/IOException;

    if-eqz v2, :cond_1c

    check-cast v14, Ljava/io/IOException;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 150
    .end local v18    # "success":Z
    :cond_5
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentReader;

    aput-object v2, v4, v11

    goto :goto_2

    .line 164
    .restart local v14    # "prior":Ljava/lang/Throwable;
    .restart local v18    # "success":Z
    :cond_6
    :try_start_1
    aget-object v2, v4, v11

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelGen()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-nez v2, :cond_8

    .line 168
    const/4 v2, 0x1

    aput-boolean v2, v15, v11

    .line 169
    aget-object v2, v4, v11

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->incRef()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 179
    :catch_0
    move-exception v10

    .line 180
    .local v10, "ex":Ljava/lang/Throwable;
    move-object v14, v10

    .line 182
    if-nez v18, :cond_7

    .line 183
    add-int/lit8 v11, v11, 0x1

    :goto_5
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v2

    if-lt v11, v2, :cond_c

    .line 202
    :cond_7
    if-eqz v14, :cond_1f

    .line 203
    instance-of v2, v14, Ljava/io/IOException;

    if-eqz v2, :cond_f

    check-cast v14, Ljava/io/IOException;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 171
    .end local v10    # "ex":Ljava/lang/Throwable;
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_8
    const/4 v2, 0x0

    :try_start_2
    aput-boolean v2, v15, v11

    .line 173
    sget-boolean v2, Lorg/apache/lucene/index/StandardDirectoryReader;->$assertionsDisabled:Z

    if-nez v2, :cond_a

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    aget-object v3, v4, v11

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    if-eq v2, v3, :cond_a

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    :catchall_0
    move-exception v2

    .line 182
    if-nez v18, :cond_9

    .line 183
    add-int/lit8 v11, v11, 0x1

    :goto_6
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v3

    if-lt v11, v3, :cond_12

    .line 202
    :cond_9
    if-eqz v14, :cond_18

    .line 203
    instance-of v2, v14, Ljava/io/IOException;

    if-eqz v2, :cond_15

    check-cast v14, Ljava/io/IOException;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 174
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_a
    :try_start_3
    sget-boolean v2, Lorg/apache/lucene/index/StandardDirectoryReader;->$assertionsDisabled:Z

    if-nez v2, :cond_b

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->hasDeletions()Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 175
    :cond_b
    new-instance v2, Lorg/apache/lucene/index/SegmentReader;

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lorg/apache/lucene/index/SegmentInfos;->info(I)Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v3

    aget-object v5, v4, v11

    iget-object v5, v5, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    sget-object v6, Lorg/apache/lucene/store/IOContext;->READ:Lorg/apache/lucene/store/IOContext;

    invoke-direct {v2, v3, v5, v6}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentCoreReaders;Lorg/apache/lucene/store/IOContext;)V

    aput-object v2, v4, v11
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 184
    .restart local v10    # "ex":Ljava/lang/Throwable;
    :cond_c
    aget-object v2, v4, v11

    if-eqz v2, :cond_d

    .line 186
    :try_start_4
    aget-boolean v2, v15, v11

    if-nez v2, :cond_e

    .line 189
    aget-object v2, v4, v11

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 183
    :cond_d
    :goto_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 193
    :cond_e
    aget-object v2, v4, v11

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_7

    .line 195
    :catch_1
    move-exception v19

    .line 196
    .local v19, "t":Ljava/lang/Throwable;
    if-nez v14, :cond_d

    move-object/from16 v14, v19

    goto :goto_7

    .line 204
    .end local v19    # "t":Ljava/lang/Throwable;
    :cond_f
    instance-of v2, v14, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_10

    check-cast v14, Ljava/lang/RuntimeException;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 205
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_10
    instance-of v2, v14, Ljava/lang/Error;

    if-eqz v2, :cond_11

    check-cast v14, Ljava/lang/Error;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 206
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_11
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 184
    .end local v10    # "ex":Ljava/lang/Throwable;
    :cond_12
    aget-object v3, v4, v11

    if-eqz v3, :cond_13

    .line 186
    :try_start_5
    aget-boolean v3, v15, v11

    if-nez v3, :cond_14

    .line 189
    aget-object v3, v4, v11

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 183
    :cond_13
    :goto_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 193
    :cond_14
    aget-object v3, v4, v11

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_8

    .line 195
    :catch_2
    move-exception v19

    .line 196
    .restart local v19    # "t":Ljava/lang/Throwable;
    if-nez v14, :cond_13

    move-object/from16 v14, v19

    goto :goto_8

    .line 204
    .end local v19    # "t":Ljava/lang/Throwable;
    :cond_15
    instance-of v2, v14, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_16

    check-cast v14, Ljava/lang/RuntimeException;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 205
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_16
    instance-of v2, v14, Ljava/lang/Error;

    if-eqz v2, :cond_17

    check-cast v14, Ljava/lang/Error;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 206
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_17
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 208
    :cond_18
    throw v2

    .line 184
    :cond_19
    aget-object v2, v4, v11

    if-eqz v2, :cond_1a

    .line 186
    :try_start_6
    aget-boolean v2, v15, v11

    if-nez v2, :cond_1b

    .line 189
    aget-object v2, v4, v11

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->close()V

    .line 183
    :cond_1a
    :goto_9
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 193
    :cond_1b
    aget-object v2, v4, v11

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_9

    .line 195
    :catch_3
    move-exception v19

    .line 196
    .restart local v19    # "t":Ljava/lang/Throwable;
    if-nez v14, :cond_1a

    move-object/from16 v14, v19

    goto :goto_9

    .line 204
    .end local v19    # "t":Ljava/lang/Throwable;
    :cond_1c
    instance-of v2, v14, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_1d

    check-cast v14, Ljava/lang/RuntimeException;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 205
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_1d
    instance-of v2, v14, Ljava/lang/Error;

    if-eqz v2, :cond_1e

    check-cast v14, Ljava/lang/Error;

    .end local v14    # "prior":Ljava/lang/Throwable;
    throw v14

    .line 206
    .restart local v14    # "prior":Ljava/lang/Throwable;
    :cond_1e
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 142
    :cond_1f
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_1
.end method


# virtual methods
.method protected doClose()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 341
    const/4 v0, 0x0

    .line 342
    .local v0, "firstExc":Ljava/lang/Throwable;
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 351
    iget-object v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v3, :cond_1

    .line 354
    iget-object v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->deletePendingFiles()V

    .line 358
    :cond_1
    if-eqz v0, :cond_6

    .line 359
    instance-of v3, v0, Ljava/io/IOException;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/io/IOException;

    .end local v0    # "firstExc":Ljava/lang/Throwable;
    throw v0

    .line 342
    .restart local v0    # "firstExc":Ljava/lang/Throwable;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReader;

    .line 345
    .local v1, "r":Lorg/apache/lucene/index/AtomicReader;
    :try_start_0
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReader;->decRef()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 346
    :catch_0
    move-exception v2

    .line 347
    .local v2, "t":Ljava/lang/Throwable;
    if-nez v0, :cond_0

    move-object v0, v2

    goto :goto_0

    .line 360
    .end local v1    # "r":Lorg/apache/lucene/index/AtomicReader;
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_3
    instance-of v3, v0, Ljava/lang/RuntimeException;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/RuntimeException;

    .end local v0    # "firstExc":Ljava/lang/Throwable;
    throw v0

    .line 361
    .restart local v0    # "firstExc":Ljava/lang/Throwable;
    :cond_4
    instance-of v3, v0, Ljava/lang/Error;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/Error;

    .end local v0    # "firstExc":Ljava/lang/Throwable;
    throw v0

    .line 362
    .restart local v0    # "firstExc":Ljava/lang/Throwable;
    :cond_5
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 364
    :cond_6
    return-void
.end method

.method protected doOpenIfChanged()Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->ensureOpen()V

    .line 244
    iget-object v0, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v0, :cond_0

    .line 245
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenFromWriter(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenNoWriter(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    goto :goto_0
.end method

.method protected doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->ensureOpen()V

    .line 254
    iget-object v0, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->applyAllDeletes:Z

    if-ne p2, v0, :cond_0

    .line 255
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenFromWriter(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    .line 257
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/IndexWriter;->getReader(Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    goto :goto_0
.end method

.method doOpenIfChanged(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 3
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->termInfosIndexDivisor:I

    invoke-static {v0, p2, p1, v1, v2}, Lorg/apache/lucene/index/StandardDirectoryReader;->open(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfos;Ljava/util/List;I)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 368
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->ensureOpen()V

    .line 369
    new-instance v0, Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;

    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    iget-object v2, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/StandardDirectoryReader$ReaderCommit;-><init>(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/store/Directory;)V

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 316
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->ensureOpen()V

    .line 317
    iget-object v0, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method public isCurrent()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->ensureOpen()V

    .line 323
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 329
    :cond_0
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 330
    .local v0, "sis":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;)V

    .line 333
    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v2

    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 335
    .end local v0    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :goto_0
    return v1

    .line 333
    .restart local v0    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 335
    .end local v0    # "sis":Lorg/apache/lucene/index/SegmentInfos;
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->nrtIsCurrent(Lorg/apache/lucene/index/SegmentInfos;)Z

    move-result v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const/16 v3, 0x28

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 218
    iget-object v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfos;->getSegmentsFileName()Ljava/lang/String;

    move-result-object v2

    .line 219
    .local v2, "segmentsFile":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 220
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->segmentInfos:Lorg/apache/lucene/index/SegmentInfos;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfos;->getVersion()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 222
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/StandardDirectoryReader;->writer:Lorg/apache/lucene/index/IndexWriter;

    if-eqz v3, :cond_1

    .line 223
    const-string v3, ":nrt"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/StandardDirectoryReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 229
    const/16 v3, 0x29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 230
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 225
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReader;

    .line 226
    .local v1, "r":Lorg/apache/lucene/index/AtomicReader;
    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
