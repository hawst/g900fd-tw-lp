.class public final Lorg/apache/lucene/index/MultiDocsEnum;
.super Lorg/apache/lucene/index/DocsEnum;
.source "MultiDocsEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field current:Lorg/apache/lucene/index/DocsEnum;

.field currentBase:I

.field doc:I

.field numSubs:I

.field private final parent:Lorg/apache/lucene/index/MultiTermsEnum;

.field final subDocsEnum:[Lorg/apache/lucene/index/DocsEnum;

.field private subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

.field upto:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/MultiDocsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiDocsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/MultiTermsEnum;I)V
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/MultiTermsEnum;
    .param p2, "subReaderCount"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsEnum;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    .line 45
    iput-object p1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->parent:Lorg/apache/lucene/index/MultiTermsEnum;

    .line 46
    new-array v0, p2, [Lorg/apache/lucene/index/DocsEnum;

    iput-object v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subDocsEnum:[Lorg/apache/lucene/index/DocsEnum;

    .line 47
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 4
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0x7fffffff

    .line 93
    sget-boolean v2, Lorg/apache/lucene/index/MultiDocsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    if-gt p1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 95
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    if-eqz v2, :cond_3

    .line 97
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->currentBase:I

    if-ge p1, v2, :cond_1

    .line 99
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .line 103
    .local v0, "doc":I
    :goto_1
    if-ne v0, v1, :cond_2

    .line 104
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    goto :goto_0

    .line 101
    .end local v0    # "doc":I
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->currentBase:I

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, Lorg/apache/lucene/index/DocsEnum;->advance(I)I

    move-result v0

    .restart local v0    # "doc":I
    goto :goto_1

    .line 106
    :cond_2
    iget v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->currentBase:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    .line 109
    .end local v0    # "doc":I
    :goto_2
    return v1

    .line 108
    :cond_3
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->numSubs:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_4

    .line 109
    iput v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    goto :goto_2

    .line 111
    :cond_4
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    .line 112
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    .line 113
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v2, v2, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->currentBase:I

    goto :goto_0
.end method

.method public canReuse(Lorg/apache/lucene/index/MultiTermsEnum;)Z
    .locals 1
    .param p1, "parent"    # Lorg/apache/lucene/index/MultiTermsEnum;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->parent:Lorg/apache/lucene/index/MultiTermsEnum;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cost()J
    .locals 6

    .prologue
    .line 142
    const-wide/16 v0, 0x0

    .line 143
    .local v0, "cost":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->numSubs:I

    if-lt v2, v3, :cond_0

    .line 146
    return-wide v0

    .line 144
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    aget-object v3, v3, v2

    iget-object v3, v3, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocsEnum;->cost()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 143
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public getNumSubs()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->numSubs:I

    return v0
.end method

.method public getSubs()[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    return-object v0
.end method

.method public nextDoc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0x7fffffff

    .line 121
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    if-nez v2, :cond_1

    .line 122
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->numSubs:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_0

    .line 123
    iput v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    .line 133
    :goto_1
    return v1

    .line 125
    :cond_0
    iget v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    .line 126
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    .line 127
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iget v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    aget-object v2, v2, v3

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iget v2, v2, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iput v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->currentBase:I

    .line 131
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v0

    .line 132
    .local v0, "doc":I
    if-eq v0, v1, :cond_2

    .line 133
    iget v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->currentBase:I

    add-int/2addr v1, v0

    iput v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    goto :goto_1

    .line 135
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    goto :goto_0
.end method

.method reset([Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;I)Lorg/apache/lucene/index/MultiDocsEnum;
    .locals 4
    .param p1, "subs"    # [Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;
    .param p2, "numSubs"    # I

    .prologue
    const/4 v3, -0x1

    .line 50
    iput p2, p0, Lorg/apache/lucene/index/MultiDocsEnum;->numSubs:I

    .line 52
    array-length v1, p1

    new-array v1, v1, [Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    iput-object v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    .line 53
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 58
    iput v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->upto:I

    .line 59
    iput v3, p0, Lorg/apache/lucene/index/MultiDocsEnum;->doc:I

    .line 60
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->current:Lorg/apache/lucene/index/DocsEnum;

    .line 61
    return-object p0

    .line 54
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    new-instance v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    invoke-direct {v2}, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;-><init>()V

    aput-object v2, v1, v0

    .line 55
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    iput-object v2, v1, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 56
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocsEnum;->subs:[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    aget-object v1, v1, v0

    aget-object v2, p1, v0

    iget-object v2, v2, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    iput-object v2, v1, Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;->slice:Lorg/apache/lucene/index/ReaderSlice;

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MultiDocsEnum("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/lucene/index/MultiDocsEnum;->getSubs()[Lorg/apache/lucene/index/MultiDocsEnum$EnumWithSlice;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
