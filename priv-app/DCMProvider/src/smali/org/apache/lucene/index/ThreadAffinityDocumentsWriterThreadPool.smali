.class Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;
.super Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
.source "ThreadAffinityDocumentsWriterThreadPool.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private threadBindings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Thread;",
            "Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "maxNumPerThreads"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;-><init>(I)V

    .line 34
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->threadBindings:Ljava/util/Map;

    .line 41
    sget-boolean v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->getMaxThreadStates()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->clone()Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->clone()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    .line 81
    .local v0, "clone":Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->threadBindings:Ljava/util/Map;

    .line 82
    return-object v0
.end method

.method public getAndLock(Ljava/lang/Thread;Lorg/apache/lucene/index/DocumentsWriter;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 5
    .param p1, "requestingThread"    # Ljava/lang/Thread;
    .param p2, "documentsWriter"    # Lorg/apache/lucene/index/DocumentsWriter;

    .prologue
    .line 46
    iget-object v3, p0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->threadBindings:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .line 47
    .local v2, "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->tryLock()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    .end local v2    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :goto_0
    return-object v2

    .line 50
    .restart local v2    # "threadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :cond_0
    const/4 v0, 0x0

    .line 56
    .local v0, "minThreadState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {p0}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->minContendedThreadState()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->hasQueuedThreads()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 58
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->newThreadState()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v1

    .line 59
    .local v1, "newState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    if-eqz v1, :cond_3

    .line 60
    sget-boolean v3, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 61
    :cond_2
    iget-object v3, p0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->threadBindings:Ljava/util/Map;

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 62
    goto :goto_0

    .line 63
    :cond_3
    if-nez v0, :cond_4

    .line 69
    invoke-virtual {p0}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->minContendedThreadState()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v0

    .line 72
    .end local v1    # "newState":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :cond_4
    sget-boolean v3, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    if-nez v0, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "ThreadState is null"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 74
    :cond_5
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V

    move-object v2, v0

    .line 75
    goto :goto_0
.end method
