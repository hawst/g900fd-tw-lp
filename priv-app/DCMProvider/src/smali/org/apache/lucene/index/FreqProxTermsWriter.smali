.class final Lorg/apache/lucene/index/FreqProxTermsWriter;
.super Lorg/apache/lucene/index/TermsHashConsumer;
.source "FreqProxTermsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field payload:Lorg/apache/lucene/util/BytesRef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/index/FreqProxTermsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumer;-><init>()V

    return-void
.end method


# virtual methods
.method abort()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 113
    new-instance v0, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    invoke-direct {v0, p1, p0, p2}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;-><init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FreqProxTermsWriter;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method finishDocument(Lorg/apache/lucene/index/TermsHash;)V
    .locals 0
    .param p1, "termsHash"    # Lorg/apache/lucene/index/TermsHash;

    .prologue
    .line 118
    return-void
.end method

.method public flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 15
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "fieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v1, "allFields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/FreqProxTermsWriterPerField;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_2

    .line 54
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    .line 57
    .local v7, "numAllFields":I
    invoke-static {v1}, Lorg/apache/lucene/util/CollectionUtil;->quickSort(Ljava/util/List;)V

    .line 59
    move-object/from16 v0, p2

    iget-object v12, v0, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v12}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/lucene/codecs/Codec;->postingsFormat()Lorg/apache/lucene/codecs/PostingsFormat;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lorg/apache/lucene/codecs/PostingsFormat;->fieldsConsumer(Lorg/apache/lucene/index/SegmentWriteState;)Lorg/apache/lucene/codecs/FieldsConsumer;

    move-result-object v2

    .line 61
    .local v2, "consumer":Lorg/apache/lucene/codecs/FieldsConsumer;
    const/4 v10, 0x0

    .line 64
    .local v10, "success":Z
    const/4 v11, 0x0

    .line 78
    .local v11, "termsHash":Lorg/apache/lucene/index/TermsHash;
    const/4 v5, 0x0

    .local v5, "fieldNumber":I
    :goto_1
    if-lt v5, v7, :cond_3

    .line 96
    if-eqz v11, :cond_1

    .line 97
    :try_start_0
    invoke-virtual {v11}, Lorg/apache/lucene/index/TermsHash;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :cond_1
    const/4 v10, 0x1

    .line 101
    if-eqz v10, :cond_6

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    .line 102
    aput-object v2, v12, v13

    invoke-static {v12}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 107
    :goto_2
    return-void

    .line 47
    .end local v2    # "consumer":Lorg/apache/lucene/codecs/FieldsConsumer;
    .end local v5    # "fieldNumber":I
    .end local v7    # "numAllFields":I
    .end local v10    # "success":Z
    .end local v11    # "termsHash":Lorg/apache/lucene/index/TermsHash;
    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/TermsHashConsumerPerField;

    .local v3, "f":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    move-object v9, v3

    .line 48
    check-cast v9, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .line 49
    .local v9, "perField":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    iget-object v13, v9, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v13, v13, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v13}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v13

    if-lez v13, :cond_0

    .line 50
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 79
    .end local v3    # "f":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .end local v9    # "perField":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    .restart local v2    # "consumer":Lorg/apache/lucene/codecs/FieldsConsumer;
    .restart local v5    # "fieldNumber":I
    .restart local v7    # "numAllFields":I
    .restart local v10    # "success":Z
    .restart local v11    # "termsHash":Lorg/apache/lucene/index/TermsHash;
    :cond_3
    :try_start_1
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    iget-object v4, v12, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 81
    .local v4, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;

    .line 85
    .local v6, "fieldWriter":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    iget-object v12, v4, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v6, v12, v2, v0}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->flush(Ljava/lang/String;Lorg/apache/lucene/codecs/FieldsConsumer;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 87
    iget-object v9, v6, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 88
    .local v9, "perField":Lorg/apache/lucene/index/TermsHashPerField;
    sget-boolean v12, Lorg/apache/lucene/index/FreqProxTermsWriter;->$assertionsDisabled:Z

    if-nez v12, :cond_4

    if-eqz v11, :cond_4

    iget-object v12, v9, Lorg/apache/lucene/index/TermsHashPerField;->termsHash:Lorg/apache/lucene/index/TermsHash;

    if-eq v11, v12, :cond_4

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    .end local v4    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v6    # "fieldWriter":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    .end local v9    # "perField":Lorg/apache/lucene/index/TermsHashPerField;
    :catchall_0
    move-exception v12

    .line 101
    if-eqz v10, :cond_5

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/io/Closeable;

    const/4 v14, 0x0

    .line 102
    aput-object v2, v13, v14

    invoke-static {v13}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 106
    :goto_3
    throw v12

    .line 89
    .restart local v4    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .restart local v6    # "fieldWriter":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    .restart local v9    # "perField":Lorg/apache/lucene/index/TermsHashPerField;
    :cond_4
    :try_start_2
    iget-object v11, v9, Lorg/apache/lucene/index/TermsHashPerField;->termsHash:Lorg/apache/lucene/index/TermsHash;

    .line 90
    iget-object v12, v9, Lorg/apache/lucene/index/TermsHashPerField;->bytesHash:Lorg/apache/lucene/util/BytesRefHash;

    invoke-virtual {v12}, Lorg/apache/lucene/util/BytesRefHash;->size()I

    move-result v8

    .line 91
    .local v8, "numPostings":I
    invoke-virtual {v9}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 92
    invoke-virtual {v9, v8}, Lorg/apache/lucene/index/TermsHashPerField;->shrinkHash(I)V

    .line 93
    invoke-virtual {v6}, Lorg/apache/lucene/index/FreqProxTermsWriterPerField;->reset()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 78
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 103
    .end local v4    # "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    .end local v6    # "fieldWriter":Lorg/apache/lucene/index/FreqProxTermsWriterPerField;
    .end local v8    # "numPostings":I
    .end local v9    # "perField":Lorg/apache/lucene/index/TermsHashPerField;
    :cond_5
    const/4 v13, 0x1

    new-array v13, v13, [Ljava/io/Closeable;

    const/4 v14, 0x0

    .line 104
    aput-object v2, v13, v14

    invoke-static {v13}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_3

    .line 103
    :cond_6
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/io/Closeable;

    const/4 v13, 0x0

    .line 104
    aput-object v2, v12, v13

    invoke-static {v12}, Lorg/apache/lucene/util/IOUtils;->closeWhileHandlingException([Ljava/io/Closeable;)V

    goto :goto_2
.end method

.method startDocument()V
    .locals 0

    .prologue
    .line 122
    return-void
.end method
