.class Lorg/apache/lucene/index/MergeState$NoDelDocMap;
.super Lorg/apache/lucene/index/MergeState$DocMap;
.source "MergeState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergeState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NoDelDocMap"
.end annotation


# instance fields
.field private final maxDoc:I


# direct methods
.method private constructor <init>(I)V
    .locals 0
    .param p1, "maxDoc"    # I

    .prologue
    .line 111
    invoke-direct {p0}, Lorg/apache/lucene/index/MergeState$DocMap;-><init>()V

    .line 112
    iput p1, p0, Lorg/apache/lucene/index/MergeState$NoDelDocMap;->maxDoc:I

    .line 113
    return-void
.end method

.method synthetic constructor <init>(ILorg/apache/lucene/index/MergeState$NoDelDocMap;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/MergeState$NoDelDocMap;-><init>(I)V

    return-void
.end method


# virtual methods
.method public get(I)I
    .locals 0
    .param p1, "docID"    # I

    .prologue
    .line 117
    return p1
.end method

.method public maxDoc()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lorg/apache/lucene/index/MergeState$NoDelDocMap;->maxDoc:I

    return v0
.end method

.method public numDeletedDocs()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method
