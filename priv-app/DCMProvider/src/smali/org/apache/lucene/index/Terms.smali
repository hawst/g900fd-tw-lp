.class public abstract Lorg/apache/lucene/index/Terms;
.super Ljava/lang/Object;
.source "Terms.java"


# static fields
.field public static final EMPTY_ARRAY:[Lorg/apache/lucene/index/Terms;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/lucene/index/Terms;

    sput-object v0, Lorg/apache/lucene/index/Terms;->EMPTY_ARRAY:[Lorg/apache/lucene/index/Terms;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public abstract getComparator()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDocCount()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSumDocFreq()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getSumTotalTermFreq()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract hasOffsets()Z
.end method

.method public abstract hasPayloads()Z
.end method

.method public abstract hasPositions()Z
.end method

.method public intersect(Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum;
    .locals 3
    .param p1, "compiled"    # Lorg/apache/lucene/util/automaton/CompiledAutomaton;
    .param p2, "startTerm"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 60
    iget-object v0, p1, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->type:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    sget-object v1, Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;->NORMAL:Lorg/apache/lucene/util/automaton/CompiledAutomaton$AUTOMATON_TYPE;

    if-eq v0, v1, :cond_0

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "please use CompiledAutomaton.getTermsEnum instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    if-nez p2, :cond_1

    .line 64
    new-instance v0, Lorg/apache/lucene/index/AutomatonTermsEnum;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/apache/lucene/index/AutomatonTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/automaton/CompiledAutomaton;)V

    .line 66
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/lucene/index/Terms$1;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lorg/apache/lucene/index/Terms$1;-><init>(Lorg/apache/lucene/index/Terms;Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/automaton/CompiledAutomaton;Lorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method public abstract iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract size()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
