.class final Lorg/apache/lucene/index/DocInverter;
.super Lorg/apache/lucene/index/DocFieldConsumer;
.source "DocInverter.java"


# instance fields
.field final consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;Lorg/apache/lucene/index/InvertedDocConsumer;Lorg/apache/lucene/index/InvertedDocEndConsumer;)V
    .locals 0
    .param p1, "docState"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;
    .param p2, "consumer"    # Lorg/apache/lucene/index/InvertedDocConsumer;
    .param p3, "endConsumer"    # Lorg/apache/lucene/index/InvertedDocEndConsumer;

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/apache/lucene/index/DocFieldConsumer;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/apache/lucene/index/DocInverter;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 37
    iput-object p2, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    .line 38
    iput-object p3, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    .line 39
    return-void
.end method


# virtual methods
.method abort()V
    .locals 2

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumer;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->abort()V

    .line 78
    return-void

    .line 75
    :catchall_0
    move-exception v0

    .line 76
    iget-object v1, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v1}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->abort()V

    .line 77
    throw v0
.end method

.method public addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/DocFieldConsumerPerField;
    .locals 1
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 82
    new-instance v0, Lorg/apache/lucene/index/DocInverterPerField;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/DocInverterPerField;-><init>(Lorg/apache/lucene/index/DocInverter;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method public finishDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->finishDocument()V

    .line 68
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumer;->finishDocument()V

    .line 69
    return-void
.end method

.method flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 7
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/DocFieldConsumerPerField;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "fieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/DocFieldConsumerPerField;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 45
    .local v0, "childFieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocConsumerPerField;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 47
    .local v1, "endChildFieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;>;"
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 53
    iget-object v4, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v4, v0, p2}, Lorg/apache/lucene/index/InvertedDocConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 54
    iget-object v4, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v4, v1, p2}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V

    .line 55
    return-void

    .line 47
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 48
    .local v2, "fieldToFlush":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/apache/lucene/index/DocFieldConsumerPerField;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/DocInverterPerField;

    .line 49
    .local v3, "perField":Lorg/apache/lucene/index/DocInverterPerField;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, v3, Lorg/apache/lucene/index/DocInverterPerField;->consumer:Lorg/apache/lucene/index/InvertedDocConsumerPerField;

    invoke-interface {v0, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v6, v3, Lorg/apache/lucene/index/DocInverterPerField;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumerPerField;

    invoke-interface {v1, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->consumer:Lorg/apache/lucene/index/InvertedDocConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocConsumer;->startDocument()V

    .line 60
    iget-object v0, p0, Lorg/apache/lucene/index/DocInverter;->endConsumer:Lorg/apache/lucene/index/InvertedDocEndConsumer;

    invoke-virtual {v0}, Lorg/apache/lucene/index/InvertedDocEndConsumer;->startDocument()V

    .line 61
    return-void
.end method
