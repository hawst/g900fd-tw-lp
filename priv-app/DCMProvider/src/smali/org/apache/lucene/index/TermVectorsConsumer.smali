.class final Lorg/apache/lucene/index/TermVectorsConsumer;
.super Lorg/apache/lucene/index/TermsHashConsumer;
.source "TermVectorsConsumer.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

.field final docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

.field final flushTerm:Lorg/apache/lucene/util/BytesRef;

.field freeCount:I

.field hasVectors:Z

.field lastDocID:I

.field lastVectorFieldName:Ljava/lang/String;

.field numVectorFields:I

.field perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

.field final vectorSliceReaderOff:Lorg/apache/lucene/index/ByteSliceReader;

.field final vectorSliceReaderPos:Lorg/apache/lucene/index/ByteSliceReader;

.field writer:Lorg/apache/lucene/codecs/TermVectorsWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/TermVectorsConsumer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TermVectorsConsumer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 1
    .param p1, "docWriter"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 46
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsHashConsumer;-><init>()V

    .line 39
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->flushTerm:Lorg/apache/lucene/util/BytesRef;

    .line 42
    new-instance v0, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->vectorSliceReaderPos:Lorg/apache/lucene/index/ByteSliceReader;

    .line 43
    new-instance v0, Lorg/apache/lucene/index/ByteSliceReader;

    invoke-direct {v0}, Lorg/apache/lucene/index/ByteSliceReader;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->vectorSliceReaderOff:Lorg/apache/lucene/index/ByteSliceReader;

    .line 47
    iput-object p1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 48
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    .line 49
    return-void
.end method

.method private final initTermVectorsWriter()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    if-nez v1, :cond_0

    .line 88
    new-instance v0, Lorg/apache/lucene/store/IOContext;

    new-instance v1, Lorg/apache/lucene/store/FlushInfo;

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->bytesUsed()J

    move-result-wide v4

    invoke-direct {v1, v2, v4, v5}, Lorg/apache/lucene/store/FlushInfo;-><init>(IJ)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/IOContext;-><init>(Lorg/apache/lucene/store/FlushInfo;)V

    .line 89
    .local v0, "context":Lorg/apache/lucene/store/IOContext;
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->codec:Lorg/apache/lucene/codecs/Codec;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/Codec;->termVectorsFormat()Lorg/apache/lucene/codecs/TermVectorsFormat;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread;->directory:Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfo;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lorg/apache/lucene/codecs/TermVectorsFormat;->vectorsWriter(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfo;Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/codecs/TermVectorsWriter;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    .line 90
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    .line 92
    .end local v0    # "context":Lorg/apache/lucene/store/IOContext;
    :cond_0
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    iput-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->hasVectors:Z

    .line 127
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->abort()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    .line 132
    :cond_0
    iput v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    .line 134
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsConsumer;->reset()V

    .line 135
    return-void
.end method

.method public addField(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/TermsHashConsumerPerField;
    .locals 1
    .param p1, "termsHashPerField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 148
    new-instance v0, Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    invoke-direct {v0, p1, p0, p2}, Lorg/apache/lucene/index/TermVectorsConsumerPerField;-><init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/index/TermVectorsConsumer;Lorg/apache/lucene/index/FieldInfo;)V

    return-object v0
.end method

.method addFieldToFlush(Lorg/apache/lucene/index/TermVectorsConsumerPerField;)V
    .locals 5
    .param p1, "fieldToFlush"    # Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    .prologue
    const/4 v4, 0x0

    .line 152
    iget v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 153
    iget v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    .line 154
    .local v1, "newSize":I
    new-array v0, v1, [Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    .line 155
    .local v0, "newArray":[Lorg/apache/lucene/index/TermVectorsConsumerPerField;
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    iget v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 156
    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    .line 159
    .end local v0    # "newArray":[Lorg/apache/lucene/index/TermVectorsConsumerPerField;
    .end local v1    # "newSize":I
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    iget v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    aput-object p1, v2, v3

    .line 160
    return-void
.end method

.method final clearLastVectorFieldName()Z
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastVectorFieldName:Ljava/lang/String;

    .line 171
    const/4 v0, 0x1

    return v0
.end method

.method fill(I)V
    .locals 2
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    :goto_0
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    if-lt v0, p1, :cond_0

    .line 84
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startDocument(I)V

    .line 81
    iget-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishDocument()V

    .line 82
    iget v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    goto :goto_0
.end method

.method finishDocument(Lorg/apache/lucene/index/TermsHash;)V
    .locals 4
    .param p1, "termsHash"    # Lorg/apache/lucene/index/TermsHash;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 97
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumer;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string v2, "TermVectorsTermsWriter.finishDocument start"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 99
    :cond_0
    iget-boolean v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->hasVectors:Z

    if-nez v1, :cond_2

    .line 121
    :cond_1
    return-void

    .line 103
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/index/TermVectorsConsumer;->initTermVectorsWriter()V

    .line 105
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/TermVectorsConsumer;->fill(I)V

    .line 108
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    iget v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    invoke-virtual {v1, v2}, Lorg/apache/lucene/codecs/TermVectorsWriter;->startDocument(I)V

    .line 109
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    if-lt v0, v1, :cond_3

    .line 112
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finishDocument()V

    .line 114
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumer;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    iget-object v2, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v2, v2, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    if-eq v1, v2, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "lastDocID="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " docState.docID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docState:Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;

    iget v3, v3, Lorg/apache/lucene/index/DocumentsWriterPerThread$DocState;->docID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 110
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->finishDocument()V

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_4
    iget v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    .line 118
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermsHash;->reset()V

    .line 119
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsConsumer;->reset()V

    .line 120
    sget-boolean v1, Lorg/apache/lucene/index/TermVectorsConsumer;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->docWriter:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->writer:Lorg/apache/lucene/index/IndexWriter;

    const-string v2, "TermVectorsTermsWriter.finishDocument end"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexWriter;->testPoint(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
.end method

.method flush(Ljava/util/Map;Lorg/apache/lucene/index/SegmentWriteState;)V
    .locals 8
    .param p2, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/TermsHashConsumerPerField;",
            ">;",
            "Lorg/apache/lucene/index/SegmentWriteState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "fieldsToFlush":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lorg/apache/lucene/index/TermsHashConsumerPerField;>;"
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 53
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    if-eqz v3, :cond_1

    .line 54
    iget-object v3, p2, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    .line 57
    .local v1, "numDocs":I
    :try_start_0
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/TermVectorsConsumer;->fill(I)V

    .line 58
    sget-boolean v3, Lorg/apache/lucene/index/TermVectorsConsumer;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p2, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :catchall_0
    move-exception v3

    new-array v4, v5, [Ljava/io/Closeable;

    .line 61
    iget-object v5, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    aput-object v5, v4, v6

    invoke-static {v4}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 62
    iput-object v7, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    .line 64
    iput v6, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    .line 65
    iput-boolean v6, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->hasVectors:Z

    .line 66
    throw v3

    .line 59
    :cond_0
    :try_start_1
    iget-object v3, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    iget-object v4, p2, Lorg/apache/lucene/index/SegmentWriteState;->fieldInfos:Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v3, v4, v1}, Lorg/apache/lucene/codecs/TermVectorsWriter;->finish(Lorg/apache/lucene/index/FieldInfos;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    new-array v3, v5, [Ljava/io/Closeable;

    .line 61
    iget-object v4, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    aput-object v4, v3, v6

    invoke-static {v3}, Lorg/apache/lucene/util/IOUtils;->close([Ljava/io/Closeable;)V

    .line 62
    iput-object v7, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->writer:Lorg/apache/lucene/codecs/TermVectorsWriter;

    .line 64
    iput v6, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastDocID:I

    .line 65
    iput-boolean v6, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->hasVectors:Z

    .line 69
    .end local v1    # "numDocs":I
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 74
    return-void

    .line 69
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/TermsHashConsumerPerField;

    .local v0, "field":Lorg/apache/lucene/index/TermsHashConsumerPerField;
    move-object v2, v0

    .line 70
    check-cast v2, Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    .line 71
    .local v2, "perField":Lorg/apache/lucene/index/TermVectorsConsumerPerField;
    iget-object v4, v2, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->termsHashPerField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v4}, Lorg/apache/lucene/index/TermsHashPerField;->reset()V

    .line 72
    invoke-virtual {v2}, Lorg/apache/lucene/index/TermVectorsConsumerPerField;->shrinkHash()V

    goto :goto_0
.end method

.method reset()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->numVectorFields:I

    .line 143
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    iput-object v0, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->perFields:[Lorg/apache/lucene/index/TermVectorsConsumerPerField;

    .line 144
    return-void
.end method

.method startDocument()V
    .locals 1

    .prologue
    .line 164
    sget-boolean v0, Lorg/apache/lucene/index/TermVectorsConsumer;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsConsumer;->clearLastVectorFieldName()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 165
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/TermVectorsConsumer;->reset()V

    .line 166
    return-void
.end method

.method final vectorFieldsInOrder(Lorg/apache/lucene/index/FieldInfo;)Z
    .locals 3
    .param p1, "fi"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    const/4 v0, 0x1

    .line 178
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastVectorFieldName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 179
    iget-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastVectorFieldName:Ljava/lang/String;

    iget-object v2, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gez v1, :cond_0

    .line 183
    :goto_0
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastVectorFieldName:Ljava/lang/String;

    .line 181
    :goto_1
    return v0

    .line 179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 183
    :cond_1
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastVectorFieldName:Ljava/lang/String;

    goto :goto_1

    .line 182
    :catchall_0
    move-exception v0

    .line 183
    iget-object v1, p1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/lucene/index/TermVectorsConsumer;->lastVectorFieldName:Ljava/lang/String;

    .line 184
    throw v0
.end method
