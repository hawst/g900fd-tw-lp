.class public final Lorg/apache/lucene/index/ReaderManager;
.super Lorg/apache/lucene/search/ReferenceManager;
.source "ReaderManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/search/ReferenceManager",
        "<",
        "Lorg/apache/lucene/index/DirectoryReader;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;Z)V
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 57
    invoke-static {p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/ReaderManager;->current:Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;)V
    .locals 1
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Lorg/apache/lucene/search/ReferenceManager;-><init>()V

    .line 67
    invoke-static {p1}, Lorg/apache/lucene/index/DirectoryReader;->open(Lorg/apache/lucene/store/Directory;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/ReaderManager;->current:Ljava/lang/Object;

    .line 68
    return-void
.end method


# virtual methods
.method protected bridge synthetic decRef(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/ReaderManager;->decRef(Lorg/apache/lucene/index/DirectoryReader;)V

    return-void
.end method

.method protected decRef(Lorg/apache/lucene/index/DirectoryReader;)V
    .locals 0
    .param p1, "reference"    # Lorg/apache/lucene/index/DirectoryReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    invoke-virtual {p1}, Lorg/apache/lucene/index/DirectoryReader;->decRef()V

    .line 73
    return-void
.end method

.method protected bridge synthetic refreshIfNeeded(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/ReaderManager;->refreshIfNeeded(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected refreshIfNeeded(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "referenceToRefresh"    # Lorg/apache/lucene/index/DirectoryReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {p1}, Lorg/apache/lucene/index/DirectoryReader;->openIfChanged(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic tryIncRef(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/ReaderManager;->tryIncRef(Lorg/apache/lucene/index/DirectoryReader;)Z

    move-result v0

    return v0
.end method

.method protected tryIncRef(Lorg/apache/lucene/index/DirectoryReader;)Z
    .locals 1
    .param p1, "reference"    # Lorg/apache/lucene/index/DirectoryReader;

    .prologue
    .line 82
    invoke-virtual {p1}, Lorg/apache/lucene/index/DirectoryReader;->tryIncRef()Z

    move-result v0

    return v0
.end method
