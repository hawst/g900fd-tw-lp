.class public Lorg/apache/lucene/index/DocTermOrds;
.super Ljava/lang/Object;
.source "DocTermOrds.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocTermOrds$Iterator;,
        Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;
    }
.end annotation


# static fields
.field public static final DEFAULT_INDEX_INTERVAL_BITS:I = 0x7

.field private static final TNUM_OFFSET:I = 0x2


# instance fields
.field protected docsEnum:Lorg/apache/lucene/index/DocsEnum;

.field protected final field:Ljava/lang/String;

.field protected index:[I

.field private indexInterval:I

.field private indexIntervalBits:I

.field private indexIntervalMask:I

.field protected indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

.field protected final maxTermDocFreq:I

.field private memsz:J

.field protected numTermsInField:I

.field protected ordBase:I

.field protected phase1_time:I

.field protected prefix:Lorg/apache/lucene/util/BytesRef;

.field protected sizeOfIndexedStrings:J

.field protected termInstances:J

.field protected tnums:[[B

.field protected total_time:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "maxTermDocFreq"    # I
    .param p3, "indexIntervalBits"    # I

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    const/16 v0, 0x100

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    .line 206
    iput-object p1, p0, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    .line 207
    iput p2, p0, Lorg/apache/lucene/index/DocTermOrds;->maxTermDocFreq:I

    .line 208
    iput p3, p0, Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I

    .line 209
    const/4 v0, -0x1

    rsub-int/lit8 v1, p3, 0x20

    ushr-int/2addr v0, v1

    iput v0, p0, Lorg/apache/lucene/index/DocTermOrds;->indexIntervalMask:I

    .line 210
    const/4 v0, 0x1

    shl-int/2addr v0, p3

    iput v0, p0, Lorg/apache/lucene/index/DocTermOrds;->indexInterval:I

    .line 211
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p3, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    const/4 v4, 0x0

    const v5, 0x7fffffff

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/DocTermOrds;-><init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)V

    .line 179
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)V
    .locals 6
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p3, "field"    # Ljava/lang/String;
    .param p4, "termPrefix"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    const v5, 0x7fffffff

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/lucene/index/DocTermOrds;-><init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)V

    .line 184
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)V
    .locals 7
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p3, "field"    # Ljava/lang/String;
    .param p4, "termPrefix"    # Lorg/apache/lucene/util/BytesRef;
    .param p5, "maxTermDocFreq"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    const/4 v6, 0x7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lorg/apache/lucene/index/DocTermOrds;-><init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;II)V

    .line 191
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;II)V
    .locals 0
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p3, "field"    # Ljava/lang/String;
    .param p4, "termPrefix"    # Lorg/apache/lucene/util/BytesRef;
    .param p5, "maxTermDocFreq"    # I
    .param p6, "indexIntervalBits"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0, p3, p5, p6}, Lorg/apache/lucene/index/DocTermOrds;-><init>(Ljava/lang/String;II)V

    .line 199
    invoke-virtual {p0, p1, p2, p4}, Lorg/apache/lucene/index/DocTermOrds;->uninvert(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/util/BytesRef;)V

    .line 200
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/DocTermOrds;)I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lorg/apache/lucene/index/DocTermOrds;->indexInterval:I

    return v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/index/DocTermOrds;)I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lorg/apache/lucene/index/DocTermOrds;->indexIntervalBits:I

    return v0
.end method

.method private static vIntSize(I)I
    .locals 1
    .param p0, "x"    # I

    .prologue
    .line 562
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    .line 563
    const/4 v0, 0x1

    .line 574
    :goto_0
    return v0

    .line 565
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    .line 566
    const/4 v0, 0x2

    goto :goto_0

    .line 568
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    .line 569
    const/4 v0, 0x3

    goto :goto_0

    .line 571
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    .line 572
    const/4 v0, 0x4

    goto :goto_0

    .line 574
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private static writeInt(I[BI)I
    .locals 3
    .param p0, "x"    # I
    .param p1, "arr"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 581
    ushr-int/lit8 v0, p0, 0x1c

    .line 582
    .local v0, "a":I
    if-eqz v0, :cond_3

    .line 583
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "pos":I
    .local v1, "pos":I
    or-int/lit16 v2, v0, 0x80

    int-to-byte v2, v2

    aput-byte v2, p1, p2

    .line 585
    :goto_0
    ushr-int/lit8 v0, p0, 0x15

    .line 586
    if-eqz v0, :cond_0

    .line 587
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "pos":I
    .restart local p2    # "pos":I
    or-int/lit16 v2, v0, 0x80

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    move v1, p2

    .line 589
    .end local p2    # "pos":I
    .restart local v1    # "pos":I
    :cond_0
    ushr-int/lit8 v0, p0, 0xe

    .line 590
    if-eqz v0, :cond_1

    .line 591
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "pos":I
    .restart local p2    # "pos":I
    or-int/lit16 v2, v0, 0x80

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    move v1, p2

    .line 593
    .end local p2    # "pos":I
    .restart local v1    # "pos":I
    :cond_1
    ushr-int/lit8 v0, p0, 0x7

    .line 594
    if-eqz v0, :cond_2

    .line 595
    add-int/lit8 p2, v1, 0x1

    .end local v1    # "pos":I
    .restart local p2    # "pos":I
    or-int/lit16 v2, v0, 0x80

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 597
    :goto_1
    add-int/lit8 v1, p2, 0x1

    .end local p2    # "pos":I
    .restart local v1    # "pos":I
    and-int/lit8 v2, p0, 0x7f

    int-to-byte v2, v2

    aput-byte v2, p1, p2

    .line 598
    return v1

    :cond_2
    move p2, v1

    .end local v1    # "pos":I
    .restart local p2    # "pos":I
    goto :goto_1

    :cond_3
    move v1, p2

    .end local p2    # "pos":I
    .restart local v1    # "pos":I
    goto :goto_0
.end method


# virtual methods
.method public getOrdTermsEnum(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/TermsEnum;
    .locals 4
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 224
    iget-object v3, p0, Lorg/apache/lucene/index/DocTermOrds;->indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

    if-nez v3, :cond_2

    .line 226
    invoke-virtual {p1}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 227
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v0, :cond_1

    .line 238
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    :cond_0
    :goto_0
    return-object v2

    .line 230
    .restart local v0    # "fields":Lorg/apache/lucene/index/Fields;
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    .line 231
    .local v1, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    .line 234
    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v2

    goto :goto_0

    .line 238
    .end local v0    # "fields":Lorg/apache/lucene/index/Fields;
    .end local v1    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_2
    new-instance v2, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;

    invoke-direct {v2, p0, p1}, Lorg/apache/lucene/index/DocTermOrds$OrdWrappedTermsEnum;-><init>(Lorg/apache/lucene/index/DocTermOrds;Lorg/apache/lucene/index/AtomicReader;)V

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lorg/apache/lucene/index/DocTermOrds;->index:[I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/SortedSetDocValues;
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 770
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocTermOrds;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    sget-object v0, Lorg/apache/lucene/index/SortedSetDocValues;->EMPTY:Lorg/apache/lucene/index/SortedSetDocValues;

    .line 773
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/lucene/index/DocTermOrds$Iterator;

    invoke-direct {v0, p0, p1}, Lorg/apache/lucene/index/DocTermOrds$Iterator;-><init>(Lorg/apache/lucene/index/DocTermOrds;Lorg/apache/lucene/index/AtomicReader;)V

    goto :goto_0
.end method

.method public lookupTerm(Lorg/apache/lucene/index/TermsEnum;I)Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .param p1, "termsEnum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "ord"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 764
    int-to-long v0, p2

    invoke-virtual {p1, v0, v1}, Lorg/apache/lucene/index/TermsEnum;->seekExact(J)V

    .line 765
    invoke-virtual {p1}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public numTerms()I
    .locals 1

    .prologue
    .line 246
    iget v0, p0, Lorg/apache/lucene/index/DocTermOrds;->numTermsInField:I

    return v0
.end method

.method public ramUsedInBytes()J
    .locals 8

    .prologue
    .line 165
    iget-wide v4, p0, Lorg/apache/lucene/index/DocTermOrds;->memsz:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds;->memsz:J

    .line 173
    :goto_0
    return-wide v2

    .line 166
    :cond_0
    const-wide/16 v2, 0x60

    .line 167
    .local v2, "sz":J
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds;->index:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds;->index:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    int-to-long v4, v1

    add-long/2addr v2, v4

    .line 168
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    if-eqz v1, :cond_2

    .line 169
    iget-object v4, p0, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    array-length v5, v4

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v5, :cond_3

    .line 172
    :cond_2
    iput-wide v2, p0, Lorg/apache/lucene/index/DocTermOrds;->memsz:J

    goto :goto_0

    .line 169
    :cond_3
    aget-object v0, v4, v1

    .line 170
    .local v0, "arr":[B
    if-eqz v0, :cond_4

    array-length v6, v0

    int-to-long v6, v6

    add-long/2addr v2, v6

    .line 169
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected setActualDocFreq(II)V
    .locals 0
    .param p1, "termNum"    # I
    .param p2, "df"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    return-void
.end method

.method protected uninvert(Lorg/apache/lucene/index/AtomicReader;Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/util/BytesRef;)V
    .locals 54
    .param p1, "reader"    # Lorg/apache/lucene/index/AtomicReader;
    .param p2, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p3, "termPrefix"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 268
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    move-object/from16 v51, v0

    invoke-virtual/range {v50 .. v51}, Lorg/apache/lucene/index/FieldInfos;->fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;

    move-result-object v22

    .line 269
    .local v22, "info":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v22, :cond_0

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v50

    if-eqz v50, :cond_0

    .line 270
    new-instance v50, Ljava/lang/IllegalStateException;

    new-instance v51, Ljava/lang/StringBuilder;

    const-string v52, "Type mismatch: "

    invoke-direct/range {v51 .. v52}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    move-object/from16 v52, v0

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    const-string v52, " was indexed as "

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v22 .. v22}, Lorg/apache/lucene/index/FieldInfo;->getDocValuesType()Lorg/apache/lucene/index/FieldInfo$DocValuesType;

    move-result-object v52

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-direct/range {v50 .. v51}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v50

    .line 273
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v40

    .line 274
    .local v40, "startTime":J
    if-nez p3, :cond_2

    const/16 v50, 0x0

    :goto_0
    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocTermOrds;->prefix:Lorg/apache/lucene/util/BytesRef;

    .line 276
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v28

    .line 277
    .local v28, "maxDoc":I
    move/from16 v0, v28

    new-array v0, v0, [I

    move-object/from16 v18, v0

    .line 278
    .local v18, "index":[I
    move/from16 v0, v28

    new-array v0, v0, [I

    move-object/from16 v25, v0

    .line 279
    .local v25, "lastTerm":[I
    move/from16 v0, v28

    new-array v8, v0, [[B

    .line 281
    .local v8, "bytes":[[B
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v16

    .line 282
    .local v16, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v16, :cond_3

    .line 558
    :cond_1
    :goto_1
    return-void

    .line 274
    .end local v8    # "bytes":[[B
    .end local v16    # "fields":Lorg/apache/lucene/index/Fields;
    .end local v18    # "index":[I
    .end local v25    # "lastTerm":[I
    .end local v28    # "maxDoc":I
    :cond_2
    invoke-static/range {p3 .. p3}, Lorg/apache/lucene/util/BytesRef;->deepCopyOf(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v50

    goto :goto_0

    .line 286
    .restart local v8    # "bytes":[[B
    .restart local v16    # "fields":Lorg/apache/lucene/index/Fields;
    .restart local v18    # "index":[I
    .restart local v25    # "lastTerm":[I
    .restart local v28    # "maxDoc":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    move-object/from16 v50, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v46

    .line 287
    .local v46, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v46, :cond_1

    .line 292
    const/16 v50, 0x0

    move-object/from16 v0, v46

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v43

    .line 293
    .local v43, "te":Lorg/apache/lucene/index/TermsEnum;
    if-eqz p3, :cond_8

    move-object/from16 v38, p3

    .line 295
    .local v38, "seekStart":Lorg/apache/lucene/util/BytesRef;
    :goto_2
    move-object/from16 v0, v43

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v50

    sget-object v51, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-object/from16 v0, v50

    move-object/from16 v1, v51

    if-eq v0, v1, :cond_1

    .line 302
    const/16 v20, 0x0

    .line 303
    .local v20, "indexedTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    const/16 v21, 0x0

    .line 305
    .local v21, "indexedTermsBytes":Lorg/apache/lucene/util/PagedBytes;
    const/16 v47, 0x0

    .line 309
    .local v47, "testedOrd":Z
    const/16 v50, 0xc

    move/from16 v0, v50

    new-array v0, v0, [B

    move-object/from16 v44, v0

    .line 327
    .local v44, "tempArr":[B
    const/16 v45, 0x0

    .line 328
    .local v45, "termNum":I
    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocTermOrds;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 333
    :cond_4
    invoke-virtual/range {v43 .. v43}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v39

    .line 334
    .local v39, "t":Lorg/apache/lucene/util/BytesRef;
    if-eqz v39, :cond_5

    if-eqz p3, :cond_9

    move-object/from16 v0, v39

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lorg/apache/lucene/util/StringHelper;->startsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v50

    if-nez v50, :cond_9

    .line 461
    :cond_5
    :goto_3
    move/from16 v0, v45

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DocTermOrds;->numTermsInField:I

    .line 463
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    .line 465
    .local v30, "midPoint":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/DocTermOrds;->termInstances:J

    move-wide/from16 v50, v0

    const-wide/16 v52, 0x0

    cmp-long v50, v50, v52

    if-nez v50, :cond_17

    .line 468
    const/16 v50, 0x0

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    .line 550
    :cond_6
    if-eqz v20, :cond_7

    .line 551
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v50

    move/from16 v0, v50

    new-array v0, v0, [Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v50, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v50

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v50

    check-cast v50, [Lorg/apache/lucene/util/BytesRef;

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocTermOrds;->indexedTermsArray:[Lorg/apache/lucene/util/BytesRef;

    .line 554
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 556
    .local v14, "endTime":J
    sub-long v50, v14, v40

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v50, v0

    move/from16 v0, v50

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DocTermOrds;->total_time:I

    .line 557
    sub-long v50, v30, v40

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v50, v0

    move/from16 v0, v50

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DocTermOrds;->phase1_time:I

    goto/16 :goto_1

    .line 293
    .end local v14    # "endTime":J
    .end local v20    # "indexedTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    .end local v21    # "indexedTermsBytes":Lorg/apache/lucene/util/PagedBytes;
    .end local v30    # "midPoint":J
    .end local v38    # "seekStart":Lorg/apache/lucene/util/BytesRef;
    .end local v39    # "t":Lorg/apache/lucene/util/BytesRef;
    .end local v44    # "tempArr":[B
    .end local v45    # "termNum":I
    .end local v47    # "testedOrd":Z
    :cond_8
    new-instance v38, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v38 .. v38}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    goto/16 :goto_2

    .line 339
    .restart local v20    # "indexedTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    .restart local v21    # "indexedTermsBytes":Lorg/apache/lucene/util/PagedBytes;
    .restart local v38    # "seekStart":Lorg/apache/lucene/util/BytesRef;
    .restart local v39    # "t":Lorg/apache/lucene/util/BytesRef;
    .restart local v44    # "tempArr":[B
    .restart local v45    # "termNum":I
    .restart local v47    # "testedOrd":Z
    :cond_9
    if-nez v47, :cond_a

    .line 341
    :try_start_0
    invoke-virtual/range {v43 .. v43}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v50

    move-wide/from16 v0, v50

    long-to-int v0, v0

    move/from16 v50, v0

    move/from16 v0, v50

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/lucene/index/DocTermOrds;->ordBase:I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :goto_4
    const/16 v47, 0x1

    .line 353
    :cond_a
    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocTermOrds;->visitTerm(Lorg/apache/lucene/index/TermsEnum;I)V

    .line 355
    if-eqz v20, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/DocTermOrds;->indexIntervalMask:I

    move/from16 v50, v0

    and-int v50, v50, v45

    if-nez v50, :cond_b

    .line 357
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/DocTermOrds;->sizeOfIndexedStrings:J

    move-wide/from16 v50, v0

    move-object/from16 v0, v39

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    move/from16 v52, v0

    move/from16 v0, v52

    int-to-long v0, v0

    move-wide/from16 v52, v0

    add-long v50, v50, v52

    move-wide/from16 v0, v50

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/DocTermOrds;->sizeOfIndexedStrings:J

    .line 358
    new-instance v19, Lorg/apache/lucene/util/BytesRef;

    invoke-direct/range {v19 .. v19}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 359
    .local v19, "indexedTerm":Lorg/apache/lucene/util/BytesRef;
    move-object/from16 v0, v21

    move-object/from16 v1, v39

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/PagedBytes;->copy(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)V

    .line 362
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    .end local v19    # "indexedTerm":Lorg/apache/lucene/util/BytesRef;
    :cond_b
    invoke-virtual/range {v43 .. v43}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v10

    .line 366
    .local v10, "df":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/DocTermOrds;->maxTermDocFreq:I

    move/from16 v50, v0

    move/from16 v0, v50

    if-gt v10, v0, :cond_c

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    move-object/from16 v50, v0

    const/16 v51, 0x0

    move-object/from16 v0, v43

    move-object/from16 v1, p2

    move-object/from16 v2, v50

    move/from16 v3, v51

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v50

    move-object/from16 v0, v50

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocTermOrds;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    .line 371
    const/4 v6, 0x0

    .line 374
    .local v6, "actualDF":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->docsEnum:Lorg/apache/lucene/index/DocsEnum;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lorg/apache/lucene/index/DocsEnum;->nextDoc()I

    move-result v11

    .line 375
    .local v11, "doc":I
    const v50, 0x7fffffff

    move/from16 v0, v50

    if-ne v11, v0, :cond_d

    .line 452
    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-virtual {v0, v1, v6}, Lorg/apache/lucene/index/DocTermOrds;->setActualDocFreq(II)V

    .line 455
    .end local v6    # "actualDF":I
    .end local v11    # "doc":I
    :cond_c
    add-int/lit8 v45, v45, 0x1

    .line 456
    invoke-virtual/range {v43 .. v43}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v50

    if-nez v50, :cond_4

    goto/16 :goto_3

    .line 343
    .end local v10    # "df":I
    :catch_0
    move-exception v48

    .line 346
    .local v48, "uoe":Ljava/lang/UnsupportedOperationException;
    new-instance v20, Ljava/util/ArrayList;

    .end local v20    # "indexedTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 347
    .restart local v20    # "indexedTerms":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/util/BytesRef;>;"
    new-instance v21, Lorg/apache/lucene/util/PagedBytes;

    .end local v21    # "indexedTermsBytes":Lorg/apache/lucene/util/PagedBytes;
    const/16 v50, 0xf

    move-object/from16 v0, v21

    move/from16 v1, v50

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/PagedBytes;-><init>(I)V

    .restart local v21    # "indexedTermsBytes":Lorg/apache/lucene/util/PagedBytes;
    goto/16 :goto_4

    .line 380
    .end local v48    # "uoe":Ljava/lang/UnsupportedOperationException;
    .restart local v6    # "actualDF":I
    .restart local v10    # "df":I
    .restart local v11    # "doc":I
    :cond_d
    add-int/lit8 v6, v6, 0x1

    .line 381
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/DocTermOrds;->termInstances:J

    move-wide/from16 v50, v0

    const-wide/16 v52, 0x1

    add-long v50, v50, v52

    move-wide/from16 v0, v50

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/apache/lucene/index/DocTermOrds;->termInstances:J

    .line 386
    aget v50, v25, v11

    sub-int v50, v45, v50

    add-int/lit8 v9, v50, 0x2

    .line 387
    .local v9, "delta":I
    aput v45, v25, v11

    .line 388
    aget v49, v18, v11

    .line 390
    .local v49, "val":I
    move/from16 v0, v49

    and-int/lit16 v0, v0, 0xff

    move/from16 v50, v0

    const/16 v51, 0x1

    move/from16 v0, v50

    move/from16 v1, v51

    if-ne v0, v1, :cond_f

    .line 393
    ushr-int/lit8 v37, v49, 0x8

    .line 394
    .local v37, "pos":I
    invoke-static {v9}, Lorg/apache/lucene/index/DocTermOrds;->vIntSize(I)I

    move-result v17

    .line 395
    .local v17, "ilen":I
    aget-object v7, v8, v11

    .line 396
    .local v7, "arr":[B
    add-int v33, v37, v17

    .line 397
    .local v33, "newend":I
    array-length v0, v7

    move/from16 v50, v0

    move/from16 v0, v33

    move/from16 v1, v50

    if-le v0, v1, :cond_e

    .line 404
    add-int/lit8 v50, v33, 0x3

    and-int/lit8 v29, v50, -0x4

    .line 405
    .local v29, "newLen":I
    move/from16 v0, v29

    new-array v0, v0, [B

    move-object/from16 v32, v0

    .line 406
    .local v32, "newarr":[B
    const/16 v50, 0x0

    const/16 v51, 0x0

    move/from16 v0, v50

    move-object/from16 v1, v32

    move/from16 v2, v51

    move/from16 v3, v37

    invoke-static {v7, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 407
    move-object/from16 v7, v32

    .line 408
    aput-object v32, v8, v11

    .line 410
    .end local v29    # "newLen":I
    .end local v32    # "newarr":[B
    :cond_e
    move/from16 v0, v37

    invoke-static {v9, v7, v0}, Lorg/apache/lucene/index/DocTermOrds;->writeInt(I[BI)I

    move-result v37

    .line 411
    shl-int/lit8 v50, v37, 0x8

    or-int/lit8 v50, v50, 0x1

    aput v50, v18, v11

    goto/16 :goto_5

    .line 416
    .end local v7    # "arr":[B
    .end local v17    # "ilen":I
    .end local v33    # "newend":I
    .end local v37    # "pos":I
    :cond_f
    if-nez v49, :cond_10

    .line 417
    const/16 v23, 0x0

    .line 430
    .local v23, "ipos":I
    :goto_6
    move-object/from16 v0, v44

    move/from16 v1, v23

    invoke-static {v9, v0, v1}, Lorg/apache/lucene/index/DocTermOrds;->writeInt(I[BI)I

    move-result v13

    .line 432
    .local v13, "endPos":I
    const/16 v50, 0x4

    move/from16 v0, v50

    if-gt v13, v0, :cond_15

    .line 435
    move/from16 v24, v23

    .local v24, "j":I
    :goto_7
    move/from16 v0, v24

    if-lt v0, v13, :cond_14

    .line 438
    aput v49, v18, v11

    goto/16 :goto_5

    .line 418
    .end local v13    # "endPos":I
    .end local v23    # "ipos":I
    .end local v24    # "j":I
    :cond_10
    const v50, 0xff80

    and-int v50, v50, v49

    if-nez v50, :cond_11

    .line 419
    const/16 v23, 0x1

    .line 420
    .restart local v23    # "ipos":I
    goto :goto_6

    .end local v23    # "ipos":I
    :cond_11
    const v50, 0xff8000

    and-int v50, v50, v49

    if-nez v50, :cond_12

    .line 421
    const/16 v23, 0x2

    .line 422
    .restart local v23    # "ipos":I
    goto :goto_6

    .end local v23    # "ipos":I
    :cond_12
    const/high16 v50, -0x800000    # Float.NEGATIVE_INFINITY

    and-int v50, v50, v49

    if-nez v50, :cond_13

    .line 423
    const/16 v23, 0x3

    .line 424
    .restart local v23    # "ipos":I
    goto :goto_6

    .line 425
    .end local v23    # "ipos":I
    :cond_13
    const/16 v23, 0x4

    .restart local v23    # "ipos":I
    goto :goto_6

    .line 436
    .restart local v13    # "endPos":I
    .restart local v24    # "j":I
    :cond_14
    aget-byte v50, v44, v24

    move/from16 v0, v50

    and-int/lit16 v0, v0, 0xff

    move/from16 v50, v0

    shl-int/lit8 v51, v24, 0x3

    shl-int v50, v50, v51

    or-int v49, v49, v50

    .line 435
    add-int/lit8 v24, v24, 0x1

    goto :goto_7

    .line 441
    .end local v24    # "j":I
    :cond_15
    const/16 v24, 0x0

    .restart local v24    # "j":I
    :goto_8
    move/from16 v0, v24

    move/from16 v1, v23

    if-lt v0, v1, :cond_16

    .line 446
    shl-int/lit8 v50, v13, 0x8

    or-int/lit8 v50, v50, 0x1

    aput v50, v18, v11

    .line 447
    aput-object v44, v8, v11

    .line 448
    const/16 v50, 0xc

    move/from16 v0, v50

    new-array v0, v0, [B

    move-object/from16 v44, v0

    .line 373
    goto/16 :goto_5

    .line 442
    :cond_16
    move/from16 v0, v49

    int-to-byte v0, v0

    move/from16 v50, v0

    aput-byte v50, v44, v24

    .line 443
    ushr-int/lit8 v49, v49, 0x8

    .line 441
    add-int/lit8 v24, v24, 0x1

    goto :goto_8

    .line 471
    .end local v6    # "actualDF":I
    .end local v9    # "delta":I
    .end local v10    # "df":I
    .end local v11    # "doc":I
    .end local v13    # "endPos":I
    .end local v23    # "ipos":I
    .end local v24    # "j":I
    .end local v49    # "val":I
    .restart local v30    # "midPoint":J
    :cond_17
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/lucene/index/DocTermOrds;->index:[I

    .line 479
    const/16 v36, 0x0

    .local v36, "pass":I
    :goto_9
    const/16 v50, 0x100

    move/from16 v0, v36

    move/from16 v1, v50

    if-ge v0, v1, :cond_6

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    move-object/from16 v50, v0

    aget-object v42, v50, v36

    .line 481
    .local v42, "target":[B
    const/16 v37, 0x0

    .line 482
    .restart local v37    # "pos":I
    if-eqz v42, :cond_19

    .line 483
    move-object/from16 v0, v42

    array-length v0, v0

    move/from16 v37, v0

    .line 491
    :goto_a
    shl-int/lit8 v12, v36, 0x10

    .local v12, "docbase":I
    :goto_b
    move/from16 v0, v28

    if-lt v12, v0, :cond_1a

    .line 537
    move-object/from16 v0, v42

    array-length v0, v0

    move/from16 v50, v0

    move/from16 v0, v37

    move/from16 v1, v50

    if-ge v0, v1, :cond_18

    .line 538
    move/from16 v0, v37

    new-array v0, v0, [B

    move-object/from16 v35, v0

    .line 539
    .local v35, "newtarget":[B
    const/16 v50, 0x0

    const/16 v51, 0x0

    move-object/from16 v0, v42

    move/from16 v1, v50

    move-object/from16 v2, v35

    move/from16 v3, v51

    move/from16 v4, v37

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 540
    move-object/from16 v42, v35

    .line 543
    .end local v35    # "newtarget":[B
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->tnums:[[B

    move-object/from16 v50, v0

    aput-object v42, v50, v36

    .line 545
    shl-int/lit8 v50, v36, 0x10

    move/from16 v0, v50

    move/from16 v1, v28

    if-gt v0, v1, :cond_6

    .line 479
    add-int/lit8 v36, v36, 0x1

    goto :goto_9

    .line 485
    .end local v12    # "docbase":I
    :cond_19
    const/16 v50, 0x1000

    move/from16 v0, v50

    new-array v0, v0, [B

    move-object/from16 v42, v0

    goto :goto_a

    .line 492
    .restart local v12    # "docbase":I
    :cond_1a
    const/high16 v50, 0x10000

    add-int v50, v50, v12

    move/from16 v0, v50

    move/from16 v1, v28

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v27

    .line 493
    .local v27, "lim":I
    move v11, v12

    .restart local v11    # "doc":I
    :goto_c
    move/from16 v0, v27

    if-lt v11, v0, :cond_1b

    .line 491
    const/high16 v50, 0x1000000

    add-int v12, v12, v50

    goto :goto_b

    .line 495
    :cond_1b
    aget v49, v18, v11

    .line 496
    .restart local v49    # "val":I
    move/from16 v0, v49

    and-int/lit16 v0, v0, 0xff

    move/from16 v50, v0

    const/16 v51, 0x1

    move/from16 v0, v50

    move/from16 v1, v51

    if-ne v0, v1, :cond_1e

    .line 497
    ushr-int/lit8 v26, v49, 0x8

    .line 499
    .local v26, "len":I
    shl-int/lit8 v50, v37, 0x8

    or-int/lit8 v50, v50, 0x1

    aput v50, v18, v11

    .line 500
    const/high16 v50, -0x1000000

    and-int v50, v50, v37

    if-eqz v50, :cond_1c

    .line 502
    new-instance v50, Ljava/lang/IllegalStateException;

    new-instance v51, Ljava/lang/StringBuilder;

    const-string v52, "Too many values for UnInvertedField faceting on field "

    invoke-direct/range {v51 .. v52}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/DocTermOrds;->field:Ljava/lang/String;

    move-object/from16 v52, v0

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-direct/range {v50 .. v51}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v50

    .line 504
    :cond_1c
    aget-object v7, v8, v11

    .line 510
    .restart local v7    # "arr":[B
    const/16 v50, 0x0

    aput-object v50, v8, v11

    .line 511
    move-object/from16 v0, v42

    array-length v0, v0

    move/from16 v50, v0

    add-int v51, v37, v26

    move/from16 v0, v50

    move/from16 v1, v51

    if-gt v0, v1, :cond_1d

    .line 512
    move-object/from16 v0, v42

    array-length v0, v0

    move/from16 v34, v0

    .line 525
    .local v34, "newlen":I
    :goto_d
    add-int v50, v37, v26

    move/from16 v0, v34

    move/from16 v1, v50

    if-le v0, v1, :cond_1f

    .line 526
    move/from16 v0, v34

    new-array v0, v0, [B

    move-object/from16 v35, v0

    .line 527
    .restart local v35    # "newtarget":[B
    const/16 v50, 0x0

    const/16 v51, 0x0

    move-object/from16 v0, v42

    move/from16 v1, v50

    move-object/from16 v2, v35

    move/from16 v3, v51

    move/from16 v4, v37

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 528
    move-object/from16 v42, v35

    .line 530
    .end local v34    # "newlen":I
    .end local v35    # "newtarget":[B
    :cond_1d
    const/16 v50, 0x0

    move/from16 v0, v50

    move-object/from16 v1, v42

    move/from16 v2, v37

    move/from16 v3, v26

    invoke-static {v7, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 531
    add-int/lit8 v50, v26, 0x1

    add-int v37, v37, v50

    .line 493
    .end local v7    # "arr":[B
    .end local v26    # "len":I
    :cond_1e
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_c

    .line 525
    .restart local v7    # "arr":[B
    .restart local v26    # "len":I
    .restart local v34    # "newlen":I
    :cond_1f
    shl-int/lit8 v34, v34, 0x1

    goto :goto_d
.end method

.method protected visitTerm(Lorg/apache/lucene/index/TermsEnum;I)V
    .locals 0
    .param p1, "te"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "termNum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 258
    return-void
.end method
