.class final Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;
.super Lorg/apache/lucene/index/Fields;
.source "ParallelAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ParallelAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ParallelFields"
.end annotation


# instance fields
.field final fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Terms;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/lucene/index/ParallelAtomicReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/ParallelAtomicReader;)V
    .locals 1

    .prologue
    .line 156
    iput-object p1, p0, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->this$0:Lorg/apache/lucene/index/ParallelAtomicReader;

    invoke-direct {p0}, Lorg/apache/lucene/index/Fields;-><init>()V

    .line 154
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->fields:Ljava/util/Map;

    .line 157
    return-void
.end method


# virtual methods
.method addField(Ljava/lang/String;Lorg/apache/lucene/index/Terms;)V
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "terms"    # Lorg/apache/lucene/index/Terms;

    .prologue
    .line 160
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->fields:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->fields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->fields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 1
    .param p1, "field"    # Ljava/lang/String;

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelAtomicReader$ParallelFields;->fields:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/Terms;

    return-object v0
.end method
