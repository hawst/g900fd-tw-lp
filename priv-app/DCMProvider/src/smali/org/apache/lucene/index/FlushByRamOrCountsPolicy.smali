.class Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;
.super Lorg/apache/lucene/index/FlushPolicy;
.source "FlushByRamOrCountsPolicy.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lorg/apache/lucene/index/FlushPolicy;-><init>()V

    return-void
.end method


# virtual methods
.method protected flushOnDeleteTerms()Z
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected flushOnDocCount()Z
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDocs()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected flushOnRAM()Z
    .locals 4

    .prologue
    .line 126
    iget-object v0, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected markLargestWriterPending(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;J)V
    .locals 1
    .param p1, "control"    # Lorg/apache/lucene/index/DocumentsWriterFlushControl;
    .param p2, "perThreadState"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .param p3, "currentBytesPerThread"    # J

    .prologue
    .line 98
    .line 99
    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->findLargestNonPendingWriter(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setFlushPending(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 100
    return-void
.end method

.method public onDelete(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 10
    .param p1, "control"    # Lorg/apache/lucene/index/DocumentsWriterFlushControl;
    .param p2, "state"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    .line 54
    invoke-virtual {p0}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->flushOnDeleteTerms()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    iget-object v2, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .line 57
    invoke-virtual {v2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v0

    .line 58
    .local v0, "maxBufferedDeleteTerms":I
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getNumGlobalTermDeletes()I

    move-result v2

    if-lt v2, v0, :cond_0

    .line 59
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setApplyAllDeletes()V

    .line 62
    .end local v0    # "maxBufferedDeleteTerms":I
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v2}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/DocumentsWriter;

    .line 63
    .local v1, "writer":Lorg/apache/lucene/index/DocumentsWriter;
    invoke-virtual {p0}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->flushOnRAM()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 64
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getDeleteBytesUsed()J

    move-result-wide v2

    long-to-double v2, v2

    iget-object v4, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v4}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v4

    mul-double/2addr v4, v8

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    .line 65
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setApplyAllDeletes()V

    .line 66
    iget-object v2, v1, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "FP"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    iget-object v2, v1, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "FP"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "force apply deletes bytesUsed="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getDeleteBytesUsed()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " vs ramBuffer="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v5}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v6

    mul-double/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_1
    return-void
.end method

.method public onInsert(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    .locals 10
    .param p1, "control"    # Lorg/apache/lucene/index/DocumentsWriterFlushControl;
    .param p2, "state"    # Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    .prologue
    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    .line 74
    invoke-virtual {p0}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->flushOnDocCount()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 75
    iget-object v5, p2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->getNumDocsInRAM()I

    move-result v5

    iget-object v6, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    .line 76
    invoke-virtual {v6}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDocs()I

    move-result v6

    if-lt v5, v6, :cond_1

    .line 78
    invoke-virtual {p1, p2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setFlushPending(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->flushOnRAM()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 80
    iget-object v5, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->indexWriterConfig:Lorg/apache/lucene/index/LiveIndexWriterConfig;

    invoke-virtual {v5}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v6

    mul-double/2addr v6, v8

    mul-double/2addr v6, v8

    double-to-long v0, v6

    .line 81
    .local v0, "limit":J
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes()J

    move-result-wide v6

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getDeleteBytesUsed()J

    move-result-wide v8

    add-long v2, v6, v8

    .line 82
    .local v2, "totalRam":J
    cmp-long v5, v2, v0

    if-ltz v5, :cond_0

    .line 83
    iget-object v5, p0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v5}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/DocumentsWriter;

    .line 84
    .local v4, "writer":Lorg/apache/lucene/index/DocumentsWriter;
    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "FP"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 85
    iget-object v5, v4, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "FP"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "flush: activeBytes="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->activeBytes()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " deleteBytes="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getDeleteBytesUsed()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " vs limit="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_2
    invoke-virtual {p0, p1, p2, v2, v3}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;->markLargestWriterPending(Lorg/apache/lucene/index/DocumentsWriterFlushControl;Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;J)V

    goto :goto_0
.end method
