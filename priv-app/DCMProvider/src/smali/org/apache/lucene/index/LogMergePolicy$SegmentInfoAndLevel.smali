.class Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;
.super Ljava/lang/Object;
.source "LogMergePolicy.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/LogMergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SegmentInfoAndLevel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;",
        ">;"
    }
.end annotation


# instance fields
.field index:I

.field info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

.field level:F


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;FI)V
    .locals 0
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .param p2, "level"    # F
    .param p3, "index"    # I

    .prologue
    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 541
    iput-object p1, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 542
    iput p2, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    .line 543
    iput p3, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->index:I

    .line 544
    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->compareTo(Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;)I
    .locals 2
    .param p1, "other"    # Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;

    .prologue
    .line 549
    iget v0, p1, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    iget v1, p0, Lorg/apache/lucene/index/LogMergePolicy$SegmentInfoAndLevel;->level:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method
