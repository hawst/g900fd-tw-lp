.class public Lorg/apache/lucene/index/SegmentInfoPerCommit;
.super Ljava/lang/Object;
.source "SegmentInfoPerCommit.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bufferedDeletesGen:J

.field private delCount:I

.field private delGen:J

.field public final info:Lorg/apache/lucene/index/SegmentInfo;

.field private nextWriteDelGen:J

.field private volatile sizeInBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V
    .locals 5
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfo;
    .param p2, "delCount"    # I
    .param p3, "delGen"    # J

    .prologue
    const-wide/16 v2, 0x1

    const-wide/16 v0, -0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    .line 56
    iput-object p1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    .line 57
    iput p2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delCount:I

    .line 58
    iput-wide p3, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    .line 59
    cmp-long v0, p3, v0

    if-nez v0, :cond_0

    .line 60
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    add-long v0, p3, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    goto :goto_0
.end method


# virtual methods
.method advanceDelGen()V
    .locals 4

    .prologue
    .line 68
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    .line 69
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    .line 70
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    .line 71
    return-void
.end method

.method advanceNextWriteDelGen()V
    .locals 4

    .prologue
    .line 77
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    .line 78
    return-void
.end method

.method clearDelGen()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 121
    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    .line 122
    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    .line 123
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->clone()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .locals 6

    .prologue
    .line 184
    new-instance v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delCount:I

    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    invoke-direct {v0, v1, v2, v4, v5}, Lorg/apache/lucene/index/SegmentInfoPerCommit;-><init>(Lorg/apache/lucene/index/SegmentInfo;IJ)V

    .line 189
    .local v0, "other":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    iput-wide v2, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    .line 190
    return-object v0
.end method

.method public files()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->files()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 102
    .local v0, "files":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lorg/apache/lucene/codecs/LiveDocsFormat;->files(Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/util/Collection;)V

    .line 104
    return-object v0
.end method

.method getBufferedDeletesGen()J
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->bufferedDeletesGen:J

    return-wide v0
.end method

.method public getDelCount()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delCount:I

    return v0
.end method

.method public getDelGen()J
    .locals 2

    .prologue
    .line 153
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    return-wide v0
.end method

.method public getNextDelGen()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->nextWriteDelGen:J

    return-wide v0
.end method

.method public hasDeletions()Z
    .locals 4

    .prologue
    .line 137
    iget-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setBufferedDeletesGen(J)V
    .locals 3
    .param p1, "v"    # J

    .prologue
    .line 116
    iput-wide p1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->bufferedDeletesGen:J

    .line 117
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    .line 118
    return-void
.end method

.method setDelCount(I)V
    .locals 1
    .param p1, "delCount"    # I

    .prologue
    .line 164
    iput p1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delCount:I

    .line 165
    sget-boolean v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    if-le p1, v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 166
    :cond_0
    return-void
.end method

.method public setDelGen(J)V
    .locals 3
    .param p1, "delGen"    # J

    .prologue
    .line 130
    iput-wide p1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    .line 131
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    .line 132
    return-void
.end method

.method public sizeInBytes()J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 86
    const-wide/16 v2, 0x0

    .line 87
    .local v2, "sum":J
    invoke-virtual {p0}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->files()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 90
    iput-wide v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    .line 93
    .end local v2    # "sum":J
    :cond_0
    iget-wide v4, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes:J

    return-wide v4

    .line 87
    .restart local v2    # "sum":J
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88
    .local v0, "fileName":Ljava/lang/String;
    iget-object v4, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/store/Directory;->fileLength(Ljava/lang/String;)J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 175
    iget-object v1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    iget v3, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delCount:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "s":Ljava/lang/String;
    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 177
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":delGen="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delGen:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    :cond_0
    return-object v0
.end method

.method public toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "pendingDelCount"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget v1, p0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->delCount:I

    add-int/2addr v1, p2

    invoke-virtual {v0, p1, v1}, Lorg/apache/lucene/index/SegmentInfo;->toString(Lorg/apache/lucene/store/Directory;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
