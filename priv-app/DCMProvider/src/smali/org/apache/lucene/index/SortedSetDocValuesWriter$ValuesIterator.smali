.class Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;
.super Ljava/lang/Object;
.source "SortedSetDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SortedSetDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValuesIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/util/BytesRef;",
        ">;"
    }
.end annotation


# instance fields
.field ordUpto:I

.field final scratch:Lorg/apache/lucene/util/BytesRef;

.field final sortedValues:[I

.field final synthetic this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

.field final valueCount:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;[II)V
    .locals 1
    .param p2, "sortedValues"    # [I
    .param p3, "valueCount"    # I

    .prologue
    .line 199
    iput-object p1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->scratch:Lorg/apache/lucene/util/BytesRef;

    .line 200
    iput-object p2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->sortedValues:[I

    .line 201
    iput p3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->valueCount:I

    .line 202
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 206
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->ordUpto:I

    iget v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->valueCount:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 214
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->hash:Lorg/apache/lucene/util/BytesRefHash;

    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->sortedValues:[I

    iget v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->ordUpto:I

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->scratch:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/BytesRefHash;->get(ILorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    .line 215
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->ordUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->ordUpto:I

    .line 216
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$ValuesIterator;->scratch:Lorg/apache/lucene/util/BytesRef;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 221
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
