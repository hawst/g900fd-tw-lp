.class Lorg/apache/lucene/index/StandardDirectoryReader$2;
.super Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;
.source "StandardDirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenFromCommit(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/StandardDirectoryReader;


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/StandardDirectoryReader;Lorg/apache/lucene/store/Directory;)V
    .locals 0
    .param p2, "$anonymous0"    # Lorg/apache/lucene/store/Directory;

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/StandardDirectoryReader$2;->this$0:Lorg/apache/lucene/index/StandardDirectoryReader;

    .line 300
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/SegmentInfos$FindSegmentsFile;-><init>(Lorg/apache/lucene/store/Directory;)V

    return-void
.end method


# virtual methods
.method protected doBody(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "segmentFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    new-instance v0, Lorg/apache/lucene/index/SegmentInfos;

    invoke-direct {v0}, Lorg/apache/lucene/index/SegmentInfos;-><init>()V

    .line 304
    .local v0, "infos":Lorg/apache/lucene/index/SegmentInfos;
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader$2;->directory:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/index/SegmentInfos;->read(Lorg/apache/lucene/store/Directory;Ljava/lang/String;)V

    .line 305
    iget-object v1, p0, Lorg/apache/lucene/index/StandardDirectoryReader$2;->this$0:Lorg/apache/lucene/index/StandardDirectoryReader;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/apache/lucene/index/StandardDirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/IndexWriter;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v1

    return-object v1
.end method
