.class public final Lorg/apache/lucene/index/IndexFileNames;
.super Ljava/lang/Object;
.source "IndexFileNames.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final CODEC_FILE_PATTERN:Ljava/util/regex/Pattern;

.field public static final COMPOUND_FILE_ENTRIES_EXTENSION:Ljava/lang/String; = "cfe"

.field public static final COMPOUND_FILE_EXTENSION:Ljava/lang/String; = "cfs"

.field public static final GEN_EXTENSION:Ljava/lang/String; = "gen"

.field public static final INDEX_EXTENSIONS:[Ljava/lang/String;

.field public static final SEGMENTS:Ljava/lang/String; = "segments"

.field public static final SEGMENTS_GEN:Ljava/lang/String; = "segments.gen"


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    const-class v0, Lorg/apache/lucene/index/IndexFileNames;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/IndexFileNames;->$assertionsDisabled:Z

    .line 69
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 70
    const-string v3, "cfs"

    aput-object v3, v0, v2

    .line 71
    const-string v2, "cfe"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 72
    const-string v2, "gen"

    aput-object v2, v0, v1

    .line 69
    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->INDEX_EXTENSIONS:[Ljava/lang/String;

    .line 204
    const-string v0, "_[a-z0-9]+(_.*)?\\..*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/lucene/index/IndexFileNames;->CODEC_FILE_PATTERN:Ljava/util/regex/Pattern;

    return-void

    :cond_0
    move v0, v2

    .line 42
    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fileNameFromGeneration(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 6
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "ext"    # Ljava/lang/String;
    .param p2, "gen"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 88
    const-wide/16 v2, -0x1

    cmp-long v1, p2, v2

    if-nez v1, :cond_0

    .line 89
    const/4 v1, 0x0

    .line 102
    :goto_0
    return-object v1

    .line 90
    :cond_0
    cmp-long v1, p2, v4

    if-nez v1, :cond_1

    .line 91
    const-string v1, ""

    invoke-static {p0, v1, p1}, Lorg/apache/lucene/index/IndexFileNames;->segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 93
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileNames;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    cmp-long v1, p2, v4

    if-gtz v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 97
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 98
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x24

    invoke-static {p2, p3, v2}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 99
    .local v0, "res":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 100
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static indexOfSegmentName(Ljava/lang/String;)I
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 151
    const/16 v1, 0x5f

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 152
    .local v0, "idx":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 154
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 156
    :cond_0
    return v0
.end method

.method public static matchesExtension(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "ext"    # Ljava/lang/String;

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static parseSegmentName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 183
    invoke-static {p0}, Lorg/apache/lucene/index/IndexFileNames;->indexOfSegmentName(Ljava/lang/String;)I

    move-result v0

    .line 184
    .local v0, "idx":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 185
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 187
    :cond_0
    return-object p0
.end method

.method public static segmentFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "segmentName"    # Ljava/lang/String;
    .param p1, "segmentSuffix"    # Ljava/lang/String;
    .param p2, "ext"    # Ljava/lang/String;

    .prologue
    .line 122
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 123
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/IndexFileNames;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    const-string v1, "."

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 124
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 125
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 127
    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 130
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 134
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    .end local p0    # "segmentName":Ljava/lang/String;
    :cond_4
    return-object p0
.end method

.method public static stripExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 195
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 196
    .local v0, "idx":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 197
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 199
    :cond_0
    return-object p0
.end method

.method public static stripSegmentName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-static {p0}, Lorg/apache/lucene/index/IndexFileNames;->indexOfSegmentName(Ljava/lang/String;)I

    move-result v0

    .line 170
    .local v0, "idx":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 171
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 173
    :cond_0
    return-object p0
.end method
