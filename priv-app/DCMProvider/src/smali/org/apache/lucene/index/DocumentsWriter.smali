.class final Lorg/apache/lucene/index/DocumentsWriter;
.super Ljava/lang/Object;
.source "DocumentsWriter.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private abortedFiles:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final chain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

.field private volatile closed:Z

.field final codec:Lorg/apache/lucene/codecs/Codec;

.field private volatile currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

.field volatile deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

.field directory:Lorg/apache/lucene/store/Directory;

.field final flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

.field final flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

.field final indexWriter:Lorg/apache/lucene/index/IndexWriter;

.field final infoStream:Lorg/apache/lucene/util/InfoStream;

.field newFiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile pendingChangesInCurrentFullFlush:Z

.field final perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

.field similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field private final ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-class v0, Lorg/apache/lucene/index/DocumentsWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/codecs/Codec;Lorg/apache/lucene/index/LiveIndexWriterConfig;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/index/BufferedDeletesStream;)V
    .locals 2
    .param p1, "codec"    # Lorg/apache/lucene/codecs/Codec;
    .param p2, "config"    # Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .param p3, "directory"    # Lorg/apache/lucene/store/Directory;
    .param p4, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p5, "globalFieldNumbers"    # Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
    .param p6, "bufferedDeletesStream"    # Lorg/apache/lucene/index/BufferedDeletesStream;

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 120
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 121
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-direct {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 141
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 142
    iput-object p3, p0, Lorg/apache/lucene/index/DocumentsWriter;->directory:Lorg/apache/lucene/store/Directory;

    .line 143
    iput-object p4, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    .line 144
    invoke-virtual {p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getInfoStream()Lorg/apache/lucene/util/InfoStream;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 145
    invoke-virtual {p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 146
    invoke-virtual {p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexerThreadPool()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 147
    invoke-virtual {p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexingChain()Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->chain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v0, p0, p5, p2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->initialize(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/FieldInfos$FieldNumbers;Lorg/apache/lucene/index/LiveIndexWriterConfig;)V

    .line 149
    invoke-virtual {p2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getFlushPolicy()Lorg/apache/lucene/index/FlushPolicy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    .line 150
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 151
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    invoke-virtual {v0, p0}, Lorg/apache/lucene/index/FlushPolicy;->init(Lorg/apache/lucene/index/DocumentsWriter;)V

    .line 152
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-direct {v0, p0, p2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;-><init>(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/LiveIndexWriterConfig;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    .line 153
    return-void
.end method

.method private applyAllDeletes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V
    .locals 1
    .param p1, "deleteQueue"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->isFullFlush()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v0, p0, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->addDeletesAndPurge(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V

    .line 183
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriter;->applyAllDeletes()V

    .line 184
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 185
    return-void
.end method

.method private doFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Z
    .locals 14
    .param p1, "flushingDWPT"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/high16 v12, 0x4130000000000000L    # 1048576.0

    .line 393
    const/4 v0, 0x0

    .line 394
    .local v0, "maybeMerge":Z
    :goto_0
    if-nez p1, :cond_2

    .line 462
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v2

    .line 463
    .local v2, "ramBufferSizeMB":D
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    cmpl-double v6, v2, v6

    if-eqz v6, :cond_1

    .line 464
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getDeleteBytesUsed()J

    move-result-wide v6

    long-to-double v6, v6

    mul-double v8, v12, v2

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v8, v10

    cmpl-double v6, v6, v8

    if-lez v6, :cond_1

    .line 465
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "DW"

    invoke-virtual {v6, v7}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 466
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v7, "DW"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "force apply deletes bytesUsed="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v9}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getDeleteBytesUsed()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " vs ramBuffer="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    mul-double v10, v12, v2

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-direct {p0, v6}, Lorg/apache/lucene/index/DocumentsWriter;->applyAllDeletes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V

    .line 471
    :cond_1
    return v0

    .line 395
    .end local v2    # "ramBufferSizeMB":D
    :cond_2
    const/4 v0, 0x1

    .line 396
    const/4 v4, 0x0

    .line 397
    .local v4, "success":Z
    const/4 v5, 0x0

    .line 399
    .local v5, "ticket":Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;
    :try_start_0
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_3

    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eqz v6, :cond_3

    .line 400
    iget-object v6, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v6, v7, :cond_3

    new-instance v6, Ljava/lang/AssertionError;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "expected: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 401
    iget-object v8, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "but was: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 402
    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v8}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->isFullFlush()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    :catchall_0
    move-exception v6

    .line 449
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v7, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doAfterFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 450
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    .line 451
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v7, v7, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 452
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 453
    throw v6

    .line 419
    :cond_3
    :try_start_1
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->addFlushTicket(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;

    move-result-object v5

    .line 422
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->flush()Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;

    move-result-object v1

    .line 423
    .local v1, "newSegment":Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v6, v5, v1}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->addSegment(Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 425
    const/4 v4, 0x1

    .line 427
    if-nez v4, :cond_4

    if-eqz v5, :cond_4

    .line 431
    :try_start_2
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v6, v5}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->markTicketFailed(Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;)V

    .line 438
    :cond_4
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->getTicketCount()I

    move-result v6

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v7}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v7

    if-lt v6, v7, :cond_6

    .line 443
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v6, p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->forcePurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 449
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6, p1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doAfterFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    .line 450
    invoke-virtual {p1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    .line 451
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v6, v6, Lorg/apache/lucene/index/IndexWriter;->flushCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 452
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v6}, Lorg/apache/lucene/index/IndexWriter;->doAfterFlush()V

    .line 455
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->nextPendingFlush()Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object p1

    goto/16 :goto_0

    .line 426
    .end local v1    # "newSegment":Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    :catchall_1
    move-exception v6

    .line 427
    if-nez v4, :cond_5

    if-eqz v5, :cond_5

    .line 431
    :try_start_3
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v7, v5}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->markTicketFailed(Lorg/apache/lucene/index/DocumentsWriterFlushQueue$SegmentFlushTicket;)V

    .line 433
    :cond_5
    throw v6

    .line 445
    .restart local v1    # "newSegment":Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    :cond_6
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v6, p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->tryPurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private ensureOpen()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/store/AlreadyClosedException;
        }
    .end annotation

    .prologue
    .line 197
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->closed:Z

    if-eqz v0, :cond_0

    .line 198
    new-instance v0, Lorg/apache/lucene/store/AlreadyClosedException;

    const-string v1, "this IndexWriter is closed"

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/AlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_0
    return-void
.end method

.method private postUpdate(Lorg/apache/lucene/index/DocumentsWriterPerThread;Z)Z
    .locals 2
    .param p1, "flushingDWPT"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .param p2, "maybeMerge"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doApplyAllDeletes()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->applyAllDeletes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V

    .line 315
    :cond_0
    if-eqz p1, :cond_2

    .line 316
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriter;->doFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Z

    move-result v1

    or-int/2addr p2, v1

    .line 324
    :cond_1
    :goto_0
    return p2

    .line 318
    :cond_2
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->nextPendingFlush()Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v0

    .line 319
    .local v0, "nextPendingFlush":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    if-eqz v0, :cond_1

    .line 320
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriter;->doFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Z

    move-result v1

    or-int/2addr p2, v1

    goto :goto_0
.end method

.method private preUpdate()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->ensureOpen()V

    .line 281
    const/4 v1, 0x0

    .line 282
    .local v1, "maybeMerge":Z
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->anyStalledThreads()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numQueuedFlushes()I

    move-result v2

    if-lez v2, :cond_3

    .line 284
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 285
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DW"

    const-string v4, "DocumentsWriter has queued dwpt; will hijack this thread to flush pending segment(s)"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_1
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->nextPendingFlush()Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v0

    .local v0, "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    if-nez v0, :cond_4

    .line 295
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 296
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->anyStalledThreads()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 297
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DW"

    const-string v4, "WARNING DocumentsWriter has stalled threads; waiting"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->waitIfStalled()V

    .line 302
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->numQueuedFlushes()I

    move-result v2

    if-nez v2, :cond_1

    .line 304
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DW"

    invoke-virtual {v2, v3}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 305
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v3, "DW"

    const-string v4, "continue indexing after helping out flushing DocumentsWriter is healthy"

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    .end local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_3
    return v1

    .line 292
    .restart local v0    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_4
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriter;->doFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Z

    move-result v2

    or-int/2addr v1, v2

    goto :goto_0
.end method

.method private publishFlushedSegment(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 5
    .param p1, "newSegment"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    .param p2, "globalPacket"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 506
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 507
    :cond_0
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 508
    :cond_1
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentDeletes:Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 510
    .local v0, "segmentDeletes":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "publishFlushedSegment seg-private deletes="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DW"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 515
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "DW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flush: push buffered seg private deletes: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    :cond_3
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    iget-object v2, p1, Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;->segmentInfo:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v1, v2, v0, p2}, Lorg/apache/lucene/index/IndexWriter;->publishFlushedSegment(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/FrozenBufferedDeletes;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 519
    return-void
.end method

.method private declared-synchronized setFlushingDeleteQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z
    .locals 1
    .param p1, "session"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .prologue
    .line 526
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 526
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method declared-synchronized abort()V
    .locals 9

    .prologue
    .line 207
    monitor-enter p0

    const/4 v3, 0x0

    .line 210
    .local v3, "success":Z
    :try_start_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->clear()V

    .line 211
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "DW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 212
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "DW"

    const-string v6, "abort"

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getActiveThreadState()I

    move-result v1

    .line 216
    .local v1, "limit":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_2

    .line 234
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->abortPendingFlushes()V

    .line 235
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->waitForFlush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 236
    const/4 v3, 0x1

    .line 238
    :try_start_1
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "DW"

    invoke-virtual {v4, v5}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 239
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v5, "DW"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "done abort; abortedFiles="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->abortedFiles:Ljava/util/Collection;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " success="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 242
    :cond_1
    monitor-exit p0

    return-void

    .line 217
    :cond_2
    :try_start_2
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v4, v0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getThreadState(I)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v2

    .line 218
    .local v2, "perThread":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->lock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 220
    :try_start_3
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v4

    if-eqz v4, :cond_5

    .line 222
    :try_start_4
    iget-object v4, v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->abort()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 224
    :try_start_5
    iget-object v4, v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    .line 225
    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v4, v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 231
    :cond_3
    :try_start_6
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v4

    .line 224
    :try_start_7
    iget-object v5, v2, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    .line 225
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v5, v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 226
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 230
    :catchall_1
    move-exception v4

    .line 231
    :try_start_8
    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 232
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 237
    .end local v0    # "i":I
    .end local v1    # "limit":I
    .end local v2    # "perThread":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :catchall_2
    move-exception v4

    .line 238
    :try_start_9
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DW"

    invoke-virtual {v5, v6}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 239
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v6, "DW"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "done abort; abortedFiles="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lorg/apache/lucene/index/DocumentsWriter;->abortedFiles:Ljava/util/Collection;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " success="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_4
    throw v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 207
    :catchall_3
    move-exception v4

    monitor-exit p0

    throw v4

    .line 228
    .restart local v0    # "i":I
    .restart local v1    # "limit":I
    .restart local v2    # "perThread":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :cond_5
    :try_start_a
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->closed:Z

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1
.end method

.method abortedFiles()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->abortedFiles:Ljava/util/Collection;

    return-object v0
.end method

.method anyChanges()Z
    .locals 4

    .prologue
    .line 245
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "anyChanges? numDocsInRam="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 247
    const-string v3, " deletes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->anyDeletions()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " hasTickets:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 248
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->hasTickets()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pendingChangesInFullFlush: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 249
    iget-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingChangesInCurrentFullFlush:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 246
    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->anyDeletions()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->hasTickets()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingChangesInCurrentFullFlush:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public anyDeletions()Z
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->anyChanges()Z

    move-result v0

    return v0
.end method

.method close()V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->closed:Z

    .line 276
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->setClosed()V

    .line 277
    return-void
.end method

.method currentDeleteSession()Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    return-object v0
.end method

.method varargs declared-synchronized deleteQueries([Lorg/apache/lucene/search/Query;)V
    .locals 1
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->addDelete([Lorg/apache/lucene/search/Query;)V

    .line 157
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnDelete()V

    .line 158
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doApplyAllDeletes()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriter;->applyAllDeletes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :cond_0
    monitor-exit p0

    return-void

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method varargs declared-synchronized deleteTerms([Lorg/apache/lucene/index/Term;)V
    .locals 2
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 168
    .local v0, "deleteQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->addDelete([Lorg/apache/lucene/index/Term;)V

    .line 169
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnDelete()V

    .line 170
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doApplyAllDeletes()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriter;->applyAllDeletes(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_0
    monitor-exit p0

    return-void

    .line 167
    .end local v0    # "deleteQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method finishFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V
    .locals 4
    .param p1, "newSegment"    # Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;
    .param p2, "bufferedDeletes"    # Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 478
    if-nez p1, :cond_2

    .line 479
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 480
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lorg/apache/lucene/index/FrozenBufferedDeletes;->any()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 481
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->indexWriter:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/IndexWriter;->publishFrozenDeletes(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    .line 482
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 483
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flush: push buffered deletes: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    :cond_1
    :goto_0
    return-void

    .line 487
    :cond_2
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/DocumentsWriter;->publishFlushedSegment(Lorg/apache/lucene/index/DocumentsWriterPerThread$FlushedSegment;Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    goto :goto_0
.end method

.method final finishFullFlush(Z)V
    .locals 5
    .param p1, "success"    # Z

    .prologue
    const/4 v4, 0x0

    .line 580
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DW"

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "DW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " finishFullFlush success="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :cond_0
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/DocumentsWriter;->setFlushingDeleteQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    :catchall_0
    move-exception v0

    .line 591
    iput-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingChangesInCurrentFullFlush:Z

    .line 592
    throw v0

    .line 584
    :cond_1
    if-eqz p1, :cond_2

    .line 586
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->finishFullFlush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591
    :goto_0
    iput-boolean v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingChangesInCurrentFullFlush:Z

    .line 594
    return-void

    .line 588
    :cond_2
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->abortFullFlushes()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final flushAllThreads()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 538
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "DW"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 539
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "DW"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " startFullFlush"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_0
    monitor-enter p0

    .line 543
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriter;->anyChanges()Z

    move-result v3

    iput-boolean v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->pendingChangesInCurrentFullFlush:Z

    .line 544
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    .line 548
    .local v2, "flushingDeleteQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->markForFullFlush()V

    .line 549
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    invoke-direct {p0, v2}, Lorg/apache/lucene/index/DocumentsWriter;->setFlushingDeleteQueue(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 542
    .end local v2    # "flushingDeleteQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v2    # "flushingDeleteQueue":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 551
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 552
    :cond_2
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-ne v3, v4, :cond_3

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 554
    :cond_3
    const/4 v0, 0x0

    .line 558
    .local v0, "anythingFlushed":Z
    :goto_0
    :try_start_2
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->nextPendingFlush()Lorg/apache/lucene/index/DocumentsWriterPerThread;

    move-result-object v1

    .local v1, "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    if-nez v1, :cond_6

    .line 562
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->waitForFlush()V

    .line 563
    if-nez v0, :cond_7

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->anyChanges()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 564
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "DW"

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 565
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v4, "DW"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ": flush naked frozen global deletes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :cond_4
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v3, p0, v2}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->addDeletesAndPurge(Lorg/apache/lucene/index/DocumentsWriter;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;)V

    .line 571
    :goto_1
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_9

    invoke-virtual {v2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->anyChanges()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v3}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->hasTickets()Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_5
    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 572
    .end local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :catchall_1
    move-exception v3

    .line 573
    sget-boolean v4, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v4, :cond_8

    iget-object v4, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v2, v4, :cond_8

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 559
    .restart local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_6
    :try_start_3
    invoke-direct {p0, v1}, Lorg/apache/lucene/index/DocumentsWriter;->doFlush(Lorg/apache/lucene/index/DocumentsWriterPerThread;)Z

    move-result v3

    or-int/2addr v0, v3

    goto :goto_0

    .line 569
    :cond_7
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->ticketQueue:Lorg/apache/lucene/index/DocumentsWriterFlushQueue;

    invoke-virtual {v3, p0}, Lorg/apache/lucene/index/DocumentsWriterFlushQueue;->forcePurge(Lorg/apache/lucene/index/DocumentsWriter;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 574
    .end local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_8
    throw v3

    .line 573
    .restart local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :cond_9
    sget-boolean v3, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriter;->currentFullFlushDelQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    if-eq v2, v3, :cond_a

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 575
    :cond_a
    return v0
.end method

.method public getBufferedDeleteTermsSize()I
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->getBufferedDeleteTermsSize()I

    move-result v0

    return v0
.end method

.method public getNumBufferedDeleteTerms()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->deleteQueue:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->numGlobalTermDeletes()I

    move-result v0

    return v0
.end method

.method getNumDocs()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method final subtractFlushedNumDocs(I)V
    .locals 3
    .param p1, "numFlushed"    # I

    .prologue
    .line 492
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 493
    .local v0, "oldValue":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    sub-int v2, v0, p1

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496
    return-void

    .line 494
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    goto :goto_0
.end method

.method updateDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    .locals 7
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/Term;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 361
    .local p1, "doc":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->preUpdate()Z

    move-result v3

    .line 363
    .local v3, "maybeMerge":Z
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->obtainAndLock()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v4

    .line 369
    .local v4, "perThread":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :try_start_0
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v5

    if-nez v5, :cond_0

    .line 370
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->ensureOpen()V

    .line 371
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "perThread is not active but we are still open"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :catchall_0
    move-exception v5

    .line 386
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 387
    throw v5

    .line 374
    :cond_0
    :try_start_1
    iget-object v0, v4, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 376
    .local v0, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_start_2
    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->updateDocument(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)V

    .line 377
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 379
    :try_start_3
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 380
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v5, v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 383
    :cond_1
    if-eqz p3, :cond_3

    const/4 v2, 0x1

    .line 384
    .local v2, "isUpdate":Z
    :goto_0
    iget-object v5, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v5, v4, v2}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doAfterDocument(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 386
    .local v1, "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    invoke-virtual {v4}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 389
    invoke-direct {p0, v1, v3}, Lorg/apache/lucene/index/DocumentsWriter;->postUpdate(Lorg/apache/lucene/index/DocumentsWriterPerThread;Z)Z

    move-result v5

    return v5

    .line 378
    .end local v1    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .end local v2    # "isUpdate":Z
    :catchall_1
    move-exception v5

    .line 379
    :try_start_4
    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 380
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6, v4}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 382
    :cond_2
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 383
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method updateDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)Z
    .locals 8
    .param p2, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p3, "delTerm"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Iterable",
            "<+",
            "Lorg/apache/lucene/index/IndexableField;",
            ">;>;",
            "Lorg/apache/lucene/analysis/Analyzer;",
            "Lorg/apache/lucene/index/Term;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 329
    .local p1, "docs":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Ljava/lang/Iterable<+Lorg/apache/lucene/index/IndexableField;>;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->preUpdate()Z

    move-result v4

    .line 331
    .local v4, "maybeMerge":Z
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->obtainAndLock()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v5

    .line 335
    .local v5, "perThread":Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    :try_start_0
    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive()Z

    move-result v6

    if-nez v6, :cond_0

    .line 336
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriter;->ensureOpen()V

    .line 337
    sget-boolean v6, Lorg/apache/lucene/index/DocumentsWriter;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "perThread is not active but we are still open"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    :catchall_0
    move-exception v6

    .line 352
    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 353
    throw v6

    .line 340
    :cond_0
    :try_start_1
    iget-object v1, v5, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342
    .local v1, "dwpt":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_start_2
    invoke-virtual {v1, p1, p2, p3}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->updateDocuments(Ljava/lang/Iterable;Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/index/Term;)I

    move-result v0

    .line 343
    .local v0, "docCount":I
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->numDocsInRAM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v6, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 345
    :try_start_3
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 346
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6, v5}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 349
    :cond_1
    if-eqz p3, :cond_3

    const/4 v3, 0x1

    .line 350
    .local v3, "isUpdate":Z
    :goto_0
    iget-object v6, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v6, v5, v3}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doAfterDocument(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Z)Lorg/apache/lucene/index/DocumentsWriterPerThread;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 352
    .local v2, "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    invoke-virtual {v5}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->unlock()V

    .line 355
    invoke-direct {p0, v2, v4}, Lorg/apache/lucene/index/DocumentsWriter;->postUpdate(Lorg/apache/lucene/index/DocumentsWriterPerThread;Z)Z

    move-result v6

    return v6

    .line 344
    .end local v0    # "docCount":I
    .end local v2    # "flushingDWPT":Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .end local v3    # "isUpdate":Z
    :catchall_1
    move-exception v6

    .line 345
    :try_start_4
    invoke-virtual {v1}, Lorg/apache/lucene/index/DocumentsWriterPerThread;->checkAndResetHasAborted()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 346
    iget-object v7, p0, Lorg/apache/lucene/index/DocumentsWriter;->flushControl:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    invoke-virtual {v7, v5}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->doOnAbort(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)V

    .line 348
    :cond_2
    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 349
    .restart local v0    # "docCount":I
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
