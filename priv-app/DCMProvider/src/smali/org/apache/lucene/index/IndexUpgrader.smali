.class public final Lorg/apache/lucene/index/IndexUpgrader;
.super Ljava/lang/Object;
.source "IndexUpgrader.java"


# instance fields
.field private final deletePriorCommits:Z

.field private final dir:Lorg/apache/lucene/store/Directory;

.field private final iwc:Lorg/apache/lucene/index/IndexWriterConfig;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;Z)V
    .locals 0
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "iwc"    # Lorg/apache/lucene/index/IndexWriterConfig;
    .param p3, "deletePriorCommits"    # Z

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    .line 134
    iput-object p2, p0, Lorg/apache/lucene/index/IndexUpgrader;->iwc:Lorg/apache/lucene/index/IndexWriterConfig;

    .line 135
    iput-boolean p3, p0, Lorg/apache/lucene/index/IndexUpgrader;->deletePriorCommits:Z

    .line 136
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/util/Version;)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    .line 119
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/lucene/index/IndexUpgrader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;Z)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/util/Version;Ljava/io/PrintStream;Z)V
    .locals 2
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;
    .param p3, "infoStream"    # Ljava/io/PrintStream;
    .param p4, "deletePriorCommits"    # Z

    .prologue
    .line 126
    new-instance v0, Lorg/apache/lucene/index/IndexWriterConfig;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lorg/apache/lucene/index/IndexWriterConfig;-><init>(Lorg/apache/lucene/util/Version;Lorg/apache/lucene/analysis/Analyzer;)V

    invoke-virtual {v0, p3}, Lorg/apache/lucene/index/IndexWriterConfig;->setInfoStream(Ljava/io/PrintStream;)Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    invoke-direct {p0, p1, v0, p4}, Lorg/apache/lucene/index/IndexUpgrader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;Z)V

    .line 127
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 9
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v6, 0x0

    .line 75
    .local v6, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 76
    .local v1, "deletePriorCommits":Z
    const/4 v5, 0x0

    .line 77
    .local v5, "out":Ljava/io/PrintStream;
    const/4 v3, 0x0

    .line 78
    .local v3, "dirImpl":Ljava/lang/String;
    const/4 v4, 0x0

    .line 79
    .local v4, "i":I
    :goto_0
    array-length v7, p0

    if-lt v4, v7, :cond_1

    .line 99
    if-nez v6, :cond_0

    .line 100
    invoke-static {}, Lorg/apache/lucene/index/IndexUpgrader;->printUsage()V

    .line 103
    :cond_0
    const/4 v2, 0x0

    .line 104
    .local v2, "dir":Lorg/apache/lucene/store/Directory;
    if-nez v3, :cond_7

    .line 105
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lorg/apache/lucene/store/FSDirectory;->open(Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v2

    .line 109
    :goto_1
    new-instance v7, Lorg/apache/lucene/index/IndexUpgrader;

    sget-object v8, Lorg/apache/lucene/util/Version;->LUCENE_CURRENT:Lorg/apache/lucene/util/Version;

    invoke-direct {v7, v2, v8, v5, v1}, Lorg/apache/lucene/index/IndexUpgrader;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/util/Version;Ljava/io/PrintStream;Z)V

    invoke-virtual {v7}, Lorg/apache/lucene/index/IndexUpgrader;->upgrade()V

    .line 110
    return-void

    .line 80
    .end local v2    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_1
    aget-object v0, p0, v4

    .line 81
    .local v0, "arg":Ljava/lang/String;
    const-string v7, "-delete-prior-commits"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 82
    const/4 v1, 0x1

    .line 97
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 83
    :cond_2
    const-string v7, "-verbose"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 84
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 85
    goto :goto_2

    :cond_3
    if-nez v6, :cond_4

    .line 86
    move-object v6, v0

    .line 87
    goto :goto_2

    :cond_4
    const-string v7, "-dir-impl"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 88
    array-length v7, p0

    add-int/lit8 v7, v7, -0x1

    if-ne v4, v7, :cond_5

    .line 89
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "ERROR: missing value for -dir-impl option"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 90
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    .line 92
    :cond_5
    add-int/lit8 v4, v4, 0x1

    .line 93
    aget-object v3, p0, v4

    .line 94
    goto :goto_2

    .line 95
    :cond_6
    invoke-static {}, Lorg/apache/lucene/index/IndexUpgrader;->printUsage()V

    goto :goto_2

    .line 107
    .end local v0    # "arg":Ljava/lang/String;
    .restart local v2    # "dir":Lorg/apache/lucene/store/Directory;
    :cond_7
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v7}, Lorg/apache/lucene/util/CommandLineUtil;->newFSDirectory(Ljava/lang/String;Ljava/io/File;)Lorg/apache/lucene/store/FSDirectory;

    move-result-object v2

    goto :goto_1
.end method

.method private static printUsage()V
    .locals 3

    .prologue
    .line 56
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Upgrades an index so all segments created with a previous Lucene version are rewritten."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 57
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Usage:"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 58
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  java "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lorg/apache/lucene/index/IndexUpgrader;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " [-delete-prior-commits] [-verbose] [-dir-impl X] indexDir"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 59
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "This tool keeps only the last commit in an index; for this"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 60
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "reason, if the incoming index has more than one commit, the tool"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "refuses to run by default. Specify -delete-prior-commits to override"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 62
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "this, allowing the tool to delete all but the last commit."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 63
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Specify a "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 64
    const-string v2, " implementation through the -dir-impl option to force its use. If no package is specified the "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 65
    const-class v2, Lorg/apache/lucene/store/FSDirectory;

    invoke-virtual {v2}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " package will be used."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 66
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "WARNING: This tool may reorder document IDs!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 68
    return-void
.end method


# virtual methods
.method public upgrade()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 140
    iget-object v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-static {v4}, Lorg/apache/lucene/index/DirectoryReader;->indexExists(Lorg/apache/lucene/store/Directory;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 141
    new-instance v4, Lorg/apache/lucene/index/IndexNotFoundException;

    iget-object v5, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5}, Lorg/apache/lucene/store/Directory;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/IndexNotFoundException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 144
    :cond_0
    iget-boolean v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->deletePriorCommits:Z

    if-nez v4, :cond_1

    .line 145
    iget-object v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-static {v4}, Lorg/apache/lucene/index/DirectoryReader;->listCommits(Lorg/apache/lucene/store/Directory;)Ljava/util/List;

    move-result-object v1

    .line 146
    .local v1, "commits":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexCommit;>;"
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v4

    if-le v4, v5, :cond_1

    .line 147
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "This tool was invoked to not delete prior commit points, but the following commits were found: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 151
    .end local v1    # "commits":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/IndexCommit;>;"
    :cond_1
    iget-object v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->iwc:Lorg/apache/lucene/index/IndexWriterConfig;

    invoke-virtual {v4}, Lorg/apache/lucene/index/IndexWriterConfig;->clone()Lorg/apache/lucene/index/IndexWriterConfig;

    move-result-object v0

    .line 152
    .local v0, "c":Lorg/apache/lucene/index/IndexWriterConfig;
    new-instance v4, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;

    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/UpgradeIndexMergePolicy;-><init>(Lorg/apache/lucene/index/MergePolicy;)V

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/IndexWriterConfig;->setMergePolicy(Lorg/apache/lucene/index/MergePolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 153
    new-instance v4, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;

    invoke-direct {v4}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;-><init>()V

    invoke-virtual {v0, v4}, Lorg/apache/lucene/index/IndexWriterConfig;->setIndexDeletionPolicy(Lorg/apache/lucene/index/IndexDeletionPolicy;)Lorg/apache/lucene/index/IndexWriterConfig;

    .line 155
    new-instance v3, Lorg/apache/lucene/index/IndexWriter;

    iget-object v4, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-direct {v3, v4, v0}, Lorg/apache/lucene/index/IndexWriter;-><init>(Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/IndexWriterConfig;)V

    .line 157
    .local v3, "w":Lorg/apache/lucene/index/IndexWriter;
    :try_start_0
    invoke-virtual {v0}, Lorg/apache/lucene/index/IndexWriterConfig;->getInfoStream()Lorg/apache/lucene/util/InfoStream;

    move-result-object v2

    .line 158
    .local v2, "infoStream":Lorg/apache/lucene/util/InfoStream;
    const-string v4, "IndexUpgrader"

    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 159
    const-string v4, "IndexUpgrader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Upgrading all pre-"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " segments of index directory \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/lucene/index/IndexUpgrader;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' to version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/IndexWriter;->forceMerge(I)V

    .line 162
    const-string v4, "IndexUpgrader"

    invoke-virtual {v2, v4}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 163
    const-string v4, "IndexUpgrader"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "All segments upgraded to version "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lorg/apache/lucene/util/Constants;->LUCENE_MAIN_VERSION:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    :cond_3
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 168
    return-void

    .line 165
    .end local v2    # "infoStream":Lorg/apache/lucene/util/InfoStream;
    :catchall_0
    move-exception v4

    .line 166
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexWriter;->close()V

    .line 167
    throw v4
.end method
