.class public Lorg/apache/lucene/index/CheckIndex$Status;
.super Ljava/lang/Object;
.source "CheckIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CheckIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/CheckIndex$Status$DocValuesStatus;,
        Lorg/apache/lucene/index/CheckIndex$Status$FieldNormStatus;,
        Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;,
        Lorg/apache/lucene/index/CheckIndex$Status$StoredFieldStatus;,
        Lorg/apache/lucene/index/CheckIndex$Status$TermIndexStatus;,
        Lorg/apache/lucene/index/CheckIndex$Status$TermVectorStatus;
    }
.end annotation


# instance fields
.field public cantOpenSegments:Z

.field public clean:Z

.field public dir:Lorg/apache/lucene/store/Directory;

.field public maxSegmentName:I

.field public missingSegmentVersion:Z

.field public missingSegments:Z

.field newSegments:Lorg/apache/lucene/index/SegmentInfos;

.field public numBadSegments:I

.field public numSegments:I

.field public partial:Z

.field public segmentInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/CheckIndex$Status$SegmentInfoStatus;",
            ">;"
        }
    .end annotation
.end field

.field public segmentsChecked:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public segmentsFileName:Ljava/lang/String;

.field public toolOutOfDate:Z

.field public totLoseDocCount:I

.field public userData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public validCounter:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex$Status;->segmentsChecked:Ljava/util/List;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CheckIndex$Status;->segmentInfos:Ljava/util/List;

    .line 75
    return-void
.end method
