.class public abstract Lorg/apache/lucene/index/SortedDocValues;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "SortedDocValues.java"


# static fields
.field public static final EMPTY:Lorg/apache/lucene/index/SortedDocValues;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lorg/apache/lucene/index/SortedDocValues$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/SortedDocValues$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/SortedDocValues;->EMPTY:Lorg/apache/lucene/index/SortedDocValues;

    .line 88
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 3
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/SortedDocValues;->getOrd(I)I

    move-result v0

    .line 61
    .local v0, "ord":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 62
    sget-object v1, Lorg/apache/lucene/index/SortedDocValues;->MISSING:[B

    iput-object v1, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 63
    iput v2, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 64
    iput v2, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p0, v0, p2}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    goto :goto_0
.end method

.method public abstract getOrd(I)I
.end method

.method public abstract getValueCount()I
.end method

.method public abstract lookupOrd(ILorg/apache/lucene/util/BytesRef;)V
.end method

.method public lookupTerm(Lorg/apache/lucene/util/BytesRef;)I
    .locals 6
    .param p1, "key"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 97
    new-instance v4, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v4}, Lorg/apache/lucene/util/BytesRef;-><init>()V

    .line 98
    .local v4, "spare":Lorg/apache/lucene/util/BytesRef;
    const/4 v2, 0x0

    .line 99
    .local v2, "low":I
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedDocValues;->getValueCount()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .line 101
    .local v1, "high":I
    :goto_0
    if-le v2, v1, :cond_1

    .line 115
    add-int/lit8 v5, v2, 0x1

    neg-int v3, v5

    :cond_0
    return v3

    .line 102
    :cond_1
    add-int v5, v2, v1

    ushr-int/lit8 v3, v5, 0x1

    .line 103
    .local v3, "mid":I
    invoke-virtual {p0, v3, v4}, Lorg/apache/lucene/index/SortedDocValues;->lookupOrd(ILorg/apache/lucene/util/BytesRef;)V

    .line 104
    invoke-virtual {v4, p1}, Lorg/apache/lucene/util/BytesRef;->compareTo(Lorg/apache/lucene/util/BytesRef;)I

    move-result v0

    .line 106
    .local v0, "cmp":I
    if-gez v0, :cond_2

    .line 107
    add-int/lit8 v2, v3, 0x1

    .line 108
    goto :goto_0

    :cond_2
    if-lez v0, :cond_0

    .line 109
    add-int/lit8 v1, v3, -0x1

    .line 110
    goto :goto_0
.end method

.method public termsEnum()Lorg/apache/lucene/index/TermsEnum;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/SortedDocValuesTermsEnum;-><init>(Lorg/apache/lucene/index/SortedDocValues;)V

    return-object v0
.end method
