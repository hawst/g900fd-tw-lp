.class public abstract Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
.super Ljava/lang/Object;
.source "TieredMergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TieredMergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "MergeScore"
.end annotation


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    return-void
.end method


# virtual methods
.method abstract getExplanation()Ljava/lang/String;
.end method

.method abstract getScore()D
.end method
