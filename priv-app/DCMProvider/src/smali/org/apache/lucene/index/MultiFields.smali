.class public final Lorg/apache/lucene/index/MultiFields;
.super Lorg/apache/lucene/index/Fields;
.source "MultiFields.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final subSlices:[Lorg/apache/lucene/index/ReaderSlice;

.field private final subs:[Lorg/apache/lucene/index/Fields;

.field private final terms:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/Terms;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lorg/apache/lucene/index/MultiFields;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiFields;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/index/Fields;[Lorg/apache/lucene/index/ReaderSlice;)V
    .locals 1
    .param p1, "subs"    # [Lorg/apache/lucene/index/Fields;
    .param p2, "subSlices"    # [Lorg/apache/lucene/index/ReaderSlice;

    .prologue
    .line 193
    invoke-direct {p0}, Lorg/apache/lucene/index/Fields;-><init>()V

    .line 51
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/MultiFields;->terms:Ljava/util/Map;

    .line 194
    iput-object p1, p0, Lorg/apache/lucene/index/MultiFields;->subs:[Lorg/apache/lucene/index/Fields;

    .line 195
    iput-object p2, p0, Lorg/apache/lucene/index/MultiFields;->subSlices:[Lorg/apache/lucene/index/ReaderSlice;

    .line 196
    return-void
.end method

.method public static getFields(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/Fields;
    .locals 13
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x0

    .line 62
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v3

    .line 63
    .local v3, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 71
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .local v2, "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Fields;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v5, "slices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ReaderSlice;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 81
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 86
    .end local v2    # "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Fields;>;"
    .end local v5    # "slices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ReaderSlice;>;"
    :goto_1
    :pswitch_0
    return-object v6

    .line 69
    :pswitch_1
    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v6

    goto :goto_1

    .line 73
    .restart local v2    # "fields":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Fields;>;"
    .restart local v5    # "slices":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ReaderSlice;>;"
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 74
    .local v0, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v4

    .line 75
    .local v4, "r":Lorg/apache/lucene/index/AtomicReader;
    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->fields()Lorg/apache/lucene/index/Fields;

    move-result-object v1

    .line 76
    .local v1, "f":Lorg/apache/lucene/index/Fields;
    if-eqz v1, :cond_0

    .line 77
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    new-instance v8, Lorg/apache/lucene/index/ReaderSlice;

    iget v9, v0, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    invoke-virtual {v4}, Lorg/apache/lucene/index/AtomicReader;->maxDoc()I

    move-result v10

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-direct {v8, v9, v10, v11}, Lorg/apache/lucene/index/ReaderSlice;-><init>(III)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 83
    .end local v0    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v1    # "f":Lorg/apache/lucene/index/Fields;
    .end local v4    # "r":Lorg/apache/lucene/index/AtomicReader;
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 84
    invoke-interface {v2, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/Fields;

    goto :goto_1

    .line 86
    :cond_3
    new-instance v8, Lorg/apache/lucene/index/MultiFields;

    sget-object v6, Lorg/apache/lucene/index/Fields;->EMPTY_ARRAY:[Lorg/apache/lucene/index/Fields;

    invoke-interface {v2, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/lucene/index/Fields;

    .line 87
    sget-object v7, Lorg/apache/lucene/index/ReaderSlice;->EMPTY_ARRAY:[Lorg/apache/lucene/index/ReaderSlice;

    invoke-interface {v5, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/index/ReaderSlice;

    .line 86
    invoke-direct {v8, v6, v7}, Lorg/apache/lucene/index/MultiFields;-><init>([Lorg/apache/lucene/index/Fields;[Lorg/apache/lucene/index/ReaderSlice;)V

    move-object v6, v8

    goto :goto_1

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getIndexedFields(Lorg/apache/lucene/index/IndexReader;)Ljava/util/Collection;
    .locals 4
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/IndexReader;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 272
    .local v1, "fields":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-static {p0}, Lorg/apache/lucene/index/MultiFields;->getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/FieldInfos;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 277
    return-object v1

    .line 272
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    .line 273
    .local v0, "fieldInfo":Lorg/apache/lucene/index/FieldInfo;
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    iget-object v3, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getLiveDocs(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/util/Bits;
    .locals 8
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    const/4 v7, 0x1

    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->hasDeletions()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 103
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v2

    .line 104
    .local v2, "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 105
    .local v4, "size":I
    sget-boolean v6, Lorg/apache/lucene/index/MultiFields;->$assertionsDisabled:Z

    if-nez v6, :cond_0

    if-gtz v4, :cond_0

    new-instance v6, Ljava/lang/AssertionError;

    const-string v7, "A reader with deletions must have at least one leave"

    invoke-direct {v6, v7}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v6

    .line 106
    :cond_0
    if-ne v4, v7, :cond_1

    .line 107
    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/lucene/index/AtomicReaderContext;

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    .line 120
    .end local v2    # "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    .end local v4    # "size":I
    :goto_0
    return-object v6

    .line 109
    .restart local v2    # "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    .restart local v4    # "size":I
    :cond_1
    new-array v3, v4, [Lorg/apache/lucene/util/Bits;

    .line 110
    .local v3, "liveDocs":[Lorg/apache/lucene/util/Bits;
    add-int/lit8 v6, v4, 0x1

    new-array v5, v6, [I

    .line 111
    .local v5, "starts":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v4, :cond_2

    .line 117
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v6

    aput v6, v5, v4

    .line 118
    new-instance v6, Lorg/apache/lucene/index/MultiBits;

    invoke-direct {v6, v3, v5, v7}, Lorg/apache/lucene/index/MultiBits;-><init>([Lorg/apache/lucene/util/Bits;[IZ)V

    goto :goto_0

    .line 113
    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 114
    .local v0, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v0}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/lucene/index/AtomicReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v6

    aput-object v6, v3, v1

    .line 115
    iget v6, v0, Lorg/apache/lucene/index/AtomicReaderContext;->docBase:I

    aput v6, v5, v1

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 120
    .end local v0    # "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v1    # "i":I
    .end local v2    # "leaves":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/AtomicReaderContext;>;"
    .end local v3    # "liveDocs":[Lorg/apache/lucene/util/Bits;
    .end local v4    # "size":I
    .end local v5    # "starts":[I
    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static getMergedFieldInfos(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/FieldInfos;
    .locals 4
    .param p0, "reader"    # Lorg/apache/lucene/index/IndexReader;

    .prologue
    .line 255
    new-instance v0, Lorg/apache/lucene/index/FieldInfos$Builder;

    invoke-direct {v0}, Lorg/apache/lucene/index/FieldInfos$Builder;-><init>()V

    .line 256
    .local v0, "builder":Lorg/apache/lucene/index/FieldInfos$Builder;
    invoke-virtual {p0}, Lorg/apache/lucene/index/IndexReader;->leaves()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 259
    invoke-virtual {v0}, Lorg/apache/lucene/index/FieldInfos$Builder;->finish()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v2

    return-object v2

    .line 256
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/AtomicReaderContext;

    .line 257
    .local v1, "ctx":Lorg/apache/lucene/index/AtomicReaderContext;
    invoke-virtual {v1}, Lorg/apache/lucene/index/AtomicReaderContext;->reader()Lorg/apache/lucene/index/AtomicReader;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/AtomicReader;->getFieldInfos()Lorg/apache/lucene/index/FieldInfos;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/lucene/index/FieldInfos$Builder;->add(Lorg/apache/lucene/index/FieldInfos;)V

    goto :goto_0
.end method

.method public static getTermDocsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "term"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lorg/apache/lucene/index/MultiFields;->getTermDocsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public static getTermDocsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 4
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 148
    sget-boolean v3, Lorg/apache/lucene/index/MultiFields;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 149
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/index/MultiFields;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-nez p3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 150
    :cond_1
    invoke-static {p0, p2}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    .line 151
    .local v0, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v0, :cond_2

    .line 152
    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 153
    .local v1, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x1

    invoke-virtual {v1, p3, v3}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 154
    invoke-virtual {v1, p1, v2, p4}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v2

    .line 157
    .end local v1    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    return-object v2
.end method

.method public static getTermPositionsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "term"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    const/4 v0, 0x3

    invoke-static {p0, p1, p2, p3, v0}, Lorg/apache/lucene/index/MultiFields;->getTermPositionsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    return-object v0
.end method

.method public static getTermPositionsEnum(Lorg/apache/lucene/index/IndexReader;Lorg/apache/lucene/util/Bits;Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 4
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "liveDocs"    # Lorg/apache/lucene/util/Bits;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 176
    sget-boolean v3, Lorg/apache/lucene/index/MultiFields;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez p2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 177
    :cond_0
    sget-boolean v3, Lorg/apache/lucene/index/MultiFields;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    if-nez p3, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 178
    :cond_1
    invoke-static {p0, p2}, Lorg/apache/lucene/index/MultiFields;->getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v0

    .line 179
    .local v0, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v0, :cond_2

    .line 180
    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/Terms;->iterator(Lorg/apache/lucene/index/TermsEnum;)Lorg/apache/lucene/index/TermsEnum;

    move-result-object v1

    .line 181
    .local v1, "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    const/4 v3, 0x1

    invoke-virtual {v1, p3, v3}, Lorg/apache/lucene/index/TermsEnum;->seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 182
    invoke-virtual {v1, p1, v2, p4}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v2

    .line 185
    .end local v1    # "termsEnum":Lorg/apache/lucene/index/TermsEnum;
    :cond_2
    return-object v2
.end method

.method public static getTerms(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 2
    .param p0, "r"    # Lorg/apache/lucene/index/IndexReader;
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-static {p0}, Lorg/apache/lucene/index/MultiFields;->getFields(Lorg/apache/lucene/index/IndexReader;)Lorg/apache/lucene/index/Fields;

    move-result-object v0

    .line 127
    .local v0, "fields":Lorg/apache/lucene/index/Fields;
    if-nez v0, :cond_0

    .line 128
    const/4 v1, 0x0

    .line 130
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    iget-object v2, p0, Lorg/apache/lucene/index/MultiFields;->subs:[Lorg/apache/lucene/index/Fields;

    array-length v2, v2

    new-array v1, v2, [Ljava/util/Iterator;

    .line 202
    .local v1, "subIterators":[Ljava/util/Iterator;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiFields;->subs:[Lorg/apache/lucene/index/Fields;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 205
    new-instance v2, Lorg/apache/lucene/index/MergedIterator;

    invoke-direct {v2, v1}, Lorg/apache/lucene/index/MergedIterator;-><init>([Ljava/util/Iterator;)V

    return-object v2

    .line 203
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiFields;->subs:[Lorg/apache/lucene/index/Fields;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lorg/apache/lucene/index/Fields;->iterator()Ljava/util/Iterator;

    move-result-object v2

    aput-object v2, v1, v0

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 243
    const/4 v0, -0x1

    return v0
.end method

.method public terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;
    .locals 8
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    iget-object v6, p0, Lorg/apache/lucene/index/MultiFields;->terms:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/Terms;

    .line 211
    .local v1, "result":Lorg/apache/lucene/index/Terms;
    if-eqz v1, :cond_0

    move-object v2, v1

    .line 238
    .end local v1    # "result":Lorg/apache/lucene/index/Terms;
    .local v2, "result":Lorg/apache/lucene/index/Terms;
    :goto_0
    return-object v2

    .line 217
    .end local v2    # "result":Lorg/apache/lucene/index/Terms;
    .restart local v1    # "result":Lorg/apache/lucene/index/Terms;
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 218
    .local v4, "subs2":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/Terms;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v3, "slices2":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/ReaderSlice;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v6, p0, Lorg/apache/lucene/index/MultiFields;->subs:[Lorg/apache/lucene/index/Fields;

    array-length v6, v6

    if-lt v0, v6, :cond_1

    .line 228
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_3

    .line 229
    const/4 v1, 0x0

    :goto_2
    move-object v2, v1

    .line 238
    .end local v1    # "result":Lorg/apache/lucene/index/Terms;
    .restart local v2    # "result":Lorg/apache/lucene/index/Terms;
    goto :goto_0

    .line 222
    .end local v2    # "result":Lorg/apache/lucene/index/Terms;
    .restart local v1    # "result":Lorg/apache/lucene/index/Terms;
    :cond_1
    iget-object v6, p0, Lorg/apache/lucene/index/MultiFields;->subs:[Lorg/apache/lucene/index/Fields;

    aget-object v6, v6, v0

    invoke-virtual {v6, p1}, Lorg/apache/lucene/index/Fields;->terms(Ljava/lang/String;)Lorg/apache/lucene/index/Terms;

    move-result-object v5

    .line 223
    .local v5, "terms":Lorg/apache/lucene/index/Terms;
    if-eqz v5, :cond_2

    .line 224
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v6, p0, Lorg/apache/lucene/index/MultiFields;->subSlices:[Lorg/apache/lucene/index/ReaderSlice;

    aget-object v6, v6, v0

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 233
    .end local v5    # "terms":Lorg/apache/lucene/index/Terms;
    :cond_3
    new-instance v1, Lorg/apache/lucene/index/MultiTerms;

    .end local v1    # "result":Lorg/apache/lucene/index/Terms;
    sget-object v6, Lorg/apache/lucene/index/Terms;->EMPTY_ARRAY:[Lorg/apache/lucene/index/Terms;

    invoke-interface {v4, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lorg/apache/lucene/index/Terms;

    .line 234
    sget-object v7, Lorg/apache/lucene/index/ReaderSlice;->EMPTY_ARRAY:[Lorg/apache/lucene/index/ReaderSlice;

    invoke-interface {v3, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lorg/apache/lucene/index/ReaderSlice;

    .line 233
    invoke-direct {v1, v6, v7}, Lorg/apache/lucene/index/MultiTerms;-><init>([Lorg/apache/lucene/index/Terms;[Lorg/apache/lucene/index/ReaderSlice;)V

    .line 235
    .restart local v1    # "result":Lorg/apache/lucene/index/Terms;
    iget-object v6, p0, Lorg/apache/lucene/index/MultiFields;->terms:Ljava/util/Map;

    invoke-interface {v6, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method
