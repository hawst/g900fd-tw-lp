.class public Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
.super Ljava/lang/Object;
.source "BufferedDeletesStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/BufferedDeletesStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QueryAndLimit"
.end annotation


# instance fields
.field public final limit:I

.field public final query:Lorg/apache/lucene/search/Query;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/search/Query;I)V
    .locals 0
    .param p1, "query"    # Lorg/apache/lucene/search/Query;
    .param p2, "limit"    # I

    .prologue
    .line 435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 436
    iput-object p1, p0, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;->query:Lorg/apache/lucene/search/Query;

    .line 437
    iput p2, p0, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;->limit:I

    .line 438
    return-void
.end method
