.class Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;
.super Ljava/lang/Object;
.source "DocumentsWriterFlushControl.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/DocumentsWriterFlushControl;->getPerThreadsIterator(I)Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;",
        ">;"
    }
.end annotation


# instance fields
.field i:I

.field final synthetic this$0:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

.field private final synthetic val$upto:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriterFlushControl;I)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->this$0:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    iput p2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->val$upto:I

    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->i:I

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 389
    iget v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->i:I

    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->val$upto:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->next()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
    .locals 3

    .prologue
    .line 394
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->this$0:Lorg/apache/lucene/index/DocumentsWriterFlushControl;

    # getter for: Lorg/apache/lucene/index/DocumentsWriterFlushControl;->perThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    invoke-static {v0}, Lorg/apache/lucene/index/DocumentsWriterFlushControl;->access$0(Lorg/apache/lucene/index/DocumentsWriterFlushControl;)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/lucene/index/DocumentsWriterFlushControl$1;->i:I

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;->getThreadState(I)Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 2

    .prologue
    .line 399
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "remove() not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
