.class final Lorg/apache/lucene/index/CompositeReaderContext$Builder;
.super Ljava/lang/Object;
.source "CompositeReaderContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/CompositeReaderContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Builder"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private leafDocBase:I

.field private final leaves:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/AtomicReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private final reader:Lorg/apache/lucene/index/CompositeReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lorg/apache/lucene/index/CompositeReaderContext;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/CompositeReader;)V
    .locals 1
    .param p1, "reader"    # Lorg/apache/lucene/index/CompositeReader;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leaves:Ljava/util/List;

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leafDocBase:I

    .line 87
    iput-object p1, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->reader:Lorg/apache/lucene/index/CompositeReader;

    .line 88
    return-void
.end method

.method private build(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/IndexReader;II)Lorg/apache/lucene/index/IndexReaderContext;
    .locals 15
    .param p1, "parent"    # Lorg/apache/lucene/index/CompositeReaderContext;
    .param p2, "reader"    # Lorg/apache/lucene/index/IndexReader;
    .param p3, "ord"    # I
    .param p4, "docBase"    # I

    .prologue
    .line 95
    move-object/from16 v0, p2

    instance-of v2, v0, Lorg/apache/lucene/index/AtomicReader;

    if-eqz v2, :cond_0

    move-object/from16 v3, p2

    .line 96
    check-cast v3, Lorg/apache/lucene/index/AtomicReader;

    .line 97
    .local v3, "ar":Lorg/apache/lucene/index/AtomicReader;
    new-instance v1, Lorg/apache/lucene/index/AtomicReaderContext;

    iget-object v2, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leaves:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    iget v7, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leafDocBase:I

    move-object/from16 v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    invoke-direct/range {v1 .. v7}, Lorg/apache/lucene/index/AtomicReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/AtomicReader;IIII)V

    .line 98
    .local v1, "atomic":Lorg/apache/lucene/index/AtomicReaderContext;
    iget-object v2, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leaves:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget v2, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leafDocBase:I

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v5

    add-int/2addr v2, v5

    iput v2, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leafDocBase:I

    .line 118
    .end local v1    # "atomic":Lorg/apache/lucene/index/AtomicReaderContext;
    .end local v3    # "ar":Lorg/apache/lucene/index/AtomicReader;
    :goto_0
    return-object v1

    :cond_0
    move-object/from16 v6, p2

    .line 102
    check-cast v6, Lorg/apache/lucene/index/CompositeReader;

    .line 103
    .local v6, "cr":Lorg/apache/lucene/index/CompositeReader;
    invoke-virtual {v6}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v14

    .line 104
    .local v14, "sequentialSubReaders":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexReader;>;"
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lorg/apache/lucene/index/IndexReaderContext;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    .line 106
    .local v9, "children":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/IndexReaderContext;>;"
    if-nez p1, :cond_1

    .line 107
    new-instance v4, Lorg/apache/lucene/index/CompositeReaderContext;

    iget-object v2, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->leaves:Ljava/util/List;

    invoke-direct {v4, v6, v9, v2}, Lorg/apache/lucene/index/CompositeReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReader;Ljava/util/List;Ljava/util/List;)V

    .line 111
    .local v4, "newParent":Lorg/apache/lucene/index/CompositeReaderContext;
    :goto_1
    const/4 v12, 0x0

    .line 112
    .local v12, "newDocBase":I
    const/4 v11, 0x0

    .local v11, "i":I
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v10

    .local v10, "c":I
    :goto_2
    if-lt v11, v10, :cond_2

    .line 117
    sget-boolean v2, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    invoke-virtual {v6}, Lorg/apache/lucene/index/CompositeReader;->maxDoc()I

    move-result v2

    if-eq v12, v2, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 109
    .end local v4    # "newParent":Lorg/apache/lucene/index/CompositeReaderContext;
    .end local v10    # "c":I
    .end local v11    # "i":I
    .end local v12    # "newDocBase":I
    :cond_1
    new-instance v4, Lorg/apache/lucene/index/CompositeReaderContext;

    move-object/from16 v5, p1

    move/from16 v7, p3

    move/from16 v8, p4

    invoke-direct/range {v4 .. v9}, Lorg/apache/lucene/index/CompositeReaderContext;-><init>(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/CompositeReader;IILjava/util/List;)V

    .restart local v4    # "newParent":Lorg/apache/lucene/index/CompositeReaderContext;
    goto :goto_1

    .line 113
    .restart local v10    # "c":I
    .restart local v11    # "i":I
    .restart local v12    # "newDocBase":I
    :cond_2
    invoke-interface {v14, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/apache/lucene/index/IndexReader;

    .line 114
    .local v13, "r":Lorg/apache/lucene/index/IndexReader;
    invoke-direct {p0, v4, v13, v11, v12}, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->build(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/IndexReader;II)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v2

    invoke-interface {v9, v11, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-virtual {v13}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v2

    add-int/2addr v12, v2

    .line 112
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .end local v13    # "r":Lorg/apache/lucene/index/IndexReader;
    :cond_3
    move-object v1, v4

    .line 118
    goto :goto_0
.end method


# virtual methods
.method public build()Lorg/apache/lucene/index/CompositeReaderContext;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 91
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->reader:Lorg/apache/lucene/index/CompositeReader;

    invoke-direct {p0, v0, v1, v2, v2}, Lorg/apache/lucene/index/CompositeReaderContext$Builder;->build(Lorg/apache/lucene/index/CompositeReaderContext;Lorg/apache/lucene/index/IndexReader;II)Lorg/apache/lucene/index/IndexReaderContext;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/CompositeReaderContext;

    return-object v0
.end method
