.class Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;
.super Ljava/lang/Object;
.source "SortedSetDocValuesWriter.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/SortedSetDocValuesWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OrdCountIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field docUpto:I

.field final iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

.field final maxDoc:I

.field final synthetic this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 275
    const-class v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;I)V
    .locals 4
    .param p2, "maxDoc"    # I

    .prologue
    .line 280
    iput-object p1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    # getter for: Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->access$1(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->iterator()Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    .line 281
    iput p2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->maxDoc:I

    .line 282
    sget-boolean v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    # getter for: Lorg/apache/lucene/index/SortedSetDocValuesWriter;->pendingCounts:Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    invoke-static {p1}, Lorg/apache/lucene/index/SortedSetDocValuesWriter;->access$1(Lorg/apache/lucene/index/SortedSetDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v0

    int-to-long v2, p2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 283
    :cond_0
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 287
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->docUpto:I

    iget v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->maxDoc:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Number;
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 295
    :cond_0
    iget v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->docUpto:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->docUpto:I

    .line 297
    iget-object v0, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->iter:Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer$Iterator;->next()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdCountIterator;->next()Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 302
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
