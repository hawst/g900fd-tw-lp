.class Lorg/apache/lucene/index/BinaryDocValuesWriter;
.super Lorg/apache/lucene/index/DocValuesWriter;
.source "BinaryDocValuesWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/BinaryDocValuesWriter$BytesIterator;
    }
.end annotation


# instance fields
.field private addedValues:I

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field private final lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

.field private final pool:Lorg/apache/lucene/util/ByteBlockPool;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V
    .locals 2
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "iwBytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 43
    invoke-direct {p0}, Lorg/apache/lucene/index/DocValuesWriter;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    .line 44
    iput-object p1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 45
    new-instance v0, Lorg/apache/lucene/util/ByteBlockPool;

    new-instance v1, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;

    invoke-direct {v1, p2}, Lorg/apache/lucene/util/ByteBlockPool$DirectTrackingAllocator;-><init>(Lorg/apache/lucene/util/Counter;)V

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/ByteBlockPool;-><init>(Lorg/apache/lucene/util/ByteBlockPool$Allocator;)V

    iput-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    .line 46
    new-instance v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-direct {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 47
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/BinaryDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    return-object v0
.end method

.method static synthetic access$1(Lorg/apache/lucene/index/BinaryDocValuesWriter;)Lorg/apache/lucene/util/ByteBlockPool;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    return-object v0
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public addValue(ILorg/apache/lucene/util/BytesRef;)V
    .locals 4
    .param p1, "docID"    # I
    .param p2, "value"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    const/16 v3, 0x7ffe

    .line 50
    iget v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    if-ge p1, v0, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocValuesField \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" appears more than once in this document (only one value is allowed per field)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    if-nez p2, :cond_1

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "field=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\": null value not allowed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_1
    iget v0, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    if-le v0, v3, :cond_3

    .line 57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DocValuesField \""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v2, v2, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is too large, must be <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_2
    iget v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    .line 63
    iget-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 61
    :cond_3
    iget v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    if-lt v0, p1, :cond_2

    .line 65
    iget v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->addedValues:I

    .line 66
    iget-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->lengths:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    iget v1, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 67
    iget-object v0, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->pool:Lorg/apache/lucene/util/ByteBlockPool;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/util/ByteBlockPool;->append(Lorg/apache/lucene/util/BytesRef;)V

    .line 68
    return-void
.end method

.method public finish(I)V
    .locals 0
    .param p1, "maxDoc"    # I

    .prologue
    .line 72
    return-void
.end method

.method public flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "dvConsumer"    # Lorg/apache/lucene/codecs/DocValuesConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    .line 77
    .local v0, "maxDoc":I
    iget-object v1, p0, Lorg/apache/lucene/index/BinaryDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 78
    new-instance v2, Lorg/apache/lucene/index/BinaryDocValuesWriter$1;

    invoke-direct {v2, p0, v0}, Lorg/apache/lucene/index/BinaryDocValuesWriter$1;-><init>(Lorg/apache/lucene/index/BinaryDocValuesWriter;I)V

    .line 77
    invoke-virtual {p2, v1, v2}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addBinaryField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 84
    return-void
.end method
