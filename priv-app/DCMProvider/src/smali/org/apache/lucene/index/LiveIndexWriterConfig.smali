.class public Lorg/apache/lucene/index/LiveIndexWriterConfig;
.super Ljava/lang/Object;
.source "LiveIndexWriterConfig.java"


# instance fields
.field private final analyzer:Lorg/apache/lucene/analysis/Analyzer;

.field protected volatile codec:Lorg/apache/lucene/codecs/Codec;

.field protected volatile commit:Lorg/apache/lucene/index/IndexCommit;

.field protected volatile delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

.field protected volatile flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

.field protected volatile indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

.field protected volatile indexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

.field protected volatile infoStream:Lorg/apache/lucene/util/InfoStream;

.field protected final matchVersion:Lorg/apache/lucene/util/Version;

.field private volatile maxBufferedDeleteTerms:I

.field private volatile maxBufferedDocs:I

.field protected volatile mergePolicy:Lorg/apache/lucene/index/MergePolicy;

.field protected volatile mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

.field private volatile mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

.field protected volatile openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

.field protected volatile perThreadHardLimitMB:I

.field private volatile ramBufferSizeMB:D

.field protected volatile readerPooling:Z

.field private volatile readerTermsIndexDivisor:I

.field protected volatile similarity:Lorg/apache/lucene/search/similarities/Similarity;

.field private volatile termIndexInterval:I

.field protected volatile writeLockTimeout:J


# direct methods
.method constructor <init>(Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/util/Version;)V
    .locals 4
    .param p1, "analyzer"    # Lorg/apache/lucene/analysis/Analyzer;
    .param p2, "matchVersion"    # Lorg/apache/lucene/util/Version;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 104
    iput-object p2, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 105
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    iput-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->ramBufferSizeMB:D

    .line 106
    iput v2, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDocs:I

    .line 107
    iput v2, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDeleteTerms:I

    .line 108
    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerTermsIndexDivisor:I

    .line 109
    iput-object v3, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .line 110
    const/16 v0, 0x20

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->termIndexInterval:I

    .line 111
    new-instance v0, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 112
    iput-object v3, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    .line 113
    sget-object v0, Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;->CREATE_OR_APPEND:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 114
    invoke-static {}, Lorg/apache/lucene/search/IndexSearcher;->getDefaultSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 115
    new-instance v0, Lorg/apache/lucene/index/ConcurrentMergeScheduler;

    invoke-direct {v0}, Lorg/apache/lucene/index/ConcurrentMergeScheduler;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 116
    sget-wide v0, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    iput-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->writeLockTimeout:J

    .line 117
    sget-object v0, Lorg/apache/lucene/index/DocumentsWriterPerThread;->defaultIndexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .line 118
    invoke-static {}, Lorg/apache/lucene/codecs/Codec;->getDefault()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 119
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->codec:Lorg/apache/lucene/codecs/Codec;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 122
    :cond_0
    invoke-static {}, Lorg/apache/lucene/util/InfoStream;->getDefault()Lorg/apache/lucene/util/InfoStream;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 123
    new-instance v0, Lorg/apache/lucene/index/TieredMergePolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/TieredMergePolicy;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 124
    new-instance v0, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;

    invoke-direct {v0}, Lorg/apache/lucene/index/FlushByRamOrCountsPolicy;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerPooling:Z

    .line 126
    new-instance v0, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 127
    const/16 v0, 0x799

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->perThreadHardLimitMB:I

    .line 128
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/IndexWriterConfig;)V
    .locals 2
    .param p1, "config"    # Lorg/apache/lucene/index/IndexWriterConfig;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDeleteTerms:I

    .line 136
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getMaxBufferedDocs()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDocs:I

    .line 137
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .line 138
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->ramBufferSizeMB:D

    .line 139
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerTermsIndexDivisor:I

    .line 140
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getTermIndexInterval()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->termIndexInterval:I

    .line 141
    iget-object v0, p1, Lorg/apache/lucene/index/IndexWriterConfig;->matchVersion:Lorg/apache/lucene/util/Version;

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->matchVersion:Lorg/apache/lucene/util/Version;

    .line 142
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    .line 143
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    .line 144
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    .line 145
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    .line 146
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    .line 147
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    .line 148
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getWriteLockTimeout()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->writeLockTimeout:J

    .line 149
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexingChain()Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    .line 150
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->codec:Lorg/apache/lucene/codecs/Codec;

    .line 151
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getInfoStream()Lorg/apache/lucene/util/InfoStream;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    .line 152
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    .line 153
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getIndexerThreadPool()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    .line 154
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getReaderPooling()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerPooling:Z

    .line 155
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getFlushPolicy()Lorg/apache/lucene/index/FlushPolicy;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    .line 156
    invoke-virtual {p1}, Lorg/apache/lucene/index/IndexWriterConfig;->getRAMPerThreadHardLimitMB()I

    move-result v0

    iput v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->perThreadHardLimitMB:I

    .line 157
    return-void
.end method


# virtual methods
.method public getAnalyzer()Lorg/apache/lucene/analysis/Analyzer;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    return-object v0
.end method

.method public getCodec()Lorg/apache/lucene/codecs/Codec;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->codec:Lorg/apache/lucene/codecs/Codec;

    return-object v0
.end method

.method getFlushPolicy()Lorg/apache/lucene/index/FlushPolicy;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->flushPolicy:Lorg/apache/lucene/index/FlushPolicy;

    return-object v0
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->commit:Lorg/apache/lucene/index/IndexCommit;

    return-object v0
.end method

.method public getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->delPolicy:Lorg/apache/lucene/index/IndexDeletionPolicy;

    return-object v0
.end method

.method getIndexerThreadPool()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    return-object v0
.end method

.method getIndexingChain()Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexingChain:Lorg/apache/lucene/index/DocumentsWriterPerThread$IndexingChain;

    return-object v0
.end method

.method public getInfoStream()Lorg/apache/lucene/util/InfoStream;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->infoStream:Lorg/apache/lucene/util/InfoStream;

    return-object v0
.end method

.method public getMaxBufferedDeleteTerms()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDeleteTerms:I

    return v0
.end method

.method public getMaxBufferedDocs()I
    .locals 1

    .prologue
    .line 370
    iget v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDocs:I

    return v0
.end method

.method public getMaxThreadStates()I
    .locals 2

    .prologue
    .line 498
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->indexerThreadPool:Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    check-cast v1, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ThreadAffinityDocumentsWriterThreadPool;->getMaxThreadStates()I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "cce":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getMergePolicy()Lorg/apache/lucene/index/MergePolicy;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergePolicy:Lorg/apache/lucene/index/MergePolicy;

    return-object v0
.end method

.method public getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergeScheduler:Lorg/apache/lucene/index/MergeScheduler;

    return-object v0
.end method

.method public getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    return-object v0
.end method

.method public getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->openMode:Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    return-object v0
.end method

.method public getRAMBufferSizeMB()D
    .locals 2

    .prologue
    .line 324
    iget-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->ramBufferSizeMB:D

    return-wide v0
.end method

.method public getRAMPerThreadHardLimitMB()I
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->perThreadHardLimitMB:I

    return v0
.end method

.method public getReaderPooling()Z
    .locals 1

    .prologue
    .line 509
    iget-boolean v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerPooling:Z

    return v0
.end method

.method public getReaderTermsIndexDivisor()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerTermsIndexDivisor:I

    return v0
.end method

.method public getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->similarity:Lorg/apache/lucene/search/similarities/Similarity;

    return-object v0
.end method

.method public getTermIndexInterval()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->termIndexInterval:I

    return v0
.end method

.method public getWriteLockTimeout()J
    .locals 2

    .prologue
    .line 465
    iget-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->writeLockTimeout:J

    return-wide v0
.end method

.method public setMaxBufferedDeleteTerms(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 2
    .param p1, "maxBufferedDeleteTerms"    # I

    .prologue
    .line 245
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 246
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxBufferedDeleteTerms must at least be 1 when enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDeleteTerms:I

    .line 249
    return-object p0
.end method

.method public setMaxBufferedDocs(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 4
    .param p1, "maxBufferedDocs"    # I

    .prologue
    const/4 v1, -0x1

    .line 352
    if-eq p1, v1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 353
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxBufferedDocs must at least be 2 when enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_0
    if-ne p1, v1, :cond_1

    .line 356
    iget-wide v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->ramBufferSizeMB:D

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "at least one of ramBufferSize and maxBufferedDocs must be enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_1
    iput p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDocs:I

    .line 360
    return-object p0
.end method

.method public setMergedSegmentWarmer(Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 0
    .param p1, "mergeSegmentWarmer"    # Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .prologue
    .line 380
    iput-object p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->mergedSegmentWarmer:Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    .line 381
    return-object p0
.end method

.method public setRAMBufferSizeMB(D)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 5
    .param p1, "ramBufferSizeMB"    # D

    .prologue
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 311
    cmpl-double v0, p1, v2

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ramBufferSize should be > 0.0 MB when enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    cmpl-double v0, p1, v2

    if-nez v0, :cond_1

    .line 315
    iget v0, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->maxBufferedDocs:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 316
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "at least one of ramBufferSize and maxBufferedDocs must be enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->ramBufferSizeMB:D

    .line 319
    return-object p0
.end method

.method public setReaderTermsIndexDivisor(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 3
    .param p1, "divisor"    # I

    .prologue
    .line 406
    if-gtz p1, :cond_0

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 407
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "divisor must be >= 1, or -1 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 409
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->readerTermsIndexDivisor:I

    .line 410
    return-object p0
.end method

.method public setTermIndexInterval(I)Lorg/apache/lucene/index/LiveIndexWriterConfig;
    .locals 0
    .param p1, "interval"    # I

    .prologue
    .line 214
    iput p1, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->termIndexInterval:I

    .line 215
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 547
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 548
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "matchVersion="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->matchVersion:Lorg/apache/lucene/util/Version;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 549
    const-string v2, "analyzer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    if-nez v2, :cond_1

    const-string v2, "null"

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    const-string v2, "ramBufferSizeMB="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMBufferSizeMB()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const-string v2, "maxBufferedDocs="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDocs()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    const-string v2, "maxBufferedDeleteTerms="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMaxBufferedDeleteTerms()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 553
    const-string v2, "mergedSegmentWarmer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergedSegmentWarmer()Lorg/apache/lucene/index/IndexWriter$IndexReaderWarmer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    const-string v2, "readerTermsIndexDivisor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    const-string v2, "termIndexInterval="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getTermIndexInterval()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    const-string v2, "delPolicy="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexDeletionPolicy()Lorg/apache/lucene/index/IndexDeletionPolicy;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v0

    .line 558
    .local v0, "commit":Lorg/apache/lucene/index/IndexCommit;
    const-string v2, "commit="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v0, :cond_0

    const-string v0, "null"

    .end local v0    # "commit":Lorg/apache/lucene/index/IndexCommit;
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 559
    const-string v2, "openMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getOpenMode()Lorg/apache/lucene/index/IndexWriterConfig$OpenMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    const-string v2, "similarity="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getSimilarity()Lorg/apache/lucene/search/similarities/Similarity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 561
    const-string v2, "mergeScheduler="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergeScheduler()Lorg/apache/lucene/index/MergeScheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    const-string v2, "default WRITE_LOCK_TIMEOUT="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-wide v4, Lorg/apache/lucene/index/IndexWriterConfig;->WRITE_LOCK_TIMEOUT:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    const-string/jumbo v2, "writeLockTimeout="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getWriteLockTimeout()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 564
    const-string v2, "codec="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    const-string v2, "infoStream="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getInfoStream()Lorg/apache/lucene/util/InfoStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    const-string v2, "mergePolicy="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getMergePolicy()Lorg/apache/lucene/index/MergePolicy;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    const-string v2, "indexerThreadPool="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getIndexerThreadPool()Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    const-string v2, "readerPooling="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getReaderPooling()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 569
    const-string v2, "perThreadHardLimitMB="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getRAMPerThreadHardLimitMB()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 570
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 549
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/LiveIndexWriterConfig;->analyzer:Lorg/apache/lucene/analysis/Analyzer;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0
.end method
