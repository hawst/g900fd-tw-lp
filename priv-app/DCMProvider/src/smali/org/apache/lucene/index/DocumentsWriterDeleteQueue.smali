.class final Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
.super Ljava/lang/Object;
.source "DocumentsWriterDeleteQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;,
        Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;,
        Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$QueryArrayNode;,
        Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermArrayNode;,
        Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater",
            "<",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final generation:J

.field private final globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private final globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

.field private final globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

.field private volatile tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->$assertionsDisabled:Z

    .line 72
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;

    const-class v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    const-string v2, "tail"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    .line 71
    sput-object v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    .line 72
    return-void

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 82
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;-><init>(J)V

    .line 83
    return-void
.end method

.method constructor <init>(J)V
    .locals 1
    .param p1, "generation"    # J

    .prologue
    .line 86
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletes;

    invoke-direct {v0}, Lorg/apache/lucene/index/BufferedDeletes;-><init>()V

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;-><init>(Lorg/apache/lucene/index/BufferedDeletes;J)V

    .line 87
    return-void
.end method

.method constructor <init>(Lorg/apache/lucene/index/BufferedDeletes;J)V
    .locals 2
    .param p1, "globalBufferedDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "generation"    # J

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 90
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    .line 91
    iput-wide p2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->generation:J

    .line 96
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 97
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;-><init>(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V

    iput-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .line 98
    return-void
.end method

.method private forceApplyGlobalSlice()Z
    .locals 4

    .prologue
    .line 385
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 386
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 388
    .local v0, "currentTail":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v1, v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-eq v1, v0, :cond_0

    .line 389
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iput-object v0, v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 390
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    sget-object v3, Lorg/apache/lucene/index/BufferedDeletes;->MAX_INT:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V

    .line 392
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v1}, Lorg/apache/lucene/index/BufferedDeletes;->any()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 394
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 392
    return v1

    .line 393
    :catchall_0
    move-exception v1

    .line 394
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 395
    throw v1
.end method


# virtual methods
.method add(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "item":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 140
    .local v0, "currentTail":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    iget-object v1, v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->next:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 141
    .local v1, "tailNext":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-ne v2, v0, :cond_0

    .line 142
    if-eqz v1, :cond_1

    .line 148
    sget-object v2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->casNext(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    sget-object v2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tailUpdater:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v2, p0, v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 162
    return-void
.end method

.method add(Lorg/apache/lucene/index/Term;Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .param p2, "slice"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .prologue
    .line 114
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;-><init>(Lorg/apache/lucene/index/Term;)V

    .line 116
    .local v0, "termNode":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;
    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->add(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V

    .line 127
    iput-object v0, p2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 128
    sget-boolean v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v2, p2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    const-string v2, "slice head and tail must differ after add"

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 129
    :cond_0
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tryApplyGlobalSlice()V

    .line 131
    return-void
.end method

.method varargs addDelete([Lorg/apache/lucene/index/Term;)V
    .locals 1
    .param p1, "terms"    # [Lorg/apache/lucene/index/Term;

    .prologue
    .line 106
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermArrayNode;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermArrayNode;-><init>([Lorg/apache/lucene/index/Term;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->add(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V

    .line 107
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tryApplyGlobalSlice()V

    .line 108
    return-void
.end method

.method varargs addDelete([Lorg/apache/lucene/search/Query;)V
    .locals 1
    .param p1, "queries"    # [Lorg/apache/lucene/search/Query;

    .prologue
    .line 101
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$QueryArrayNode;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$QueryArrayNode;-><init>([Lorg/apache/lucene/search/Query;)V

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->add(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V

    .line 102
    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tryApplyGlobalSlice()V

    .line 103
    return-void
.end method

.method anyChanges()Z
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 177
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v0}, Lorg/apache/lucene/index/BufferedDeletes;->any()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-ne v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v0, v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;->next:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 180
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 177
    return v0

    .line 178
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    .line 180
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 181
    throw v0
.end method

.method public bytesUsed()J
    .locals 2

    .prologue
    .line 409
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->bytesUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method clear()V
    .locals 3

    .prologue
    .line 304
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 306
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 307
    .local v0, "currentTail":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iput-object v0, v2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iput-object v0, v1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceHead:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 308
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v1}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 312
    return-void

    .line 309
    .end local v0    # "currentTail":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    :catchall_0
    move-exception v1

    .line 310
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 311
    throw v1
.end method

.method freezeGlobalBuffer(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)Lorg/apache/lucene/index/FrozenBufferedDeletes;
    .locals 5
    .param p1, "callerSlice"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .prologue
    .line 204
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 210
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 213
    .local v0, "currentTail":Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;, "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node<*>;"
    if-eqz p1, :cond_0

    .line 215
    iput-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 218
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v2, v2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-eq v2, v0, :cond_1

    .line 219
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iput-object v0, v2, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 220
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    sget-object v4, Lorg/apache/lucene/index/BufferedDeletes;->MAX_INT:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V

    .line 224
    :cond_1
    new-instance v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    .line 225
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    const/4 v3, 0x0

    .line 224
    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/FrozenBufferedDeletes;-><init>(Lorg/apache/lucene/index/BufferedDeletes;Z)V

    .line 226
    .local v1, "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    invoke-virtual {v2}, Lorg/apache/lucene/index/BufferedDeletes;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    iget-object v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 227
    return-object v1

    .line 228
    .end local v1    # "packet":Lorg/apache/lucene/index/FrozenBufferedDeletes;
    :catchall_0
    move-exception v2

    .line 229
    iget-object v3, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 230
    throw v2
.end method

.method public getBufferedDeleteTermsSize()I
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 401
    :try_start_0
    invoke-direct {p0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->forceApplyGlobalSlice()Z

    .line 402
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 404
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 402
    return v0

    .line 403
    :catchall_0
    move-exception v0

    .line 404
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 405
    throw v0
.end method

.method newSlice()Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;
    .locals 2

    .prologue
    .line 234
    new-instance v0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;-><init>(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;)V

    return-object v0
.end method

.method public numGlobalTermDeletes()I
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    iget-object v0, v0, Lorg/apache/lucene/index/BufferedDeletes;->numTermDeletes:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DWDQ: [ generation: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->generation:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method tryApplyGlobalSlice()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    invoke-virtual {p0, v0}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->updateSlice(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalSlice:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferedDeletes:Lorg/apache/lucene/index/BufferedDeletes;

    sget-object v2, Lorg/apache/lucene/index/BufferedDeletes;->MAX_INT:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 201
    :cond_1
    return-void

    .line 197
    :catchall_0
    move-exception v0

    .line 198
    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->globalBufferLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 199
    throw v0
.end method

.method updateSlice(Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;)Z
    .locals 2
    .param p1, "slice"    # Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;

    .prologue
    .line 238
    iget-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    if-eq v0, v1, :cond_0

    .line 239
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;->tail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    iput-object v0, p1, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$DeleteSlice;->sliceTail:Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;

    .line 240
    const/4 v0, 0x1

    .line 242
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
