.class final Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "DocumentsWriterPerThreadPool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ThreadState"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field bytesUsed:J

.field dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

.field volatile flushPending:Z

.field private isActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 2
    .param p1, "dpwt"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive:Z

    .line 65
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 66
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->resetWriter(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V

    return-void
.end method

.method static synthetic access$1(Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive:Z

    return v0
.end method

.method private resetWriter(Lorg/apache/lucene/index/DocumentsWriterPerThread;)V
    .locals 3
    .param p1, "dwpt"    # Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .prologue
    const/4 v2, 0x0

    .line 75
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 76
    :cond_0
    if-nez p1, :cond_1

    .line 77
    iput-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive:Z

    .line 79
    :cond_1
    iput-object p1, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    .line 80
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    .line 81
    iput-boolean v2, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    .line 82
    return-void
.end method


# virtual methods
.method public getBytesUsedPerThread()J
    .locals 2

    .prologue
    .line 99
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 101
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->bytesUsed:J

    return-wide v0
.end method

.method public getDocumentsWriterPerThread()Lorg/apache/lucene/index/DocumentsWriterPerThread;
    .locals 1

    .prologue
    .line 108
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->dwpt:Lorg/apache/lucene/index/DocumentsWriterPerThread;

    return-object v0
.end method

.method isActive()Z
    .locals 1

    .prologue
    .line 90
    sget-boolean v0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 91
    :cond_0
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->isActive:Z

    return v0
.end method

.method public isFlushPending()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lorg/apache/lucene/index/DocumentsWriterPerThreadPool$ThreadState;->flushPending:Z

    return v0
.end method
