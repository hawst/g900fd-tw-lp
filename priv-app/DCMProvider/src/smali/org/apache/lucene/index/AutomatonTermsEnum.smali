.class Lorg/apache/lucene/index/AutomatonTermsEnum;
.super Lorg/apache/lucene/index/FilteredTermsEnum;
.source "AutomatonTermsEnum.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final allTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

.field private final commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

.field private curGen:J

.field private final finite:Z

.field private linear:Z

.field private final linearUpperBound:Lorg/apache/lucene/util/BytesRef;

.field private final runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

.field private final savedStates:Lorg/apache/lucene/util/IntsRef;

.field private final seekBytesRef:Lorg/apache/lucene/util/BytesRef;

.field private final termComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation
.end field

.field private final visited:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lorg/apache/lucene/index/AutomatonTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/AutomatonTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Lorg/apache/lucene/util/automaton/CompiledAutomaton;)V
    .locals 2
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "compiled"    # Lorg/apache/lucene/util/automaton/CompiledAutomaton;

    .prologue
    const/16 v1, 0xa

    .line 79
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;)V

    .line 61
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    .line 67
    new-instance v0, Lorg/apache/lucene/util/BytesRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/BytesRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    .line 166
    new-instance v0, Lorg/apache/lucene/util/IntsRef;

    invoke-direct {v0, v1}, Lorg/apache/lucene/util/IntsRef;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->savedStates:Lorg/apache/lucene/util/IntsRef;

    .line 80
    iget-object v0, p2, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->finite:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->finite:Z

    .line 81
    iget-object v0, p2, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    .line 82
    sget-boolean v0, Lorg/apache/lucene/index/AutomatonTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 83
    :cond_0
    iget-object v0, p2, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    .line 84
    iget-object v0, p2, Lorg/apache/lucene/util/automaton/CompiledAutomaton;->sortedTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->allTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    .line 87
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v0}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->getSize()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    .line 89
    invoke-virtual {p0}, Lorg/apache/lucene/index/AutomatonTermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->termComp:Ljava/util/Comparator;

    .line 90
    return-void
.end method

.method private backtrack(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 309
    move v2, p1

    .end local p1    # "position":I
    .local v2, "position":I
    :goto_0
    add-int/lit8 p1, v2, -0x1

    .end local v2    # "position":I
    .restart local p1    # "position":I
    if-gtz v2, :cond_0

    .line 319
    const/4 p1, -0x1

    .end local p1    # "position":I
    :goto_1
    return p1

    .line 310
    .restart local p1    # "position":I
    :cond_0
    iget-object v3, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v3, v3, p1

    and-int/lit16 v0, v3, 0xff

    .line 313
    .local v0, "nextChar":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nextChar":I
    .local v1, "nextChar":I
    const/16 v3, 0xff

    if-eq v0, v3, :cond_1

    .line 314
    iget-object v3, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v3, v3, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v4, v1

    aput-byte v4, v3, p1

    .line 315
    iget-object v3, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    add-int/lit8 v4, p1, 0x1

    iput v4, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    goto :goto_1

    :cond_1
    move v2, p1

    .end local p1    # "position":I
    .restart local v2    # "position":I
    goto :goto_0
.end method

.method private nextString()Z
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 180
    const/4 v2, 0x0

    .line 181
    .local v2, "pos":I
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->savedStates:Lorg/apache/lucene/util/IntsRef;

    iget-object v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/IntsRef;->grow(I)V

    .line 182
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->savedStates:Lorg/apache/lucene/util/IntsRef;

    iget-object v4, v7, Lorg/apache/lucene/util/IntsRef;->ints:[I

    .line 183
    .local v4, "states":[I
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v7}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->getInitialState()I

    move-result v7

    aput v7, v4, v6

    .line 186
    :cond_0
    :goto_0
    iget-wide v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    .line 187
    iput-boolean v6, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    .line 189
    aget v3, v4, v2

    .local v3, "state":I
    :goto_1
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    if-lt v2, v7, :cond_3

    .line 204
    :cond_1
    invoke-direct {p0, v3, v2}, Lorg/apache/lucene/index/AutomatonTermsEnum;->nextString(II)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 212
    :cond_2
    :goto_2
    return v5

    .line 190
    :cond_3
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    iget-wide v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    aput-wide v8, v7, v3

    .line 191
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iget-object v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, v8, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v8, v8, v2

    and-int/lit16 v8, v8, 0xff

    invoke-virtual {v7, v3, v8}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->step(II)I

    move-result v1

    .line 192
    .local v1, "nextState":I
    const/4 v7, -0x1

    if-eq v1, v7, :cond_1

    .line 194
    add-int/lit8 v7, v2, 0x1

    aput v1, v4, v7

    .line 196
    iget-boolean v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->finite:Z

    if-nez v7, :cond_4

    iget-boolean v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    if-nez v7, :cond_4

    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    aget-wide v8, v7, v1

    iget-wide v10, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    cmp-long v7, v8, v10

    if-nez v7, :cond_4

    .line 197
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/AutomatonTermsEnum;->setLinear(I)V

    .line 199
    :cond_4
    move v3, v1

    .line 189
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 207
    .end local v1    # "nextState":I
    :cond_5
    invoke-direct {p0, v2}, Lorg/apache/lucene/index/AutomatonTermsEnum;->backtrack(I)I

    move-result v2

    if-gez v2, :cond_6

    move v5, v6

    .line 208
    goto :goto_2

    .line 209
    :cond_6
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    aget v8, v4, v2

    iget-object v9, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v9, v9, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v9, v9, v2

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {v7, v8, v9}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->step(II)I

    move-result v0

    .line 210
    .local v0, "newState":I
    if-ltz v0, :cond_7

    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v7, v0}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v7

    if-nez v7, :cond_2

    .line 216
    :cond_7
    iget-boolean v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->finite:Z

    if-nez v7, :cond_0

    const/4 v2, 0x0

    .line 185
    goto :goto_0
.end method

.method private nextString(II)Z
    .locals 12
    .param p1, "state"    # I
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x0

    .line 244
    const/4 v0, 0x0

    .line 245
    .local v0, "c":I
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    if-ge p2, v7, :cond_2

    .line 246
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v7, v7, p2

    and-int/lit16 v0, v7, 0xff

    .line 250
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "c":I
    .local v1, "c":I
    const/16 v7, 0xff

    if-ne v0, v7, :cond_1

    move v0, v1

    .line 297
    .end local v1    # "c":I
    .restart local v0    # "c":I
    :cond_0
    :goto_0
    return v6

    .end local v0    # "c":I
    .restart local v1    # "c":I
    :cond_1
    move v0, v1

    .line 254
    .end local v1    # "c":I
    .restart local v0    # "c":I
    :cond_2
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iput p2, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 255
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    iget-wide v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    aput-wide v8, v7, p1

    .line 257
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->allTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v5, v7, p1

    .line 261
    .local v5, "transitions":[Lorg/apache/lucene/util/automaton/Transition;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v7, v5

    if-ge v2, v7, :cond_0

    .line 262
    aget-object v4, v5, v2

    .line 263
    .local v4, "transition":Lorg/apache/lucene/util/automaton/Transition;
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getMax()I

    move-result v7

    if-lt v7, v0, :cond_6

    .line 264
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getMin()I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 266
    .local v3, "nextChar":I
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 267
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 268
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, -0x1

    int-to-byte v9, v3

    aput-byte v9, v7, v8

    .line 269
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getDest()Lorg/apache/lucene/util/automaton/State;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/util/automaton/State;->getNumber()I

    move-result p1

    .line 274
    :cond_3
    :goto_2
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    aget-wide v8, v7, p1

    iget-wide v10, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_4

    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v7, p1}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 294
    :cond_4
    const/4 v6, 0x1

    goto :goto_0

    .line 275
    :cond_5
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    iget-wide v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    aput-wide v8, v7, p1

    .line 281
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->allTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v7, v7, p1

    aget-object v4, v7, v6

    .line 282
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getDest()Lorg/apache/lucene/util/automaton/State;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/lucene/util/automaton/State;->getNumber()I

    move-result p1

    .line 285
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Lorg/apache/lucene/util/BytesRef;->grow(I)V

    .line 286
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 287
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v7, v7, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v8, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v8, v8, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getMin()I

    move-result v9

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    .line 290
    iget-boolean v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->finite:Z

    if-nez v7, :cond_3

    iget-boolean v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    if-nez v7, :cond_3

    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->visited:[J

    aget-wide v8, v7, p1

    iget-wide v10, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->curGen:J

    cmp-long v7, v8, v10

    if-nez v7, :cond_3

    .line 291
    iget-object v7, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v7, v7, Lorg/apache/lucene/util/BytesRef;->length:I

    add-int/lit8 v7, v7, -0x1

    invoke-direct {p0, v7}, Lorg/apache/lucene/index/AutomatonTermsEnum;->setLinear(I)V

    goto :goto_2

    .line 261
    .end local v3    # "nextChar":I
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method

.method private setLinear(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v7, 0x0

    .line 137
    sget-boolean v5, Lorg/apache/lucene/index/AutomatonTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 139
    :cond_0
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v5}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->getInitialState()I

    move-result v3

    .line 140
    .local v3, "state":I
    const/16 v2, 0xff

    .line 141
    .local v2, "maxInterval":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_3

    .line 145
    const/4 v0, 0x0

    :goto_1
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->allTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v5, v5, v3

    array-length v5, v5

    if-lt v0, v5, :cond_5

    .line 154
    :goto_2
    const/16 v5, 0xff

    if-eq v2, v5, :cond_1

    .line 155
    add-int/lit8 v2, v2, 0x1

    .line 156
    :cond_1
    add-int/lit8 v1, p1, 0x1

    .line 157
    .local v1, "length":I
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    array-length v5, v5

    if-ge v5, v1, :cond_2

    .line 158
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    new-array v6, v1, [B

    iput-object v6, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    .line 159
    :cond_2
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v6, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    invoke-static {v5, v7, v6, v7, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    int-to-byte v6, v2

    aput-byte v6, v5, p1

    .line 161
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    iput v1, v5, Lorg/apache/lucene/util/BytesRef;->length:I

    .line 163
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    .line 164
    return-void

    .line 142
    .end local v1    # "length":I
    :cond_3
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iget-object v6, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v6, v6, v0

    and-int/lit16 v6, v6, 0xff

    invoke-virtual {v5, v3, v6}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->step(II)I

    move-result v3

    .line 143
    sget-boolean v5, Lorg/apache/lucene/index/AutomatonTermsEnum;->$assertionsDisabled:Z

    if-nez v5, :cond_4

    if-gez v3, :cond_4

    new-instance v5, Ljava/lang/AssertionError;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "state="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 141
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_5
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->allTransitions:[[Lorg/apache/lucene/util/automaton/Transition;

    aget-object v5, v5, v3

    aget-object v4, v5, v0

    .line 147
    .local v4, "t":Lorg/apache/lucene/util/automaton/Transition;
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getMin()I

    move-result v5

    iget-object v6, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v6, v6, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v6, v6, p1

    and-int/lit16 v6, v6, 0xff

    if-gt v5, v6, :cond_6

    .line 148
    iget-object v5, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget-object v5, v5, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    aget-byte v5, v5, p1

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getMax()I

    move-result v6

    if-gt v5, v6, :cond_6

    .line 149
    invoke-virtual {v4}, Lorg/apache/lucene/util/automaton/Transition;->getMax()I

    move-result v2

    .line 150
    goto/16 :goto_2

    .line 145
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method protected accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .locals 4
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 98
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->commonSuffixRef:Lorg/apache/lucene/util/BytesRef;

    invoke-static {p1, v0}, Lorg/apache/lucene/util/StringHelper;->endsWith(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iget-object v1, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v2, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->run([BII)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    iget-boolean v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    .line 105
    :goto_0
    return-object v0

    .line 100
    :cond_1
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    .line 102
    :cond_2
    iget-boolean v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_3

    .line 103
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    :cond_3
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    .line 105
    :cond_4
    iget-boolean v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linear:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->termComp:Ljava/util/Comparator;

    iget-object v1, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->linearUpperBound:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v0, p1, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_5

    .line 106
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0

    :cond_5
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    goto :goto_0
.end method

.method protected nextSeekTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    if-nez p1, :cond_1

    .line 114
    sget-boolean v0, Lorg/apache/lucene/index/AutomatonTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    iget v0, v0, Lorg/apache/lucene/util/BytesRef;->length:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 116
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    iget-object v1, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->runAutomaton:Lorg/apache/lucene/util/automaton/ByteRunAutomaton;

    invoke-virtual {v1}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->getInitialState()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/util/automaton/ByteRunAutomaton;->isAccept(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    .line 127
    :goto_0
    return-object v0

    .line 120
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 124
    :cond_2
    invoke-direct {p0}, Lorg/apache/lucene/index/AutomatonTermsEnum;->nextString()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lorg/apache/lucene/index/AutomatonTermsEnum;->seekBytesRef:Lorg/apache/lucene/util/BytesRef;

    goto :goto_0

    .line 127
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
