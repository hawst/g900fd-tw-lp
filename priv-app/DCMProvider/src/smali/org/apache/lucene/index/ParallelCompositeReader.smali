.class public final Lorg/apache/lucene/index/ParallelCompositeReader;
.super Lorg/apache/lucene/index/BaseCompositeReader;
.source "ParallelCompositeReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/BaseCompositeReader",
        "<",
        "Lorg/apache/lucene/index/IndexReader;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final closeSubReaders:Z

.field private final completeReaderSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/apache/lucene/index/CompositeReader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lorg/apache/lucene/index/ParallelCompositeReader;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ParallelCompositeReader;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public varargs constructor <init>(Z[Lorg/apache/lucene/index/CompositeReader;)V
    .locals 0
    .param p1, "closeSubReaders"    # Z
    .param p2, "readers"    # [Lorg/apache/lucene/index/CompositeReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p2}, Lorg/apache/lucene/index/ParallelCompositeReader;-><init>(Z[Lorg/apache/lucene/index/CompositeReader;[Lorg/apache/lucene/index/CompositeReader;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Z[Lorg/apache/lucene/index/CompositeReader;[Lorg/apache/lucene/index/CompositeReader;)V
    .locals 3
    .param p1, "closeSubReaders"    # Z
    .param p2, "readers"    # [Lorg/apache/lucene/index/CompositeReader;
    .param p3, "storedFieldReaders"    # [Lorg/apache/lucene/index/CompositeReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-static {p2, p3}, Lorg/apache/lucene/index/ParallelCompositeReader;->prepareSubReaders([Lorg/apache/lucene/index/CompositeReader;[Lorg/apache/lucene/index/CompositeReader;)[Lorg/apache/lucene/index/IndexReader;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/lucene/index/BaseCompositeReader;-><init>([Lorg/apache/lucene/index/IndexReader;)V

    .line 53
    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->completeReaderSet:Ljava/util/Set;

    .line 72
    iput-boolean p1, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->closeSubReaders:Z

    .line 73
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->completeReaderSet:Ljava/util/Set;

    invoke-static {v1, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->completeReaderSet:Ljava/util/Set;

    invoke-static {v1, p3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 76
    if-nez p1, :cond_0

    .line 77
    iget-object v1, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 81
    :cond_0
    return-void

    .line 77
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/CompositeReader;

    .line 78
    .local v0, "reader":Lorg/apache/lucene/index/CompositeReader;
    invoke-virtual {v0}, Lorg/apache/lucene/index/CompositeReader;->incRef()V

    goto :goto_0
.end method

.method public varargs constructor <init>([Lorg/apache/lucene/index/CompositeReader;)V
    .locals 1
    .param p1, "readers"    # [Lorg/apache/lucene/index/CompositeReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/lucene/index/ParallelCompositeReader;-><init>(Z[Lorg/apache/lucene/index/CompositeReader;)V

    .line 59
    return-void
.end method

.method private static prepareSubReaders([Lorg/apache/lucene/index/CompositeReader;[Lorg/apache/lucene/index/CompositeReader;)[Lorg/apache/lucene/index/IndexReader;
    .locals 14
    .param p0, "readers"    # [Lorg/apache/lucene/index/CompositeReader;
    .param p1, "storedFieldsReaders"    # [Lorg/apache/lucene/index/CompositeReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    array-length v12, p0

    if-nez v12, :cond_2

    .line 85
    array-length v12, p1

    if-lez v12, :cond_0

    .line 86
    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string v13, "There must be at least one main reader if storedFieldsReaders are used."

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 87
    :cond_0
    const/4 v12, 0x0

    new-array v11, v12, [Lorg/apache/lucene/index/IndexReader;

    .line 134
    :cond_1
    return-object v11

    .line 89
    :cond_2
    const/4 v12, 0x0

    aget-object v12, p0, v12

    invoke-virtual {v12}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v4

    .line 92
    .local v4, "firstSubReaders":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexReader;>;"
    const/4 v12, 0x0

    aget-object v12, p0, v12

    invoke-virtual {v12}, Lorg/apache/lucene/index/CompositeReader;->maxDoc()I

    move-result v7

    .local v7, "maxDoc":I
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    .line 93
    .local v8, "noSubs":I
    new-array v2, v8, [I

    .line 94
    .local v2, "childMaxDoc":[I
    new-array v1, v8, [Z

    .line 95
    .local v1, "childAtomic":[Z
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-lt v5, v8, :cond_3

    .line 100
    invoke-static {p0, v7, v2, v1}, Lorg/apache/lucene/index/ParallelCompositeReader;->validate([Lorg/apache/lucene/index/CompositeReader;I[I[Z)V

    .line 101
    invoke-static {p1, v7, v2, v1}, Lorg/apache/lucene/index/ParallelCompositeReader;->validate([Lorg/apache/lucene/index/CompositeReader;I[I[Z)V

    .line 104
    new-array v11, v8, [Lorg/apache/lucene/index/IndexReader;

    .line 105
    .local v11, "subReaders":[Lorg/apache/lucene/index/IndexReader;
    const/4 v5, 0x0

    :goto_1
    array-length v12, v11

    if-ge v5, v12, :cond_1

    .line 106
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Lorg/apache/lucene/index/AtomicReader;

    if-eqz v12, :cond_6

    .line 107
    array-length v12, p0

    new-array v0, v12, [Lorg/apache/lucene/index/AtomicReader;

    .line 108
    .local v0, "atomicSubs":[Lorg/apache/lucene/index/AtomicReader;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_2
    array-length v12, p0

    if-lt v6, v12, :cond_4

    .line 111
    array-length v12, p1

    new-array v10, v12, [Lorg/apache/lucene/index/AtomicReader;

    .line 112
    .local v10, "storedSubs":[Lorg/apache/lucene/index/AtomicReader;
    const/4 v6, 0x0

    :goto_3
    array-length v12, p1

    if-lt v6, v12, :cond_5

    .line 118
    new-instance v12, Lorg/apache/lucene/index/ParallelAtomicReader;

    const/4 v13, 0x1

    invoke-direct {v12, v13, v0, v10}, Lorg/apache/lucene/index/ParallelAtomicReader;-><init>(Z[Lorg/apache/lucene/index/AtomicReader;[Lorg/apache/lucene/index/AtomicReader;)V

    aput-object v12, v11, v5

    .line 105
    .end local v0    # "atomicSubs":[Lorg/apache/lucene/index/AtomicReader;
    .end local v10    # "storedSubs":[Lorg/apache/lucene/index/AtomicReader;
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 96
    .end local v6    # "j":I
    .end local v11    # "subReaders":[Lorg/apache/lucene/index/IndexReader;
    :cond_3
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/lucene/index/IndexReader;

    .line 97
    .local v9, "r":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v9}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v12

    aput v12, v2, v5

    .line 98
    instance-of v12, v9, Lorg/apache/lucene/index/AtomicReader;

    aput-boolean v12, v1, v5

    .line 95
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 109
    .end local v9    # "r":Lorg/apache/lucene/index/IndexReader;
    .restart local v0    # "atomicSubs":[Lorg/apache/lucene/index/AtomicReader;
    .restart local v6    # "j":I
    .restart local v11    # "subReaders":[Lorg/apache/lucene/index/IndexReader;
    :cond_4
    aget-object v12, p0, v6

    invoke-virtual {v12}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/AtomicReader;

    aput-object v12, v0, v6

    .line 108
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 113
    .restart local v10    # "storedSubs":[Lorg/apache/lucene/index/AtomicReader;
    :cond_5
    aget-object v12, p1, v6

    invoke-virtual {v12}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/AtomicReader;

    aput-object v12, v10, v6

    .line 112
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 120
    .end local v0    # "atomicSubs":[Lorg/apache/lucene/index/AtomicReader;
    .end local v6    # "j":I
    .end local v10    # "storedSubs":[Lorg/apache/lucene/index/AtomicReader;
    :cond_6
    sget-boolean v12, Lorg/apache/lucene/index/ParallelCompositeReader;->$assertionsDisabled:Z

    if-nez v12, :cond_7

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    instance-of v12, v12, Lorg/apache/lucene/index/CompositeReader;

    if-nez v12, :cond_7

    new-instance v12, Ljava/lang/AssertionError;

    invoke-direct {v12}, Ljava/lang/AssertionError;-><init>()V

    throw v12

    .line 121
    :cond_7
    array-length v12, p0

    new-array v3, v12, [Lorg/apache/lucene/index/CompositeReader;

    .line 122
    .local v3, "compositeSubs":[Lorg/apache/lucene/index/CompositeReader;
    const/4 v6, 0x0

    .restart local v6    # "j":I
    :goto_5
    array-length v12, p0

    if-lt v6, v12, :cond_8

    .line 125
    array-length v12, p1

    new-array v10, v12, [Lorg/apache/lucene/index/CompositeReader;

    .line 126
    .local v10, "storedSubs":[Lorg/apache/lucene/index/CompositeReader;
    const/4 v6, 0x0

    :goto_6
    array-length v12, p1

    if-lt v6, v12, :cond_9

    .line 131
    new-instance v12, Lorg/apache/lucene/index/ParallelCompositeReader;

    const/4 v13, 0x1

    invoke-direct {v12, v13, v3, v10}, Lorg/apache/lucene/index/ParallelCompositeReader;-><init>(Z[Lorg/apache/lucene/index/CompositeReader;[Lorg/apache/lucene/index/CompositeReader;)V

    aput-object v12, v11, v5

    goto :goto_4

    .line 123
    .end local v10    # "storedSubs":[Lorg/apache/lucene/index/CompositeReader;
    :cond_8
    aget-object v12, p0, v6

    invoke-virtual {v12}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/CompositeReader;

    aput-object v12, v3, v6

    .line 122
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 127
    .restart local v10    # "storedSubs":[Lorg/apache/lucene/index/CompositeReader;
    :cond_9
    aget-object v12, p1, v6

    invoke-virtual {v12}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/lucene/index/CompositeReader;

    aput-object v12, v10, v6

    .line 126
    add-int/lit8 v6, v6, 0x1

    goto :goto_6
.end method

.method private static validate([Lorg/apache/lucene/index/CompositeReader;I[I[Z)V
    .locals 9
    .param p0, "readers"    # [Lorg/apache/lucene/index/CompositeReader;
    .param p1, "maxDoc"    # I
    .param p2, "childMaxDoc"    # [I
    .param p3, "childAtomic"    # [Z

    .prologue
    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, p0

    if-lt v0, v6, :cond_0

    .line 159
    return-void

    .line 140
    :cond_0
    aget-object v3, p0, v0

    .line 141
    .local v3, "reader":Lorg/apache/lucene/index/CompositeReader;
    invoke-virtual {v3}, Lorg/apache/lucene/index/CompositeReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v5

    .line 142
    .local v5, "subs":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexReader;>;"
    invoke-virtual {v3}, Lorg/apache/lucene/index/CompositeReader;->maxDoc()I

    move-result v6

    if-eq v6, p1, :cond_1

    .line 143
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "All readers must have same maxDoc: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lorg/apache/lucene/index/CompositeReader;->maxDoc()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 145
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    .line 146
    .local v1, "noSubs":I
    array-length v6, p2

    if-eq v1, v6, :cond_2

    .line 147
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "All readers must have same number of subReaders"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 149
    :cond_2
    const/4 v4, 0x0

    .local v4, "subIDX":I
    :goto_1
    if-lt v4, v1, :cond_3

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_3
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexReader;

    .line 151
    .local v2, "r":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v6

    aget v7, p2, v4

    if-eq v6, v7, :cond_4

    .line 152
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "All readers must have same corresponding subReader maxDoc"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 154
    :cond_4
    aget-boolean v6, p3, v4

    if-eqz v6, :cond_6

    instance-of v6, v2, Lorg/apache/lucene/index/AtomicReader;

    if-nez v6, :cond_7

    .line 155
    :cond_5
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "All readers must have same corresponding subReader types (atomic or composite)"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 154
    :cond_6
    instance-of v6, v2, Lorg/apache/lucene/index/CompositeReader;

    if-eqz v6, :cond_5

    .line 149
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected declared-synchronized doClose()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    monitor-enter p0

    const/4 v1, 0x0

    .line 174
    .local v1, "ioe":Ljava/io/IOException;
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 186
    if-eqz v1, :cond_3

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 174
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/CompositeReader;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    .local v2, "reader":Lorg/apache/lucene/index/CompositeReader;
    :try_start_2
    iget-boolean v4, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->closeSubReaders:Z

    if-eqz v4, :cond_2

    .line 177
    invoke-virtual {v2}, Lorg/apache/lucene/index/CompositeReader;->close()V

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/io/IOException;
    if-nez v1, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 179
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {v2}, Lorg/apache/lucene/index/CompositeReader;->decRef()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 187
    .end local v2    # "reader":Lorg/apache/lucene/index/CompositeReader;
    :cond_3
    monitor-exit p0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ParallelCompositeReader("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "buffer":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/apache/lucene/index/ParallelCompositeReader;->completeReaderSet:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/apache/lucene/index/CompositeReader;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 168
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 165
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
