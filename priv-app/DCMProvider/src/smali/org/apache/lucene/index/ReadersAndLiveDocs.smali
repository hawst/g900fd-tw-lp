.class Lorg/apache/lucene/index/ReadersAndLiveDocs;
.super Ljava/lang/Object;
.source "ReadersAndLiveDocs.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public final info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

.field private liveDocs:Lorg/apache/lucene/util/Bits;

.field private mergeReader:Lorg/apache/lucene/index/SegmentReader;

.field private pendingDeleteCount:I

.field private reader:Lorg/apache/lucene/index/SegmentReader;

.field private final refCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private shared:Z

.field private final writer:Lorg/apache/lucene/index/IndexWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/IndexWriter;Lorg/apache/lucene/index/SegmentInfoPerCommit;)V
    .locals 2
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    const/4 v1, 0x1

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 72
    iput-object p2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 73
    iput-object p1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writer:Lorg/apache/lucene/index/IndexWriter;

    .line 74
    iput-boolean v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z

    .line 75
    return-void
.end method


# virtual methods
.method public decRef()V
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 84
    .local v0, "rc":I
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 85
    :cond_0
    return-void
.end method

.method public declared-synchronized delete(I)Z
    .locals 4
    .param p1, "docID"    # I

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 172
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 173
    :cond_1
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_3

    if-ltz p1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v1

    if-lt p1, v1, :cond_3

    :cond_2
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "out of bounds: docid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " liveDocsLength="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v3}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seg="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " docCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v3, v3, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 174
    :cond_3
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 175
    :cond_4
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v0

    .line 176
    .local v0, "didDelete":Z
    if-eqz v0, :cond_5

    .line 177
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    check-cast v1, Lorg/apache/lucene/util/MutableBits;

    invoke-interface {v1, p1}, Lorg/apache/lucene/util/MutableBits;->clear(I)V

    .line 178
    iget v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    :cond_5
    monitor-exit p0

    return v0
.end method

.method public declared-synchronized dropChanges()V
    .locals 1

    .prologue
    .line 276
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    monitor-exit p0

    return-void

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized dropReaders()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    .line 192
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 198
    :cond_0
    :try_start_3
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v0, :cond_1

    .line 201
    :try_start_4
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 203
    const/4 v0, 0x0

    :try_start_5
    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    .line 208
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->decRef()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 209
    monitor-exit p0

    return-void

    .line 193
    :catchall_0
    move-exception v0

    .line 194
    const/4 v1, 0x0

    :try_start_6
    iput-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    .line 195
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 197
    :catchall_1
    move-exception v0

    .line 198
    :try_start_7
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v1, :cond_2

    .line 201
    :try_start_8
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 203
    const/4 v1, 0x0

    :try_start_9
    iput-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    .line 206
    :cond_2
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 189
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 202
    :catchall_3
    move-exception v0

    .line 203
    const/4 v1, 0x0

    :try_start_a
    iput-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    .line 204
    throw v0

    .line 202
    :catchall_4
    move-exception v0

    .line 203
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    .line 204
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2
.end method

.method public declared-synchronized getLiveDocs()Lorg/apache/lucene/util/Bits;
    .locals 1

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 255
    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getMergeReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->incRef()V

    .line 148
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    .line 161
    :cond_0
    :goto_0
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->incRef()V

    .line 162
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 153
    :cond_1
    :try_start_1
    new-instance v0, Lorg/apache/lucene/index/SegmentReader;

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2, p1}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V

    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    .line 154
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->mergeReader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPendingDeleteCount()I
    .locals 1

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getReadOnlyClone(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;
    .locals 6
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    if-nez v0, :cond_0

    .line 218
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/ReadersAndLiveDocs;->getReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->decRef()V

    .line 219
    sget-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 221
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z

    .line 222
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v0, :cond_1

    .line 223
    new-instance v0, Lorg/apache/lucene/index/SegmentReader;

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentReader;->core:Lorg/apache/lucene/index/SegmentCoreReaders;

    iget-object v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    iget-object v4, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    iget-object v5, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v5}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v5

    sub-int/2addr v4, v5

    iget v5, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    sub-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;Lorg/apache/lucene/index/SegmentCoreReaders;Lorg/apache/lucene/util/Bits;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    :goto_0
    monitor-exit p0

    return-object v0

    .line 225
    :cond_1
    :try_start_2
    sget-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 226
    :cond_2
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->incRef()V

    .line 227
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized getReadOnlyLiveDocs()Lorg/apache/lucene/util/Bits;
    .locals 1

    .prologue
    .line 260
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 261
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z

    .line 265
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getReader(Lorg/apache/lucene/store/IOContext;)Lorg/apache/lucene/index/SegmentReader;
    .locals 3
    .param p1, "context"    # Lorg/apache/lucene/store/IOContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    if-nez v0, :cond_0

    .line 121
    new-instance v0, Lorg/apache/lucene/index/SegmentReader;

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexWriter;->getConfig()Lorg/apache/lucene/index/LiveIndexWriterConfig;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/lucene/index/LiveIndexWriterConfig;->getReaderTermsIndexDivisor()I

    move-result v2

    invoke-direct {v0, v1, v2, p1}, Lorg/apache/lucene/index/SegmentReader;-><init>(Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V

    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    .line 122
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->getLiveDocs()Lorg/apache/lucene/util/Bits;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 130
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentReader;->incRef()V

    .line 131
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->reader:Lorg/apache/lucene/index/SegmentReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public incRef()V
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 79
    .local v0, "rc":I
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 80
    :cond_0
    return-void
.end method

.method public declared-synchronized initWritableLiveDocs()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->writer:Lorg/apache/lucene/index/IndexWriter;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 233
    :cond_0
    :try_start_1
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    if-gtz v1, :cond_1

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 235
    :cond_1
    iget-boolean v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z

    if-eqz v1, :cond_4

    .line 240
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v0

    .line 241
    .local v0, "liveDocsFormat":Lorg/apache/lucene/codecs/LiveDocsFormat;
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-nez v1, :cond_3

    .line 243
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/LiveDocsFormat;->newLiveDocs(I)Lorg/apache/lucene/util/MutableBits;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    .line 247
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    .end local v0    # "liveDocsFormat":Lorg/apache/lucene/codecs/LiveDocsFormat;
    :cond_2
    monitor-exit p0

    return-void

    .line 245
    .restart local v0    # "liveDocsFormat":Lorg/apache/lucene/codecs/LiveDocsFormat;
    :cond_3
    :try_start_2
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-virtual {v0, v1}, Lorg/apache/lucene/codecs/LiveDocsFormat;->newLiveDocs(Lorg/apache/lucene/util/Bits;)Lorg/apache/lucene/util/MutableBits;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    goto :goto_0

    .line 249
    .end local v0    # "liveDocsFormat":Lorg/apache/lucene/codecs/LiveDocsFormat;
    :cond_4
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public refCount()I
    .locals 2

    .prologue
    .line 88
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->refCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 89
    .local v0, "rc":I
    sget-boolean v1, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 90
    :cond_0
    return v0
.end method

.method public declared-synchronized release(Lorg/apache/lucene/index/SegmentReader;)V
    .locals 2
    .param p1, "sr"    # Lorg/apache/lucene/index/SegmentReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->getSegmentInfo()Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 167
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentReader;->decRef()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    monitor-exit p0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReadersAndLiveDocs(seg="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pendingDeleteCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " shared="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->shared:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized verifyDocCounts()Z
    .locals 5

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    if-eqz v2, :cond_2

    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "docID":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 111
    .end local v1    # "docID":I
    :goto_1
    sget-boolean v2, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v2

    iget-object v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    sub-int/2addr v2, v3

    if-eq v2, v0, :cond_3

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "info.docCount="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " info.getDelCount()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pendingDeleteCount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " count="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    .end local v0    # "count":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 103
    .restart local v0    # "count":I
    .restart local v1    # "docID":I
    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v2, v1}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104
    add-int/lit8 v0, v0, 0x1

    .line 102
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    .end local v0    # "count":I
    .end local v1    # "docID":I
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v2, v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v2}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .restart local v0    # "count":I
    goto :goto_1

    .line 112
    :cond_3
    const/4 v2, 0x1

    monitor-exit p0

    return v2
.end method

.method public declared-synchronized writeLiveDocs(Lorg/apache/lucene/store/Directory;)Z
    .locals 8
    .param p1, "dir"    # Lorg/apache/lucene/store/Directory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 284
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    if-eqz v1, :cond_2

    .line 286
    sget-boolean v0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    invoke-interface {v0}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, v1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 291
    :cond_0
    :try_start_1
    new-instance v2, Lorg/apache/lucene/store/TrackingDirectoryWrapper;

    invoke-direct {v2, p1}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;-><init>(Lorg/apache/lucene/store/Directory;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 296
    .local v2, "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    const/4 v7, 0x0

    .line 298
    .local v7, "success":Z
    :try_start_2
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v0, v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfo;->getCodec()Lorg/apache/lucene/codecs/Codec;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/lucene/codecs/Codec;->liveDocsFormat()Lorg/apache/lucene/codecs/LiveDocsFormat;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->liveDocs:Lorg/apache/lucene/util/Bits;

    check-cast v1, Lorg/apache/lucene/util/MutableBits;

    iget-object v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget v4, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    sget-object v5, Lorg/apache/lucene/store/IOContext;->DEFAULT:Lorg/apache/lucene/store/IOContext;

    invoke-virtual/range {v0 .. v5}, Lorg/apache/lucene/codecs/LiveDocsFormat;->writeLiveDocs(Lorg/apache/lucene/util/MutableBits;Lorg/apache/lucene/store/Directory;Lorg/apache/lucene/index/SegmentInfoPerCommit;ILorg/apache/lucene/store/IOContext;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 299
    const/4 v7, 0x1

    .line 301
    if-nez v7, :cond_1

    .line 304
    :try_start_3
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->advanceNextWriteDelGen()V

    .line 307
    invoke-virtual {v2}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    .line 320
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v0}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->advanceDelGen()V

    .line 321
    iget-object v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->getDelCount()I

    move-result v1

    iget v3, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I

    add-int/2addr v1, v3

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->setDelCount(I)V

    .line 323
    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->pendingDeleteCount:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 324
    const/4 v0, 0x1

    .line 326
    .end local v2    # "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    .end local v7    # "success":Z
    :cond_2
    monitor-exit p0

    return v0

    .line 300
    .restart local v2    # "trackingDir":Lorg/apache/lucene/store/TrackingDirectoryWrapper;
    .restart local v7    # "success":Z
    :catchall_1
    move-exception v0

    .line 301
    if-nez v7, :cond_3

    .line 304
    :try_start_4
    iget-object v1, p0, Lorg/apache/lucene/index/ReadersAndLiveDocs;->info:Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->advanceNextWriteDelGen()V

    .line 307
    invoke-virtual {v2}, Lorg/apache/lucene/store/TrackingDirectoryWrapper;->getCreatedFiles()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 315
    :cond_3
    throw v0

    .line 307
    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 309
    .local v6, "fileName":Ljava/lang/String;
    :try_start_5
    invoke-virtual {p1, v6}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 310
    :catch_0
    move-exception v3

    goto :goto_1

    .line 307
    .end local v6    # "fileName":Ljava/lang/String;
    :cond_5
    :try_start_6
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 309
    .restart local v6    # "fileName":Ljava/lang/String;
    :try_start_7
    invoke-virtual {p1, v6}, Lorg/apache/lucene/store/Directory;->deleteFile(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 310
    :catch_1
    move-exception v1

    goto :goto_0
.end method
