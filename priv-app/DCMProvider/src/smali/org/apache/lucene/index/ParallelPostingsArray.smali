.class Lorg/apache/lucene/index/ParallelPostingsArray;
.super Ljava/lang/Object;
.source "ParallelPostingsArray.java"


# static fields
.field static final BYTES_PER_POSTING:I = 0xc


# instance fields
.field final byteStarts:[I

.field final intStarts:[I

.field final size:I

.field final textStarts:[I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput p1, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    .line 33
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    .line 34
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    .line 35
    new-array v0, p1, [I

    iput-object v0, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    .line 36
    return-void
.end method


# virtual methods
.method bytesPerPosting()I
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0xc

    return v0
.end method

.method copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V
    .locals 3
    .param p1, "toArray"    # Lorg/apache/lucene/index/ParallelPostingsArray;
    .param p2, "numToCopy"    # I

    .prologue
    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    iget-object v1, p1, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    invoke-static {v0, v2, v1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    iget-object v1, p1, Lorg/apache/lucene/index/ParallelPostingsArray;->intStarts:[I

    invoke-static {v0, v2, v1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    iget-object v0, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    iget-object v1, p1, Lorg/apache/lucene/index/ParallelPostingsArray;->byteStarts:[I

    invoke-static {v0, v2, v1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 57
    return-void
.end method

.method final grow()Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 4

    .prologue
    .line 47
    iget v2, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    .line 48
    .local v1, "newSize":I
    invoke-virtual {p0, v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->newInstance(I)Lorg/apache/lucene/index/ParallelPostingsArray;

    move-result-object v0

    .line 49
    .local v0, "newArray":Lorg/apache/lucene/index/ParallelPostingsArray;
    iget v2, p0, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    invoke-virtual {p0, v0, v2}, Lorg/apache/lucene/index/ParallelPostingsArray;->copyTo(Lorg/apache/lucene/index/ParallelPostingsArray;I)V

    .line 50
    return-object v0
.end method

.method newInstance(I)Lorg/apache/lucene/index/ParallelPostingsArray;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 43
    new-instance v0, Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-direct {v0, p1}, Lorg/apache/lucene/index/ParallelPostingsArray;-><init>(I)V

    return-object v0
.end method
