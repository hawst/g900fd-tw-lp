.class public abstract Lorg/apache/lucene/index/BinaryDocValues;
.super Ljava/lang/Object;
.source "BinaryDocValues.java"


# static fields
.field public static final EMPTY:Lorg/apache/lucene/index/BinaryDocValues;

.field public static final MISSING:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/apache/lucene/index/BinaryDocValues;->MISSING:[B

    .line 40
    new-instance v0, Lorg/apache/lucene/index/BinaryDocValues$1;

    invoke-direct {v0}, Lorg/apache/lucene/index/BinaryDocValues$1;-><init>()V

    sput-object v0, Lorg/apache/lucene/index/BinaryDocValues;->EMPTY:Lorg/apache/lucene/index/BinaryDocValues;

    .line 47
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract get(ILorg/apache/lucene/util/BytesRef;)V
.end method
