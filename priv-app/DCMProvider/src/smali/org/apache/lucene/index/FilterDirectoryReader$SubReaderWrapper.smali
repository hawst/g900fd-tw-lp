.class public abstract Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;
.super Ljava/lang/Object;
.source "FilterDirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterDirectoryReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SubReaderWrapper"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;Ljava/util/List;)[Lorg/apache/lucene/index/AtomicReader;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;->wrap(Ljava/util/List;)[Lorg/apache/lucene/index/AtomicReader;

    move-result-object v0

    return-object v0
.end method

.method private wrap(Ljava/util/List;)[Lorg/apache/lucene/index/AtomicReader;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/AtomicReader;",
            ">;)[",
            "Lorg/apache/lucene/index/AtomicReader;"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "readers":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/AtomicReader;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v1, v2, [Lorg/apache/lucene/index/AtomicReader;

    .line 46
    .local v1, "wrapped":[Lorg/apache/lucene/index/AtomicReader;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 49
    return-object v1

    .line 47
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/AtomicReader;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;->wrap(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/AtomicReader;

    move-result-object v2

    aput-object v2, v1, v0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public abstract wrap(Lorg/apache/lucene/index/AtomicReader;)Lorg/apache/lucene/index/AtomicReader;
.end method
