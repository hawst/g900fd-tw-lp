.class Lorg/apache/lucene/index/FrozenBufferedDeletes;
.super Ljava/lang/Object;
.source "FrozenBufferedDeletes.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final BYTES_PER_DEL_QUERY:I


# instance fields
.field final bytesUsed:I

.field private gen:J

.field final isSegmentPrivate:Z

.field final numTermDeletes:I

.field final queries:[Lorg/apache/lucene/search/Query;

.field final queryLimits:[I

.field termCount:I

.field final terms:Lorg/apache/lucene/index/PrefixCodedTerms;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->$assertionsDisabled:Z

    .line 36
    sget v0, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x18

    sput v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->BYTES_PER_DEL_QUERY:I

    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/BufferedDeletes;Z)V
    .locals 8
    .param p1, "deletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "isSegmentPrivate"    # Z

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    .line 55
    iput-boolean p2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->isSegmentPrivate:Z

    .line 56
    sget-boolean v5, Lorg/apache/lucene/index/FrozenBufferedDeletes;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    if-eqz p2, :cond_0

    iget-object v5, p1, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    const-string v6, "segment private package should only have del queries"

    invoke-direct {v5, v6}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5

    .line 57
    :cond_0
    iget-object v5, p1, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    iget-object v6, p1, Lorg/apache/lucene/index/BufferedDeletes;->terms:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    new-array v6, v6, [Lorg/apache/lucene/index/Term;

    invoke-interface {v5, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/lucene/index/Term;

    .line 58
    .local v3, "termsArray":[Lorg/apache/lucene/index/Term;
    array-length v5, v3

    iput v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termCount:I

    .line 59
    invoke-static {v3}, Lorg/apache/lucene/util/ArrayUtil;->mergeSort([Ljava/lang/Comparable;)V

    .line 60
    new-instance v0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;

    invoke-direct {v0}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;-><init>()V

    .line 61
    .local v0, "builder":Lorg/apache/lucene/index/PrefixCodedTerms$Builder;
    array-length v6, v3

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_1

    .line 64
    invoke-virtual {v0}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->finish()Lorg/apache/lucene/index/PrefixCodedTerms;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->terms:Lorg/apache/lucene/index/PrefixCodedTerms;

    .line 66
    iget-object v5, p1, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    new-array v5, v5, [Lorg/apache/lucene/search/Query;

    iput-object v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    .line 67
    iget-object v5, p1, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    new-array v5, v5, [I

    iput-object v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queryLimits:[I

    .line 68
    const/4 v4, 0x0

    .line 69
    .local v4, "upto":I
    iget-object v5, p1, Lorg/apache/lucene/index/BufferedDeletes;->queries:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 75
    iget-object v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->terms:Lorg/apache/lucene/index/PrefixCodedTerms;

    invoke-virtual {v5}, Lorg/apache/lucene/index/PrefixCodedTerms;->getSizeInBytes()J

    move-result-wide v6

    long-to-int v5, v6

    iget-object v6, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v6, v6

    sget v7, Lorg/apache/lucene/index/FrozenBufferedDeletes;->BYTES_PER_DEL_QUERY:I

    mul-int/2addr v6, v7

    add-int/2addr v5, v6

    iput v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    .line 76
    iget-object v5, p1, Lorg/apache/lucene/index/BufferedDeletes;->numTermDeletes:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    iput v5, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    .line 77
    return-void

    .line 61
    .end local v4    # "upto":I
    :cond_1
    aget-object v2, v3, v5

    .line 62
    .local v2, "term":Lorg/apache/lucene/index/Term;
    invoke-virtual {v0, v2}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->add(Lorg/apache/lucene/index/Term;)V

    .line 61
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 69
    .end local v2    # "term":Lorg/apache/lucene/index/Term;
    .restart local v4    # "upto":I
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 70
    .local v1, "ent":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/apache/lucene/search/Query;Ljava/lang/Integer;>;"
    iget-object v7, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/lucene/search/Query;

    aput-object v5, v7, v4

    .line 71
    iget-object v7, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queryLimits:[I

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v7, v4

    .line 72
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method any()Z
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termCount:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public delGen()J
    .locals 4

    .prologue
    .line 85
    sget-boolean v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 86
    :cond_0
    iget-wide v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    return-wide v0
.end method

.method public queriesIterable()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    return-object v0
.end method

.method public setDelGen(J)V
    .locals 5
    .param p1, "gen"    # J

    .prologue
    .line 80
    sget-boolean v0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 81
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->gen:J

    .line 82
    return-void
.end method

.method public termsIterable()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lorg/apache/lucene/index/Term;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lorg/apache/lucene/index/FrozenBufferedDeletes$1;

    invoke-direct {v0, p0}, Lorg/apache/lucene/index/FrozenBufferedDeletes$1;-><init>(Lorg/apache/lucene/index/FrozenBufferedDeletes;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    const-string v0, ""

    .line 129
    .local v0, "s":Ljava/lang/String;
    iget v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    if-eqz v1, :cond_0

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->numTermDeletes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deleted terms (unique count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->termCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v1, v1

    if-eqz v1, :cond_1

    .line 133
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deleted queries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 135
    :cond_1
    iget v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    if-eqz v1, :cond_2

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " bytesUsed="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes;->bytesUsed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 139
    :cond_2
    return-object v0
.end method
