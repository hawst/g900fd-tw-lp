.class Lorg/apache/lucene/index/NumericDocValuesWriter;
.super Lorg/apache/lucene/index/DocValuesWriter;
.source "NumericDocValuesWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/NumericDocValuesWriter$NumericIterator;
    }
.end annotation


# static fields
.field private static final MISSING:J


# instance fields
.field private bytesUsed:J

.field private final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field private final iwBytesUsed:Lorg/apache/lucene/util/Counter;

.field private pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/FieldInfo;Lorg/apache/lucene/util/Counter;)V
    .locals 2
    .param p1, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;
    .param p2, "iwBytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/lucene/index/DocValuesWriter;-><init>()V

    .line 40
    new-instance v0, Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-direct {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    .line 41
    iget-object v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v0}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->bytesUsed:J

    .line 42
    iput-object p1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 43
    iput-object p2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    .line 44
    iget-wide v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->bytesUsed:J

    invoke-virtual {p2, v0, v1}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 45
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/NumericDocValuesWriter;)Lorg/apache/lucene/util/packed/AppendingLongBuffer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    return-object v0
.end method

.method private updateBytesUsed()V
    .locals 6

    .prologue
    .line 63
    iget-object v2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v2}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->ramBytesUsed()J

    move-result-wide v0

    .line 64
    .local v0, "newBytesUsed":J
    iget-object v2, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->iwBytesUsed:Lorg/apache/lucene/util/Counter;

    iget-wide v4, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->bytesUsed:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 65
    iput-wide v0, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->bytesUsed:J

    .line 66
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public addValue(IJ)V
    .locals 6
    .param p1, "docID"    # I
    .param p2, "value"    # J

    .prologue
    .line 48
    int-to-long v2, p1

    iget-object v1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 49
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DocValuesField \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v3, v3, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" appears more than once in this document (only one value is allowed per field)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v1}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->size()J

    move-result-wide v2

    long-to-int v0, v2

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_1

    .line 57
    iget-object v1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    invoke-virtual {v1, p2, p3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 59
    invoke-direct {p0}, Lorg/apache/lucene/index/NumericDocValuesWriter;->updateBytesUsed()V

    .line 60
    return-void

    .line 54
    :cond_1
    iget-object v1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->pending:Lorg/apache/lucene/util/packed/AppendingLongBuffer;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/apache/lucene/util/packed/AppendingLongBuffer;->add(J)V

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public finish(I)V
    .locals 0
    .param p1, "maxDoc"    # I

    .prologue
    .line 70
    return-void
.end method

.method public flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
    .locals 3
    .param p1, "state"    # Lorg/apache/lucene/index/SegmentWriteState;
    .param p2, "dvConsumer"    # Lorg/apache/lucene/codecs/DocValuesConsumer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v1, p1, Lorg/apache/lucene/index/SegmentWriteState;->segmentInfo:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v1}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v0

    .line 77
    .local v0, "maxDoc":I
    iget-object v1, p0, Lorg/apache/lucene/index/NumericDocValuesWriter;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 78
    new-instance v2, Lorg/apache/lucene/index/NumericDocValuesWriter$1;

    invoke-direct {v2, p0, v0}, Lorg/apache/lucene/index/NumericDocValuesWriter$1;-><init>(Lorg/apache/lucene/index/NumericDocValuesWriter;I)V

    .line 77
    invoke-virtual {p2, v1, v2}, Lorg/apache/lucene/codecs/DocValuesConsumer;->addNumericField(Lorg/apache/lucene/index/FieldInfo;Ljava/lang/Iterable;)V

    .line 84
    return-void
.end method
