.class public abstract Lorg/apache/lucene/index/FilterDirectoryReader;
.super Lorg/apache/lucene/index/DirectoryReader;
.source "FilterDirectoryReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FilterDirectoryReader$StandardReaderWrapper;,
        Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;
    }
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/DirectoryReader;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DirectoryReader;)V
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/index/DirectoryReader;

    .prologue
    .line 87
    new-instance v0, Lorg/apache/lucene/index/FilterDirectoryReader$StandardReaderWrapper;

    invoke-direct {v0}, Lorg/apache/lucene/index/FilterDirectoryReader$StandardReaderWrapper;-><init>()V

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/FilterDirectoryReader;-><init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/DirectoryReader;Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;)V
    .locals 2
    .param p1, "in"    # Lorg/apache/lucene/index/DirectoryReader;
    .param p2, "wrapper"    # Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;

    .prologue
    .line 97
    invoke-virtual {p1}, Lorg/apache/lucene/index/DirectoryReader;->directory()Lorg/apache/lucene/store/Directory;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/lucene/index/DirectoryReader;->getSequentialSubReaders()Ljava/util/List;

    move-result-object v1

    # invokes: Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;->wrap(Ljava/util/List;)[Lorg/apache/lucene/index/AtomicReader;
    invoke-static {p2, v1}, Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;->access$0(Lorg/apache/lucene/index/FilterDirectoryReader$SubReaderWrapper;Ljava/util/List;)[Lorg/apache/lucene/index/AtomicReader;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/lucene/index/DirectoryReader;-><init>(Lorg/apache/lucene/store/Directory;[Lorg/apache/lucene/index/AtomicReader;)V

    .line 98
    iput-object p1, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    .line 99
    return-void
.end method

.method private final wrapDirectoryReader(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "in"    # Lorg/apache/lucene/index/DirectoryReader;

    .prologue
    .line 113
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/FilterDirectoryReader;->doWrapDirectoryReader(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected doClose()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->doClose()V

    .line 149
    return-void
.end method

.method protected final doOpenIfChanged()Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged()Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/FilterDirectoryReader;->wrapDirectoryReader(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected final doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "commit"    # Lorg/apache/lucene/index/IndexCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexCommit;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/FilterDirectoryReader;->wrapDirectoryReader(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected final doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;
    .locals 1
    .param p1, "writer"    # Lorg/apache/lucene/index/IndexWriter;
    .param p2, "applyAllDeletes"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0, p1, p2}, Lorg/apache/lucene/index/DirectoryReader;->doOpenIfChanged(Lorg/apache/lucene/index/IndexWriter;Z)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/lucene/index/FilterDirectoryReader;->wrapDirectoryReader(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;

    move-result-object v0

    return-object v0
.end method

.method protected abstract doWrapDirectoryReader(Lorg/apache/lucene/index/DirectoryReader;)Lorg/apache/lucene/index/DirectoryReader;
.end method

.method public getIndexCommit()Lorg/apache/lucene/index/IndexCommit;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->getIndexCommit()Lorg/apache/lucene/index/IndexCommit;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->getVersion()J

    move-result-wide v0

    return-wide v0
.end method

.method public isCurrent()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lorg/apache/lucene/index/FilterDirectoryReader;->in:Lorg/apache/lucene/index/DirectoryReader;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DirectoryReader;->isCurrent()Z

    move-result v0

    return v0
.end method
