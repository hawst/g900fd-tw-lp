.class Lorg/apache/lucene/index/DocFieldProcessor$1;
.super Ljava/lang/Object;
.source "DocFieldProcessor.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocFieldProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/index/DocFieldProcessorPerField;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/DocFieldProcessorPerField;

    check-cast p2, Lorg/apache/lucene/index/DocFieldProcessorPerField;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/DocFieldProcessor$1;->compare(Lorg/apache/lucene/index/DocFieldProcessorPerField;Lorg/apache/lucene/index/DocFieldProcessorPerField;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/index/DocFieldProcessorPerField;Lorg/apache/lucene/index/DocFieldProcessorPerField;)I
    .locals 2
    .param p1, "o1"    # Lorg/apache/lucene/index/DocFieldProcessorPerField;
    .param p2, "o2"    # Lorg/apache/lucene/index/DocFieldProcessorPerField;

    .prologue
    .line 266
    iget-object v0, p1, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v0, v0, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    iget-object v1, p2, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    iget-object v1, v1, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
