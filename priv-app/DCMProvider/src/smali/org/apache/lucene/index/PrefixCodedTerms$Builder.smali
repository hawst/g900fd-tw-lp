.class public Lorg/apache/lucene/index/PrefixCodedTerms$Builder;
.super Ljava/lang/Object;
.source "PrefixCodedTerms.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/PrefixCodedTerms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private buffer:Lorg/apache/lucene/store/RAMFile;

.field private lastTerm:Lorg/apache/lucene/index/Term;

.field private output:Lorg/apache/lucene/store/RAMOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    const-class v0, Lorg/apache/lucene/index/PrefixCodedTerms;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v0}, Lorg/apache/lucene/store/RAMFile;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->buffer:Lorg/apache/lucene/store/RAMFile;

    .line 100
    new-instance v0, Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->buffer:Lorg/apache/lucene/store/RAMFile;

    invoke-direct {v0, v1}, Lorg/apache/lucene/store/RAMOutputStream;-><init>(Lorg/apache/lucene/store/RAMFile;)V

    iput-object v0, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    .line 101
    new-instance v0, Lorg/apache/lucene/index/Term;

    const-string v1, ""

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    .line 98
    return-void
.end method

.method private sharedPrefix(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I
    .locals 6
    .param p1, "term1"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "term2"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "pos1":I
    iget v3, p1, Lorg/apache/lucene/util/BytesRef;->length:I

    iget v4, p2, Lorg/apache/lucene/util/BytesRef;->length:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    add-int v1, v0, v3

    .line 138
    .local v1, "pos1End":I
    const/4 v2, 0x0

    .line 139
    .local v2, "pos2":I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 146
    :cond_0
    return v0

    .line 140
    :cond_1
    iget-object v3, p1, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v4, p1, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    iget-object v4, p2, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget v5, p2, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v5, v2

    aget-byte v4, v4, v5

    if-ne v3, v4, :cond_0

    .line 143
    add-int/lit8 v0, v0, 0x1

    .line 144
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/lucene/index/Term;)V
    .locals 6
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 105
    sget-boolean v3, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    new-instance v4, Lorg/apache/lucene/index/Term;

    const-string v5, ""

    invoke-direct {v4, v5}, Lorg/apache/lucene/index/Term;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lorg/apache/lucene/index/Term;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    invoke-virtual {p1, v3}, Lorg/apache/lucene/index/Term;->compareTo(Lorg/apache/lucene/index/Term;)I

    move-result v3

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 108
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    iget-object v3, v3, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-direct {p0, v3, v4}, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->sharedPrefix(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/util/BytesRef;)I

    move-result v1

    .line 109
    .local v1, "prefix":I
    iget-object v3, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v3, v3, Lorg/apache/lucene/util/BytesRef;->length:I

    sub-int v2, v3, v1

    .line 110
    .local v2, "suffix":I
    iget-object v3, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    iget-object v4, v4, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    shl-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 116
    :goto_0
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v3, v2}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 117
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v4, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, v4, Lorg/apache/lucene/util/BytesRef;->bytes:[B

    iget-object v5, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget v5, v5, Lorg/apache/lucene/util/BytesRef;->offset:I

    add-int/2addr v5, v1

    invoke-virtual {v3, v4, v5, v2}, Lorg/apache/lucene/store/RAMOutputStream;->writeBytes([BII)V

    .line 118
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    iget-object v3, v3, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    iget-object v4, p1, Lorg/apache/lucene/index/Term;->bytes:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/util/BytesRef;->copyBytes(Lorg/apache/lucene/util/BytesRef;)V

    .line 119
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->lastTerm:Lorg/apache/lucene/index/Term;

    iget-object v4, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    iput-object v4, v3, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    .line 123
    return-void

    .line 113
    :cond_1
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    shl-int/lit8 v4, v1, 0x1

    or-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/RAMOutputStream;->writeVInt(I)V

    .line 114
    iget-object v3, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    iget-object v4, p1, Lorg/apache/lucene/index/Term;->field:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lorg/apache/lucene/store/RAMOutputStream;->writeString(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 120
    .end local v1    # "prefix":I
    .end local v2    # "suffix":I
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public finish()Lorg/apache/lucene/index/PrefixCodedTerms;
    .locals 4

    .prologue
    .line 128
    :try_start_0
    iget-object v1, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->output:Lorg/apache/lucene/store/RAMOutputStream;

    invoke-virtual {v1}, Lorg/apache/lucene/store/RAMOutputStream;->close()V

    .line 129
    new-instance v1, Lorg/apache/lucene/index/PrefixCodedTerms;

    iget-object v2, p0, Lorg/apache/lucene/index/PrefixCodedTerms$Builder;->buffer:Lorg/apache/lucene/store/RAMFile;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/apache/lucene/index/PrefixCodedTerms;-><init>(Lorg/apache/lucene/store/RAMFile;Lorg/apache/lucene/index/PrefixCodedTerms;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
