.class public Lorg/apache/lucene/index/FieldInfos;
.super Ljava/lang/Object;
.source "FieldInfos.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FieldInfos$Builder;,
        Lorg/apache/lucene/index/FieldInfos$FieldNumbers;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/apache/lucene/index/FieldInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final byName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final byNumber:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final hasDocValues:Z

.field private final hasFreq:Z

.field private final hasNorms:Z

.field private final hasOffsets:Z

.field private final hasPayloads:Z

.field private final hasProx:Z

.field private final hasVectors:Z

.field private final values:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lorg/apache/lucene/index/FieldInfos;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FieldInfos;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/index/FieldInfo;)V
    .locals 13
    .param p1, "infos"    # [Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v9, Ljava/util/TreeMap;

    invoke-direct {v9}, Ljava/util/TreeMap;-><init>()V

    iput-object v9, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/SortedMap;

    .line 45
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    .line 52
    const/4 v6, 0x0

    .line 53
    .local v6, "hasVectors":Z
    const/4 v5, 0x0

    .line 54
    .local v5, "hasProx":Z
    const/4 v4, 0x0

    .line 55
    .local v4, "hasPayloads":Z
    const/4 v3, 0x0

    .line 56
    .local v3, "hasOffsets":Z
    const/4 v1, 0x0

    .line 57
    .local v1, "hasFreq":Z
    const/4 v2, 0x0

    .line 58
    .local v2, "hasNorms":Z
    const/4 v0, 0x0

    .line 60
    .local v0, "hasDocValues":Z
    array-length v11, p1

    const/4 v9, 0x0

    move v10, v9

    :goto_0
    if-lt v10, v11, :cond_0

    .line 79
    iput-boolean v6, p0, Lorg/apache/lucene/index/FieldInfos;->hasVectors:Z

    .line 80
    iput-boolean v5, p0, Lorg/apache/lucene/index/FieldInfos;->hasProx:Z

    .line 81
    iput-boolean v4, p0, Lorg/apache/lucene/index/FieldInfos;->hasPayloads:Z

    .line 82
    iput-boolean v3, p0, Lorg/apache/lucene/index/FieldInfos;->hasOffsets:Z

    .line 83
    iput-boolean v1, p0, Lorg/apache/lucene/index/FieldInfos;->hasFreq:Z

    .line 84
    iput-boolean v2, p0, Lorg/apache/lucene/index/FieldInfos;->hasNorms:Z

    .line 85
    iput-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasDocValues:Z

    .line 86
    iget-object v9, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/SortedMap;

    invoke-interface {v9}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v9

    iput-object v9, p0, Lorg/apache/lucene/index/FieldInfos;->values:Ljava/util/Collection;

    .line 87
    return-void

    .line 60
    :cond_0
    aget-object v7, p1, v10

    .line 61
    .local v7, "info":Lorg/apache/lucene/index/FieldInfo;
    iget-object v9, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/SortedMap;

    iget v12, v7, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v9, v12, v7}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/lucene/index/FieldInfo;

    .line 62
    .local v8, "previous":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v8, :cond_1

    .line 63
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "duplicate field numbers: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v8, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " and "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " have: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 65
    :cond_1
    iget-object v9, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    iget-object v12, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v9, v12, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "previous":Lorg/apache/lucene/index/FieldInfo;
    check-cast v8, Lorg/apache/lucene/index/FieldInfo;

    .line 66
    .restart local v8    # "previous":Lorg/apache/lucene/index/FieldInfo;
    if-eqz v8, :cond_2

    .line 67
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "duplicate field names: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v8, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " and "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v7, Lorg/apache/lucene/index/FieldInfo;->number:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " have: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v7, Lorg/apache/lucene/index/FieldInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 70
    :cond_2
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->hasVectors()Z

    move-result v9

    or-int/2addr v6, v9

    .line 71
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v9

    sget-object v12, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v9

    if-ltz v9, :cond_3

    const/4 v9, 0x1

    :goto_1
    or-int/2addr v5, v9

    .line 72
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v9

    sget-object v12, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_ONLY:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    if-eq v9, v12, :cond_4

    const/4 v9, 0x1

    :goto_2
    or-int/2addr v1, v9

    .line 73
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->isIndexed()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->getIndexOptions()Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    move-result-object v9

    sget-object v12, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS:Lorg/apache/lucene/index/FieldInfo$IndexOptions;

    invoke-virtual {v9, v12}, Lorg/apache/lucene/index/FieldInfo$IndexOptions;->compareTo(Ljava/lang/Enum;)I

    move-result v9

    if-ltz v9, :cond_5

    const/4 v9, 0x1

    :goto_3
    or-int/2addr v3, v9

    .line 74
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->hasNorms()Z

    move-result v9

    or-int/2addr v2, v9

    .line 75
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->hasDocValues()Z

    move-result v9

    or-int/2addr v0, v9

    .line 76
    invoke-virtual {v7}, Lorg/apache/lucene/index/FieldInfo;->hasPayloads()Z

    move-result v9

    or-int/2addr v4, v9

    .line 60
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto/16 :goto_0

    .line 71
    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    .line 72
    :cond_4
    const/4 v9, 0x0

    goto :goto_2

    .line 73
    :cond_5
    const/4 v9, 0x0

    goto :goto_3
.end method


# virtual methods
.method public fieldInfo(I)Lorg/apache/lucene/index/FieldInfo;
    .locals 2
    .param p1, "fieldNumber"    # I

    .prologue
    .line 159
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/SortedMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public fieldInfo(Ljava/lang/String;)Lorg/apache/lucene/index/FieldInfo;
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 146
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/FieldInfo;

    return-object v0
.end method

.method public hasDocValues()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasDocValues:Z

    return v0
.end method

.method public hasFreq()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasFreq:Z

    return v0
.end method

.method public hasNorms()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasNorms:Z

    return v0
.end method

.method public hasOffsets()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasOffsets:Z

    return v0
.end method

.method public hasPayloads()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasPayloads:Z

    return v0
.end method

.method public hasProx()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasProx:Z

    return v0
.end method

.method public hasVectors()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lorg/apache/lucene/index/FieldInfos;->hasVectors:Z

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/apache/lucene/index/FieldInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->values:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 126
    sget-boolean v0, Lorg/apache/lucene/index/FieldInfos;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/lucene/index/FieldInfos;->byName:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 127
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/FieldInfos;->byNumber:Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->size()I

    move-result v0

    return v0
.end method
