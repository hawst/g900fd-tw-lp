.class public abstract Lorg/apache/lucene/index/BaseCompositeReader;
.super Lorg/apache/lucene/index/CompositeReader;
.source "BaseCompositeReader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lorg/apache/lucene/index/IndexReader;",
        ">",
        "Lorg/apache/lucene/index/CompositeReader;"
    }
.end annotation


# instance fields
.field private final maxDoc:I

.field private final numDocs:I

.field private final starts:[I

.field private final subReaders:[Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TR;"
        }
    .end annotation
.end field

.field private final subReadersList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TR;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>([Lorg/apache/lucene/index/IndexReader;)V
    .locals 6
    .param p1, "subReaders"    # [Lorg/apache/lucene/index/IndexReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TR;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/CompositeReader;-><init>()V

    .line 68
    iput-object p1, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    .line 69
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReadersList:Ljava/util/List;

    .line 70
    array-length v4, p1

    add-int/lit8 v4, v4, 0x1

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    .line 71
    const/4 v1, 0x0

    .local v1, "maxDoc":I
    const/4 v2, 0x0

    .line 72
    .local v2, "numDocs":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-lt v0, v4, :cond_0

    .line 82
    iget-object v4, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    array-length v5, p1

    aput v1, v4, v5

    .line 83
    iput v1, p0, Lorg/apache/lucene/index/BaseCompositeReader;->maxDoc:I

    .line 84
    iput v2, p0, Lorg/apache/lucene/index/BaseCompositeReader;->numDocs:I

    .line 85
    return-void

    .line 73
    :cond_0
    iget-object v4, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    aput v1, v4, v0

    .line 74
    aget-object v3, p1, v0

    .line 75
    .local v3, "r":Lorg/apache/lucene/index/IndexReader;
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->maxDoc()I

    move-result v4

    add-int/2addr v1, v4

    .line 76
    if-gez v1, :cond_1

    .line 77
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Too many documents, composite IndexReaders cannot exceed 2147483647"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 79
    :cond_1
    invoke-virtual {v3}, Lorg/apache/lucene/index/IndexReader;->numDocs()I

    move-result v4

    add-int/2addr v2, v4

    .line 80
    invoke-virtual {v3, p0}, Lorg/apache/lucene/index/IndexReader;->registerParentReader(Lorg/apache/lucene/index/IndexReader;)V

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final docFreq(Lorg/apache/lucene/index/Term;)I
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 116
    const/4 v1, 0x0

    .line 117
    .local v1, "total":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 120
    return v1

    .line 118
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lorg/apache/lucene/index/IndexReader;->docFreq(Lorg/apache/lucene/index/Term;)I

    move-result v2

    add-int/2addr v1, v2

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final document(ILorg/apache/lucene/index/StoredFieldVisitor;)V
    .locals 3
    .param p1, "docID"    # I
    .param p2, "visitor"    # Lorg/apache/lucene/index/StoredFieldVisitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 109
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/BaseCompositeReader;->readerIndex(I)I

    move-result v0

    .line 110
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/IndexReader;->document(ILorg/apache/lucene/index/StoredFieldVisitor;)V

    .line 111
    return-void
.end method

.method public final getDocCount(Ljava/lang/String;)I
    .locals 7
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    const/4 v3, -0x1

    .line 153
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 154
    const/4 v2, 0x0

    .line 155
    .local v2, "total":I
    iget-object v5, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_0

    .line 162
    .end local v2    # "total":I
    :goto_1
    return v2

    .line 155
    .restart local v2    # "total":I
    :cond_0
    aget-object v0, v5, v4

    .line 156
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;, "TR;"
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->getDocCount(Ljava/lang/String;)I

    move-result v1

    .line 157
    .local v1, "sub":I
    if-ne v1, v3, :cond_1

    move v2, v3

    .line 158
    goto :goto_1

    .line 160
    :cond_1
    add-int/2addr v2, v1

    .line 155
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected final getSequentialSubReaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+TR;>;"
        }
    .end annotation

    .prologue
    .line 197
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    iget-object v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReadersList:Ljava/util/List;

    return-object v0
.end method

.method public final getSumDocFreq(Ljava/lang/String;)J
    .locals 11
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    const-wide/16 v6, -0x1

    .line 139
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 140
    const-wide/16 v4, 0x0

    .line 141
    .local v4, "total":J
    iget-object v8, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v9, v8

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v9, :cond_0

    .line 148
    .end local v4    # "total":J
    :goto_1
    return-wide v4

    .line 141
    .restart local v4    # "total":J
    :cond_0
    aget-object v0, v8, v1

    .line 142
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;, "TR;"
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->getSumDocFreq(Ljava/lang/String;)J

    move-result-wide v2

    .line 143
    .local v2, "sub":J
    cmp-long v10, v2, v6

    if-nez v10, :cond_1

    move-wide v4, v6

    .line 144
    goto :goto_1

    .line 146
    :cond_1
    add-long/2addr v4, v2

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final getSumTotalTermFreq(Ljava/lang/String;)J
    .locals 11
    .param p1, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    const-wide/16 v6, -0x1

    .line 167
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 168
    const-wide/16 v4, 0x0

    .line 169
    .local v4, "total":J
    iget-object v8, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v9, v8

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v9, :cond_0

    .line 176
    .end local v4    # "total":J
    :goto_1
    return-wide v4

    .line 169
    .restart local v4    # "total":J
    :cond_0
    aget-object v0, v8, v1

    .line 170
    .local v0, "reader":Lorg/apache/lucene/index/IndexReader;, "TR;"
    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/IndexReader;->getSumTotalTermFreq(Ljava/lang/String;)J

    move-result-wide v2

    .line 171
    .local v2, "sub":J
    cmp-long v10, v2, v6

    if-nez v10, :cond_1

    move-wide v4, v6

    .line 172
    goto :goto_1

    .line 174
    :cond_1
    add-long/2addr v4, v2

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final getTermVectors(I)Lorg/apache/lucene/index/Fields;
    .locals 3
    .param p1, "docID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 90
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/BaseCompositeReader;->readerIndex(I)I

    move-result v0

    .line 91
    .local v0, "i":I
    iget-object v1, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/IndexReader;->getTermVectors(I)Lorg/apache/lucene/index/Fields;

    move-result-object v1

    return-object v1
.end method

.method public final maxDoc()I
    .locals 1

    .prologue
    .line 103
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    iget v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->maxDoc:I

    return v0
.end method

.method public final numDocs()I
    .locals 1

    .prologue
    .line 97
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    iget v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->numDocs:I

    return v0
.end method

.method protected final readerBase(I)I
    .locals 2
    .param p1, "readerIndex"    # I

    .prologue
    .line 189
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 190
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "readerIndex must be >= 0 and < getSequentialSubReaders().size()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    aget v0, v0, p1

    return v0
.end method

.method protected final readerIndex(I)I
    .locals 3
    .param p1, "docID"    # I

    .prologue
    .line 181
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    if-ltz p1, :cond_0

    iget v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->maxDoc:I

    if-lt p1, v0, :cond_1

    .line 182
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "docID must be >= 0 and < maxDoc="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lorg/apache/lucene/index/BaseCompositeReader;->maxDoc:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (got docID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_1
    iget-object v0, p0, Lorg/apache/lucene/index/BaseCompositeReader;->starts:[I

    invoke-static {p1, v0}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v0

    return v0
.end method

.method public final totalTermFreq(Lorg/apache/lucene/index/Term;)J
    .locals 8
    .param p1, "term"    # Lorg/apache/lucene/index/Term;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lorg/apache/lucene/index/BaseCompositeReader;, "Lorg/apache/lucene/index/BaseCompositeReader<TR;>;"
    const-wide/16 v6, -0x1

    .line 125
    invoke-virtual {p0}, Lorg/apache/lucene/index/BaseCompositeReader;->ensureOpen()V

    .line 126
    const-wide/16 v4, 0x0

    .line 127
    .local v4, "total":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 134
    .end local v4    # "total":J
    :goto_1
    return-wide v4

    .line 128
    .restart local v4    # "total":J
    :cond_0
    iget-object v1, p0, Lorg/apache/lucene/index/BaseCompositeReader;->subReaders:[Lorg/apache/lucene/index/IndexReader;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexReader;->totalTermFreq(Lorg/apache/lucene/index/Term;)J

    move-result-wide v2

    .line 129
    .local v2, "sub":J
    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    move-wide v4, v6

    .line 130
    goto :goto_1

    .line 132
    :cond_1
    add-long/2addr v4, v2

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
