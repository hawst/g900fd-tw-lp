.class final Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;
.super Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;
.source "DocumentsWriterDeleteQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/DocumentsWriterDeleteQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TermNode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node",
        "<",
        "Lorg/apache/lucene/index/Term;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/Term;)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/index/Term;

    .prologue
    .line 338
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$Node;-><init>(Ljava/lang/Object;)V

    .line 339
    return-void
.end method


# virtual methods
.method apply(Lorg/apache/lucene/index/BufferedDeletes;I)V
    .locals 1
    .param p1, "bufferedDeletes"    # Lorg/apache/lucene/index/BufferedDeletes;
    .param p2, "docIDUpto"    # I

    .prologue
    .line 343
    iget-object v0, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;->item:Ljava/lang/Object;

    check-cast v0, Lorg/apache/lucene/index/Term;

    invoke-virtual {p1, v0, p2}, Lorg/apache/lucene/index/BufferedDeletes;->addTerm(Lorg/apache/lucene/index/Term;I)V

    .line 344
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "del="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/lucene/index/DocumentsWriterDeleteQueue$TermNode;->item:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
