.class Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;
.super Ljava/lang/Object;
.source "FrozenBufferedDeletes.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

.field private upto:I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/FrozenBufferedDeletes$2;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->this$1:Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->upto:I

    iget-object v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->this$1:Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

    # getter for: Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->this$0:Lorg/apache/lucene/index/FrozenBufferedDeletes;
    invoke-static {v1}, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->access$0(Lorg/apache/lucene/index/FrozenBufferedDeletes$2;)Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0}, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->next()Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    .locals 4

    .prologue
    .line 112
    new-instance v0, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;

    iget-object v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->this$1:Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

    # getter for: Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->this$0:Lorg/apache/lucene/index/FrozenBufferedDeletes;
    invoke-static {v1}, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->access$0(Lorg/apache/lucene/index/FrozenBufferedDeletes$2;)Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-result-object v1

    iget-object v1, v1, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queries:[Lorg/apache/lucene/search/Query;

    iget v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->upto:I

    aget-object v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->this$1:Lorg/apache/lucene/index/FrozenBufferedDeletes$2;

    # getter for: Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->this$0:Lorg/apache/lucene/index/FrozenBufferedDeletes;
    invoke-static {v2}, Lorg/apache/lucene/index/FrozenBufferedDeletes$2;->access$0(Lorg/apache/lucene/index/FrozenBufferedDeletes$2;)Lorg/apache/lucene/index/FrozenBufferedDeletes;

    move-result-object v2

    iget-object v2, v2, Lorg/apache/lucene/index/FrozenBufferedDeletes;->queryLimits:[I

    iget v3, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->upto:I

    aget v2, v2, v3

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;-><init>(Lorg/apache/lucene/search/Query;I)V

    .line 113
    .local v0, "ret":Lorg/apache/lucene/index/BufferedDeletesStream$QueryAndLimit;
    iget v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->upto:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/lucene/index/FrozenBufferedDeletes$2$1;->upto:I

    .line 114
    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
