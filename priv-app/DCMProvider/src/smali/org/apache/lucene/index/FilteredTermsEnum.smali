.class public abstract Lorg/apache/lucene/index/FilteredTermsEnum;
.super Lorg/apache/lucene/index/TermsEnum;
.source "FilteredTermsEnum.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$org$apache$lucene$index$FilteredTermsEnum$AcceptStatus:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private actualTerm:Lorg/apache/lucene/util/BytesRef;

.field private doSeek:Z

.field private initialSeekTerm:Lorg/apache/lucene/util/BytesRef;

.field private final tenum:Lorg/apache/lucene/index/TermsEnum;


# direct methods
.method static synthetic $SWITCH_TABLE$org$apache$lucene$index$FilteredTermsEnum$AcceptStatus()[I
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lorg/apache/lucene/index/FilteredTermsEnum;->$SWITCH_TABLE$org$apache$lucene$index$FilteredTermsEnum$AcceptStatus:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->values()[Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->END:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->NO_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->YES_AND_SEEK:Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    invoke-virtual {v1}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lorg/apache/lucene/index/FilteredTermsEnum;->$SWITCH_TABLE$org$apache$lucene$index$FilteredTermsEnum$AcceptStatus:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lorg/apache/lucene/index/FilteredTermsEnum;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/FilteredTermsEnum;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;)V
    .locals 1
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/lucene/index/FilteredTermsEnum;-><init>(Lorg/apache/lucene/index/TermsEnum;Z)V

    .line 77
    return-void
.end method

.method public constructor <init>(Lorg/apache/lucene/index/TermsEnum;Z)V
    .locals 1
    .param p1, "tenum"    # Lorg/apache/lucene/index/TermsEnum;
    .param p2, "startWithSeek"    # Z

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Lorg/apache/lucene/index/TermsEnum;-><init>()V

    .line 40
    iput-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->initialSeekTerm:Lorg/apache/lucene/util/BytesRef;

    .line 42
    iput-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    .line 84
    sget-boolean v0, Lorg/apache/lucene/index/FilteredTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 85
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    .line 86
    iput-boolean p2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->doSeek:Z

    .line 87
    return-void
.end method


# virtual methods
.method protected abstract accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public attributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    return-object v0
.end method

.method public docFreq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->docFreq()I

    move-result v0

    return v0
.end method

.method public docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;
    .locals 1
    .param p1, "bits"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docs(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsEnum;I)Lorg/apache/lucene/index/DocsEnum;

    move-result-object v0

    return-object v0
.end method

.method public docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .locals 1
    .param p1, "bits"    # Lorg/apache/lucene/util/Bits;
    .param p2, "reuse"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;
    .param p3, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/lucene/index/TermsEnum;->docsAndPositions(Lorg/apache/lucene/util/Bits;Lorg/apache/lucene/index/DocsAndPositionsEnum;I)Lorg/apache/lucene/index/DocsAndPositionsEnum;

    move-result-object v0

    return-object v0
.end method

.method public getComparator()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lorg/apache/lucene/util/BytesRef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/lucene/util/BytesRef;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 219
    :goto_0
    iget-boolean v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->doSeek:Z

    if-eqz v2, :cond_4

    .line 220
    iput-boolean v4, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->doSeek:Z

    .line 221
    iget-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v2}, Lorg/apache/lucene/index/FilteredTermsEnum;->nextSeekTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    .line 224
    .local v0, "t":Lorg/apache/lucene/util/BytesRef;
    sget-boolean v2, Lorg/apache/lucene/index/FilteredTermsEnum;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/lucene/index/FilteredTermsEnum;->getComparator()Ljava/util/Comparator;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-interface {v2, v0, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gtz v2, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "curTerm="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seekTerm="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 225
    :cond_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2, v0, v4}, Lorg/apache/lucene/index/TermsEnum;->seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    move-result-object v2

    sget-object v3, Lorg/apache/lucene/index/TermsEnum$SeekStatus;->END:Lorg/apache/lucene/index/TermsEnum$SeekStatus;

    if-ne v2, v3, :cond_2

    .line 254
    .end local v0    # "t":Lorg/apache/lucene/util/BytesRef;
    :cond_1
    :goto_1
    :pswitch_0
    return-object v1

    .line 230
    .restart local v0    # "t":Lorg/apache/lucene/util/BytesRef;
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    .line 241
    .end local v0    # "t":Lorg/apache/lucene/util/BytesRef;
    :cond_3
    invoke-static {}, Lorg/apache/lucene/index/FilteredTermsEnum;->$SWITCH_TABLE$org$apache$lucene$index$FilteredTermsEnum$AcceptStatus()[I

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    invoke-virtual {p0, v3}, Lorg/apache/lucene/index/FilteredTermsEnum;->accept(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/lucene/index/FilteredTermsEnum$AcceptStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 247
    :goto_2
    :pswitch_2
    iget-object v1, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    goto :goto_1

    .line 233
    :cond_4
    iget-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v2}, Lorg/apache/lucene/index/TermsEnum;->next()Lorg/apache/lucene/util/BytesRef;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    .line 234
    iget-object v2, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->actualTerm:Lorg/apache/lucene/util/BytesRef;

    if-nez v2, :cond_3

    goto :goto_1

    .line 243
    :pswitch_3
    iput-boolean v5, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->doSeek:Z

    goto :goto_2

    .line 250
    :pswitch_4
    iput-boolean v5, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->doSeek:Z

    goto/16 :goto_0

    .line 241
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method protected nextSeekTerm(Lorg/apache/lucene/util/BytesRef;)Lorg/apache/lucene/util/BytesRef;
    .locals 2
    .param p1, "currentTerm"    # Lorg/apache/lucene/util/BytesRef;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->initialSeekTerm:Lorg/apache/lucene/util/BytesRef;

    .line 119
    .local v0, "t":Lorg/apache/lucene/util/BytesRef;
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->initialSeekTerm:Lorg/apache/lucene/util/BytesRef;

    .line 120
    return-object v0
.end method

.method public ord()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->ord()J

    move-result-wide v0

    return-wide v0
.end method

.method public seekCeil(Lorg/apache/lucene/util/BytesRef;Z)Lorg/apache/lucene/index/TermsEnum$SeekStatus;
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " does not support seeking"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public seekExact(J)V
    .locals 3
    .param p1, "ord"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " does not support seeking"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/TermState;)V
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "state"    # Lorg/apache/lucene/index/TermState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " does not support seeking"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public seekExact(Lorg/apache/lucene/util/BytesRef;Z)Z
    .locals 3
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;
    .param p2, "useCache"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " does not support seeking"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final setInitialSeekTerm(Lorg/apache/lucene/util/BytesRef;)V
    .locals 0
    .param p1, "term"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 99
    iput-object p1, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->initialSeekTerm:Lorg/apache/lucene/util/BytesRef;

    .line 100
    return-void
.end method

.method public term()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->term()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public termState()Lorg/apache/lucene/index/TermState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    sget-boolean v0, Lorg/apache/lucene/index/FilteredTermsEnum;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 209
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->termState()Lorg/apache/lucene/index/TermState;

    move-result-object v0

    return-object v0
.end method

.method public totalTermFreq()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lorg/apache/lucene/index/FilteredTermsEnum;->tenum:Lorg/apache/lucene/index/TermsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/TermsEnum;->totalTermFreq()J

    move-result-wide v0

    return-wide v0
.end method
