.class public Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;
.super Lorg/apache/lucene/index/DocsAndPositionsEnum;
.source "FilterAtomicReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/FilterAtomicReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilterDocsAndPositionsEnum"
.end annotation


# instance fields
.field protected final in:Lorg/apache/lucene/index/DocsAndPositionsEnum;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocsAndPositionsEnum;)V
    .locals 0
    .param p1, "in"    # Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .prologue
    .line 263
    invoke-direct {p0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;-><init>()V

    .line 264
    iput-object p1, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    .line 265
    return-void
.end method


# virtual methods
.method public advance(I)I
    .locals 1
    .param p1, "target"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0, p1}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->advance(I)I

    move-result v0

    return v0
.end method

.method public attributes()Lorg/apache/lucene/util/AttributeSource;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->attributes()Lorg/apache/lucene/util/AttributeSource;

    move-result-object v0

    return-object v0
.end method

.method public cost()J
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->cost()J

    move-result-wide v0

    return-wide v0
.end method

.method public docID()I
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->docID()I

    move-result v0

    return v0
.end method

.method public endOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->endOffset()I

    move-result v0

    return v0
.end method

.method public freq()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->freq()I

    move-result v0

    return v0
.end method

.method public getPayload()Lorg/apache/lucene/util/BytesRef;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->getPayload()Lorg/apache/lucene/util/BytesRef;

    move-result-object v0

    return-object v0
.end method

.method public nextDoc()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextDoc()I

    move-result v0

    return v0
.end method

.method public nextPosition()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->nextPosition()I

    move-result v0

    return v0
.end method

.method public startOffset()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lorg/apache/lucene/index/FilterAtomicReader$FilterDocsAndPositionsEnum;->in:Lorg/apache/lucene/index/DocsAndPositionsEnum;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocsAndPositionsEnum;->startOffset()I

    move-result v0

    return v0
.end method
