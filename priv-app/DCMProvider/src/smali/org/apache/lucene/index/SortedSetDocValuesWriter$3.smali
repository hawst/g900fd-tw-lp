.class Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;
.super Ljava/lang/Object;
.source "SortedSetDocValuesWriter.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/SortedSetDocValuesWriter;->flush(Lorg/apache/lucene/index/SegmentWriteState;Lorg/apache/lucene/codecs/DocValuesConsumer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

.field private final synthetic val$maxCountPerDoc:I

.field private final synthetic val$ordMap:[I


# direct methods
.method constructor <init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;[II)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;->this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    iput-object p2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;->val$ordMap:[I

    iput p3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;->val$maxCountPerDoc:I

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    new-instance v0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;

    iget-object v1, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;->this$0:Lorg/apache/lucene/index/SortedSetDocValuesWriter;

    iget-object v2, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;->val$ordMap:[I

    iget v3, p0, Lorg/apache/lucene/index/SortedSetDocValuesWriter$3;->val$maxCountPerDoc:I

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/lucene/index/SortedSetDocValuesWriter$OrdsIterator;-><init>(Lorg/apache/lucene/index/SortedSetDocValuesWriter;[II)V

    return-object v0
.end method
