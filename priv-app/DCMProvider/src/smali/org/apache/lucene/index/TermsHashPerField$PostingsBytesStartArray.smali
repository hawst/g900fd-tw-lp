.class final Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;
.super Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;
.source "TermsHashPerField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/TermsHashPerField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PostingsBytesStartArray"
.end annotation


# instance fields
.field private final bytesUsed:Lorg/apache/lucene/util/Counter;

.field private final perField:Lorg/apache/lucene/index/TermsHashPerField;


# direct methods
.method private constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/util/Counter;)V
    .locals 0
    .param p1, "perField"    # Lorg/apache/lucene/index/TermsHashPerField;
    .param p2, "bytesUsed"    # Lorg/apache/lucene/util/Counter;

    .prologue
    .line 288
    invoke-direct {p0}, Lorg/apache/lucene/util/BytesRefHash$BytesStartArray;-><init>()V

    .line 290
    iput-object p1, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    .line 291
    iput-object p2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 292
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/util/Counter;Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0, p1, p2}, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;-><init>(Lorg/apache/lucene/index/TermsHashPerField;Lorg/apache/lucene/util/Counter;)V

    return-void
.end method


# virtual methods
.method public bytesUsed()Lorg/apache/lucene/util/Counter;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    return-object v0
.end method

.method public clear()[I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 314
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v1, v1, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v2

    mul-int/2addr v1, v2

    neg-int v1, v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 316
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iput-object v4, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 318
    :cond_0
    return-object v4
.end method

.method public grow()[I
    .locals 6

    .prologue
    .line 305
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 306
    .local v1, "postingsArray":Lorg/apache/lucene/index/ParallelPostingsArray;
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v0, v2, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    .line 307
    .local v0, "oldSize":I
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->grow()Lorg/apache/lucene/index/ParallelPostingsArray;

    move-result-object v1

    .end local v1    # "postingsArray":Lorg/apache/lucene/index/ParallelPostingsArray;
    iput-object v1, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 308
    .restart local v1    # "postingsArray":Lorg/apache/lucene/index/ParallelPostingsArray;
    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    invoke-virtual {v1}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v3

    iget v4, v1, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    sub-int/2addr v4, v0

    mul-int/2addr v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 309
    iget-object v2, v1, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    return-object v2
.end method

.method public init()[I
    .locals 4

    .prologue
    .line 296
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    if-nez v0, :cond_0

    .line 297
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v1, Lorg/apache/lucene/index/TermsHashPerField;->consumer:Lorg/apache/lucene/index/TermsHashConsumerPerField;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lorg/apache/lucene/index/TermsHashConsumerPerField;->createPostingsArray(I)Lorg/apache/lucene/index/ParallelPostingsArray;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    .line 298
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iget-object v1, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v1, v1, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget v1, v1, Lorg/apache/lucene/index/ParallelPostingsArray;->size:I

    iget-object v2, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v2, v2, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    invoke-virtual {v2}, Lorg/apache/lucene/index/ParallelPostingsArray;->bytesPerPosting()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lorg/apache/lucene/util/Counter;->addAndGet(J)J

    .line 300
    :cond_0
    iget-object v0, p0, Lorg/apache/lucene/index/TermsHashPerField$PostingsBytesStartArray;->perField:Lorg/apache/lucene/index/TermsHashPerField;

    iget-object v0, v0, Lorg/apache/lucene/index/TermsHashPerField;->postingsArray:Lorg/apache/lucene/index/ParallelPostingsArray;

    iget-object v0, v0, Lorg/apache/lucene/index/ParallelPostingsArray;->textStarts:[I

    return-object v0
.end method
