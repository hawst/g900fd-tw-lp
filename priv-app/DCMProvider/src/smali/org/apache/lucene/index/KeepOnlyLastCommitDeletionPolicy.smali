.class public final Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;
.super Lorg/apache/lucene/index/IndexDeletionPolicy;
.source "KeepOnlyLastCommitDeletionPolicy.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/apache/lucene/index/IndexDeletionPolicy;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public onCommit(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 52
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v2, v1, -0x1

    if-lt v0, v2, :cond_0

    .line 55
    return-void

    .line 53
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/IndexCommit;

    invoke-virtual {v2}, Lorg/apache/lucene/index/IndexCommit;->delete()V

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onInit(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lorg/apache/lucene/index/IndexCommit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "commits":Ljava/util/List;, "Ljava/util/List<+Lorg/apache/lucene/index/IndexCommit;>;"
    invoke-virtual {p0, p1}, Lorg/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy;->onCommit(Ljava/util/List;)V

    .line 42
    return-void
.end method
