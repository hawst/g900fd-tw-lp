.class final Lorg/apache/lucene/index/MultiBits;
.super Ljava/lang/Object;
.source "MultiBits.java"

# interfaces
.implements Lorg/apache/lucene/util/Bits;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/MultiBits$SubResult;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final defaultValue:Z

.field private final starts:[I

.field private final subs:[Lorg/apache/lucene/util/Bits;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lorg/apache/lucene/index/MultiBits;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Lorg/apache/lucene/util/Bits;[IZ)V
    .locals 2
    .param p1, "subs"    # [Lorg/apache/lucene/util/Bits;
    .param p2, "starts"    # [I
    .param p3, "defaultValue"    # Z

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-boolean v0, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p2

    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_0
    iput-object p1, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    .line 41
    iput-object p2, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    .line 42
    iput-boolean p3, p0, Lorg/apache/lucene/index/MultiBits;->defaultValue:Z

    .line 43
    return-void
.end method

.method private checkLength(II)Z
    .locals 4
    .param p1, "reader"    # I
    .param p2, "doc"    # I

    .prologue
    .line 46
    iget-object v1, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    add-int/lit8 v2, p1, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v2, v2, p1

    sub-int v0, v1, v2

    .line 47
    .local v0, "length":I
    sget-boolean v1, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v1, v1, p1

    sub-int v1, p2, v1

    if-lt v1, v0, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "doc="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reader="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " starts[reader]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " length="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 48
    :cond_0
    const/4 v1, 0x1

    return v1
.end method


# virtual methods
.method public get(I)Z
    .locals 3
    .param p1, "doc"    # I

    .prologue
    .line 53
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    invoke-static {p1, v2}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v1

    .line 54
    .local v1, "reader":I
    sget-boolean v2, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 55
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    aget-object v0, v2, v1

    .line 56
    .local v0, "bits":Lorg/apache/lucene/util/Bits;
    if-nez v0, :cond_1

    .line 57
    iget-boolean v2, p0, Lorg/apache/lucene/index/MultiBits;->defaultValue:Z

    .line 60
    :goto_0
    return v2

    .line 59
    :cond_1
    sget-boolean v2, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    if-nez v2, :cond_2

    invoke-direct {p0, v1, p1}, Lorg/apache/lucene/index/MultiBits;->checkLength(II)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 60
    :cond_2
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v2, v2, v1

    sub-int v2, p1, v2

    invoke-interface {v0, v2}, Lorg/apache/lucene/util/Bits;->get(I)Z

    move-result v2

    goto :goto_0
.end method

.method public getMatchingSub(Lorg/apache/lucene/index/ReaderSlice;)Lorg/apache/lucene/index/MultiBits$SubResult;
    .locals 6
    .param p1, "slice"    # Lorg/apache/lucene/index/ReaderSlice;

    .prologue
    .line 100
    iget v2, p1, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    invoke-static {v2, v3}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v0

    .line 101
    .local v0, "reader":I
    sget-boolean v2, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 102
    :cond_0
    sget-boolean v2, Lorg/apache/lucene/index/MultiBits;->$assertionsDisabled:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "slice="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " starts[-1]="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    iget-object v5, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 103
    :cond_1
    new-instance v1, Lorg/apache/lucene/index/MultiBits$SubResult;

    invoke-direct {v1}, Lorg/apache/lucene/index/MultiBits$SubResult;-><init>()V

    .line 104
    .local v1, "subResult":Lorg/apache/lucene/index/MultiBits$SubResult;
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v2, v2, v0

    iget v3, p1, Lorg/apache/lucene/index/ReaderSlice;->start:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    add-int/lit8 v3, v0, 0x1

    aget v2, v2, v3

    iget v3, p1, Lorg/apache/lucene/index/ReaderSlice;->start:I

    iget v4, p1, Lorg/apache/lucene/index/ReaderSlice;->length:I

    add-int/2addr v3, v4

    if-ne v2, v3, :cond_2

    .line 105
    const/4 v2, 0x1

    iput-boolean v2, v1, Lorg/apache/lucene/index/MultiBits$SubResult;->matches:Z

    .line 106
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    aget-object v2, v2, v0

    iput-object v2, v1, Lorg/apache/lucene/index/MultiBits$SubResult;->result:Lorg/apache/lucene/util/Bits;

    .line 110
    :goto_0
    return-object v1

    .line 108
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v1, Lorg/apache/lucene/index/MultiBits$SubResult;->matches:Z

    goto :goto_0
.end method

.method public length()I
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    iget-object v1, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " subs: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    .line 78
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " end="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    iget-object v4, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    array-length v4, v4

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 69
    :cond_0
    if-eqz v1, :cond_1

    .line 70
    const-string v2, "; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_1
    iget-object v2, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    aget-object v2, v2, v1

    if-nez v2, :cond_2

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "s="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " l=null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "s="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->starts:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " l="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    aget-object v3, v3, v1

    invoke-interface {v3}, Lorg/apache/lucene/util/Bits;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " b="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/lucene/index/MultiBits;->subs:[Lorg/apache/lucene/util/Bits;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
