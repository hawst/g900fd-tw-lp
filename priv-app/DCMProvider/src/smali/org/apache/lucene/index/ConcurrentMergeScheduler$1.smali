.class Lorg/apache/lucene/index/ConcurrentMergeScheduler$1;
.super Ljava/lang/Object;
.source "ConcurrentMergeScheduler.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/ConcurrentMergeScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    check-cast p2, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    invoke-virtual {p0, p1, p2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$1;->compare(Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;)I
    .locals 5
    .param p1, "t1"    # Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;
    .param p2, "t2"    # Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;

    .prologue
    const v1, 0x7fffffff

    .line 150
    invoke-virtual {p1}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v2

    .line 151
    .local v2, "m1":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-virtual {p2}, Lorg/apache/lucene/index/ConcurrentMergeScheduler$MergeThread;->getCurrentMerge()Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-result-object v3

    .line 153
    .local v3, "m2":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    if-nez v2, :cond_0

    move v0, v1

    .line 154
    .local v0, "c1":I
    :goto_0
    if-nez v3, :cond_1

    .line 156
    .local v1, "c2":I
    :goto_1
    sub-int v4, v1, v0

    return v4

    .line 153
    .end local v0    # "c1":I
    .end local v1    # "c2":I
    :cond_0
    iget v0, v2, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalDocCount:I

    goto :goto_0

    .line 154
    .restart local v0    # "c1":I
    :cond_1
    iget v1, v3, Lorg/apache/lucene/index/MergePolicy$OneMerge;->totalDocCount:I

    goto :goto_1
.end method
