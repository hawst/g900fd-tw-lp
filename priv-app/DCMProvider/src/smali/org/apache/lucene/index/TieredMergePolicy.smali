.class public Lorg/apache/lucene/index/TieredMergePolicy;
.super Lorg/apache/lucene/index/MergePolicy;
.source "TieredMergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;,
        Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private floorSegmentBytes:J

.field private forceMergeDeletesPctAllowed:D

.field private maxCFSSegmentSize:J

.field private maxMergeAtOnce:I

.field private maxMergeAtOnceExplicit:I

.field private maxMergedSegmentBytes:J

.field private noCFSRatio:D

.field private reclaimDeletesWeight:D

.field private segsPerTier:D

.field private useCompoundFile:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-class v0, Lorg/apache/lucene/index/TieredMergePolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lorg/apache/lucene/index/TieredMergePolicy;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    .line 94
    invoke-direct {p0}, Lorg/apache/lucene/index/MergePolicy;-><init>()V

    .line 80
    const/16 v0, 0xa

    iput v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    .line 81
    const-wide v0, 0x140000000L

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    .line 82
    const/16 v0, 0x1e

    iput v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    .line 84
    const-wide/32 v0, 0x200000

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    .line 85
    iput-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    .line 86
    iput-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    .line 88
    const-wide v0, 0x3fb999999999999aL    # 0.1

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    .line 89
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxCFSSegmentSize:J

    .line 90
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    .line 95
    return-void
.end method

.method static synthetic access$0(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/SegmentInfoPerCommit;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v0

    return-wide v0
.end method

.method private floorSize(J)J
    .locals 3
    .param p1, "bytes"    # J

    .prologue
    .line 682
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 8
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 663
    iget-object v4, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v4}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/lucene/index/IndexWriter;

    .line 664
    .local v1, "w":Lorg/apache/lucene/index/IndexWriter;
    sget-boolean v4, Lorg/apache/lucene/index/TieredMergePolicy;->$assertionsDisabled:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 665
    :cond_0
    invoke-virtual {v1, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v4

    if-lez v4, :cond_2

    move v0, v2

    .line 666
    .local v0, "hasDeletions":Z
    :goto_0
    if-nez v0, :cond_3

    .line 667
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->hasSeparateNorms()Z

    move-result v4

    if-nez v4, :cond_3

    .line 668
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    iget-object v4, v4, Lorg/apache/lucene/index/SegmentInfo;->dir:Lorg/apache/lucene/store/Directory;

    invoke-virtual {v1}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v5

    if-ne v4, v5, :cond_3

    .line 669
    iget-object v4, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v4}, Lorg/apache/lucene/index/SegmentInfo;->getUseCompoundFile()Z

    move-result v4

    iget-boolean v5, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    if-eq v4, v5, :cond_1

    iget-wide v4, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_1

    iget-wide v4, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxCFSSegmentSize:J

    const-wide v6, 0x7fffffffffffffffL

    .line 666
    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    :cond_1
    :goto_1
    return v2

    .end local v0    # "hasDeletions":Z
    :cond_2
    move v0, v3

    .line 665
    goto :goto_0

    .restart local v0    # "hasDeletions":Z
    :cond_3
    move v2, v3

    .line 666
    goto :goto_1
.end method

.method private message(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 691
    iget-object v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v0}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    iget-object v0, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v1, "TMP"

    invoke-virtual {v0, v1, p1}, Lorg/apache/lucene/util/InfoStream;->message(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    return-void
.end method

.method private size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J
    .locals 12
    .param p1, "info"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 674
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v0

    .line 675
    .local v0, "byteSize":J
    iget-object v3, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v3}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v3, p1}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v2

    .line 676
    .local v2, "delCount":I
    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    if-gtz v3, :cond_0

    const-wide/16 v4, 0x0

    .line 677
    .local v4, "delRatio":D
    :goto_0
    sget-boolean v3, Lorg/apache/lucene/index/TieredMergePolicy;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    cmpg-double v3, v4, v10

    if-lez v3, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 676
    .end local v4    # "delRatio":D
    :cond_0
    int-to-double v6, v2

    iget-object v3, p1, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v3}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v3

    int-to-double v8, v3

    div-double v4, v6, v8

    goto :goto_0

    .line 678
    .restart local v4    # "delRatio":D
    :cond_1
    long-to-double v6, v0

    sub-double v8, v10, v4

    mul-double/2addr v6, v8

    double-to-long v6, v6

    return-wide v6
.end method

.method private verbose()Z
    .locals 3

    .prologue
    .line 686
    iget-object v1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v1}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/IndexWriter;

    .line 687
    .local v0, "w":Lorg/apache/lucene/index/IndexWriter;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/lucene/index/IndexWriter;->infoStream:Lorg/apache/lucene/util/InfoStream;

    const-string v2, "TMP"

    invoke-virtual {v1, v2}, Lorg/apache/lucene/util/InfoStream;->isEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 660
    return-void
.end method

.method public findForcedDeletesMerges(Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 18
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 594
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 595
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v11, "findForcedDeletesMerges infos="

    invoke-direct {v12, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " forceMergeDeletesPctAllowed="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-wide v12, v0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 597
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 598
    .local v2, "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v11}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v6

    .line 599
    .local v6, "merging":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    invoke-virtual/range {p1 .. p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_3

    .line 606
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_4

    .line 607
    const/4 v7, 0x0

    .line 636
    :cond_2
    return-object v7

    .line 599
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 600
    .local v4, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    const-wide/high16 v14, 0x4059000000000000L    # 100.0

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v11, v4}, Lorg/apache/lucene/index/IndexWriter;->numDeletedDocs(Lorg/apache/lucene/index/SegmentInfoPerCommit;)I

    move-result v11

    int-to-double v0, v11

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    iget-object v11, v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;->info:Lorg/apache/lucene/index/SegmentInfo;

    invoke-virtual {v11}, Lorg/apache/lucene/index/SegmentInfo;->getDocCount()I

    move-result v11

    int-to-double v0, v11

    move-wide/from16 v16, v0

    div-double v8, v14, v16

    .line 601
    .local v8, "pctDeletes":D
    move-object/from16 v0, p0

    iget-wide v14, v0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    cmpl-double v11, v8, v14

    if-lez v11, :cond_1

    invoke-interface {v6, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 602
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 610
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v8    # "pctDeletes":D
    :cond_4
    new-instance v11, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v12}, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;)V

    invoke-static {v2, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 612
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 613
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "eligible="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 616
    :cond_5
    const/4 v10, 0x0

    .line 617
    .local v10, "start":I
    const/4 v7, 0x0

    .line 619
    .local v7, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    if-ge v10, v11, :cond_2

    .line 623
    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    add-int/2addr v11, v10

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 624
    .local v3, "end":I
    if-nez v7, :cond_6

    .line 625
    new-instance v7, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v7    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v7}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 628
    .restart local v7    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_6
    new-instance v5, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    invoke-interface {v2, v10, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    invoke-direct {v5, v11}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 629
    .local v5, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 630
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v11, "add merge="

    invoke-direct {v12, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    iget-object v13, v5, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v11, v13}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 632
    :cond_7
    invoke-virtual {v7, v5}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 633
    move v10, v3

    goto :goto_1
.end method

.method public findForcedMerges(Lorg/apache/lucene/index/SegmentInfos;ILjava/util/Map;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 14
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "maxSegmentCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/lucene/index/SegmentInfos;",
            "I",
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lorg/apache/lucene/index/MergePolicy$MergeSpecification;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 522
    .local p3, "segmentsToMerge":Ljava/util/Map;, "Ljava/util/Map<Lorg/apache/lucene/index/SegmentInfoPerCommit;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 523
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "findForcedMerges maxSegmentCount="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " infos="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v11, p1}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " segmentsToMerge="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 526
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 527
    .local v1, "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/4 v3, 0x0

    .line 528
    .local v3, "forceMergeRunning":Z
    iget-object v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v11}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v7

    .line 529
    .local v7, "merging":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/4 v9, 0x0

    .line 530
    .local v9, "segmentIsOriginal":Z
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-nez v12, :cond_3

    .line 542
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_5

    .line 543
    const/4 v10, 0x0

    .line 589
    :cond_2
    :goto_1
    return-object v10

    .line 530
    :cond_3
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 531
    .local v4, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    .line 532
    .local v5, "isOriginal":Ljava/lang/Boolean;
    if-eqz v5, :cond_1

    .line 533
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    .line 534
    invoke-interface {v7, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 535
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 537
    :cond_4
    const/4 v3, 0x1

    goto :goto_0

    .line 546
    .end local v4    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v5    # "isOriginal":Ljava/lang/Boolean;
    :cond_5
    const/4 v11, 0x1

    move/from16 v0, p2

    if-le v0, v11, :cond_6

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    move/from16 v0, p2

    if-le v11, v0, :cond_7

    .line 547
    :cond_6
    const/4 v11, 0x1

    move/from16 v0, p2

    if-ne v0, v11, :cond_9

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_9

    if-eqz v9, :cond_7

    const/4 v11, 0x0

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->isMerged(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 548
    :cond_7
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 549
    const-string v11, "already merged"

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 551
    :cond_8
    const/4 v10, 0x0

    goto :goto_1

    .line 554
    :cond_9
    new-instance v11, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;)V

    invoke-static {v1, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 556
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 557
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "eligible="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 558
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "forceMergeRunning="

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 561
    :cond_a
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 563
    .local v2, "end":I
    const/4 v10, 0x0

    .line 566
    .local v10, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :goto_2
    iget v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    add-int v11, v11, p2

    add-int/lit8 v11, v11, -0x1

    if-ge v2, v11, :cond_c

    .line 578
    if-nez v10, :cond_2

    if-nez v3, :cond_2

    .line 580
    sub-int v11, v2, p2

    add-int/lit8 v8, v11, 0x1

    .line 581
    .local v8, "numToMerge":I
    new-instance v6, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    sub-int v11, v2, v8

    invoke-interface {v1, v11, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    invoke-direct {v6, v11}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 582
    .local v6, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 583
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v11, "add final merge="

    invoke-direct {v12, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual {v11}, Lorg/apache/lucene/index/IndexWriter;->getDirectory()Lorg/apache/lucene/store/Directory;

    move-result-object v11

    invoke-virtual {v6, v11}, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segString(Lorg/apache/lucene/store/Directory;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 585
    :cond_b
    new-instance v10, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v10    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v10}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 586
    .restart local v10    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-virtual {v10, v6}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    goto/16 :goto_1

    .line 567
    .end local v6    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v8    # "numToMerge":I
    :cond_c
    if-nez v10, :cond_d

    .line 568
    new-instance v10, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v10    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct {v10}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 570
    .restart local v10    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_d
    new-instance v6, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    iget v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    sub-int v11, v2, v11

    invoke-interface {v1, v11, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v11

    invoke-direct {v6, v11}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 571
    .restart local v6    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    invoke-direct {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v11

    if-eqz v11, :cond_e

    .line 572
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v11, "add merge="

    invoke-direct {v12, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    invoke-virtual {v11}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/lucene/index/IndexWriter;

    iget-object v13, v6, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    invoke-virtual {v11, v13}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 574
    :cond_e
    invoke-virtual {v10, v6}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 575
    iget v11, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    sub-int/2addr v2, v11

    goto/16 :goto_2
.end method

.method public findMerges(Lorg/apache/lucene/index/MergePolicy$MergeTrigger;Lorg/apache/lucene/index/SegmentInfos;)Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .locals 54
    .param p1, "mergeTrigger"    # Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .param p2, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v39

    if-eqz v39, :cond_0

    .line 306
    new-instance v39, Ljava/lang/StringBuilder;

    const-string v44, "findMerges: "

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v44

    move-object/from16 v0, v39

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " segments"

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 308
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->size()I

    move-result v39

    if-nez v39, :cond_2

    .line 309
    const/16 v31, 0x0

    .line 459
    :cond_1
    return-object v31

    .line 311
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/IndexWriter;

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/index/IndexWriter;->getMergingSegments()Ljava/util/Collection;

    move-result-object v25

    .line 312
    .local v25, "merging":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    new-instance v37, Ljava/util/HashSet;

    invoke-direct/range {v37 .. v37}, Ljava/util/HashSet;-><init>()V

    .line 314
    .local v37, "toBeMerged":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-virtual/range {p2 .. p2}, Lorg/apache/lucene/index/SegmentInfos;->asList()Ljava/util/List;

    move-result-object v39

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 315
    .local v20, "infosSorted":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    new-instance v39, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;

    const/16 v44, 0x0

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    move-object/from16 v2, v44

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;Lorg/apache/lucene/index/TieredMergePolicy$SegmentByteSizeDescending;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 318
    const-wide/16 v42, 0x0

    .line 319
    .local v42, "totIndexBytes":J
    const-wide v28, 0x7fffffffffffffffL

    .line 320
    .local v28, "minSegmentBytes":J
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v44

    :goto_0
    invoke-interface/range {v44 .. v44}, Ljava/util/Iterator;->hasNext()Z

    move-result v39

    if-nez v39, :cond_7

    .line 339
    const/16 v38, 0x0

    .line 340
    .local v38, "tooBigCount":I
    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v39

    move/from16 v0, v38

    move/from16 v1, v39

    if-ge v0, v1, :cond_3

    move-object/from16 v0, v20

    move/from16 v1, v38

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v44

    move-wide/from16 v0, v44

    long-to-double v0, v0

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    long-to-double v0, v0

    move-wide/from16 v46, v0

    const-wide/high16 v48, 0x4000000000000000L    # 2.0

    div-double v46, v46, v48

    cmpl-double v39, v44, v46

    if-gez v39, :cond_c

    .line 345
    :cond_3
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/TieredMergePolicy;->floorSize(J)J

    move-result-wide v28

    .line 348
    move-wide/from16 v22, v28

    .line 349
    .local v22, "levelSize":J
    move-wide/from16 v12, v42

    .line 350
    .local v12, "bytesLeft":J
    const-wide/16 v4, 0x0

    .line 352
    .local v4, "allowedSegCount":D
    :goto_2
    long-to-double v0, v12

    move-wide/from16 v44, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v46, v0

    div-double v34, v44, v46

    .line 353
    .local v34, "segCountLevel":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    move-wide/from16 v44, v0

    cmpg-double v39, v34, v44

    if-gez v39, :cond_d

    .line 354
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v44

    add-double v4, v4, v44

    .line 361
    double-to-int v6, v4

    .line 363
    .local v6, "allowedSegCountInt":I
    const/16 v31, 0x0

    .line 368
    .local v31, "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_4
    :goto_3
    const-wide/16 v26, 0x0

    .line 373
    .local v26, "mergingBytes":J
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 374
    .local v15, "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    move/from16 v18, v38

    .local v18, "idx":I
    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v39

    move/from16 v0, v18

    move/from16 v1, v39

    if-lt v0, v1, :cond_e

    .line 383
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v44, v0

    cmp-long v39, v26, v44

    if-ltz v39, :cond_11

    const/16 v21, 0x1

    .line 385
    .local v21, "maxMergeIsRunning":Z
    :goto_5
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v39

    if-eqz v39, :cond_5

    .line 386
    new-instance v39, Ljava/lang/StringBuilder;

    const-string v44, "  allowedSegmentCount="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " vs count="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v44

    move-object/from16 v0, v39

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " (eligible count="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v44

    move-object/from16 v0, v39

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, ") tooBigCount="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 389
    :cond_5
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v39

    if-eqz v39, :cond_1

    .line 393
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v39

    move/from16 v0, v39

    if-lt v0, v6, :cond_1

    .line 396
    const/4 v10, 0x0

    .line 397
    .local v10, "bestScore":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    const/4 v7, 0x0

    .line 398
    .local v7, "best":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/4 v11, 0x0

    .line 399
    .local v11, "bestTooLarge":Z
    const-wide/16 v8, 0x0

    .line 402
    .local v8, "bestMergeBytes":J
    const/16 v36, 0x0

    .local v36, "startIdx":I
    :goto_6
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v39

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v44, v0

    sub-int v39, v39, v44

    move/from16 v0, v36

    move/from16 v1, v39

    if-le v0, v1, :cond_12

    .line 442
    if-eqz v7, :cond_1

    .line 443
    if-nez v31, :cond_6

    .line 444
    new-instance v31, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;

    .end local v31    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    invoke-direct/range {v31 .. v31}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;-><init>()V

    .line 446
    .restart local v31    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_6
    new-instance v24, Lorg/apache/lucene/index/MergePolicy$OneMerge;

    move-object/from16 v0, v24

    invoke-direct {v0, v7}, Lorg/apache/lucene/index/MergePolicy$OneMerge;-><init>(Ljava/util/List;)V

    .line 447
    .local v24, "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/MergePolicy$MergeSpecification;->add(Lorg/apache/lucene/index/MergePolicy$OneMerge;)V

    .line 448
    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v39, v0

    invoke-interface/range {v39 .. v39}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v39

    :goto_7
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->hasNext()Z

    move-result v44

    if-nez v44, :cond_1a

    .line 452
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v39

    if-eqz v39, :cond_4

    .line 453
    new-instance v44, Ljava/lang/StringBuilder;

    const-string v39, "  add merge="

    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v24

    iget-object v0, v0, Lorg/apache/lucene/index/MergePolicy$OneMerge;->segments:Ljava/util/List;

    move-object/from16 v45, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " size="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v44, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v45, "%.3f MB"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    long-to-double v0, v8

    move-wide/from16 v48, v0

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v44 .. v46}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " score="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v44, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v45, "%.3f"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    invoke-virtual {v10}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v48

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v44 .. v46}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " "

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual {v10}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getExplanation()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    if-eqz v11, :cond_1b

    const-string v39, " [max merge]"

    :goto_8
    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 320
    .end local v4    # "allowedSegCount":D
    .end local v6    # "allowedSegCountInt":I
    .end local v7    # "best":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .end local v8    # "bestMergeBytes":J
    .end local v10    # "bestScore":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .end local v11    # "bestTooLarge":Z
    .end local v12    # "bytesLeft":J
    .end local v15    # "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .end local v18    # "idx":I
    .end local v21    # "maxMergeIsRunning":Z
    .end local v22    # "levelSize":J
    .end local v24    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    .end local v26    # "mergingBytes":J
    .end local v31    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    .end local v34    # "segCountLevel":D
    .end local v36    # "startIdx":I
    .end local v38    # "tooBigCount":I
    :cond_7
    invoke-interface/range {v44 .. v44}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 321
    .local v19, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v32

    .line 322
    .local v32, "segBytes":J
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v39

    if-eqz v39, :cond_9

    .line 323
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_a

    const-string v16, " [merging]"

    .line 324
    .local v16, "extra":Ljava/lang/String;
    :goto_9
    move-wide/from16 v0, v32

    long-to-double v0, v0

    move-wide/from16 v46, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v48, v0

    move-wide/from16 v0, v48

    long-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide/high16 v50, 0x4000000000000000L    # 2.0

    div-double v48, v48, v50

    cmpl-double v39, v46, v48

    if-ltz v39, :cond_b

    .line 325
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, " [skip: too large]"

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 329
    :cond_8
    :goto_a
    new-instance v45, Ljava/lang/StringBuilder;

    const-string v39, "  seg="

    move-object/from16 v0, v45

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v39

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/apache/lucene/index/IndexWriter;->segString(Lorg/apache/lucene/index/SegmentInfoPerCommit;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v45

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v45, " size="

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v45, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v46, "%.3f"

    const/16 v47, 0x1

    move/from16 v0, v47

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v47, v0

    const/16 v48, 0x0

    const-wide/16 v50, 0x400

    div-long v50, v32, v50

    move-wide/from16 v0, v50

    long-to-double v0, v0

    move-wide/from16 v50, v0

    const-wide/high16 v52, 0x4090000000000000L    # 1024.0

    div-double v50, v50, v52

    invoke-static/range {v50 .. v51}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v49

    aput-object v49, v47, v48

    invoke-static/range {v45 .. v47}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v45, " MB"

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 332
    .end local v16    # "extra":Ljava/lang/String;
    :cond_9
    move-wide/from16 v0, v32

    move-wide/from16 v2, v28

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v28

    .line 334
    add-long v42, v42, v32

    goto/16 :goto_0

    .line 323
    :cond_a
    const-string v16, ""

    goto/16 :goto_9

    .line 326
    .restart local v16    # "extra":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    move-wide/from16 v46, v0

    cmp-long v39, v32, v46

    if-gez v39, :cond_8

    .line 327
    new-instance v39, Ljava/lang/StringBuilder;

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v45, " [floored]"

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_a

    .line 341
    .end local v16    # "extra":Ljava/lang/String;
    .end local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v32    # "segBytes":J
    .restart local v38    # "tooBigCount":I
    :cond_c
    move-object/from16 v0, v20

    move/from16 v1, v38

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v44

    sub-long v42, v42, v44

    .line 342
    add-int/lit8 v38, v38, 0x1

    goto/16 :goto_1

    .line 357
    .restart local v4    # "allowedSegCount":D
    .restart local v12    # "bytesLeft":J
    .restart local v22    # "levelSize":J
    .restart local v34    # "segCountLevel":D
    :cond_d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    move-wide/from16 v44, v0

    add-double v4, v4, v44

    .line 358
    long-to-double v0, v12

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    move-wide/from16 v46, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v48, v0

    mul-double v46, v46, v48

    sub-double v44, v44, v46

    move-wide/from16 v0, v44

    double-to-long v12, v0

    .line 359
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v39, v0

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v44, v0

    mul-long v22, v22, v44

    .line 351
    goto/16 :goto_2

    .line 375
    .restart local v6    # "allowedSegCountInt":I
    .restart local v15    # "eligible":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .restart local v18    # "idx":I
    .restart local v26    # "mergingBytes":J
    .restart local v31    # "spec":Lorg/apache/lucene/index/MergePolicy$MergeSpecification;
    :cond_e
    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 376
    .restart local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_10

    .line 377
    invoke-virtual/range {v19 .. v19}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v44

    add-long v26, v26, v44

    .line 374
    :cond_f
    :goto_b
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_4

    .line 378
    :cond_10
    move-object/from16 v0, v37

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_f

    .line 379
    move-object/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 383
    .end local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_11
    const/16 v21, 0x0

    goto/16 :goto_5

    .line 404
    .restart local v7    # "best":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .restart local v8    # "bestMergeBytes":J
    .restart local v10    # "bestScore":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .restart local v11    # "bestTooLarge":Z
    .restart local v21    # "maxMergeIsRunning":Z
    .restart local v36    # "startIdx":I
    :cond_12
    const-wide/16 v40, 0x0

    .line 406
    .local v40, "totAfterMergeBytes":J
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 407
    .local v14, "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const/16 v17, 0x0

    .line 408
    .local v17, "hitTooLarge":Z
    move/from16 v18, v36

    :goto_c
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v39

    move/from16 v0, v18

    move/from16 v1, v39

    if-ge v0, v1, :cond_13

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v39

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    move/from16 v44, v0

    move/from16 v0, v39

    move/from16 v1, v44

    if-lt v0, v1, :cond_18

    .line 426
    :cond_13
    move-object/from16 v0, p0

    move/from16 v1, v17

    move-wide/from16 v2, v26

    invoke-virtual {v0, v14, v1, v2, v3}, Lorg/apache/lucene/index/TieredMergePolicy;->score(Ljava/util/List;ZJ)Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;

    move-result-object v30

    .line 427
    .local v30, "score":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    invoke-direct/range {p0 .. p0}, Lorg/apache/lucene/index/TieredMergePolicy;->verbose()Z

    move-result v39

    if-eqz v39, :cond_14

    .line 428
    new-instance v44, Ljava/lang/StringBuilder;

    const-string v39, "  maybe="

    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->writer:Lorg/apache/lucene/util/SetOnce;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lorg/apache/lucene/util/SetOnce;->get()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lorg/apache/lucene/index/IndexWriter;

    move-object/from16 v0, v39

    invoke-virtual {v0, v14}, Lorg/apache/lucene/index/IndexWriter;->segString(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v44

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " score="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v44

    move-object/from16 v0, v39

    move-wide/from16 v1, v44

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " "

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getExplanation()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " tooLarge="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    move-object/from16 v0, v39

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v39

    const-string v44, " size="

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    sget-object v44, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v45, "%.3f MB"

    const/16 v46, 0x1

    move/from16 v0, v46

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v46, v0

    const/16 v47, 0x0

    move-wide/from16 v0, v40

    long-to-double v0, v0

    move-wide/from16 v48, v0

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    const-wide/high16 v50, 0x4090000000000000L    # 1024.0

    div-double v48, v48, v50

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v48

    aput-object v48, v46, v47

    invoke-static/range {v44 .. v46}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v39

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->message(Ljava/lang/String;)V

    .line 434
    :cond_14
    if-eqz v10, :cond_15

    invoke-virtual/range {v30 .. v30}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v44

    invoke-virtual {v10}, Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;->getScore()D

    move-result-wide v46

    cmpg-double v39, v44, v46

    if-gez v39, :cond_17

    :cond_15
    if-eqz v17, :cond_16

    if-nez v21, :cond_17

    .line 435
    :cond_16
    move-object v7, v14

    .line 436
    move-object/from16 v10, v30

    .line 437
    move/from16 v11, v17

    .line 438
    move-wide/from16 v8, v40

    .line 402
    :cond_17
    add-int/lit8 v36, v36, 0x1

    goto/16 :goto_6

    .line 409
    .end local v30    # "score":Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    :cond_18
    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 410
    .restart local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v32

    .line 412
    .restart local v32    # "segBytes":J
    add-long v44, v40, v32

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    move-wide/from16 v46, v0

    cmp-long v39, v44, v46

    if-lez v39, :cond_19

    .line 413
    const/16 v17, 0x1

    .line 408
    :goto_d
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_c

    .line 422
    :cond_19
    move-object/from16 v0, v19

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    add-long v40, v40, v32

    goto :goto_d

    .line 448
    .end local v14    # "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    .end local v17    # "hitTooLarge":Z
    .end local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v32    # "segBytes":J
    .end local v40    # "totAfterMergeBytes":J
    .restart local v24    # "merge":Lorg/apache/lucene/index/MergePolicy$OneMerge;
    :cond_1a
    invoke-interface/range {v39 .. v39}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 449
    .restart local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, v37

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 453
    .end local v19    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_1b
    const-string v39, ""

    goto/16 :goto_8
.end method

.method public getFloorSegmentMB()D
    .locals 4

    .prologue
    .line 192
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    long-to-double v0, v0

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getForceMergeDeletesPctAllowed()D
    .locals 2

    .prologue
    .line 210
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    return-wide v0
.end method

.method public final getMaxCFSSegmentSizeMB()D
    .locals 4

    .prologue
    .line 711
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxCFSSegmentSize:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-double v0, v0

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getMaxMergeAtOnce()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    return v0
.end method

.method public getMaxMergeAtOnceExplicit()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    return v0
.end method

.method public getMaxMergedSegmentMB()D
    .locals 4

    .prologue
    .line 154
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-double v0, v0

    const-wide/high16 v2, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getNoCFSRatio()D
    .locals 2

    .prologue
    .line 268
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    return-wide v0
.end method

.method public getReclaimDeletesWeight()D
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    return-wide v0
.end method

.method public getSegmentsPerTier()D
    .locals 2

    .prologue
    .line 233
    iget-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    return-wide v0
.end method

.method public getUseCompoundFile()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    return v0
.end method

.method protected score(Ljava/util/List;ZJ)Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;
    .locals 24
    .param p2, "hitTooLarge"    # Z
    .param p3, "mergingBytes"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/lucene/index/SegmentInfoPerCommit;",
            ">;ZJ)",
            "Lorg/apache/lucene/index/TieredMergePolicy$MergeScore;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    .local p1, "candidate":Ljava/util/List;, "Ljava/util/List<Lorg/apache/lucene/index/SegmentInfoPerCommit;>;"
    const-wide/16 v20, 0x0

    .line 467
    .local v20, "totBeforeMergeBytes":J
    const-wide/16 v16, 0x0

    .line 468
    .local v16, "totAfterMergeBytes":J
    const-wide/16 v18, 0x0

    .line 469
    .local v18, "totAfterMergeBytesFloored":J
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 480
    if-eqz p2, :cond_1

    .line 485
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    int-to-double v0, v11

    move-wide/from16 v22, v0

    div-double v6, v2, v22

    .line 492
    .local v6, "skew":D
    :goto_1
    move-wide v12, v6

    .line 498
    .local v12, "mergeScore":D
    move-wide/from16 v0, v16

    long-to-double v2, v0

    const-wide v22, 0x3fa999999999999aL    # 0.05

    move-wide/from16 v0, v22

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v12, v2

    .line 501
    move-wide/from16 v0, v16

    long-to-double v2, v0

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v8, v2, v22

    .line 502
    .local v8, "nonDelRatio":D
    move-object/from16 v0, p0

    iget-wide v2, v0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v12, v2

    .line 504
    move-wide v4, v12

    .line 506
    .local v4, "finalMergeScore":D
    new-instance v2, Lorg/apache/lucene/index/TieredMergePolicy$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lorg/apache/lucene/index/TieredMergePolicy$1;-><init>(Lorg/apache/lucene/index/TieredMergePolicy;DDD)V

    return-object v2

    .line 469
    .end local v4    # "finalMergeScore":D
    .end local v6    # "skew":D
    .end local v8    # "nonDelRatio":D
    .end local v12    # "mergeScore":D
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 470
    .local v10, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v14

    .line 471
    .local v14, "segBytes":J
    add-long v16, v16, v14

    .line 472
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lorg/apache/lucene/index/TieredMergePolicy;->floorSize(J)J

    move-result-wide v22

    add-long v18, v18, v22

    .line 473
    invoke-virtual {v10}, Lorg/apache/lucene/index/SegmentInfoPerCommit;->sizeInBytes()J

    move-result-wide v22

    add-long v20, v20, v22

    goto :goto_0

    .line 487
    .end local v10    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .end local v14    # "segBytes":J
    :cond_1
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lorg/apache/lucene/index/TieredMergePolicy;->floorSize(J)J

    move-result-wide v2

    long-to-double v2, v2

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v6, v2, v22

    .restart local v6    # "skew":D
    goto :goto_1
.end method

.method public setFloorSegmentMB(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 180
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "floorSegmentMB must be >= 0.0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    const-wide/high16 v0, 0x4130000000000000L    # 1048576.0

    mul-double/2addr p1, v0

    .line 184
    const-wide/high16 v0, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    .line 185
    return-object p0

    .line 184
    :cond_1
    double-to-long v0, p1

    goto :goto_0
.end method

.method public setForceMergeDeletesPctAllowed(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 199
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 200
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "forceMergeDeletesPctAllowed must be between 0.0 and 100.0 inclusive (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    .line 203
    return-object p0
.end method

.method public final setMaxCFSSegmentSizeMB(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 720
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 721
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxCFSSegmentSizeMB must be >=0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 723
    :cond_0
    const-wide/high16 v0, 0x4130000000000000L    # 1048576.0

    mul-double/2addr p1, v0

    .line 724
    const-wide/high16 v0, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxCFSSegmentSize:J

    .line 725
    return-object p0

    .line 724
    :cond_1
    double-to-long v0, p1

    goto :goto_0
.end method

.method public setMaxMergeAtOnce(I)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # I

    .prologue
    .line 102
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxMergeAtOnce must be > 1 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    .line 106
    return-object p0
.end method

.method public setMaxMergeAtOnceExplicit(I)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # I

    .prologue
    .line 122
    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxMergeAtOnceExplicit must be > 1 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    iput p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    .line 126
    return-object p0
.end method

.method public setMaxMergedSegmentMB(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 142
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "maxMergedSegmentMB must be >=0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    const-wide/high16 v0, 0x4130000000000000L    # 1048576.0

    mul-double/2addr p1, v0

    .line 146
    const-wide/high16 v0, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    iput-wide v0, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    .line 147
    return-object p0

    .line 146
    :cond_1
    double-to-long v0, p1

    goto :goto_0
.end method

.method public setNoCFSRatio(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "noCFSRatio"    # D

    .prologue
    .line 257
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 258
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "noCFSRatio must be 0.0 to 1.0 inclusive; got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_1
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    .line 261
    return-object p0
.end method

.method public setReclaimDeletesWeight(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 162
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reclaimDeletesWeight must be >= 0.0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->reclaimDeletesWeight:D

    .line 166
    return-object p0
.end method

.method public setSegmentsPerTier(D)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 3
    .param p1, "v"    # D

    .prologue
    .line 222
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "segmentsPerTier must be >= 2.0 (got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_0
    iput-wide p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    .line 226
    return-object p0
.end method

.method public setUseCompoundFile(Z)Lorg/apache/lucene/index/TieredMergePolicy;
    .locals 0
    .param p1, "useCompoundFile"    # Z

    .prologue
    .line 240
    iput-boolean p1, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    .line 241
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const-wide/16 v6, 0x400

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 697
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "maxMergeAtOnce="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnce:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    const-string v1, "maxMergeAtOnceExplicit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergeAtOnceExplicit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 699
    const-string v1, "maxMergedSegmentMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxMergedSegmentBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string v1, "floorSegmentMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->floorSegmentBytes:J

    div-long/2addr v2, v6

    long-to-double v2, v2

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    const-string v1, "forceMergeDeletesPctAllowed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->forceMergeDeletesPctAllowed:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 702
    const-string v1, "segmentsPerTier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->segsPerTier:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    const-string v1, "useCompoundFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->useCompoundFile:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    const-string v1, "maxCFSSegmentSizeMB="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->getMaxCFSSegmentSizeMB()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 705
    const-string v1, "noCFSRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lorg/apache/lucene/index/TieredMergePolicy;->noCFSRatio:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 706
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public useCompoundFile(Lorg/apache/lucene/index/SegmentInfos;Lorg/apache/lucene/index/SegmentInfoPerCommit;)Z
    .locals 12
    .param p1, "infos"    # Lorg/apache/lucene/index/SegmentInfos;
    .param p2, "mergedInfo"    # Lorg/apache/lucene/index/SegmentInfoPerCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 641
    invoke-virtual {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->getUseCompoundFile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 642
    const/4 v1, 0x0

    .line 655
    :goto_0
    return v1

    .line 644
    :cond_0
    invoke-direct {p0, p2}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v2

    .line 645
    .local v2, "mergedInfoSize":J
    iget-wide v6, p0, Lorg/apache/lucene/index/TieredMergePolicy;->maxCFSSegmentSize:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 646
    const/4 v1, 0x0

    goto :goto_0

    .line 648
    :cond_1
    invoke-virtual {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->getNoCFSRatio()D

    move-result-wide v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v6, v8

    if-ltz v1, :cond_2

    .line 649
    const/4 v1, 0x1

    goto :goto_0

    .line 651
    :cond_2
    const-wide/16 v4, 0x0

    .line 652
    .local v4, "totalSize":J
    invoke-virtual {p1}, Lorg/apache/lucene/index/SegmentInfos;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_3

    .line 655
    long-to-double v6, v2

    invoke-virtual {p0}, Lorg/apache/lucene/index/TieredMergePolicy;->getNoCFSRatio()D

    move-result-wide v8

    long-to-double v10, v4

    mul-double/2addr v8, v10

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_0

    .line 652
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/SegmentInfoPerCommit;

    .line 653
    .local v0, "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    invoke-direct {p0, v0}, Lorg/apache/lucene/index/TieredMergePolicy;->size(Lorg/apache/lucene/index/SegmentInfoPerCommit;)J

    move-result-wide v6

    add-long/2addr v4, v6

    goto :goto_1

    .line 655
    .end local v0    # "info":Lorg/apache/lucene/index/SegmentInfoPerCommit;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method
