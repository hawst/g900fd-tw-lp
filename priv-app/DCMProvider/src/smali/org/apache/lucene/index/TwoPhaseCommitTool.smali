.class public final Lorg/apache/lucene/index/TwoPhaseCommitTool;
.super Ljava/lang/Object;
.source "TwoPhaseCommitTool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/lucene/index/TwoPhaseCommitTool$CommitFailException;,
        Lorg/apache/lucene/index/TwoPhaseCommitTool$PrepareCommitFailException;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs execute([Lorg/apache/lucene/index/TwoPhaseCommit;)V
    .locals 4
    .param p0, "objects"    # [Lorg/apache/lucene/index/TwoPhaseCommit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/lucene/index/TwoPhaseCommitTool$PrepareCommitFailException;,
            Lorg/apache/lucene/index/TwoPhaseCommitTool$CommitFailException;
        }
    .end annotation

    .prologue
    .line 96
    const/4 v2, 0x0

    .line 99
    .local v2, "tpc":Lorg/apache/lucene/index/TwoPhaseCommit;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    array-length v3, p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v0, v3, :cond_0

    .line 114
    const/4 v0, 0x0

    :goto_1
    :try_start_1
    array-length v3, p0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    if-lt v0, v3, :cond_2

    .line 126
    return-void

    .line 100
    :cond_0
    :try_start_2
    aget-object v2, p0, v0

    .line 101
    if-eqz v2, :cond_1

    .line 102
    invoke-interface {v2}, Lorg/apache/lucene/index/TwoPhaseCommit;->prepareCommit()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 99
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :catch_0
    move-exception v1

    .line 108
    .local v1, "t":Ljava/lang/Throwable;
    invoke-static {p0}, Lorg/apache/lucene/index/TwoPhaseCommitTool;->rollback([Lorg/apache/lucene/index/TwoPhaseCommit;)V

    .line 109
    new-instance v3, Lorg/apache/lucene/index/TwoPhaseCommitTool$PrepareCommitFailException;

    invoke-direct {v3, v1, v2}, Lorg/apache/lucene/index/TwoPhaseCommitTool$PrepareCommitFailException;-><init>(Ljava/lang/Throwable;Lorg/apache/lucene/index/TwoPhaseCommit;)V

    throw v3

    .line 115
    .end local v1    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_3
    aget-object v2, p0, v0

    .line 116
    if-eqz v2, :cond_3

    .line 117
    invoke-interface {v2}, Lorg/apache/lucene/index/TwoPhaseCommit;->commit()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 114
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 120
    :catch_1
    move-exception v1

    .line 123
    .restart local v1    # "t":Ljava/lang/Throwable;
    invoke-static {p0}, Lorg/apache/lucene/index/TwoPhaseCommitTool;->rollback([Lorg/apache/lucene/index/TwoPhaseCommit;)V

    .line 124
    new-instance v3, Lorg/apache/lucene/index/TwoPhaseCommitTool$CommitFailException;

    invoke-direct {v3, v1, v2}, Lorg/apache/lucene/index/TwoPhaseCommitTool$CommitFailException;-><init>(Ljava/lang/Throwable;Lorg/apache/lucene/index/TwoPhaseCommit;)V

    throw v3
.end method

.method private static varargs rollback([Lorg/apache/lucene/index/TwoPhaseCommit;)V
    .locals 4
    .param p0, "objects"    # [Lorg/apache/lucene/index/TwoPhaseCommit;

    .prologue
    .line 62
    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 71
    return-void

    .line 62
    :cond_0
    aget-object v0, p0, v1

    .line 65
    .local v0, "tpc":Lorg/apache/lucene/index/TwoPhaseCommit;
    if-eqz v0, :cond_1

    .line 67
    :try_start_0
    invoke-interface {v0}, Lorg/apache/lucene/index/TwoPhaseCommit;->rollback()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :catch_0
    move-exception v3

    goto :goto_1
.end method
