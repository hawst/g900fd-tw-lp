.class public final enum Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
.super Ljava/lang/Enum;
.source "MergePolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/lucene/index/MergePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MergeTrigger"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/apache/lucene/index/MergePolicy$MergeTrigger;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

.field public static final enum EXPLICIT:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

.field public static final enum FULL_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

.field public static final enum MERGE_FINISHED:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

.field public static final enum SEGMENT_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 466
    new-instance v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const-string v1, "SEGMENT_FLUSH"

    invoke-direct {v0, v1, v2}, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;-><init>(Ljava/lang/String;I)V

    .line 469
    sput-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->SEGMENT_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    .line 470
    new-instance v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const-string v1, "FULL_FLUSH"

    invoke-direct {v0, v1, v3}, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;-><init>(Ljava/lang/String;I)V

    .line 474
    sput-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->FULL_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    .line 475
    new-instance v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v4}, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;-><init>(Ljava/lang/String;I)V

    .line 478
    sput-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->EXPLICIT:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    .line 480
    new-instance v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    const-string v1, "MERGE_FINISHED"

    invoke-direct {v0, v1, v5}, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;-><init>(Ljava/lang/String;I)V

    .line 483
    sput-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->MERGE_FINISHED:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    .line 465
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    sget-object v1, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->SEGMENT_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    aput-object v1, v0, v2

    sget-object v1, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->FULL_FLUSH:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    aput-object v1, v0, v3

    sget-object v1, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->EXPLICIT:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    aput-object v1, v0, v4

    sget-object v1, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->MERGE_FINISHED:Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    aput-object v1, v0, v5

    sput-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->ENUM$VALUES:[Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    return-object v0
.end method

.method public static values()[Lorg/apache/lucene/index/MergePolicy$MergeTrigger;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lorg/apache/lucene/index/MergePolicy$MergeTrigger;->ENUM$VALUES:[Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    array-length v1, v0

    new-array v2, v1, [Lorg/apache/lucene/index/MergePolicy$MergeTrigger;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
