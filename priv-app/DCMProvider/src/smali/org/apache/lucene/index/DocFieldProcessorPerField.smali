.class final Lorg/apache/lucene/index/DocFieldProcessorPerField;
.super Ljava/lang/Object;
.source "DocFieldProcessorPerField.java"


# instance fields
.field private final bytesUsed:Lorg/apache/lucene/util/Counter;

.field final consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

.field private final dvFields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/apache/lucene/index/FieldInfo;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field fieldCount:I

.field final fieldInfo:Lorg/apache/lucene/index/FieldInfo;

.field fields:[Lorg/apache/lucene/index/IndexableField;

.field lastGen:I

.field next:Lorg/apache/lucene/index/DocFieldProcessorPerField;


# direct methods
.method public constructor <init>(Lorg/apache/lucene/index/DocFieldProcessor;Lorg/apache/lucene/index/FieldInfo;)V
    .locals 1
    .param p1, "docFieldProcessor"    # Lorg/apache/lucene/index/DocFieldProcessor;
    .param p2, "fieldInfo"    # Lorg/apache/lucene/index/FieldInfo;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->lastGen:I

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/apache/lucene/index/IndexableField;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/index/IndexableField;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->dvFields:Ljava/util/Map;

    .line 46
    iget-object v0, p1, Lorg/apache/lucene/index/DocFieldProcessor;->consumer:Lorg/apache/lucene/index/DocFieldConsumer;

    invoke-virtual {v0, p2}, Lorg/apache/lucene/index/DocFieldConsumer;->addField(Lorg/apache/lucene/index/FieldInfo;)Lorg/apache/lucene/index/DocFieldConsumerPerField;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    .line 47
    iput-object p2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldInfo:Lorg/apache/lucene/index/FieldInfo;

    .line 48
    iget-object v0, p1, Lorg/apache/lucene/index/DocFieldProcessor;->bytesUsed:Lorg/apache/lucene/util/Counter;

    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->bytesUsed:Lorg/apache/lucene/util/Counter;

    .line 49
    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->consumer:Lorg/apache/lucene/index/DocFieldConsumerPerField;

    invoke-virtual {v0}, Lorg/apache/lucene/index/DocFieldConsumerPerField;->abort()V

    .line 64
    return-void
.end method

.method public addField(Lorg/apache/lucene/index/IndexableField;)V
    .locals 5
    .param p1, "field"    # Lorg/apache/lucene/index/IndexableField;

    .prologue
    const/4 v4, 0x0

    .line 52
    iget v2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    iget-object v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/index/IndexableField;

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 53
    iget v2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    add-int/lit8 v2, v2, 0x1

    sget v3, Lorg/apache/lucene/util/RamUsageEstimator;->NUM_BYTES_OBJECT_REF:I

    invoke-static {v2, v3}, Lorg/apache/lucene/util/ArrayUtil;->oversize(II)I

    move-result v1

    .line 54
    .local v1, "newSize":I
    new-array v0, v1, [Lorg/apache/lucene/index/IndexableField;

    .line 55
    .local v0, "newArray":[Lorg/apache/lucene/index/IndexableField;
    iget-object v2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/index/IndexableField;

    iget v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    iput-object v0, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/index/IndexableField;

    .line 59
    .end local v0    # "newArray":[Lorg/apache/lucene/index/IndexableField;
    .end local v1    # "newSize":I
    :cond_0
    iget-object v2, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fields:[Lorg/apache/lucene/index/IndexableField;

    iget v3, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/lucene/index/DocFieldProcessorPerField;->fieldCount:I

    aput-object p1, v2, v3

    .line 60
    return-void
.end method
