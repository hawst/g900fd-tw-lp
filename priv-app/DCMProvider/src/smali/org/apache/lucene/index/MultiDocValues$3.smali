.class Lorg/apache/lucene/index/MultiDocValues$3;
.super Lorg/apache/lucene/index/BinaryDocValues;
.source "MultiDocValues.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/lucene/index/MultiDocValues;->getBinaryValues(Lorg/apache/lucene/index/IndexReader;Ljava/lang/String;)Lorg/apache/lucene/index/BinaryDocValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$starts:[I

.field private final synthetic val$values:[Lorg/apache/lucene/index/BinaryDocValues;


# direct methods
.method constructor <init>([I[Lorg/apache/lucene/index/BinaryDocValues;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lorg/apache/lucene/index/MultiDocValues$3;->val$starts:[I

    iput-object p2, p0, Lorg/apache/lucene/index/MultiDocValues$3;->val$values:[Lorg/apache/lucene/index/BinaryDocValues;

    .line 173
    invoke-direct {p0}, Lorg/apache/lucene/index/BinaryDocValues;-><init>()V

    return-void
.end method


# virtual methods
.method public get(ILorg/apache/lucene/util/BytesRef;)V
    .locals 3
    .param p1, "docID"    # I
    .param p2, "result"    # Lorg/apache/lucene/util/BytesRef;

    .prologue
    .line 176
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocValues$3;->val$starts:[I

    invoke-static {p1, v1}, Lorg/apache/lucene/index/ReaderUtil;->subIndex(I[I)I

    move-result v0

    .line 177
    .local v0, "subIndex":I
    iget-object v1, p0, Lorg/apache/lucene/index/MultiDocValues$3;->val$values:[Lorg/apache/lucene/index/BinaryDocValues;

    aget-object v1, v1, v0

    iget-object v2, p0, Lorg/apache/lucene/index/MultiDocValues$3;->val$starts:[I

    aget v2, v2, v0

    sub-int v2, p1, v2

    invoke-virtual {v1, v2, p2}, Lorg/apache/lucene/index/BinaryDocValues;->get(ILorg/apache/lucene/util/BytesRef;)V

    .line 178
    return-void
.end method
